cmake_minimum_required(VERSION 2.6)

project(LINEABLE_ONE C ASM)

set(SILABS_MCU EFR32BG12P)
add_definitions(-DEFR32BG12P132F1024GM48=1)
add_definitions(-DHAL_CONFIG=1)
add_definitions(-D__HEAP_SIZE=0xD00)
add_definitions(-DARM_MATH_CM4=1)
add_definitions(-D__StackLimit=0x20000000)
add_definitions(-D__STACK_SIZE=0x800)
add_definitions(-DMBEDTLS_CONFIG_FILE="library/mbedtls/mbedtls_config.h")

# define some convenience variables to point to example project directories
set(SDK_PATH ${CMAKE_SOURCE_DIR})

include("${CMAKE_SOURCE_DIR}/build/CMake_build.cmake")

SILABS_toolchainSetup()
SILABS_setup()

SILABS_addSDK()
SILABS_addLibs()

SILABS_addProjectSources()

SILABS_addExecutable(LINEABLE_ONE_SDK)
