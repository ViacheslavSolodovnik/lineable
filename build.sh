#!/bin/bash

shopt -s expand_aliases
# alias die='diefunc "${FUNCNAME:-$0}" "$LINENO"'
alias die='diefunc "${ME}": "${FUNCNAME:-$0}": "$LINENO"'

if [ "$(uname)" == "Darwin" ]; then
    readonly CPU_COUNT=$(sysctl -n hw.logicalcpu)
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    readonly CPU_COUNT=$(nproc)
fi

readonly DOCKER_IMAGE="crosscompile/cortex-m"
readonly ME=${0##*/}
readonly DOCKER_WORKDIR=$(pwd)
readonly TMP_BUILD_DIR="./build/cmake-build"

diefunc() {
	echo "${@}" >&2
	exit 1
}

edo() {
	echo ">>> ${@}" >&2
	"${@}" || die "${@}"
}

container_run() {
	local cmd="${1}"; shift
	edo docker run -t --rm -v "$(pwd)":${DOCKER_WORKDIR} -w ${DOCKER_WORKDIR} \
		${DOCKER_IMAGE} \
		sh -c -x "${cmd}"
}

usage() {
	cat >&2 <<EOF
Usage:
	build-docker     - builds docker image for crosscompilation
	generate-cmake   - removes all previously build files and issue CMake
	build            - issues build on previously generated CMake files
	rebuild          - regenerates CMake files and issue build
	create-gbl-files - create bootloader files
EOF
exit 1
}

mount_tmpfs() {
	if ! mount | grep "${TMP_BUILD_DIR}" | grep tmpfs > /dev/null; then
		echo "Creating mountpoint"
		edo mkdir -p "${TMP_BUILD_DIR}"

		echo "Mount TMPFS"
		sudo mount -t tmpfs -o size=512m tmpfs "${TMP_BUILD_DIR}"
	fi
}

unmount_tmpfs() {
	if mount | grep "${TMP_BUILD_DIR}" | grep tmpfs > /dev/null; then
		echo "Unmount TMPFS"
		sudo umount "${TMP_BUILD_DIR}"

		echo "Removing mountpoint"
		rmdir "${TMP_BUILD_DIR}"
	fi
}

if [[ -f "${1}" || "$#" -lt 1 ]]; then
	usage
fi

case "$1" in
	"build-docker")
		pushd build &>/dev/null;
		edo docker build -t $DOCKER_IMAGE .
		;;

	"generate-cmake")
		# clearing tmpfs
		unmount_tmpfs
		mount_tmpfs

		container_run "\
			cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=OFF -B \"${TMP_BUILD_DIR}\" ."
		;;

	"rebuild")
		container_run "\
			make -C \"${TMP_BUILD_DIR}\" -j$CPU_COUNT clean;\
			 make -C \"${TMP_BUILD_DIR}\" -j$CPU_COUNT"
		;;

	"build")
		container_run "\
			cmake --build \"${TMP_BUILD_DIR}\" -j $CPU_COUNT"
		;;

	"create-gbl-files")
		container_run "\
			make -C \"${TMP_BUILD_DIR}\" -j$CPU_COUNT gbl"
		;;

	*)
		echo "You have failed to specify what to do correctly."
		usage
		;;
esac
