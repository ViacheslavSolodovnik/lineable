if(NOT WIN32)
    string(ASCII 27 Esc)
    set(ColourReset "${Esc}[m")
    set(ColourBold  "${Esc}[1m")
    set(Red         "${Esc}[31m")
    set(Green       "${Esc}[32m")
    set(Yellow      "${Esc}[33m")
    set(Blue        "${Esc}[34m")
    set(Magenta     "${Esc}[35m")
    set(Cyan        "${Esc}[36m")
    set(White       "${Esc}[37m")
    set(BoldRed     "${Esc}[1;31m")
    set(BoldGreen   "${Esc}[1;32m")
    set(BoldYellow  "${Esc}[1;33m")
    set(BoldBlue    "${Esc}[1;34m")
    set(BoldMagenta "${Esc}[1;35m")
    set(BoldCyan    "${Esc}[1;36m")
    set(BoldWhite   "${Esc}[1;37m")
endif()

function(message)
    list(GET ARGV 0 MessageType)
    if(MessageType STREQUAL FATAL_ERROR OR MessageType STREQUAL SEND_ERROR)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${BoldRed}${ARGV}${ColourReset}")
    elseif(MessageType STREQUAL WARNING)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${BoldYellow}${ARGV}${ColourReset}")
    elseif(MessageType STREQUAL AUTHOR_WARNING)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${BoldCyan}${ARGV}${ColourReset}")
    elseif(MessageType STREQUAL STATUS)
        list(REMOVE_AT ARGV 0)
        _message(${MessageType} "${Green}${ARGV}${ColourReset}")
    else()
        _message("${ARGV}")
    endif()
endfunction()

# check if all the necessary tools paths have been provided.
if (NOT SDK_PATH)
    message(FATAL_ERROR "The path to the SDK (SDK_PATH) must be set.")
endif()

# convert toolchain path to bin path
if (DEFINED ARM_NONE_EABI_TOOLCHAIN_PATH)
    set(ARM_NONE_EABI_TOOLCHAIN_BIN_PATH ${ARM_NONE_EABI_TOOLCHAIN_PATH}/bin)
endif()

# check if the SILABS_MCU has been set
if (NOT SILABS_MCU MATCHES "EFR32MG12P" AND NOT SILABS_MCU MATCHES "EFR32BG12P")
    message(FATAL_ERROR "Only EFR32MG12P and EFR32BG12P is supported right now")
elseif (NOT SILABS_MCU)
    message(FATAL_ERROR "SILABS_MCU target must be defined")
endif()

# must be set in file (not macro) scope (in macro would point to parent CMake directory)
set(DIR_OF_SILABS_CMAKE ${CMAKE_CURRENT_LIST_DIR})

macro(SILABS_toolchainSetup)
    include(${DIR_OF_SILABS_CMAKE}/arm-gcc-toolchain.cmake)
endmacro()

macro(SILABS_setup)
    if (NOT DEFINED ARM_GCC_TOOLCHAIN)
        message(FATAL_ERROR "The toolchain must be set up before calling this macro")
    endif()
    # fix on macOS: prevent cmake from adding implicit parameters to Xcode
    set(CMAKE_OSX_SYSROOT "/")
    set(CMAKE_OSX_DEPLOYMENT_TARGET "")

    # language standard/version settings
    set(CMAKE_C_STANDARD 99)

    if (SILABS_MCU MATCHES "EFR32BG12P")
        set(LINKER_SCRIPT "${SDK_PATH}/projects/LINEABLE_ONE_SDK/efr32bg12p132f1024gm48.ld")
    endif()

    # Debug flags
    set(DEBUG_FLAGS "-g3 -gdwarf-2")

    # CPU specific settings
    set(CPU_FLAGS "-mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=softfp")

    set(COMMON_FLAGS "-mthumb ${DEBUG_FLAGS} ${CPU_FLAGS}")

    # compiler/assambler/linker flags
    set(CMAKE_C_FLAGS "-std=c99 -O2 -Wall -ffunction-sections -fdata-sections \
    -fmessage-length=0 ${COMMON_FLAGS}")

    set(CMAKE_ASM_FLAGS "-x assembler-with-cpp ${COMMON_FLAGS}")

    set(CMAKE_EXE_LINKER_FLAGS "-T${LINKER_SCRIPT} --specs=nano.specs -u _printf_float -Xlinker \
        --gc-sections ${COMMON_FLAGS}" CACHE STRING "" FORCE)

    # fix: unrecognized -rdynamic option
    set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "")
    set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "")

endmacro()

macro(SILABS_addSDK)
    file(GLOB SDK_SRC_C
        # Sdk sources
        "${SDK_PATH}/util/gps/*.c"
        "${SDK_PATH}/util/hrm_siliconlabs/accelerometer/*.c"
        "${SDK_PATH}/util/hrm_siliconlabs/*.c"
        "${SDK_PATH}/util/hrm_siliconlabs/si117xdrv/*.c"
        "${SDK_PATH}/util/pedometer/*.c"
        "${SDK_PATH}/platform/bootloader/api/*.c"
        "${SDK_PATH}/platform/emlib/src/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/auth/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/clk/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/collections/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/common/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/kal/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/lib/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/logging/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/op_lock/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/platform_mgr/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/ring_buf/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/rtos/*.c"
        "${SDK_PATH}/platform/micrium_os/common/source/shell/*.c"
        "${SDK_PATH}/platform/micrium_os/cpu/source/*.c"
        "${SDK_PATH}/platform/micrium_os/drivers/io/source/sd/*.c"
        "${SDK_PATH}/platform/micrium_os/drivers/io/source/serial/*.c"
        "${SDK_PATH}/platform/micrium_os/io/source/*.c"
        "${SDK_PATH}/platform/micrium_os/io/source/sd/*.c"
        "${SDK_PATH}/platform/micrium_os/io/source/serial/*.c"
        "${SDK_PATH}/platform/micrium_os/io/source/serial/spi/*.c"
        "${SDK_PATH}/platform/micrium_os/kernel/source/*.c"
        "${SDK_PATH}/platform/micrium_os/ports/source/generic/*.c"
        "${SDK_PATH}/platform/micrium_os/ports/source/gnu/*.c"
        "${SDK_PATH}/protocol/bluetooth/ble_stack/src/soc/*.c"
        "${SDK_PATH}/util/terminal/src/*.c"
        "${SDK_PATH}/util/third_party/segger/systemview/SEGGER/*.c"
        "${SDK_PATH}/util/third_party/segger/systemview/Config/*.c"
        "${SDK_PATH}/util/third_party/segger/systemview/Sample/MicriumOSKernel/Config/*.c"
        "${SDK_PATH}/util/third_party/segger/systemview/Sample/MicriumOSKernel/*.c"
        "${SDK_PATH}/util/third_party/segger/systemview/Sample/MicriumOSKernel/Config/Cortex-M/*.c"
    )

    # Setting assembler sources
    list(APPEND SDK_SRC_ASM
        "${SDK_PATH}/platform/micrium_os/ports/source/gnu/armv7m_lib_mem.S"
        "${SDK_PATH}/platform/micrium_os/ports/source/gnu/armv7m_cpu_a.S"
        "${SDK_PATH}/platform/micrium_os/ports/source/gnu/armv7m_os_cpu_a.S"
    )

    include_directories(
        # SDK includes
        "${SDK_PATH}/platform/micrium_os/ports/source/generic"
        "${SDK_PATH}/platform/halconfig/inc/hal-config"
        "${SDK_PATH}/platform/bootloader/api"
        "${SDK_PATH}/platform/micrium_os/common/source/rtos"
        "${SDK_PATH}/platform/micrium_os/common/include"
        "${SDK_PATH}/platform/micrium_os/common/source/common"
        "${SDK_PATH}/platform/micrium_os/cpu/include"
        "${SDK_PATH}/platform/micrium_os/ports/source/gnu"
        "${SDK_PATH}/platform/micrium_os"
        "${SDK_PATH}/platform/micrium_os/common/source/collections"
        "${SDK_PATH}/platform/micrium_os/common/source/ring_buf"
        "${SDK_PATH}/platform/emlib/inc"
        "${SDK_PATH}/platform/micrium_os/common/source/lib"
        "${SDK_PATH}/platform/micrium_os/kernel/include"
        "${SDK_PATH}/platform/micrium_os/common/source/preprocessor"
        "${SDK_PATH}/platform/micrium_os/cpu/source"
        "${SDK_PATH}/platform/micrium_os/kernel/source"
        "${SDK_PATH}/platform/radio/rail_lib/chip/efr32/efr32xg1x"
        "${SDK_PATH}/platform/micrium_os/common/source/platform_mgr"
        "${SDK_PATH}/platform/radio/rail_lib/chip/efr32"
        "${SDK_PATH}/platform/emlib/src"
        "${SDK_PATH}/platform/micrium_os/common/source/kal"
        "${SDK_PATH}/platform/bootloader"
        "${SDK_PATH}/platform/micrium_os/common/source/logging"
        "${SDK_PATH}/platform/bluetooth/ble_stack/src/soc"
        "${SDK_PATH}/platform/radio/rail_lib/common"
        "${SDK_PATH}/platform/CMSIS/Include/"
        "${SDK_PATH}/util/third_party/segger/systemview/SEGGER"
        "${SDK_PATH}/util/third_party/segger/systemview/Config"
        "${SDK_PATH}/util/third_party/segger/systemview/Sample/MicriumOSKernel/Config"
        "${SDK_PATH}/util/third_party/segger/systemview/Sample/MicriumOSKernel"
        "${SDK_PATH}/util/terminal"
        "${SDK_PATH}/util/pedometer"
        "${SDK_PATH}/util/hrm_siliconlabs"
        "${SDK_PATH}/util/hrm_siliconlabs/lib"
        "${SDK_PATH}/util/hrm_siliconlabs/si117xdrv"
        "${SDK_PATH}/util/hrm_siliconlabs/accelerometer"
        "${SDK_PATH}/util/gps"
        "${SDK_PATH}/protocol/bluetooth/ble_stack/inc/"
        "${SDK_PATH}/protocol/bluetooth/ble_stack/inc/common/"
        "${SDK_PATH}/protocol/bluetooth/ble_stack/inc/soc/"
    )

    file(GLOB GLOB_RES
        "${SDK_PATH}/platform/Device/SiliconLabs/${SILABS_MCU}/Source/GCC/*.c"
        "${SDK_PATH}/platform/Device/SiliconLabs/${SILABS_MCU}/Source/*.c"
    )

    list(APPEND SDK_SRC_C ${GLOB_RES})

    include_directories(
        "${SDK_PATH}/platform/Device/SiliconLabs/${SILABS_MCU}/Include/"
    )

    set(SDK_SRC "${SDK_SRC_C}" "${SDK_SRC_ASM}")

endmacro()


macro(SILABS_addProjectSources)
    set(PROJECT_SOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/projects/LINEABLE_ONE_SDK")

    file(GLOB PROJ_SRC
        # Project sources
        "${PROJECT_SOURCE_DIR}/app/*.c"
        "${PROJECT_SOURCE_DIR}/*.c"
        "${PROJECT_SOURCE_DIR}/bsp/source/*.c"
        "${PROJECT_SOURCE_DIR}/drivers/source/*.c"
        "${PROJECT_SOURCE_DIR}/drivers/fuelgauge/lc709203f/*.c"
        "${PROJECT_SOURCE_DIR}/drivers/peripheral/i2c/*.c"
        "${PROJECT_SOURCE_DIR}/drivers/pmic/max14676/*.c"
        "${PROJECT_SOURCE_DIR}/drivers/pmic/tps65720/*.c"
        "${PROJECT_SOURCE_DIR}/drivers/radio/sx1276/*.c"
        "${PROJECT_SOURCE_DIR}/framework/*.c"
        "${PROJECT_SOURCE_DIR}/lora/crypto/*.c"
        "${PROJECT_SOURCE_DIR}/lora/*.c"
        "${PROJECT_SOURCE_DIR}/lora/mac/*.c"
        "${PROJECT_SOURCE_DIR}/lora/mac/region/*.c"
        "${PROJECT_SOURCE_DIR}/library/mbedtls/library/*.c"
        "${PROJECT_SOURCE_DIR}/library/mbedtls/sl_crypto/src/*.c"
    )

    include_directories(
        # Project includes
        "${PROJECT_SOURCE_DIR}"
        "${PROJECT_SOURCE_DIR}/lora/mac"
        "${PROJECT_SOURCE_DIR}/drivers/include"
        "${PROJECT_SOURCE_DIR}/drivers/fuelgauge"
        "${PROJECT_SOURCE_DIR}/drivers/fuelgauge/lc709203f"
        "${PROJECT_SOURCE_DIR}/drivers/peripheral/i2c"
        "${PROJECT_SOURCE_DIR}/drivers/pmic/max14676"
        "${PROJECT_SOURCE_DIR}/drivers/pmic/tps65720"
        "${PROJECT_SOURCE_DIR}/drivers/pmic"
        "${PROJECT_SOURCE_DIR}/drivers/radio"
        "${PROJECT_SOURCE_DIR}/framework"
        "${PROJECT_SOURCE_DIR}/lora/radio"
        "${PROJECT_SOURCE_DIR}/app"
        "${PROJECT_SOURCE_DIR}/app/bluetooth/common/stack_bridge"
        "${PROJECT_SOURCE_DIR}/config"
        "${PROJECT_SOURCE_DIR}/lora"
        "${PROJECT_SOURCE_DIR}/bsp/source"
        "${PROJECT_SOURCE_DIR}/lora/radio/sx1276"
        "${PROJECT_SOURCE_DIR}/lora/crypto"
        "${PROJECT_SOURCE_DIR}/bsp/include"
        "${PROJECT_SOURCE_DIR}/lora/mac"
        "${PROJECT_SOURCE_DIR}/lora/mac/region"
        "${PROJECT_SOURCE_DIR}/library/mbedtls/include"
        "${PROJECT_SOURCE_DIR}/library/mbedtls/sl_crypto/include"
    )

endmacro()

############################################################
#                    GBL files
############################################################

# create commands for creating gecko bootloader files from ${EXECUTABLE_NAME}
macro(SILABS_addGblCommands EXECUTABLE_NAME)
    set(COMMANDER "${CMAKE_SOURCE_DIR}/build/commander/commander")
    set(OTA_APPLOADER "ota_apploader")
    set(OTA_APPLICATION "ota_application")
    set(UARTDFU_FULL "uartdfu_full")

    set(PATH_GBL "output_gbl")
    set(PATH_OUT "${EXECUTABLE_NAME}.out")
    set(GBL_SIGNING_KEY_FILE "app-sign-key.pem")
    set(GBL_ENCRYPT_KEY_FILE "app-encrypt-key.txt")

    set(GBL_TARGETS "")

    # create ${PATH_GBL} directory
    add_custom_target(${PATH_GBL}
        COMMAND ${CMAKE_COMMAND} -E make_directory ${PATH_GBL}
    )

    add_custom_target(gbl_files
        COMMAND ${CMAKE_OBJCOPY} -O srec -j .text_apploader* ${PATH_OUT} ${PATH_GBL}/${OTA_APPLOADER}.srec
        COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLOADER}.gbl --app ${PATH_GBL}/${OTA_APPLOADER}.srec

        COMMAND ${CMAKE_OBJCOPY} -O srec -j .text_application* ${PATH_OUT} ${PATH_GBL}/${OTA_APPLICATION}.srec
        COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLICATION}.gbl --app ${PATH_GBL}/${OTA_APPLICATION}.srec

        COMMAND ${CMAKE_OBJCOPY} -O srec -R .text_bootloader* ${PATH_OUT} ${PATH_GBL}/${UARTDFU_FULL}.srec
        COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${UARTDFU_FULL}.gbl --app ${PATH_GBL}/${UARTDFU_FULL}.srec

        DEPENDS ${EXECUTABLE_NAME} ${PATH_GBL}
    )

    LIST(APPEND GBL_TARGETS "gbl_files")

    if (EXISTS ${GBL_SIGNING_KEY_FILE})
        add_custom_target(sign_gbl_files
            COMMAND ${COMMANDER} convert ${PATH_GBL}/${OTA_APPLOADER}.srec --secureboot --keyfile ${GBL_SIGNING_KEY_FILE} -o ${PATH_GBL}/${OTA_APPLOADER}-signed.srec
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLOADER}-signed.gbl --app ${PATH_GBL}/${OTA_APPLOADER}-signed.srec --sign ${GBL_SIGNING_KEY_FILE}

            COMMAND ${COMMANDER} convert ${PATH_GBL}/${OTA_APPLICATION}.srec --secureboot --keyfile ${GBL_SIGNING_KEY_FILE} -o ${PATH_GBL}/${OTA_APPLICATION}-signed.srec
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLICATION}-signed.gbl --app ${PATH_GBL}/${OTA_APPLICATION}-signed.srec --sign ${GBL_SIGNING_KEY_FILE}

            COMMAND ${COMMANDER} convert ${PATH_GBL}/${OTA_APPLOADER}-signed.srec ${PATH_GBL}/${OTA_APPLICATION}-signed.srec -o ${PATH_GBL}/${UARTDFU_FULL}-signed.srec
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${UARTDFU_FULL}-signed.gbl --app ${PATH_GBL}/${UARTDFU_FULL}-signed.srec --sign ${GBL_SIGNING_KEY_FILE}

            DEPENDS gbl_files
        )

        LIST(APPEND GBL_TARGETS "sign_gbl_files")
    endif()


    if (EXISTS ${GBL_ENCRYPT_KEY_FILE})
        add_custom_target(enc_gbl_files
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLOADER}-encrypted.gbl --app ${PATH_GBL}/${OTA_APPLOADER}.srec --encrypt ${GBL_ENCRYPT_KEY_FILE}
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLICATION}-encrypted.gbl --app ${PATH_GBL}/${OTA_APPLICATION}.srec --encrypt ${GBL_ENCRYPT_KEY_FILE}
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${UARTDFU_FULL}-encrypted.gbl --app ${PATH_GBL}/${UARTDFU_FULL}.srec --encrypt ${GBL_ENCRYPT_KEY_FILE}

            DEPENDS gbl_files
        )

        LIST(APPEND GBL_TARGETS "enc_gbl_files")
    endif()

    if (EXISTS ${GBL_SIGNING_KEY_FILE} AND EXISTS ${GBL_ENCRYPT_KEY_FILE})
        add_custom_target(sign_enc_gbl_files
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLOADER}-signed-encrypted.gbl --app ${PATH_GBL}/${OTA_APPLOADER}.srec --encrypt ${GBL_ENCRYPT_KEY_FILE} --sign ${GBL_SIGNING_KEY_FILE}
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${OTA_APPLICATION}-signed-encrypted.gbl --app ${PATH_GBL}/${OTA_APPLICATION}.srec --encrypt ${GBL_ENCRYPT_KEY_FILE} --sign ${GBL_SIGNING_KEY_FILE}
            COMMAND ${COMMANDER} gbl create ${PATH_GBL}/${UARTDFU_FULL}-signed-encrypted.gbl --app ${PATH_GBL}/${UARTDFU_FULL}.srec --encrypt ${GBL_ENCRYPT_KEY_FILE} --sign ${GBL_SIGNING_KEY_FILE}

            DEPENDS gbl_files
        )

        LIST(APPEND GBL_TARGETS "sign_enc_gbl_files")
    endif()

    add_custom_target(gbl
        DEPENDS ${GBL_TARGETS}
    )

endmacro()

############################################################
#                    Libraries
############################################################


macro(SILABS_addLibs)
    set(SDK_LIBRARIES
        "${SDK_PATH}/protocol/bluetooth/lib/${SILABS_MCU}/GCC/libbluetooth.a"
        "${SDK_PATH}/protocol/bluetooth/lib/${SILABS_MCU}/GCC/libnvm3.a"
        "${SDK_PATH}/protocol/bluetooth/lib/${SILABS_MCU}/GCC/binapploader.o"
        "${SDK_PATH}/platform/radio/rail_lib/autogen/librail_release/librail_efr32xg12_gcc_release.a"
        "${SDK_PATH}/util/hrm_siliconlabs/lib/libsi117xhrm_gcc_cm4f.a"
    )

    # m is for the math library
    set(LIBRARIES ${SDK_LIBRARIES} m)

endmacro()

# adds a target for comiling and flashing an executable
macro(SILABS_addExecutable EXECUTABLE_NAME)
    # executable
    add_executable(${EXECUTABLE_NAME} ${SDK_SRC} ${PROJ_SRC})
    # link libraries
    target_link_libraries(${EXECUTABLE_NAME} ${LIBRARIES})

    set_target_properties(${EXECUTABLE_NAME} PROPERTIES SUFFIX ".out")
    set_target_properties(${EXECUTABLE_NAME} PROPERTIES LINK_FLAGS "-Wl,-Map=${EXECUTABLE_NAME}.map")

    # additional POST BUILD setps to create the .bin and .hex files
    add_custom_command(TARGET ${EXECUTABLE_NAME}
            POST_BUILD
            COMMAND ${CMAKE_SIZE_UTIL} ${EXECUTABLE_NAME}.out
            COMMAND ${CMAKE_OBJCOPY} -O binary ${EXECUTABLE_NAME}.out "${EXECUTABLE_NAME}.bin"
            COMMAND ${CMAKE_OBJCOPY} -O ihex ${EXECUTABLE_NAME}.out "${EXECUTABLE_NAME}.hex"
            COMMAND ${CMAKE_OBJCOPY} -O srec ${EXECUTABLE_NAME}.out "${EXECUTABLE_NAME}.s37"
            COMMENT "post build steps for ${EXECUTABLE_NAME}")

    SILABS_addGblCommands(${EXECUTABLE_NAME})

endmacro()
