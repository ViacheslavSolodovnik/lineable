/*******************************************************************************
 * @file pmic_max14676.c
 * @brief Driver for the MAXIM PMIC max14676d
 * @version 0.0
 ******************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>

#include "pmic_max14676.h"
#include "i2cspm.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint8_t PmicDeviceId;

/* Private function prototypes -----------------------------------------------*/
static int8_t pmic_read_reg(uint8_t Reg, uint8_t *Data);
static int8_t pmic_write_reg(uint8_t Reg, uint8_t Data);

static uint32_t max14676_charge_control_A(void);
static uint32_t max14676_charge_enable(void);
static uint32_t max14676_config(void);
static uint32_t max14676_charge_config(void);

/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name   : pmic_read_reg
* Description     : Generic Reading function. It must be fullfilled with either
*                 : I2C or SPI reading functions
* Input           : Register Address
* Output          : Data REad
* Return          : None
*******************************************************************************/
static int8_t pmic_read_reg(uint8_t reg, uint8_t *data)
{
    I2C_TransferReturn_TypeDef ret;
    uint8_t read_val;

    ret = I2CSPM_MasterRead(i2cmsId0, PMIC_I2C_BUS_ADDRESS, &reg, 1, &read_val, 1);
    *data = read_val;

    if (ret != i2cTransferDone)
        return PMIC_ERROR_I2C_TRANSACTION_FAILED;

    return PMIC_OK;
}

/*******************************************************************************
* Function Name   : pmic_write_reg
* Description     : Generic Writing function. It must be fullfilled with either
*                 : I2C or SPI writing function
* Input           : Register Address, Data to be written
* Output          : None
* Return          : None
*******************************************************************************/
static int8_t pmic_write_reg(uint8_t reg, uint8_t data)
{
    I2C_TransferReturn_TypeDef ret;

    ret = I2CSPM_MasterWrite(i2cmsId0, PMIC_I2C_BUS_ADDRESS, &reg, 1, &data, 1);

    if (ret != i2cTransferDone)
        return PMIC_ERROR_I2C_TRANSACTION_FAILED;

    return PMIC_OK;
}

/***************************************************************************//**
 * @brief
 *    Configure charge control A
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t max14676_charge_control_A(void)
{
#if PMIC_DEBUG
    uint8_t r_data = 0;
#endif

#if PMIC_DEBUG
    pmic_log("[Charge] : MAX14676_REG_CHG_CNTL_A\n");

    if (pmic_read_reg(MAX14676_REG_CHG_CNTL_A, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("\tBefore: reg = 0x%x\n", r_data);
#endif

    if (PmicDeviceId == PMIC_DEVICE_ID_MAX14676D) {
        // Configure charger registers
        if (pmic_write_reg(MAX14676_REG_CHG_CNTL_A, IF_CFG_7 << 2) != PMIC_OK)
            return PMIC_ERROR;
    } else if (PmicDeviceId == PMIC_DEVICE_ID_MAX14676) {
        // Configure charger registers
        if (pmic_write_reg(MAX14676_REG_CHG_CNTL_A, IF_CFG_4 << 2) != PMIC_OK)
            return PMIC_ERROR;
    }

#if PMIC_DEBUG
    if (pmic_read_reg(MAX14676_REG_CHG_CNTL_A, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("\tAfter : reg = 0x%x\n", r_data);
#endif

    return PMIC_OK;
}


/***************************************************************************//**
 * @brief
 *    Charge enable
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t max14676_charge_enable(void)
{
#if PMIC_DEBUG
    uint8_t r_data = 0;

    pmic_log("[Charge] : MAX14676_REG_CHG_CNTL_B\n");

    if (pmic_read_reg(MAX14676_REG_CHG_CNTL_B, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("\tBefore: reg = 0x%x\n", r_data);
#endif

    // CHG EN, NTC thermistor disabled
    if (pmic_write_reg(MAX14676_REG_CHG_CNTL_B, CHG_ENABLE) != PMIC_OK)
        return PMIC_ERROR;

#if PMIC_DEBUG
    if (pmic_read_reg(MAX14676_REG_CHG_CNTL_B, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("\tAfter : reg = 0x%x\n", r_data);
#endif

    return PMIC_OK;
}


/***************************************************************************//**
 * @brief
 *    Charge configuration of max14676
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t max14676_charge_config(void)
{
#if 0//#if PMIC_DEBUG
    uint8_t r_data = 0;
#endif

    if (max14676_charge_control_A() != PMIC_OK)
        return PMIC_ERROR;

#if 0//#if PMIC_DEBUG
    if (pmic_read_reg(MAX14676_REG_CHG_V_SET, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("MAX14676_REG_CHG_V_SET = 0x%x\r\n", r_data);

    if (pmic_write_reg(MAX14676_REG_CHG_V_SET, BAT_RE_CHG_1 | BAT_CHG_0) != PMIC_OK)
        return PMIC_ERROR;

    if (pmic_read_reg(MAX14676_REG_CHG_V_SET, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("MAX14676_REG_CHG_V_SET = 0x%x\r\n", r_data);
#endif

    if (max14676_charge_enable() != PMIC_OK)
        return PMIC_ERROR;

    return PMIC_OK;
}


/***************************************************************************//**
 * @brief
 *    Configure the power
 *
 * @param[in] enable, voltage
 *    boost enable/disable and, which holds the configuration parameters
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t max14676_pwr_config(void)
{
    if (pmic_write_reg(MAX14676_REG_PWR_CFG, 0x80) != PMIC_OK)
        return PMIC_ERROR;

#if PMIC_DEBUG
    uint8_t r_data = 0;

    if (pmic_read_reg(MAX14676_REG_PWR_CFG, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("MAX14676_REG_PWR_CFG = 0x%x\r\n", r_data);
#endif

    return PMIC_OK;
}

/***************************************************************************//**
 * @brief
 *    Configure the boost
 *
 * @param[in] enable, voltage
 *    boost enable/disable and, which holds the configuration parameters
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint32_t max14676_boost_config(uint8_t enable, uint8_t voltage)
{
    if (pmic_write_reg(MAX14676_REG_BST_CFG, enable | voltage) != PMIC_OK)
        return PMIC_ERROR;

    return PMIC_OK;
}


/***************************************************************************//**
 * @brief
 *    LDO enable for MAX14676 and MAX14676A
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint32_t max14676_ldo_enable(void)
{
    if (pmic_write_reg(MAX14676_REG_LDO_CFG, LDO_ENABLED) != PMIC_OK)
        return PMIC_ERROR;

#if PMIC_DEBUG
    uint8_t r_data = 0;

    if (pmic_read_reg(MAX14676_REG_LDO_CFG, &r_data) != PMIC_OK)
        return PMIC_ERROR;

    pmic_log("MAX14676_REG_LDO_CFG = 0x%x\r\n", r_data);
#endif

    return PMIC_OK;
}


/***************************************************************************//**
 * @brief
 *    Configuration for max14676
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t max14676_config(void)
{
#if 0// no default power on
    // Configure Power Supplies
    if (max14676_boost_config(BOOST_ENABLED, BOOST_VOL_13V) != PMIC_OK)
        return PMIC_ERROR;
#endif
    // Configure charger registers
    if (max14676_charge_config() != PMIC_OK)
        return PMIC_ERROR;

    return PMIC_OK;
}

/***************************************************************************//**
 * @brief
 *    Initializes the pmic (max14676d) chip
 *
 * @param[out] deviceId
 *    The device ID of the connected chip
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint32_t pmic_init(void)
{
    /* Read device ID to determine if we have a MAX14676D connected */
    if (pmic_read_reg(MAX14676_REG_DEVID, &PmicDeviceId) != PMIC_OK)
        return PMIC_ERROR;

    if (max14676_pwr_config() != PMIC_OK)
        return PMIC_ERROR;

    if (max14676_ldo_enable() != PMIC_OK)
        return PMIC_ERROR;

    if (max14676_config() != PMIC_OK)
        return PMIC_ERROR;

    return PMIC_OK;
}

/***************************************************************************//**
 * @brief
 *    De-initializes the pmic (max14676)
 *
 * @return
 *    None
 ******************************************************************************/
void pmic_deInit(void)
{
    return;
}
