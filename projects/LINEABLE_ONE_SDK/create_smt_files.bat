@echo off

for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
set ldt=%ldt:~0,4%%ldt:~4,2%%ldt:~6,2%_%ldt:~8,2%%ldt:~10,2%%ldt:~12,2%
echo Local date is [%ldt%]

if "%PATH_SILAB%"=="" (
  set CMDR=C:\SiliconLabs\SimplicityStudio\v4\developer\adapter_packs\commander\commander.exe
) else (
  set CMDR=%PATH_SILAB%\adapter_packs\commander\commander.exe
)

set PATH_S37=output_s37
set PATH_BOOT=LINEABLE_ONE_BOOTLOADER

set BOOTLOADER=LINEABLE_ONE_BOOTLOADER-combined.s37
set FIRMWARE=LINEABLE_ONE_SDK.s37
set MODEL=LINEABLE_ONE

cd %~dp0

:: delete old files
del "%PATH_S37%\*.s37"

if not exist %PATH_S37% (
    mkdir %PATH_S37%
)

:: copy current file
echo.
echo ********************************************************************
echo Copy %BOOTLOADER% and %FIRMWARE% files to %PATH_S37%
echo ********************************************************************
echo.
copy /Y "..\%PATH_BOOT%\GNU ARM v7.2.1 - Default\%BOOTLOADER%" %PATH_S37%
if errorlevel 1 (
    pause
    goto:eof
)
copy /Y ".\GNU ARM v7.2.1 - Debug\%FIRMWARE%" %PATH_S37%
if errorlevel 1 (
    pause
    goto:eof
)

:: create s37 files
echo.
echo ********************************************************************
echo Create %PATH_S37%\%FACTORY%_%ldt%.s37 for SMT
echo ********************************************************************
echo.
%CMDR% convert %PATH_S37%\%BOOTLOADER% %PATH_S37%\%FIRMWARE% -o %PATH_S37%\%MODEL%_%ldt%.s37

pause