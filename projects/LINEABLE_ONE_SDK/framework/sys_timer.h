/*
****************************************************************************************************
*                                               MODULE
****************************************************************************************************
*/

#ifndef __SYS_TIMER_H__
#define __SYS_TIMER_H__

/*
****************************************************************************************************
*                                                INCLUDE
****************************************************************************************************
*/

#include "bsp_timer.h"

/*
****************************************************************************************************
*                                            PUBLIC DEFINES
****************************************************************************************************
*/

typedef uint32_t sys_timer_t;

#define SYSTMR_SUCCESS               0x00
#define SYSTMR_FAIL                  0x01

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION PROTOTYPES
****************************************************************************************************
*/

void SYSTimer_Init(TimerEvent_t *obj, void (*callback)(void));
void SYSTimer_Start(TimerEvent_t *obj);
void SYSTimer_Stop(TimerEvent_t *obj);
void SYSTimer_Reset(TimerEvent_t *obj);
void SYSTimer_SetValue(TimerEvent_t *obj, uint32_t value);
sys_timer_t SYSTimer_GetCurrentTime(void);
sys_timer_t SYSTimer_GetElapsedTime(sys_timer_t savedTime);
sys_timer_t SYSTimer_GetFutureTime(sys_timer_t eventInFuture);

#endif  /* __SYS_TIMER_H__ */
