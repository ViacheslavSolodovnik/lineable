/*******************************************************************************
* INCLUDE FILES
*******************************************************************************/
/*---- system and platform files ---------------------------------------------*/
#include "rtos_app.h"
/*---- program files ---------------------------------------------------------*/

/*******************************************************************************
* EXTERNAL REFERENCES             NOTE: only use if not available in header file
*******************************************************************************/
/*---- function prototypes ---------------------------------------------------*/
/*---- data declarations -----------------------------------------------------*/

/*******************************************************************************
* PUBLIC DECLARATIONS             Defined here, used elsewhere
*******************************************************************************/
/*---- context ---------------------------------------------------------------*/
/*---- function prototypes ---------------------------------------------------*/
void SYSOS_DelayMs(uint32_t ms);
void SYSOS_DelayUs(uint32_t us);
void SYSOS_DumpData(const uint8_t *data, uint32_t size, char *title);
/*---- data declarations -----------------------------------------------------*/

/*******************************************************************************
* PRIVATE DECLARATIONS            Defined here, used elsewhere
*******************************************************************************/
/*---- context ---------------------------------------------------------------*/
/*---- function prototypes ---------------------------------------------------*/
/*---- data declarations -----------------------------------------------------*/

/*******************************************************************************
* PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

void SYSOS_DelayMs(uint32_t ms)
{
    RTOS_ERR os_err;
    OSTimeDly((OS_TICK   ) OS_MS_2_TICKS(ms),
              (OS_OPT    ) OS_OPT_TIME_DLY,
              (RTOS_ERR *)&os_err);
}

void SYSOS_DelayUs(uint32_t us)
{
    RTOS_ERR os_err;
    OSTimeDly((OS_TICK   ) OS_US_2_TICKS(us),
              (OS_OPT    ) OS_OPT_TIME_DLY,
              (RTOS_ERR *)&os_err);
}

void SYSOS_DumpData(const uint8_t *data, uint32_t size, char *title)
{
    int i = 0;

    debug_printf_data("\t\t  [%s] ", title);
    for (i = 0; i < size; i++) {
        debug_printf_data("%02x ", data[i]);
    }
    debug_printf_data("\n");
}

/*******************************************************************************
* PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

