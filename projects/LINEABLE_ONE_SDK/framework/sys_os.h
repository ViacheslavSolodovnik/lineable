#ifndef  _SYS_OS_H_
#define  _SYS_OS_H_

/***************************************************************************************************
* INCLUDE FILES
***************************************************************************************************/
/*---- system and platform files -----------------------------------------------------------------*/
/*---- program files -----------------------------------------------------------------------------*/

/***************************************************************************************************
* EXTERNAL REFERENCES             NOTE: only use if not available in header file
*******************************************************************************/
/*---- function prototypes -----------------------------------------------------------------------*/
/*---- data declarations -------------------------------------------------------------------------*/

/***************************************************************************************************
* PUBLIC DECLARATIONS             Defined here, used elsewhere
***************************************************************************************************/
/*---- context -----------------------------------------------------------------------------------*/
#define SYS_OS_OK           (0)
#define SYS_OS_ERR          (1)
#define OS_OPT_DEFAULT      (OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ANY + OS_OPT_PEND_FLAG_CONSUME)
#define OS_OPT_AND          (OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ALL + OS_OPT_PEND_FLAG_CONSUME)

static inline uint8_t SYSOS_CalcFFS(uint8_t n)
{
    uint8_t position = 0;
    uint8_t m = 0x1;

    while (!(n & m))
    {
        m = m << 1;
        position++;
    }
    return position;
}

/*---- function prototypes -----------------------------------------------------------------------*/
extern void SYSOS_DelayMs(uint32_t ms);
extern void SYSOS_DelayUs(uint32_t us);
extern void SYSOS_DumpData(const uint8_t *data, uint32_t size, char *title);
/*---- data declarations -------------------------------------------------------------------------*/

#endif /* _SYS_OS_H_ */
