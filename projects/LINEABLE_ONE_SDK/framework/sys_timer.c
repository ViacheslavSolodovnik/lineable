/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdio.h>
#include <math.h>
#include "rtos_app.h"

#include "em_device.h"
#include "em_common.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_core.h"
#include "em_timer.h"

#include "sys_timer.h"

/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/

#define SYSTIMER_TIMER0 0
#define SYSTIMER_TIMER1 1
#define SYSTIMER_TIMER2 2
#define SYSTIMER_TIMER3 3
#define SYSTIMER_TIMER4 4
#define SYSTIMER_TIMER5 5
#define SYSTIMER_TIMER6 6

#define SYSTIMER_WTIMER0 10
#define SYSTIMER_WTIMER1 11
#define SYSTIMER_WTIMER2 12
#define SYSTIMER_WTIMER3 13

#ifndef SYSTIMER_TIMER
#define SYSTIMER_TIMER SYSTIMER_WTIMER0
#endif

#if (SYSTIMER_TIMER == SYSTIMER_TIMER0) && (TIMER_COUNT >= 1)
#define SYSTIMER             TIMER0
#define SYSTIMER_CLK         cmuClock_TIMER0
#define SYSTIMER_IRQ         TIMER0_IRQn
#define SYSTIMER_IRQHandler  TIMER0_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_TIMER1) && (TIMER_COUNT >= 2)
#define SYSTIMER             TIMER1
#define SYSTIMER_CLK         cmuClock_TIMER1
#define SYSTIMER_IRQ         TIMER1_IRQn
#define SYSTIMER_IRQHandler  TIMER1_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_TIMER2) && (TIMER_COUNT >= 3)
#define SYSTIMER             TIMER2
#define SYSTIMER_CLK         cmuClock_TIMER2
#define SYSTIMER_IRQ         TIMER2_IRQn
#define SYSTIMER_IRQHandler  TIMER2_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_TIMER3) && (TIMER_COUNT >= 4)
#define SYSTIMER             TIMER3
#define SYSTIMER_CLK         cmuClock_TIMER3
#define SYSTIMER_IRQ         TIMER3_IRQn
#define SYSTIMER_IRQHandler  TIMER3_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_TIMER4) && (TIMER_COUNT >= 5)
#define SYSTIMER             TIMER4
#define SYSTIMER_CLK         cmuClock_TIMER4
#define SYSTIMER_IRQ         TIMER4_IRQn
#define SYSTIMER_IRQHandler  TIMER4_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_TIMER5) && (TIMER_COUNT >= 6)
#define SYSTIMER             TIMER5
#define SYSTIMER_CLK         cmuClock_TIMER5
#define SYSTIMER_IRQ         TIMER5_IRQn
#define SYSTIMER_IRQHandler  TIMER5_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_TIMER6) && (TIMER_COUNT >= 7)
#define SYSTIMER             TIMER6
#define SYSTIMER_CLK         cmuClock_TIMER6
#define SYSTIMER_IRQ         TIMER6_IRQn
#define SYSTIMER_IRQHandler  TIMER6_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_WTIMER0) && (WTIMER_COUNT >= 1)
#define SYSTIMER             WTIMER0
#define SYSTIMER_CLK         cmuClock_WTIMER0
#define SYSTIMER_IRQ         WTIMER0_IRQn
#define SYSTIMER_IRQHandler  WTIMER0_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_WTIMER1) && (WTIMER_COUNT >= 2)
#define SYSTIMER             WTIMER1
#define SYSTIMER_CLK         cmuClock_WTIMER1
#define SYSTIMER_IRQ         WTIMER1_IRQn
#define SYSTIMER_IRQHandler  WTIMER1_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_WTIMER2) && (WTIMER_COUNT >= 3)
#define SYSTIMER             WTIMER2
#define SYSTIMER_CLK         cmuClock_WTIMER2
#define SYSTIMER_IRQ         WTIMER2_IRQn
#define SYSTIMER_IRQHandler  WTIMER2_IRQHandler

#elif (SYSTIMER_TIMER == SYSTIMER_WTIMER3) && (WTIMER_COUNT >= 4)
#define SYSTIMER             WTIMER3
#define SYSTIMER_CLK         cmuClock_WTIMER3
#define SYSTIMER_IRQ         WTIMER3_IRQn
#define SYSTIMER_IRQHandler  WTIMER3_IRQHandler

#else
#error "Illegal USTIMER SYSTIMER selection"
#endif

// Use 16-bit TOP value for timer, independent of the width of the timer
#define SYSTIMER_MAX   0xFFFFFFFF

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

static bool hwTimerInitState = false;

/* This flag is used to make sure we have looped through the main several time to avoid race issues */
static volatile uint8_t hasLoopedThroughMain = 0;

/* Timers list head pointer */
static TimerEvent_t *timerListHead = DEF_NULL;

static uint32_t hwTimerFreq;

static sys_timer_t previousTime = 0;

/** Hold the cumulated error in micro-second to compensate the timing errors */
static int32_t TimeoutValueError = 0;

/** Hold the Wake-up time duration in ms */
static volatile uint32_t McuWakeUpTime = 0;

/** Flag used to indicates a the MCU has waken-up from an external IRQ */
static volatile bool NonScheduledWakeUp = false;

/**
 * Flag to indicate if the timestamps until the next event is long enough
 * to set the MCU into low power mode
 */
static bool RtcTimerEventAllowsLowPower = false;

/** Temperature compensation variables */
static int32_t tempCompAccumulator   = 0;

/** RTC variables. Used for converting RTC counter to system time */
//static uint32_t tmrOverflowCounter    = 0;
static uint32_t tmrOverflowInterval   = 0;

static uint32_t tmrTickDurationUs;
static uint32_t tmrTicksPerMs;

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

static void InsertNewHeadTimer(TimerEvent_t *obj, uint32_t remainingTime);
static void InsertTimer(TimerEvent_t *obj, uint32_t remainingTime);
static void SetTimeout(TimerEvent_t *obj);
static bool TimerExists(TimerEvent_t *obj);
static sys_timer_t GetValue(void);
static void IrqHandler(void);

static uint32_t HwTimerInit(void);
static void HwTimerSetTimeout(uint32_t timeout);
static sys_timer_t HwTimerGetAdjustedTimeoutValue(uint32_t timeout);
static sys_timer_t HwTimerGetTimerValue(void);
static sys_timer_t HwTimerGetElapsedAlarmTime(void);
static sys_timer_t HwTimerComputeFutureEventTime(sys_timer_t futureEventInTime);
static sys_timer_t HwTimerComputeElapsedTime(sys_timer_t eventInTime);

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/

void SYSTimer_Init(TimerEvent_t *obj, void (*callback)(void))
{
    obj->Timestamp = 0;
    obj->ReloadValue = 0;
    obj->IsRunning = false;
    obj->Callback = callback;
    obj->Next = DEF_NULL;

    if (hwTimerInitState == false) {
        HwTimerInit();
        hwTimerInitState = true;
    }
}

void SYSTimer_Start(TimerEvent_t *obj)
{
    uint32_t elapsedTime = 0;
    uint32_t remainingTime = 0;

    //BoardDisableIrq( );

    if ((obj == DEF_NULL) || (TimerExists(obj) == true)) {
        //BoardEnableIrq( );
        return;
    }

    obj->Timestamp = obj->ReloadValue;
    obj->IsRunning = false;

    if (timerListHead == DEF_NULL)
        InsertNewHeadTimer(obj, obj->Timestamp);
    else {
        if (timerListHead->IsRunning == true) {
            elapsedTime = GetValue();
            if (elapsedTime > timerListHead->Timestamp) {
                elapsedTime = timerListHead->Timestamp; // security but should never occur
            }
            remainingTime = timerListHead->Timestamp - elapsedTime;
        } else
            remainingTime = timerListHead->Timestamp;

        if (obj->Timestamp < remainingTime)
            InsertNewHeadTimer(obj, remainingTime);
        else
            InsertTimer(obj, remainingTime);
    }
    //BoardEnableIrq( );
}

void SYSTimer_Stop(TimerEvent_t *obj)
{
    //BoardDisableIrq();

    uint32_t elapsedTime = 0;
    uint32_t remainingTime = 0;

    TimerEvent_t *prev = timerListHead;
    TimerEvent_t *cur = timerListHead;

    // List is empty or the Obj to stop does not exist
    if ((timerListHead == DEF_NULL) || (obj == DEF_NULL)) {
        //BoardEnableIrq();
        return;
    }

    if (timerListHead == obj) { // Stop the Head
        if (timerListHead->IsRunning == true) { // The head is already running
            elapsedTime = GetValue();
            if (elapsedTime > obj->Timestamp)
                elapsedTime = obj->Timestamp;

            remainingTime = obj->Timestamp - elapsedTime;

            if (timerListHead->Next != DEF_NULL) {
                timerListHead->IsRunning = false;
                timerListHead = timerListHead->Next;
                timerListHead->Timestamp += remainingTime;
                timerListHead->IsRunning = true;
                SetTimeout(timerListHead);
            } else
                timerListHead = DEF_NULL;
        } else { // Stop the head before it is started
            if (timerListHead->Next != DEF_NULL) {
                remainingTime = obj->Timestamp;
                timerListHead = timerListHead->Next;
                timerListHead->Timestamp += remainingTime;
            } else
                timerListHead = DEF_NULL;
        }
    } else { // Stop an object within the list
        remainingTime = obj->Timestamp;

        while (cur != DEF_NULL) {
            if (cur == obj) {
                if (cur->Next != DEF_NULL) {
                    cur = cur->Next;
                    prev->Next = cur;
                    cur->Timestamp += remainingTime;
                } else {
                    cur = DEF_NULL;
                    prev->Next = cur;
                }
                break;
            } else {
                prev = cur;
                cur = cur->Next;
            }
        }
    }
    //BoardEnableIrq();
}

void SYSTimer_Reset(TimerEvent_t *obj)
{
    SYSTimer_Stop(obj);
    SYSTimer_Start(obj);
}

void SYSTimer_SetValue(TimerEvent_t *obj, uint32_t value)
{
    if (value < 10) {
        value = 10;         /* Do immediately */
    }

    SYSTimer_Stop(obj);
    obj->Timestamp = value;
    obj->ReloadValue = value;
}

sys_timer_t SYSTimer_GetCurrentTime(void)
{
    return HwTimerGetTimerValue();
}

sys_timer_t SYSTimer_GetElapsedTime(sys_timer_t savedTime)
{
    return HwTimerComputeElapsedTime(savedTime);
}

sys_timer_t SYSTimer_GetFutureTime(sys_timer_t eventInFuture)
{
    return HwTimerComputeFutureEventTime(eventInFuture);
}

void SYSTIMER_IRQHandler(void)
{
    uint32_t flags;

    flags = TIMER_IntGet(SYSTIMER);

    if (flags & TIMER_IF_CC0) {
        TIMER_IntClear(SYSTIMER, TIMER_IFC_CC0);
        IrqHandler();
    }
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

/*!
 * \brief Adds a timer to the list.
 *
 * \remark The list is automatically sorted. The list head always contains the
 *         next timer to expire.
 *
 * \param [IN]  obj Timer object to be added to the list
 * \param [IN]  remainingTime Remaining time of the running head after which the object may be added
 */
static void InsertTimer(TimerEvent_t *obj, uint32_t remainingTime)
{
    uint32_t aggregatedTimestamp = 0;      // hold the sum of timestamps
    uint32_t aggregatedTimestampNext = 0;  // hold the sum of timestamps up to the next event

    TimerEvent_t *prev = timerListHead;
    TimerEvent_t *cur = timerListHead->Next;

    if (cur == DEF_NULL) {
        // obj comes just after the head
        obj->Timestamp -= remainingTime;
        prev->Next = obj;
        obj->Next = DEF_NULL;
    } else {
        aggregatedTimestamp = remainingTime;
        aggregatedTimestampNext = remainingTime + cur->Timestamp;

        while (prev != DEF_NULL) {
            if (aggregatedTimestampNext > obj->Timestamp) {
                obj->Timestamp -= aggregatedTimestamp;
                if (cur != DEF_NULL)
                    cur->Timestamp -= obj->Timestamp;
                prev->Next = obj;
                obj->Next = cur;
                break;
            } else {
                prev = cur;
                cur = cur->Next;
                if (cur == DEF_NULL) {
                    // obj comes at the end of the list
                    aggregatedTimestamp = aggregatedTimestampNext;
                    obj->Timestamp -= aggregatedTimestamp;
                    prev->Next = obj;
                    obj->Next = DEF_NULL;
                    break;
                } else {
                    aggregatedTimestamp = aggregatedTimestampNext;
                    aggregatedTimestampNext = aggregatedTimestampNext + cur->Timestamp;
                }
            }
        }
    }
}

/*!
 * \brief Adds or replace the head timer of the list.
 *
 * \remark The list is automatically sorted. The list head always contains the
 *         next timer to expire.
 *
 * \param [IN]  obj Timer object to be become the new head
 * \param [IN]  remainingTime Remaining time of the previous head to be replaced
 */
static void InsertNewHeadTimer(TimerEvent_t *obj, uint32_t remainingTime)
{
    TimerEvent_t *cur = timerListHead;

    if (cur != DEF_NULL) {
        cur->Timestamp = remainingTime - obj->Timestamp;
        cur->IsRunning = false;
    }

    obj->Next = cur;
    obj->IsRunning = true;
    timerListHead = obj;
    SetTimeout(timerListHead);
}

/*!
 * \brief Check if the Object to be added is not already in the list
 *
 * \param [IN] timestamp Delay duration
 * \retval true (the object is already in the list) or false
 */
static bool TimerExists(TimerEvent_t *obj)
{
    TimerEvent_t *cur = timerListHead;

    while (cur != DEF_NULL) {
        if (cur == obj)
            return true;
        cur = cur->Next;
    }
    return false;
}

/*!
 * \brief Read the timer value of the currently running timer
 *
 * \retval value current timer value
 */
static sys_timer_t GetValue(void)
{
    return HwTimerGetElapsedAlarmTime();
}

/*!
 * \brief Sets a timeout with the duration "timestamp"
 *
 * \param [IN] timestamp Delay duration
 */
static void SetTimeout(TimerEvent_t *obj)
{
    hasLoopedThroughMain = 0;
    obj->Timestamp = HwTimerGetAdjustedTimeoutValue(obj->Timestamp);
    HwTimerSetTimeout(obj->Timestamp);
}

static void IrqHandler(void)
{
    uint32_t elapsedTime = 0;

    // Early out when timerListHead is null to prevent null pointer
    if (timerListHead == DEF_NULL)
        return;

    elapsedTime = GetValue();

    if (elapsedTime >= timerListHead->Timestamp)
        timerListHead->Timestamp = 0;
    else
        timerListHead->Timestamp -= elapsedTime;

    timerListHead->IsRunning = false;

    while ((timerListHead != DEF_NULL) && (timerListHead->Timestamp == 0)) {
        TimerEvent_t *elapsedTimer = timerListHead;
        timerListHead = timerListHead->Next;

        if (elapsedTimer->Callback != DEF_NULL)
            elapsedTimer->Callback();
    }

    // start the next timerListHead if it exists
    if (timerListHead != DEF_NULL) {
        if (timerListHead->IsRunning != true) {
            timerListHead->IsRunning = true;
            SetTimeout(timerListHead);
        }
    }
}

static uint32_t HwTimerInit(void)
{
    TIMER_Init_TypeDef timerInit     = TIMER_INIT_DEFAULT;
    TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;

    timerCCInit.mode = timerCCModeCompare;
    CMU_ClockEnable(SYSTIMER_CLK, true);
    TIMER_TopSet(SYSTIMER, SYSTIMER_MAX);
    TIMER_InitCC(SYSTIMER, 0, &timerCCInit);

    /* Run timer at slowest frequency that still gives less than 1 us per tick */
    timerInit.prescale = (TIMER_Prescale_TypeDef)_TIMER_CTRL_PRESC_DIV1;
    do {
        TIMER_Init(SYSTIMER, &timerInit);

        hwTimerFreq = CMU_ClockFreqGet(cmuClock_HFPER);
        hwTimerFreq /= 1 << timerInit.prescale;
        timerInit.prescale++;
    } while ((timerInit.prescale <= _TIMER_CTRL_PRESC_DIV1024) && (hwTimerFreq > 2000000));

    tmrTicksPerMs = (((uint64_t)hwTimerFreq * 1000) + 500000) / 1000000;
    tmrTickDurationUs = (uint32_t)((1000000000.0 / ((uint64_t)hwTimerFreq * 1000)) + 0.5);

    TIMER_IntDisable(SYSTIMER, TIMER_IEN_CC0);
    NVIC_ClearPendingIRQ(SYSTIMER_IRQ);
    NVIC_EnableIRQ(SYSTIMER_IRQ);

    return SYSTMR_SUCCESS;
}

static void HwTimerSetTimeout(uint32_t timeout)
{
    double timeoutValue, timeoutValueTemp, error;
    uint32_t value;

    TIMER_IntClear(SYSTIMER, TIMER_IFC_CC0);

    previousTime = RtcGetTimerValue();

    if (timeout < 1)
        timeout = 1;

    // timeoutValue is used for complete computation
    timeoutValue = round((double)timeout * tmrTicksPerMs);

    // timeoutValueTemp is used to compensate the cumulating errors in timing far in the future
    timeoutValueTemp = (double)timeout * tmrTicksPerMs;

    // Compute timeoutValue error
    error = timeoutValue - timeoutValueTemp;

    // Add new error value to the cumulated value in uS
    TimeoutValueError += (error * 1000);

    // Correct cumulated error if greater than ( RTC_ALARM_TICK_DURATION * 1000 )
    if (TimeoutValueError >= tmrTickDurationUs) {
        TimeoutValueError = TimeoutValueError - tmrTickDurationUs;
        timeoutValue = timeoutValue + 1;
    }

    // Rounding errors should not cause us to set the number behind the current time
    value = (previousTime * tmrTicksPerMs) + timeoutValue;
    if ((TIMER_CounterGet(SYSTIMER) > value) &&
        (value + tmrTicksPerMs > TIMER_CounterGet(SYSTIMER)))
        TIMER_CompareSet(SYSTIMER, 0, TIMER_CounterGet(SYSTIMER) + 1);
    else
        TIMER_CompareSet(SYSTIMER, 0, value);

    TIMER_IntEnable(SYSTIMER, TIMER_IEN_CC0);
}

static sys_timer_t HwTimerGetAdjustedTimeoutValue(uint32_t timeout)
{
    if (timeout > McuWakeUpTime) {
        /* we have waken up from a GPIO and we have lost "McuWakeUpTime"
         * that we need to compensate on next event
         */
        if (NonScheduledWakeUp == true) {
            NonScheduledWakeUp = false;
            timeout -= McuWakeUpTime;
        }
    }

    if (timeout > McuWakeUpTime)
        RtcTimerEventAllowsLowPower = true;

    return  timeout;
}

static sys_timer_t HwTimerGetTimerValue(void)
{
    sys_timer_t t = 0;

    // Add time based on number of counter overflows
    //  t += tmrOverflowCounter * tmrOverflowInterval;

    // Add remainder if the overflow interval is not an integer
    //  if ( rtcOverflowIntervalR != 0 )
    //  {
    //    t += (tmrOverflowCounter * rtcOverflowIntervalR) / tmrTicksPerMs;
    //  }

    /* Add the number of milliseconds for RTC */
    t += (TIMER_CounterGet(SYSTIMER) / tmrTicksPerMs);

    /* Add compensation for crystal temperature drift */
    t += tempCompAccumulator;

    return t;
}

static sys_timer_t HwTimerGetElapsedAlarmTime(void)
{
    sys_timer_t currentTime = HwTimerGetTimerValue();
    if (currentTime < previousTime)
        return (currentTime + (tmrOverflowInterval - previousTime));
    else
        return (currentTime - previousTime);
}

static sys_timer_t HwTimerComputeFutureEventTime(sys_timer_t futureEventInTime)
{
    return (HwTimerGetTimerValue() + futureEventInTime);
}

static sys_timer_t HwTimerComputeElapsedTime(sys_timer_t eventInTime)
{
    sys_timer_t elapsedTime = 0;

    /* Needed at boot, cannot compute with 0 or elapsed time will be equal to current time */
    if (eventInTime == 0)
        return 0;

    elapsedTime = HwTimerGetTimerValue();
    if (elapsedTime < eventInTime) {
        /* roll over of the counter */
        return (elapsedTime + (tmrOverflowInterval - eventInTime));
    } else
        return (elapsedTime - eventInTime);
}

void SYSHAL_Delay(uint32_t ms)
{
    uint32_t startTime = HwTimerGetTimerValue();
    while ((HwTimerGetTimerValue() - startTime) < ms); /* busy wait until at least ms
                                                           ticks have passed */
}
