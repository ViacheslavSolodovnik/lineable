/***************************************************************************//**
 * @file app_csen.c
 * @brief Helper functions for capacitive touch using CSEN
 * @version 5.5.0
 *******************************************************************************
 * # License
 * <b>Copyright 2017 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include "rtos_app.h"

#include <app_input.h>
#include <stdbool.h>
#include <string.h>
#include "em_cmu.h"
#include "em_csen.h"
#include "em_rtcc.h"

#include "gpiointerrupt.h"
#include "app_input.h"

#if configUSE_UI_TASK
#include "app_ui.h"
#endif


#if (configUSE_BUTTON > 0)

#define BTN_LONG_PRESS_TIME             3000
#define BTN_CHECK_INTERVAL              100
#define BTN_LONG_PRESS_COUNT            (BTN_LONG_PRESS_TIME/BTN_CHECK_INTERVAL)

static uint8_t btn_state = 0;
static volatile OS_TICK btn_start_tick = 0;
static OS_TMR  btn_tim;


#define BTN_DEBUG BTN_PRINTF
//#define BTN_DEBUG(...)


/* TO DO : need to update for filtering SOS correctly */
void app_button_isr_cb(uint8_t btn_bits)
{
    RTOS_ERR err;
    OS_TICK cur_tick = OSTimeGet(&err);

    if(btn_state != btn_bits){
        //filtering one more time
        if(btn_bits != 0 ){
            btn_start_tick = cur_tick;
        } else{
            //APP_TRACE_DEBUG("0x%x\r\n", btn_state);
            if(btn_start_tick != 0){
            	BTN_DEBUG("SHORT BTN 0x%x \r\n", btn_state);
#if configUSE_UI_TASK
                ui_send_msg( UI_MSG_TYPE_KEY,  (UI_KEY_SHORT|btn_state), 0);
#endif
                btn_start_tick = 0;
            }
        }
    }

    btn_state = btn_bits;
}

static void  btn_timer_cb (void  *p_tmr,
                           void  *p_arg)
{
    RTOS_ERR err;

    /* Called when timer expires:                            */
    /*   'p_tmr' is pointer to the user-allocated timer.     */
    /*   'p_arg' is argument passed when creating the timer. */
    if(btn_start_tick){
        OS_TICK  diff_tick = OSTimeGet(&err);
        if( diff_tick > btn_start_tick)
            diff_tick = diff_tick - btn_start_tick;
        else
            diff_tick = btn_start_tick - diff_tick;

        BTN_DEBUG("btn hold ticks %d\r\n", diff_tick );
        BTN_DEBUG("key value 0x%X\r\n", BSP_ReadButtons());
        if(diff_tick> OS_MS_2_TICKS(BTN_LONG_PRESS_TIME)){
            //BTN_DEBUG("LONG BTN\r\n");
            if((btn_state&(UI_KEY_LEFT|UI_KEY_RIGHT))==(UI_KEY_LEFT|UI_KEY_RIGHT)){
#if configUSE_UI_TASK
                ui_send_msg(UI_MSG_TYPE_KEY, UI_KEY_SOS, 0);
#endif
                BTN_DEBUG("SOS BTN\r\n");
                btn_start_tick = 0; //stop
            }
            else{//re-check
                btn_start_tick -= (BTN_CHECK_INTERVAL*2);
            }
        }

    }
}


void app_button_init(void)
{
    RTOS_ERR err;

    BSP_ButtonInit(app_button_isr_cb);

    //BSP_ButtonInit(DEF_NULL);
    OSTmrCreate(&btn_tim,
        "btn_timer",
        0, //OS_MS_2_TICKS(BTN_LONG_PRESS_TIME), /* initial delay */
        OS_TMR_MS_2_TICKS(BTN_CHECK_INTERVAL),
        OS_OPT_TMR_PERIODIC,
        btn_timer_cb,
        NULL,
        &err);

    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE),DEF_NULL);

    OSTmrStart(&btn_tim, &err);

    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE),DEF_NULL);
}

#endif

#if configUSE_TOUCH_CSENS
/* Use ACMP0 module for capsense */
#define ACMP_CAPSENSE                           ACMP0
#define ACMP_CAPSENSE_CMUCLOCK                  cmuClock_ACMP0
#define PRS_CH_CTRL_SOURCESEL_ACMP_CAPSENSE     PRS_CH_CTRL_SOURCESEL_ACMP0
#define PRS_CH_CTRL_SIGSEL_ACMPOUT_CAPSENSE     PRS_CH_CTRL_SIGSEL_ACMP0OUT

/* Touch buttons are connected to PC0, PC1, PC2 and PC3.
 *
 * Pin  | APORT Channel (for ACMP0)
 * -------------------------
 * PC0  | APORT1XCH0 or APORT2YCH0
 * PC1  | APORT1YCH1 or APORT2XCH1
 * PC2  | APORT1XCH2 or APORT2YCH2
 * PC3  | APORT1YCH3 or APORT2XCH3
 *
 */
#define CAPSENSE_CHANNELS       { acmpInputAPORT1XCH0, acmpInputAPORT2XCH1, acmpInputAPORT1XCH2, acmpInputAPORT2XCH3 }
#define BUTTON0_CHANNEL         0             /**< Button 0 channel */
#define BUTTON1_CHANNEL         3             /**< Button 1 channel */
#define ACMP_CHANNELS           4             /**< Number of channels in use for capsense */
#define NUM_SLIDER_CHANNELS     4             /**< The kit has a slider with 4 pads */


#define CSEN_INIT_TATAFACTORY                                         \
  {                                                                   \
    false,                      /* Charge pump low accuracy mode. */  \
    false,                      /* Use external kelvin connection. */ \
    false,                      /* Disable keep warm. */              \
    0,                          /* 0+3 cycle warm-up time. */         \
    0,                          /* Period counter reload. */          \
    csenPCPrescaleDiv1,         /* Period counter prescale. */        \
    csenPRSSELCh0,              /* PRS channel 0. */                  \
    csenInputSelDefault,        /* input0To7   -> aport1ch0to7 */     \
    csenInputSelDefault,        /* input8To15  -> aport1ch8to15 */    \
    csenInputSelDefault,        /* input16To23 -> aport1ch16to23 */   \
    csenInputSelDefault,        /* input24To31 -> aport1ch24to31 */   \
    csenInputSelDefault,        /* input32To39 -> aport3ch0to7 */     \
    csenInputSelDefault,        /* input40To47 -> aport3ch8to15 */    \
    csenInputSelDefault,        /* input48To55 -> aport3ch16to23 */   \
    csenInputSelDefault,        /* input56To63 -> aport3ch24to31 */   \
  }

static uint32_t millis(void);

static uint32_t pad_capacitance[NUM_OF_PAD] = { 0, 0 };
static uint32_t pad_mins[NUM_OF_PAD] = { 60000, 60000 };
static uint32_t pad_level[NUM_OF_PAD + 2] = PAD_LEVEL_THRS;
static uint8_t currentChannel = 0;
static int8_t max_pad = -1;
static uint32_t maxval = PAD_THRS;

static CSEN_Event_t currentEvent = CSEN_EVENT_DEFAULT;

static const uint8_t pad_channels[NUM_OF_PAD] = { 0, 1 };
/*
    |  PIN name  | PORT name |  PORT   | CH  |
    |------------|-----------|---------|-----|
    | UIF_TOUCH1 |  PC2      | APORT1X | CH2 |
    | UIF_TOUCH2 |  PC3      | APORT1Y | CH3 |
 */
#define CSEN_USE_CH2    2
#define CSEN_USE_CH3    3
static const CSEN_SingleSel_TypeDef sliderInpChannels[] = { csenSingleSelAPORT1XCH2, csenSingleSelAPORT1YCH3 };

/**************************************************************************//**
 * @brief  Setup CSEN, do initial calibration and start continuous scan
 *****************************************************************************/
void setupCSEN(void)
{
  CSEN_Init_TypeDef csenInit = CSEN_INIT_DEFAULT;
  CSEN_InitMode_TypeDef csenMeasureModeInit = CSEN_INITMODE_DEFAULT;
  uint8_t i;

  /* Use LFXO as LF clock source since we are already using it
     for the RTCC */
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);

  CMU_ClockEnable(cmuClock_CSEN_HF, true);
  CMU_ClockEnable(cmuClock_CSEN_LF, true);

  /* Setup timer to do 16 CSEN scans per second
     The input to the CSEN_LF clock is by default LFB/16 */
  csenInit.pcPrescale = csenPCPrescaleDiv16;
  csenInit.pcReload = 7;

  CSEN_Init(CSEN, &csenInit);

  NVIC_ClearPendingIRQ(CSEN_IRQn);
  NVIC_EnableIRQ(CSEN_IRQn);

  // Setup measurement mode to single conversion for calibration
  csenMeasureModeInit.sampleMode = csenSampleModeSingle;
  csenMeasureModeInit.trigSel = csenTrigSelStart;
  csenMeasureModeInit.sarRes = csenSARRes16;
  csenMeasureModeInit.gainSel = csenGainSel8X;                          /* Normal reference current when measuring single pads */

  // Do an initial reading of each pad to establish the zero-level
  for (i = 0; i < NUM_OF_PAD; i++) {
    csenMeasureModeInit.singleSel = sliderInpChannels[i];
    CSEN_InitMode(CSEN, &csenMeasureModeInit);
    CSEN_Enable(CSEN);
    CSEN_Start(CSEN);
    while (CSEN_IsBusy(CSEN)) ;
    // Subtract a margin from the reading to account for read noise
    pad_mins[i] = CSEN_DataGet(CSEN) - APP_CSEN_NOISE_MARGIN;
  }

  // Setup measurement mode to timer-triggered scan
  csenMeasureModeInit.inputMask0 = (1 << CSEN_USE_CH3) | (1 << CSEN_USE_CH2);
  csenMeasureModeInit.sampleMode = csenSampleModeScan;
  csenMeasureModeInit.trigSel = csenTrigSelTimer;

  CSEN_InitMode(CSEN, &csenMeasureModeInit);
  CSEN_Enable(CSEN);

  CSEN_IntEnable(CSEN, CSEN_IEN_CONV);

  // Start continuous scan cycle
  currentChannel = 0;
  CSEN_Start(CSEN);
}

/**************************************************************************//**
 * @brief  Calculate slider position
 *****************************************************************************/
int32_t csenCalcPos(void)
{
  int32_t tmp_pos = -1;

  if (max_pad != -1) {
    tmp_pos = (max_pad) << 6;

    // Avoid moving calculated position to the left when we are close to the right edge
    // and the measured capacitance at the rightmost pad is close to the noise floor
    tmp_pos -= (pad_level[max_pad] << 5) / pad_level[max_pad + 1];

    // Avoid moving calculated position to the right when we are close to the left edge
    // and the measured capacitance at the leftmost pad is close to the noise floor
    tmp_pos += (pad_level[max_pad + 2] << 5) / pad_level[max_pad + 1];
  }

  return tmp_pos;
}

/**************************************************************************//**
 * @brief  Get touch event data
 *****************************************************************************/
CSEN_Event_t csenGetEvent(void)
{
  return currentEvent;
}

/**************************************************************************//**
 * @brief  Check CSEN data after a scan is completed
 *****************************************************************************/
void csenCheckScannedData(void)
{
  uint8_t i;
  uint32_t tmp_max_val = PAD_THRS;

  CSEN_Event_t tmpEvent = currentEvent;

  max_pad = -1;

  for (i = 0; i < NUM_OF_PAD; i++) {
    // Order scan results and check if any pad has exceeded the threshold defining a touch event
    pad_level[i + 1] = ((pad_capacitance[i] - pad_mins[i]) << 16) / (65535 - pad_mins[i]);
    if (pad_level[i + 1] > tmp_max_val) {
      tmp_max_val = pad_level[i + 1];
      max_pad = i;
    } else if (pad_level[i + 1] < PAD_THRS) {
      pad_level[i + 1] = PAD_THRS;
    }
  }

  maxval = tmp_max_val;

  tmpEvent.sliderPos = csenCalcPos();

  if (tmpEvent.eventActive) {
    tmpEvent.eventDuration = millis() - tmpEvent.eventStart;
  } else if (max_pad != -1) {
    tmpEvent.eventActive = true;
    tmpEvent.eventStart = millis();
    tmpEvent.sliderStartPos = tmpEvent.sliderPos;
    tmpEvent.sliderTravel = 0;
  }

  if (max_pad == -1) {
    tmpEvent.eventActive = false;
  }

  if (tmpEvent.eventActive) {
    tmpEvent.touchForce = maxval;
    tmpEvent.sliderPrevPos = tmpEvent.sliderPos;
    tmpEvent.sliderTravel = tmpEvent.sliderPos - tmpEvent.sliderStartPos;
  }

  currentEvent = tmpEvent;
}

/**************************************************************************//**
 * @brief CSEN Interrupt handler
 *****************************************************************************/
void CSEN_IRQHandler(void)
{
  uint8_t pad_number;

  CSEN->IFC = _CSEN_IFC_MASK;

  pad_number = pad_channels[currentChannel];

  pad_capacitance[pad_number] = CSEN->DATA;

  currentChannel++;

  /* If a scan is completed, do some more checking of the data since we have
     time to spare before the next scan is scheduled to start */
  if (currentChannel > 1) {
    currentChannel = 0;
    csenCheckScannedData();
  }
}

static uint32_t millis(void)
{
  return (RTCC_CounterGet() * 1000) / 1024;
}


static CSEN_Event_t appCsenData = CSEN_EVENT_DEFAULT;
static int32_t touchHandle = 0;
static bool forceTriggered = false;
int32_t lastCsenSliderPos = -1;
static OS_TMR  csens_tim;
#define CSENS_CHECK_INTERVAL              100
void app_CSENS_cb(void  *p_tmr,
                    void  *p_arg)
{

    appCsenData = csenGetEvent();
    /* Touch event handing */
    if (appCsenData.eventActive) {
        touchHandle = appCsenData.sliderTravel;
        lastCsenSliderPos = appCsenData.sliderPos;
    } else {
        if (touchHandle < -(APP_CSEN_SLIDE_THRS)) {
            APP_TRACE_DEBUG("TOUCH:: ++\n");
        } else if (touchHandle > APP_CSEN_SLIDE_THRS) {
            APP_TRACE_DEBUG("TOUCH:: --\n");
        }

        if (lastCsenSliderPos != -1) {
            if (!forceTriggered && appCsenData.eventDuration < APP_CSEN_TAP_THRS) {
                APP_TRACE_DEBUG("TOUCH:: Tap\n");
            }
        }

        lastCsenSliderPos = -1;
        forceTriggered = false;
        touchHandle = 0;
    }
}

void app_CSEN_init(void)
{
    RTOS_ERR err;

    setupCSEN();

    OSTmrCreate(&csens_tim,
        "csen_timer",
        0, //OS_MS_2_TICKS(BTN_LONG_PRESS_TIME), /* initial delay */
        OS_TMR_MS_2_TICKS(CSENS_CHECK_INTERVAL),
        OS_OPT_TMR_PERIODIC,
        app_CSENS_cb,
        NULL,
        &err);

    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE),DEF_NULL);

    OSTmrStart(&csens_tim, &err);

    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE),DEF_NULL);
}

#endif
