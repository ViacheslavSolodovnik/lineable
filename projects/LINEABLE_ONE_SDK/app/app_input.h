/****************************************************************************************
// @file app_input.h
// @brief functions for input devices
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author:
//   File name :
//   Created :
//   Last Update :
****************************************************************************************/
#ifndef APP_INPUT_H
#define APP_INPUT_H

#include <stdbool.h>

#if 1// button part 

extern void app_button_init(void);

#endif

#if 1 // CSENS part
#define NUM_OF_PAD 2
#define PAD_THRS 22000
#define PAD_LEVEL_THRS { PAD_THRS, PAD_THRS, PAD_THRS, PAD_THRS }
#define APP_CSEN_NOISE_MARGIN 500
#define APP_CSEN_TAP_THRS     3500
#define APP_CSEN_SLIDE_THRS   60

typedef struct {
  int32_t sliderPos;
  int32_t sliderPrevPos;
  int32_t sliderStartPos;
  int32_t sliderTravel;
  uint32_t eventStart;
  uint32_t eventDuration;
  uint32_t touchForce;
  bool eventActive;
} CSEN_Event_t;

#define CSEN_EVENT_DEFAULT \
  {                        \
    -1,                    \
    -1,                    \
    -1,                    \
    0,                     \
    0,                     \
    0,                     \
    0,                     \
    false,                 \
  }

// Function prototypes
extern void setupCSEN(void);

extern int32_t csenCalcPos(void);

extern void csenCheckScannedData(void);

extern CSEN_Event_t csenGetEvent(void);
#endif

#endif /* APP_INPUT_H */
