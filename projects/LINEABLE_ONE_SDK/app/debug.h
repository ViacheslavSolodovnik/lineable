/****************************************************************************************
//
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author:
//   File name :
//   Created :
//   Last Update :
****************************************************************************************/
#ifndef  __DEBUG_H__
#define  __DEBUG_H__

#include <string.h>
#include "inc/terminal.h"
#include "userdata.h"

#define DBG_COLOR_BLACK   "\x1b[30m"               // blac[k]
#define DBG_COLOR_RED     "\x1b[31m"               // [r]ed
#define DBG_COLOR_GREEN   "\x1b[32m"               // [g]reen
#define DBG_COLOR_YELLOW  "\x1b[33m\x1b[40m"       // [y]ellow
#define DBG_COLOR_BLUE    "\x1b[34m"               // [b]lue
#define DBG_COLOR_RESET   "\x1b[0m"                // Reset  : Reset fg coloring

#define  __MODULE__         "NONE"
#define DBG_MODULE          0

#define LOG_FMT             "[%-10s] "
#define LORA_FMT            "[%s] "
#define LOG_ARGS(LOG_TAG)   LOG_TAG
#define LOG_CR              "\r "

#define PRINTFUNCTION(format, ...)  debug_printf(format, __VA_ARGS__)
#define PRINTDATAFUNC(format, ...)  debug_printf_data(format, __VA_ARGS__)

#if (APP_TRACE_LEVEL >= TRACE_LEVEL_INFO)
#define APP_TRACE_INFO(message, args...)   \
            if (DBG_MODULE) PRINTFUNCTION(LOG_FMT message, LOG_ARGS(__MODULE__), ## args)
#else
#define APP_TRACE_INFO(...)
#endif

#if (APP_TRACE_LEVEL >= TRACE_LEVEL_DBG)
#define APP_TRACE_DEBUG(message, args...)  \
            if (DBG_MODULE) PRINTFUNCTION(LOG_FMT message, LOG_ARGS(__MODULE__), ## args)

#define APP_TRACE_IF_DEBUG(condition, message, args...)     \
            if (DBG_MODULE)                                 \
                if (condition)                              \
                    PRINTFUNCTION(LOG_FMT message, LOG_ARGS(__MODULE__), ## args)
#else
#define APP_TRACE_DEBUG(...)
#define APP_TRACE_IF_DEBUG(...)
#endif

#define APP_TRACE_DATA(message, args...)    \
            if (DBG_MODULE) debug_printf_data(message, ## args)

#define APP_TRACE_LOG(message, args...)  \
            if (UNIT_SKT_OTB_TEST) PRINTFUNCTION(LOG_FMT message, LOG_ARGS(__MODULE__), ## args)

#define APP_TRACE_LORA(message, args...)  \
            if (UNIT_SKT_OTB_TEST) debug_printf_data(LOG_CR message, ## args)

#define APP_TRACE_LORA_M(message, args...)  \
            if (UNIT_SKT_OTB_TEST) debug_printf(LORA_FMT message, ## args)

#define APP_TRACE_STATE(message, args...) \
            if (UNIT_SKT_OTB_TEST) debug_printf_data(message, ## args)

/*
*********************************************************************************************************
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*********************************************************************************************************
*/
extern void  debugInit (void);
extern void  hexdump(uint8_t *name, void *mem, uint32_t len);

void debug_printf(const char *format, ...);
void debug_printf_data(const char *format, ...);

#endif //__DEBUG_H__
