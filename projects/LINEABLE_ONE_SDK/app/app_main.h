/*
****************************************************************************************************
*                                               MODULE
****************************************************************************************************
*/

#ifndef APP_MAIN_H_
#define APP_MAIN_H_

/*
****************************************************************************************************
*                                                INCLUDE
****************************************************************************************************
*/
#include <stdbool.h>
#include "lora_packet.h"

/*
****************************************************************************************************
*                                            PUBLIC DEFINES
****************************************************************************************************
*/

#define AM_SUCCESS                                              ((uint8_t) 0)
#define AM_FAIL                                                 ((uint8_t) 1)

/** define log data */
#define LOG_PACKET_LENGTH  (LORA_PACKET_LENGTH)
#define ONE_DAY_LOG_NUM    (6 * 24) //10 minute period
#define THREE_DAY_LOG_NUM  (ONE_DAY_LOG_NUM * 3)
#define FIVE_DAY_LOG_NUM   (ONE_DAY_LOG_NUM * 5)
#define MAXIMUM_LOG_NUM    ONE_DAY_LOG_NUM

typedef enum {
    MAIN_OP_MODE_NORMAL = 0,
    MAIN_OP_MODE_SOS,

    MAIN_OP_MODE_TEST,
    MAIN_OP_MODE_CERTI,

    MAIN_OP_MODE_MAX
} MainOpMode;

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION VARIABLES
****************************************************************************************************
*/
extern uint8_t LogData[LOG_PACKET_LENGTH * MAXIMUM_LOG_NUM];
extern uint16_t StackPointer, LOG_Space_IsFull;
extern uint8_t g_packet_buff[LORA_PACKET_MAX_LENGTH];

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION PROTOTYPES
****************************************************************************************************
*/

uint8_t MainApp_Init(void);
uint8_t MainApp_Start(void);
uint8_t MainApp_ChangeOpMode(MainOpMode op_mode);
uint8_t MainApp_GetOpMode(void);
uint8_t MainApp_ChangeMsgType(bool is_cfm);
uint8_t MainApp_DetectDoubleTap(void);
uint8_t MainApp_ChangeTestMode(bool run);
uint8_t MainApp_UpdateSysTime(uint32_t epoch);
uint8_t MainApp_DoTimeSync(void);

#endif

