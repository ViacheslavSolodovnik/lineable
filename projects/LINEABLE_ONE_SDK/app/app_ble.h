/*
 * app_ble.h
 *
 *  Created on: 2018. 7. 17.
 *      Author: Eliott
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef APP_APP_BLE_H_
#define APP_APP_BLE_H_

/** timer */
#define LOG_TX_TIMER              (0)
#define ADVERTISE_CHANGE_TIMER    (1)
#define PERIODIC_DATA_TX_TIMER    (2)
#define LOG_LENGTH_TX_TIMER       (3)
#define TIMER_START_100MSEC       (32768 / 10)
#define TIMER_START_500MSEC       (32768 / 2)
#define TIMER_START_1SEC          (32768)
#define TIMER_START_10SEC         (TIMER_START_1SEC * 10)
#define TIMER_START_30SEC         (TIMER_START_1SEC * 30)
#define TIMER_START_1MINUTE       (TIMER_START_1SEC * 60)
#define TIMER_START_10MINUTE      (TIMER_START_1MINUTE * 10)
#define TIMER_STOP                (0)

/* Exported macro ------------------------------------------------------------*/
/* Exported Types ------------------------------------------------------------*/
SL_PACK_START(1)
typedef struct {
    uint8_t  HeaderData[13];        /* eddystone header data */
    uint8_t  ES_UID_NameSpace[10];  /* eddystone namespace */
    uint8_t  ES_UID_ID[6];          /* eddystone instance id */
    uint8_t  Reserved[2];           /* reserved */
} SL_ATTRIBUTE_PACKED ADVERTISE_PACKET;
SL_PACK_END()

/* Exported Constants --------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void SetupBleAdvertise(uint8_t adv_period_type);
extern void StartBleAdvertise(void);
extern void GetCurrentTime(uint32_t *epoch_ts);
extern void ble_task_start(OS_PRIO lora_priority);
/* Exported variables ------------------------------------------------------- */
extern uint8_t BLE_APP_IsConnected;

#endif /* APP_APP_BLE_H_ */
