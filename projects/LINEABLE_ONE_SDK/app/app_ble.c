/****************************************************************************************
//
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author:
//   File name :
//   Created :
//   Last Update :
****************************************************************************************/
#include "rtos_app.h"

#include <stdio.h>
#include <stdlib.h>
#include "em_cmu.h"
#include "sleep.h"

#include "rtos_bluetooth.h"
#include "gatt_db.h"

#include "rail.h"
#include "rail_types.h"
#include "rail_config.h"

#include "app_cfg.h"
#include "app_ble.h"
#include "app_main.h"

#include <drivers/fuelgauge/fuelgauge.h>
#include <drivers/pmic/pmic.h>

#include "userdata.h"
#include "assistNow.h"
#include "gps.h"


#undef  __MODULE__
#define __MODULE__ "BLE"
#undef  DBG_MODULE
#define DBG_MODULE 0

#define APPLE_I_BEACON		((uint32_t)0x4C0002)

//eddystone packet
#define NO_SCAN_RESPONSE	(0)
#define APP_ES_UID_NAMESPACE                0xEB, 0xB8, 0x1F, 0xD0, \
                                            0x48, 0x3E, 0x9F, 0xA3, \
                                            0x0B, 0x3E                // first 10 value from SHA1 hash of FQN(lineable.net)

#define APP_ES_UID_ID                       0x01, 0x02, 0x03, 0x04, \
                                            0x05, 0xC4                  //!< Mock values for 6-byte Eddystone UID ID instance.
#define GAP_DATA_TYPE_UUID16_SVC_DATA       0x16
#define GAP_DATA_TYPE_UUID16_LIST           0x03

ADVERTISE_PACKET DEFAULT_ADV_PACKET={
    .HeaderData = {
    		/* Flag bits - See Bluetooth 4.0 Core Specification , Volume 3, Appendix C, 18.1 for more details on flags. */
    		2,  /* length  */
    		0x01, /* type */
    		0x04 | 0x02, /* Flags: LE General Discoverable Mode, BR/EDR is disabled. */    //add three lines(android galaxy7s,8s issue solution) 20181005
    	                                                                                   //standard ble flags
    	    0x03,                      //length
    	    GAP_DATA_TYPE_UUID16_LIST, //Complete list of 16-bit Service UUIDs.
    	    0xAA, 0xFE,                //16-bit Eddystone UUID
    	    0x17,
    	    GAP_DATA_TYPE_UUID16_SVC_DATA,
    	    0xAA, 0xFE,               // 16-bit Eddystone UUID
    	    0x00,    //!< UID frame type (fixed at 0x00).,
    	    0xD0,    // 0M -48dbm (2's complement)},
    },
    .ES_UID_NameSpace = {APP_ES_UID_NAMESPACE},
    .ES_UID_ID = {APP_ES_UID_ID},
	.Reserved = {0x00, 0x00},
};

//humax i-beacon UUID :  5f26d24a-c62b-11e7-8c86-48a7ca11d2e8
const uint8 serviceUUID[16] =   {0x5f, 0x26, 0xd2, 0x4a, 0xc6, 0x2b, 0x11, 0xe7, 0x8c, 0x86, 0x48, 0xa7, 0xca, 0x11, 0xd2, 0xe8};

/** ble command */
#define BLE_PACKET_LENGTH            (22)
#define LOG_DATA_CHAR_REQUEST_CMD    (0xF0)
#define LOG_DATA_CHAR_COMPLETE_CMD   (0x0F)
#define LOG_DATA_CHAR_CANCEL_CMD     (0xFF)
/** log */
#define PRE_DATA_TRANSMIT        (0)
#define LATELY_DATA_TRANSMIT     (1)

uint8_t data_array[BLE_PACKET_LENGTH]={0};
uint8_t BLE_APP_IsConnected = 0;
uint8_t BLE_LogTx_IsBlocked = 0;
uint16_t Pre_LogData_SP = 0, Lately_LogData_SP = 0;

#if (config_BLE_CENTRAL_MODE | config_BLE_DUAL_MODE)
static int ProcessScanResponse(struct gecko_msg_le_gap_scan_response_evt_t *pResp)
{
    // decoding advertising packets is done here. The list of AD types can be found
    // at: https://www.bluetooth.com/specifications/assigned-numbers/Generic-Access-Profile
    int i = 0;
    int ad_match_found = 0;
    static int ad_len;
    static int ad_type;
    uint32_t manufac_id_n_subtype;

    char name[32];

    while (i < (pResp->data.len - 1))
    {

        ad_len  = pResp->data.data[i];
        ad_type = pResp->data.data[i+1];

        if (ad_type == 0x08 || ad_type == 0x09 )
        {
            // type 0x08 = Shortened Local Name
            // type 0x09 = Complete Local Name
            memcpy(name, &(pResp->data.data[i+2]), ad_len-1);
            name[ad_len-1] = 0;
//            APP_TRACE_DEBUG(name);
//            APP_TRACE_DEBUG("\r\n");
        }

        if (ad_type == 0x06 || ad_type == 0x07)
        {
        	// type 0x06 = Incomplete List of 128-bit Service Class UUIDs
        	// type 0x07 = Complete List of 128-bit Service Class UUIDs
        }

        if (ad_type == 0xFF)	//custom manufacturer packet
        {
        	manufac_id_n_subtype = ((uint32_t)pResp->data.data[i+2] << 16) | ((uint32_t)pResp->data.data[i+3] << 8) | (uint32_t)pResp->data.data[i+4];
            if( manufac_id_n_subtype == APPLE_I_BEACON )
            {
            	if(memcmp(serviceUUID, &(pResp->data.data[i+6]),16) == 0)
            	{
            	  	ad_match_found = 1;
            	}
            }
        }

        //jump to next AD record
        i = i + ad_len + 1;
    }

    return(ad_match_found);
}
#endif

#define BTID_LENGTH  (6)
#define ADVERTISE_INTERVAL_MIN_20MS      (32)
#define ADVERTISE_INTERVAL_MAX_30MS      (48)
#define ADVERTISE_INTERVAL_MIN_100MS     (160)
#define ADVERTISE_INTERVAL_MAX_100MS     (160)
#define ADVERTISE_INTERVAL_MIN_1SEC      (ADVERTISE_INTERVAL_MIN_100MS * 10)
#define ADVERTISE_INTERVAL_MAX_1P2SEC    (ADVERTISE_INTERVAL_MIN_100MS * 12)
#define ADVERTISE_CHANNEL_ALL            (7)
#define FAST_CONNECTION_ADV_TYPE         (0)
#define REDUCED_POWER_ADV_TIME           (1)

void SetupBleAdvertise(uint8_t adv_period_type)    //0:fast connection(20,30ms), 1:reduced power(1,1.2sec)
{
	gecko_cmd_system_set_tx_power(0);
    //set advertise mode to save power consumption
	if( adv_period_type == FAST_CONNECTION_ADV_TYPE){
	    gecko_cmd_le_gap_set_adv_parameters(ADVERTISE_INTERVAL_MIN_20MS ,ADVERTISE_INTERVAL_MAX_30MS, ADVERTISE_CHANNEL_ALL);
	    APP_TRACE_DEBUG("Start fast connection advertise mode\n");
	    //mode change timer start
	    gecko_cmd_hardware_set_soft_timer(TIMER_START_30SEC, ADVERTISE_CHANGE_TIMER, 0);	//start
	}else if( adv_period_type == REDUCED_POWER_ADV_TIME){
		gecko_cmd_le_gap_set_adv_parameters(ADVERTISE_INTERVAL_MIN_1SEC ,ADVERTISE_INTERVAL_MAX_1P2SEC, ADVERTISE_CHANNEL_ALL);
		APP_TRACE_DEBUG("Start reduced power advertise mode\n");
	}
    /** read eddystone uid ID saved in userdata*/
	memcpy((uint8_t *)&DEFAULT_ADV_PACKET.ES_UID_ID, UNIT_BTID, BTID_LENGTH);

	gecko_cmd_le_gap_set_adv_data(NO_SCAN_RESPONSE, sizeof(ADVERTISE_PACKET), (uint8_t *)&DEFAULT_ADV_PACKET);
}

void StartBleAdvertise(void)
{
    /* Start advertising in user mode and enable connections */
    gecko_cmd_le_gap_start_advertising(0, le_gap_user_data, le_gap_connectable_scannable);
}

void SynchronizeTime(uint32_t epoch_ts)
{
    CLK_DATE_TIME         date_time;
    CPU_CHAR              date_time_str[30];

	Clk_TS_UnixToDateTime(epoch_ts, 0, &date_time);
	Clk_SetDateTime(&date_time);

	Clk_DateTimeToStr(&date_time, CLK_STR_FMT_YYYY_MM_DD_HH_MM_SS_UTC, date_time_str, 30);
	APP_TRACE_DEBUG("Set Time\r\n");
	APP_TRACE_DEBUG("\t %10s\r\n", date_time_str);
}

void GetCurrentTime(uint32_t *epoch_ts)
{
    CLK_DATE_TIME         date_time;
    CLK_TS_SEC            unix_sec;
    CPU_CHAR              date_time_str[30];

	Clk_GetDateTime(&date_time);
	Clk_DateTimeToTS_Unix(&unix_sec, &date_time);
	*epoch_ts = unix_sec;

	Clk_DateTimeToStr(&date_time, CLK_STR_FMT_YYYY_MM_DD_HH_MM_SS_UTC, date_time_str, 30);
	APP_TRACE_DEBUG("- Time :\n");
	APP_TRACE_DEBUG("\t %s\r\n", date_time_str);
}

static uint8_t boot_to_dfu = 0;
uint32_t epoch_value = 0;
uint8_t sp = PRE_DATA_TRANSMIT;
uint16_t TotalPacketLength;
uint8_t LogLengthArray[2];
void BluetoothEventHandler(struct gecko_cmd_packet* evt)
{
    switch (BGLIB_MSG_ID(evt->header)) {
        // This boot event is generated when the system boots up after reset.
        // Do not call any stack commands before receiving the boot event.
        // Here the system is set to start advertising immediately after boot
        // procedure.
        case gecko_evt_system_boot_id:
#if config_BLE_OFF_MODE
            __NOP();
#elif config_BLE_PERIPHERAL_MODE
    	    SetupBleAdvertise(FAST_CONNECTION_ADV_TYPE);
    	    StartBleAdvertise();
#elif config_BLE_CENTRAL_MODE
            gecko_cmd_le_gap_discover(le_gap_discover_observation);
#elif config_BLE_DUAL_MODE
    	    SetupBleAdvertise(FAST_CONNECTION_ADV_TYPE);
    	    StartBleAdvertise();
    	    gecko_cmd_le_gap_discover(le_gap_discover_observation);
#endif
            break;
        case gecko_evt_le_connection_opened_id:
        	BLE_APP_IsConnected = 1;
        	gecko_cmd_hardware_set_soft_timer(TIMER_STOP, ADVERTISE_CHANGE_TIMER, 0);	//stop
        	printf("Connected\r\n");
            break;

        case gecko_evt_le_connection_closed_id:
        	BLE_APP_IsConnected = 0;
            //to stop transmiting log data
            BLE_LogTx_IsBlocked = 0;
    	    gecko_cmd_hardware_set_soft_timer(TIMER_STOP, LOG_TX_TIMER, 0);	           //stop
            gecko_cmd_hardware_set_soft_timer(TIMER_STOP, LOG_LENGTH_TX_TIMER, 0);	   //stop
        	printf("Disconnected\r\n");

           // Check if need to boot to dfu mode.
#if configUSE_OTA
            if (boot_to_dfu) {
                // Enter to DFU OTA mode.
                gecko_cmd_system_reset(2);
            }
            else
#endif
            {
#if config_BLE_OFF_MODE
                __NOP();
#elif config_BLE_PERIPHERAL_MODE
    	        SetupBleAdvertise(FAST_CONNECTION_ADV_TYPE);
    	        StartBleAdvertise();
#elif config_BLE_CENTRAL_MODE
                gecko_cmd_le_gap_discover(le_gap_discover_observation);
#elif config_BLE_DUAL_MODE
    	        SetupBleAdvertise(FAST_CONNECTION_ADV_TYPE);
    	        StartBleAdvertise();
    	        gecko_cmd_le_gap_discover(le_gap_discover_observation);
#endif
            }
            break;

        /* This event indicates that a remote GATT client is attempting to write a value of an
         * attribute in to the local GATT database, where the attribute was defined in the GATT
         * XML firmware configuration file to have type="user".  */
        // Events related to OTA upgrading
        // Check if the user-type OTA Control Characteristic was written.
        // If ota_control was written, boot the device into Device Firmware
        // Upgrade (DFU) mode.
        case gecko_evt_gatt_server_user_write_request_id:
#if configUSE_OTA
            if (bluetooth_evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
                // Set flag to enter to OTA mode.
                boot_to_dfu = 1;
                // Send response to Write Request.
                gecko_cmd_gatt_server_send_user_write_response(bluetooth_evt->data.evt_gatt_server_user_write_request.connection,
                                                                                              gattdb_ota_control, bg_err_success);
                // Close connection to enter to DFU OTA mode.
                gecko_cmd_le_connection_close(bluetooth_evt->data.evt_gatt_server_user_write_request.connection);
            }
#endif
            /* characteristic write(user type) */
            if (bluetooth_evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_time_syncronization)
            {
                // Send response to Write Request.
                gecko_cmd_gatt_server_send_user_write_response(bluetooth_evt->data.evt_gatt_server_user_write_request.connection,
                        gattdb_time_syncronization, bg_err_success);

                epoch_value = (evt->data.evt_gatt_server_user_write_request.value.data[3] << 24) |    //little endian
                		      (evt->data.evt_gatt_server_user_write_request.value.data[2] << 16) |
							  (evt->data.evt_gatt_server_user_write_request.value.data[1] <<  8) |
							  (evt->data.evt_gatt_server_user_write_request.value.data[0]);

                MainApp_UpdateSysTime(epoch_value);
                APP_TRACE_DEBUG("write value(user type) : %x %x %x %x\r\n", evt->data.evt_gatt_server_user_write_request.value.data[0],
                                                                            evt->data.evt_gatt_server_user_write_request.value.data[1],
                                                                            evt->data.evt_gatt_server_user_write_request.value.data[2],
                                                                            evt->data.evt_gatt_server_user_write_request.value.data[3]);
                APP_TRACE_DEBUG("epoch value : %d\n", epoch_value);
            }

            // GPS assistNow Offline Assistance
            if (bluetooth_evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_assistNow_offline_assistance)
            {
                // Send response to Write Request.
                gecko_cmd_gatt_server_send_user_write_response(bluetooth_evt->data.evt_gatt_server_user_write_request.connection,
                        gattdb_assistNow_offline_assistance, bg_err_success);

                //APP_TRACE_DEBUG("AssistNow Offline Data entry received: \n");

                if (evt->data.evt_gatt_server_user_write_request.value.data[0] == 0xDE &&
                    evt->data.evt_gatt_server_user_write_request.value.data[1] == 0xAD &&
                    evt->data.evt_gatt_server_user_write_request.value.data[2] == 0xBE &&
                    evt->data.evt_gatt_server_user_write_request.value.data[3] == 0xEF)
                {
                  APP_TRACE_DEBUG("Erase AssistNow Offline Data: \n");
                  AssistNow_eraseMgaData();
                }
                else
                {
                  AssistNow_saveMga2Flash(&evt->data.evt_gatt_server_user_write_request.value.data[0], 168);
                }
            }

            // Initial GPS Location
            if (bluetooth_evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_gps_location)
            {
                // Send response to Write Request.
                gecko_cmd_gatt_server_send_user_write_response(bluetooth_evt->data.evt_gatt_server_user_write_request.connection,
                        gattdb_assistNow_offline_assistance, bg_err_success);

                APP_TRACE_DEBUG("GPS Location received: \n");

                double lat = 0;
                double lon = 0;
                float alt = 0;
                memcpy(&lat, &(evt->data.evt_gatt_server_user_write_request.value.data[0]), sizeof(double));
                memcpy(&lon, &(evt->data.evt_gatt_server_user_write_request.value.data[8]), sizeof(double));
                memcpy(&alt, &(evt->data.evt_gatt_server_user_write_request.value.data[16]), sizeof(float));
                /*
               for(int i=0;i<4;i++)
               {
            	   APP_TRACE_DEBUG("%x %x %x %x %x\n",
                                evt->data.evt_gatt_server_user_write_request.value.data[(i*5)],
   	   	   	  	   	   	        evt->data.evt_gatt_server_user_write_request.value.data[(i*5)+1],
      													evt->data.evt_gatt_server_user_write_request.value.data[(i*5)+2],
      													evt->data.evt_gatt_server_user_write_request.value.data[(i*5)+3],
      													evt->data.evt_gatt_server_user_write_request.value.data[(i*5)+4]);
               }
               */
              APP_TRACE_DEBUG("Location: lat= %lf; lon= %lf; alt= %lf \n", lat, lon,alt);

              LocationUpdateParams_t gps;
              gps.valid = true;
              gps.lat = lat;
              gps.lon = lon;
              gps.altitude = alt;
              gps.utcTime = 0;

              gps_setLastPosition(gps);
            }

            if (bluetooth_evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_log_data) {

                // Send response to Write Request.
                gecko_cmd_gatt_server_send_user_write_response(bluetooth_evt->data.evt_gatt_server_user_write_request.connection,
                                                               gattdb_log_data, bg_err_success);

                if( evt->data.evt_gatt_server_user_write_request.value.data[0] == LOG_DATA_CHAR_REQUEST_CMD ) {

                	if( !BLE_LogTx_IsBlocked )
                	{
                		BLE_LogTx_IsBlocked = 1;

                	    if( !LOG_Space_IsFull ){
                		    Pre_LogData_SP = 0;
                		    TotalPacketLength = StackPointer * LOG_PACKET_LENGTH;
                	    }else{
                		    Pre_LogData_SP = StackPointer;
                		    TotalPacketLength = MAXIMUM_LOG_NUM * LOG_PACKET_LENGTH;
                	    }

                	    /** transmit total packet length */
                	    LogLengthArray[0] = (uint8_t)(TotalPacketLength & 0x00FF);           //masking low byte
                	    LogLengthArray[1] = (uint8_t)((TotalPacketLength & 0xFF00) >> 8);    //masking high byte

    	                gecko_cmd_hardware_set_soft_timer(TIMER_START_1SEC, LOG_LENGTH_TX_TIMER, 0);	//start
                	}else{
                		APP_TRACE_DEBUG("block log tx request command\n");
                	}
                }else if( evt->data.evt_gatt_server_user_write_request.value.data[0] == LOG_DATA_CHAR_COMPLETE_CMD ){

                	if( !BLE_LogTx_IsBlocked )
                	{
                	    /** clear saved log data & log counter */
                	    memset(LogData, 0, sizeof(LogData));
                	    StackPointer = 0;     //init stack pointer
                        LOG_Space_IsFull = 0; //init log space flag
                	    APP_TRACE_DEBUG("erase log data\n");
                	}else{
                		APP_TRACE_DEBUG("block log complete command\n");
                	}
                }else if( evt->data.evt_gatt_server_user_write_request.value.data[0] == LOG_DATA_CHAR_CANCEL_CMD ){
                	/** cancel log data tx */
                	BLE_LogTx_IsBlocked = 0;
                	gecko_cmd_hardware_set_soft_timer(TIMER_STOP, LOG_TX_TIMER, 0);	//stop
                	APP_TRACE_DEBUG("- cancel log data tx\n");
                }
            }
            break;

        /* characteristic write(hex type) */
        case gecko_evt_gatt_server_attribute_value_id:
#if 0
        	APP_TRACE_DEBUG("write value(hex type) : %x %x %x %x\r\n", evt->data.evt_gatt_server_attribute_value.value.data[0],
        			evt->data.evt_gatt_server_attribute_value.value.data[1],
					evt->data.evt_gatt_server_attribute_value.value.data[2],
					evt->data.evt_gatt_server_attribute_value.value.data[3]);
#endif
        	break;

        /* This event indicates that a remote GATT client is attempting to read a value of an
        *  attribute from the local GATT database, where the attribute was defined in the GATT
        *  XML firmware configuration file to have type="user". */
        case gecko_evt_gatt_server_user_read_request_id:
            if(evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_device_data_read)
    	    {
            	gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection, gattdb_device_data_read, 0,
    		    		                                                                                            BLE_PACKET_LENGTH, data_array);
            }

    	    if(evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_log_data)
    	    {
    	        gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
    	        		                                      gattdb_log_data, 0, BLE_PACKET_LENGTH, data_array);
    	    }

    	    if(evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_battery_level)
    	    {
    	    	uint8_t bat_level;
    	        uint16_t battery_val;
    	        FUELGAUGE_GetSoc(&battery_val);
    	        bat_level = (uint8_t)battery_val;
    	    	gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
    	    			                                      gattdb_battery_level, 0, 1, &bat_level);
    	    	APP_TRACE_DEBUG("battery level : %d\n", bat_level);
    	    }

    	    if(evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_system_id)
    	    {
    	    	gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
    	    			                                      gattdb_system_id, 0, sizeof(DEFAULT_ADV_PACKET.ES_UID_ID), (uint8_t *)&DEFAULT_ADV_PACKET.ES_UID_ID);
    	    }

            break;

        /* This event indicates either that a local Client Characteristic Configuration descriptor
         * has been changed by the remote GATT client, or that a confirmation from the remote GATT
         * client was received upon a successful reception of the indication. */
        case gecko_evt_gatt_server_characteristic_status_id:
//        	if(evt->data.evt_gatt_server_characteristic_status.characteristic == gattdb_device_data_read){    //0x2:indicate enable, 0x0:indicate disable
//        		APP_TRACE_DEBUG("data read characteristic indicate status : %d\n", evt->data.evt_gatt_server_characteristic_status.client_config_flags);
//        	}
            break;

        case gecko_evt_le_gap_scan_request_id:
            break;

        case gecko_evt_le_gap_scan_response_id:
#if (config_BLE_CENTRAL_MODE | config_BLE_DUAL_MODE)
    	    // this function returns 1 if we found the service we are looking for
    	    if(ProcessScanResponse(&(evt->data.evt_le_gap_scan_response)) > 0)
    	    {
    	    	APP_TRACE_DEBUG("found our i-beacon\r\n");

    	       // match found -> stop discovery and try to connect
    	       //gecko_cmd_le_gap_end_procedure();
    	    }
#endif
            break;

        case gecko_evt_test_dtm_completed_id:
        	printf("number of packets : %d\r\n", evt->data.evt_test_dtm_completed.number_of_packets);
        	evt->data.evt_test_dtm_completed.number_of_packets = 0;
        	break;

        /* Software Timer event */
        case gecko_evt_hardware_soft_timer_id:
    	    /* Check which software timer handle is in question */
    	    switch (evt->data.evt_hardware_soft_timer.handle)
    	    {
    	        case LOG_TX_TIMER:
    	        	gecko_cmd_hardware_set_soft_timer(TIMER_START_1SEC, LOG_TX_TIMER, 0);	//start
    	        	/** transmit saved log data */
    	        	if(!LOG_Space_IsFull){
    	        		/** Log data is not full(static buffer) */
    	        		if( Pre_LogData_SP < StackPointer ){
    	        		    gecko_cmd_gatt_server_send_characteristic_notification(0xff, gattdb_log_data, LOG_PACKET_LENGTH,
    	        		    	        		    	        	               &LogData[Pre_LogData_SP++ * LOG_PACKET_LENGTH]);
    	        		    APP_TRACE_DEBUG("static log sequence byte : %x\n", LogData[(Pre_LogData_SP - 1) * LOG_PACKET_LENGTH + 3]);
    	        		}
                        if( Pre_LogData_SP == StackPointer ){
    	        			Pre_LogData_SP = 0;
    	        			BLE_LogTx_IsBlocked = 0;
    	        			APP_TRACE_DEBUG("- Total log number : %d\n", StackPointer);
    	        			gecko_cmd_hardware_set_soft_timer(TIMER_STOP, LOG_TX_TIMER, 0);	//stop
    	        		}
    	        	}else{
    	        		switch(sp)
    	        		{
    	        		    case PRE_DATA_TRANSMIT:
    	        		    	gecko_cmd_gatt_server_send_characteristic_notification(0xff, gattdb_log_data, LOG_PACKET_LENGTH,
    	        		    	   	        		    	        	               &LogData[Pre_LogData_SP++ * LOG_PACKET_LENGTH]);
    	        		    	APP_TRACE_DEBUG("pre log sequence byte : %x\n", LogData[(Pre_LogData_SP - 1) * LOG_PACKET_LENGTH + 3]);
    	        		    	if(Pre_LogData_SP == MAXIMUM_LOG_NUM){
    	        		    		Lately_LogData_SP = 0;
    	        		    		sp = LATELY_DATA_TRANSMIT;
    	        		    	}
    	        		    	break;
    	        		    case LATELY_DATA_TRANSMIT:
    	        		    	gecko_cmd_gatt_server_send_characteristic_notification(0xff, gattdb_log_data, LOG_PACKET_LENGTH,
    	        		    	   	        		    	        	               &LogData[Lately_LogData_SP++ * LOG_PACKET_LENGTH]);
    	        		    	APP_TRACE_DEBUG("lately log sequence byte : %x\n", LogData[(Lately_LogData_SP - 1) * LOG_PACKET_LENGTH + 3]);
    	        		    	if(Lately_LogData_SP == StackPointer){
    	        		    		sp = PRE_DATA_TRANSMIT;
    	        		    		BLE_LogTx_IsBlocked = 0;
    	        		    		APP_TRACE_DEBUG("- Total log number : %d\n", MAXIMUM_LOG_NUM);
    	        		    		gecko_cmd_hardware_set_soft_timer(TIMER_STOP, LOG_TX_TIMER, 0);	//stop
    	        		    	}
    	        		    	break;
    	        		    default:
    	        		    	break;
    	        		}
    	        	}
    	            break;

    	        case ADVERTISE_CHANGE_TIMER:
    	        	gecko_cmd_le_gap_stop_advertising(0);   //stop advertising before setting again
    	        	SetupBleAdvertise(REDUCED_POWER_ADV_TIME);
    	        	StartBleAdvertise();
    	        	gecko_cmd_hardware_set_soft_timer(TIMER_STOP, ADVERTISE_CHANGE_TIMER, 0);	//stop
    	        	break;

    	        case PERIODIC_DATA_TX_TIMER:
    	        	gecko_cmd_gatt_server_send_characteristic_notification(
    	        			0xff, gattdb_device_data_read, LORA_PACKET_LENGTH, g_packet_buff);

    	        	APP_TRACE_DEBUG("send periodic data by bluetooth\n");

    	        	gecko_cmd_hardware_set_soft_timer(TIMER_STOP, PERIODIC_DATA_TX_TIMER, 0);	//stop
    	        	break;

                case LOG_LENGTH_TX_TIMER:
    	        	gecko_cmd_gatt_server_send_characteristic_notification(0xff, gattdb_log_data, sizeof(LogLengthArray),
                	                				                                               LogLengthArray);
                	APP_TRACE_DEBUG("log total byte : %x %x\n", LogLengthArray[0], LogLengthArray[1]);

    	        	gecko_cmd_hardware_set_soft_timer(TIMER_STOP, LOG_LENGTH_TX_TIMER, 0);	//stop
                    gecko_cmd_hardware_set_soft_timer(TIMER_START_1SEC, LOG_TX_TIMER, 0);	//start
    	        	break;

                default:
    	            break;
    	    }
    	    break;

        default:
            break;
    }
}

// Bluetooth stack
#define MAX_BLE_CONNECTIONS 1

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_BLE_CONNECTIONS)];
/* Configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t bluetooth_config =
{
    .config_flags = GECKO_CONFIG_FLAG_RTOS,
    .sleep.flags = SLEEP_FLAGS_DEEP_SLEEP_ENABLE,
    .bluetooth.max_connections = MAX_BLE_CONNECTIONS,
    .bluetooth.heap = bluetooth_stack_heap,
    .bluetooth.heap_size = sizeof(bluetooth_stack_heap),
    .bluetooth.sleep_clock_accuracy = 100, // ppm,  0  = default (250 ppm)
    /*************
    * The maximum number of advertisement sets must be defined.
    * These sets can be used to start multiple advertisers. Each context allocates
    * ~60 bytes of RAM.
    ***************/
    //.bluetooth.max_advertisers = 5, //!< Maximum number of advertisers to support, if 0 defaults to 1
    .gattdb = &bg_gattdb_data,
#if configUSE_OTA
    .ota.flags = 0,
    .ota.device_name_len = 3,
    .ota.device_name_ptr = "OTA",
#endif
    .scheduler_callback = BluetoothLLCallback,
    .stack_schedule_callback = BluetoothUpdate,
#if (HAL_PA_ENABLE) && defined(FEATURE_PA_HIGH_POWER)
    .pa.config_enable = 1, // Enable high power PA
    .pa.input = GECKO_RADIO_PA_INPUT_VBAT, // Configure PA input to VBAT
#endif // (HAL_PA_ENABLE) && defined(FEATURE_PA_HIGH_POWER)
    .mbedtls.flags = GECKO_MBEDTLS_FLAGS_NO_MBEDTLS_DEVICE_INIT,
    .mbedtls.dev_number = 0,
};

#ifdef  RTOS_MODULE_COMMON_SHELL_AVAIL

static  CPU_INT16S  StartAdvertise (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
	SetupBleAdvertise(FAST_CONNECTION_ADV_TYPE);
	StartBleAdvertise();
    printf("start advertise\r\n");

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  EndAdvertise (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
	gecko_cmd_hardware_set_soft_timer(TIMER_STOP, ADVERTISE_CHANGE_TIMER, 0);
    gecko_cmd_le_gap_stop_advertising(0);
    printf("end advertise\r\n");

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  StartDirectTestModeTx (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    int dtm_packet_type;
    int dtm_packet_length;
    int dtm_channel;
    int dtm_phy;

    if(argc != 5){
        printf("please check input. command [packet type] [length] [ch] [phy]\r\n");
        printf("ex) startdtmtx 0 37 0 1\r\n");
        return SHELL_EXEC_ERR;
    }

    dtm_packet_type = atoi(argv[1]);
    if( ((7<dtm_packet_type) && (dtm_packet_type<253)) || (dtm_packet_type==255) ){
        printf("invalid packet type. range(0~7,253,254)\r\n");
        return SHELL_EXEC_ERR;
    }else{
        switch(dtm_packet_type){
            case 0:
                printf("packet type : PRBS9\r\n");
                break;
            case 1:
                printf("packet type : 11110000(0xF0)\r\n");
                break;
            case 2:
                printf("packet type : 10101010(0xAA)\r\n");
                break;
            case 3:
                printf("packet type : unmodulated carrier-deprecated\r\n");
                break;
            case 4:
                printf("packet type : 11111111(0xFF)\r\n");
                break;
            case 5:
                printf("packet type : 00000000(0x00)\r\n");
                break;
            case 6:
                printf("packet type : 00001111(0x0F)\r\n");
                break;
            case 7:
                printf("packet type : 01010101(0x55)\r\n");
                break;
            case 253:
                printf("packet type : pn9 continuously modulated output\r\n");
                break;
            case 254:
                printf("packet type : unmodulated carrier\r\n");
                break;
            default:
                break;
        }
    }

    dtm_packet_length = atoi(argv[2]);
    if( (0>dtm_packet_length) || (dtm_packet_length>255) ){
        printf("invalid length. range(0~255)\r\n");
        return SHELL_EXEC_ERR;
    }else{
        printf("length : %d\r\n",dtm_packet_length);
    }

    dtm_channel = atoi(argv[3]);
    if( (0>dtm_channel) || (dtm_channel>39) ){
        printf("invalid channel. range(0~39)\r\n");
        return SHELL_EXEC_ERR;
    }else{
        printf("channel : %d\r\n",dtm_channel);
    }


    dtm_phy = atoi(argv[4]);
    if( (1>dtm_phy) || (dtm_phy>4) ){
        printf("invalid phy. range(1~4)\r\n");
        return SHELL_EXEC_ERR;
    }else{
        switch(dtm_phy){
            case 1:
                printf("phy : 1M\r\n");
                break;
            case 2:
                printf("phy : 2M\r\n");
                break;
            case 3:
                printf("phy : 125k coded\r\n");
                break;
            case 4:
                printf("phy : 500k coded\r\n");
                break;
            default:
                break;
        }
    }

    gecko_cmd_test_dtm_tx((uint8_t)dtm_packet_type ,(uint8_t)dtm_packet_length ,(uint8_t)dtm_channel ,(uint8_t)dtm_phy);

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  StartDirectTestModeRx (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    int dtm_channel;
    int dtm_phy;

    if(argc != 3){
        printf("please check input. command [ch] [phy]\r\n");
        printf("ex) startdtmrx 0 1\r\n");
        return SHELL_EXEC_ERR;
    }

    dtm_channel = atoi(argv[1]);
    if( (0>dtm_channel) || (dtm_channel>39) ){
        printf("invalid channel. range(0~39)\r\n");
        return SHELL_EXEC_ERR;
    }else{
        printf("channel : %d\r\n",dtm_channel);
    }


    dtm_phy = atoi(argv[2]);
    if( (1>dtm_phy) || (dtm_phy>4) ){
        printf("invalid phy. range(1~4)\r\n");
        return SHELL_EXEC_ERR;
    }else{
        switch(dtm_phy){
            case 1:
                printf("phy : 1M\r\n");
                break;
            case 2:
                printf("phy : 2M\r\n");
                break;
            case 3:
                printf("phy : 125k coded\r\n");
                break;
            case 4:
                printf("phy : 500k coded\r\n");
                break;
            default:
                break;
        }
    }

    gecko_cmd_test_dtm_rx((uint8_t)dtm_channel ,(uint8_t)dtm_phy);

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  EndDirectTestMode (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    gecko_cmd_test_dtm_end();
    printf("end direct test mode\r\n");

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  StartScan (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    gecko_cmd_le_gap_discover(le_gap_discover_observation);
    printf("start scan\r\n");

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  EndScan (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    gecko_cmd_le_gap_end_procedure();
    printf("end scan\r\n");

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  GetBleMacAddress (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    struct gecko_msg_system_get_bt_address_rsp_t *pResp;

    pResp = gecko_cmd_system_get_bt_address();

    printf("ble mac address : 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\r\n", pResp->address.addr[5],
											  	                 pResp->address.addr[4],
											  	                 pResp->address.addr[3],
												                 pResp->address.addr[2],
												                 pResp->address.addr[1],
												                 pResp->address.addr[0]);

    return (SHELL_EXEC_ERR_NONE);
}

SHELL_CMD  BleDebug_CmdTbl []={
 // BLE Test Command
    {"startadv",  StartAdvertise},
    {"endadv",    EndAdvertise},
    {"startscan", StartScan},
    {"endscan",   EndScan},
	{"startdtmtx",StartDirectTestModeTx},
	{"startdtmrx",StartDirectTestModeRx},
	{"enddtm",    EndDirectTestMode},
	{"getaddr",   GetBleMacAddress},
    { 0,          0}
};

#endif //RTOS_MODULE_COMMON_SHELL_AVAIL

/**************************************************************************//**
 * Bluetooth Application task.
 *
 * @param p_arg Pointer to an optional data area which can pass parameters to
 *              the task when the task executes.
 *
 * This is a minimal Bluetooth Application task that starts advertising after
 * boot and supports OTA upgrade feature.
 *****************************************************************************/
// Bluetooth Application task
static CPU_STK bluetoothAppTaskStk[BLUETOOTH_APP_TASK_STK_SIZE];
static OS_TCB  bluetoothAppTaskTCB;
void bluetoothAppTask(void *p_arg)
{
    PP_UNUSED_PARAM(p_arg);
    RTOS_ERR err;

    // Initialise Bluetooth stack.
    gecko_init(&bluetooth_config);// gecko_init_multiprotocol(NULL);

    // Create Bluetooth Link Layer and stack tasks
    bluetooth_start_task(APP_CFG_TASK_BLUETOOTH_LL_PRIO,
                       APP_CFG_TASK_BLUETOOTH_STACK_PRIO);


    while (DEF_TRUE) {
    OSFlagPend(&bluetooth_event_flags,
               (OS_FLAGS)BLUETOOTH_EVENT_FLAG_EVT_WAITING,
               (OS_TICK)0,
               OS_OPT_PEND_BLOCKING       \
               + OS_OPT_PEND_FLAG_SET_ANY \
               + OS_OPT_PEND_FLAG_CONSUME,
               NULL,
               &err);

    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);

    BluetoothEventHandler((struct gecko_cmd_packet*)bluetooth_evt);


    OSFlagPost(&bluetooth_event_flags,
               (OS_FLAGS)BLUETOOTH_EVENT_FLAG_EVT_HANDLED,
               OS_OPT_POST_FLAG_SET,
               &err);

    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);
  }
}

void ble_task_start(OS_PRIO lora_priority)
{
    RTOS_ERR err;

    // Create the Bluetooth Application task
    OSTaskCreate(&bluetoothAppTaskTCB,
                 "Bluetooth App Task",
                 bluetoothAppTask,
                 0u,
                 lora_priority,
                 &bluetoothAppTaskStk[0u],
                 (BLUETOOTH_APP_TASK_STK_SIZE / 10u),
                 BLUETOOTH_APP_TASK_STK_SIZE,
                 0u,
                 0u,
                 0u,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);


#ifdef  RTOS_MODULE_COMMON_SHELL_AVAIL
    Shell_CmdTblAdd((CPU_CHAR *)"BleDebug", BleDebug_CmdTbl, &err);
#endif

}
