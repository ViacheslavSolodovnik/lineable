/****************************************************************************************
//
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author:
//   File name :
//   Created :
//   Last Update :
****************************************************************************************/
#include "rtos_app.h"

#include <stdio.h>
#include <strings.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>
#include <rtos_description.h>
#include <common/include/shell.h>
#include <common/include/lib_math.h>

#include "device.h"
#include "debug.h"
#include "pti.h"

#include "rtos_bluetooth.h"
#include "app_ble.h"
#include  <drivers/peripheral/i2c/micrium_i2c.h>
#include "gpiointerrupt.h"
#include "sys_os.h"

#if (configUSE_LORA_TEST > 0)
#include "lora_test.h"
#endif

#include "app_main.h"

#include <drivers/pmic/pmic.h>
#include <drivers/fuelgauge/fuelgauge.h>

// Temp for factory test
#if (configUSE_ACC_GYRO_SENSOR > 0)
#include "lsm6ds3_acc_gyro.h"
#endif

#if (configUSE_HRM > 0)
#include "si117xhrm.h"
#include "sihrmUser.h"
#include "app_hrm.h"
#endif

#include <drivers/radio/sx1276/sx1276.h>
#include "gps.h"

#if (configUSE_GET_BOOTINFO == 1)
#include "api/btl_interface.h"
#include "api/btl_interface_storage.h"
#endif

/*
*********************************************************************************************************
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*********************************************************************************************************
*                                           LOCAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/
#ifdef  RTOS_MODULE_COMMON_SHELL_AVAIL
extern CPU_BOOLEAN Terminal_Init(void);

#if (configUSE_MOTER > 0)
static CPU_INT16S motor_test(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
    static uint8_t motor_flag = 0;

    motor_flag ^= 0x01;

    if (motor_flag)
    {
        printf("motor turn on");
        GPIO_PinModeSet(BSP_MOTOR_EN_PORT, BSP_MOTOR_EN_PIN, gpioModePushPull, 1);
    }
    else
    {
        printf("motor turn off");
        GPIO_PinModeSet(BSP_MOTOR_EN_PORT, BSP_MOTOR_EN_PIN, gpioModePushPull, 0);
    }

    return (SHELL_EXEC_ERR_NONE);
}
#endif
#if (configUSE_LORA_TEST > 0)
// 12. Set LoRa TX
static  CPU_INT16S  set_lora_tx(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("Set LoRa Tx\r\n");

    int tx_id;
    int ch_id;
    int dr;
    int tx_power;
    int modulation = 0;

    if (argc != 6)
    {
        printf("please check input.. command [id] [ch] [datarate] [txpower] [modulation]\r\n");
        printf(" ex) loratx 1 30 12 14 0 \r\n");

        return SHELL_EXEC_ERR;
    }

    tx_id = (int)atoi(argv[1]);
    if (tx_id > 999 || tx_id < 1)
    {
        printf("Invalid TX ID.. TX ID (1 ~ 999)\r\n");
        return SHELL_EXEC_ERR;
    }

    ch_id = (int)atoi(argv[2]);

#if defined( REGION_KR920 )
    if (ch_id > 32 || ch_id < 25)
    {
        printf("Invalid Channel ID..\r\n");
        return SHELL_EXEC_ERR;
    }
#elif defined( REGION_AS923 )
    if (ch_id > 38 || ch_id < 30)
    {
        printf("Invalid Channel ID..\r\n");
        return SHELL_EXEC_ERR;
    }
#endif

    dr = (int)atoi(argv[3]);
    if (dr > 12 || dr < 7)
    {
        printf("Invalid Data Rate.. Data rate (7 ~ 12)\r\n");
        return SHELL_EXEC_ERR;
    }

    tx_power = (int)atoi(argv[4]);
    if (tx_power > 14)
    {
        return SHELL_EXEC_ERR;
    }

    if (argc == 6)
    {
        modulation = (int)atoi(argv[5]);
    }

    return LoRaTx(tx_id, ch_id, dr, tx_power, modulation);
}

// 12a. Set LoRa RX
static  CPU_INT16S  set_lora_rx(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("Set LoRa Rx\r\n");

    int ch_id;
    int dr;

    if (argc != 2)
    {
        printf("please check input.. command [ch] [datarate]\r\n");
        printf(" ex) 12a 1 12\r\n");
        return SHELL_EXEC_ERR;
    }

    ch_id = (int)atoi(argv[1]);
    if (ch_id > 8 || ch_id < 1)
    {
        printf("Invalid Channel ID.. Channel ID (1 ~ 8)\r\n");
        return SHELL_EXEC_ERR;
    }

    dr = (int)atoi(argv[3]);
    if (dr > 12 || dr < 7)
    {
        printf("Invalid Data Rate.. Data rate (7 ~ 12)\r\n");
        return SHELL_EXEC_ERR;
    }

    return LoRaRx(ch_id, dr);
}

// 12b. Set LoRa CW
static  CPU_INT16S  set_lora_cw(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("Set LoRa CW\r\n");

    return LoraCW();
}

// 12c. Set LoRa stop
static  CPU_INT16S  set_lora_stop(CPU_INT16U        argc,
                                  CPU_CHAR         *argv[],
                                  SHELL_OUT_FNCT    out_fnct,
                                  SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("Set LoRa stop\r\n");

    return LoRaStop();
}

static  CPU_INT16S  set_lora_tx_lbt(CPU_INT16U        argc,
                                    CPU_CHAR         *argv[],
                                    SHELL_OUT_FNCT    out_fnct,
                                    SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("Set LoRa Tx LBT\r\n");
    RTOS_ERR err;
    int modem, ch_id;

    if (argc != 3)
    {
        printf("please check input.. \r\n");
        printf(" ex) loratxlbt ch modem \r\n");

        return SHELL_EXEC_ERR;
    }

    ch_id = (int)atoi(argv[1]);

#if defined( REGION_KR920 )
    if (ch_id > 32 || ch_id < 25)
    {
        printf("Invalid Channel ID..\r\n");
        return SHELL_EXEC_ERR;
    }
#elif defined( REGION_AS923 )
    if (ch_id > 38 || ch_id < 30)
    {
        printf("Invalid Channel ID..\r\n");
        return SHELL_EXEC_ERR;
    }
#endif

    modem = (int)atoi(argv[2]);
    if (modem == 0 || modem == 1)
    {
        for (int i = 0; i < 1000; i++)
        {
            LoRaTx(1, ch_id, 12, 10, modem);
            OSTimeDly(OS_MS_2_TICKS(10000), OS_OPT_TIME_DLY, &err);    //60sec.
        }
    }
    else
    {
        return SHELL_EXEC_ERR;
    }
    return (SHELL_EXEC_ERR_NONE);
}

#endif

static CPU_INT16S i2c_dbg_w(CPU_INT16U        argc,
                            CPU_CHAR         *p_argv[],
                            SHELL_OUT_FNCT    out_fnct,
                            SHELL_CMD_PARAM  *p_cmd_param);

static CPU_INT16S i2c_dbg_r(CPU_INT16U        argc,
                            CPU_CHAR         *p_argv[],
                            SHELL_OUT_FNCT    out_fnct,
                            SHELL_CMD_PARAM  *p_cmd_param);


static CPU_INT16S system_reset(CPU_INT16U        argc,
                               CPU_CHAR         *argv[],
                               SHELL_OUT_FNCT    out_fnct,
                               SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t flag = (uint8_t)SYSTEM_RESET_NORMAL;

    debug_printf("SYSTEM RESET\r\n");

    if (argc == 2)
    {
        if (strcasecmp(argv[1], "user_data") == 0)
        {
            flag = (uint8_t)SYSTEM_RESET_WITH_USERDATA;
        }
    }

    DeviceSystemReset(flag, 0);

    return (SHELL_EXEC_ERR_NONE);
}

#if (configUSE_PMIC > 0)
static CPU_INT16S pmic_get_vcell(CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    uint16_t vcell_val;

    FUELGAUGE_GetVcell(&vcell_val);
    printf("VCELL value(percent) %d\r\n", vcell_val);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S pmic_get_soc(CPU_INT16U        argc,
                               CPU_CHAR         *argv[],
                               SHELL_OUT_FNCT    out_fnct,
                               SHELL_CMD_PARAM  *p_cmd_param)
{
    uint16_t soc_val;

    FUELGAUGE_GetSoc(&soc_val);
    printf("SOC value(percent) %d\r\n", soc_val);

    return (SHELL_EXEC_ERR_NONE);
}
#endif

#if (configUSE_PMIC == PMIC_DEVICE_TPS65720)
static CPU_INT16S pmic_read_charge_state(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ch_state;

    PMIC_ReadCharState(&ch_state);
    printf("Charge State : 0x%02x\r\n", ch_state);

    return (SHELL_EXEC_ERR_NONE);
}
#endif

#if (configUSE_HRM > 0)
static CPU_INT16S hrm_check_ver(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    int8_t hrmVersion[32];

    if (si117xhrm_QuerySoftwareRevision(hrmVersion) != 0)
    {
        APP_TRACE_DEBUG("Read for HRM algorithm version failed!\r\n");
        return (SHELL_EXEC_ERR);
    }
    APP_TRACE_DEBUG("HRM algorithm version : %s\r\n", hrmVersion);

    return (SHELL_EXEC_ERR_NONE);
}
#endif


#if (LIB_MEM_CFG_DBG_INFO_EN == DEF_ENABLED && \
     LIB_MEM_CFG_HEAP_SIZE > 0 && \
    (APP_TRACE_LEVEL >= TRACE_LEVEL_INFO))
static  CPU_INT16S  mem_info(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
    RTOS_ERR err;
    Mem_OutputUsage((void *)printf,  &err);
    return (SHELL_EXEC_ERR_NONE);
}
#else
static  CPU_INT16S  mem_info(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("NOT WORKING\r\n");
    return (SHELL_EXEC_ERR_NONE);
}

#endif

#if (configUSE_GET_BOOTINFO==1)

static  CPU_INT16S   bootloader_info(CPU_INT16U        argc,
                                     CPU_CHAR         *argv[],
                                     SHELL_OUT_FNCT    out_fnct,
                                     SHELL_CMD_PARAM  *p_cmd_param)
{
    BootloaderInformation_t bootInfo;
    BootloaderStorageInformation_t storageinfo;
    bootloader_getInfo(&bootInfo);

    if (bootInfo.type == NO_BOOTLOADER)
    {
        printf("\nNO BOOTLOADER\n");
        return (SHELL_EXEC_ERR_NONE);
    }
    else if (bootInfo.type == SL_BOOTLOADER)
    {
        printf("\nSiLab Boot Version : %ld.%ld.%ld\n",
               (bootInfo.version >> 24) & 0xFF, (bootInfo.version >> 16) & 0xFF, (bootInfo.version >> 8) & 0xFF);

        printf("Capability : 0x%lX\n", bootInfo.capabilities);
        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_ENFORCE_UPGRADE_SIGNATURE)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_ENFORCE_UPGRADE_SIGNATURE));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_ENFORCE_UPGRADE_ENCRYPTION)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_ENFORCE_UPGRADE_ENCRYPTION));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_ENFORCE_SECURE_BOOT)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_ENFORCE_SECURE_BOOT));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_BOOTLOADER_UPGRADE)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_BOOTLOADER_UPGRADE));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_EBL)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_EBL));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_EBL_SIGNATURE)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_EBL_SIGNATURE));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_EBL_ENCRYPTION)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_EBL_ENCRYPTION));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_STORAGE)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_STORAGE));
        }

        if (bootInfo.capabilities & BOOTLOADER_CAPABILITY_COMMUNICATION)
        {
            printf("%s\n", STRINGIZE(BOOTLOADER_CAPABILITY_COMMUNICATION));
        }
    }
    else
    {
        printf("\nBOOTLOADER ERR\n");
    }

    bootloader_getStorageInfo(&storageinfo);

    printf("\nStorage Info\nVersion : 0x%lX\n Capability : 0x%lX\nNum Slot : %ld\n", \
           storageinfo.version, storageinfo.capabilities, storageinfo.numStorageSlots);
    printf("Type(%d) : %s\n", (int)storageinfo.storageType, (storageinfo.storageType == SPIFLASH) ? STRINGIZE(SPIFLASH) : \
           (storageinfo.storageType == INTERNAL_FLASH) ? STRINGIZE(INTERNAL_FLASH) : \
           STRINGIZE(CUSTOM_STORAGE));

    printf("Implementation Information(%s)\nCapability : 0x%X\nPage Size : %ld Bytes\nTotal Size : %ld Bytes\nWord Size : %d\n", \
           storageinfo.info->partDescription, storageinfo.info->capabilitiesMask, storageinfo.info->pageSize, storageinfo.info->partSize, storageinfo.info->wordSizeBytes);

    if (storageinfo.numStorageSlots > 0)
    {
        BootloaderStorageSlot_t slotInfo;
        uint32_t slot_id = 0;
        do
        {
            bootloader_getStorageSlotInfo(slot_id,   &slotInfo);
            printf("Start Address : 0x%lX, Size 0x%lX(%ld Bytes)\n", slotInfo.address, slotInfo.length, slotInfo.length);
        }
        while (++slot_id < storageinfo.numStorageSlots);
    }

    return (SHELL_EXEC_ERR_NONE);
}
#endif

static  CPU_INT16S  set_lora_lna(CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("LNA set\r\n");
    //test by eliott
    SX1276Write(REG_RXCONFIG, RF_RXCONFIG_AGCAUTO_OFF | RF_RXCONFIG_RXTRIGER_PREAMBLEDETECT);
    SX1276Write(REG_LNA, RFLR_LNA_GAIN_G1 | RFLR_LNA_BOOST_HF_ON);
    return (SHELL_EXEC_ERR_NONE);
}


static  CPU_INT16S  Check6axisSensor(CPU_INT16U        argc,
                                     CPU_CHAR         *argv[],
                                     SHELL_OUT_FNCT    out_fnct,
                                     SHELL_CMD_PARAM  *p_cmd_param)
{
    printf("%s : %s\n", "LSM6DS3", (LSM6DS3_Prove() == MOTION_DEVICE_OK) ? "OK" : "ERROR");
    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  CheckSx1276(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ver;
    ver = SX1276Read(REG_VERSION);
    printf("%s : %s\n", "LORA", (ver == 0x12) ? "OK" : "ERROR");
    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  CheckUbloxM8(CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    gnssPowerOn();
    SYSOS_DelayMs(TIME_SEC(10));
    printf("%s : %s\n", "GPS", (gnssProve() == true) ? "OK" : "ERROR");
    gnssPowerOff();
    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  CheckLed(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 2)
    {
        if (strcasecmp(argv[1], "R") == 0)
        {
            PMICLED_SetColor(RED);
        }
        else if (strcasecmp(argv[1], "G") == 0)
        {
            PMICLED_SetColor(GREEN);
        }
        else if (strcasecmp(argv[1], "B") == 0)
        {
            PMICLED_SetColor(BLUE);
        }
        else if (strcasecmp(argv[1], "A") == 0)
        {
            PMICLED_SetColor(AMBER);
        }
        else if (strcasecmp(argv[1], "M") == 0)
        {
            PMICLED_SetColor(MAGENTA);
        }
        else if (strcasecmp(argv[1], "C") == 0)
        {
            PMICLED_SetColor(CYAN);
        }
        else if (strcasecmp(argv[1], "OFF") == 0)
        {
            PMICLED_SetColor(OFF);
        }
        else
        {
            printf("ERROR: Invalid param\n");
        }
    }
    else
    {
        printf("ERROR: Invalid param\n");
    }

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  CheckHrmSensor(CPU_INT16U        argc,
                                   CPU_CHAR         *argv[],
                                   SHELL_OUT_FNCT    out_fnct,
                                   SHELL_CMD_PARAM  *p_cmd_param)
{
    hrm_start();
    printf("%s : %s\n", "HRM", (Si117xCheckSensor() == SI117xHRM_SUCCESS) ? "OK" : "ERROR");
    hrm_stop();
    return (SHELL_EXEC_ERR_NONE);
}

static  SHELL_CMD  MainDebug_CmdTbl [] =
{
    {"reset",    system_reset},
    {"mem_info", mem_info},
#if (configUSE_GET_BOOTINFO==1)
    {"boot_info", bootloader_info},
#endif
    // I2C
    {"i2cr", i2c_dbg_r},
    {"i2cw", i2c_dbg_w},
#if (configUSE_PMIC > 0)
    {"vcell", pmic_get_vcell},
    {"soc", pmic_get_soc},
#endif
#if (configUSE_PMIC == PMIC_DEVICE_TPS65720)
    {"chstate", pmic_read_charge_state},
#endif
    // Temp for factory test
#if (configUSE_HRM > 0)
    {"hrmver", hrm_check_ver},
#endif
#if (configUSE_MOTER > 0)
    {"mt", motor_test},
#endif

#if (configUSE_LORA_TEST > 0)
    {"loratx", set_lora_tx},
    {"lorarx", set_lora_rx},
    {"loracw", set_lora_cw},
    {"lorastop", set_lora_stop},
    {"loratxlbt", set_lora_tx_lbt},
    {"loralna", set_lora_lna},
#endif

    /* Factory Test Command */
    {"FACC",  Check6axisSensor },             // 6 Axis Sensor
    {"FLORA", CheckSx1276      },
    {"FGPS",  CheckUbloxM8     },
    {"FLED",  CheckLed         },
    {"FHRM",  CheckHrmSensor   },

    {0,       0                }
};


void shell_start(void)
{
    RTOS_ERR  err;
    Terminal_Init();
    Shell_CmdTblAdd((CPU_CHAR *)"MainDebug", MainDebug_CmdTbl, &err);
}

#else
#define shell_start()
#endif  /* RTOS_MODULE_COMMON_SHELL_AVAIL */

void  debugInit(void)
{
#ifdef FEATURE_PTI_SUPPORT
    // Enable PTI
    configEnablePti();
#endif

    shell_start();
}


/*****************************************************************************************************
 *Function Name   :
 *Create Date     :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/

/*
 *      Dump raw data in hexadecimal format and charater format
 */
#ifndef HEXDUMP_COLS
#define HEXDUMP_COLS 8
#endif

//http://grapsus.net/blog/post/Hexadecimal-dump-in-C
void hexdump(uint8_t *name, void *mem, uint32_t len)
{
    unsigned int i, j;

    printf("\r\n%s (size = %u)\r\n", (char *)name, (unsigned int)len);

    for (i = 0; i < len + ((len % HEXDUMP_COLS) ? (HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++)
    {
        /* print offset */
        if (i % HEXDUMP_COLS == 0)
        {
            printf("0x%06x: ", i);
        }

        /* print hex data */
        if (i < len)
        {
            printf("%02x ", 0xFF & ((char *)mem)[i]);
        }
        else     /* end of block, just aligning for ASCII dump */
        {
            printf("   ");
        }

        /* print ASCII dump */
        if (i % HEXDUMP_COLS == (HEXDUMP_COLS - 1))
        {
            for (j = i - (HEXDUMP_COLS - 1); j <= i; j++)
            {
                if (j >= len)   /* end of block, not really printing */
                {
                    putchar(' ');
                    //dbg_printf(" ");
                }
                else if (isprint((int)((char *)mem)[j]))     /* printable char */
                {
                    putchar(0xFF & ((char *)mem)[j]);
                    //dbg_printf("%c", 0xFF & ((char*)mem)[j]);
                }
                else     /* other char */
                {
                    putchar('.');
                    //dbg_printf(".");
                }
            }
            //putchar('\n');
            printf("\r\n");
        }
    }
}


/*****************************************************************************************************
 *Function Name   :
 *Create Date     :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
static CPU_INT16S i2c_dbg_w(CPU_INT16U        argc,
                            CPU_CHAR         *argv[],
                            SHELL_OUT_FNCT    out_fnct,
                            SHELL_CMD_PARAM  *p_cmd_param)
{
    int32_t len = 0;
    char *pSubAddrStr = NULL;
    uint8_t buffer[64];
    uint8_t chan_id = 0; /* Channel Number           */
    uint8_t chip_id = 0; /* Chip Id / Slave Address  */
    uint16_t sub_addr = 0; /* Sub address              */
    uint8_t sub_mode = 0; /* Sub addressing Mode      */
    uint16_t n_bytes = 1; /* # Of bytes to be read    */
    int32_t retVal = 0;
    I2CM_ID pTmpI2cId;
    uint8_t pSub[10];

    if ((argc != 4) && (argc != 6)) {
        Usage: printf("\t - All number except size is treated as hex\n");
        printf("\t - Size must be less than 64 bytes\n");
        printf("\t   ex 1) i2cw 0 c0 10 2 0120  : write with sub address\n");
        printf("\t   ex 2) i2cw 0 c0 10020120   : write without sub address\n");

        return (SHELL_EXEC_ERR_NONE);

    }

    argv++; // skip i2cr

    if (*argv != NULL) {
        chan_id = Str_ParseNbr_Int32U((const CPU_CHAR *) *argv++,
        DEF_NULL,
        DEF_NBR_BASE_HEX);
    }
    if (*argv != NULL) {
        chip_id = Str_ParseNbr_Int32U((const CPU_CHAR *) *argv++,
        DEF_NULL,
        DEF_NBR_BASE_HEX);
    }

    if (argc == 6) {
        sub_addr = Str_ParseNbr_Int32U((const CPU_CHAR *) (pSubAddrStr = *argv++),
        DEF_NULL,
        DEF_NBR_BASE_HEX);

        n_bytes = Str_ParseNbr_Int32U((const CPU_CHAR *) *argv++,
        DEF_NULL,
        DEF_NBR_BASE_HEX);
    }

    if (*argv != NULL) {
        int i;
        char *inputStr = *argv;
        n_bytes = Str_Len(inputStr) / 2;
        for (i = 0; i < n_bytes * 2; i += 2) {
            buffer[i] = (inputStr[i] % 32 + 9) % 25 * 16 + (inputStr[i + 1] % 32 + 9) % 25;
        }
        hexdump((uint8_t *) "I2C Write Data", buffer, n_bytes);
        //test
        //return (SHELL_EXEC_ERR_NONE);
    }

    if ((n_bytes < 1) || (n_bytes > 64)) {
        goto Usage;
    }

    if (pSubAddrStr) {
        len = Str_Len(pSubAddrStr);

        if ((len != 2) && (len != 4)) {
            goto Usage;
        }

        sub_mode = len / 2;
    }

    if (sub_mode == 0) {
        printf("[I2C %d] I2C Write CH=0x%02x ID=x%02x size=%d", chan_id, chan_id, chip_id, n_bytes);
    } else {
        printf("[I2C %d] I2C Write CH=0x%02x ID=0x%02x sA=0x%x AS=%d, size=%d", chan_id, chan_id, chip_id, sub_addr,
                sub_mode, n_bytes);
    }

    if (chan_id == 0) {
        pTmpI2cId = i2c0_id;
    } else if (chan_id == 1) {
        pTmpI2cId = i2c1_id;
    } else {
        printf("[I2C %d] Error : Out of tuner number....", chan_id);
        goto Usage;
    }

    if (sub_mode == 2) {
        pSub[0] = (sub_addr >> 8) & 0xFF;
        pSub[1] = (sub_addr) & 0xFF;
    } else {
        pSub[0] = sub_addr;
    }


//	retVal = I2CSPM_MasterWrite(pTmpI2cId, chip_id, pSub, sub_mode, buffer, n_bytes);

    if (sub_mode!=0) {
        retVal = i2cm_writeData(pTmpI2cId, chip_id, pSub[1], buffer, n_bytes); // FIXME
    } else {
//        retVal = i2cm_readData(pTmpI2cId, chip_id, pSub[1], buffer, n_bytes); // FIXME
        //    retVal = I2CSPM_MasterRead(pTmpI2cId, chip_id, pSub,  sub_mode, buffer, n_bytes);
        printf("Not supported for now\r\n");
    }

    if (retVal < 0) {
        printf("[I2C %d]I2C Write Failed\r\n", chan_id);
    }

    return (SHELL_EXEC_ERR_NONE);

}


static CPU_INT16S i2c_dbg_r(CPU_INT16U        argc,
                            CPU_CHAR         *argv[],
                            SHELL_OUT_FNCT    out_fnct,
                            SHELL_CMD_PARAM  *p_cmd_param)
{
    PP_UNUSED_PARAM(out_fnct);
    PP_UNUSED_PARAM(p_cmd_param);

    int32_t len = 0;
    char *pSubAddrStr = NULL;
    uint8_t buffer[64];
    uint8_t chan_id = 0; /* Channel Number           */
    uint8_t chip_id = 0; /* Chip Id / Slave Address  */
    uint16_t sub_addr = 0; /* Sub address              */
    uint8_t sub_mode = 0; /* Sub addressing Mode      */
    uint16_t n_bytes = 1; /* # Of bytes to be read    */
    int32_t retVal = 0;
    I2CM_ID pTmpI2cId;
    uint8_t pSub[10];

    if ((argc < 3) || (argc > 5)) {
        Usage: printf("\t - All number except size is treated as hex\n");
        printf("\t - Size must be less than 64 bytes\n");
        printf("\t - i2cr bus_id chip_addr [reg_addr] [n_bytes=1] \n");
        printf("\t   ex 1) i2cr 0 a0 0010 4     : Read with 2 byte sub address\n");
        printf("\t   ex 2) i2cr 0 b0 10 8       : Read with 1 byte sub address\n");
        printf("\t   ex 3) i2cr 0 c0            : Read without sub address 1byte\n");
        printf("\t   ex 3) i2cr 0 c0 2          : Read without sub address 2byte\n");
        return (SHELL_EXEC_ERR_NONE);
    }

    argv++; // skip i2cr

    if (*argv != NULL) {
        chan_id = Str_ParseNbr_Int32U((const CPU_CHAR *) *argv++,
        DEF_NULL,
        DEF_NBR_BASE_HEX);
    }
    if (*argv != NULL) {
        chip_id = Str_ParseNbr_Int32U((const CPU_CHAR *) *argv++,
        DEF_NULL,
        DEF_NBR_BASE_HEX);
    }

    if (argc > 4) {
        sub_addr = Str_ParseNbr_Int32U((const CPU_CHAR *) (pSubAddrStr = *argv++),
        DEF_NULL,
        DEF_NBR_BASE_HEX);

    }

    if (*argv != NULL) {
        n_bytes = Str_ParseNbr_Int32U((const CPU_CHAR *) *argv++,
        DEF_NULL,
        DEF_NBR_BASE_HEX);
    }

    if (n_bytes == 0) {
        n_bytes = 1;
    }

    if (n_bytes > 64) {
        goto Usage;
    }

    if (pSubAddrStr) {
        len = Str_Len(pSubAddrStr);

        if ((len != 2) && (len != 4)) {
            goto Usage;
        }

        sub_mode = len / 2;
    }

    if (sub_mode == 0) {
        printf("[I2C %d] I2C Read CH=0x%02x ID=x%02x size=%d\n", chan_id, chan_id, chip_id, n_bytes);
    } else {
        printf("[I2C %d] I2C Read CH=0x%02x ID=0x%02x sA=0x%x AS=%d, size=%d\n", chan_id, chan_id, chip_id, sub_addr,
                sub_mode, n_bytes);
    }

    if (chan_id == 0) {
        pTmpI2cId = i2c0_id;
    } else if (chan_id == 1) {
        pTmpI2cId = i2c1_id;
    } else {
        printf("[I2C %d] Error : Out of tuner number....", chan_id);
        goto Usage;
    }

    if (sub_mode == 2) {
        pSub[0] = (sub_addr >> 8) & 0xFF;
        pSub[1] = (sub_addr) & 0xFF;
    } else {
        pSub[0] = sub_addr;
    }

    if (sub_mode!=0) {
        retVal = i2cm_readData(pTmpI2cId, chip_id, pSub[0], buffer, n_bytes); // FIXME
    } else {
//        retVal = i2cm_readData(pTmpI2cId, chip_id, pSub[1], buffer, n_bytes); // FIXME
        //    retVal = I2CSPM_MasterRead(pTmpI2cId, chip_id, pSub,  sub_mode, buffer, n_bytes);
        printf("Not supported for now\r\n");
    }

    if (retVal == 0) {
        hexdump((uint8_t *) "I2C Read Results", buffer, n_bytes);
    } else if (retVal < 0) {
        printf("[I2C %d]I2C Read Failed\r\n", chan_id);
    }

    return (SHELL_EXEC_ERR_NONE);
}



void debug_printf(const char *format, ...)
{
    RTOS_ERR err;
    uint32_t timeInMsec;
    uint8_t idx = 0;
    char msg[255] = {'\0'};
    va_list args;

    timeInMsec = OS_TICKS_2_MS(OSTimeGet(&err));
    idx = sprintf(msg, "\r%10ld.%03lu s: ", timeInMsec / 1000, timeInMsec % 1000);

    va_start(args, format);
    idx += vsnprintf(msg + idx, sizeof(msg) - idx, format, args);
    va_end(args);

    Terminal_WrStr(msg, idx);
}

void debug_printf_data(const char *format, ...)
{
    uint8_t len = 0;
    char msg[255] = {'\0'};
    va_list args;

    va_start(args, format);
    len = vsnprintf(msg, sizeof(msg), format, args);
    va_end(args);

    Terminal_WrStr(msg, len);
}
