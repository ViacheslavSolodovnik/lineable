/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "rtos_app.h"
#include "rtos_gecko.h"

#include "app_cfg.h"
#include "timer.h"
#include "userdata.h"
#include "sleep.h"
#include "flash.h"
#include "gpiointerrupt.h"
#include "sys_os.h"

#include "pedometer.h"

#include <drivers/fuelgauge/fuelgauge.h>
#include <drivers/pmic/pmic.h>

#include "app_hrm.h"
#include "gps.h"
#include "lsm6ds3_acc_gyro.h"

#include "lora.h"
#include "lora_packet.h"

#include "gatt_db.h"
#include "app_ble.h"

#include "stateMachine.h"

#include "app_main.h"

/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/

#undef  __MODULE__
#define __MODULE__ "MAIN APP"
#undef  DBG_MODULE
#define DBG_MODULE   0
#define MAIN_APP_DBG 0

/* Define main event flag */
#define MAIN_EVENT_FLAG_INPUT_DOUBLE_TAP            ((OS_FLAGS)(1<<0))
#define MAIN_EVENT_FLAG_CHANGE_OP_MODE              ((OS_FLAGS)(1<<1))
#define MAIN_EVENT_FLAG_ALL                         (MAIN_EVENT_FLAG_INPUT_DOUBLE_TAP       | \
                                                     MAIN_EVENT_FLAG_CHANGE_OP_MODE)

#define COMM_EVENT_FLAG_LORA_JOIN                   ((OS_FLAGS)(1<<0))
#define COMM_EVENT_FLAG_LORA_TIMESYNC               ((OS_FLAGS)(1<<1))
#define COMM_EVENT_FLAG_LORA_SEND                   ((OS_FLAGS)(1<<2))
#define COMM_EVENT_FLAG_LORA_ALL                    (COMM_EVENT_FLAG_LORA_JOIN | COMM_EVENT_FLAG_LORA_TIMESYNC | COMM_EVENT_FLAG_LORA_SEND)


#define BATTERY_CHECK_DUTY                          30  /* 30 sec */

#define MSG_Q_MAX_NUM                               10

/** Notification bits reservation */
typedef struct
{
    uint32_t id;
    uint32_t size;
    uint32_t addr;
} MainAppMsg;

/* Type of main events */
typedef enum
{
    MAIN_EVENT_TYPE_SEQUENCE = 1,
    MAIN_EVENT_TYPE_REPEAT
} MainEventType;

typedef enum
{
    MAIN_SEQ_EVENT_SYS_RESET = 0,
    MAIN_SEQ_EVENT_SYS_INIT,
    MAIN_SEQ_EVENT_NEXT_NORMAL,
    MAIN_SEQ_EVENT_NET_JOIN_DONE,
    MAIN_SEQ_EVENT_SYS_NOP,

    MAIN_SEQ_EVENT_NEXT_TEST,
    MAIN_SEQ_EVENT_TEST_MODE_DONE,

    MAIN_SEQ_EVENT_NEXT_CERT,
    MAIN_SEQ_EVENT_CERT_MODE_JOIN,
    MAIN_SEQ_EVENT_CERT_MODE_RUN,
    MAIN_SEQ_EVENT_CERT_MODE_DONE,

    MAIN_SEQ_EVENT_STOP_MODE,
    MAIN_SEQ_EVENT_START_MODE,

    MAIN_REP_EVENT_WAKEUP,
    MAIN_REP_EVENT_SEND,
    MAIN_REP_EVENT_SEND_DONE,

} MainEvents;

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

static void MainStateTask(void *p_arg);
static void MainEventTask(void *p_arg);
static void MainCommTask(void *p_arg);
static void WakeupTimerCb(void);
static void SendTimerCb(void);

static void SetRepEventTimer(MainOpMode op_mode);
/* for Finite State Machine */
static uint8_t SendMainEvent(MainEventType type, MainEvents event);
static uint8_t SendLoRaEvent(OS_FLAGS event);

static bool CompareSeqEvent(void *in, struct event *event);
static bool ComparePepEvent(void *in, struct event *event);

static void EnterInitState(void *state_data, struct event *event);
static void EnterJoinState(void *state_data, struct event *event);
static void EnterSleepState(void *state_data, struct event *event);
static void EnterWakeupState(void *state_data, struct event *event);
static void EnterSendState(void *state_data, struct event *event);
static void EnterTestState(void *state_data, struct event *event);

static void ExecSendDone(void *old_state_data, struct event *event, void *new_state_data);

static void EnterCertState(void *state_data, struct event *event);
static void ExecCertJoin(void *old_state_data, struct event *event, void *new_state_data);
static void ExecCertRun(void *old_state_data, struct event *event, void *new_state_data);
static void ExecCertSend(void *old_state_data, struct event *event, void *new_state_data);

static void PrintEnterState(void *state_data, struct event *event);
static void PrintExitState(void *state_data, struct event *event);

static void DeviceEventISR(uint8_t intNo);

/*
****************************************************************************************************
*                                       GLOBAL VARIABLES
****************************************************************************************************
*/

/** log data */
uint8_t LogData[LOG_PACKET_LENGTH * MAXIMUM_LOG_NUM] = {0};
uint16_t StackPointer = 0, LOG_Space_IsFull = 0;

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

/* Main Event Task */
static OS_TCB       g_main_event_task_tcb;
static CPU_STK      g_main_event_task_stk[MAIN_EVENT_TASK_STK_SIZE];
static OS_FLAG_GRP  g_main_event_flags;
/* Main State Task */
static OS_TCB       g_main_state_task_tcb;
static CPU_STK      g_main_state_task_stk[MAIN_STATE_TASK_STK_SIZE];
/* Main LoRa Task */
static OS_TCB       g_main_comm_task_tcb;
static CPU_STK      g_main_comm_task_stk[MAIN_COMM_TASK_STK_SIZE];
static OS_FLAG_GRP  g_main_comm_flags;

static struct
{
    uint32_t wakeup;
    uint32_t send;
} g_duty_table[MAIN_OP_MODE_MAX] =
{
    [MAIN_OP_MODE_NORMAL] = {
        .wakeup = TIME_SEC(150),//450),
        .send   = TIME_MIN(5),//10),
    },

    [MAIN_OP_MODE_SOS] = {
        .wakeup = 0,
        .send   = TIME_MIN(1),
    },

    [MAIN_OP_MODE_TEST] = {
        .wakeup = 0,
        .send   = TIME_SEC(30),
    },

    [MAIN_OP_MODE_CERTI] = {
        .wakeup = 0,
        .send   = TIME_SEC(10),
    },
};

#define OP_MODE_NORMAL_VAL           0
#define OP_MODE_NORMAL_MASK          0x01
#define OP_MODE_EMERGENCY_VAL        1
#define OP_MODE_EMERGENCY_MASK       0x01
#define OP_MODE_CHARGING_VAL         1
#define OP_MODE_CHARGING_MASK        0x10

static struct
{
    uint8_t      op_mode;
    bool         allow_sleep;
    uint8_t      q_cnt;

    /** Time Sync */
    bool         is_sync_time;

    /** RTC Timer */
    TimerEvent_t timer_wakeup;
    TimerEvent_t timer_send;
} g_app_obj;
uint8_t g_packet_buff[LORA_PACKET_MAX_LENGTH] = {0};
static MainAppMsg g_main_evt_msg[MSG_Q_MAX_NUM];
#if USE_SKT_OTA_TEST
static bool g_lora_msg_cfm = false;     /* true=MCPS_CONFIRMED, false=MCPS_UNCONFIRMED */
#else
static bool g_lora_msg_cfm = true;      /* true=MCPS_CONFIRMED, false=MCPS_UNCONFIRMED */
#endif

/* Finite State Machine */
static struct state root_state;
static struct state idle_state;
static struct state error_state;
static struct state init_state;
static struct state join_state;
static struct state sleep_state;
static struct state wakeup_state;
static struct state send_state;
static struct state test_state;
static struct state cert_state;
static struct state none_state;

static struct state root_state =
{
    .parentState    = DEF_NULL,
    .entryState     = &idle_state,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_SYS_RESET, &CompareSeqEvent, DEF_NULL, &idle_state },
    },
    .numTransitions = 1,
    .data           = "ROOT",
    .entryAction    = &PrintEnterState,
    .exitAction     = &PrintExitState
};

static struct state idle_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_SYS_INIT, &CompareSeqEvent, DEF_NULL, &init_state },
    },
    .numTransitions = 1,
    .data           = "IDLE",
    .entryAction    = &PrintEnterState,
    .exitAction     = &PrintExitState
};

static struct state init_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_NEXT_NORMAL, &CompareSeqEvent, DEF_NULL, &join_state },
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_NEXT_TEST,   &CompareSeqEvent, DEF_NULL, &test_state },
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_NEXT_CERT,   &CompareSeqEvent, DEF_NULL, &cert_state },
    },
    .numTransitions = 3,
    .data           = "INIT",
    .entryAction    = &EnterInitState,
    .exitAction     = &PrintExitState
};

static struct state join_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_NET_JOIN_DONE, &CompareSeqEvent,  DEF_NULL,     &sleep_state },
    },
    .numTransitions = 1,
    .data           = "JOIN",
    .entryAction    = &EnterJoinState,
    .exitAction     = &PrintExitState
};

static struct state sleep_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_REPEAT,   (void *)(MainEvents *)MAIN_REP_EVENT_WAKEUP,        &ComparePepEvent,  DEF_NULL,     &wakeup_state },
        { MAIN_EVENT_TYPE_REPEAT,   (void *)(MainEvents *)MAIN_REP_EVENT_SEND,          &ComparePepEvent,  DEF_NULL,     &send_state   },
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_STOP_MODE,     &CompareSeqEvent,  DEF_NULL,     &none_state   },
    },
    .numTransitions = 3,
    .data           = "SLEEP",
    .entryAction    = &EnterSleepState,
    .exitAction     = &PrintExitState
};

static struct state wakeup_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_REPEAT, (void *)(MainEvents *)MAIN_REP_EVENT_SEND,        &ComparePepEvent, DEF_NULL, &send_state },
    },
    .numTransitions = 1,
    .data           = "WAKEUP",
    .entryAction    = &EnterWakeupState,
    .exitAction     = &PrintExitState
};

static struct state send_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_REPEAT, (void *)(MainEvents *)MAIN_REP_EVENT_SEND_DONE, &ComparePepEvent, &ExecSendDone, &sleep_state }
    },
    .numTransitions = 1,
    .data           = "SEND",
    .entryAction    = &EnterSendState,
    .exitAction     = &PrintExitState
};

static struct state test_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_TEST_MODE_DONE, &CompareSeqEvent, DEF_NULL, &init_state },
    },
    .numTransitions = 1,
    .data           = "TEST",
    .entryAction    = &EnterTestState,
    .exitAction     = &PrintExitState
};

static struct state cert_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_CERT_MODE_JOIN, &CompareSeqEvent, &ExecCertJoin, &cert_state },
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_CERT_MODE_RUN,  &CompareSeqEvent, &ExecCertRun,  &cert_state },
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_CERT_MODE_DONE, &CompareSeqEvent,  DEF_NULL,     &init_state },
        { MAIN_EVENT_TYPE_REPEAT,   (void *)(MainEvents *)MAIN_REP_EVENT_SEND,           &ComparePepEvent, &ExecCertSend, &cert_state },
    },
    .numTransitions = 4,
    .data           = "CERT",
    .entryAction    = &EnterCertState,
    .exitAction     = &PrintExitState
};

static struct state none_state =
{
    .parentState    = &root_state,
    .entryState     = DEF_NULL,
    .transitions    = (struct transition[])
    {
        { MAIN_EVENT_TYPE_SEQUENCE, (void *)(MainEvents *)MAIN_SEQ_EVENT_START_MODE, &CompareSeqEvent, DEF_NULL,       &sleep_state },
    },
    .numTransitions = 1,
    .data           = "NONE",
    .entryAction    = &PrintEnterState,
    .exitAction     = &PrintExitState
};

static struct state error_state =
{
    .entryAction = DEF_NULL
};


/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/

uint8_t MainApp_Init(void)
{
    RTOS_ERR err;
    uint8_t error = AM_SUCCESS;

    g_app_obj.allow_sleep  = false;
    g_app_obj.op_mode      = MAIN_OP_MODE_NORMAL;
    g_app_obj.q_cnt        = 0;
    g_app_obj.is_sync_time = false;

    /** Check user-data region */
    DeviceUserDataCheck();

    /** init device */
    LSM6DS3_Init(DeviceEventISR);
    PEDOMETER_Init();

    /**
     * Create App-Main component OS task.
     */

    /* Event flags */
    OSFlagCreate((OS_FLAG_GRP *)&g_main_event_flags,
                 (CPU_CHAR    *) "Main Event Flags",
                 (OS_FLAGS     ) 0,
                 (RTOS_ERR    *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
    OSFlagCreate((OS_FLAG_GRP *)&g_main_comm_flags,
                 (CPU_CHAR    *) "Main LoRa Flags",
                 (OS_FLAGS     ) 0,
                 (RTOS_ERR    *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* Event task */
    OSTaskCreate((OS_TCB       *)&g_main_event_task_tcb,
                 (CPU_CHAR     *) "Main Event Task",
                 (OS_TASK_PTR   ) MainEventTask,
                 (void         *) DEF_NULL,
                 (OS_PRIO       ) MAIN_EVENT_TASK_PRIO,
                 (CPU_STK      *)&g_main_event_task_stk[0],
                 (CPU_STK_SIZE  ) (MAIN_EVENT_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  ) MAIN_EVENT_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) 0u,
                 (OS_TICK       ) 0u,
                 (void         *) DEF_NULL,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* State task */
    OSTaskCreate((OS_TCB       *)&g_main_state_task_tcb,
                 (CPU_CHAR     *) "Main State Task",
                 (OS_TASK_PTR   ) MainStateTask,
                 (void         *) DEF_NULL,
                 (OS_PRIO       ) MAIN_STATE_TASK_PRIO,
                 (CPU_STK      *)&g_main_state_task_stk[0],
                 (CPU_STK_SIZE  ) (MAIN_STATE_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  ) MAIN_STATE_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) MAIN_STATE_TASK_Q_NUM,
                 (OS_TICK       ) 0u,
                 (void         *) DEF_NULL,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* LoRa task */
    OSTaskCreate((OS_TCB       *)&g_main_comm_task_tcb,
                 (CPU_CHAR     *) "Main Comm Task",
                 (OS_TASK_PTR   ) MainCommTask,
                 (void         *) DEF_NULL,
                 (OS_PRIO       ) MAIN_COMM_TASK_PRIO,
                 (CPU_STK      *)&g_main_comm_task_stk[0],
                 (CPU_STK_SIZE  ) (MAIN_COMM_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  ) MAIN_COMM_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) 0u,
                 (OS_TICK       ) 0u,
                 (void         *) DEF_NULL,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    return error;
}

uint8_t MainApp_Start(void)
{
    return SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_SYS_INIT);
}

uint8_t MainApp_DetectDoubleTap(void)
{
    RTOS_ERR err;

    OSFlagPost((OS_FLAG_GRP *)&g_main_event_flags,
               (OS_FLAGS     ) MAIN_EVENT_FLAG_INPUT_DOUBLE_TAP,
               (OS_OPT       ) OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
    return (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
}

uint8_t MainApp_ChangeOpMode(MainOpMode op_mode)
{
    RTOS_ERR err;

    g_app_obj.op_mode = op_mode;

    OSFlagPost((OS_FLAG_GRP *)&g_main_event_flags,
               (OS_FLAGS     ) MAIN_EVENT_FLAG_CHANGE_OP_MODE,
               (OS_OPT       ) OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
    return (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
}

uint8_t MainApp_GetOpMode(void)
{
    return g_app_obj.op_mode;
}

uint8_t MainApp_ChangeMsgType(bool is_cfm)
{
    g_lora_msg_cfm = is_cfm;
    return 0;
}

uint8_t MainApp_ChangeTestMode(bool run)
{
    if (run == true)
    {
        SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_START_MODE);
    }
    else
    {
        SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_STOP_MODE);
    }
    return 0;
}

uint8_t MainApp_UpdateSysTime(uint32_t epoch)
{
    CLK_DATE_TIME date_time;
    CPU_CHAR date_time_str[30];

    /* Update System time */
    Clk_TS_UnixToDateTime(epoch, 0, &date_time);
    Clk_SetDateTime(&date_time);

    /* Update flag */
    g_app_obj.is_sync_time = true;

    /* debug */
    Clk_DateTimeToStr(&date_time, CLK_STR_FMT_YYYY_MM_DD_HH_MM_SS_UTC, date_time_str, 30);
    //APP_TRACE_DEBUG("Set Time : %10s\n", date_time);

    return 0;
}

uint8_t MainApp_DoTimeSync(void)
{
    if (UNIT_INSTALLED)
    {
        #if (LORA_SUPPORT_TIMESYNC)
        return SendLoRaEvent(COMM_EVENT_FLAG_LORA_TIMESYNC);
        #else
        return SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NET_JOIN_DONE);
        #endif
    }

    return 0;
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

static void PrintEnterState(void *state_data, struct event *event)
{
    APP_TRACE_STATE("\t\t             ==> {%-10s}\n", (char *)state_data);
}

static void PrintExitState(void *state_data, struct event *event)
{
    APP_TRACE_STATE("\t\t{%-10s}\n", (char *)state_data);
}

static uint8_t SendMainEvent(MainEventType type, MainEvents event)
{
    RTOS_ERR err;
    MainAppMsg *q_msg;

    g_app_obj.q_cnt %= MSG_Q_MAX_NUM;
    q_msg = &g_main_evt_msg[g_app_obj.q_cnt];
    g_app_obj.q_cnt++;

    q_msg->id   = (uint32_t)type;
    q_msg->addr = (uint32_t)event;

    OSTaskQPost((OS_TCB      *)&g_main_state_task_tcb,
                (void        *) q_msg,
                (OS_MSG_SIZE  ) sizeof(MainAppMsg),
                (OS_OPT       ) OS_OPT_POST_FIFO,
                (RTOS_ERR    *)&err);

    return (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
}

static uint8_t SendLoRaEvent(OS_FLAGS event)
{
    RTOS_ERR err;

    OSFlagPost((OS_FLAG_GRP *)&g_main_comm_flags,
               (OS_FLAGS     ) event,
               (OS_OPT       ) OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
    return (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
}

static void SetRepEventTimer(MainOpMode op_mode)
{
    switch (op_mode)
    {
        case MAIN_OP_MODE_NORMAL:
            /* Wake-up Timer */
            TimerSetValue(&g_app_obj.timer_wakeup, g_duty_table[op_mode].wakeup);
            TimerStart(&g_app_obj.timer_wakeup);
            /* Send Timer */
            TimerSetValue(&g_app_obj.timer_send, g_duty_table[op_mode].send);
            TimerStart(&g_app_obj.timer_send);
            break;
        case MAIN_OP_MODE_SOS:
            /* Wake-up Timer */
            TimerStop(&g_app_obj.timer_wakeup);
            /* Send Timer */
            TimerSetValue(&g_app_obj.timer_send, g_duty_table[op_mode].send);
            TimerStart(&g_app_obj.timer_send);
            break;
        default:
            break;
    }
}

static bool CompareSeqEvent(void *in, struct event *event)
{
    if (event->type != MAIN_EVENT_TYPE_SEQUENCE)
    {
        return false;
    }

    return ((MainEvents *)in == (MainEvents *)event->data) ? true : false;
}

static bool ComparePepEvent(void *in, struct event *event)
{
    if (event->type != MAIN_EVENT_TYPE_REPEAT)
    {
        return false;
    }

    return ((MainEvents *)in == (MainEvents *)event->data) ? true : false;
}

static void EnterInitState(void *state_data, struct event *event)
{
    PrintEnterState(state_data, event);

    if (UNIT_FACTORY_TEST)
    {
        APP_TRACE_LOG("OPERATION MODE : %s\n", "Factory Test Mode");
        SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NEXT_TEST);
    }
    else if (UNIT_CERTI_TEST)
    {
        APP_TRACE_LOG("OPERATION MODE : %s\n", "Certification Mode");
        g_app_obj.is_sync_time = true;  /* Don't need time sync */
        SET_USERFLAG(FLAG_SKT_OTB_TEST);
        SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NEXT_CERT);
    }
    else
    {
        /* Start device */
        PMIC_Start();                       /* PMIC */
        LSM6DS3_Start();                    /* Pedometer, Tilt */

        APP_TRACE_LOG("OPERATION MODE : %s\n", (g_app_obj.op_mode == MAIN_OP_MODE_SOS) ? "SOS" : "NORMAL");
        SetRepEventTimer((MainOpMode)g_app_obj.op_mode);

        /* Change state */
        SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NEXT_NORMAL);
    }
}

static void EnterJoinState(void *state_data, struct event *event)
{
    PrintEnterState(state_data, event);

    SendLoRaEvent(COMM_EVENT_FLAG_LORA_JOIN);
}

static void ExecSendDone(void *old_state_data, struct event *event, void *new_state_data)
{
    /* Restart timer */
    SetRepEventTimer((MainOpMode)g_app_obj.op_mode);
}

static void EnterSleepState(void *state_data, struct event *event)
{
    PrintEnterState(state_data, event);

    /* Control Device */
    if (g_app_obj.op_mode == MAIN_OP_MODE_SOS)
    {
        APP_TRACE_LOG("\t- Enable device (HRM, GPS)\n");
        hrm_start();            /* HRM */
        gnssPowerOn();          /* GPS */

        if (LORAWAN_IsNetworkJoined() == false)
        {
            SendLoRaEvent(COMM_EVENT_FLAG_LORA_JOIN);
        }
    }
}

static void EnterWakeupState(void *state_data, struct event *event)
{
    PrintEnterState(state_data, event);

    if (LORAWAN_IsNetworkJoined() == false)
    {
        SendLoRaEvent(COMM_EVENT_FLAG_LORA_JOIN);
    }

    /* wake-up sensor (HRM, GPS, Fuelgage) */
    APP_TRACE_LOG("Wake-up sensors (HRM, GPS)\n");
    hrm_start();        /* HRM */
    gnssPowerOn();      /* GPS */
}

static void EnterSendState(void *state_data, struct event *event)
{
    SensorData sensor_data;
    uint8_t sos_state;
    uint8_t location_type = 0;
    uint8_t worn_state = 0;

    PrintEnterState(state_data, event);

    /*--- Turn off sensors -----------------------------------------------------------------------*/
    APP_TRACE_LOG("\t- Disable device (HRM, GPS)\n");
    hrm_stop();             /* HRM */
    gnssPowerOff();         /* GPS */

    /*--- read operation mode --------------------------------------------------------------------*/
    sos_state = g_app_obj.op_mode;  //normal:0, emergency:1

    /*--- read sensor ----------------------------------------------------------------------------*/
    /* Time */
    GetCurrentTime((uint32_t *)&sensor_data.time_data.epoch_timestamp);
    /* Battery */
    FUELGAUGE_GetSoc((uint16_t *)&sensor_data.batt_data.level);
    /* 6-Axis */
    PEDOMETER_ReadData((uint32_t *)&sensor_data.accel_data.steps);
    /* GPS */
    if (gps_position_get((uint8_t *)&sensor_data.gps_data))
    {
        location_type = LOCATION_TYPE_GPS;
    }
    else
    {
        location_type = LOCATION_TYPE_NONE;
        memset(&sensor_data.gps_data, 0, 6);    //initialize gps data
    }
    /* HRM */
    worn_state = hrm_get((int16_t *)&sensor_data.hrm_data.heart_rate); /* HRM            */
    /* Bluetooth connection state */
    if (BLE_APP_IsConnected == true)
    {
        //change into normal mode, when bluetooth is connected.
        if (sos_state)
        {
            sos_state = SOS_TYPE_NO_SOS;
            MainApp_ChangeOpMode(MAIN_OP_MODE_NORMAL);
        }
    }

    /*--- print op mode and sensor value ---------------------------------------------------------*/
    APP_TRACE_LOG("\t- Sensor Data:\n");
    APP_TRACE_LOG("\t %10s : %s\n", "MODE", (sos_state == true) ? "SOS" : "Normal");
    APP_TRACE_LOG("\t %10s : %d\n", "BATT", sensor_data.batt_data.level);
    APP_TRACE_LOG("\t %10s : %s\n", "HRM", (worn_state == STATUS_FLAG_TYPE_WORN) ? "WORN" : "REMOVE");
    APP_TRACE_LOG("\t %10s   %d\n", " ", sensor_data.hrm_data.heart_rate);
    APP_TRACE_LOG("\t %10s : %s\n", "GPS", (location_type == LOCATION_TYPE_GPS) ? "Fix" : "Don't Fix");
    APP_TRACE_LOG("\t %10s : %d\n", "STEP", sensor_data.accel_data.steps);
    APP_TRACE_LOG("\t %10s : %s\n", "COMM", (BLE_APP_IsConnected == true) ? "BLE" : "LORA");

    /*--- make packet ----------------------------------------------------------------------------*/
    LORA_MakePacket(DEVICE_TYPE_LINEABLE_ONE,
                    PACKET_TYPE_SCHEDULE,
                    PROTOCOL_VERSION_TYPE_0,
                    worn_state,
                    sos_state,
                    location_type,
                    sensor_data,
                    g_packet_buff);
    #if MAIN_APP_DBG
    SYSOS_DumpData(&g_packet_buff[0], LORA_PACKET_LENGTH, "PACKET");
    #endif

    /*--- send packet ----------------------------------------------------------------------------*/
    APP_TRACE_LORA("Send Data\n");
    if (BLE_APP_IsConnected == true)
    {
        gecko_cmd_hardware_set_soft_timer(TIMER_START_1SEC, PERIODIC_DATA_TX_TIMER, 0);

        /* Send send-done event */
        SendMainEvent(MAIN_EVENT_TYPE_REPEAT, MAIN_REP_EVENT_SEND_DONE);
    }
    else
    {
        SendLoRaEvent(COMM_EVENT_FLAG_LORA_SEND);
    }
}

static void EnterTestState(void *state_data, struct event *event)
{
    PrintEnterState(state_data, event);
}

static void EnterCertState(void *state_data, struct event *event)
{
    PrintEnterState(state_data, event);

    SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_CERT_MODE_JOIN);
}

static void ExecCertJoin(void *old_state_data, struct event *event, void *new_state_data)
{
    uint8_t retry_join = 0;

    APP_TRACE_LOG("JOIN:\n");
    if (LORAWAN_IsNetworkJoined() == false)
    {
        if (LORAWAN_JoinNetwork(LORA_JOIN_TYPE_AUTO) != LORA_OK)
        {
            retry_join = 1;
        }
    }

    APP_TRACE_LOG("\t- Do %s\n", (retry_join == 0) ? "next" : "retry");

    SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, (retry_join == 0) ? MAIN_SEQ_EVENT_CERT_MODE_RUN : MAIN_SEQ_EVENT_CERT_MODE_JOIN);
}

static void ExecCertRun(void *old_state_data, struct event *event, void *new_state_data)
{
    APP_TRACE_LOG("RUN:\n");

    Mem_Set(g_packet_buff, 0x01, LORA_PACKET_LENGTH);

    SendMainEvent(MAIN_EVENT_TYPE_REPEAT, MAIN_REP_EVENT_SEND);
}

static void ExecCertSend(void *old_state_data, struct event *event, void *new_state_data)
{
    APP_TRACE_LOG("SEND:\n");

    /* Restart send timer */
    TimerSetValue(&g_app_obj.timer_send, g_duty_table[MAIN_OP_MODE_CERTI].send);
    TimerStart(&g_app_obj.timer_send);

    /* Send data */
    LORAWAN_SendOnly(LORA_APP_SERVER_PORT,
                     false,                      /* Confirmed message */
                     g_packet_buff,
                     LORA_PACKET_CERTI_LENGTH);
    APP_TRACE_LOG("\t- Done\n");
}

static void WakeupTimerCb(void)
{
    /* Stop timer */
    TimerStop(&g_app_obj.timer_wakeup);

    /* Change WAKEUP state */
    SendMainEvent(MAIN_EVENT_TYPE_REPEAT, MAIN_REP_EVENT_WAKEUP);
}

static void SendTimerCb(void)
{
    /* Stop timer */
    TimerStop(&g_app_obj.timer_send);

#if USE_SKT_OTA_TEST
    /* Restart timer */
    SetRepEventTimer((MainOpMode)g_app_obj.op_mode);
#endif

    /* Change SEND state */
    SendMainEvent(MAIN_EVENT_TYPE_REPEAT, MAIN_REP_EVENT_SEND);
}

static void MainEventTask(void *p_arg)
{
    RTOS_ERR err;
    OS_FLAGS event;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON)
    {
        /* Wait event flag */
        event = OSFlagPend((OS_FLAG_GRP *)&g_main_event_flags,
                           (OS_FLAGS     ) MAIN_EVENT_FLAG_ALL,
                           (OS_TICK      ) 0,
                           (OS_OPT       ) OS_OPT_DEFAULT,
                           (CPU_TS      *) NULL,
                           (RTOS_ERR    *)&err);
        if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
        {
            APP_TRACE_LOG("%s - ERROR: OSFlagPend\n", __func__);
            continue;
        }

        /*------------------------------------------------------------------------------------------
         *  Double-tap Event
         -----------------------------------------------------------------------------------------*/
        if (event & MAIN_EVENT_FLAG_INPUT_DOUBLE_TAP)
        {
            uint8_t motion_event;

            LSM6DS3_GetEvent(&motion_event);

            if (motion_event & MOTION_EVENT_DOUBLE_TAP_DET)
            {
                APP_TRACE_LOG("EVENT:: INPUT_DOUBLE_TAP\n");
                /* Indicate batter level */
                PMICLED_Tap();
            }
        }

        /*------------------------------------------------------------------------------------------
         *  Change Operation Event
         -----------------------------------------------------------------------------------------*/
        if (event & MAIN_EVENT_FLAG_CHANGE_OP_MODE)
        {
            APP_TRACE_LOG("EVENT:: CHANGE_OP_MODE\n");

            TimerStop(&g_app_obj.timer_wakeup);
            TimerStop(&g_app_obj.timer_send);

            SetRepEventTimer((MainOpMode)g_app_obj.op_mode);
        }
    }
}

static void MainStateTask(void *p_arg)
{
    RTOS_ERR err;
    OS_MSG_SIZE q_size;
    MainAppMsg *q_msg;

    struct stateMachine main_state;

    PP_UNUSED_PARAM(p_arg);

    /* Initialize event timer */
    TimerInit(&g_app_obj.timer_wakeup,  WakeupTimerCb);
    TimerInit(&g_app_obj.timer_send,    SendTimerCb);

    /* Initialize state machine */
    stateM_init(&main_state, &idle_state, &error_state);

    /* Initialize LoRaWAN */
    LORAWAN_Init();

    while (DEF_ON)
    {
        /* Wait queue message */
        q_msg = (MainAppMsg *)OSTaskQPend((OS_TICK      ) 0,
                                          (OS_OPT       ) OS_OPT_PEND_BLOCKING,
                                          (OS_MSG_SIZE *)&q_size,
                                          (CPU_TS      *) DEF_NULL,
                                          (RTOS_ERR    *)&err);
        if (q_msg == DEF_NULL)
        {
            continue;
        }

        /* Process event */
        stateM_handleEvent(&main_state, &(struct event)
        {
            q_msg->id, (void *)(MainEvents *)q_msg->addr
        });
    }
}

static void MainCommTask(void *p_arg)
{
    RTOS_ERR err;
    OS_FLAGS events;
    uint8_t cnt_time_sync = 0;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON)
    {
        /* Wait event flag */
        events = OSFlagPend((OS_FLAG_GRP *)&g_main_comm_flags,
                            (OS_FLAGS     ) COMM_EVENT_FLAG_LORA_ALL,
                            (OS_TICK      ) 0,
                            (OS_OPT       ) OS_OPT_DEFAULT,
                            (CPU_TS      *) NULL,
                            (RTOS_ERR    *)&err);
        if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
        {
            APP_TRACE_LOG("%s - ERROR: OSFlagPend\n", __func__);
            continue;
        }

        if (events & COMM_EVENT_FLAG_LORA_JOIN)
        {
            cnt_time_sync = 0;

            /* Try join LoRa network */
            if (LORAWAN_JoinNetwork(LORA_JOIN_TYPE_AUTO) != LORA_OK)
            {
                SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NET_JOIN_DONE);
            }
        }

        if (events & COMM_EVENT_FLAG_LORA_TIMESYNC)
        {
            if (g_app_obj.is_sync_time == true)     /* Already time sync done */
            {
                APP_TRACE_LOG("Already time-syinc done\n");
                SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NET_JOIN_DONE);
            }
            else
            {
                LORAWAN_SendDevTimeReq();

                SYSOS_DelayMs(TIME_SEC(5));     /* Some delay : 5 sec */

                if ((g_app_obj.is_sync_time == true) || (cnt_time_sync > 3))
                {
                    SendMainEvent(MAIN_EVENT_TYPE_SEQUENCE, MAIN_SEQ_EVENT_NET_JOIN_DONE);
                }
                else
                {
                    SendLoRaEvent(COMM_EVENT_FLAG_LORA_TIMESYNC);
                    cnt_time_sync++;
                }
            }
        }

        if (events & COMM_EVENT_FLAG_LORA_SEND)
        {
            /** check LoRa network state */
            if (LORAWAN_IsNetworkJoined() == true)
            {
                #if USE_SKT_OTA_TEST
                LORAWAN_SendData(LORA_APP_SERVER_PORT,
                                 g_lora_msg_cfm,
                                 g_packet_buff,
                                 LORA_PACKET_LENGTH,
                                 false,
                                 true);
                #else
                LORAWAN_SendData(LORA_APP_SERVER_PORT,
                                 g_lora_msg_cfm,
                                 g_packet_buff,
                                 LORA_PACKET_LENGTH,
                                 true,
                                 false);
                #endif
            }

            /** ring buffer */
            if (StackPointer == MAXIMUM_LOG_NUM)
            {
                LOG_Space_IsFull = 1;
                StackPointer = 0;    //init stack pointer
            }
            /** save log data */
            // 22byte(packet length) * 6(1hour : 10minutes period) * 24(1day) * 3(3day) = 9504byte = 9kbyte
            memcpy(&LogData[StackPointer++ * LOG_PACKET_LENGTH], g_packet_buff, LOG_PACKET_LENGTH);

            /* Send send-done event */
            SendMainEvent(MAIN_EVENT_TYPE_REPEAT, MAIN_REP_EVENT_SEND_DONE);
        }
    }
}

static void DeviceEventISR(uint8_t intNo)
{
    switch (intNo)
    {
        case BSP_ACC_GYRO_INT1_PIN:
            MainApp_DetectDoubleTap();
            break;
        default:
            break;
    }
}
