/****************************************************************************************
//
//
//  Copyright Lineable (C) 2018 by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author: 
//   File name : 
//   Created : 
//   Last Update :            
****************************************************************************************/


#include <bsphalconfig.h>
#include <common/source/rtos/rtos_utils_priv.h>
#include <string.h>
#include <stdio.h>
#include "em_device.h"
#include "em_gpio.h"
#include "em_core.h"
#include "em_usart.h"
#include "dmadrv.h"
#include "spidrv.h"
#include "em_assert.h"


#if 1
//#define SPI_ASSERT(expr,ret_val)   EFM_ASSERT(expr)
//#define SPI_ASSERT(expr,ret_val)   RTOS_ASSERT_DBG(expr,RTOS_ERR_INVALID_ARG,ret_val)
#define SPI_ASSERT(expr,ret_val)   RTOS_ASSERT_CRITICAL(expr,RTOS_ERR_INVALID_ARG,ret_val)
#define SPI_DEBUG(...) printf( __VA_ARGS__ )
#else
#define SPI_ASSERT(expr)    ((void)(expr))
#define SPI_DEBUG(...)
#endif




#define BSP_SPI_DMA_SUPPORT   0

#if 1// new driver 

#if 0
typedef enum {
     HW_SPI_TRANSFER_READ      = 1,
     HW_SPI_TRANSFER_WRITE     = 2,
     HW_SPI_TRANSFER_READWRITE = 3,
}HW_SPI_TRANSFER;
 
 typedef struct
 {
         SPI_Pad            cs_pad;
         hw_spi_tx_callback rx_cb;
         hw_spi_tx_callback tx_cb;
         void               *cb_data;
 
         const uint8_t      *tx_buffer;
         uint16_t           tx_len;
         uint16_t           tx_words_rem;
 
         uint8_t            *rx_buffer;
         uint16_t           rx_len;
         uint16_t           rx_words_rem;
 
         HW_SPI_TRANSFER    transfer_mode;
#ifdef BSP_SPI_DMA_SUPPORT
         uint8_t            use_dma;
         DMA_setup          tx_dma;
         DMA_setup          rx_dma;
#endif
 } SPI_Data;
 
 /* SPI data are not retained. The user must ensure that they are updated after exiting sleep. */
 static SPI_Data spi_data[2];
 
 //#define SPI_INT(id)  ((id) == USART0 ? (USART0_RX_IRQn) : (SPI2_IRQn))
#define SPIIX(id)    ((id) == USART0 ? 0 : USART1? 1: USART2? 2: USART3? 3: -1)
#define SPIDATA(id)  (&spi_data[SPIIX(id)])

 #endif

#if 0//defined(DMA_PRESENT) && (DMA_COUNT == 1)
#define SPI_DMA_IRQ   DMA_IRQn
 
#elif defined(LDMA_PRESENT) && (LDMA_COUNT == 1)
#define SPI_DMA_IRQ   LDMA_IRQn
 
#else
#error "No valid SPIDRV DMA engine defined."
#endif
 
static Ecode_t  bsp_spi_gpio_config(SPIDRV_Handle_t handle, bool enable);

 /***************************************************************************//**
  * @brief
  *    Initialize an SPI driver instance.
  *
  * @param[out] handle  Pointer to an SPI driver handle; refer to @ref
  *                     SPIDRV_Handle_t.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate
  *    SPIDRV @ref Ecode_t is returned.
  ******************************************************************************/
 uint32_t bsp_spi_init(SPIDRV_Handle_t spi)
 {
    //CORE_DECLARE_IRQ_STATE;
    USART_InitSync_TypeDef usartInit = USART_INITSYNC_DEFAULT;

    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

     if ( 0 ) {
#if defined(USART0)
     } else if ( spi->usart == USART0 ) {
         spi->usartClock  = cmuClock_USART0;
         spi->tx_dma.signal = dmadrvPeripheralSignal_USART0_TXBL;
         spi->rx_dma.signal = dmadrvPeripheralSignal_USART0_RXDATAV;
#endif
#if defined(USART1)
     } else if ( spi->usart == USART1 ) {
         spi->usartClock  = cmuClock_USART1;
         spi->tx_dma.signal = dmadrvPeripheralSignal_USART1_TXBL;
         spi->rx_dma.signal = dmadrvPeripheralSignal_USART1_RXDATAV;
#endif
#if defined(USART2)
     } else if ( spi->usart == USART2 ) {
         spi->usartClock  = cmuClock_USART2;
         spi->tx_dma.signal = dmadrvPeripheralSignal_USART2_TXBL;
         spi->rx_dma.signal = dmadrvPeripheralSignal_USART2_RXDATAV;
#endif
#if defined(USART3)
     } else if ( spi->usart == USART3 ) {
         spi->usartClock  = cmuClock_USART3;
         spi->tx_dma.signal = dmadrvPeripheralSignal_USART3_TXBL;
         spi->rx_dma.signal = dmadrvPeripheralSignal_USART3_RXDATAV;
#endif
 
     } else {
         return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
     }

     if ( spi->bitOrder == spidrvBitOrderMsbFirst ) {
        usartInit.msbf = true;
     }

    if ( spi->clockMode == spidrvClockMode0 ) {
        usartInit.clockMode = usartClockMode0;
    } else if ( spi->clockMode == spidrvClockMode1 ) {
        usartInit.clockMode = usartClockMode1;
    } else if ( spi->clockMode == spidrvClockMode2 ) {
        usartInit.clockMode = usartClockMode2;
    } else if ( spi->clockMode == spidrvClockMode3 ) {
        usartInit.clockMode = usartClockMode3;
    } else {
        return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }

    usartInit.baudrate = spi->bitRate;

    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_GPIO, true);
    CMU_ClockEnable(spi->usartClock, true);
    USART_InitSync(spi->usart, &usartInit);

#if 0

    //test
      {
      //USART_InitSync_TypeDef init = USART_INITSYNC_DEFAULT;

      //init.msbf     = true;
      //init.baudrate = 8000000;
      //USART_InitSync( BSP_EXTFLASH_SPI_PORT, &init );

      /* IO config */
       GPIO_PinModeSet( BSP_EXTFLASH_MOSI_PORT, BSP_EXTFLASH_MOSI_PIN, gpioModePushPull, 1 );
       GPIO_PinModeSet( BSP_EXTFLASH_MISO_PORT, BSP_EXTFLASH_MISO_PIN, gpioModeInput,    0 );
       GPIO_PinModeSet( BSP_EXTFLASH_CLK_PORT,  BSP_EXTFLASH_CLK_PIN,  gpioModePushPull, 1 );
       GPIO_PinModeSet( BSP_EXTFLASH_CS_PORT,   BSP_EXTFLASH_CS_PIN,   gpioModePushPull, 1 );

       BSP_EXTFLASH_SPI_PORT->ROUTELOC0 = ( (BSP_EXTFLASH_MISO_LOC << _USART_ROUTELOC0_RXLOC_SHIFT)
                               | (BSP_EXTFLASH_MOSI_LOC << _USART_ROUTELOC0_TXLOC_SHIFT)
                               | (BSP_EXTFLASH_CLK_LOC << _USART_ROUTELOC0_CLKLOC_SHIFT) );
       BSP_EXTFLASH_SPI_PORT->ROUTEPEN  = (  USART_ROUTEPEN_RXPEN
                                | USART_ROUTEPEN_TXPEN
                                | USART_ROUTEPEN_CLKPEN );


      }
#else
    if ( bsp_spi_gpio_config(spi, true) != ECODE_EMDRV_SPIDRV_OK ) {
        return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }

    if (spi->csControl == spidrvCsControlAuto ) {
        spi->usart->CTRL |= USART_CTRL_AUTOCS;
    }
 
   if ( spi->csControl == spidrvCsControlAuto ) {
     // SPI 4 wire mode
       spi->usart->ROUTEPEN = USART_ROUTEPEN_TXPEN
                                | USART_ROUTEPEN_RXPEN
                                | USART_ROUTEPEN_CLKPEN
                                | USART_ROUTEPEN_CSPEN;
 
       spi->usart->ROUTELOC0 = (spi->usart->ROUTELOC0
                                  & ~(_USART_ROUTELOC0_TXLOC_MASK
                                      | _USART_ROUTELOC0_RXLOC_MASK
                                      | _USART_ROUTELOC0_CLKLOC_MASK
                                      | _USART_ROUTELOC0_CSLOC_MASK))
                                 | (spi->portLocationTx  << _USART_ROUTELOC0_TXLOC_SHIFT)
                                 | (spi->portLocationRx  << _USART_ROUTELOC0_RXLOC_SHIFT)
                                 | (spi->portLocationClk << _USART_ROUTELOC0_CLKLOC_SHIFT)
                                 | (spi->portLocationCs  << _USART_ROUTELOC0_CSLOC_SHIFT);
   } else {
     // SPI 3 wire mode
       spi->usart->ROUTEPEN = USART_ROUTEPEN_TXPEN
                                | USART_ROUTEPEN_RXPEN
                                | USART_ROUTEPEN_CLKPEN;
 
       spi->usart->ROUTELOC0 = (spi->usart->ROUTELOC0
                                  & ~(_USART_ROUTELOC0_TXLOC_MASK
                                      | _USART_ROUTELOC0_RXLOC_MASK
                                      | _USART_ROUTELOC0_CLKLOC_MASK))
                                 | (spi->portLocationTx  << _USART_ROUTELOC0_TXLOC_SHIFT)
                                 | (spi->portLocationRx  << _USART_ROUTELOC0_RXLOC_SHIFT)
                                 | (spi->portLocationClk << _USART_ROUTELOC0_CLKLOC_SHIFT);
   }
 
#endif

 
    if(spi->use_dma){
        // Initialize DMA.
        DMADRV_Init();

        if ( DMADRV_AllocateChannel(&spi->tx_dma.ch, NULL) != ECODE_EMDRV_DMADRV_OK ) {
        return ECODE_EMDRV_SPIDRV_DMA_ALLOC_ERROR;
        }

        if ( DMADRV_AllocateChannel(&spi->rx_dma.ch, NULL) != ECODE_EMDRV_DMADRV_OK ) {
        return ECODE_EMDRV_SPIDRV_DMA_ALLOC_ERROR;
        }
    }

    return ECODE_EMDRV_SPIDRV_OK;
 }
 
 /***************************************************************************//**
  * @brief
  *    Deinitialize an SPI driver instance.
  *
  * @param[in] handle Pointer to an SPI driver handle.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate
  *    SPIDRV @ref Ecode_t is returned.
  ******************************************************************************/
 Ecode_t bsp_spi_deinit(SPIDRV_Handle_t spi)
 {
    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

    // Stop DMAs.
    DMADRV_StopTransfer(spi->rx_dma.ch);
    DMADRV_StopTransfer(spi->tx_dma.ch);

    bsp_spi_gpio_config(spi, false);

    USART_Reset(spi->usart);
    CMU_ClockEnable(spi->usartClock, false);

    DMADRV_FreeChannel(spi->tx_dma.ch);
    DMADRV_FreeChannel(spi->rx_dma.ch);
    DMADRV_DeInit();

    return ECODE_EMDRV_SPIDRV_OK;
 }


void bsp_spi_set_cs_low(SPIDRV_Handle_t spi)
{
    if(spi){
        if(spi->csControl==spidrvCsControlAuto)
            SPI_DEBUG("auto control used\r\n");
        GPIO_PinOutClear( spi->csPort, spi->csPin );
    }
}

void bsp_spi_set_cs_high(SPIDRV_Handle_t spi)
{
    if(spi){
        if(spi->csControl==spidrvCsControlAuto)
             SPI_DEBUG("auto control used\r\n");

        GPIO_PinOutSet( spi->csPort, spi->csPin );
    }
}
   
uint8_t bsp_spi_writeread(SPIDRV_Handle_t spi, uint8_t byte)
{
    uint8_t buffer[2];

    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

    bsp_spi_set_cs_low(spi);
    
    buffer[0] =  USART_SpiTransfer( spi->usart,  byte);
    buffer[1] =  USART_SpiTransfer( spi->usart,  0xFF);
    bsp_spi_set_cs_high(spi);
    return buffer[1];
}

void bsp_spi_write(SPIDRV_Handle_t spi, uint8_t *tx_buffer, uint32_t tx_len)
{  
    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

    bsp_spi_set_cs_low(spi);

    while(tx_len--){
        USART_SpiTransfer( spi->usart,  *tx_buffer++);
    }

    bsp_spi_set_cs_high(spi);
}

void bsp_spi_read(SPIDRV_Handle_t spi, uint8_t *rx_buffer, uint32_t rx_len)
{

    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

    bsp_spi_set_cs_low(spi);

    while(rx_len--){
        *rx_buffer++ = USART_SpiTransfer( spi->usart,  0xFF);
    }

    bsp_spi_set_cs_high(spi);
}

void bsp_spi_write_then_read(SPIDRV_Handle_t spi, uint8_t *tx_buffer, uint32_t tx_len, uint8_t *rx_buffer, uint32_t rx_len)
{
    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

    bsp_spi_set_cs_low(spi);

    while(tx_len--){
        USART_SpiTransfer( spi->usart,  *tx_buffer++);
    }

    while(rx_len--){
        *rx_buffer++ = USART_SpiTransfer( spi->usart,  0xFF);
    }

    bsp_spi_set_cs_high(spi);
}

 
uint32_t bsp_spi_get_bitrate(SPIDRV_Handle_t spi)
{
    SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

    return USART_BaudrateGet(spi->usart);
}

Ecode_t bsp_spi_set_bitrate(SPIDRV_Handle_t spi, uint32_t bitRate)
{
  CORE_DECLARE_IRQ_STATE;

  SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);

  CORE_ENTER_ATOMIC();
  if ( spi->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }

  spi->bitRate = bitRate;
  USART_BaudrateSyncSet(spi->usart, 0, bitRate);
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}

uint32_t bsp_spi_get_framelength(SPIDRV_Handle_t spi)
{
   SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);
   
   return spi->frameLength;
}

Ecode_t bsp_spi_set_framelength(SPIDRV_Handle_t spi, uint32_t frameLength)
{
  CORE_DECLARE_IRQ_STATE;

  SPI_ASSERT(( spi != NULL ), ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE);
  
  frameLength -= 3;
  if ( (frameLength < _USART_FRAME_DATABITS_FOUR)
       || (frameLength > _USART_FRAME_DATABITS_SIXTEEN) ) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  CORE_ENTER_ATOMIC();
  if ( spi->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }

  spi->frameLength = frameLength + 3;
  spi->usart->FRAME = (spi->usart->FRAME
                                  & ~_USART_FRAME_DATABITS_MASK)
                                 | (frameLength
                                    << _USART_FRAME_DATABITS_SHIFT);
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}


/***************************************************************************//**
 * @brief Configure/deconfigure SPI GPIO pins.
 ******************************************************************************/
static Ecode_t bsp_spi_gpio_config(SPIDRV_Handle_t spi, bool enable)
{
    int mosiPin, misoPin, clkPin;
    int mosiPort, misoPort, clkPort;

    if ( 0 ) {
#if defined(USART0)
    } else if ( spi->usart == USART0 ) {
         mosiPort       = AF_USART0_TX_PORT(spi->portLocationTx);
         misoPort       = AF_USART0_RX_PORT(spi->portLocationRx);
         clkPort        = AF_USART0_CLK_PORT(spi->portLocationClk);
         spi->csPort = AF_USART0_CS_PORT(spi->portLocationCs);
         mosiPin        = AF_USART0_TX_PIN(spi->portLocationTx);
         misoPin        = AF_USART0_RX_PIN(spi->portLocationRx);
         clkPin         = AF_USART0_CLK_PIN(spi->portLocationClk);
         spi->csPin  = AF_USART0_CS_PIN(spi->portLocationCs);
#endif
#if defined(USART1)
    } else if ( spi->usart  == USART1 ) {
         mosiPort       = AF_USART1_TX_PORT(spi->portLocationTx);
         misoPort       = AF_USART1_RX_PORT(spi->portLocationRx);
         clkPort        = AF_USART1_CLK_PORT(spi->portLocationClk);
         spi->csPort = AF_USART1_CS_PORT(spi->portLocationCs);
         mosiPin        = AF_USART1_TX_PIN(spi->portLocationTx);
         misoPin        = AF_USART1_RX_PIN(spi->portLocationRx);
         clkPin         = AF_USART1_CLK_PIN(spi->portLocationClk);
         spi->csPin  = AF_USART1_CS_PIN(spi->portLocationCs);
#endif
#if defined(USART2)
    } else if ( spi->usart  == USART2 ) {
         mosiPort       = AF_USART2_TX_PORT(spi->portLocationTx);
         misoPort       = AF_USART2_RX_PORT(spi->portLocationRx);
         clkPort        = AF_USART2_CLK_PORT(spi->portLocationClk);
         spi->csPort = AF_USART2_CS_PORT(spi->portLocationCs);
         mosiPin        = AF_USART2_TX_PIN(spi->portLocationTx);
         misoPin        = AF_USART2_RX_PIN(spi->portLocationRx);
         clkPin         = AF_USART2_CLK_PIN(spi->portLocationClk);
         spi->csPin  = AF_USART2_CS_PIN(spi->portLocationCs);
#endif
#if defined(USART3)
    } else if (spi->usart  == USART3 ) {
         mosiPort       = AF_USART3_TX_PORT(spi->portLocationTx);
         misoPort       = AF_USART3_RX_PORT(spi->portLocationRx);
         clkPort        = AF_USART3_CLK_PORT(spi->portLocationClk);
         spi->csPort = AF_USART3_CS_PORT(spi->portLocationCs);
         mosiPin        = AF_USART3_TX_PIN(spi->portLocationTx);
         misoPin        = AF_USART3_RX_PIN(spi->portLocationRx);
         clkPin         = AF_USART3_CLK_PIN(spi->portLocationClk);
         spi->csPin  = AF_USART3_CS_PIN(spi->portLocationCs);
#endif
    }else{
     return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }
 
    //SPI_DEBUG("(%d, %d), (%d, %d), (%d, %d), (%d, %d)\r\n", mosiPort, mosiPin, misoPort, misoPin, clkPort, clkPin, handle->csPort, handle->csPin  );

    if ( enable ) {

        GPIO_PinModeSet((GPIO_Port_TypeDef)mosiPort, mosiPin, gpioModePushPull, 1);
        GPIO_PinModeSet((GPIO_Port_TypeDef)misoPort, misoPin, gpioModeInput, 0);
        GPIO_PinModeSet((GPIO_Port_TypeDef)spi->csPort, spi->csPin, gpioModePushPull, 1);

        if (    (spi->clockMode == spidrvClockMode0)
            || (spi->clockMode == spidrvClockMode1) ) {
            GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModePushPull, 0);
        } else {
            GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModePushPull, 1);
        }

    } else {

        GPIO_PinModeSet((GPIO_Port_TypeDef)mosiPort, mosiPin, gpioModeInputPull, 0);
        GPIO_PinModeSet((GPIO_Port_TypeDef)misoPort, misoPin, gpioModeInputPull, 0);
        GPIO_PinModeSet((GPIO_Port_TypeDef)spi->csPort, spi->csPin, gpioModeDisabled, 0);
        GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModeInputPull, 0);
    }
 
   return ECODE_EMDRV_SPIDRV_OK;
}

static bool RxDMAComplete(unsigned int channel,
                           unsigned int sequenceNo,
                           void *userParam);
 
/***************************************************************************//**
  * @brief
  *    Abort an ongoing SPI transfer.
  *
  * @param[in] handle Pointer to an SPI driver handle.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success, @ref ECODE_EMDRV_SPIDRV_IDLE if
  *    SPI is idle. On failure, an appropriate SPIDRV @ref Ecode_t is returned.
  ******************************************************************************/
Ecode_t SPIDRV_AbortTransfer(SPIDRV_Handle_t handle)
{
    CORE_DECLARE_IRQ_STATE;

    if ( handle == NULL ) {
        return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
    }

    CORE_ENTER_ATOMIC();
    if ( handle->state == spidrvStateIdle ) {
        CORE_EXIT_ATOMIC();
        return ECODE_EMDRV_SPIDRV_IDLE;
    }


    // Stop DMA's.
    DMADRV_StopTransfer(handle->rx_dma.ch);
    DMADRV_StopTransfer(handle->tx_dma.ch);
    DMADRV_TransferRemainingCount(handle->rx_dma.ch, &handle->remaining);
    handle->transferStatus    = ECODE_EMDRV_SPIDRV_ABORTED;
    handle->state             = spidrvStateIdle;
    handle->transferStatus    = ECODE_EMDRV_SPIDRV_ABORTED;
    handle->blockingCompleted = true;

    if ( handle->userCallback != NULL ) {
        handle->userCallback(handle,
        ECODE_EMDRV_SPIDRV_ABORTED,
        handle->transferCount - handle->remaining);
    }
    CORE_EXIT_ATOMIC();

    return ECODE_EMDRV_SPIDRV_OK;
}

 
 /***************************************************************************//**
  * @brief
  *    Get the status of an SPI transfer.
  *
  * @details
  *    Returns status of an ongoing transfer. If no transfer is in progress,
  *    the status of the last transfer is reported.
  *
  * @param[in] handle Pointer to an SPI driver handle.
  *
  * @param[out] itemsTransferred Number of items (frames) transferred.
  *
  * @param[out] itemsRemaining Number of items (frames) remaining.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
  *    @ref Ecode_t is returned.
  ******************************************************************************/
 Ecode_t SPIDRV_GetTransferStatus(SPIDRV_Handle_t handle,
                                  int *itemsTransferred,
                                  int *itemsRemaining)
 {
     int remaining;
 
     if ( handle == NULL ) {
         return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
     }
 
     if ( (itemsTransferred == NULL) || (itemsRemaining == NULL) ) {
         return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
     }
 
     CORE_ATOMIC_SECTION(
         if ( handle->state == spidrvStateIdle ) {
             remaining = handle->remaining;
         } else {
             DMADRV_TransferRemainingCount(handle->rx_dma.ch, &remaining);
         }
     )
 
     *itemsTransferred = handle->transferCount - remaining;
     *itemsRemaining   = remaining;
 
     return ECODE_EMDRV_SPIDRV_OK;
 }
 
 /***************************************************************************//**
  * @brief
  *    Start an SPI master receive transfer.
  *
  * @note
  *    The MOSI wire will transmit @ref SPIDRV_Init_t.dummyTxValue.
  *
  * @param[in]  handle Pointer to an SPI driver handle.
  *
  * @param[out] buffer Receive data buffer.
  *
  * @param[in]  count Number of bytes in transfer.
  *
  * @param[in]  callback Transfer completion callback.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
  *    @ref Ecode_t is returned.
  ******************************************************************************/
 Ecode_t SPIDRV_MReceive(SPIDRV_Handle_t handle,
                         void *rxBuffer,
                         int count,
                         SPIDRV_Callback_t callback)
 {
     void *rxPort, *txPort;
     DMADRV_DataSize_t size;
 
 
     CORE_DECLARE_IRQ_STATE;
     
     if ( handle == NULL ) {
       return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
     }
     
     if ((rxBuffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
       return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
     }
     
     CORE_ENTER_ATOMIC();
     if ( handle->state != spidrvStateIdle ) {
       CORE_EXIT_ATOMIC();
       return ECODE_EMDRV_SPIDRV_BUSY;
     }
     handle->state = spidrvStateTransferring;
     CORE_EXIT_ATOMIC();
 
 
     handle->blockingCompleted  = false;
     handle->transferCount      = count;
     handle->usart->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
     handle->userCallback       = callback;
 
     if ( handle->frameLength > 8 ) {
     size = dmadrvDataSize2;
     } else {
     size = dmadrvDataSize1;
     }
 
     if ( handle->frameLength > 8 ) {
         rxPort = (void *)&(handle->usart->RXDOUBLE);
         txPort = (void *)&(handle->usart->TXDOUBLE);
     } else {
         rxPort = (void *)&(handle->usart->RXDATA);
         txPort = (void *)&(handle->usart->TXDATA);
     }
 
     // Start receive DMA.
     DMADRV_PeripheralMemory(handle->rx_dma.ch,
                 handle->rx_dma.signal,
                 (void*)rxBuffer,
                 rxPort,
                 true,
                 count,
                 size,
                 RxDMAComplete,
                 handle);
 
     // Start transmit DMA.
     DMADRV_MemoryPeripheral(handle->tx_dma.ch,
                 handle->tx_dma.signal,
                 txPort,
                 (void *)&(handle->dummyTxValue),
                 false,
                 count,
                 size,
                 NULL,
                 NULL);
 
     return ECODE_EMDRV_SPIDRV_OK;
 }
 /***************************************************************************//**
  * @brief
  *    Start an SPI master transfer.
  *
  * @param[in]  handle Pointer to an SPI driver handle.
  *
  * @param[in]  txBuffer Transmit data buffer.
  *
  * @param[out] rxBuffer Receive data buffer.
  *
  * @param[in]  count Number of bytes in transfer.
  *
  * @param[in]  callback Transfer completion callback.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
  *    @ref Ecode_t is returned.
  ******************************************************************************/
 Ecode_t SPIDRV_MTransfer(SPIDRV_Handle_t handle,
                          const void *txBuffer,
                          void *rxBuffer,
                          int count,
                          SPIDRV_Callback_t callback)
 {
   
     void *rxPort, *txPort;
     DMADRV_DataSize_t size;
 
     CORE_DECLARE_IRQ_STATE;
     
     if ( handle == NULL ) {
       return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
     }
     
     if ( (txBuffer == NULL) || (rxBuffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
       return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
     }
     
     CORE_ENTER_ATOMIC();
     if ( handle->state != spidrvStateIdle ) {
         CORE_EXIT_ATOMIC();
         return ECODE_EMDRV_SPIDRV_BUSY;
     }
     handle->state = spidrvStateTransferring;
     CORE_EXIT_ATOMIC();
 
   
     handle->blockingCompleted  = false;
     handle->transferCount      = count;
     handle->usart->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
     handle->userCallback       = callback;
   
     if ( handle->frameLength > 8 ) {
       size = dmadrvDataSize2;
     } else {
       size = dmadrvDataSize1;
     }
   
     if ( handle->frameLength > 8 ) {
         rxPort = (void *)&(handle->usart->RXDOUBLE);
         txPort = (void *)&(handle->usart->TXDOUBLE);
     } else {
         rxPort = (void *)&(handle->usart->RXDATA);
         txPort = (void *)&(handle->usart->TXDATA);
     }
   
     // Start receive DMA.
     DMADRV_PeripheralMemory(handle->rx_dma.ch,
                             handle->rx_dma.signal,
                             rxBuffer,
                             rxPort,
                             true,
                             count,
                             size,
                             RxDMAComplete,
                             handle);
   
     // Start transmit DMA.
     DMADRV_MemoryPeripheral(handle->tx_dma.ch,
                             handle->tx_dma.signal,
                             txPort,
                             (void*)txBuffer,
                             true,
                             count,
                             size,
                             NULL,
                             NULL);
   
 
 
     return ECODE_EMDRV_SPIDRV_OK;
 }
 
 /***************************************************************************//**
  * @brief
  *    Start an SPI master transmit transfer.
  *
  * @note
  *    The data received on the MISO wire is discarded.
  *
  * @param[in] handle Pointer to an SPI driver handle.
  *
  * @param[in] buffer Transmit data buffer.
  *
  * @param[in] count Number of bytes in transfer.
  *
  * @param[in] callback Transfer completion callback.
  *
  * @return
  *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
  *    @ref Ecode_t is returned.
  ******************************************************************************/
 Ecode_t SPIDRV_MTransmit(SPIDRV_Handle_t handle,
                          const void *txBuffer,
                          int count,
                          SPIDRV_Callback_t callback)
 {
 
     void *rxPort, *txPort;
     DMADRV_DataSize_t size;
 
 
     CORE_DECLARE_IRQ_STATE;
     
     if ( handle == NULL ) {
       return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
     }
     
     if ( (txBuffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
       return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
     }
     
     CORE_ENTER_ATOMIC();
     if ( handle->state != spidrvStateIdle ) {
       CORE_EXIT_ATOMIC();
       return ECODE_EMDRV_SPIDRV_BUSY;
     }
     handle->state = spidrvStateTransferring;
     CORE_EXIT_ATOMIC();
 
 
     handle->blockingCompleted  = false;
     handle->transferCount      = count;
     handle->usart->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
     handle->userCallback       = callback;
 
     if ( handle->frameLength > 8 ) {
       size = dmadrvDataSize2;
     } else {
       size = dmadrvDataSize1;
     }
 
     if ( handle->frameLength > 8 ) {
       rxPort = (void *)&(handle->usart->RXDOUBLE);
       txPort = (void *)&(handle->usart->TXDOUBLE);
     } else {
       rxPort = (void *)&(handle->usart->RXDATA);
       txPort = (void *)&(handle->usart->TXDATA);
     }
 
     // Receive DMA runs only to get precise numbers for SPIDRV_GetTransferStatus()
     // Start receive DMA.
     DMADRV_PeripheralMemory(handle->rx_dma.ch,
                             handle->rx_dma.signal,
                             &(handle->dummyRx),
                             rxPort,
                             false,
                             count,
                             size,
                             RxDMAComplete,
                             handle);
 
     // Start transmit DMA.
     DMADRV_MemoryPeripheral(handle->tx_dma.ch,
                             handle->tx_dma.signal,
                             txPort,
                             (void*)txBuffer,
                             true,
                             count,
                             size,
                             NULL,
                             NULL);
 
 
     return ECODE_EMDRV_SPIDRV_OK;
 }
 


 
 
 /***************************************************************************//**
  * @brief DMA transfer completion callback. Called by the DMA interrupt handler.
  ******************************************************************************/
 static bool RxDMAComplete(unsigned int channel,
                           unsigned int sequenceNo,
                           void *userParam)
 {
    CORE_DECLARE_IRQ_STATE;
    SPIDRV_Handle_t handle;
    (void)channel;
    (void)sequenceNo;

    CORE_ENTER_ATOMIC();

    handle = (SPIDRV_Handle_t)userParam;

    handle->transferStatus = ECODE_EMDRV_SPIDRV_OK;
    handle->state          = spidrvStateIdle;
    handle->remaining      = 0;


    if ( handle->userCallback != NULL ) {
        handle->userCallback(handle, ECODE_EMDRV_SPIDRV_OK, handle->transferCount);
    }

    CORE_EXIT_ATOMIC();
    return true;
 }

#else // old driver

#if 0//defined(DMA_PRESENT) && (DMA_COUNT == 1)
#define SPI_DMA_IRQ   DMA_IRQn

#elif defined(LDMA_PRESENT) && (LDMA_COUNT == 1)
#define SPI_DMA_IRQ   LDMA_IRQn

#else
#error "No valid SPIDRV DMA engine defined."
#endif

static bool     spidrvIsInitialized = false;

#if 0//Brandon
static void     BlockingComplete(SPIDRV_Handle_t handle,
                                 Ecode_t transferStatus,
                                 int itemsTransferred);
#endif
static Ecode_t  ConfigGPIO(SPIDRV_Handle_t handle, bool enable);
#if 0//Brandon
static bool     RxDMAComplete(unsigned int channel,
                              unsigned int sequenceNo,
                              void *userParam);
#endif

static bool RxDMAComplete(unsigned int channel,
                          unsigned int sequenceNo,
                          void *userParam);

#if 0// moved upper function
static void     StartReceiveDMA(SPIDRV_Handle_t handle,
                                void *buffer,
                                int count,
                                SPIDRV_Callback_t callback);


static void     StartTransferDMA(SPIDRV_Handle_t handle,
                                 const void *txBuffer,
                                 void *rxBuffer,
                                 int count,
                                 SPIDRV_Callback_t callback);

static void     StartTransmitDMA(SPIDRV_Handle_t handle,
                                 const void *buffer,
                                 int count,
                                 SPIDRV_Callback_t callback);

static Ecode_t  TransferApiPrologue(SPIDRV_Handle_t handle,
                                    void *buffer,
                                    int count);
#endif

#if 0//Brandon

static Ecode_t  TransferApiBlockingPrologue(SPIDRV_Handle_t handle,
                                            void *buffer,
                                            int count);

static void     WaitForTransferCompletion(SPIDRV_Handle_t handle);
#endif

/// @endcond

/***************************************************************************//**
 * @brief
 *    Initialize an SPI driver instance.
 *
 * @param[out] handle  Pointer to an SPI driver handle; refer to @ref
 *                     SPIDRV_Handle_t.
 *
 * @param[in] initData Pointer to an initialization data structure;
 *                     refer to @ref SPIDRV_Init_t.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate
 *    SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_Init(SPIDRV_Handle_t handle, SPIDRV_Init_t *initData)
{
    CORE_DECLARE_IRQ_STATE;
    USART_TypeDef *port;   

    USART_InitSync_TypeDef usartInit =     {                                                   \
      usartEnable,     /* Enable RX/TX when initialization is complete. */                     \
      0,               /* Use current configured reference clock for configuring baud rate. */ \
      1000000,         /* 1 Mbits/s. */                                                        \
      usartDatabits8,  /* 8 data bits. */                                                      \
      false,           /* Send least significant bit first. */                                 \
      usartClockMode0, /* Clock idle low, sample on rising edge. */                            \
      false,           /* Not USART PRS input mode. */                                         \
      usartPrsRxCh0,   /* PRS channel 0. */                                                    \
      false,           /* No AUTOTX mode. */                                                   \
      false,           /* No AUTOCS mode */                                                    \
      0,               /* Auto CS Hold cycles */                                               \
      0                /* Auto CS Setup cycles */                                              \
    };


    if ( handle == NULL ) {
        return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
    }

    if ( initData == NULL ) {
        return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }

    memset(handle, 0, sizeof(SPIDRV_HandleData_t));

    port = (USART_TypeDef *)initData->port;

    if ( 0 ) {
#if defined(USART0)
    } else if ( port == USART0 ) {
        handle->usartClock  = cmuClock_USART0;
        handle->txDMASignal = dmadrvPeripheralSignal_USART0_TXBL;
        handle->rxDMASignal = dmadrvPeripheralSignal_USART0_RXDATAV;
#endif
#if defined(USART1)
    } else if ( port == USART1 ) {
        handle->usartClock  = cmuClock_USART1;
        handle->txDMASignal = dmadrvPeripheralSignal_USART1_TXBL;
        handle->rxDMASignal = dmadrvPeripheralSignal_USART1_RXDATAV;
#endif
#if defined(USART2)
    } else if ( port == USART2 ) {
        handle->usartClock  = cmuClock_USART2;
        handle->txDMASignal = dmadrvPeripheralSignal_USART2_TXBL;
        handle->rxDMASignal = dmadrvPeripheralSignal_USART2_RXDATAV;
#endif
#if defined(USART3)
    } else if ( port == USART3 ) {
        handle->usartClock  = cmuClock_USART3;
        handle->txDMASignal = dmadrvPeripheralSignal_USART3_TXBL;
        handle->rxDMASignal = dmadrvPeripheralSignal_USART3_RXDATAV;
#endif

    } else {
        return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }

    handle->cfg = *initData;

    if ( initData->bitOrder == spidrvBitOrderMsbFirst ) {
    usartInit.msbf = true;
    }

  if ( initData->clockMode == spidrvClockMode0 ) {
    usartInit.clockMode = usartClockMode0;
  } else if ( initData->clockMode == spidrvClockMode1 ) {
    usartInit.clockMode = usartClockMode1;
  } else if ( initData->clockMode == spidrvClockMode2 ) {
    usartInit.clockMode = usartClockMode2;
  } else if ( initData->clockMode == spidrvClockMode3 ) {
    usartInit.clockMode = usartClockMode3;
  } else {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

    usartInit.baudrate = initData->bitRate;

  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(handle->usartClock, true);
  USART_InitSync(port, &usartInit);

  if ( initData->csControl == spidrvCsControlAuto ) {
    port->CTRL |= USART_CTRL_AUTOCS;
  }

  if ( initData->csControl == spidrvCsControlAuto ) {
    // SPI 4 wire mode
#if defined(USART_ROUTEPEN_TXPEN)
    port->ROUTEPEN = USART_ROUTEPEN_TXPEN
                               | USART_ROUTEPEN_RXPEN
                               | USART_ROUTEPEN_CLKPEN
                               | USART_ROUTEPEN_CSPEN;

    port->ROUTELOC0 = (port->ROUTELOC0
                                 & ~(_USART_ROUTELOC0_TXLOC_MASK
                                     | _USART_ROUTELOC0_RXLOC_MASK
                                     | _USART_ROUTELOC0_CLKLOC_MASK
                                     | _USART_ROUTELOC0_CSLOC_MASK))
                                | (initData->portLocationTx  << _USART_ROUTELOC0_TXLOC_SHIFT)
                                | (initData->portLocationRx  << _USART_ROUTELOC0_RXLOC_SHIFT)
                                | (initData->portLocationClk << _USART_ROUTELOC0_CLKLOC_SHIFT)
                                | (initData->portLocationCs  << _USART_ROUTELOC0_CSLOC_SHIFT);
#else
    port->ROUTE = USART_ROUTE_TXPEN
                            | USART_ROUTE_RXPEN
                            | USART_ROUTE_CLKPEN
                            | USART_ROUTE_CSPEN
                            | (initData->portLocation
                               << _USART_ROUTE_LOCATION_SHIFT);
#endif
  } else {
    // SPI 3 wire mode
#if defined(USART_ROUTEPEN_TXPEN)
    port->ROUTEPEN = USART_ROUTEPEN_TXPEN
                               | USART_ROUTEPEN_RXPEN
                               | USART_ROUTEPEN_CLKPEN;

    port->ROUTELOC0 = (port->ROUTELOC0
                                 & ~(_USART_ROUTELOC0_TXLOC_MASK
                                     | _USART_ROUTELOC0_RXLOC_MASK
                                     | _USART_ROUTELOC0_CLKLOC_MASK))
                                | (initData->portLocationTx  << _USART_ROUTELOC0_TXLOC_SHIFT)
                                | (initData->portLocationRx  << _USART_ROUTELOC0_RXLOC_SHIFT)
                                | (initData->portLocationClk << _USART_ROUTELOC0_CLKLOC_SHIFT);
#else
    port->ROUTE = USART_ROUTE_TXPEN
                            | USART_ROUTE_RXPEN
                            | USART_ROUTE_CLKPEN
                            | (initData->portLocation
                               << _USART_ROUTE_LOCATION_SHIFT);
#endif
  }

  if ( ConfigGPIO(handle, true) != ECODE_EMDRV_SPIDRV_OK ) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  CORE_ENTER_ATOMIC();
  if ( !spidrvIsInitialized ) {
    spidrvIsInitialized = true;
    CORE_EXIT_ATOMIC();

  } else {
    CORE_EXIT_ATOMIC();
  }


  // Initialize DMA.
  DMADRV_Init();

  if ( DMADRV_AllocateChannel(&handle->txDMACh, NULL) != ECODE_EMDRV_DMADRV_OK ) {
    return ECODE_EMDRV_SPIDRV_DMA_ALLOC_ERROR;
  }

  if ( DMADRV_AllocateChannel(&handle->rxDMACh, NULL) != ECODE_EMDRV_DMADRV_OK ) {
    return ECODE_EMDRV_SPIDRV_DMA_ALLOC_ERROR;
  }

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Deinitialize an SPI driver instance.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate
 *    SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_DeInit(SPIDRV_Handle_t handle)
{
  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  // Stop DMAs.
  DMADRV_StopTransfer(handle->rxDMACh);
  DMADRV_StopTransfer(handle->txDMACh);

  ConfigGPIO(handle, false);

  USART_Reset(handle->cfg.port);
  CMU_ClockEnable(handle->usartClock, false);

  DMADRV_FreeChannel(handle->txDMACh);
  DMADRV_FreeChannel(handle->rxDMACh);
  DMADRV_DeInit();

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Abort an ongoing SPI transfer.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success, @ref ECODE_EMDRV_SPIDRV_IDLE if
 *    SPI is idle. On failure, an appropriate SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_AbortTransfer(SPIDRV_Handle_t handle)
{
  CORE_DECLARE_IRQ_STATE;

  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  CORE_ENTER_ATOMIC();
  if ( handle->state == spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_IDLE;
  }


  // Stop DMA's.
  DMADRV_StopTransfer(handle->rxDMACh);
  DMADRV_StopTransfer(handle->txDMACh);
  DMADRV_TransferRemainingCount(handle->rxDMACh, &handle->remaining);
  handle->transferStatus    = ECODE_EMDRV_SPIDRV_ABORTED;
  handle->state             = spidrvStateIdle;
  handle->transferStatus    = ECODE_EMDRV_SPIDRV_ABORTED;
  handle->blockingCompleted = true;

  if ( handle->userCallback != NULL ) {
    handle->userCallback(handle,
                         ECODE_EMDRV_SPIDRV_ABORTED,
                         handle->transferCount - handle->remaining);
  }
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Get current SPI bus bitrate.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[out] bitRate Current SPI bus bitrate.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_GetBitrate(SPIDRV_Handle_t handle, uint32_t *bitRate)
{
  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  if ( bitRate == NULL ) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  *bitRate = USART_BaudrateGet(handle->cfg.port);

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Get current SPI framelength.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[out] frameLength Current SPI bus framelength.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_GetFramelength(SPIDRV_Handle_t handle, uint32_t *frameLength)
{
  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  if ( frameLength == NULL ) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  *frameLength = handle->cfg.frameLength;

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Get the status of an SPI transfer.
 *
 * @details
 *    Returns status of an ongoing transfer. If no transfer is in progress,
 *    the status of the last transfer is reported.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[out] itemsTransferred Number of items (frames) transferred.
 *
 * @param[out] itemsRemaining Number of items (frames) remaining.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_GetTransferStatus(SPIDRV_Handle_t handle,
                                 int *itemsTransferred,
                                 int *itemsRemaining)
{
    int remaining;

    if ( handle == NULL ) {
        return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
    }

    if ( (itemsTransferred == NULL) || (itemsRemaining == NULL) ) {
        return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }

    CORE_ATOMIC_SECTION(
        if ( handle->state == spidrvStateIdle ) {
            remaining = handle->remaining;
        } else {
            DMADRV_TransferRemainingCount(handle->rxDMACh, &remaining);
        }
    )

    *itemsTransferred = handle->transferCount - remaining;
    *itemsRemaining   = remaining;

    return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Start an SPI master receive transfer.
 *
 * @note
 *    The MOSI wire will transmit @ref SPIDRV_Init_t.dummyTxValue.
 *
 * @param[in]  handle Pointer to an SPI driver handle.
 *
 * @param[out] buffer Receive data buffer.
 *
 * @param[in]  count Number of bytes in transfer.
 *
 * @param[in]  callback Transfer completion callback.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MReceive(SPIDRV_Handle_t handle,
                        void *rxBuffer,
                        int count,
                        SPIDRV_Callback_t callback)
{
    void *rxPort, *txPort;
    DMADRV_DataSize_t size;


    CORE_DECLARE_IRQ_STATE;
    
    if ( handle == NULL ) {
      return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
    }
    
    if ((rxBuffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
      return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }
    
    CORE_ENTER_ATOMIC();
    if ( handle->state != spidrvStateIdle ) {
      CORE_EXIT_ATOMIC();
      return ECODE_EMDRV_SPIDRV_BUSY;
    }
    handle->state = spidrvStateTransferring;
    CORE_EXIT_ATOMIC();


    handle->blockingCompleted  = false;
    handle->transferCount      = count;
    handle->cfg.port->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
    handle->userCallback       = callback;

    if ( handle->cfg.frameLength > 8 ) {
    size = dmadrvDataSize2;
    } else {
    size = dmadrvDataSize1;
    }

    if ( handle->cfg.frameLength > 8 ) {
        rxPort = (void *)&(handle->cfg.port->RXDOUBLE);
        txPort = (void *)&(handle->cfg.port->TXDOUBLE);
    } else {
        rxPort = (void *)&(handle->cfg.port->RXDATA);
        txPort = (void *)&(handle->cfg.port->TXDATA);
    }

    // Start receive DMA.
    DMADRV_PeripheralMemory(handle->rxDMACh,
                handle->rxDMASignal,
                (void*)rxBuffer,
                rxPort,
                true,
                count,
                size,
                RxDMAComplete,
                handle);

    // Start transmit DMA.
    DMADRV_MemoryPeripheral(handle->txDMACh,
                handle->txDMASignal,
                txPort,
                (void *)&(handle->cfg.dummyTxValue),
                false,
                count,
                size,
                NULL,
                NULL);

    return ECODE_EMDRV_SPIDRV_OK;
}
#if 0//Brandon

/***************************************************************************//**
 * @brief
 *    Start an SPI master blocking receive transfer.
 *
 * @note
 *    The MOSI wire will transmit @ref SPIDRV_Init_t.dummyTxValue.
 *    @n This function is blocking and returns when the transfer is complete
 *    or when @ref SPIDRV_AbortTransfer() is called.
 *
 * @param[in]  handle Pointer to an SPI driver handle.
 *
 * @param[out] buffer Receive data buffer.
 *
 * @param[in]  count Number of bytes in transfer.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success or @ref ECODE_EMDRV_SPIDRV_ABORTED
 *    if @ref SPIDRV_AbortTransfer() has been called. On failure, an appropriate
 *    SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MReceiveB(SPIDRV_Handle_t handle,
                         void *buffer,
                         int count)
{
  Ecode_t retVal;


  if ( (retVal = TransferApiBlockingPrologue(handle, buffer, count))
       != ECODE_EMDRV_SPIDRV_OK ) {
    return retVal;
  }

  StartReceiveDMA(handle, buffer, count, BlockingComplete);

  WaitForTransferCompletion(handle);

  return handle->transferStatus;
}
#endif
/***************************************************************************//**
 * @brief
 *    Start an SPI master transfer.
 *
 * @param[in]  handle Pointer to an SPI driver handle.
 *
 * @param[in]  txBuffer Transmit data buffer.
 *
 * @param[out] rxBuffer Receive data buffer.
 *
 * @param[in]  count Number of bytes in transfer.
 *
 * @param[in]  callback Transfer completion callback.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MTransfer(SPIDRV_Handle_t handle,
                         const void *txBuffer,
                         void *rxBuffer,
                         int count,
                         SPIDRV_Callback_t callback)
{
  
    void *rxPort, *txPort;
    DMADRV_DataSize_t size;

    CORE_DECLARE_IRQ_STATE;
    
    if ( handle == NULL ) {
      return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
    }
    
    if ( (txBuffer == NULL) || (rxBuffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
      return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }
    
    CORE_ENTER_ATOMIC();
    if ( handle->state != spidrvStateIdle ) {
        CORE_EXIT_ATOMIC();
        return ECODE_EMDRV_SPIDRV_BUSY;
    }
    handle->state = spidrvStateTransferring;
    CORE_EXIT_ATOMIC();

  
    handle->blockingCompleted  = false;
    handle->transferCount      = count;
    handle->cfg.port->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
    handle->userCallback       = callback;
  
    if ( handle->cfg.frameLength > 8 ) {
      size = dmadrvDataSize2;
    } else {
      size = dmadrvDataSize1;
    }
  
    if ( handle->cfg.frameLength > 8 ) {
        rxPort = (void *)&(handle->cfg.port->RXDOUBLE);
        txPort = (void *)&(handle->cfg.port->TXDOUBLE);
    } else {
        rxPort = (void *)&(handle->cfg.port->RXDATA);
        txPort = (void *)&(handle->cfg.port->TXDATA);
    }
  
    // Start receive DMA.
    DMADRV_PeripheralMemory(handle->rxDMACh,
                            handle->rxDMASignal,
                            rxBuffer,
                            rxPort,
                            true,
                            count,
                            size,
                            RxDMAComplete,
                            handle);
  
    // Start transmit DMA.
    DMADRV_MemoryPeripheral(handle->txDMACh,
                            handle->txDMASignal,
                            txPort,
                            (void*)txBuffer,
                            true,
                            count,
                            size,
                            NULL,
                            NULL);
  


    return ECODE_EMDRV_SPIDRV_OK;
}

#if 0//Brandon

/***************************************************************************//**
 * @brief
 *    Start an SPI master blocking transfer.
 *
 * @note
 *    This function is blocking and returns when the transfer is complete
 *    or when @ref SPIDRV_AbortTransfer() is called.
 *
 * @param[in]  handle Pointer to an SPI driver handle.
 *
 * @param[in]  txBuffer Transmit data buffer.
 *
 * @param[out] rxBuffer Receive data buffer.
 *
 * @param[in]  count Number of bytes in transfer.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success or @ref ECODE_EMDRV_SPIDRV_ABORTED
 *    if @ref SPIDRV_AbortTransfer() has been called. On failure, an appropriate
 *    SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MTransferB(SPIDRV_Handle_t handle,
                          const void *txBuffer,
                          void *rxBuffer,
                          int count)
{
  Ecode_t retVal;


  if ( (retVal = TransferApiBlockingPrologue(handle, (void*)txBuffer, count))
       != ECODE_EMDRV_SPIDRV_OK ) {
    return retVal;
  }

  if ( rxBuffer == NULL ) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  StartTransferDMA(handle, txBuffer, rxBuffer, count, BlockingComplete);

  WaitForTransferCompletion(handle);

  return handle->transferStatus;
}


/***************************************************************************//**
 * @brief
 *    Start an SPI master blocking single item (frame) transfer.
 *
 * @note
 *    This function is blocking and returns when the transfer is complete
 *    or when @ref SPIDRV_AbortTransfer() is called.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[in] txValue Value to transmit.
 *
 * @param[out] rxValue Value received.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success or @ref ECODE_EMDRV_SPIDRV_ABORTED
 *    if @ref SPIDRV_AbortTransfer() has been called. On failure, an appropriate
 *    SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MTransferSingleItemB(SPIDRV_Handle_t handle,
                                    uint32_t txValue,
                                    void *rxValue)
{
  void *pRx;
  CORE_DECLARE_IRQ_STATE;
  uint32_t rxBuffer;

  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }


  CORE_ENTER_ATOMIC();
  if ( handle->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }
  handle->state = spidrvStateTransferring;
  CORE_EXIT_ATOMIC();

  if ( (pRx = rxValue) == NULL ) {
    pRx = &rxBuffer;
  }

  StartTransferDMA(handle, &txValue, pRx, 1, BlockingComplete);

  WaitForTransferCompletion(handle);

  return handle->transferStatus;
}
#endif
/***************************************************************************//**
 * @brief
 *    Start an SPI master transmit transfer.
 *
 * @note
 *    The data received on the MISO wire is discarded.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[in] buffer Transmit data buffer.
 *
 * @param[in] count Number of bytes in transfer.
 *
 * @param[in] callback Transfer completion callback.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MTransmit(SPIDRV_Handle_t handle,
                         const void *txBuffer,
                         int count,
                         SPIDRV_Callback_t callback)
{

    void *rxPort, *txPort;
    DMADRV_DataSize_t size;


    CORE_DECLARE_IRQ_STATE;
    
    if ( handle == NULL ) {
      return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
    }
    
    if ( (txBuffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
      return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
    }
    
    CORE_ENTER_ATOMIC();
    if ( handle->state != spidrvStateIdle ) {
      CORE_EXIT_ATOMIC();
      return ECODE_EMDRV_SPIDRV_BUSY;
    }
    handle->state = spidrvStateTransferring;
    CORE_EXIT_ATOMIC();


    handle->blockingCompleted  = false;
    handle->transferCount      = count;
    handle->cfg.port->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
    handle->userCallback       = callback;

    if ( handle->cfg.frameLength > 8 ) {
      size = dmadrvDataSize2;
    } else {
      size = dmadrvDataSize1;
    }

    if ( handle->cfg.frameLength > 8 ) {
      rxPort = (void *)&(handle->cfg.port->RXDOUBLE);
      txPort = (void *)&(handle->cfg.port->TXDOUBLE);
    } else {
      rxPort = (void *)&(handle->cfg.port->RXDATA);
      txPort = (void *)&(handle->cfg.port->TXDATA);
    }

    // Receive DMA runs only to get precise numbers for SPIDRV_GetTransferStatus()
    // Start receive DMA.
    DMADRV_PeripheralMemory(handle->rxDMACh,
                            handle->rxDMASignal,
                            &(handle->dummyRx),
                            rxPort,
                            false,
                            count,
                            size,
                            RxDMAComplete,
                            handle);

    // Start transmit DMA.
    DMADRV_MemoryPeripheral(handle->txDMACh,
                            handle->txDMASignal,
                            txPort,
                            (void*)txBuffer,
                            true,
                            count,
                            size,
                            NULL,
                            NULL);


    return ECODE_EMDRV_SPIDRV_OK;
}

#if 0//Brandon

/***************************************************************************//**
 * @brief
 *    Start an SPI master blocking transmit transfer.
 *
 * @note
 *    The data received on the MISO wire is discarded.
 *    @n This function is blocking and returns when the transfer is complete.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[in] buffer Transmit data buffer.
 *
 * @param[in] count Number of bytes in transfer.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success or @ref ECODE_EMDRV_SPIDRV_ABORTED
 *    if @ref SPIDRV_AbortTransfer() has been called. On failure, an appropriate
 *    SPIDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_MTransmitB(SPIDRV_Handle_t handle,
                          const void *buffer,
                          int count)
{
  Ecode_t retVal;


  if ( (retVal = TransferApiBlockingPrologue(handle, (void*)buffer, count))
       != ECODE_EMDRV_SPIDRV_OK ) {
    return retVal;
  }

  StartTransmitDMA(handle, buffer, count, BlockingComplete);

  WaitForTransferCompletion(handle);

  return handle->transferStatus;
}
#endif
/***************************************************************************//**
 * @brief
 *    Set SPI bus bitrate.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[in] bitRate New SPI bus bitrate.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_SetBitrate(SPIDRV_Handle_t handle, uint32_t bitRate)
{
  CORE_DECLARE_IRQ_STATE;

  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  CORE_ENTER_ATOMIC();
  if ( handle->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }

  handle->cfg.bitRate = bitRate;
  USART_BaudrateSyncSet(handle->cfg.port, 0, bitRate);
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Set SPI framelength.
 *
 * @param[in] handle Pointer to an SPI driver handle.
 *
 * @param[in] frameLength New SPI bus framelength.
 *
 * @return
 *    @ref ECODE_EMDRV_SPIDRV_OK on success. On failure, an appropriate SPIDRV
 *    @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t SPIDRV_SetFramelength(SPIDRV_Handle_t handle, uint32_t frameLength)
{
  CORE_DECLARE_IRQ_STATE;

  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  frameLength -= 3;
  if ( (frameLength < _USART_FRAME_DATABITS_FOUR)
       || (frameLength > _USART_FRAME_DATABITS_SIXTEEN) ) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  CORE_ENTER_ATOMIC();
  if ( handle->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }

  handle->cfg.frameLength = frameLength + 3;
  handle->cfg.port->FRAME = (handle->cfg.port->FRAME
                                  & ~_USART_FRAME_DATABITS_MASK)
                                 | (frameLength
                                    << _USART_FRAME_DATABITS_SHIFT);
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}

/// @cond DO_NOT_INCLUDE_WITH_DOXYGEN
#if 0//Brandon

/***************************************************************************//**
 * @brief
 *    Transfer complete callback function used by blocking transfer API
 *    functions. Called by DMA interrupt handler, timer timeout handler
 *    or @ref SPIDRV_AbortTransfer() function.
 ******************************************************************************/
static void BlockingComplete(SPIDRV_Handle_t handle,
                             Ecode_t transferStatus,
                             int itemsTransferred)
{
  (void)itemsTransferred;

  handle->transferStatus    = transferStatus;
  handle->blockingCompleted = true;
}
#endif
/***************************************************************************//**
 * @brief Configure/deconfigure SPI GPIO pins.
 ******************************************************************************/
static Ecode_t ConfigGPIO(SPIDRV_Handle_t handle, bool enable)
{
  SPIDRV_Init_t *initData;

  int mosiPin, misoPin, clkPin;
  int mosiPort, misoPort, clkPort;

  initData = &handle->cfg;

  if ( 0 ) {
#if defined(USART0)
  } else if ( handle->cfg.port == USART0 ) {
    mosiPort       = AF_USART0_TX_PORT(initData->portLocationTx);
    misoPort       = AF_USART0_RX_PORT(initData->portLocationRx);
    clkPort        = AF_USART0_CLK_PORT(initData->portLocationClk);
    handle->csPort = AF_USART0_CS_PORT(initData->portLocationCs);
    mosiPin        = AF_USART0_TX_PIN(initData->portLocationTx);
    misoPin        = AF_USART0_RX_PIN(initData->portLocationRx);
    clkPin         = AF_USART0_CLK_PIN(initData->portLocationClk);
    handle->csPin  = AF_USART0_CS_PIN(initData->portLocationCs);
#endif
#if defined(USART1)
  } else if ( handle->cfg.port == USART1 ) {
    mosiPort       = AF_USART1_TX_PORT(initData->portLocationTx);
    misoPort       = AF_USART1_RX_PORT(initData->portLocationRx);
    clkPort        = AF_USART1_CLK_PORT(initData->portLocationClk);
    handle->csPort = AF_USART1_CS_PORT(initData->portLocationCs);
    mosiPin        = AF_USART1_TX_PIN(initData->portLocationTx);
    misoPin        = AF_USART1_RX_PIN(initData->portLocationRx);
    clkPin         = AF_USART1_CLK_PIN(initData->portLocationClk);
    handle->csPin  = AF_USART1_CS_PIN(initData->portLocationCs);
#endif
#if defined(USART2)
  } else if ( handle->cfg.port == USART2 ) {
    mosiPort       = AF_USART2_TX_PORT(initData->portLocationTx);
    misoPort       = AF_USART2_RX_PORT(initData->portLocationRx);
    clkPort        = AF_USART2_CLK_PORT(initData->portLocationClk);
    handle->csPort = AF_USART2_CS_PORT(initData->portLocationCs);
    mosiPin        = AF_USART2_TX_PIN(initData->portLocationTx);
    misoPin        = AF_USART2_RX_PIN(initData->portLocationRx);
    clkPin         = AF_USART2_CLK_PIN(initData->portLocationClk);
    handle->csPin  = AF_USART2_CS_PIN(initData->portLocationCs);
#endif
#if defined(USART3)
  } else if ( handle->cfg.port == USART3 ) {
    mosiPort       = AF_USART3_TX_PORT(initData->portLocationTx);
    misoPort       = AF_USART3_RX_PORT(initData->portLocationRx);
    clkPort        = AF_USART3_CLK_PORT(initData->portLocationClk);
    handle->csPort = AF_USART3_CS_PORT(initData->portLocationCs);
    mosiPin        = AF_USART3_TX_PIN(initData->portLocationTx);
    misoPin        = AF_USART3_RX_PIN(initData->portLocationRx);
    clkPin         = AF_USART3_CLK_PIN(initData->portLocationClk);
    handle->csPin  = AF_USART3_CS_PIN(initData->portLocationCs);
#endif
  } else {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }


    if ( enable ) {
        if(mosiPort >= 0) // consider 3 wire mode
        GPIO_PinModeSet((GPIO_Port_TypeDef)mosiPort, mosiPin, gpioModePushPull, 0);

        GPIO_PinModeSet((GPIO_Port_TypeDef)misoPort, misoPin, gpioModeInputPull, 0);

        if (    (handle->cfg.clockMode == spidrvClockMode0)
            || (handle->cfg.clockMode == spidrvClockMode1) ) {
            GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModePushPull, 0);
        } else {
            GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModePushPull, 1);
        }

        if ( handle->cfg.csControl == spidrvCsControlAuto ) {
            GPIO_PinModeSet((GPIO_Port_TypeDef)handle->csPort, handle->csPin, gpioModePushPull, 1);
        }

    } else {
    
        if(mosiPort >= 0) // consider 3 wire mode
        GPIO_PinModeSet((GPIO_Port_TypeDef)mosiPort, mosiPin, gpioModeInputPull, 0);

        GPIO_PinModeSet((GPIO_Port_TypeDef)misoPort, misoPin, gpioModeInputPull, 0);

        if (    (handle->cfg.clockMode == spidrvClockMode0)
            || (handle->cfg.clockMode == spidrvClockMode1) ) {
            GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModeInputPull, 0);
        } else {
            GPIO_PinModeSet((GPIO_Port_TypeDef)clkPort, clkPin, gpioModeInputPull, 1);
        }

        if ( handle->cfg.csControl == spidrvCsControlAuto ) {
            GPIO_PinModeSet((GPIO_Port_TypeDef)handle->csPort, handle->csPin, gpioModeDisabled, 0);
        }
    }

  return ECODE_EMDRV_SPIDRV_OK;
}


/***************************************************************************//**
 * @brief DMA transfer completion callback. Called by the DMA interrupt handler.
 ******************************************************************************/
static bool RxDMAComplete(unsigned int channel,
                          unsigned int sequenceNo,
                          void *userParam)
{
  CORE_DECLARE_IRQ_STATE;
  SPIDRV_Handle_t handle;
  (void)channel;
  (void)sequenceNo;

  CORE_ENTER_ATOMIC();

  handle = (SPIDRV_Handle_t)userParam;

  handle->transferStatus = ECODE_EMDRV_SPIDRV_OK;
  handle->state          = spidrvStateIdle;
  handle->remaining      = 0;


  if ( handle->userCallback != NULL ) {
    handle->userCallback(handle, ECODE_EMDRV_SPIDRV_OK, handle->transferCount);
  }

  CORE_EXIT_ATOMIC();
  return true;
}

#if 0// moved upper function
/***************************************************************************//**
 * @brief Start an SPI receive DMA.
 ******************************************************************************/
static void StartReceiveDMA(SPIDRV_Handle_t handle,
                            void *buffer,
                            int count,
                            SPIDRV_Callback_t callback)
{
  void *rxPort, *txPort;
  DMADRV_DataSize_t size;

  handle->blockingCompleted  = false;
  handle->transferCount      = count;
  handle->cfg.port->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
  handle->userCallback       = callback;

  if ( handle->cfg.frameLength > 8 ) {
    size = dmadrvDataSize2;
  } else {
    size = dmadrvDataSize1;
  }

  if ( handle->cfg.frameLength > 8 ) {
    rxPort = (void *)&(handle->cfg.port->RXDOUBLE);
    txPort = (void *)&(handle->cfg.port->TXDOUBLE);
  } else {
    rxPort = (void *)&(handle->cfg.port->RXDATA);
    txPort = (void *)&(handle->cfg.port->TXDATA);
  }

  // Start receive DMA.
  DMADRV_PeripheralMemory(handle->rxDMACh,
                          handle->rxDMASignal,
                          (void*)buffer,
                          rxPort,
                          true,
                          count,
                          size,
                          RxDMAComplete,
                          handle);

  // Start transmit DMA.
  DMADRV_MemoryPeripheral(handle->txDMACh,
                          handle->txDMASignal,
                          txPort,
                          (void *)&(handle->cfg.dummyTxValue),
                          false,
                          count,
                          size,
                          NULL,
                          NULL);
}

/***************************************************************************//**
 * @brief Start an SPI transmit/receive DMA.
 ******************************************************************************/
static void StartTransferDMA(SPIDRV_Handle_t handle,
                             const void *txBuffer,
                             void *rxBuffer,
                             int count,
                             SPIDRV_Callback_t callback)
{
  void *rxPort, *txPort;
  DMADRV_DataSize_t size;

  handle->blockingCompleted  = false;
  handle->transferCount      = count;
  handle->cfg.port->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
  handle->userCallback       = callback;

  if ( handle->cfg.frameLength > 8 ) {
    size = dmadrvDataSize2;
  } else {
    size = dmadrvDataSize1;
  }

  if ( handle->cfg.frameLength > 8 ) {
    rxPort = (void *)&(handle->cfg.port->RXDOUBLE);
    txPort = (void *)&(handle->cfg.port->TXDOUBLE);
  } else {
    rxPort = (void *)&(handle->cfg.port->RXDATA);
    txPort = (void *)&(handle->cfg.port->TXDATA);
  }

  // Start receive DMA.
  DMADRV_PeripheralMemory(handle->rxDMACh,
                          handle->rxDMASignal,
                          rxBuffer,
                          rxPort,
                          true,
                          count,
                          size,
                          RxDMAComplete,
                          handle);

  // Start transmit DMA.
  DMADRV_MemoryPeripheral(handle->txDMACh,
                          handle->txDMASignal,
                          txPort,
                          (void*)txBuffer,
                          true,
                          count,
                          size,
                          NULL,
                          NULL);
}

/***************************************************************************//**
 * @brief Start an SPI transmit DMA.
 ******************************************************************************/
static void StartTransmitDMA(SPIDRV_Handle_t handle,
                             const void *buffer,
                             int count,
                             SPIDRV_Callback_t callback)
{
  void *rxPort, *txPort;
  DMADRV_DataSize_t size;

  handle->blockingCompleted  = false;
  handle->transferCount      = count;
  handle->cfg.port->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
  handle->userCallback       = callback;

  if ( handle->cfg.frameLength > 8 ) {
    size = dmadrvDataSize2;
  } else {
    size = dmadrvDataSize1;
  }

  if ( handle->cfg.frameLength > 8 ) {
    rxPort = (void *)&(handle->cfg.port->RXDOUBLE);
    txPort = (void *)&(handle->cfg.port->TXDOUBLE);
  } else {
    rxPort = (void *)&(handle->cfg.port->RXDATA);
    txPort = (void *)&(handle->cfg.port->TXDATA);
  }

  // Receive DMA runs only to get precise numbers for SPIDRV_GetTransferStatus()
  // Start receive DMA.
  DMADRV_PeripheralMemory(handle->rxDMACh,
                          handle->rxDMASignal,
                          &(handle->dummyRx),
                          rxPort,
                          false,
                          count,
                          size,
                          RxDMAComplete,
                          handle);

  // Start transmit DMA.
  DMADRV_MemoryPeripheral(handle->txDMACh,
                          handle->txDMASignal,
                          txPort,
                          (void*)buffer,
                          true,
                          count,
                          size,
                          NULL,
                          NULL);
}
#endif

#if 0//Brandon

/***************************************************************************//**
 * @brief Parameter checking function for blocking transfer API functions.
 ******************************************************************************/
static Ecode_t TransferApiBlockingPrologue(SPIDRV_Handle_t handle,
                                           void *buffer,
                                           int count)
{
  CORE_DECLARE_IRQ_STATE;

  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  if ((buffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  CORE_ENTER_ATOMIC();
  if ( handle->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }
  handle->state = spidrvStateTransferring;
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief Parameter checking function for non-blocking transfer API functions.
 ******************************************************************************/
static Ecode_t TransferApiPrologue(SPIDRV_Handle_t handle,
                                   void *buffer,
                                   int count)
{
  CORE_DECLARE_IRQ_STATE;

  if ( handle == NULL ) {
    return ECODE_EMDRV_SPIDRV_ILLEGAL_HANDLE;
  }

  if ((buffer == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
    return ECODE_EMDRV_SPIDRV_PARAM_ERROR;
  }

  CORE_ENTER_ATOMIC();
  if ( handle->state != spidrvStateIdle ) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_SPIDRV_BUSY;
  }
  handle->state = spidrvStateTransferring;
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_SPIDRV_OK;
}

/***************************************************************************//**
 * @brief Wait for transfer completion.
 ******************************************************************************/
static void WaitForTransferCompletion(SPIDRV_Handle_t handle)
{
  if (CORE_IrqIsBlocked(SPI_DMA_IRQ)) {
    // Poll for completion by calling IRQ handler.
    while ( handle->blockingCompleted == false ) {
#if defined(DMA_PRESENT) && (DMA_COUNT == 1)
      DMA_IRQHandler();
#elif defined(LDMA_PRESENT) && (LDMA_COUNT == 1)
      LDMA_IRQHandler();
#else
#error "No valid SPIDRV DMA engine defined."
#endif
    }
  } else {
    while ( handle->blockingCompleted == false ) ;
  }
}
#endif
/// @endcond

/* *INDENT-OFF* */
/******** THE REST OF THE FILE IS DOCUMENTATION ONLY !**********************//**
 * @addtogroup emdrv
 * @{
 * @addtogroup SPIDRV
 * @brief SPIDRV Serial Peripheral Interface Driver
 * @{

   @details
   The spidrv.c and spidrv.h source files for the SPI driver library are in the
   emdrv/spidrv folder.

   @li @ref spidrv_intro
   @li @ref spidrv_conf
   @li @ref spidrv_api
   @li @ref spidrv_example

   @n @section spidrv_intro Introduction
   The SPI driver supports the SPI capabilities of EFM32/EZR32/EFR32 USARTs.
   The driver is fully reentrant, supports several driver instances, and
   does not buffer or queue data. Both synchronous and asynchronous transfer
   functions are included for both master and slave SPI mode. Synchronous
   transfer functions are blocking and do
   not return before the transfer is complete. Asynchronous transfer
   functions report transfer completion with callback functions. Transfers are
   handled using DMA.

   @note Transfer completion callback functions are called from within the DMA
   interrupt handler with interrupts disabled.

   @n @section spidrv_conf Configuration Options

   Some properties of the SPIDRV driver are compile-time configurable. These
   properties are stored in a file named @ref spidrv_config.h. A template for
   this file, containing default values, is in the emdrv/config folder.
   Currently the configuration options are as follows:
   @li Inclusion of slave API transfer functions.

   To configure SPIDRV, provide a custom configuration file. This is a
   sample @ref spidrv_config.h file:
   @verbatim
#ifndef __SILICON_LABS_SPIDRV_CONFIG_H__
#define __SILICON_LABS_SPIDRV_CONFIG_H__

// SPIDRV configuration option. Use this define to include the
// slave part of the SPIDRV API.
#define EMDRV_SPIDRV_INCLUDE_SLAVE

#endif
   @endverbatim

   The properties of each SPI driver instance are set at run-time using the
   @ref SPIDRV_Init_t data structure input parameter to the @ref SPIDRV_Init()
   function.

   @n @section spidrv_api The API

   This section contains brief descriptions of the API functions. For
   detailed information on input and output parameters and return values,
   click on the hyperlinked function names. Most functions return an error
   code, @ref ECODE_EMDRV_SPIDRV_OK is returned on success,
   see @ref ecode.h and @ref spidrv.h for other error codes.

   The application code must include @em spidrv.h.

   @ref SPIDRV_Init(), @ref SPIDRV_DeInit() @n
    These functions initialize or deinitializes the SPIDRV driver. Typically,
    @htmlonly SPIDRV_Init() @endhtmlonly is called once in the startup code.

   @ref SPIDRV_GetTransferStatus() @n
    Query the status of a transfer. Reports number of items (frames) transmitted
    and remaining.

   @ref SPIDRV_AbortTransfer() @n
    Stop an ongoing transfer.

   @ref SPIDRV_SetBitrate(), @ref SPIDRV_GetBitrate() @n
    Set or query the SPI bus bitrate.

   @ref SPIDRV_SetFramelength(), @ref SPIDRV_GetFramelength() @n
    Set or query SPI the bus frame length.

   SPIDRV_MReceive(), SPIDRV_MReceiveB() @n
   SPIDRV_MTransfer(), SPIDRV_MTransferB(), SPIDRV_MTransferSingleItemB() @n
   SPIDRV_MTransmit(), SPIDRV_MTransmitB() @n
   SPIDRV_SReceive(), SPIDRV_SReceiveB() @n
   SPIDRV_STransfer(), SPIDRV_STransferB() @n
   SPIDRV_STransmit(), SPIDRV_STransmitB() @n
    SPI transfer functions for SPI masters have an uppercase M in their name,
    the slave counterparts have an S.

    As previously mentioned, transfer functions are synchronous and asynchronous.
    The synchronous versions have an uppercase B (for Blocking) at the end of
    their function name.

    @em Transmit functions discard received data, @em receive functions transmit
    a fixed data pattern set when the driver is initialized
    (@ref SPIDRV_Init_t.dummyTxValue). @em Transfer functions both receive and
    transmit data.

    All slave transfer functions have a millisecond timeout parameter. Use 0
    for no (infinite) timeout.

   @n @section spidrv_example Example
   @verbatim
#include "spidrv.h"

SPIDRV_HandleData_t handleData;
SPIDRV_Handle_t handle = &handleData;

void TransferComplete(SPIDRV_Handle_t handle,
                      Ecode_t transferStatus,
                      int itemsTransferred)
{
  if (transferStatus == ECODE_EMDRV_SPIDRV_OK) {
   // Success !
  }
}

int main(void)
{
  uint8_t buffer[10];
  SPIDRV_Init_t initData = SPIDRV_MASTER_USART2;

  // Initialize an SPI driver instance.
  SPIDRV_Init(handle, &initData);

  // Transmit data using a blocking transmit function.
  SPIDRV_MTransmitB(handle, buffer, 10);

  // Transmit data using a callback to catch transfer completion.
  SPIDRV_MTransmit(handle, buffer, 10, TransferComplete);
}
   @endverbatim

 * @} end group SPIDRV ********************************************************
 * @} end group emdrv ****************************************************/

#endif
