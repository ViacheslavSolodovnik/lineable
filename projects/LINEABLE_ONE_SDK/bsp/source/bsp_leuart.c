/****************************************************************************************
//
//
//  Copyright Lineable (C) 2018 by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author: 
//   File name : 
//   Created : 
//   Last Update : 
//      NO CTS RTS : no hardware flow control
****************************************************************************************/
#include <bsphalconfig.h>
#include <common/source/rtos/rtos_utils_priv.h>
#include <string.h>


#if 0// Brandon

#include "bsp_leuart.h"
#include "em_device.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_core.h"
#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
#include "gpiointerrupt.h"
#endif

//#include  <common/source/ring_buf/ring_buf_priv.h>

#if 1
//#define LEUART_ASSERT(expr,ret_val)   EFM_ASSERT(expr)
//#define LEUART_ASSERT(expr,ret_val)   RTOS_ASSERT_DBG(expr,RTOS_ERR_INVALID_ARG,ret_val)
#define LEUART_ASSERT(expr,ret_val)   RTOS_ASSERT_CRITICAL(expr,RTOS_ERR_INVALID_ARG,ret_val)
#define LEUART_DEBUG(...) printf( __VA_ARGS__ )
#else
#define LEUART_ASSERT(expr)    ((void)(expr))
#define LEUART_DEBUG(...)
#endif


#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE) && defined(_USART_ROUTEPEN_CTSPEN_MASK)
#define UART_HW_FLOW_CONTROL_SUPPORT
#endif

/// @cond DO_NOT_INCLUDE_WITH_DOXYGEN

#if 0//defined(DMA_PRESENT) && (DMA_COUNT == 1)
#define UART_DMA_IRQ          DMA_IRQn
#define UART_DMA_IRQHANDLER() DMA_IRQHandler()
#elif defined(LDMA_PRESENT) && (LDMA_COUNT == 1)
#define UART_DMA_IRQ          LDMA_IRQn
#define UART_DMA_IRQHANDLER() LDMA_IRQHandler()
#else
#error "No valid UARTDRV DMA engine defined."
#endif

#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
static bool uartdrvHandleIsInitialized = false;
static UARTDRV_Handle_t uartdrvHandle[EMDRV_UARTDRV_MAX_DRIVER_INSTANCES];
#endif

static bool ReceiveDmaComplete(unsigned int channel,
                               unsigned int sequenceNo,
                               void *userParam);
static bool TransmitDmaComplete(unsigned int channel,
                                unsigned int sequenceNo,
                                void *userParam);
/***************************************************************************//**
 * @brief Enqueue UART transfer buffer.
 ******************************************************************************/
static Ecode_t EnqueueBuffer(UARTDRV_Buffer_FifoQueue_t *queue,
                             UARTDRV_Buffer_t *inputBuffer,
                             UARTDRV_Buffer_t **queueBuffer)
{
  CORE_DECLARE_IRQ_STATE;

  CORE_ENTER_ATOMIC();
  if (queue->used >= queue->size) {
    *queueBuffer = NULL;
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_UARTDRV_QUEUE_FULL;
  }
  Mem_Copy((void *)&queue->fifo[queue->head],
         (const void *)inputBuffer,
         sizeof(UARTDRV_Buffer_t));
  *queueBuffer = &queue->fifo[queue->head];
  queue->head = (queue->head + 1) % queue->size;
  queue->used++;
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief Dequeue UART transfer buffer.
 ******************************************************************************/
static Ecode_t DequeueBuffer(UARTDRV_Buffer_FifoQueue_t *queue,
                             UARTDRV_Buffer_t **buffer)
{
  CORE_DECLARE_IRQ_STATE;

  CORE_ENTER_ATOMIC();
  if (queue->used == 0) {
    *buffer = NULL;
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_UARTDRV_QUEUE_EMPTY;
  }
  *buffer = &queue->fifo[queue->tail];
  queue->tail = (queue->tail + 1) % queue->size;
  queue->used--;
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief Get tail UART transfer buffer.
 ******************************************************************************/
static Ecode_t GetTailBuffer(UARTDRV_Buffer_FifoQueue_t *queue,
                             UARTDRV_Buffer_t **buffer)
{
  CORE_DECLARE_IRQ_STATE;

  CORE_ENTER_ATOMIC();
  if (queue->used == 0) {
    *buffer = NULL;
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_UARTDRV_QUEUE_EMPTY;
  }
  *buffer = &queue->fifo[queue->tail];

  CORE_EXIT_ATOMIC();
  return ECODE_EMDRV_UARTDRV_OK;
}


/***************************************************************************//**
 * @brief Disable UART transmitter.
 ******************************************************************************/
static void leuart_disable_tx(UARTDRV_Handle_t handle)
{
    // Wait for previous register writes to sync
    while ((handle->peripheral.leuart->SYNCBUSY & LEUART_SYNCBUSY_CMD) != 0U)
        ;

    // Disable Tx route
    handle->peripheral.leuart->ROUTEPEN &= ~LEUART_ROUTEPEN_TXPEN;
    // Disable Tx
    handle->peripheral.leuart->CMD = LEUART_CMD_TXDIS;
}
/***************************************************************************//**
 * @brief Enable UART receiver.
 ******************************************************************************/
static void leuart_enable_rx(UARTDRV_Handle_t handle)
{
    // Wait for previous register writes to sync
    while ((handle->peripheral.leuart->SYNCBUSY & LEUART_SYNCBUSY_CMD) != 0U)
        ;
    // Enable Rx
    handle->peripheral.leuart->CMD = LEUART_CMD_RXEN;

    // Wait for Rx to be enabled
    while (!(handle->peripheral.leuart->STATUS & LEUART_STATUS_RXENS))
        ; 

    // Enable Rx route
    handle->peripheral.leuart->ROUTEPEN |= LEUART_ROUTEPEN_RXPEN;
}
/***************************************************************************//**
 * @brief Disable UART receiver.
 ******************************************************************************/
static void leuart_disable_rx(UARTDRV_Handle_t handle)
{
  
    // Wait for prevous register writes to sync
    while ((handle->peripheral.leuart->SYNCBUSY & LEUART_SYNCBUSY_CMD) != 0U)
        ;

    // Disable Rx route
    handle->peripheral.leuart->ROUTEPEN &= ~LEUART_ROUTEPEN_RXPEN;
    // Disable Rx
    handle->peripheral.leuart->CMD = LEUART_CMD_RXDIS;
  
}
/***************************************************************************//**
 * @brief Start a UART DMA receive operation.
 ******************************************************************************/
static void StartReceiveDma(UARTDRV_Handle_t handle,
                            UARTDRV_Buffer_t *buffer)
{
  void *rxPort = NULL;

  handle->rxDmaActive = true;
  if (handle->type == uartdrvUartTypeUart) {
    rxPort = (void *)&(handle->peripheral.uart->RXDATA);
  } else if (handle->type == uartdrvUartTypeLeuart) {
    rxPort = (void *)&(handle->peripheral.leuart->RXDATA);
  } else {
    handle->rxDmaActive = false;
    return;
  }

  DMADRV_PeripheralMemory(handle->rxDmaCh,
                          handle->rxDmaSignal,
                          buffer->data,
                          rxPort,
                          true,
                          buffer->transferCount,
                          dmadrvDataSize1,
                          ReceiveDmaComplete,
                          handle);
}

/***************************************************************************//**
 * @brief Start a UART DMA transmit operation.
 ******************************************************************************/
static void StartTransmitDma(UARTDRV_Handle_t handle,
                             UARTDRV_Buffer_t *buffer)
{
  void *txPort = NULL;

  handle->txDmaActive = true;
  if (handle->type == uartdrvUartTypeUart) {
    txPort = (void *)&(handle->peripheral.uart->TXDATA);
  } 
  else if (handle->type == uartdrvUartTypeLeuart) {
    txPort = (void *)&(handle->peripheral.leuart->TXDATA);
  } else {
    handle->txDmaActive = false;
    return;
  }

  DMADRV_MemoryPeripheral(handle->txDmaCh,
                          handle->txDmaSignal,
                          txPort,
                          buffer->data,
                          true,
                          buffer->transferCount,
                          dmadrvDataSize1,
                          TransmitDmaComplete,
                          handle);
}

/***************************************************************************//**
 * @brief DMA transfer completion callback. Called by the DMA interrupt handler.
 ******************************************************************************/
static bool ReceiveDmaComplete(unsigned int channel,
                               unsigned int sequenceNo,
                               void *userParam)
{
  CORE_DECLARE_IRQ_STATE;
  UARTDRV_Handle_t handle;
  UARTDRV_Buffer_t *buffer;
  Ecode_t status;
  (void)channel;
  (void)sequenceNo;

  handle = (UARTDRV_Handle_t)userParam;
  status = GetTailBuffer(handle->rxQueue, &buffer);

  // If an abort was in progress when DMA completed, the ISR could be deferred
  // until after the critical section. In this case, the buffers no longer
  // exist, even though the DMA complete callback was called.
  if (status == ECODE_EMDRV_UARTDRV_QUEUE_EMPTY) {
    return true;
  }

  EFM_ASSERT(buffer != NULL);

  if (handle->type == uartdrvUartTypeUart
      && (handle->peripheral.uart->IF & USART_IF_FERR)) {
    buffer->transferStatus = ECODE_EMDRV_UARTDRV_FRAME_ERROR;
    buffer->itemsRemaining = 0;
    handle->peripheral.uart->IFC = USART_IFC_FERR;
  } else if (handle->type == uartdrvUartTypeLeuart
             && (handle->peripheral.leuart->IF & LEUART_IF_FERR)) {
    buffer->transferStatus = ECODE_EMDRV_UARTDRV_FRAME_ERROR;
    buffer->itemsRemaining = 0;
    handle->peripheral.leuart->IFC = LEUART_IFC_FERR;
  } else if (handle->type == uartdrvUartTypeUart
             && (handle->peripheral.uart->IF & USART_IF_PERR)) {
    buffer->transferStatus = ECODE_EMDRV_UARTDRV_PARITY_ERROR;
    buffer->itemsRemaining = 0;
    handle->peripheral.uart->IFC = USART_IFC_PERR;
  } else if (handle->type == uartdrvUartTypeLeuart
             && (handle->peripheral.leuart->IF & LEUART_IF_PERR)) {
    buffer->transferStatus = ECODE_EMDRV_UARTDRV_PARITY_ERROR;
    buffer->itemsRemaining = 0;
    handle->peripheral.leuart->IFC = LEUART_IFC_PERR;
  } else {
    buffer->transferStatus = ECODE_EMDRV_UARTDRV_OK;
    buffer->itemsRemaining = 0;
  }

  CORE_ENTER_ATOMIC();

  if (buffer->callback != NULL) {
    buffer->callback(handle, buffer->transferStatus, buffer->data, buffer->transferCount - buffer->itemsRemaining);
  }
  // Dequeue the current tail Rx operation, check if more in queue
  DequeueBuffer(handle->rxQueue, &buffer);

  if (handle->rxQueue->used > 0) {
    GetTailBuffer(handle->rxQueue, &buffer);
    StartReceiveDma(handle, buffer);
  } else {
#if 0//EMDRV_UARTDRV_FLOW_CONTROL_ENABLE
    handle->fcSelfState = uartdrvFlowControlOff;
    FcApplyState(handle);
#endif
    handle->rxDmaActive = false;

    if (handle->fcType != uartdrvFlowControlHwUart) {
      leuart_disable_rx(handle);
    }
  }
  CORE_EXIT_ATOMIC();
  return true;
}

/***************************************************************************//**
 * @brief DMA transfer completion callback. Called by the DMA interrupt handler.
 ******************************************************************************/
static bool TransmitDmaComplete(unsigned int channel,
                                unsigned int sequenceNo,
                                void *userParam)
{
  CORE_DECLARE_IRQ_STATE;
  UARTDRV_Handle_t handle;
  UARTDRV_Buffer_t *buffer;
  Ecode_t status;
  (void)channel;
  (void)sequenceNo;

  handle = (UARTDRV_Handle_t)userParam;
  status = GetTailBuffer(handle->txQueue, &buffer);

  // If an abort was in progress when DMA completed, the ISR could be deferred
  // until after the critical section. In this case, the buffers no longer
  // exist, even though the DMA complete callback was called.
  if (status == ECODE_EMDRV_UARTDRV_QUEUE_EMPTY) {
    return true;
  }

  EFM_ASSERT(buffer != NULL);

  buffer->transferStatus = ECODE_EMDRV_UARTDRV_OK;
  buffer->itemsRemaining = 0;

  CORE_ENTER_ATOMIC();

  if (buffer->callback != NULL) {
    buffer->callback(handle, ECODE_EMDRV_UARTDRV_OK, buffer->data, buffer->transferCount);
  }
  // Dequeue the current tail Tx operation, check if more in queue
  DequeueBuffer(handle->txQueue, &buffer);

  if (handle->txQueue->used > 0) {
    GetTailBuffer(handle->txQueue, &buffer);
    StartTransmitDma(handle, buffer);
  } else {
    handle->txDmaActive = false;
  }
  CORE_EXIT_ATOMIC();
  return true;
}

/***************************************************************************//**
 * @brief Parameter checking function for blocking transfer API functions.
 ******************************************************************************/
static Ecode_t CheckParams(UARTDRV_Handle_t handle, void *data, uint32_t count)
{
  if (handle == NULL) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE;
  }
  if ((data == NULL) || (count == 0) || (count > DMADRV_MAX_XFER_COUNT)) {
    return ECODE_EMDRV_UARTDRV_PARAM_ERROR;
  }
  return ECODE_EMDRV_UARTDRV_OK;
}


/***************************************************************************//**
 * @brief Initialize FIFO queues for a handle.
 ******************************************************************************/
static void InitializeQueues(UARTDRV_Handle_t handle,
                             UARTDRV_Buffer_FifoQueue_t * rxQueue,
                             UARTDRV_Buffer_FifoQueue_t * txQueue)
{
  handle->rxQueue = rxQueue;
  handle->rxQueue->head = 0;
  handle->rxQueue->tail = 0;
  handle->rxQueue->used = 0;
  handle->rxDmaActive = false;

  handle->txQueue = txQueue;
  handle->txQueue->head = 0;
  handle->txQueue->tail = 0;
  handle->txQueue->used = 0;
  handle->txDmaActive = false;

}

#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
/***************************************************************************//**
 * @brief Initialize GPIO-driven flow control.
 ******************************************************************************/
static void InitializeGpioFlowControl(UARTDRV_Handle_t handle)
{
  GPIOINT_Init();
  GPIOINT_CallbackRegister(handle->ctsPin, HwFcManageClearToSend);
  handle->fcPeerState = uartdrvFlowControlOn;
  handle->fcSelfState = uartdrvFlowControlOn;
  handle->fcSelfCfg = uartdrvFlowControlAuto;
  FcApplyState(handle);
}
#endif

/***************************************************************************//**
 * @brief Initialize DMA channels for a handle.
 ******************************************************************************/
static Ecode_t InitializeDma(UARTDRV_Handle_t handle)
{
  // Initialize DMA.
  DMADRV_Init();

  if (DMADRV_AllocateChannel(&handle->txDmaCh, NULL) != ECODE_EMDRV_DMADRV_OK) {
    return ECODE_EMDRV_UARTDRV_DMA_ALLOC_ERROR;
  }

  if (DMADRV_AllocateChannel(&handle->rxDmaCh, NULL) != ECODE_EMDRV_DMADRV_OK) {
    return ECODE_EMDRV_UARTDRV_DMA_ALLOC_ERROR;
  }

  return ECODE_EMDRV_UARTDRV_OK;
}
/// @endcond


/***************************************************************************//**
 * @brief
 *    Initialize a LEUART driver instance.
 *
 * @param[out] handle  Pointer to a UARTDRV handle, refer to @ref
 *                     UARTDRV_Handle_t.
 *
 * @param[in] initData Pointer to an initialization data structure,
 *                     refer to @ref UARTDRV_InitLeuart_t.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success. On failure, an appropriate
 *    UARTDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t UARTDRV_InitLeuart(UARTDRV_Handle_t handle,
                           const UARTDRV_InitLeuart_t *initData)
{
  Ecode_t retVal;
  CORE_DECLARE_IRQ_STATE;
  LEUART_Init_TypeDef leuartInit = LEUART_INIT_DEFAULT;

  if (handle == NULL) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE;
  }
  if (initData == NULL) {
    return ECODE_EMDRV_UARTDRV_PARAM_ERROR;
  }
  if (initData->fcType == uartdrvFlowControlHwUart) {
    // LEUART doesn't support peripheral hw flow control
    return ECODE_EMDRV_UARTDRV_PARAM_ERROR;
  }

  Mem_Set(handle, 0, sizeof(UARTDRV_HandleData_t));

#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
  retVal = SetHandleIndex(handle);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  handle->fcType = initData->fcType;
#else
  // Force init data to uartdrvFlowControlNone if flow control is excluded by EMDRV_UARTDRV_FLOW_CONTROL_ENABLE
  handle->fcType = uartdrvFlowControlNone;
#endif

  handle->peripheral.leuart = initData->port;
  handle->type = uartdrvUartTypeLeuart;

  // Set clocks and DMA requests according to available peripherals
  if (initData->port == LEUART0) {
    handle->uartClock   = cmuClock_LEUART0;
    handle->txDmaSignal = dmadrvPeripheralSignal_LEUART0_TXBL;
    handle->rxDmaSignal = dmadrvPeripheralSignal_LEUART0_RXDATAV;
  } else {
    return ECODE_EMDRV_UARTDRV_PARAM_ERROR;
  }

  InitializeQueues(handle, initData->rxQueue, initData->txQueue);

  leuartInit.baudrate   = initData->baudRate;
  leuartInit.stopbits   = initData->stopBits;
  leuartInit.parity     = initData->parity;

  // UARTDRV is fixed at 8 bit frames.
  leuartInit.databits = (LEUART_Databits_TypeDef)LEUART_CTRL_DATABITS_EIGHT;

  // Enable clocks
  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_HFLE, true);

  // Only try to use LF clock if LFXO is enabled and requested baudrate is low
  if (CMU->STATUS & CMU_STATUS_LFXOENS
      && (leuartInit.baudrate <= SystemLFXOClockGet())) {
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
  } else {
    // Try to figure out the prescaler that will give us the best stability
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_HFCLKLE);

   // Clock divider in LEUARTn is large enough for any baudrate.
  }

  CMU_ClockEnable(handle->uartClock, true);

  // Init LEUART to default async config.
  // Rx/Tx enable is done on demand
  leuartInit.enable = leuartDisable;
  LEUART_Init(initData->port, &leuartInit);


    initData->port->ROUTEPEN = LEUART_ROUTEPEN_TXPEN
                         | LEUART_ROUTEPEN_RXPEN;
    initData->port->ROUTELOC0 = (initData->port->ROUTELOC0
                           & ~(_LEUART_ROUTELOC0_TXLOC_MASK
                               | _LEUART_ROUTELOC0_RXLOC_MASK))
                          | (initData->portLocationTx
                             << _LEUART_ROUTELOC0_TXLOC_SHIFT)
                          | (initData->portLocationRx
                             << _LEUART_ROUTELOC0_RXLOC_SHIFT);

    handle->txPort  = (GPIO_Port_TypeDef)AF_LEUART0_TX_PORT(initData->portLocationTx);
    handle->rxPort  = (GPIO_Port_TypeDef)AF_LEUART0_RX_PORT(initData->portLocationRx);
    handle->txPin   = AF_LEUART0_TX_PIN(initData->portLocationTx);
    handle->rxPin   = AF_LEUART0_RX_PIN(initData->portLocationRx);

    GPIO_PinModeSet(handle->txPort, handle->txPin, gpioModePushPull, 1);
    GPIO_PinModeSet(handle->rxPort, handle->rxPin, gpioModeInputPull, 1);


  CORE_ENTER_ATOMIC();

  // Configure hardware flow control pins and interrupt vectors
#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
  if (initData->fcType == uartdrvFlowControlHw) {
    InitializeGpioFlowControl(handle);
  }
#endif

  // Clear any false IRQ/DMA request
  LEUART_IntClear(initData->port, ~0x0);

  // Enable Tx permanently as the Tx circuit consumes very little energy.
  // Rx is enabled on demand as the Rx circuit consumes some energy due to
  // continuous (over)sampling.
  LEUART_Enable(initData->port, leuartEnableTx);

  // Wait for previous write (TXEN) to sync before clearing FIFOs
  while ((initData->port->SYNCBUSY & LEUART_SYNCBUSY_CMD) != 0U) {
  }

  // Discard false frames and/or IRQs
  initData->port->CMD = LEUART_CMD_CLEARRX | LEUART_CMD_CLEARTX;

  // Initialize DMA.
  DMADRV_Init();

  retVal = InitializeDma(handle);

  CORE_EXIT_ATOMIC();

  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }

  // Wait for everything to be synchronized
  while (initData->port->SYNCBUSY != 0U) {
  }
  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Deinitialize a UART driver instance.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success. On failure, an appropriate
 *    UARTDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t UARTDRV_DeInit(UARTDRV_Handle_t handle)
{
    if (handle == NULL) {
        return ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE;
    }

    // Stop DMA transfers.
    UARTDRV_Abort(handle, uartdrvAbortAll);

    // Do not leave any peer restrained on DeInit
    UARTDRV_FlowControlSet(handle, uartdrvFlowControlOn);

    GPIO_PinModeSet(handle->txPort, handle->txPin, gpioModeDisabled, 0);
    GPIO_PinModeSet(handle->rxPort, handle->rxPin, gpioModeDisabled, 0);

    LEUART_Reset(handle->peripheral.leuart);
    while ((handle->peripheral.leuart->SYNCBUSY & LEUART_SYNCBUSY_CMD) != 0U) {
    }
    handle->peripheral.leuart->CMD = LEUART_CMD_RXDIS | LEUART_CMD_TXDIS;

    CMU_ClockEnable(handle->uartClock, false);


    DMADRV_FreeChannel(handle->txDmaCh);
    DMADRV_FreeChannel(handle->rxDmaCh);
    DMADRV_DeInit();

    handle->rxQueue->head = 0;
    handle->rxQueue->tail = 0;
    handle->rxQueue->used = 0;

    handle->txQueue->head = 0;
    handle->txQueue->tail = 0;
    handle->txQueue->used = 0;

    return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Abort ongoing UART transfers.
 *
 * @details
 *    All ongoing or queued operations of the given abort type will be aborted.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] type   Abort type -- whether to abort only Tx, only Rx, or both.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success, @ref ECODE_EMDRV_UARTDRV_IDLE if
 *    the UART is idle. On failure, an appropriate UARTDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t UARTDRV_Abort(UARTDRV_Handle_t handle, UARTDRV_AbortType_t type)
{
  UARTDRV_Buffer_t *rxBuffer, *txBuffer;
  CORE_DECLARE_IRQ_STATE;

  if (handle == NULL) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE;
  }

  CORE_ENTER_ATOMIC();
  if ((type == uartdrvAbortTransmit) && (handle->txQueue->used == 0)) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_UARTDRV_IDLE;
  } else if ((type == uartdrvAbortReceive) && (handle->rxQueue->used == 0)) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_UARTDRV_IDLE;
  } else if ((type == uartdrvAbortAll)
             && (handle->txQueue->used == 0)
             && (handle->rxQueue->used == 0)) {
    CORE_EXIT_ATOMIC();
    return ECODE_EMDRV_UARTDRV_IDLE;
  }

  // Stop DMA transfers.
  if ((type == uartdrvAbortTransmit) || (type == uartdrvAbortAll)) {
    // Stop the current transfer
    DMADRV_StopTransfer(handle->txDmaCh);
    handle->txDmaActive = false;

    if (handle->txQueue->used > 0) {
      // Update the transfer status of the active transfer
      GetTailBuffer(handle->txQueue, &txBuffer);
      DMADRV_TransferRemainingCount(handle->txDmaCh,
                                    (int*)&txBuffer->itemsRemaining);
      txBuffer->transferStatus = ECODE_EMDRV_UARTDRV_ABORTED;

      // Dequeue all transfers and call callback
      while (handle->txQueue->used > 0) {
        DequeueBuffer(handle->txQueue, &txBuffer);

        // Call the callback with ABORTED error code
        if (txBuffer->callback != NULL) {
          txBuffer->callback(handle,
                             ECODE_EMDRV_UARTDRV_ABORTED,
                             NULL,
                             txBuffer->itemsRemaining);
        }
      }
    }

    // Wait for peripheral to finish cleaning up, to prevent framing errors
    // on subsequent transfers
    while (!(UARTDRV_GetPeripheralStatus(handle) & UARTDRV_STATUS_TXIDLE)) {
    }
  }
  if ((type == uartdrvAbortReceive) || (type == uartdrvAbortAll)) {
    // Stop the current transfer
    DMADRV_StopTransfer(handle->rxDmaCh);
    handle->rxDmaActive = false;

    if (handle->rxQueue->used > 0) {
      // Update the transfer status of the active transfer
      GetTailBuffer(handle->rxQueue, &rxBuffer);
      DMADRV_TransferRemainingCount(handle->rxDmaCh,
                                    (int*)&rxBuffer->itemsRemaining);
      rxBuffer->transferStatus = ECODE_EMDRV_UARTDRV_ABORTED;

      // Dequeue all transfers and call callback
      while (handle->rxQueue->used > 0) {
        DequeueBuffer(handle->rxQueue, &rxBuffer);

        // Call the callback with ABORTED error code
        if (rxBuffer->callback != NULL) {
          rxBuffer->callback(handle,
                             ECODE_EMDRV_UARTDRV_ABORTED,
                             NULL,
                             rxBuffer->itemsRemaining);
        }
      }
    }

    // Disable the receiver
    if (handle->fcType != uartdrvFlowControlHwUart) {
      leuart_disable_rx(handle);
    }
  }
  CORE_EXIT_ATOMIC();

  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Return the status of the UART peripheral associated with a given handle.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *    UART status value
 ******************************************************************************/
UARTDRV_Status_t UARTDRV_GetPeripheralStatus(UARTDRV_Handle_t handle)
{
  UARTDRV_Status_t status = 0;

  if (handle->type == uartdrvUartTypeUart) {
    status = handle->peripheral.uart->STATUS;
  } 
  else if (handle->type == uartdrvUartTypeLeuart) {
    uint32_t reg = handle->peripheral.leuart->STATUS;
    if (reg & LEUART_STATUS_TXENS) {
      status |= UARTDRV_STATUS_TXEN;
    }
    if (reg & LEUART_STATUS_RXENS) {
      status |= UARTDRV_STATUS_RXEN;
    }
    if (reg & LEUART_STATUS_RXBLOCK) {
      status |= UARTDRV_STATUS_RXBLOCK;
    }
    if (reg & LEUART_STATUS_TXC) {
      status |= UARTDRV_STATUS_TXC;
    }
    if (reg & LEUART_STATUS_TXBL) {
      status |= UARTDRV_STATUS_TXBL;
    }
    if (reg & LEUART_STATUS_RXDATAV) {
      status |= UARTDRV_STATUS_RXDATAV;
    }
#if defined(LEUART_STATUS_TXIDLE)
    if (reg & LEUART_STATUS_TXIDLE) {
      status |= UARTDRV_STATUS_TXIDLE;
    }
#endif
  }


  return status;
}

/***************************************************************************//**
 * @brief
 *    Return the number of queued receive operations.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *    The number of queued operations.
 ******************************************************************************/
uint8_t UARTDRV_GetReceiveDepth(UARTDRV_Handle_t handle)
{
  return (uint8_t)handle->rxQueue->used;
}

/***************************************************************************//**
 * @brief
 *    Check the status of the UART and gather information about any ongoing
 *    receive operations.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[out] buffer Pointer to the current data buffer.
 *
 * @param[out] itemsReceived Current bytes received count.
 *
 * @param[out] itemsRemaining Current bytes remaining count.
 *
 * @return
 *    UART status.
 ******************************************************************************/
UARTDRV_Status_t UARTDRV_GetReceiveStatus(UARTDRV_Handle_t handle,
                                          uint8_t **buffer,
                                          UARTDRV_Count_t *itemsReceived,
                                          UARTDRV_Count_t *itemsRemaining)
{
  UARTDRV_Buffer_t *rxBuffer = NULL;
  Ecode_t retVal = ECODE_EMDRV_UARTDRV_OK;
  uint32_t remaining = 0;

  if (handle->rxQueue->used > 0) {
    retVal = GetTailBuffer(handle->rxQueue, &rxBuffer);
    DMADRV_TransferRemainingCount(handle->rxDmaCh,
                                  (int*)&remaining);
  }

  if (rxBuffer && (retVal == ECODE_EMDRV_UARTDRV_OK)) {
    *itemsReceived = rxBuffer->transferCount - remaining;
    *itemsRemaining = remaining;
    *buffer = rxBuffer->data;
  } else {
    *itemsRemaining = 0;
    *itemsReceived = 0;
    *buffer = NULL;
  }

  return UARTDRV_GetPeripheralStatus(handle);
}

/***************************************************************************//**
 * @brief
 *    Returns the number of queued transmit operations.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *    The number of queued operations.
 ******************************************************************************/
uint8_t UARTDRV_GetTransmitDepth(UARTDRV_Handle_t handle)
{
  return (uint8_t)handle->txQueue->used;
}

/***************************************************************************//**
 * @brief
 *    Check the status of the UART and gather information about any ongoing
 *    transmit operations.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[out] buffer Pointer to the current data buffer.
 *
 * @param[out] itemsSent Current bytes sent count.
 *
 * @param[out] itemsRemaining Current bytes remaining count.
 *
 * @return
 *    UART status.
 ******************************************************************************/
UARTDRV_Status_t UARTDRV_GetTransmitStatus(UARTDRV_Handle_t handle,
                                           uint8_t **buffer,
                                           UARTDRV_Count_t *itemsSent,
                                           UARTDRV_Count_t *itemsRemaining)
{
  UARTDRV_Buffer_t *txBuffer = NULL;
  Ecode_t retVal = ECODE_EMDRV_UARTDRV_OK;
  uint32_t remaining = 0;

  if (handle->txQueue->used > 0) {
    retVal = GetTailBuffer(handle->txQueue, &txBuffer);
    DMADRV_TransferRemainingCount(handle->txDmaCh,
                                  (int*)&remaining);
  }

  if (txBuffer && (retVal == ECODE_EMDRV_UARTDRV_OK)) {
    *itemsSent = txBuffer->transferCount - remaining;
    *itemsRemaining = remaining;
    *buffer = txBuffer->data;
  } else {
    *itemsRemaining = 0;
    *itemsSent = 0;
    *buffer = NULL;
  }

  return UARTDRV_GetPeripheralStatus(handle);
}

/***************************************************************************//**
 * @brief
 *    Set UART flow control state. Set nRTS pin if hardware flow control
 *    is enabled. Send XON/XOFF if software flow control is enabled.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] state Flow control state.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t UARTDRV_FlowControlSet(UARTDRV_Handle_t handle, UARTDRV_FlowControlState_t state)
{
#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
  if (handle->fcType == uartdrvFlowControlHwUart) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_OPERATION;
  }
  handle->fcSelfCfg = state;
  if (state != uartdrvFlowControlAuto) {
    handle->fcSelfState = state;
  }
  return FcApplyState(handle);
#else
  return ECODE_EMDRV_UARTDRV_OK;
#endif
}

/***************************************************************************//**
 * @brief
 *    Set peer UART flow control state. Pause/resume transmit accordingly.
 *    Only for manual software flow control.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] state Flow control state.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t UARTDRV_FlowControlSetPeerStatus(UARTDRV_Handle_t handle, UARTDRV_FlowControlState_t state)
{
  if ( (handle->fcType != uartdrvFlowControlSw)
       || (state == uartdrvFlowControlAuto) ) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_OPERATION;
  }

  if (handle->fcPeerState != state) {
    handle->fcPeerState = state;
    if (state == uartdrvFlowControlOn) {
      UARTDRV_ResumeTransmit(handle);
    } else if (state == uartdrvFlowControlOff) {
      UARTDRV_PauseTransmit(handle);
    }
  }

  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Check the peer's flow control status.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *    Returns uartdrvFlowControlOn if clear to send.
 ******************************************************************************/
UARTDRV_FlowControlState_t UARTDRV_FlowControlGetPeerStatus(UARTDRV_Handle_t handle)
{
  return handle->fcPeerState;
}

/***************************************************************************//**
 * @brief
 *    Check the self flow control status.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *    Returns uartdrvFlowControlOn if requesting to send.
 ******************************************************************************/
UARTDRV_FlowControlState_t UARTDRV_FlowControlGetSelfStatus(UARTDRV_Handle_t handle)
{
  return handle->fcSelfState;
}


/***************************************************************************//**
 * @brief
 *    Direct receive without interrupts or callback. This is a blocking function.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] data pointer to buffer.
 *
 * @param[in] maxCount Maximum number of bytes to receive.
 *
 * @return
 *    Number of bytes received.
 ******************************************************************************/
UARTDRV_Count_t UARTDRV_ForceReceive(UARTDRV_Handle_t handle,
                                     uint8_t *data,
                                     UARTDRV_Count_t maxCount)
{
    Ecode_t retVal;
    uint32_t rxState;
    UARTDRV_Count_t i = 0;

    retVal = CheckParams(handle, data, maxCount);
    if (retVal != ECODE_EMDRV_UARTDRV_OK) {
        return 0;
    }

    // Wait for DMA receive to complete and clear
    while (handle->rxQueue->used > 0) {
        ;
    }

    if (handle->type == uartdrvUartTypeUart) {
        rxState = (handle->peripheral.uart->STATUS & USART_STATUS_RXENS);
        if (!rxState) {
            uart_enable_rx(handle);
        }

        while ((handle->peripheral.uart->STATUS & USART_STATUS_RXDATAV) != 0U) {
            *data = (uint8_t)handle->peripheral.uart->RXDATA;
            data++;
            i++;
            if (i >= maxCount) {
            break;
            }
        }

    } else if (handle->type == uartdrvUartTypeLeuart) {
        rxState = (handle->peripheral.leuart->STATUS & LEUART_STATUS_RXENS);
        if (!rxState) {
            leuart_enable_rx(handle);
        }

        while ((handle->peripheral.leuart->STATUS & LEUART_STATUS_RXDATAV) != 0U) {
            *data = (uint8_t)handle->peripheral.leuart->RXDATA;
            data++;
            i++;
            if (i >= maxCount) {
            break;
            }
        }
    } else {
        EFM_ASSERT(false);
        rxState = 0;
    }

    data -= i;

    if (!rxState) {
        leuart_disable_rx(handle);
    }
    
    return i;
}

/***************************************************************************//**
 * @brief
 *    Direct transmit without interrupts or callback. This is a blocking function.
 *    that ignores flow control if enabled.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] data Pointer to the buffer.
 *
 * @param[in] count A number of bytes to transmit.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t  UARTDRV_ForceTransmit(UARTDRV_Handle_t handle,
                               uint8_t *data,
                               UARTDRV_Count_t count)
{
    Ecode_t retVal;
    uint32_t txState;
    bool callDmaIrqHandler;

    retVal = CheckParams(handle, data, count);
    if (retVal != ECODE_EMDRV_UARTDRV_OK) {
        return retVal;
    }

    // Wait for DMA transmit to complete and clear
    callDmaIrqHandler = CORE_IrqIsBlocked(UART_DMA_IRQ); // Loop invariant
    while ((handle->txQueue->used > 0) && (!handle->txDmaPaused)) {
        if (callDmaIrqHandler) {
          UART_DMA_IRQHANDLER();
        }
    }

    if (handle->type == uartdrvUartTypeUart) {
        txState = (handle->peripheral.uart->STATUS & USART_STATUS_TXENS);

        
    } else if (handle->type == uartdrvUartTypeLeuart) {
        txState = (handle->peripheral.leuart->STATUS & LEUART_STATUS_TXENS);

        
    } else {
        EFM_ASSERT(false);
        txState = 0;
    }
  
    if (!txState) {

        if (handle->type == uartdrvUartTypeUart) {
            // Enable Tx
            handle->peripheral.uart->CMD = USART_CMD_TXEN;
            // Wait for Tx to be enabled
            while (!(handle->peripheral.uart->STATUS & USART_STATUS_TXENS)) {
            }

            // Enable Tx route
            handle->peripheral.uart->ROUTEPEN |= USART_ROUTEPEN_TXPEN;
        } else if (handle->type == uartdrvUartTypeLeuart) {
            // Wait for previous register writes to sync
            while ((handle->peripheral.leuart->SYNCBUSY & LEUART_SYNCBUSY_CMD) != 0U) {
            }

            // Enable Tx
            handle->peripheral.leuart->CMD = LEUART_CMD_TXEN;
            // Wait for Tx to be enabled
            while (!(handle->peripheral.leuart->STATUS & LEUART_STATUS_TXENS)) {
            }

            // Enable Tx route
            handle->peripheral.leuart->ROUTEPEN |= LEUART_ROUTEPEN_TXPEN;
        }
    }

    handle->hasTransmitted = true;

    if (handle->type == uartdrvUartTypeUart) {

    
        while (count-- != 0U) {
        USART_Tx(handle->peripheral.uart, *data++);
        }
        // Wait for Tx completion
        while (!(handle->peripheral.uart->STATUS & USART_STATUS_TXC)) {
        }

        if (!txState) 
            uart_disable_tx(handle);
    } else if (handle->type == uartdrvUartTypeLeuart) {
        while (count-- != 0U) {
          LEUART_Tx(handle->peripheral.leuart, *data++);
        }
        // Wait for Tx completion
        while (!(handle->peripheral.leuart->STATUS & LEUART_STATUS_TXC)) {
        }

        if (!txState) 
          leuart_disable_tx(handle);
    }
  }


  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *  Pause an ongoing transmit operation.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *  @ref ECODE_EMDRV_UARTDRV_OK on success. On failure, an appropriate
 *  UARTDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t UARTDRV_PauseTransmit(UARTDRV_Handle_t handle)
{
  if (handle == NULL) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE;
  }
  // Pause the transfer if 1) pause counter is 0
  //                       2) HW flow control hasn't already paused the DMA
  if ( (handle->txDmaPaused == 0) ) {
    DMADRV_PauseTransfer(handle->txDmaCh);
  }
  // Increment counter to allow nested calls
  handle->txDmaPaused++;
  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Start a non-blocking receive.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] data A receive data buffer.
 *
 * @param[in] count A number of bytes received.
 *
 * @param[in]  callback A transfer completion callback.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t UARTDRV_Receive(UARTDRV_Handle_t handle,
                        uint8_t *data,
                        UARTDRV_Count_t count,
                        UARTDRV_Callback_t callback)
{
  Ecode_t retVal;
  UARTDRV_Buffer_t outputBuffer;
  UARTDRV_Buffer_t *queueBuffer;

  retVal = CheckParams(handle, data, count);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  outputBuffer.data = data;
  outputBuffer.transferCount = count;
  outputBuffer.itemsRemaining = count;
  outputBuffer.callback = callback;
  outputBuffer.transferStatus = ECODE_EMDRV_UARTDRV_WAITING;

  retVal = EnqueueBuffer(handle->rxQueue, &outputBuffer, &queueBuffer);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  if (!(handle->rxDmaActive)) {
    leuart_enable_rx(handle);
    StartReceiveDma(handle, queueBuffer);
#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
    if (handle->fcType != uartdrvFlowControlHwUart) {
      handle->fcSelfState = uartdrvFlowControlOn;
      FcApplyState(handle);
    }
#endif
  } // else: started by ReceiveDmaComplete

  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Start a blocking receive.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] data A receive data buffer.
 *
 * @param[in] count A number of bytes received.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t UARTDRV_ReceiveB(UARTDRV_Handle_t handle,
                         uint8_t *data,
                         UARTDRV_Count_t count)
{
  Ecode_t retVal;
  UARTDRV_Buffer_t inputBuffer;
  UARTDRV_Buffer_t *queueBuffer;

  retVal = CheckParams(handle, data, count);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  inputBuffer.data = data;
  inputBuffer.transferCount = count;
  inputBuffer.itemsRemaining = count;
  inputBuffer.callback = NULL;
  inputBuffer.transferStatus = ECODE_EMDRV_UARTDRV_WAITING;

  retVal = EnqueueBuffer(handle->rxQueue, &inputBuffer, &queueBuffer);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  while (handle->rxQueue->used > 1) {
    EMU_EnterEM1();
  }
  leuart_enable_rx(handle);
#if 0//(EMDRV_UARTDRV_FLOW_CONTROL_ENABLE)
  if (handle->fcType != uartdrvFlowControlHwUart) {
    handle->fcSelfState = uartdrvFlowControlOn;
    FcApplyState(handle);
  }
#endif
  StartReceiveDma(handle, queueBuffer);
  while (handle->rxDmaActive) {
    EMU_EnterEM1();
  }
  return queueBuffer->transferStatus;
}

/***************************************************************************//**
 * @brief
 *  Resume a paused transmit operation.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @return
 *  @ref ECODE_EMDRV_UARTDRV_OK on success. On failure, an appropriate
 *  UARTDRV @ref Ecode_t is returned.
 ******************************************************************************/
Ecode_t UARTDRV_ResumeTransmit(UARTDRV_Handle_t handle)
{
  if (handle == NULL) {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE;
  }

  if (handle->txDmaPaused > 0) {
    // Resume the transfer if 1) pause counter is 1
    //                        2) HW flow control doesn't need to pause the DMA
    if ( (handle->txDmaPaused == 1)  ) {
      DMADRV_ResumeTransfer(handle->txDmaCh);
    }
    handle->txDmaPaused--;
  } else {
    return ECODE_EMDRV_UARTDRV_ILLEGAL_OPERATION;
  }
  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Start a non-blocking transmit.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] data A transmit data buffer.
 *
 * @param[in] count A number of bytes to transmit.
 *
 * @param[in]  callback A transfer completion callback.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t UARTDRV_Transmit(UARTDRV_Handle_t handle,
                         uint8_t *data,
                         UARTDRV_Count_t count,
                         UARTDRV_Callback_t callback)
{
  Ecode_t retVal;
  UARTDRV_Buffer_t inputBuffer;
  UARTDRV_Buffer_t *queueBuffer;

  retVal = CheckParams(handle, data, count);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  inputBuffer.data = data;
  inputBuffer.transferCount = count;
  inputBuffer.itemsRemaining = count;
  inputBuffer.callback = callback;
  inputBuffer.transferStatus = ECODE_EMDRV_UARTDRV_WAITING;

  retVal = EnqueueBuffer(handle->txQueue, &inputBuffer, &queueBuffer);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  if (!(handle->txDmaActive)) {
    CORE_ATOMIC_SECTION(
      if (handle->txQueue->used > 0) {
      StartTransmitDma(handle, queueBuffer);
      handle->hasTransmitted = true;
    }
      )
  } // else: started by TransmitDmaComplete

  return ECODE_EMDRV_UARTDRV_OK;
}

/***************************************************************************//**
 * @brief
 *    Start a blocking transmit.
 *
 * @param[in] handle Pointer to a UART driver handle.
 *
 * @param[in] data A transmit data buffer.
 *
 * @param[in] count A number of bytes to transmit.
 *
 * @return
 *    @ref ECODE_EMDRV_UARTDRV_OK on success.
 ******************************************************************************/
Ecode_t UARTDRV_TransmitB(UARTDRV_Handle_t handle,
                          uint8_t *data,
                          UARTDRV_Count_t count)
{
  Ecode_t retVal;
  UARTDRV_Buffer_t outputBuffer;
  UARTDRV_Buffer_t *queueBuffer;

  retVal = CheckParams(handle, data, count);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  outputBuffer.data = data;
  outputBuffer.transferCount = count;
  outputBuffer.itemsRemaining = count;
  outputBuffer.callback = NULL;
  outputBuffer.transferStatus = ECODE_EMDRV_UARTDRV_WAITING;

  retVal = EnqueueBuffer(handle->txQueue, &outputBuffer, &queueBuffer);
  if (retVal != ECODE_EMDRV_UARTDRV_OK) {
    return retVal;
  }
  while (handle->txQueue->used > 1) {
    EMU_EnterEM1();
  }
  StartTransmitDma(handle, queueBuffer);
  handle->hasTransmitted = true;
  while (handle->txDmaActive) {
    EMU_EnterEM1();
  }
  return queueBuffer->transferStatus;
}






/* *INDENT-OFF* */
/******** THE REST OF THE FILE IS DOCUMENTATION ONLY !**********************//**
 * @addtogroup emdrv
 * @{
 * @addtogroup UARTDRV
 * @brief UARTDRV Universal Asynchronous Receiver/Transmitter Driver
 * @{

@details
  The source files for the UART driver library, uartdrv.c and uartdrv.h, are in the
  emdrv/uartdrv folder.

  @li @ref uartdrv_intro
  @li @ref uartdrv_conf
  @li @ref uartdrv_api
  @li @ref uartdrv_fc
  @li @ref uartdrv_example

@n @section uartdrv_intro Introduction
  The UART driver supports the UART capabilities of the USART, UART, and LEUART
  peripherals. The driver is fully reentrant and supports multiple driver instances.
  The driver does not buffer or queue data. However, it queues UART transmit
  and receive operations. Both blocking and non-blocking transfer functions are
  available. Non-blocking transfer functions report transfer completion with
  callback functions. Transfers are done using DMA. Simple direct/forced
  transmit and receive functions are also available. Note that these functions
  are blocking and not suitable for low energy applications because they use CPU
  polling.

  UART hardware flow control (CTS/RTS) is fully supported by the driver. UART
  software flow control (XON/XOFF) is partially supported by the driver. For
  more information about flow control support, see @ref uartdrv_fc.

  @note Transfer completion callback functions are called from within the DMA
  interrupt handler with interrupts disabled.

@n @section uartdrv_conf Configuration Options

  Some properties of the UARTDRV driver are compile-time configurable. These
  properties are set in a @ref uartdrv_config.h file. A template for this
  file, containing default values, is in the emdrv/config folder.
  To configure UARTDRV for your application, provide a custom configuration file,
  or override the defines on the compiler command line.
  These are the available configuration parameters with default values defined.
  @code

  // Size of the receive operation queue.
  #define EMDRV_UARTDRV_MAX_CONCURRENT_RX_BUFS    6

  // Size of the transmit operation queue.
  #define EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS    6

  // Set to 1 to enable hardware flow control.
  #define EMDRV_UARTDRV_FLOW_CONTROL_ENABLE       1

  // Maximum number of driver instances.
  // This maximum applies only when EMDRV_UARTDRV_FLOW_CONTROL_ENABLE = 1.
  #define EMDRV_UARTDRV_MAX_DRIVER_INSTANCES      4

  // UART software flow control code: request peer to start Tx.
  #define UARTDRV_FC_SW_XON                       0x11

  // UART software flow control code: request peer to stop Tx.
  #define UARTDRV_FC_SW_XOFF                      0x13
  @endcode

  The properties of each UART driver instance are set at run-time via the
  @ref UARTDRV_InitUart_t data structure input parameter to the @ref UARTDRV_InitUart()
  function for UART and USART peripherals, and the @ref UARTDRV_InitLeuart_t
  data structure input parameter to the @ref UARTDRV_InitLeuart() function for
  LEUART peripherals.

@n @section uartdrv_api The API

  This section contains brief descriptions of the functions in the API. For more
  information on input and output parameters and return values,
  click on the hyperlinked function names. Most functions return an error
  code, @ref ECODE_EMDRV_UARTDRV_OK is returned on success,
  see @ref ecode.h and @ref uartdrv.h for other error codes.

  The application code must include @em uartdrv.h header file.

  @ref UARTDRV_InitUart(), @ref UARTDRV_InitLeuart() and @ref UARTDRV_DeInit() @n
    These functions initialize and deinitialize the UARTDRV driver. Typically,
    @htmlonly UARTDRV_InitUart() @endhtmlonly (for UART/USART) or
    @htmlonly UARTDRV_InitLeuart() @endhtmlonly (for LEUART) are called once in
    the startup code.

  @ref UARTDRV_GetReceiveStatus() and @ref UARTDRV_GetTransmitStatus() @n
    Query the status of a current transmit or receive operations. Reports number
    of items (frames) transmitted and remaining.

  @ref UARTDRV_GetReceiveDepth() and  @ref UARTDRV_GetTransmitDepth() @n
    Get the number of queued receive or transmit operations.

  @ref UARTDRV_Transmit(), UARTDRV_Receive() @n
  UARTDRV_TransmitB(), UARTDRV_ReceiveB() @n
  UARTDRV_ForceTransmit() and UARTDRV_ForceReceive() @n
    Blocking and non-blocking transfer functions are included.
    The blocking versions have an uppercase B (for Blocking) at the end of
    their function name. Blocking functions do not return before the transfer
    is complete. The non-blocking functions signal a transfer completion with a
    callback function. @ref UARTDRV_ForceTransmit() and
    @ref UARTDRV_ForceReceive() are also blocking. These two functions access
    the UART peripheral directly without using DMA or interrupts.
    @ref UARTDRV_ForceTransmit() does not respect flow control.
    @ref UARTDRV_ForceReceive() forces RTS low.

  @ref UARTDRV_Abort() @n
    Abort current transmit or receive operations and remove all queued
    operations.

  @ref UARTDRV_FlowControlSet(), @ref UARTDRV_FlowControlGetSelfStatus(), @ref UARTDRV_FlowControlSetPeerStatus() and @ref UARTDRV_FlowControlGetPeerStatus() @n
    Set and get flow control status of self or peer device. Note that the return
    value from these two functions depends on the flow control mode set by
    @ref UARTDRV_FlowControlSet(), @ref UARTDRV_InitUart(), or
    @ref UARTDRV_InitLeuart().

    Enables transmission when restrained by flow control.

  @ref UARTDRV_PauseTransmit() and @ref UARTDRV_ResumeTransmit() @n
    Pause a currently active transmit operation by preventing the DMA from loading
    the UART FIFO. Will not override HW flow control state (if applicable), but
    can be used in conjunction.

@n @section uartdrv_fc Flow Control Support

  If UART flow control is not required, make sure that
  @ref EMDRV_UARTDRV_FLOW_CONTROL_ENABLE is set to 0. This reduces the code size
  and complexity of the driver.

  Both hardware and software flow control are supported. To
  enable either of these, set @ref EMDRV_UARTDRV_FLOW_CONTROL_ENABLE to 1 in
  @ref uartdrv_config.h.

@n @subsection uartdrv_fc_hw Hardware Flow Control

  UART hardware flow control uses two additional pins for flow control
  handshaking, the clear-to-send (CTS) and ready-to-send (RTS) pins.
  RTS is an output and CTS is an input. These are active-low signals.
  When CTS is high, the UART transmitter should stop sending frames.
  A receiver should set RTS high when it is no longer capable of
  receiving data.

  @par Peripheral Hardware Flow Control

  Newer devices, such as EFR32MG1 and EFM32PG1, natively support CTS/RTS in
  the USART peripheral hardware. To enable hardware flow control, perform the
  following steps:

  - Set EMDRV_UARTDRV_FLOW_CONTROL_ENABLE to 1.
  - In the @ref UARTDRV_InitUart_t struct passed to @ref UARTDRV_InitUart(), set
    @ref UARTDRV_InitUart_t.fcType = uartdrvFlowControlHwUart.
  - Define the pins for CTS and RTS by setting ctsPort, ctsPin, rtsPort
    and rtsPin in the init struct.
  - Also define the CTS and RTS locations by setting portLocationCts and
    portLocationRts in the init struct.

  @par GPIO Hardware Flow Control

  To support hardware flow control on devices that don't have UART CTS/RTS
  hardware support, the driver includes the GPIOINT driver to emulate a
  hardware implementation of UART CTS/RTS flow control on these devices.

  To enable hardware flow control, perform the following steps:

  - Set @ref EMDRV_UARTDRV_FLOW_CONTROL_ENABLE to 1.
  - UART/USART: In the @ref UARTDRV_InitUart_t struct passed to
    @ref UARTDRV_InitUart(), set
    @ref UARTDRV_InitUart_t.fcType = uartdrvFlowControlHw.
  - LEUART: In the @ref UARTDRV_InitLeuart_t struct passed to
    @ref UARTDRV_InitLeuart(), set
    @ref UARTDRV_InitLeuart_t.fcType = uartdrvFlowControlHw.
  - Define the pins for CTS and RTS by setting ctsPort, ctsPin, rtsPort and
    rtsPin in the same init struct.

  @note Because of the limitations in GPIO interrupt hardware, you cannot select
  CTS pins in multiple driver instances with the same pin number. For example, pin A0 and
  B0 cannot serve as CTS pins in two concurrent driver instances.

  RTS is set high whenever there are no Rx operations queued. The UART
  transmitter is halted when the CTS pin goes high. The transmitter completes
  the current frame before halting. DMA transfers are also halted.

@n @subsection uartdrv_fc_sw Software Flow Control

  UART software flow control uses in-band signaling, meaning the receiver sends
  special flow control characters to the transmitter and thereby removes
  the need for dedicated wires for flow control. The two symbols
  UARTDRV_FC_SW_XON and UARTDRV_FC_SW_XOFF are defined in @ref uartdrv_config.h.

  To enable support for software flow control, perform the following steps:

  - Set @ref EMDRV_UARTDRV_FLOW_CONTROL_ENABLE to 1.
  - UART/USART: In the @ref UARTDRV_InitUart_t structure passed to
    @ref UARTDRV_InitUart(), set
    @ref UARTDRV_InitUart_t.fcType = uartdrvFlowControlSw.
  - LEUART: In the @ref UARTDRV_InitLeuart_t structure passed to
    @ref UARTDRV_InitLeuart(), set
    @ref UARTDRV_InitLeuart_t.fcType = uartdrvFlowControlSw.

  @note Software flow control is partial only.

  The application must monitor buffers and make decisions on when to send XON/
  XOFF. XON/XOFF can be sent to the peer using @ref UARTDRV_FlowControlSet().
  Though @ref UARTDRV_FlowControlSet() will pause the active transmit operation
  to send a flow control character, there is no way to guarantee the order.
  If the application implements a specific packet format where the flow control
  codes may appear only in fixed positions, the application should not
  use @ref UARTDRV_FlowControlSet() but implement read and write of XON/XOFF
  into packet buffers. If the application code fully implements all the flow
  control logic, EMDRV_UARTDRV_FLOW_CONTROL_ENABLE should be set to 0
  to reduce code space.

@n @section uartdrv_example Example
  @if DOXYDOC_P1_DEVICE
    @if DOXYDOC_EFM32G
    @include uartdrv_example_p1_nomvdis.c
    @endif
    @if DOXYDOC_EZR32HG
    @include uartdrv_example_p1_usart0.c
    @endif
    @ifnot (DOXYDOC_EFM32G || DOXYDOC_EZR32HG)
    @include uartdrv_example_p1.c
    @endif
  @endif
  @if DOXYDOC_P2_DEVICE
  @include uartdrv_example_p2.c
  @endif

 * @} end group UARTDRV *******************************************************
 * @} end group emdrv *********************************************************/

 #endif
