/***************************************************************************//**
 * @file
 * @brief Board support package API for GPIO leds on STK's.
 * @version 5.4.0
 *******************************************************************************
 * # License
 * <b>Copyright 2016 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include "bsp.h"
#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"

//****************************************************************************

#if (configUSE_LEDS > 0)


#define BSP_NO_OF_LEDS                BSP_LED_COUNT
#define BSP_GPIO_LEDARRAY_INIT        BSP_LED_INIT

typedef struct {
  GPIO_Port_TypeDef   port;
  unsigned int        pin;
} tLedIo;


static const tLedIo ledArray[BSP_NO_OF_LEDS] = BSP_GPIO_LEDARRAY_INIT;

int BSP_LedsInit(void)
{
  int ledNo;
  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  for (ledNo = 0; ledNo < BSP_NO_OF_LEDS; ledNo++) {
    GPIO_PinModeSet(ledArray[ledNo].port, ledArray[ledNo].pin, gpioModePushPull, 0);
  }

  return BSP_STATUS_OK;
}

uint32_t BSP_LedsGet(void)
{
  int ledNo;
  uint32_t retVal, mask;
  bool ledOn;

  for (ledNo = 0, retVal = 0, mask = 0x1; ledNo < BSP_NO_OF_LEDS; ledNo++, mask <<= 1 ) {
    ledOn = GPIO_PinOutGet(ledArray[ledNo].port, ledArray[ledNo].pin) != 0;
    if (ledOn) {
      retVal |= mask;
    }
  }
  return retVal;
}

int BSP_LedsSet(uint32_t leds)
{
  int ledNo;
  uint32_t mask;

  for (ledNo = 0, mask = 0x1; ledNo < BSP_NO_OF_LEDS; ledNo++, mask <<= 1) {
    if (leds & mask) {
      GPIO_PinOutSet(ledArray[ledNo].port, ledArray[ledNo].pin);
    } else {
      GPIO_PinOutClear(ledArray[ledNo].port, ledArray[ledNo].pin);
    }
  }
  return BSP_STATUS_OK;
}

int BSP_LedClear(int ledNo)
{
  if ((ledNo >= 0) && (ledNo < BSP_NO_OF_LEDS)) {
    GPIO_PinOutClear(ledArray[ledNo].port, ledArray[ledNo].pin);
    return BSP_STATUS_OK;
  }
  return BSP_STATUS_ILLEGAL_PARAM;
}

int BSP_LedGet(int ledNo)
{
  int retVal = BSP_STATUS_ILLEGAL_PARAM;

  if ((ledNo >= 0) && (ledNo < BSP_NO_OF_LEDS)) {
    retVal = (int)GPIO_PinOutGet(ledArray[ledNo].port, ledArray[ledNo].pin);
  }
  return retVal;
}

int BSP_LedSet(int ledNo)
{
  if ((ledNo >= 0) && (ledNo < BSP_NO_OF_LEDS)) {
    GPIO_PinOutSet(ledArray[ledNo].port, ledArray[ledNo].pin);
    return BSP_STATUS_OK;
  }
  return BSP_STATUS_ILLEGAL_PARAM;
}

int BSP_LedToggle(int ledNo)
{
  if ((ledNo >= 0) && (ledNo < BSP_NO_OF_LEDS)) {
    GPIO_PinOutToggle(ledArray[ledNo].port, ledArray[ledNo].pin);
    return BSP_STATUS_OK;
  }
  return BSP_STATUS_ILLEGAL_PARAM;
}

/** @endcond */
#endif /* #if (configUSE_LEDS > 0) */
