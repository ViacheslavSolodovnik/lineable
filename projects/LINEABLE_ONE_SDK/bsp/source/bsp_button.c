/***************************************************************************//**
 * @file
 * @brief Board support package API for Button GPIO
 * @version 
 *******************************************************************************
 * # License
 *******************************************************************************
 ******************************************************************************/
#include <bsphalconfig.h>
#include "bsp.h"
#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_gpio.h"
#include "gpiointerrupt.h"
//****************************************************************************
// This state is kept track of so the IRQ ISRs knows when to notify buttonIsrs.
#if (configUSE_BUTTON > 0)
// NO DEBOUNCE routine yet
#define USE_DEBOUNCE_CHECK  0 
static uint8_t buttonState;
static handle_button_callback handle_button_cb;
typedef struct {
  GPIO_Port_TypeDef   port;
  unsigned int        pin;
} tButtonArray;
static const tButtonArray buttonArray[BSP_BUTTON_COUNT] = BSP_BUTTON_INIT;

/** @brief Button state is pressed.
 */
#define BUTTON_PRESSED  1

/** @brief Button state is released.
 */
#define BUTTON_RELEASED 0

uint8_t BSP_ReadButtons(void)
{
    uint8_t i;
    uint8_t pin_bits = 0;
    for(i = 0; i < BSP_BUTTON_COUNT; i++){
        if(0 == GPIO_PinInGet(buttonArray[i].port, buttonArray[i].pin)){
            pin_bits |= (1<<i);
        }
    }
    return pin_bits;    
}

uint8_t BSP_ReadButton(uint8_t pin)
{
    uint8_t i;
    for ( i = 0; i < BSP_BUTTON_COUNT; i++ ) {
      if (buttonArray[i].pin == pin) {
        break;
      }
    }

    if( i != BSP_BUTTON_COUNT){
        return (GPIO_PinInGet(buttonArray[i].port, buttonArray[i].pin)) ? BUTTON_RELEASED : BUTTON_PRESSED;    
    }
    
    return BUTTON_RELEASED;
}



static void BSP_ButtonIsr(uint8_t pin)
{
  uint8_t buttonStateNow;
#if (USE_DEBOUNCE_CHECK > 0)
  uint8_t buttonStatePrev;
  uint32_t debounce;
#endif //(USE_DEBOUNCE_CHECK > 0)
  //buttonStateNow = BSP_ReadButton(pin);
  buttonStateNow = BSP_ReadButtons();
  
#if (USE_DEBOUNCE_CHECK > 0)
  //read button until get "DEBOUNCE" number of consistent readings
  for ( debounce = 0; debounce < USE_DEBOUNCE_CHECK; debounce = (buttonStateNow == buttonStatePrev) ? debounce + 1 : 0 ) {
        buttonStatePrev = buttonStateNow;
        //buttonStateNow = halButtonPinState(pin);
        buttonStateNow = BSP_ReadButtons();
  }
#endif //(DEBOUNCE > 0)

  //notify only when state changed
  if (buttonState != buttonStateNow) { 
    buttonState = buttonStateNow;
    if(handle_button_cb)
        handle_button_cb(buttonState);
  }
}


void BSP_ButtonInit(handle_button_callback cb)
{
    int i;
    handle_button_cb = cb;
    if(cb)
        GPIOINT_Init();
    /* Enable GPIO in CMU */
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_GPIO, true);
    for (i = 0; i < BSP_BUTTON_COUNT; i++ ) {
        /* Configure pin as input */
        GPIO_PinModeSet(buttonArray[i].port,
                        buttonArray[i].pin,
                        BSP_BUTTON_GPIO_MODE,
                        BSP_BUTTON_GPIO_DOUT);

       if(cb){
            /* only when there is cb for isr */
                        
            /* Register callbacks before setting up and enabling pin interrupt. */
            GPIOINT_CallbackRegister(buttonArray[i].pin,
                                     BSP_ButtonIsr);
            /* Set rising and falling edge interrupts */
            GPIO_IntConfig(buttonArray[i].port,
                              buttonArray[i].pin,
                              true,
                              true,
                              true);
        }
   }

   //GPIO_EM4EnablePinWakeup
}



#endif
