/********************************************************************************************************
*                                             EXAMPLE CODE
*********************************************************************************************************
* Copyright 2018 Silicon Laboratories, Inc.
 * @file bsp_os.c
 * @brief
 * @version Gecko SDK 2.4.0
 * @version Micrium OS 5.5.0
 *********************************************************************************************************
 * # License
 *   The licensor of this EXAMPLE CODE is Silicon Laboratories Inc.
 *   Silicon Laboratories Inc. grants you a personal, worldwide, royalty-free, fully paid-up license to
 *   use, copy, modify and distribute the EXAMPLE CODE software, or portions thereof, in any of your
 *   products.
 *   Your use of this EXAMPLE CODE is at your own risk. This EXAMPLE CODE does not come with any
 *   warranties, and the licensor disclaims all implied warranties concerning performance, accuracy,
 *   non-infringement, merchantability and fitness for your application.
 *   The EXAMPLE CODE is provided "AS IS" and does not come with any support.
 *********************************************************************************************************
 * Documentation:
 *   You can find user manuals, API references, release notes and more at: https://doc.micrium.com
 *********************************************************************************************************
 *   You can contact us at: https://www.micrium.com
 ********************************************************************************************************/

/*********************************************************************************************************
 *********************************************************************************************************
 *                                             INCLUDE FILES
 *********************************************************************************************************
 * Note(s) : (1) The kal_priv.h file is included only to be used by this file. Its content should remain
 *               private to rtos and not be used in an application's context.
 *********************************************************************************************************
 ********************************************************************************************************/

#include  <bsphalconfig.h>
#include  <bsp.h>

#include  <common/include/lib_def.h>
#include  <common/include/lib_utils.h>
#include  <common/include/rtos_utils.h>
#include  <common/include/rtos_path.h>
//#include  <common/source/kal/kal_priv.h>                   /* Private file, use should be limited. See Note #1.    */
#include  <rtos_description.h>

#ifdef  RTOS_MODULE_KERNEL_AVAIL
#include  <kernel/include/os.h>
#endif

#include  <em_chip.h>
#include  <em_cmu.h>
#include  <em_emu.h>
#include  <em_rtcc.h>

#include  <drivers/peripheral/i2c/micrium_i2c.h>

#if configUSE_SPI_FLASH
#include "bsp_spi_flash.h"
#endif
/*********************************************************************************************************
 *********************************************************************************************************
 *                                             LOCAL DEFINES
 *********************************************************************************************************
 ********************************************************************************************************/

 // Bit [19] in MODULEINFO is the HFXOCALVAL:
// 1=No factory cal, use default XO tunning value in FW
// 0=Factory Cal, use XO tunning value in DI
#define DEVINFO_MODULEINFO_HFXOCALVAL_MASK  0x00080000UL
// Calibration value for HFXO CTUNE is at DEVINFO Offset 0x08
#define DEVINFO_MODULEINFO_CRYSTALOSCCALVAL  (*((uint16_t *) (uint32_t)(DEVINFO_BASE+0x8UL)))
// [15:9] : (LFXOTUNING) Calibration for LFXO TUNING
// [8:0]  : (HFXOCTUNE) Calibration for HFXO CTUNE
#define DEVINFO_HFXOCTUNE_MASK  0x01FFUL

/*********************************************************************************************************
 *********************************************************************************************************
 *                                       LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************
 ********************************************************************************************************/

/*********************************************************************************************************
 *********************************************************************************************************
 *                                           GLOBAL FUNCTIONS
 *********************************************************************************************************
 ********************************************************************************************************/

/**********************************************************************************************************
*                                           BSP_SystemInit()
*
* Description : Initialize the Board Support Package (BSP).
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Note(s)     : (1) This function MUST be called before any other BSP function is called.
**********************************************************************************************************/
static periph_init_cb periph_init;// for wakeup from sleep

void BSP_SystemInit(periph_init_cb peripherals_initialization)
{
    periph_init = peripherals_initialization;
/**************************
    initMcu
**************************/
    CHIP_Init();                                                /* Chip revision alignment and errata fixes             */

#if (configUSE_DCDC>0)
    EMU_DCDCInit_TypeDef dcdcInit = BSP_DCDC_INIT;
    EMU_DCDCInit(&dcdcInit);                                 /* Initialize DCDC regulator                            */
    //EMU_DCDCModeSet(emuDcdcMode_Bypass);
#else
    EMU_DCDCPowerOff();                                     /* DCDC is not used to we power it off                  */
#endif

    // Set up clocks, initMcu_clocks()
    // Initialize HFXO
    CMU_HFXOInit_TypeDef hfxoInit = BSP_CLK_HFXO_INIT;

    if (0==(DEVINFO->MODULEINFO & DEVINFO_MODULEINFO_HFXOCALVAL_MASK)) {
        hfxoInit.ctuneSteadyState = DEVINFO_MODULEINFO_CRYSTALOSCCALVAL & DEVINFO_HFXOCTUNE_MASK;
    }

#if defined(BSP_CLK_HFXO_CTUNE) && BSP_CLK_HFXO_CTUNE > -1
    hfxoInit.ctuneStartup = BSP_CLK_HFXO_CTUNE;
    hfxoInit.ctuneSteadyState = BSP_CLK_HFXO_CTUNE;
#endif
    CMU_HFXOInit(&hfxoInit);

    // Set system HFXO frequency
    SystemHFXOClockSet(BSP_CLK_HFXO_FREQ);

    // Enable HFXO oscillator, and wait for it to be stable
    CMU_OscillatorEnable(cmuOsc_HFXO, true, true);

    // Enable HFXO Autostart only if EM2 voltage scaling is disabled.
    // In 1.0 V mode the chip does not support frequencies > 21 MHz,
    // this is why HFXO autostart is not supported in this case.
#if!defined(_EMU_CTRL_EM23VSCALE_MASK)
    // Automatically start and select HFXO
    CMU_HFXOAutostartEnable(0, true, true);
#else
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
#endif//_EMU_CTRL_EM23VSCALE_MASK


    // HFRCO not needed when using HFXO
    CMU_OscillatorEnable(cmuOsc_HFRCO, false, false);

    // Enabling HFBUSCLKLE clock for LE peripherals
    CMU_ClockEnable(cmuClock_HFLE, true);

    // Initialize LFXO
    CMU_LFXOInit_TypeDef lfxoInit = BSP_CLK_LFXO_INIT;
    lfxoInit.ctune = BSP_CLK_LFXO_CTUNE;
    CMU_LFXOInit(&lfxoInit);

    // Set system LFXO frequency
    SystemLFXOClockSet(BSP_CLK_LFXO_FREQ);

    // Set LFXO if selected as LFCLK
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
    CMU_ClockSelectSet(cmuClock_LFE, cmuSelect_LFXO);

    RTCC_Init_TypeDef rtccInit = RTCC_INIT_DEFAULT;
    rtccInit.enable                = true;
    rtccInit.debugRun              = false;
    rtccInit.precntWrapOnCCV0      = false;
    rtccInit.cntWrapOnCCV1         = false;
    rtccInit.prescMode             = rtccCntTickPresc;
    rtccInit.presc                 = rtccCntPresc_1;   // 30.5 us
    rtccInit.enaOSCFailDetect      = false;
    rtccInit.cntMode               = rtccCntModeNormal;
    RTCC_Init(&rtccInit);

#if defined(_EMU_CMD_EM01VSCALE0_MASK)
    // Set up EM0, EM1 energy mode configuration
    EMU_EM01Init_TypeDef em01Init = EMU_EM01INIT_DEFAULT;
    EMU_EM01Init(&em01Init);
#endif // _EMU_CMD_EM01VSCALE0_MASK

#if defined(_EMU_CTRL_EM23VSCALE_MASK)
    // Set up EM2, EM3 energy mode configuration
    EMU_EM23Init_TypeDef em23init = EMU_EM23INIT_DEFAULT;
    em23init.vScaleEM23Voltage = emuVScaleEM23_LowPower;
    EMU_EM23Init(&em23init);
#endif //_EMU_CTRL_EM23VSCALE_MASK

/**************************
initBoard()
*************************/

   // Enable clock for CRYOTIMER
    CMU_ClockEnable(cmuClock_CRYOTIMER, true);
    // Enable clock for PRS
    CMU_ClockEnable(cmuClock_PRS, true);

    // Enable GPIO clock source
    CMU_ClockEnable(cmuClock_GPIO, true);

    if (periph_init != DEF_NULL) {
      periph_init();                          // Power on peripherals and GPIOs
    }

/**************
void initApp(void)
***************/

    // Initialize CPU and make all interrupts Kernel Aware.CPU_Init() must be called before calling OSInit().
    BSP_CPUInit();


}

/*********************************************************************************************************
 *                                             BSP_TickInit()
 *
 * Description : Initialize the Board Support Package (BSP). This function is called from the start task
 *               after initializing the kernel. It should initialize the kernel ticking source.
 *
 * Argument(s) : None.
 *
 * Return(s)   : None.
 *
 * Note(s)     : None.
 ********************************************************************************************************/
void BSP_TickInit(void)
{
#if (!defined(OS_CFG_DYN_TICK_EN) || OS_CFG_DYN_TICK_EN == DEF_DISABLED)
  CPU_INT32U cpu_freq;
  CPU_INT32U cnts;
#endif

#if (defined(OS_CFG_DYN_TICK_EN) && OS_CFG_DYN_TICK_EN == DEF_ENABLED)
                                                                // Init uC/OS dynamic  time src
  BSP_RTCC_TickInit();
#elif (OS_CFG_TASK_TICK_EN == DEF_ENABLED)
  cpu_freq =  SystemCoreClockGet();                           // Determine SysTick reference freq.
  cnts     = (cpu_freq / (CPU_INT32U)OSCfg_TickRate_Hz);      // Cal. SysTick counts between two OS tick interrupts.

  OS_CPU_SysTickInit(cnts);                                   // Init uC/OS periodic time src (SysTick).
#endif
}

/*********************************************************************************************************
 *                                           BSP_PeriphInit()
 *
 * Description : Initialize the Board Support Package (BSP). This function is called from the start task
 *               after initializing the kernel.
 *
 * Argument(s) : None.
 *
 * Return(s)   : None.
 *
 * Note(s)     : None.
 ********************************************************************************************************/
void BSP_PeriphInit(void)
{
#if (configUSE_LEDS > 0)
/* GPIO LED port int */
    BSP_LedsInit();
#endif

i2cm_init();

#if configUSE_SPI_FLASH
    spi_flash_init(&ext_flash, "MX25L12835F");
#endif

}
