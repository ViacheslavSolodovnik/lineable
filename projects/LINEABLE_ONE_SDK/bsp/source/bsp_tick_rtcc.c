/*****************************************************************************************************//**
 *                                             EXAMPLE CODE
 *********************************************************************************************************
 * Copyright 2018 Silicon Laboratories, Inc.
 * @file bsp_tick_rtcc.c
 * @brief BSP Dynamic Tick using RTCC - Silicon Labs
 * @version Gecko SDK 2.4.0
 * @version Micrium OS 5.5.0
 *********************************************************************************************************
 * # License
 *   The licensor of this EXAMPLE CODE is Silicon Laboratories Inc.
 *   Silicon Laboratories Inc. grants you a personal, worldwide, royalty-free, fully paid-up license to
 *   use, copy, modify and distribute the EXAMPLE CODE software, or portions thereof, in any of your
 *   products.
 *   Your use of this EXAMPLE CODE is at your own risk. This EXAMPLE CODE does not come with any
 *   warranties, and the licensor disclaims all implied warranties concerning performance, accuracy,
 *   non-infringement, merchantability and fitness for your application.
 *   The EXAMPLE CODE is provided "AS IS" and does not come with any support.
 *********************************************************************************************************
 * Documentation:
 *   You can find user manuals, API references, release notes and more at: https://doc.micrium.com
 *********************************************************************************************************
 *   You can contact us at: https://www.micrium.com
 ********************************************************************************************************/

/*********************************************************************************************************
 *********************************************************************************************************
 *                                             INCLUDE FILES
 *********************************************************************************************************
 ********************************************************************************************************/


#include <bsp.h>
#include <kernel/include/os.h>
#include "em_cmu.h"
#include "em_rtcc.h"
#include "em_emu.h"
#include "timer.h"
#include "arm_math.h"//#include <math.h>
#if (OS_CFG_DYN_TICK_EN == DEF_ENABLED)

/*********************************************************************************************************
 *********************************************************************************************************
 *                                            LOCAL DEFINES
 *********************************************************************************************************
 ********************************************************************************************************/

#define  RTCC_OSCILLATOR_FREQ                     (32768u)
#define  RTCC_PRESCALER                           (1u)
#define  RTCC_CLOCK                               (cmuSelect_LFXO)
#define  BSP_OS_RTCC_TICK_RATE_HZ                 (RTCC_OSCILLATOR_FREQ / RTCC_PRESCALER)
#define  BSP_OS_RTCCTICK_TO_OSTICK(rtcctick)      (((rtcctick) * OSCfg_TickRate_Hz) / BSP_OS_RTCC_TICK_RATE_HZ)
#define  BSP_OS_OSTICK_TO_RTCCTICK(ostick)        (((ostick) * BSP_OS_RTCC_TICK_RATE_HZ) / OSCfg_TickRate_Hz)

#define RTC_LORA_CHANNEL      1                                 /* use RTCC 1 channel for LoRa                        */
#define RTC_TICKS_PER_S       RTCC_OSCILLATOR_FREQ
#define RTC_TICKS_PER_MS      32.768                            /* RTCC_OSCILLATOR_FREQ / 1000                        */
#define RTC_TICK_DURATION_MS  0.030517578                       /* 1 / (RTCC_OSCILLATOR_FREQ / 1000)                  */

/*********************************************************************************************************
 *********************************************************************************************************
 *                                       LOCAL GLOBAL VARIABLES
 *********************************************************************************************************
 ********************************************************************************************************/

// Number of ticks scheduled to pass at the next RTCC event.
// This is stored in OS Tick units
static OS_TICK BSP_OS_TicksToGo;

// The number of OS ticks that have passed at the last time
// we updated the OS time. This is stored in OS Tick units
static OS_TICK BSP_OS_LastTick;

// Value of the RTCC counter at the last OS tick event. Note that
// this value is always aligned on OS Tick'
static OS_TICK BSP_OS_LastRTCCTick;

/** RTC variables. Used for converting RTC counter to system time */
static uint32_t   rtcOverflowCounter    = 0;
static uint32_t   rtcOverflowInterval   = 0;
static uint32_t   rtcOverflowIntervalR  = 0;

/** Thermometer calibration variables */

/** Temperature compensation variables */
static int32_t    tempCompAccumulator   = 0;

static TimerTime_t   previousTime = 0;

/** Flag used to indicates a the MCU has waken-up from an external IRQ */
volatile bool NonScheduledWakeUp = false;

/**
 * Flag to indicate if the timestamps until the next event is long enough
 * to set the MCU into low power mode
 */
static bool RtcTimerEventAllowsLowPower = false;

/**
 * Flag to disable the LowPower Mode even if the timestamps until the
 * next event is long enough to allow Low Power mode
 */
static bool LowPowerDisableDuringTask = false;

/** Indicates if the RTC is already Initialized or not */
static bool RtcInitalized = false;

/** Hold the Wake-up time duration in ms */
volatile uint32_t McuWakeUpTime = 0;

/** Hold the cumulated error in micro-second to compensate the timing errors */
static int32_t TimeoutValueError = 0;

/*********************************************************************************************************
 *********************************************************************************************************
 *                                      LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************
 ********************************************************************************************************/

static void RTCC_TickHandler(void);
static void RTCC_UpdateTicks(void);

/*********************************************************************************************************
 *********************************************************************************************************
 *                                           GLOBAL FUNCTIONS
 *********************************************************************************************************
 ********************************************************************************************************/

/*********************************************************************************************************
 *                                          BSP_RTCC_TickInit()
 *
 * Description : Initialize the RTCC to enable dynamic ticking.
 *
 * Argument(s) : none.
 *
 * Return(s)   : none.
 *
 * Caller(s)   : BSP_Tick_Init.
 *
 *               This function is an INTERNAL uC/OS-III function & MUST NOT be called by application
 *               function(s).
 *
 * Note(s)     : none.
 ********************************************************************************************************/
void BSP_RTCC_TickInit(void)
{
  RTCC_Init_TypeDef     init = RTCC_INIT_DEFAULT;
  RTCC_CCChConf_TypeDef channel = RTCC_CH_INIT_COMPARE_DEFAULT;

  init.enable = false;
  init.presc = (RTCC_CntPresc_TypeDef) CMU_DivToLog2(RTCC_PRESCALER);

  CMU_ClockSelectSet(cmuClock_LFE, RTCC_CLOCK);
  CMU_ClockEnable(cmuClock_RTCC, true);
  CMU_ClockEnable(cmuClock_HFLE, true);

  // Initialize and start the RTCC
  RTCC_Init(&init);
  RTCC_ChannelInit(2, &channel);
  RTCC_ChannelCCVSet(2, 0xFFFFFFFF);

  /** Reset overflow counter */
  rtcOverflowCounter = 0;

  /** Calculate overflow interval based on RTC counter width and frequency */
  rtcOverflowInterval   = ((0x00FFFFFF+1) / RTC_TICKS_PER_MS);
  rtcOverflowIntervalR  = ((0x00FFFFFF+1) - (rtcOverflowInterval * RTC_TICKS_PER_MS)); // remainder

  RtcInitalized = true;

  RTCC_ChannelInit(RTC_LORA_CHANNEL, &channel);
  RTCC_ChannelCCVSet(RTC_LORA_CHANNEL, 0xFFFFFFFF);

  RTCC_IntEnable(RTCC_IEN_OF);
  RTCC_Enable(true);

  CPU_IntSrcHandlerSetKA(RTCC_IRQn + CPU_INT_EXT0, RTCC_TickHandler);
  NVIC_EnableIRQ(RTCC_IRQn);
}

/*********************************************************************************************************
 *                                        RTCC_TickHandler()
 *
 * Description : Handle RTCC global interrupt.
 *
 * Argument(s) : none.
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is called from the RTCC interrupt handler.
 *
 * Note(s)     : none.
 ********************************************************************************************************/
static void RTCC_TickHandler(void)
{
  CPU_BOOLEAN overflow = DEF_NO;
  CPU_BOOLEAN channel2Interrupt;

  overflow = ((RTCC_IntGetEnabled() & RTCC_IF_OF) != 0u) ? DEF_YES : DEF_NO;
  channel2Interrupt = ((RTCC_IntGetEnabled() & RTCC_IF_CC2) != 0u) ? DEF_YES : DEF_NO;

#if (defined(EMBER_STACK_ZIGBEE) || defined(EMBER_STACK_CONNECT))
  RTCC_IRQHandler();
#endif

  if (overflow) {
    RTCC_IntClear(RTCC_IF_OF);

    // If an OS tick is scheduled then the BSP_OS_UpdateTicks
    // function will be called when the CC2 interrupt fires.
    // If no OS tick is scheduled then we need to update the
    // tick count here.
    if (BSP_OS_TicksToGo == 0) {
      // Updated OS ticks based on RTCC duration
      RTCC_UpdateTicks();
    }

    rtcOverflowCounter++;
  }

  if (channel2Interrupt) {
    RTCC_IntClear(RTCC_IF_CC2);
    EFM_ASSERT(BSP_OS_TicksToGo != 0);
    /* Updated OS ticks based on RTCC duration */
    RTCC_UpdateTicks();
    BSP_OS_TicksToGo = 0;

    RTCC_IntDisable(RTCC_IF_CC2);
    RTCC_ChannelCCVSet(2, DEF_INT_32U_MAX_VAL);
  }

  if (RTCC_IntGetEnabled() & RTCC_IF_CC1) {
    RTCC_IntDisable(RTCC_IF_CC1);
    TimerIrqHandler();
  }
}

/*********************************************************************************************************
 *                                            BSP_OS_TickGet()
 *
 * Description : Get the OS Tick Counter as if it was running continuously.
 *
 * Argument(s) : none.
 *
 * Return(s)   : The effective OS Tick Counter.
 *
 * Caller(s)   : OS_TaskBlock, OS_TickListInsertDly and OSTimeGet.
 *
 *               This function is an INTERNAL uC/OS-III function & MUST NOT be called by application
 *               function(s).
 *
 * Note(s)     : The RTCC is running continuously, however we cannot use the RTCC
 *               directly as the timebase because the RTCC is wrapping much faster
 *               than the OS tick.
 ********************************************************************************************************/
OS_TICK BSP_OS_TickGet(void)
{
  OS_TICK tick = 0;
  OS_TICK prev;
  OS_TICK diff;

  // Read the tick count repeatedly until two consecutive reads are equal.
  // This is done to handle the case when the RTCC wraps
  do {
    prev = tick;
    diff = RTCC_CounterGet() - BSP_OS_LastRTCCTick;
    tick = BSP_OS_LastTick + BSP_OS_RTCCTICK_TO_OSTICK(diff);
  } while (tick != prev);

  return tick;
}

/*********************************************************************************************************
 *                                          BSP_OS_TickNextSet()
 *
 * Description : Set the number of OS Ticks to wait before calling OSTimeDynTick.
 *
 * Argument(s) : ticks       number of OS Ticks to wait.
 *
 * Return(s)   : Number of effective OS Ticks until next OSTimeDynTick.
 *
 * Caller(s)   : OS_TickTask and OS_TickListInsert.
 *
 *               This function is an INTERNAL uC/OS-III function & MUST NOT be called by application
 *               function(s).
 *
 * Note(s)     : none.
 ********************************************************************************************************/
OS_TICK BSP_OS_TickNextSet(OS_TICK ticks)
{
  CPU_INT32U rtccticks = 0u;
  CPU_INT32U counter;
  CPU_INT32U diff;

  CPU_SR_ALLOC();

  // Check if OSTimeDynTick needs to be called.
  if ((ticks != (OS_TICK) -1) && (ticks != 0)) {
    // Disable event generation.
    RTCC_IntDisable(RTCC_IEN_CC2);

    // Do not count pending ticks twice.
    CPU_CRITICAL_ENTER();
    ticks -= OSTickCtrPend;
    CPU_CRITICAL_EXIT();

    //Convert OS Ticks to RTCC ticks.
    //
    // Note that this way of calculating ticks will make the RTCC tick
    // interrupt happen at an offset from original OS Ticks. So a tick
    // is scheduled in the future in x os tick time but it is important
    // to know that the RTCC interrupt is not aligned with an os tick.
    // To align it with an os tick you would have to adjust the RTCC
    // interrupt time down to nearest os tick.
    //
    // When an RTCC interrupt occurs you would only know that at leat
    // x os ticks have passed.
    rtccticks = BSP_OS_OSTICK_TO_RTCCTICK(ticks);
    BSP_OS_TicksToGo = ticks;

    // Count the time that passed between calls to this function without
    // generating an RTCC interrupt.
    diff = RTCC_CounterGet() - BSP_OS_LastRTCCTick;
    counter = BSP_OS_RTCCTICK_TO_OSTICK(diff);
    if (counter > 0u) {
      CPU_CRITICAL_ENTER();
      // Update the OS with actual time passed based on the RTCC.
      RTCC_UpdateTicks();
      CPU_CRITICAL_EXIT();
    }

    /* Jira:MICRIUM-1457
     * When we are scheduling only few ticks in the feature, we might end up
     * scheduling the RTCC clock in the past. A work-around is to simply add a
     * couple of extra ticks to make sure we are always scheduling in the
     * future. */
    if (rtccticks <= 5) {
      rtccticks += 2;
    }

    /* Set channel 2 compare value and enable interrupt. */
    RTCC_ChannelCCVSet(2, RTCC_CounterGet() + rtccticks);
    RTCC_IntClear(RTCC_IFC_CC2);
    RTCC_IntEnable(RTCC_IEN_CC2);
  } else {
    // If not, still keep track of time.
    // Overflow in (2^32-1)/BSP_OS_RTCC_TICK_RATE_HZ seconds
    BSP_OS_TicksToGo = 0;
  }

  return BSP_OS_TicksToGo;
}

/*********************************************************************************************************
 *                                          RTCC_UpdateTicks()
 *
 * Description : Updates the number of pending OS Ticks and the internal time reference.
 *
 * Argument(s) : none.
 *
 * Return(s)   : none.
 *
 * Caller(s)   : RTCC Interrupt handler and BSP_OS_TickNextSet.
 *
 * Note(s)     : none.
 ********************************************************************************************************/
#ifdef  RTOS_MODULE_COMMON_CLK_AVAIL
clk_signal_cb   clk_update_cb = DEF_NULL; /* connect Clk module */
#endif
static void RTCC_UpdateTicks(void)
{
  // Find time since last RTCC OS tick.
  uint32_t rtccCnt = RTCC_CounterGet();
  uint32_t timeElapsed = rtccCnt - BSP_OS_LastRTCCTick;
  uint32_t tickElapsed = BSP_OS_RTCCTICK_TO_OSTICK(timeElapsed);
  if (tickElapsed > 0u) {
    // Tell the OS how many ticks that have passed
    OSTimeDynTick(tickElapsed);

    // Calculate the last point in RTCC time when there should have been an OS tick
    BSP_OS_LastTick += tickElapsed;
    uint32_t rtccDiff = rtccCnt % BSP_OS_OSTICK_TO_RTCCTICK(1);
    BSP_OS_LastRTCCTick = rtccCnt - rtccDiff;

#ifdef  RTOS_MODULE_COMMON_CLK_AVAIL
	// this is to avoid this func is called before init Clk module 
    if(clk_update_cb){
        clk_update_cb(tickElapsed);
    }
#endif 
  }  
}

void RtcSetTimeout( uint32_t timeout )
{
  RTCC_IntClear(RTCC_IF_CC1);

  previousTime = RtcGetTimerValue();

  if (timeout < 1)
  {
    timeout = 1;
  }

  // timeoutValue is used for complete computation
  double timeoutValue = round( timeout * RTC_TICKS_PER_MS );

  // timeoutValueTemp is used to compensate the cumulating errors in timing far in the future
  double timeoutValueTemp =  ( double )timeout * RTC_TICKS_PER_MS;

  // Compute timeoutValue error
  double error = timeoutValue - timeoutValueTemp;

  // Add new error value to the cumulated value in uS
  TimeoutValueError += ( error * 1000 );

  // Correct cumulated error if greater than ( RTC_ALARM_TICK_DURATION * 1000 )
  if( TimeoutValueError >= ( int32_t )( RTC_TICK_DURATION_MS * 1000 ) )
  {
    TimeoutValueError = TimeoutValueError - ( uint32_t )( RTC_TICK_DURATION_MS * 1000 );
    timeoutValue = timeoutValue + 1;
  }

  // Rounding errors should not cause us to set the number behind the current time
  uint32_t value = (previousTime * RTC_TICKS_PER_MS) + timeoutValue;
  if (RTCC_CounterGet() > value
      && value + RTC_TICKS_PER_MS > RTCC_CounterGet())
  {
    RTCC_ChannelCCVSet(RTC_LORA_CHANNEL, RTCC_CounterGet() + 1);
  }
  else
  {
    RTCC_ChannelCCVSet(RTC_LORA_CHANNEL, value);
  }

  RTCC_IntEnable(RTCC_IF_CC1);
}

TimerTime_t RtcGetAdjustedTimeoutValue( uint32_t timeout )
{
    if( timeout > McuWakeUpTime )
    {   // we have waken up from a GPIO and we have lost "McuWakeUpTime" that we need to compensate on next event
        if( NonScheduledWakeUp == true )
        {
            NonScheduledWakeUp = false;
            timeout -= McuWakeUpTime;
        }
    }

    if( timeout > McuWakeUpTime )
    {
      RtcTimerEventAllowsLowPower = true;
      /*
      // we don't go in Low Power mode for delay below 50ms (needed for LEDs)
      if (timeout < 50)
        {
            RtcTimerEventAllowsLowPower = false;
        }
        else
        {
            RtcTimerEventAllowsLowPower = true;
            timeout -= McuWakeUpTime;
        }*/
    }
    return  timeout;
}

TimerTime_t RtcGetTimerValue( void )
{
  TimerTime_t t = 0;

  // Add time based on number of counter overflows
  //  t += rtcOverflowCounter * rtcOverflowInterval;

  // Add remainder if the overflow interval is not an integer
  //  if ( rtcOverflowIntervalR != 0 )
  //  {
  //    t += (rtcOverflowCounter * rtcOverflowIntervalR) / RTC_TICKS_PER_MS;
  //  }

  // Add the number of milliseconds for RTC
  t += ( RTCC_CounterGet() / RTC_TICKS_PER_MS );

  // Add compensation for crystal temperature drift
  t += tempCompAccumulator;

  return t;
}

TimerTime_t RtcGetElapsedAlarmTime( void )
{
    TimerTime_t currentTime = RtcGetTimerValue();
    if( currentTime < previousTime )
    {
        return( currentTime + ( rtcOverflowInterval - previousTime ) );
    }
    else
    {
        return( currentTime - previousTime );
    }
}

TimerTime_t RtcComputeFutureEventTime( TimerTime_t futureEventInTime )
{
    return( RtcGetTimerValue( ) + futureEventInTime );
}

TimerTime_t RtcComputeElapsedTime( TimerTime_t eventInTime )
{
    TimerTime_t elapsedTime = 0;

    // Needed at boot, cannot compute with 0 or elapsed time will be equal to current time
    if( eventInTime == 0 )
    {
        return 0;
    }

    elapsedTime = RtcGetTimerValue();
    if( elapsedTime < eventInTime )
    { // roll over of the counter
        return( elapsedTime + ( rtcOverflowInterval - eventInTime ) );
    }
    else
    {
        return( elapsedTime - eventInTime );
    }
}

void BlockLowPowerDuringTask ( bool status )
{
    LowPowerDisableDuringTask = status;
}

void RtcEnterLowPowerStopMode( void )
{
  #if 0
  if( ( LowPowerDisableDuringTask == false ) && ( RtcTimerEventAllowsLowPower == true ) )
  {
    //EMU_EnterEM1();

    if (Radio.GetStatus() == RF_IDLE)
    {
      EMU_EnterEM2(true);
    }
    else
    {
      EMU_EnterEM1();
    }
  }
  #endif
}

void RtcRecoverMcuStatus( void )
{
  EFM_ASSERT(false);
}

TimerTime_t RtcTempCompensation( TimerTime_t period, float temperature )
{
  // stub function
  return period;
}

void HAL_Delay (uint32_t ms)
{
  uint32_t startTime = RtcGetTimerValue();
  while((RtcGetTimerValue() - startTime) < ms); // busy wait until at least ms ticks have passed
}


#endif // (OS_CFG_DYN_TICK_EN == DEF_ENABLED)
