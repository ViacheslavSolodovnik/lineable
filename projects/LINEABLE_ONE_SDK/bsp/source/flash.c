/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdio.h>
#include "rtos_app.h"

#include "em_system.h"
#include "em_msc.h"

#include "flash.h"

/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/

#define USERPAGE                (USERDATA_BASE)

/* Security Flag */
#define FLASH_WRITE_ENABLED             0x00    /* Flash Memory write enabled security flag */
#define FLASH_CHIP_ERASE_DISABLED       0x01    /* Chip Flash Memory erase disabled security flag */
#define FLASH_BLOCK_ERASE_DISABLED      0x02    /* Block Flash Memory erase disabled security flag */
#define FLASH_WRITE_DISABLED            0x04    /* Flash Memory write disabled security flag */

#define FLASHCheckAddress(a,l) (((uint32_t)(a) + (l)) > ((((void*)(a)) >= (void*)USERPAGE) ? (USERPAGE + 512) : FLASHGetTotalSize()))

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/

uint32_t FLASHGetPageSize(void)
{
    return SYSTEM_GetFlashPageSize();
}

uint32_t FLASHGetTotalSize(void)
{
    return (SYSTEM_GetFlashSize() * 1024UL);
}

void FLASHOpen(void)
{
    MSC_Init();
}

void FLASHClose(void)
{
    MSC_Deinit();
}

int8_t FLASHEraseBlock(void *BlockAddress)
{
    /* Calculate page starting address */
    uint32_t *add = (uint32_t *)(((uint32_t)BlockAddress / FLASH_PAGE_SIZE) * FLASH_PAGE_SIZE);
    if (FLASHCheckAddress(add, 0)) return FLASH_PARAMETER_ERROR;
    return (int8_t)MSC_ErasePage(add);
}

int8_t FLASHBlankCheck(void *BlockAddress)
{
    /* Calculate page starting address */
    uint32_t *add = (uint32_t *)(((uint32_t)BlockAddress / FLASH_PAGE_SIZE) * FLASH_PAGE_SIZE);
    if (FLASHCheckAddress(add, 0)) return FLASH_PARAMETER_ERROR;
    return FLASHBlankBufferCheck((uint8_t *)add, FLASH_PAGE_SIZE);
}

int8_t FLASHBlankBufferCheck(uint8_t *Buffer, uint16_t Len)
{
    if (FLASHCheckAddress(Buffer, Len)) return FLASH_PARAMETER_ERROR;
    while (Len--)
        if (*(Buffer++) != 0xFF) return 0;
    return 1;
}

int8_t FLASHWrite(void *Address, uint8_t *Buffer, uint16_t Count)
{
    if (FLASHCheckAddress(Address, Count)) return FLASH_PARAMETER_ERROR;
    return (int8_t)MSC_WriteWord((uint32_t *) Address, (void *) Buffer, Count);
}

int8_t FLASHEraseUserData(void)
{
    return (int8_t)MSC_ErasePage((uint32_t *)USERPAGE);
}

int8_t FLASHWriteUserData(uint16_t Offset, uint8_t *Buffer, uint16_t Count)
{
    if ((Offset + Count) > 512) return FLASH_PARAMETER_ERROR;
    return (int8_t)MSC_WriteWord((uint32_t *)(USERPAGE + Offset), (void const *) Buffer, (uint32_t)Count);
}

int8_t FLASHWriteVerify(void *Address, uint8_t *Buffer, uint16_t Count)
{
    int8_t ret = FLASHWrite(Address, Buffer, Count);
    if (ret == FLASH_NO_ERROR) {
        while (Count--) {
            if (*((uint8_t *)Address++) != *(Buffer++)) return FLASH_WRITE_VERIFY_ERROR;
        }
    }
    return ret;
}

int8_t FLASHBlankCheckUserData(void)
{
    return FLASHBlankBufferCheck((uint8_t *)USERPAGE, 512);
}

int8_t FLASHWriteVerifyUserData(uint16_t Offset, uint8_t *Buffer, uint16_t Count)
{
    int8_t ret = FLASHWriteUserData(Offset, Buffer, Count);
    if (ret == FLASH_NO_ERROR) {
        uint8_t *p = (uint8_t *)(USERPAGE + Offset);
        while (Count--) {
            if (*(p++) != *(Buffer++)) return FLASH_WRITE_VERIFY_ERROR;
        }
    }
    return ret;
}

int8_t FLASHSetProtectedAddress(uint8_t *StartingAddress)
{
    /* Calculate page starting address */
    uint32_t add = ((uint32_t)StartingAddress / FLASH_PAGE_SIZE);
    if (FLASHCheckAddress((void *)(add * FLASH_PAGE_SIZE), 0)) return FLASH_PARAMETER_ERROR;
    int lock = *((int *)(0x0FE04000 + (add >> 5)));
    lock &= ~(0x0001 << (add & 0x001F));
    int8_t rc = (int8_t)FLASHWrite((void *)(0x0FE04000 + (add >> 5)), (uint8_t *)&lock, sizeof(lock));
    return (rc == FLASH_NO_ERROR) ? FLASH_WRITE_DISABLED : rc;
}

int8_t FLASHGetProtectedAddress(uint8_t *StartingAddress)
{
    /* Calculate page starting address */
    uint32_t add = ((uint32_t)StartingAddress / FLASH_PAGE_SIZE);
    if (FLASHCheckAddress((void *)(add * FLASH_PAGE_SIZE), 0)) return FLASH_PARAMETER_ERROR;
    int lock = *((int *)(0x0FE04000 + (add >> 5)));
    return (lock & (0x0001 << (add & 0x001F))) ? FLASH_WRITE_ENABLED : FLASH_WRITE_DISABLED;
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

