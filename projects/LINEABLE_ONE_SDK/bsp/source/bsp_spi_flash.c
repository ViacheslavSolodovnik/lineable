/*

on going....

 */

#include  <bsphalconfig.h>
#include  <stddef.h>
#include  <stdio.h>
#include  <string.h>
#include  <cpu/include/cpu.h>
#include  <common/include/lib_mem.h>
#include  <common/include/rtos_err.h>
#include  <common/source/rtos/rtos_utils_priv.h>

#include "em_gpio.h"
#include "em_usart.h"
#include "em_cmu.h"
#include "bsp_spi_flash.h"
#include "spidrv.h"

#if configUSE_SPI_FLASH

//reference
//https://github.com/RT-Thread/realboard-stm32f4/blob/master/software/examples/drivers/spi_flash_w25qxx.c

// Flash control register mask define
// status register
#define    FLASH_MASK_WIP         0x01   /* write in progress */
#define    FLASH_MASK_LDSO        0x02
#define    FLASH_MASK_QE          0x40
// security register
#define    FLASH_MASK_OTPLOCK     0x03
#define    FLASH_4BYTE_MASK       0x04
#define    FLASH_WPSEL_MASK       0x80
// configuration reigster
#define    FLASH_DC_MASK          0x80
#define    FLASH_DC_2BIT_MASK     0xC0
// other
#define    BLOCK_PROTECT_MASK     0xff
#define    BLOCK_LOCK_MASK        0x01

/*
  System Information Define
*/
#define    CLK_PERIOD                26 // unit: ns
#define    Min_Cycle_Per_Inst        1  // cycle count of one instruction
#define    One_Loop_Inst             10 // instruction count of one loop (estimate)


/* command list */
#define FLASH_CMD_RDID          0x9F    // RDID (Read Identification) JEDEC ID
#define FLASH_CMD_RES           0xAB    // RES (Read Electronic ID), Release from DP, and Read Signature
#define FLASH_CMD_REMS          0x90    // REMS (Read Electronic & Device ID)


#define FLASH_CMD_WRSR          0x01  // Write Status Register
#define FLASH_CMD_PP            0x02  // Page Program
#define FLASH_CMD_READ          0x03  // READ Bytes (1 x I/O)
#define FLASH_CMD_FAST_READ     0x0B  // Read Data Bytes at Higher Speed
#define FLASH_CMD_2READ         0xBB    //2READ (2 x I/O)
#define FLASH_CMD_4READ         0xEB    //4READ (4 x I/O)

#define FLASH_CMD_DREAD         0x3B    //DREAD (1In/2 Out fast read)
#define FLASH_CMD_QREAD         0x6B    //QREAD (1In/4 Out fast read)
#define FLASH_CMD_RDSFDP        0x5A    //RDSFDP (Read SFDP)


#define FLASH_CMD_WREN          0x06    //WREN (Write Enable)
#define FLASH_CMD_WRDI          0x04    //WRDI (Write Disable)

#define FLASH_CMD_RDSR          0x05    // Read Status Register-1
#define FLASH_CMD_RDSR2         0x35    // Read Status Register-2 ???

#define FLASH_CMD_SE            0x20   // Sector (4K) Erase
#define FLASH_CMD_ERASE_32K     0x52   // 32KB Block Erase
#define FLASH_CMD_BE            0xD8    // Block (64K) Erase
#define FLASH_CMD_CE2           0x60    // CE (Chip Erase) hex code: 60 or C7
#define FLASH_CMD_CE            0xC7    // Chip Erase */

#define FLASH_CMD_DP		    0xB9    // Deep Power-down

#define FLASH_CMD_WRSCUR        0x2F    //WRSCUR (Write Security Register)
#define FLASH_CMD_RDSCUR        0x2B    //RDSCUR (Read Security Register)
#define FLASH_CMD_RDCR          0x15    //RDCR (Read Configuration Register)

//Reset comands
#define FLASH_CMD_RSTEN         0x66    //RSTEN (Reset Enable)
#define FLASH_CMD_RST           0x99    //RST (Reset Memory)

//Suspend/Resume comands
#define FLASH_CMD_PGM_ERS_S     0xB0    //PGM/ERS Suspend (Suspends Program/Erase)
#define FLASH_CMD_PGM_ERS_R     0x30    //PGM/ERS Erase (Resumes Program/Erase)
#define FLASH_CMD_NOP           0x00    //NOP (No Operation)

static void spi_flash_lock(struct spi_flash * device)
{
    RTOS_ERR  err;
    OSMutexPend(&device->lock,
                0,
                OS_OPT_PEND_BLOCKING,
                DEF_NULL,
                &err);
}

static void spi_flash_unlock(struct spi_flash * device)
{
    RTOS_ERR  err;
    OSMutexPost(&device->lock,
                   OS_OPT_POST_NONE,
                   &err);
}

uint8_t spi_flash_read_status(struct spi_flash * device)
{
    return bsp_spi_writeread(device->spi, FLASH_CMD_RDSR);
}

void spi_flash_wait_busy(struct spi_flash * device)
{// need time out
    while( bsp_spi_writeread(device->spi, FLASH_CMD_RDSR) & FLASH_MASK_WIP){
        ;
    }
}

/** \brief read [size] byte from [offset] to [buffer]
 *
 * \param offset uint32_t unit : byte
 * \param buffer uint8_t*
 * \param size uint32_t   unit : byte
 * \return uint32_t byte for read
 *
 */
uint32_t spi_flash_read(struct spi_flash * device, uint32_t offset, uint8_t * buffer, uint32_t size)
{
    uint8_t send_buffer[4];

    send_buffer[0]  = FLASH_CMD_WRDI;

    bsp_spi_write(device->spi, send_buffer, 1);

    send_buffer[0] = FLASH_CMD_READ;
    send_buffer[1] = (uint8_t)(offset>>16);
    send_buffer[2] = (uint8_t)(offset>>8);
    send_buffer[3] = (uint8_t)(offset);

    bsp_spi_write_then_read(device->spi,
                          send_buffer, 4,
                          buffer, size);

    return size;
}



#if 0
/** \brief write N page on [page]
 *
 * \param page_addr uint32_t unit : byte (4096 * N,1 page = 4096byte)
 * \param buffer const uint8_t*
 * \return uint32_t
 *
 */
uint32_t spi_flash_page_write(struct spi_flash * device, uint32_t page_addr, const uint8_t* buffer)
{
    uint32_t index;
    uint8_t send_buffer[4];

    RTOS_ASSERT_DBG((page_addr&0xFF) == 0); /* page addr must align to 256byte. */

    SPIDRV_writeread(device->spi, FLASH_CMD_WREN);

    send_buffer[0] = FLASH_CMD_SE;
    send_buffer[1] = (page_addr >> 16);
    send_buffer[2] = (page_addr >> 8);
    send_buffer[3] = (page_addr);
    rt_spi_send(spi_flash_device.rt_spi_device, send_buffer, 4);

    w25qxx_wait_busy(); // wait erase done.

    for(index=0; index < (PAGE_SIZE / 256); index++)
    {
        send_buffer[0] = CMD_WREN;
        rt_spi_send(spi_flash_device.rt_spi_device, send_buffer, 1);

        send_buffer[0] = CMD_PP;
        send_buffer[1] = (uint8_t)(page_addr >> 16);
        send_buffer[2] = (uint8_t)(page_addr >> 8);
        send_buffer[3] = (uint8_t)(page_addr);

        rt_spi_send_then_send(spi_flash_device.rt_spi_device,
                              send_buffer,
                              4,
                              buffer,
                              256);

        buffer += 256;
        page_addr += 256;
        w25qxx_wait_busy();
    }

    send_buffer[0] = CMD_WRDI;
    rt_spi_send(spi_flash_device.rt_spi_device, send_buffer, 1);

    return PAGE_SIZE;
}
#endif


static struct spi_flash_params * spi_flash_find_device(uint16_t id, uint8_t manu)
{
    static const struct spi_flash_params spi_flash_table[] = {
    	{
    		.id			 = 0x4017,
    		.manufacture = 0xEF,
    		.l2_page_size		= 8,
    		.pages_per_sector	= 16,
    		.sectors_per_block	= 16,
    		.nr_blocks		= 128,
    		.name			= "W25Q64",
    	},
    	{
    		.id			= 0x4018,
    		.manufacture = 0xEF,
    		.l2_page_size		= 8,
    		.pages_per_sector	= 16,
    		.sectors_per_block	= 16,
    		.nr_blocks		= 256,
    		.name			= "W25Q128",
    	},
    	{
    		.id			= 0x2018,
    		.manufacture = 0xC2,
    		.l2_page_size		= 8, //not verify
    		.pages_per_sector	= 16,//not verify
    		.sectors_per_block	= 16,//not verify
    		.nr_blocks		= 256,//not verify
    		.name			= "MX25L12835F",
    	},
    };

    int32_t i, list_count;

    list_count =  sizeof(spi_flash_table)/sizeof(struct spi_flash_params);

    for(i=0; i < list_count; i++){
        if(id == spi_flash_table[i].id && manu == spi_flash_table[i].manufacture){
            return (struct spi_flash_params *)&spi_flash_table[i];
        }
    }

    return NULL;
}


static SPIDRV_HandleData_t flash_spi = {
   .usart = BSP_EXTFLASH_SPI_PORT,
   .portLocationTx  = BSP_EXTFLASH_MOSI_LOC,
   .portLocationRx  = BSP_EXTFLASH_MISO_LOC,
   .portLocationClk = BSP_EXTFLASH_CLK_LOC,
   .portLocationCs  = BSP_EXTFLASH_CS_LOC,
   .bitRate= 8000000,
   .frameLength= 8,
   .dummyTxValue = 0xFFFFFFFF,
   .bitOrder =spidrvBitOrderMsbFirst,
   .clockMode = spidrvClockMode0,
   .csControl = 1, //spidrvCsControlAuto,
   .use_dma = 0,
};

struct spi_flash ext_flash;


void spi_flash_init(struct spi_flash *flash, const char * flash_device_name)
{
    RTOS_ERR err;
    volatile int time_cnt;


    /* port config : will be moved somewere */

#ifdef BSP_EXTFLASH_WP_PORT
    GPIO_PinModeSet(BSP_EXTFLASH_WP_PORT, BSP_EXTFLASH_WP_PIN, gpioModePushPull,1);
#endif

#ifdef BSP_EXTFLASH_RESET_PORT
    GPIO_PinModeSet(BSP_EXTFLASH_RESET_PORT, BSP_EXTFLASH_RESET_PIN, gpioModePushPull,1);
    //GPIO_PinOutClear(BSP_EXTFLASH_RESET_PORT, BSP_EXTFLASH_RESET_PIN);
    //time_cnt = 10000;
    //while(time_cnt-- > 0)
    //    ;// simple delay for test
    //GPIO_PinOutSet( BSP_EXTFLASH_RESET_PORT, BSP_EXTFLASH_RESET_PIN);
#endif

    Mem_Set(flash, 0, sizeof(struct spi_flash));

    /* initialize mutex */
    OSMutexCreate(&(flash->lock), "flash", &err);

    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
        return;
    }

    bsp_spi_init( &flash_spi);
    flash->spi = &flash_spi;

#if 0
    //test
    {
    //USART_InitSync_TypeDef init = USART_INITSYNC_DEFAULT;

    //init.msbf     = true;
    //init.baudrate = 8000000;
    //USART_InitSync( BSP_EXTFLASH_SPI_PORT, &init );

    /* IO config */
     GPIO_PinModeSet( BSP_EXTFLASH_MOSI_PORT, BSP_EXTFLASH_MOSI_PIN, gpioModePushPull, 1 );
     GPIO_PinModeSet( BSP_EXTFLASH_MISO_PORT, BSP_EXTFLASH_MISO_PIN, gpioModeInput,    0 );
     GPIO_PinModeSet( BSP_EXTFLASH_CLK_PORT,  BSP_EXTFLASH_CLK_PIN,  gpioModePushPull, 1 );
     GPIO_PinModeSet( BSP_EXTFLASH_CS_PORT,   BSP_EXTFLASH_CS_PIN,   gpioModePushPull, 1 );

     BSP_EXTFLASH_SPI_PORT->ROUTELOC0 = ( (BSP_EXTFLASH_MISO_LOC << _USART_ROUTELOC0_RXLOC_SHIFT)
                             | (BSP_EXTFLASH_MOSI_LOC << _USART_ROUTELOC0_TXLOC_SHIFT)
                             | (BSP_EXTFLASH_CLK_LOC << _USART_ROUTELOC0_CLKLOC_SHIFT) );
     BSP_EXTFLASH_SPI_PORT->ROUTEPEN  = (  USART_ROUTEPEN_RXPEN
                              | USART_ROUTEPEN_TXPEN
                              | USART_ROUTEPEN_CLKPEN );


    }
#endif
    /* Wait for flash warm-up */
    //time_cnt = FlashFullAccessTime;
    time_cnt = 10000;
    while( time_cnt-- > 0 )
        ;

    spi_flash_lock(flash);

    {
    uint8_t buffer[3];
    buffer[0] = FLASH_CMD_WRDI;
    bsp_spi_write(flash->spi, buffer, 1);
    /* read flash id */
    buffer[0] = FLASH_CMD_RDID;
    bsp_spi_write_then_read(flash->spi,  buffer,1,buffer, 3);
    flash->params_ptr = spi_flash_find_device(((uint16_t)buffer[1]<<8)|buffer[2] , buffer[0]);
    }

    spi_flash_unlock(flash);

}

static uint32_t spi_flash_open(struct spi_flash *flash, uint16_t oflag)
{
    uint8_t send_buffer[3];

    spi_flash_lock(flash);

    send_buffer[0] = FLASH_CMD_WREN;
    bsp_spi_write(flash->spi, send_buffer, 1);

    send_buffer[0] = FLASH_CMD_WRSR;
    send_buffer[1] = 0;
    send_buffer[2] = 0;
    bsp_spi_write(flash->spi, send_buffer, 3);

    spi_flash_wait_busy(flash);

    spi_flash_unlock(flash);

    return 0;
}

#endif //#if configUSE_SPI_FLASH
