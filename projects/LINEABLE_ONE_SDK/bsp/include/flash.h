/*
****************************************************************************************************
*                                               MODULE
****************************************************************************************************
*/

#ifndef FLASH_H_
#define FLASH_H_

/*
****************************************************************************************************
*                                                INCLUDE
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                            PUBLIC DEFINES
****************************************************************************************************
*/

#define FLASH_NO_ERROR                0       /* No error return code */
#define FLASH_WRITE_VERIFY_ERROR      -5      /* Flash write verify error return code */
#define FLASH_PARAMETER_ERROR         -4      /* Illegal parameter error return code */
#define FLASH_WRITE_TIMEOUT           -3      /* Write timeout error return code */
#define FLASH_PROTECTION_ERROR        -2      /* Flash memory protected error return code */
#define FLASH_WRITE_ERROR             -1      /* Flash write error return code */

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION PROTOTYPES
****************************************************************************************************
*/

void     FLASHOpen(void);
void     FLASHClose(void);
uint32_t FLASHGetPageSize(void);
uint32_t FLASHGetTotalSize(void);
int8_t   FLASHEraseBlock(void *BlockAddress);
int8_t   FLASHBlankCheck(void *BlockAddress);
int8_t   FLASHBlankBufferCheck(uint8_t *Buffer, uint16_t Len);
int8_t   FLASHWrite(void *Address, uint8_t *Buffer, uint16_t Count);
int8_t   FLASHEraseUserData(void);
int8_t   FLASHWriteUserData(uint16_t Offset, uint8_t *Buffer, uint16_t Count);
int8_t   FLASHWriteVerify(void *Address, uint8_t *Buffer, uint16_t Count);
int8_t   FLASHBlankCheckUserData(void);
int8_t   FLASHWriteVerifyUserData(uint16_t Offset, uint8_t *Buffer, uint16_t Count);
int8_t   FLASHSetProtectedAddress(uint8_t *StartingAddress);
int8_t   FLASHGetProtectedAddress(uint8_t *StartingAddress);

#endif


