/*
 */

#ifndef    __BSP_SPI_FLASH_H__
#define    __BSP_SPI_FLASH_H__

#include <bsphalconfig.h>
#include <stdbool.h>
#include <stdint.h>
#include <spidrv.h>
#include <kernel/include/os.h>

typedef struct spi_flash_params {
	uint16_t	id;
	uint8_t     manufacture;
	/* Log2 of page size in power-of-two mode */
	uint8_t		l2_page_size;
	uint16_t	pages_per_sector;
	uint16_t	sectors_per_block;
	uint16_t	nr_blocks;
	const char	*name;
}spi_flash_params_t;

struct spi_flash {
	const spi_flash_params_t *params_ptr;
	SPIDRV_HandleData_t *spi;
	OS_MUTEX 	lock;
#if 0	
	int		(*read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
	int		(*write)(struct spi_flash *flash, u32 offset,
				size_t len, const void *buf);
	int		(*erase)(struct spi_flash *flash, u32 offset,
				size_t len);
#endif				
};

typedef struct spi_flash * spi_flash_handle_t;

#if 0
struct spi_flash *spi_flash_probe(unsigned int bus, unsigned int cs,
		unsigned int max_hz, unsigned int spi_mode);
		
static inline int spi_flash_read(struct spi_flash *flash, u32 offset,
		size_t len, void *buf)
{
	return flash->read(flash, offset, len, buf);
}

static inline int spi_flash_write(struct spi_flash *flash, u32 offset,
		size_t len, const void *buf)
{
	return flash->write(flash, offset, len, buf);
}

static inline int spi_flash_erase(struct spi_flash *flash, u32 offset,
		size_t len)
{
	return flash->erase(flash, offset, len);
}
#endif
extern struct spi_flash ext_flash; // no malloc
extern void spi_flash_init(struct spi_flash *flash, const char * flash_device_name);

#endif    /* end of __BSP_SPI_FLASH_H__  */

