/***************************************************************************//**
 * @file
 * @brief Board support package API definitions.
 * @version 5.6.0
 *******************************************************************************
 * # License
 * <b>Copyright 2016 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef __BSP_H
#define __BSP_H
/*
*********************************************************************************************************
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*********************************************************************************************************
*/
#include  <stdbool.h>
#include  <bsphalconfig.h>
#include  <cpu/include/cpu.h>
#include  <common/include/toolchains.h>
#include  <common/include/rtos_path.h>
#include  "em_usart.h"

/***************************************************************************//**
 * @addtogroup BSP
 * @brief Board Support Package
 * @details
 *  The BSP provides an API for board controllers, I/O control for buttons,
 *  LEDs, etc and trace control for EFM32, EZR32 and EFR32 kits.
 *
 * @{
 ******************************************************************************/
#define OS_US_2_TICKS(us)        ((((us) * OSCfg_TickRate_Hz)  + 1000000u - 1u) / 1000000u)

#define OS_MS_2_TICKS(ms)        ((((ms) * OSCfg_TickRate_Hz)  + 1000u - 1u) / 1000u)

#define OS_TMR_MS_2_TICKS(ms)    ((((ms) * OSCfg_TmrTaskRate_Hz)  + 1000u - 1u) / 1000u)

//#define OS_TICKS_2_MS(ticks)     ((((ticks) * 1000ul) - 1000ul + 1ul) / OSCfg_TickRate_Hz)

#define OS_TICKS_2_MS(ticks)   (ticks) > 4000000 ? (((ticks) / OSCfg_TickRate_Hz) * 1000ul) : (((ticks) * 1000ul) / OSCfg_TickRate_Hz)


/**********************************************************************************************************
*********************************************************************************************************
*                                               DEFINES
*
* Note(s) : (1) A bug exists in some version of IAR where the compiler can make some optimizations using
*               a weak definition even though a strong definition exists somewhere else. Using the
*               volatile type qualifier in the weak definition prevents this optimization.
*********************************************************************************************************
*********************************************************************************************************
*/

#if (RTOS_TOOLCHAIN != RTOS_TOOLCHAIN_IAR)
#define  BSP_HW_INFO_EXT(type, name)  PP_WEAK(type, name, {0})
#else
                                                                /* See note (1).                                        */
#define  BSP_HW_INFO_EXT(type, name)  PP_WEAK(volatile type, name, {0})
#endif

#define BSP_STATUS_OK                 0     /**< BSP API return code, no errors. */
#define BSP_STATUS_ILLEGAL_PARAM      (-1)  /**< BSP API return code, illegal input parameter. */
#define BSP_STATUS_NOT_IMPLEMENTED    (-2)  /**< BSP API return code, function not implemented (dummy). */
#define BSP_STATUS_UNSUPPORTED_MODE   (-3)  /**< BSP API return code, unsupported BSP mode. */
#define BSP_STATUS_IOEXP_FAILURE      (-4)  /**< BSP API return code, io-expander communication failed */

/* Initialization flag bitmasks for BSP_Init(). */
#define BSP_INIT_DK_SPI     0x01  /**< Mode flag for @ref BSP_Init(), init DK in SPI mode (DK3x50 only). */
#define BSP_INIT_DK_EBI     0x02  /**< Mode flag for @ref BSP_Init(), init DK in EBI mode (DK3x50 only). */
#define BSP_INIT_BCC        0x04  /**< Mode flag for @ref BSP_Init(), init board controller communication. */
#define BSP_INIT_IOEXP      0x08  /**< Mode flag for @ref BSP_Init(), init io-expander on some radio boards */


#ifdef __cplusplus
extern "C" {
#endif

/** Timer time variable definition */
typedef uint32_t TimerTime_t;

/*********************************************************************************************************
 *********************************************************************************************************
 *                                           FUNCTION PROTOTYPES
 *********************************************************************************************************
 ********************************************************************************************************/

int             BSP_LedClear                (int ledNo);
int             BSP_LedGet                  (int ledNo);
int             BSP_LedSet                  (int ledNo);
uint32_t        BSP_LedsGet                 (void);
int             BSP_LedsInit                (void);
int             BSP_LedsSet                 (uint32_t leds);
int             BSP_LedToggle               (int ledNo);


/************BUTTON FUNCTION *********************************************************/
typedef void (*handle_button_callback)(uint8_t btn_state);
/** @brief Button state is pressed.
 */
#define BUTTON_PRESSED  1
/** @brief Button state is released.
 */
#define BUTTON_RELEASED 0
extern uint8_t BSP_ReadButtons(void);
extern uint8_t BSP_ReadButton(uint8_t pin);
extern void BSP_ButtonInit(handle_button_callback cb);


typedef void (*periph_init_cb)(void);

void  BSP_SystemInit(periph_init_cb peripherals_initialization);

void  BSP_TickInit  (void);

void  BSP_OS_Init    (void);

void  BSP_PeriphInit(void);

void  BSP_RTCC_TickInit (void);

typedef void (*clk_signal_cb)(uint32_t tickElapsed);
extern clk_signal_cb   clk_update_cb;

void  BSP_CPUInit(void);


/** Initializes the RTC timer */
void RtcInit( void );
/** Start the RTC timer */
void RtcSetTimeout( uint32_t timeout );
/** Adjust the value of the timeout to handle wakeup time from Alarm and GPIO irq */
TimerTime_t RtcGetAdjustedTimeoutValue( uint32_t timeout );
/** Get the RTC timer value */
TimerTime_t RtcGetTimerValue( void );
/** Get the RTC timer elapsed time since the last Alarm was set */
TimerTime_t RtcGetElapsedAlarmTime( void );
/** Compute the timeout time of a future event in time */
TimerTime_t RtcComputeFutureEventTime( TimerTime_t futureEventInTime );
/** Compute the elapsed time since a fix event in time */
TimerTime_t RtcComputeElapsedTime( TimerTime_t eventInTime );
/** This function blocks the MCU from going into Low Power mode */
void BlockLowPowerDuringTask ( bool status );
/** Sets the MCU into low power STOP mode */
void RtcEnterLowPowerStopMode( void );
/** Restore the MCU to its normal operation mode */
void RtcRecoverMcuStatus( void );
/** Computes the temperature compensation for a period of time on a specific temperature. */
TimerTime_t RtcTempCompensation( TimerTime_t period, float temperature );
/** Busy-wait for several milliseconds */
void HAL_Delay(uint32_t ms);

/** @} */ /* endgroup BSP */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */
