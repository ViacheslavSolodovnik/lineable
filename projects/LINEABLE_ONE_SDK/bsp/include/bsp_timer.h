/*
****************************************************************************************************
*                                               MODULE
****************************************************************************************************
*/

#ifndef __BSP_TIMER_H__
#define __BSP_TIMER_H__

/*
****************************************************************************************************
*                                                INCLUDE
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                            PUBLIC DEFINES
****************************************************************************************************
*/

#define DEF_TIME_SEC_PER_MIN 60u
#define DEF_TIME_MIN_PER_HR 60u

#define DEF_TIME_mS_PER_SEC 1000u
#define DEF_TIME_mS_PER_MIN (DEF_TIME_mS_PER_SEC * DEF_TIME_SEC_PER_MIN)
#define DEF_TIME_mS_PER_HR (DEF_TIME_mS_PER_MIN * DEF_TIME_MIN_PER_HR)

#define TIME_SEC(x) (x * DEF_TIME_mS_PER_SEC)
#define TIME_MIN(x) (x * DEF_TIME_mS_PER_MIN)
#define TIME_HR(x) (x * DEF_TIME_mS_PER_HR)

/*!
 * \brief Timer object description
 */
typedef struct TimerEvent_s
{
    uint32_t Timestamp;         //! Current timer value
    uint32_t ReloadValue;       //! Timer delay value
    bool IsRunning;             //! Is the timer currently running
    void ( *Callback )( void ); //! Timer IRQ callback function
    struct TimerEvent_s *Next;  //! Pointer to the next Timer object.
} TimerEvent_t;

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION PROTOTYPES
****************************************************************************************************
*/

#endif  /* __BSP_TIMER_H__ */
