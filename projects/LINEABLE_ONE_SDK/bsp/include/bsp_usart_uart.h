/****************************************************************************************
//
//
//  Copyright Lineable (C) 2018 by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author: 
//   File name : 
//   Created : 
//   Last Update :            
****************************************************************************************/
#ifndef BSP_USART_UART_H
#define BSP_USART_UART_H

#if 0// Brandon TEST
#include "em_device.h"
#include "em_usart.h"
#include "em_leuart.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "ecode.h"
/***************************************************************************//**
 * @addtogroup emdrv
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup UARTDRV
 * @{
 ******************************************************************************/

/// Size of the receive operation queue.
/// @details
///   The maximum number of receive operations that can be queued up for one
///   driver instance before @ref UARTDRV_Receive() returns
///   @ref ECODE_EMDRV_UARTDRV_QUEUE_FULL.
/// @note
///   This macro is not used by the UARTDRV itself, but is intended to be used
///   with the @ref DEFINE_BUF_QUEUE macro by the user of the driver to allocate
///   instances of the @ref UARTDRV_Buffer_FifoQueue_t struct.
#if !defined(EMDRV_UARTDRV_MAX_CONCURRENT_RX_BUFS)
#define EMDRV_UARTDRV_MAX_CONCURRENT_RX_BUFS    6
#endif

/// Size of the transmit operation queue.
/// @details
///   The maximum number of transmit operations that can be queued up for one
///   driver instance before @ref UARTDRV_Transmit() returns
///   @ref ECODE_EMDRV_UARTDRV_QUEUE_FULL.
/// @note
///   This macro is not used by the UARTDRV itself, but is intended to be used
///   with the @ref DEFINE_BUF_QUEUE macro by the user of the driver to allocate
///   instances of the @ref UARTDRV_Buffer_FifoQueue_t struct.
#if !defined(EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS)
#define EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS    6
#endif

/// Maximum number of driver instances.
/// @note
///   This maximum applies only when @ref EMDRV_UARTDRV_FLOW_CONTROL_ENABLE = 1.
#if !defined(EMDRV_UARTDRV_MAX_DRIVER_INSTANCES)
#define EMDRV_UARTDRV_MAX_DRIVER_INSTANCES      4
#endif

/// UART software flow control code: request peer to start TX
#if !defined(UARTDRV_FC_SW_XON)
#define UARTDRV_FC_SW_XON                       0x11
#endif

/// UART software flow control code: request peer to stop TX
#if !defined(UARTDRV_FC_SW_XOFF)
#define UARTDRV_FC_SW_XOFF                      0x13
#endif

#include "dmadrv.h"

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************//**
 * @addtogroup emdrv
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup UARTDRV
 * @{
 ******************************************************************************/

#define ECODE_EMDRV_UARTDRV_OK                (ECODE_OK)                              ///< A successful return value.
#define ECODE_EMDRV_UARTDRV_WAITING           (ECODE_EMDRV_UARTDRV_BASE | 0x00000001) ///< An operation is waiting in queue.
#define ECODE_EMDRV_UARTDRV_ILLEGAL_HANDLE    (ECODE_EMDRV_UARTDRV_BASE | 0x00000002) ///< An illegal UART handle.
#define ECODE_EMDRV_UARTDRV_PARAM_ERROR       (ECODE_EMDRV_UARTDRV_BASE | 0x00000003) ///< An illegal input parameter.
#define ECODE_EMDRV_UARTDRV_BUSY              (ECODE_EMDRV_UARTDRV_BASE | 0x00000004) ///< The UART port is busy.
#define ECODE_EMDRV_UARTDRV_ILLEGAL_OPERATION (ECODE_EMDRV_UARTDRV_BASE | 0x00000005) ///< An illegal operation on the UART port.
#define ECODE_EMDRV_UARTDRV_IDLE              (ECODE_EMDRV_UARTDRV_BASE | 0x00000008) ///< No UART transfer is in progress.
#define ECODE_EMDRV_UARTDRV_ABORTED           (ECODE_EMDRV_UARTDRV_BASE | 0x00000009) ///< A UART transfer has been aborted.
#define ECODE_EMDRV_UARTDRV_QUEUE_FULL        (ECODE_EMDRV_UARTDRV_BASE | 0x0000000A) ///< A UART operation queue is full.
#define ECODE_EMDRV_UARTDRV_QUEUE_EMPTY       (ECODE_EMDRV_UARTDRV_BASE | 0x0000000B) ///< A UART operation queue is empty.
#define ECODE_EMDRV_UARTDRV_PARITY_ERROR      (ECODE_EMDRV_UARTDRV_BASE | 0x0000000C) ///< A UART parity error frame. Data is ignored.
#define ECODE_EMDRV_UARTDRV_FRAME_ERROR       (ECODE_EMDRV_UARTDRV_BASE | 0x0000000D) ///< A UART frame error. Data is ignored.
#define ECODE_EMDRV_UARTDRV_DMA_ALLOC_ERROR   (ECODE_EMDRV_UARTDRV_BASE | 0x0000000E) ///< Unable to allocate DMA channels.
#define ECODE_EMDRV_UARTDRV_CLOCK_ERROR       (ECODE_EMDRV_UARTDRV_BASE | 0x0000000F) ///< Unable to set a desired baudrate.

// UARTDRV status codes
#define UARTDRV_STATUS_RXEN     (1 << 0)  ///< The receiver is enabled.
#define UARTDRV_STATUS_TXEN     (1 << 1)  ///< The transmitter is enabled.
#define UARTDRV_STATUS_RXBLOCK  (1 << 3)  ///< The receiver is blocked; incoming frames will be discarded.
#define UARTDRV_STATUS_TXTRI    (1 << 4)  ///< The transmitter is tristated.
#define UARTDRV_STATUS_TXC      (1 << 5)  ///< A transmit operation is complete. No more data is available in the transmit buffer and shift register.
#define UARTDRV_STATUS_TXBL     (1 << 6)  ///< The transmit buffer is empty.
#define UARTDRV_STATUS_RXDATAV  (1 << 7)  ///< Data is available in the receive buffer.
#define UARTDRV_STATUS_RXFULL   (1 << 8)  ///< The receive buffer is full.
#define UARTDRV_STATUS_TXIDLE   (1 << 13) ///< The transmitter is idle.

typedef uint32_t UARTDRV_Count_t;     ///< A UART transfer count
typedef uint32_t UARTDRV_Status_t;    ///< A UART status return type. Bitfield of UARTDRV_STATUS_* values.

/// Flow Control method
typedef enum UARTDRV_FlowControlType{
  uartdrvFlowControlNone   = 0,   ///< None
  uartdrvFlowControlSw     = 1,   ///< Software XON/XOFF
  uartdrvFlowControlHw     = 2,   ///< nRTS/nCTS hardware handshake
  uartdrvFlowControlHwUart = 3    ///< UART peripheral controls nRTS/nCTS
} UARTDRV_FlowControlType_t;

/// Flow Control state
typedef enum UARTDRV_FlowControlState{
  uartdrvFlowControlOn = 0,         ///< XON or nRTS/nCTS low
  uartdrvFlowControlOff = 1,        ///< XOFF or nRTS/nCTS high
  uartdrvFlowControlAuto = 2        ///< This driver controls the state.
} UARTDRV_FlowControlState_t;

/// Transfer abort type
typedef enum UARTDRV_AbortType{
  uartdrvAbortTransmit = 1,          ///< Abort current and queued transmit operations
  uartdrvAbortReceive = 2,           ///< Abort current and queued receive operations
  uartdrvAbortAll = 3                ///< Abort all current and queued operations
} UARTDRV_AbortType_t;

/// @cond DO_NOT_INCLUDE_WITH_DOXYGEN
/// Type of a UART peripheral
typedef enum UARTDRV_UartType{
  uartdrvUartTypeUart = 0,         ///< USART/UART peripheral
  uartdrvUartTypeLeuart = 1         ///< LEUART peripheral
} UARTDRV_UartType_t;
/// @endcond

struct UARTDRV_HandleData;

/***************************************************************************//**
 * @brief
 *  UARTDRV transfer completion callback function.
 *
 * @details
 *  Called when a transfer is complete. An
 *  application should check the transferStatus and itemsTransferred values.
 *
 * @param[in] handle
 *   The UARTDRV device handle used to start the transfer.
 *
 * @param[in] transferStatus
 *   Completion status of the transfer operation.
 *
 * @param[in] data
 *   A pointer to the transfer data buffer.
 *
 * @param[in] transferCount
 *   A number of bytes transferred.
 ******************************************************************************/
typedef void (*UARTDRV_Callback_t)(struct UARTDRV_HandleData *handle,
                                   Ecode_t transferStatus,
                                   uint8_t *data,
                                   UARTDRV_Count_t transferCount);

typedef void (*bsp_uart_tx_callback)(void *user_data, uint16_t written);
typedef void (*bsp_uart_rx_callback)(void *user_data, uint16_t read);


/// UART transfer buffer
typedef struct {
  uint8_t *data;                           ///< Transfer data buffer
  UARTDRV_Count_t transferCount;           ///< Transfer item count
  volatile UARTDRV_Count_t itemsRemaining; ///< Transfer items remaining
  UARTDRV_Callback_t callback;             ///< Completion callback
  Ecode_t transferStatus;                  ///< Completion status of the transfer operation
} UARTDRV_Buffer_t;

/// Transfer operation FIFO queue typedef
typedef struct {
  volatile uint16_t head;                  ///< An index of the next byte to send.
  volatile uint16_t tail;                  ///< An index of the location to enqueue the next message.
  volatile uint16_t used;                  ///< A number of bytes queued.
  const uint16_t size;                     ///< FIFO size.
  UARTDRV_Buffer_t fifo[];                 ///< FIFO of queued data.
} UARTDRV_Buffer_FifoQueue_t;

/// Macros to define FIFO and buffer queues. typedef can't be used becuase the size
/// of the FIFO array in the queues can change.
#define DEFINE_BUF_QUEUE(qSize, qName) \
  typedef struct {                     \
    uint16_t head;                     \
    uint16_t tail;                     \
    volatile uint16_t used;            \
    const uint16_t size;               \
    UARTDRV_Buffer_t fifo[qSize];      \
  } _##qName;                          \
  static volatile _##qName qName =     \
  {                                    \
    .head = 0,                         \
    .tail = 0,                         \
    .used = 0,                         \
    .size = qSize,                     \
  }

/// A UART driver instance initialization structure.
/// Contains a number of UARTDRV configuration options.
/// It is required for driver instance initialization.
/// This structure is passed to @ref UARTDRV_Init() when initializing a UARTDRV
/// instance.
typedef struct {
  USART_TypeDef              *port;             ///< The peripheral used for UART
  uint32_t                   baudRate;          ///< UART baud rate
#if defined(_USART_ROUTELOC0_MASK)
  uint8_t                    portLocationTx;    ///< A location number for UART Tx pin.
  uint8_t                    portLocationRx;    ///< A location number for UART Rx pin.
#else
  uint8_t                    portLocation;      ///< A location number for UART pins.
#endif
  USART_Stopbits_TypeDef     stopBits;          ///< A number of stop bits.
  USART_Parity_TypeDef       parity;            ///< Parity configuration.
  USART_OVS_TypeDef          oversampling;      ///< Oversampling mode.
#if defined(USART_CTRL_MVDIS)
  bool                       mvdis;             ///< Majority Vote Disable for 16x, 8x and 6x oversampling modes.
#endif
  GPIO_Port_TypeDef          ctsPort;           ///< A CTS pin port number.
  uint8_t                    ctsPin;            ///< A CTS pin number.
  GPIO_Port_TypeDef          rtsPort;           ///< An RTS pin port number.
  uint8_t                    rtsPin;            ///< An RTS pin number.
  UARTDRV_Buffer_FifoQueue_t *rxQueue;          ///< A receive operation queue.
  UARTDRV_Buffer_FifoQueue_t *txQueue;          ///< T transmit operation queue.
#if defined(_USART_ROUTELOC1_MASK)
  uint8_t                    portLocationCts;   ///< A location number for the UART CTS pin.
  uint8_t                    portLocationRts;   ///< A location number for the UART RTS pin.
#endif
} UARTDRV_InitUart_t;

/// A UART driver instance handle data structure.
/// Allocated by the application using UARTDRV.
/// Several concurrent driver instances may exist in an application. The application must
/// not modify the contents of this handle and should not depend on its values.
typedef struct UARTDRV_HandleData{
  USART_TypeDef             * usart;
  unsigned int               txDmaCh;           // A DMA ch assigned to Tx
  unsigned int               rxDmaCh;           // A DMA ch assigned to Rx
  DMADRV_PeripheralSignal_t  txDmaSignal;       // A DMA Tx trigger source signal
  DMADRV_PeripheralSignal_t  rxDmaSignal;       // A DMA Rx trigger source signal
  GPIO_Port_TypeDef          txPort;            // A Tx pin port number
  GPIO_Port_TypeDef          rxPort;            // An Rx pin port number
  GPIO_Port_TypeDef          ctsPort;           // A CTS pin port number
  GPIO_Port_TypeDef          rtsPort;           // An RTS pin port number
  uint8_t                    txPin;             // A Tx pin number
  uint8_t                    rxPin;             // An Tx pin number
  uint8_t                    ctsPin;            // A CTS pin number
  uint8_t                    rtsPin;            // An RTS pin number
  CMU_Clock_TypeDef          uartClock;         // A clock source select
  UARTDRV_Buffer_FifoQueue_t *rxQueue;          // A receive operation queue
  UARTDRV_Buffer_FifoQueue_t *txQueue;          // A transmit operation queue
  volatile bool              rxDmaActive;       // A receive DMA is currently active
  volatile bool              txDmaActive;       // A transmit DMA is currently active
  volatile uint8_t           txDmaPaused;       // A transmit DMA pause counter
  bool                       hasTransmitted;    // Indicates whether the handle has transmitted data
  UARTDRV_UartType_t         type;              // A type of UART
  /// @endcond
} UARTDRV_HandleData_t;

/// Handle pointer
typedef UARTDRV_HandleData_t * UARTDRV_Handle_t;

Ecode_t UARTDRV_InitUart(UARTDRV_Handle_t handle,
                         const UARTDRV_InitUart_t * initData);

Ecode_t UARTDRV_DeInit(UARTDRV_Handle_t handle);


UARTDRV_Status_t UARTDRV_GetReceiveStatus(UARTDRV_Handle_t handle,
                                          uint8_t **buffer,
                                          UARTDRV_Count_t *bytesReceived,
                                          UARTDRV_Count_t *bytesRemaining);

UARTDRV_Status_t UARTDRV_GetTransmitStatus(UARTDRV_Handle_t handle,
                                           uint8_t **buffer,
                                           UARTDRV_Count_t *bytesSent,
                                           UARTDRV_Count_t *bytesRemaining);

uint8_t UARTDRV_GetReceiveDepth(UARTDRV_Handle_t handle);

uint8_t UARTDRV_GetTransmitDepth(UARTDRV_Handle_t handle);

Ecode_t UARTDRV_Transmit(UARTDRV_Handle_t handle,
                         uint8_t *data,
                         UARTDRV_Count_t count,
                         UARTDRV_Callback_t callback);

Ecode_t UARTDRV_Receive(UARTDRV_Handle_t handle,
                        uint8_t *data,
                        UARTDRV_Count_t count,
                        UARTDRV_Callback_t callback);

Ecode_t UARTDRV_TransmitB(UARTDRV_Handle_t handle,
                          uint8_t *data,
                          UARTDRV_Count_t count);

Ecode_t UARTDRV_ReceiveB(UARTDRV_Handle_t handle,
                         uint8_t *data,
                         UARTDRV_Count_t count);

Ecode_t UARTDRV_ForceTransmit(UARTDRV_Handle_t handle,
                              uint8_t *data,
                              UARTDRV_Count_t count);

UARTDRV_Count_t UARTDRV_ForceReceive(UARTDRV_Handle_t handle,
                                     uint8_t *data,
                                     UARTDRV_Count_t maxLength);

Ecode_t UARTDRV_Abort(UARTDRV_Handle_t handle, UARTDRV_AbortType_t type);

Ecode_t UARTDRV_PauseTransmit(UARTDRV_Handle_t handle);

Ecode_t UARTDRV_ResumeTransmit(UARTDRV_Handle_t handle);

#endif//#if 0// Brandon TEST
// --------------------------------
// Deprecated items



#ifdef __cplusplus
}
#endif
#endif // BSP_USART_UART_H
