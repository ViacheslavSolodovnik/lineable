/*******************************************************************************
 * @file lora_test.c
 * @brief Driver for the MAXIM PMIC max14676d
 * @version 0.0
 ******************************************************************************/
#include "lora_test.h"

#include <string.h>
#include <stdlib.h>

#include <drivers/radio/sx1276/sx1276Regs-LoRa.h>
#include <drivers/radio/sx1276/sx1276Regs-Fsk.h>
#include "sx1276-board.h"
#include <drivers/radio/radio.h>


/*******************************************************************************
*                                 DEBUG OPTION
*******************************************************************************/
#undef  __MODULE__
#define __MODULE__  "LoRa Test"
#undef  DBG_MODULE
#define DBG_MODULE 1

#define LORA_DEBUG DBG_MODULE

#if (configUSE_LORA_TEST > 0)
/*******************************************************************************
*                                   TYPEDEF
*******************************************************************************/
/** Device states */
static enum eDeviceTestState {
    TEST_STATE_TX,
    TEST_STATE_RX,
    TEST_STATE_CW,
    TEST_STATE_IDLE,
} DeviceTestState;


/*******************************************************************************
*                                   DEFINE
*******************************************************************************/
#define DIO0_IRQ                0x21

#define TEST_TEXT_SIZE          17

#define TEST_TEXT               "LoRa Test"


#define CARRIER_SENSING_TEST      (1)
#if CARRIER_SENSING_TEST
/*!
 * RSSI threshold for a free channel [dBm]
 */
#define AS923_RSSI_FREE_TH                          -85

/*!
 * Specifies the time the node performs a carrier sense
 */
#define AS923_CARRIER_SENSE_TIME                    6
#endif


/*******************************************************************************
*                                   MACRO
*******************************************************************************/


/*******************************************************************************
*                                  VALEABLE
*******************************************************************************/
#if defined( REGION_KR920 )
uint32_t ChannelIDtoFreq[9] = {
    921900000U, 922100000U, 922300000U, 922500000U,
    922700000U, 922900000U, 923100000U, 923300000U,
};
#elif defined( REGION_IN865 )
uint32_t ChannelIDtoFreq[9] = {
    0, 865400000U, 865600000U, 865800000U, 866000000U,
    866200000U, 866400000U, 866600000U, 866800000U,
};
#elif defined( REGION_AS923 )
uint32_t ChannelIDtoFreq[9] = {
    921800000U, 922000000U, 922200000U, 922400000U,
    922600000U, 922800000U, 923000000U, 923200000U, 923400000U,
};
#endif

uint8_t TestTxBuffer[48];
uint8_t TestRxBuffer[48];

uint32_t Frequency;
uint32_t DateRate;
uint32_t TxDelay;

volatile uint16_t LoRaIrq;

bool tx_cw_flag = false;

/*******************************************************************************
*                             FUNCTION PROTOTYPE
*******************************************************************************/


/*******************************************************************************
*                             PRIVATE FUNCTIONS
*******************************************************************************/


/*******************************************************************************
*                             PUBLIC FUNCTIONS
*******************************************************************************/
void LoRaitoa(uint16_t value, uint8_t *str, uint8_t radix, uint8_t num_digit)
{
    uint16_t num;
    uint16_t deg = 1;
    uint16_t cnt = 0;
    uint16_t i = 0, cnt1;


    num = value;

    while (1) {
        if ((num / deg) > 0) {
            cnt++;
        } else {
            break;
        }
        deg *= radix;
    }
    deg /= radix;

    if (num_digit != 0) {
        for (i = 0; i < num_digit - cnt; i++) {
            str[i] = 0x30;
        }
    }
    cnt1 = i;
    for (i = cnt1; i < num_digit; i++) {
        str[i] = (unsigned char)(num / deg + 0x30);
        num -= (num / deg * deg);
        deg /= radix;

    }
    str[i] = '\0';
}

#if 0
static void LoRaTxContinue(void)
{
    RTOS_ERR err;

    if (LoRaIrq == DIO0_IRQ) {
        LoRaIrq = 0;

        Radio.Write(REG_LR_IRQFLAGS, RFLR_IRQFLAGS_TXDONE);

        Radio.Standby();

        Radio.Send(TestTxBuffer, strlen((const char *)TestTxBuffer));

        APP_TRACE_DEBUG("Tx Continue\n");

        if (TxDelay) {
            OSTimeDly(OS_MS_2_TICKS(TxDelay), OS_OPT_TIME_DLY, &err);
        }
    }
}


static void LoRaRxContinue(void)
{
    int8_t SnrValue;
    int16_t Rssi;

    if (LoRaIrq == DIO0_IRQ) {
        LoRaIrq = 0;

        Radio.Write(REG_LR_IRQFLAGS, RFLR_IRQFLAGS_RXDONE);

        Radio.Write(REG_LR_IRQFLAGS, RFLR_IRQFLAGS_PAYLOADCRCERROR);

        SnrValue = Radio.Read(REG_LR_PKTSNRVALUE);
        if (SnrValue & 0x80) {
            SnrValue = ((SnrValue + 1) & 0xFF) >> 2;
            SnrValue = -SnrValue;
        } else {
            SnrValue = (SnrValue & 0xFF) >> 2;
        }

        Rssi = Radio.Read(REG_LR_PKTRSSIVALUE);

        if (SnrValue < 0) {
            if (Frequency > RF_MID_BAND_THRESH) {
                Rssi = -157 + Rssi + (Rssi  >> 4) + SnrValue;
            } else {
                Rssi = -164 + Rssi + (Rssi  >> 4) + SnrValue;
            }
        } else {
            if (Frequency > RF_MID_BAND_THRESH) {
                Rssi = -157 + Rssi + (Rssi  >> 4);
            } else {
                Rssi = -164 + Rssi + (Rssi  >> 4);
            }
        }

        memset(TestRxBuffer, 0, 0x30);

        Radio.ReadBuffer(0, TestRxBuffer, TEST_TEXT_SIZE);

        if (!strncmp((const char *)TestRxBuffer, TEST_TEXT, strlen(TEST_TEXT))) {
            APP_TRACE_DEBUG("RxText:%s, RSSI:%ddBm\n", TestRxBuffer, Rssi);
        } else {
            APP_TRACE_DEBUG("RxText:%s(Invalid)\n", TestRxBuffer);
        }
    }
}
#endif

#if 0
static int LoRaIrqHandler(uint16_t pinIndex)
{
    if (DeviceState == DEVICE_STATE_TX ||
        DeviceState == DEVICE_STATE_RX ||
        DeviceState == DEVICE_STATE_CW) {

        LoRaIrq = pinIndex;

        return 1;
    }

    return 0;
}
#endif

#if 0
static void LoRaSetRx(void)
{
    Radio.SetChannel(Frequency);
    Radio.SetMaxPayloadLength(MODEM_LORA, 30);
    Radio.SetRxConfig(MODEM_LORA, 0, DateRate, 2, 0, 8, 25,
                      false, 0, false, 0, 0, 0, true);
    Radio.Rx(3000000);
}
#endif

void LoRaTestInit(void)
{
    //GpioRegisterTestIrqHandler( LoRaIrqHandler );

    Radio.Init(NULL);
    Radio.SetPublicNetwork(true);
    Radio.Sleep();

    DeviceTestState = TEST_STATE_IDLE;
}


void LoRaTestDeInit(void)
{
    //GpioUnregisterTestIrqHandler( );
}

int LoRaTx(int TxID, int ChId, int DR, int TxPower, int modem)
{
    DateRate = (uint32_t)DR;

#if 1
#if defined( REGION_KR920 )
    Frequency = ChannelIDtoFreq[ChId-25];
#elif defined( REGION_AS923 )
    Frequency = ChannelIDtoFreq[ChId-30];
#endif
#else
    Frequency = ChannelIDtoFreq[ChId];
#endif
    Radio.Standby();

    Radio.SetChannel(Frequency);

    if(modem == MODEM_LORA)
        Radio.SetTxConfig(MODEM_LORA, TxPower, 0, 0, DateRate, 1, 8, false, true, 0, 0, false, 3e6);
    else if (modem == MODEM_FSK)
        Radio.SetTxConfig(MODEM_FSK, TxPower, 0, 0, 4800, 0, 5, false, false, 0, 0, 0, 3e6);  // For KC

    memcpy(TestTxBuffer, TEST_TEXT, strlen(TEST_TEXT));

    LoRaitoa(TxID, &TestTxBuffer[strlen(TEST_TEXT)], 10, 3);

#if CARRIER_SENSING_TEST
    if (modem == MODEM_LORA)
    {
        if( Radio.IsChannelFree( MODEM_LORA, Frequency, AS923_RSSI_FREE_TH, AS923_CARRIER_SENSE_TIME ) == true ) {
            Radio.Send(TestTxBuffer, TEST_TEXT_SIZE);
            TestTxBuffer[TEST_TEXT_SIZE] = 0;

            APP_TRACE_DEBUG("Freq:%ld\n", Frequency);
        } else {
            APP_TRACE_DEBUG("Channel is not free!\n");
        }
    }
#else
    Radio.Send(TestTxBuffer, TEST_TEXT_SIZE);
    TestTxBuffer[TEST_TEXT_SIZE] = 0;
    APP_TRACE_DEBUG("Freq:%ld\n", Frequency);
#endif

    return 0;
}


int LoRaRx(int ChId, int DR)
{
#if 1
#if defined( REGION_KR920 )
    Frequency = ChannelIDtoFreq[ChId-25];
#elif defined( REGION_AS923 )
    Frequency = ChannelIDtoFreq[ChId-30];
#endif
#else
    Frequency = ChannelIDtoFreq[ChId];
#endif
    DateRate = (uint32_t)DR;

    Radio.SetChannel(Frequency);
    Radio.SetMaxPayloadLength(MODEM_LORA, 30);
    Radio.SetRxConfig(MODEM_LORA, 0, DateRate, 2, 0, 8, 25, false, 0, false, 0, 0, 0, true);
    Radio.Rx(3000000);

    APP_TRACE_DEBUG("Rx Information\n");
    APP_TRACE_DEBUG("Freq:%ld, DateRate:%ld\n", Frequency, DateRate);

    return 0;
}


int LoraCW(void)
{
    LoRaStop();

    DeviceTestState = TEST_STATE_CW;

    Radio.Write(REG_OPMODE,
                (Radio.Read(REG_OPMODE) &
                 RF_OPMODE_MASK) |
                RF_OPMODE_TRANSMITTER);

    Radio.Write(REG_LR_OPMODE, 0x83);
    Radio.Write(REG_LR_MODEMCONFIG2, 0x5F);

    tx_cw_flag = true;

    return 0;
}


int LoRaStop(void)
{
//    APP_TRACE_DEBUG("Test State : %d\r\n", DeviceTestState);
//    APP_TRACE_DEBUG("LoraIrq : %d\r\n", DeviceTestState);

#if 0
    if (DeviceTestState == TEST_STATE_TX) {
        do {
            if (LoRaIrq == DIO0_IRQ) {
                LoRaIrq = 0;
                Radio.Write(REG_LR_IRQFLAGS, RFLR_IRQFLAGS_TXDONE);
                Radio.Standby();
                DeviceTestState = TEST_STATE_IDLE;
            }
        } while (DeviceTestState == TEST_STATE_TX);
    } else if (DeviceTestState == TEST_STATE_RX) {
        Radio.Write(REG_LR_IRQFLAGS, RFLR_IRQFLAGS_RXDONE);
        Radio.Standby();
        DeviceTestState = TEST_STATE_IDLE;
    } else if (DeviceTestState == TEST_STATE_CW) {
        Radio.Write(REG_LR_DETECTOPTIMIZE,
                    (Radio.Read(REG_LR_DETECTOPTIMIZE) &
                     RFLR_DETECTIONOPTIMIZE_MASK) |
                    RFLR_DETECTIONOPTIMIZE_SF7_TO_SF12);
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_SF_MASK) |
                    RFLR_MODEMCONFIG2_SF_7);
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK) |
                    RFLR_MODEMCONFIG2_RXPAYLOADCRC_ON);
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK) |
                    (0x3F & ~RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK));
        Radio.Standby();
        DeviceTestState = TEST_STATE_IDLE;
    }
#else
    if (tx_cw_flag  == true) {
        Radio.Write(REG_LR_DETECTOPTIMIZE,
                    (Radio.Read(REG_LR_DETECTOPTIMIZE) &
                     RFLR_DETECTIONOPTIMIZE_MASK) |
                    RFLR_DETECTIONOPTIMIZE_SF7_TO_SF12);
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_SF_MASK) |
                    RFLR_MODEMCONFIG2_SF_7);
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK) |
                    RFLR_MODEMCONFIG2_RXPAYLOADCRC_ON);
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK) |
                    (0x3F & ~RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK));
#if 0
        Radio.Write(REG_LR_MODEMCONFIG2,
                    (Radio.Read(REG_LR_MODEMCONFIG2) &
                     RFLR_MODEMCONFIG2_TXCONTINUOUSMODE_MASK) |
                    RFLR_MODEMCONFIG2_TXCONTINUOUSMODE_OFF);
#endif
        Radio.Standby();
    }
#endif
    return 0;
}

#endif
