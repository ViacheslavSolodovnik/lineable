/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2015 Semtech

Description: End device commissioning parameters

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#ifndef __LORA_COMMISSIONING_H__
#define __LORA_COMMISSIONING_H__

/*!
 * When set to 1 the application uses the Over-the-Air activation procedure
 * When set to 0 the application uses the Personalization activation procedure
 */
#define OVER_THE_AIR_ACTIVATION                     1

/*!
 * Indicates if the end-device is to be connected to a private or public network
 */
#define LORAWAN_PUBLIC_NETWORK                      true

/*!
 * IEEE Organizationally Unique Identifier ( OUI ) (big endian)
 * \remark This is unique to a company or organization
 */
#if defined(REGION_IN865)
#define IEEE_OUI                                    0x70, 0x2C, 0x1F
#elif defined(REGION_US915)
#define IEEE_OUI                                    0x6C, 0xDF, 0xFB
#elif defined(REGION_KR920)
#define IEEE_OUI                                    0xFF, 0xFF, 0xFF
#elif defined(REGION_AS923)
#define IEEE_OUI                                    0x6C, 0xDF, 0xFB
#elif defined(REGION_EU868)
#define IEEE_OUI                                    0x6C, 0xDF, 0xFB
#endif
/*!
 * Mote device IEEE EUI (big endian)
 *
 * \remark In this application the value is automatically generated by calling
 *         BoardGetUniqueId function
 */

#if defined(REGION_IN865)
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0x00, 0x00, 0x00, 0x00, 0x00 }
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x26 }
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x27 }
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x28 }
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x29 }
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x2A }  // Woman 1
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x2B }  // Woman 2
#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x2C }  // Woman 3
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x2D }  // Woman 4
#elif defined(REGION_US915)
//#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x3C, 0xCA, 0x43 }  // Comcast
#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x40, 0x00, 0x07 }
#elif defined(REGION_KR920)
#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }
#elif defined(REGION_AS923)
#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x40, 0x00, 0x02 }
#elif defined(REGION_EU868)
#define LORAWAN_DEVICE_EUI                          { IEEE_OUI, 0xFF, 0xFE, 0x40, 0x00, 0x07 }
#endif

/*!
 * Application IEEE EUI (big endian)
 */
#if defined(REGION_IN865)
#define LORAWAN_APPLICATION_EUI                     { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#elif defined(REGION_US915)
#define LORAWAN_APPLICATION_EUI                     { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#elif defined(REGION_KR920)
#define LORAWAN_APPLICATION_EUI                     { 0x02, 0x20, 0x73, 0x10, 0x00, 0x00, 0x01, 0x55 }
#elif defined(REGION_AS923)
#define LORAWAN_APPLICATION_EUI                     { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#elif defined(REGION_EU868)
#define LORAWAN_APPLICATION_EUI                     { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#endif

/*!
 * AES encryption/decryption cipher application key
 */
#if defined(REGION_IN865)
//#define LORAWAN_APPLICATION_KEY                     { 0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C }
//#define LORAWAN_APPLICATION_KEY                     { 0x25, 0x03, 0x38, 0x4A, 0xA7, 0x53, 0x24, 0xA0, 0xA6, 0x65, 0x11, 0x39, 0x66, 0x4A, 0x82, 0x80 } //26
//#define LORAWAN_APPLICATION_KEY                     { 0x1A, 0x66, 0x13, 0x40, 0x3A, 0x63, 0x98, 0x03, 0x05, 0x10, 0x80, 0x10, 0x68, 0x44, 0x91, 0x21 } //27
//#define LORAWAN_APPLICATION_KEY                     { 0x95, 0x25, 0x40, 0xA3, 0x49, 0x39, 0x05, 0x38, 0x68, 0x18, 0x4A, 0x16, 0x77, 0x49, 0x28, 0x43 } //28
//#define LORAWAN_APPLICATION_KEY                     { 0x6A, 0x64, 0x23, 0x3A, 0x95, 0x42, 0x97, 0x67, 0xA0, 0x39, 0x95, 0x94, 0x63, 0x53, 0x85, 0x78 } //29
//#define LORAWAN_APPLICATION_KEY                     { 0x39, 0x10, 0x62, 0x08, 0x28, 0x76, 0x89, 0x86, 0x64, 0x2A, 0x33, 0x85, 0xA2, 0x62, 0x63, 0x12 }  // Woman 1
//#define LORAWAN_APPLICATION_KEY                     { 0x78, 0x36, 0x24, 0x15, 0x12, 0x16, 0x77, 0xA0, 0x00, 0x32, 0xA6, 0x60, 0x77, 0x85, 0x62, 0x51 }  // Woman 2
#define LORAWAN_APPLICATION_KEY                     { 0x97, 0x17, 0xA6, 0x72, 0x94, 0x12, 0x28, 0x4A, 0x71, 0x82, 0x99, 0x31, 0x12, 0x62, 0x11, 0x03 }  // Woman 3
//#define LORAWAN_APPLICATION_KEY                     { 0x54, 0x74, 0xA8, 0x02, 0x03, 0x76, 0x5A, 0xA8, 0x94, 0x95, 0x68, 0x69, 0xA4, 0x65, 0x02, 0x65 }  // Woman 4
#elif defined(REGION_US915)
//#define LORAWAN_APPLICATION_KEY                     { 0xCE, 0xB5, 0x96, 0x4C, 0xFC, 0xC5, 0x4E, 0x8E, 0x99, 0x33, 0xA8, 0x77, 0xCD, 0x29, 0x10, 0xB2 }  // Comcast
#define LORAWAN_APPLICATION_KEY                     { 0xAA, 0x11, 0xBB, 0x22, 0xCC, 0x33, 0xDD, 0x44, 0xEE, 0x55, 0xFF, 0x66, 0xAA, 0x77, 0xBB, 0x88 }
#elif defined(REGION_KR920)
#define LORAWAN_APPLICATION_KEY                     { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07 }
#elif defined(REGION_AS923)
#define LORAWAN_APPLICATION_KEY                     { 0xAA, 0x11, 0xBB, 0x22, 0xCC, 0x33, 0xDD, 0x44, 0xEE, 0x55, 0xFF, 0x66, 0xAA, 0x77, 0xBB, 0x88 }
#elif defined(REGION_EU868)
#define LORAWAN_APPLICATION_KEY                     { 0xAA, 0x11, 0xBB, 0x22, 0xCC, 0x33, 0xDD, 0x44, 0xEE, 0x55, 0xFF, 0x66, 0xAA, 0x77, 0xBB, 0x88 }
#endif

/*!
 * Current network ID
 */
#define LORAWAN_NETWORK_ID                          ( uint32_t )0

/*!
 * Device address on the network (big endian)
 *
 * \remark In this application the value is automatically generated using
 *         a pseudo random generator seeded with a value derived from
 *         BoardUniqueId value if LORAWAN_DEVICE_ADDRESS is set to 0
 */
#if defined( REGION_IN865 )
#define LORAWAN_DEVICE_ADDRESS                      ( uint32_t )0x22000442  // CLASS_A, 0x22000443  CLASS_C
#elif  defined(REGION_US915)
#define LORAWAN_DEVICE_ADDRESS                      ( uint32_t )0x22000442
#elif  defined(REGION_KR920)
#define LORAWAN_DEVICE_ADDRESS                      ( uint32_t )0x00000000
#elif  defined(REGION_AS923)
#define LORAWAN_DEVICE_ADDRESS                      ( uint32_t )0x00000000
#elif  defined(REGION_EU868)
#define LORAWAN_DEVICE_ADDRESS                      ( uint32_t )0x00000000
#endif

/*!
 * AES encryption/decryption cipher network session key
 */
#define LORAWAN_NWKSKEY                             { 0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C }

/*!
 * AES encryption/decryption cipher application session key
 */
#define LORAWAN_APPSKEY                             { 0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C }

#endif // __LORA_COMMISSIONING_H__
