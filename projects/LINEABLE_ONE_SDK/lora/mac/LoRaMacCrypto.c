/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech
 ___ _____ _   ___ _  _____ ___  ___  ___ ___
/ __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
\__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
|___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
embedded.connectivity.solutions===============

Description: LoRa MAC layer implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis ( Semtech ), Gregory Cristian ( Semtech ) and Daniel Jaeckle ( STACKFORCE )
*/
#include <stdlib.h>
#include <stdint.h>
#include "utilities.h"

#include "mbedtls/config.h"
#include "mbedtls/aes.h"
#include "mbedtls/cmac.h"

#include "LoRaMacCrypto.h"

/*!
 * CMAC/AES Message Integrity Code (MIC) Block B0 size
 */
#define LORAMAC_MIC_BLOCK_B0_SIZE                   16

#define KEY_LENGTH_BITS                             128u

/*!
 * MIC field computation initial data
 */
static uint8_t MicBlockB0[] = { 0x49, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                              };

/**
 * AES computation context variable
 */
mbedtls_aes_context aes_ctx;

/**
 * CMAC computation context variable
 */
mbedtls_cipher_context_t aes_cmac_ctx[1];

/*!
 * \brief Computes the LoRaMAC frame MIC field
 *
 * \param [IN]  buffer          Data buffer
 * \param [IN]  size            Data buffer size
 * \param [IN]  key             AES key to be used
 * \param [IN]  address         Frame address
 * \param [IN]  dir             Frame direction [0: uplink, 1: downlink]
 * \param [IN]  sequenceCounter Frame sequence counter
 * \param [OUT] mic Computed MIC field
 */
void LoRaMacComputeMic( const uint8_t *buffer, uint16_t size, const uint8_t *key, uint32_t address, uint8_t dir,
        uint32_t sequenceCounter, uint32_t *mic )
{
    uint8_t computed_mic[16] = {};
    MicBlockB0[5] = dir;

    MicBlockB0[6] = ( address ) & 0xFF;
    MicBlockB0[7] = ( address >> 8 ) & 0xFF;
    MicBlockB0[8] = ( address >> 16 ) & 0xFF;
    MicBlockB0[9] = ( address >> 24 ) & 0xFF;

    MicBlockB0[10] = ( sequenceCounter ) & 0xFF;
    MicBlockB0[11] = ( sequenceCounter >> 8 ) & 0xFF;
    MicBlockB0[12] = ( sequenceCounter >> 16 ) & 0xFF;
    MicBlockB0[13] = ( sequenceCounter >> 24 ) & 0xFF;

    MicBlockB0[15] = size & 0xFF;

    mbedtls_cipher_init(aes_cmac_ctx);
    const mbedtls_cipher_info_t *cipher_info = mbedtls_cipher_info_from_type(MBEDTLS_CIPHER_AES_128_ECB);

    if (NULL == cipher_info)
        return;

    do {
        if (0 != mbedtls_cipher_setup(aes_cmac_ctx, cipher_info))
            break;
        if (0 != mbedtls_cipher_cmac_starts(aes_cmac_ctx, key, KEY_LENGTH_BITS))
            break;
        if (0 != mbedtls_cipher_cmac_update(aes_cmac_ctx, MicBlockB0, sizeof(MicBlockB0)))
            break;
        if (0 != mbedtls_cipher_cmac_update(aes_cmac_ctx, buffer, size & 0xFF))
            break;
        if (0 != mbedtls_cipher_cmac_finish(aes_cmac_ctx, computed_mic))
            break;

        *mic = (uint32_t)((uint32_t) computed_mic[3] << 24 | (uint32_t) computed_mic[2] << 16
                          | (uint32_t) computed_mic[1] << 8 | (uint32_t) computed_mic[0]);
    } while (false);

    mbedtls_cipher_free(aes_cmac_ctx);
}

void LoRaMacPayloadEncrypt( const uint8_t *buffer, uint16_t size, const uint8_t *key, uint32_t address, uint8_t dir,
                            uint32_t sequenceCounter, uint8_t *encBuffer )
{
    uint16_t i;
    uint8_t bufferIndex = 0;
    uint16_t ctr = 1;
    int ret = 0;
    uint8_t a_block[16] = {};
    uint8_t s_block[16] = {};

    mbedtls_aes_init(&aes_ctx);
    ret = mbedtls_aes_setkey_enc(&aes_ctx, key, KEY_LENGTH_BITS);
    if (0 != ret) {
        goto exit;
    }

    a_block[0] = 0x01;
    a_block[5] = dir;

    a_block[6] = (address) & 0xFF;
    a_block[7] = (address >> 8) & 0xFF;
    a_block[8] = (address >> 16) & 0xFF;
    a_block[9] = (address >> 24) & 0xFF;

    a_block[10] = (sequenceCounter) & 0xFF;
    a_block[11] = (sequenceCounter >> 8) & 0xFF;
    a_block[12] = (sequenceCounter >> 16) & 0xFF;
    a_block[13] = (sequenceCounter >> 24) & 0xFF;

    while (size >= 16) {
        a_block[15] = ((ctr) & 0xFF);
        ctr++;
        ret = mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, a_block, s_block);
        if (0 != ret) {
            goto exit;
        }

        for (i = 0; i < 16; i++) {
            encBuffer[bufferIndex + i] = buffer[bufferIndex + i] ^ s_block[i];
        }
        size -= 16;
        bufferIndex += 16;
    }

    if (size > 0) {
        a_block[15] = ((ctr) & 0xFF);
        ret = mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, a_block,
                                    s_block);
        if (0 != ret) {
            goto exit;
        }

        for (i = 0; i < size; i++) {
            encBuffer[bufferIndex + i] = buffer[bufferIndex + i] ^ s_block[i];
        }
    }

exit:
    mbedtls_aes_free(&aes_ctx);
    return;

}

void LoRaMacPayloadDecrypt(const uint8_t *buffer, uint16_t size, const uint8_t *key, uint32_t address, uint8_t dir,
        uint32_t sequenceCounter, uint8_t *decBuffer )
{
    LoRaMacPayloadEncrypt(buffer, size, key, address, dir, sequenceCounter, decBuffer);
}

void LoRaMacJoinComputeMic(const uint8_t *buffer, uint16_t size, const uint8_t *key, uint32_t *mic)
{
    uint8_t computed_mic[16] = {};

    mbedtls_cipher_init(aes_cmac_ctx);
    const mbedtls_cipher_info_t *cipher_info = mbedtls_cipher_info_from_type(MBEDTLS_CIPHER_AES_128_ECB);

    while (NULL != cipher_info) {
        if (0 != mbedtls_cipher_setup(aes_cmac_ctx, cipher_info))
            break;
        if (0 != mbedtls_cipher_cmac_starts(aes_cmac_ctx, key, KEY_LENGTH_BITS))
            break;
        if (0 != mbedtls_cipher_cmac_update(aes_cmac_ctx, buffer, size & 0xFF))
            break;
        if (0 != mbedtls_cipher_cmac_finish(aes_cmac_ctx, computed_mic))
            break;

        *mic = (uint32_t)((uint32_t) computed_mic[3] << 24 | (uint32_t) computed_mic[2] << 16
                          | (uint32_t) computed_mic[1] << 8 | (uint32_t) computed_mic[0]);
        break;
    }

    mbedtls_cipher_free(aes_cmac_ctx);
}

void LoRaMacJoinDecrypt( const uint8_t *buffer, uint16_t size, const uint8_t *key, uint8_t *decBuffer )
{
    mbedtls_aes_init(&aes_ctx);

    do {
        if (0 != mbedtls_aes_setkey_enc(&aes_ctx, key, KEY_LENGTH_BITS))
           break;
        if (0 != mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, buffer, decBuffer))
           break;

        // Check if optional CFList is included
        if (size >= 16) {
           mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, buffer + 16, decBuffer + 16);
        }
    } while (false);

   mbedtls_aes_free(&aes_ctx);
}

void LoRaMacJoinComputeSKeys( const uint8_t *key, const uint8_t *appNonce, uint16_t devNonce, uint8_t *nwkSKey, uint8_t *appSKey )
{
    uint8_t nonce[16];
    uint8_t *p_dev_nonce = (uint8_t *) &devNonce;

    mbedtls_aes_init(&aes_ctx);

    do {
        if (0 != mbedtls_aes_setkey_enc(&aes_ctx, key, KEY_LENGTH_BITS))
            break;

        memset(nonce, 0, sizeof(nonce));
        nonce[0] = 0x01;
        memcpy(nonce + 1, appNonce, 6);
        memcpy(nonce + 7, p_dev_nonce, 2);
        if (0 != mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, nonce, nwkSKey))
            break;

        memset(nonce, 0, sizeof(nonce));
        nonce[0] = 0x02;
        memcpy(nonce + 1, appNonce, 6);
        memcpy(nonce + 7, p_dev_nonce, 2);
        mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, nonce, appSKey);

    } while(false);

    mbedtls_aes_free(&aes_ctx);
}

void LoRaMacJoinComputeRealAppKey(const uint8_t *key, const uint8_t *appNonce, uint32_t netId, uint8_t *appKey)
{
    uint8_t nonce[16];

    mbedtls_aes_init(&aes_ctx);

    do {
        if (0 != mbedtls_aes_setkey_enc(&aes_ctx, key, KEY_LENGTH_BITS))
            break;
        memset( nonce, 0, sizeof( nonce ) );
        memcpy( nonce + 0, appNonce, 3 );
        memcpy( nonce + 3, (uint8_t *)&netId, 3 );

        if (0 != mbedtls_aes_crypt_ecb(&aes_ctx, MBEDTLS_AES_ENCRYPT, nonce, appKey))
            break;
    } while (false);

    mbedtls_aes_free(&aes_ctx);
}
