/***************************************************************************************************
 * @file lora_test_cmd.c
 * @brief
 * @version 0.0
***************************************************************************************************/

/***************************************************************************************************
*                                             INCLUDE FILES
***************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "rtos_gecko.h"
#include "lora_test_cmd.h"

#include "lora.h"
#include "LoRaMac.h"
#include "LoRaMacTest.h"
#include "loramac_ex.h"
#include <drivers/radio/sx1276/sx1276.h>
#include <drivers/radio/radio.h>

#include "userdata.h"
#include <drivers/fuelgauge/fuelgauge.h>
#include <drivers/pmic/pmic.h>
#include "device.h"

#include "shell.h"
#include "utilities.h"
#include "device.h"
#include "app_main.h"
#include "bsphalconfig.h"

/***************************************************************************************************
*                                            LOCAL DEFINES
***************************************************************************************************/
#define SHELL_Printf(...)    debug_printf_data(__VA_ARGS__)


#define LORAMAC_DEV_EUI_SIZE                    8
#define LORAMAC_APP_EUI_SIZE                    8

#define LORAMAC_APP_KEY_SIZE                    16
#define LORAMAC_NWK_SKEY_SIZE                   16
#define LORAMAC_APP_SKEY_SIZE                   16

#define LORAMAC_TX_DATARATE_MIN                 0
#define LORAMAC_TX_DATARATE_MAX                 5

#define LORAMAC_TX_POWER_MIN                    0
#define LORAMAC_TX_POWER_MAX                    6

#define LORAMAC_TX_CHANNEL_MIN                  26
#define LORAMAC_TX_CHANNEL_MAX                  32

#define LORAMAC_TX_RETRANSMISSION_COUNT_MIN     1
#define LORAMAC_TX_RETRANSMISSION_COUNT_MAX     15

#define LORAWAN_RETRANSMISSION_MIN              1
#define LORAWAN_RETRANSMISSION_MAX              8

#define BLUETOOTH_ID_SIZE                       6

/***************************************************************************************************
*                                             VARIABLES
***************************************************************************************************/
static uint8_t pPacket[256];
static uint8_t nPacketLen = 0;
static const char *response_str[2] = {"OK", "ERR"};

/***************************************************************************************************
*                                    LOCAL FUNCTIONS PROTOTYPES
***************************************************************************************************/
static void SHELL_Dump(const uint8_t *pData, uint32_t ulDataLen);
static void SHELL_PrintSystemInfo(void);
static bool SHELL_GetBool(char *pArgv, bool bDefault);

/***************************************************************************************************
*                                            LOCAL FUNCTIONS
***************************************************************************************************/

// General Commands
#if 0
static CPU_INT16S AT_CMD(CPU_INT16U        argc,
                         CPU_CHAR         *argv[],
                         SHELL_OUT_FNCT    out_fnct,
                         SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("TBD\n");
#if 0
    SHELL_CMD *pCmd = pShellCommonCmds;

    while (pCmd->pName != NULL)
    {
        if ((pCmd->pName[0] == 'A') && (pCmd->pName[1] == 'T') && (pCmd->pName[2] == '+'))
        {
            SHELL_Printf("%-16s : %s\n", &pCmd->pName[3], pCmd->pHelp);
        }
        pCmd++;
    }

    if (!UNIT_FACTORY_TEST)
    {
        pCmd = pShellLoRaWANCmds;

        while (pCmd->pName != NULL)
        {
            if ((pCmd->pName[0] == 'A') && (pCmd->pName[1] == 'T') && (pCmd->pName[2] == '+'))
            {
                SHELL_Printf("%-16s : %s\n", &pCmd->pName[3], pCmd->pHelp);
            }
            pCmd++;
        }
    }
    else
    {
        pCmd = pShellTestCmds;

        while (pCmd->pName != NULL)
        {
            if ((pCmd->pName[0] == 'A') && (pCmd->pName[1] == 'T') && (pCmd->pName[2] == '+'))
            {
                SHELL_Printf("%-16s : %s\n", &pCmd->pName[3], pCmd->pHelp);
            }
            pCmd++;
        }
    }
#endif
    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_Help(CPU_INT16U        argc,
                              CPU_CHAR         *argv[],
                              SHELL_OUT_FNCT    out_fnct,
                              SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("TBD\n");
#if 0
    SHELL_CMD *pCmd = pShellCommonCmds;

    while (pCmd->pName)
    {
        SHELL_Printf("%16s %s\n", pCmd->pName, pCmd->pHelp);
        pCmd++;
    }

    if (!UNIT_FACTORY_TEST)
    {
        pCmd = pShellLoRaWANCmds;

        while (pCmd->pName)
        {
            SHELL_Printf("%16s %s\n", pCmd->pName, pCmd->pHelp);
            pCmd++;
        }
    }
    else
    {
        pCmd = pShellTestCmds;

        while (pCmd->pName)
        {
            SHELL_Printf("%16s %s\n", pCmd->pName, pCmd->pHelp);
            pCmd++;
        }
    }
#endif
    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_Trace(CPU_INT16U        argc,
                               CPU_CHAR         *argv[],
                               SHELL_OUT_FNCT    out_fnct,
                               SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("TBD\n");
#if 0
    int nRet = -1;

    if (argc == 1)
    {
        nRet = 0;
    }
    else if (argc == 2)
    {
        if (strcasecmp(argv[1], "enable") == 0)
        {
            TRACE_SetEnable(true);
            nRet = 0;
        }
        else if (strcasecmp(argv[1], "disable") == 0)
        {
            TRACE_SetEnable(false);
            nRet = 0;
        }
        else if (strcasecmp(argv[1], "dump") == 0)
        {
            TRACE_SetDump(true);
            nRet = 0;
        }
        else if (strcasecmp(argv[1], "undump") == 0)
        {
            TRACE_SetDump(false);
            nRet = 0;
        }
    }
    else if (argc == 3)
    {
        if (TRACE_SetLevel(argv[2]))
        {
            nRet = 0;
        }
    }

    if (nRet == 0)
    {
        TRACE_ShowConfig();
    }

    return nRet;
#else
    return (SHELL_EXEC_ERR_NONE);
#endif
}

static CPU_INT16S AT_CMD_Log(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
#if 0
    if (argc == 1)
    {
        SHELL_Printf("Get LOG enable/disable\n");
        if (TRACE_GetEnable())
        {
            SHELL_Printf("- LOG Message Enabled.\n");
        }
        else
        {
            SHELL_Printf("- LOG Message Disabled.\n");
        }
    }
    else
    {
        bool ret = false;

        SHELL_Printf("Set LOG enable/disable\n");

        if (argc == 2)
        {
            if (strcasecmp(argv[1], "0") == 0)
            {
                ret = TRACE_SetEnable(false);
            }
            if (strcasecmp(argv[1], "1") == 0)
            {
                ret = TRACE_SetEnable(true);
            }
        }
        else if (argc == 3)
        {
            if (strcasecmp(argv[1], "level") == 0)
            {
                ret = TRACE_SetLevel(argv[2]);
            }
        }

        if (ret)
        {
            SHELL_Printf("- LOG Message %s.\n", (TRACE_GetEnable() ? "Enabled" : "Disabled"));
            SHELL_Printf("- LOG Message %s.\n", (TRACE_GetEnable() ? "Enabled" : "Disabled"));
        }
        else
        {
            SHELL_Printf("- ERROR, Invalid Arguments\n");
        }
    }
#endif
    return (SHELL_EXEC_ERR_NONE);
}
#endif

static CPU_INT16S AT_CMD_FirmwareVer(CPU_INT16U        argc,
                                     CPU_CHAR         *argv[],
                                     SHELL_OUT_FNCT    out_fnct,
                                     SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        SHELL_Printf("Firmware Information\n");
        SHELL_Printf("- Version : %s\n", DeviceFWVersion());
    }
    else if (argc == 2)
    {
        if (strcasecmp(argv[1], "winner") == 0)
        {
            SET_USERFLAG(FLAG_FACTORY_TEST);
        }
        else
        {
            return (SHELL_EXEC_ERR);
        }
    }
    else
    {
        return (SHELL_EXEC_ERR);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_Reset(CPU_INT16U        argc,
                               CPU_CHAR         *argv[],
                               SHELL_OUT_FNCT    out_fnct,
                               SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t type;

    if ((argc == 2) && (strcasecmp(argv[1], "user") == 0))
    {
        type = SYSTEM_RESET_WITH_USERDATA;
    }
    else
    {
        type = SYSTEM_RESET_NORMAL;
    }

    SHELL_Printf("-%s\n\n", response_str[LORA_OK]);

    DeviceSystemReset(type, 0);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_BATT(CPU_INT16U        argc,
                              CPU_CHAR         *argv[],
                              SHELL_OUT_FNCT    out_fnct,
                              SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {

        SHELL_Printf("BATTERY INFORMATION\n");

        uint16_t battery_val;
        FUELGAUGE_GetSoc(&battery_val);

        SHELL_Printf("- Battery Level : %d\n", battery_val);
    }

    return (SHELL_EXEC_ERR_NONE);
}


// Lora Parameters management
static CPU_INT16S AT_CMD_DevEUI(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t  err_code = LORA_CMD_ERR_NONE;
    uint8_t pDevEui[LORAMAC_DEV_EUI_SIZE] = {0};
    uint8_t pBtID[6] = {0};

    if (argc == 1)
    {
        ret = LORA_OK;
    }
    else if (argc == 2)
    {
        if (HexString2Array(argv[1], pDevEui, sizeof(pDevEui)) == LORAMAC_DEV_EUI_SIZE)
        {
            if (UNIT_FACTORY_TEST)
            {
                //parsing BTID
                memcpy(pBtID, pDevEui, 3);
                memcpy((pBtID + 3), (pDevEui + 5), 3);
                DeviceUserDataSetDevEUI(pDevEui);     //DevEUI - XX:XX:XX:FF:FE:XX:XX:XX
                DeviceUserDateSetBluetoothID(pBtID);  //BTID   - XX:XX:XX:XX:XX:XX
                ret = LORA_OK;
            }
            else
            {
                err_code = LORA_CMD_ERR_NOT_AVAIABLE;
            }
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (ret == LORA_OK)
    {
        SHELL_Printf("-%s=[ ", response_str[LORA_OK]);
        SHELL_Dump(UNIT_DEVEUID, LORAMAC_DEV_EUI_SIZE);
        SHELL_Printf(" ]\n\n");
    }
    else
    {
        SHELL_Printf("-%s=%d\n\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_AppKey(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint8_t pAppKey[LORAMAC_APP_KEY_SIZE] = {0};

    if (argc == 1)
    {
        ret = LORA_OK;
    }
    else if (argc == 2)
    {
        if (HexString2Array(argv[1], pAppKey, sizeof(pAppKey)) == LORAMAC_APP_KEY_SIZE)
        {
            DeviceUserDataSetAppKey(pAppKey);
            ret = LORA_OK;
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (ret == LORA_OK)
    {
        SHELL_Printf("-%s=[ ", response_str[LORA_OK]);
        SHELL_Dump(UNIT_APPKEY, LORAMAC_APP_KEY_SIZE);
        SHELL_Printf(" ]\n\n");
    }
    else
    {
        SHELL_Printf("-%s=%d\n\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_AppEUI(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint8_t pAppEui[LORAMAC_APP_EUI_SIZE] = {0};

    if (argc == 1)
    {
        ret = LORA_OK;
    }
    else if (argc == 2)
    {
        if (HexString2Array(argv[1], pAppEui, sizeof(pAppEui)) == LORAMAC_APP_EUI_SIZE)
        {
            DeviceUserDataSetAppEUI(pAppEui);
            ret = LORA_OK;
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (ret == LORA_OK)
    {
        SHELL_Printf("-%s=[ ", response_str[LORA_OK]);
        SHELL_Dump(UNIT_APPEUID, LORAMAC_APP_EUI_SIZE);
        SHELL_Printf(" ]\n\n");
    }
    else
    {
        SHELL_Printf("-%s=%d\n\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_RealAppKey(CPU_INT16U        argc,
                                    CPU_CHAR         *argv[],
                                    SHELL_OUT_FNCT    out_fnct,
                                    SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint8_t RealAppKey[16] = {0};

    if (argc == 1)
    {
        ret = LORA_OK;
    }
    else if (argc == 2)
    {
        if (HexString2Array(argv[1], RealAppKey, sizeof(RealAppKey)) == LORAMAC_APP_KEY_SIZE)
        {
            /* Save real app key */
            DeviceUserDataSetSKTRealAppKey(RealAppKey);
            /* Set flag about real app key */
            DeviceUserDataSetFlag(FLAG_INSTALLED, FLAG_INSTALLED);
            DeviceUserDataSetFlag(FLAG_USE_RAK, FLAG_USE_RAK);
            ret = LORA_OK;
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (ret == LORA_OK)
    {
        SHELL_Printf("-%s=[ ", response_str[LORA_OK]);
        SHELL_Dump(UNIT_REALAPPKEY, LORAMAC_APP_KEY_SIZE);
        SHELL_Printf(" ]\n\n");
    }
    else
    {
        SHELL_Printf("-%s=%d\n\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

// Join and send data
static CPU_INT16S AT_CMD_Join(CPU_INT16U        argc,
                              CPU_CHAR         *argv[],
                              SHELL_OUT_FNCT    out_fnct,
                              SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t err_code = 0;
    LoRaJoinType type;

    if (argc == 2)
    {
        if (strcasecmp(argv[1], "otta") == 0)
        {
            type = LORA_JOIN_TYPE_OTTA;
        }
        else if (strcasecmp(argv[1], "abp") == 0)
        {
            type = LORA_JOIN_TYPE_ABP;
        }
        else if (strcasecmp(argv[1], "pseudo") == 0)
        {
            type = LORA_JOIN_TYPE_PSEUDO;
        }
        else if (strcasecmp(argv[1], "real") == 0)
        {
            type = LORA_JOIN_TYPE_REAL;
        }
        else
        {
            type = LORA_JOIN_TYPE_NONE;
        }

        if (LORAWAN_JoinNetwork(type) == LORA_FAIL)
        {
            err_code = 2;
        }
    }
    else
    {
        err_code = 1;
    }

    if (err_code == 0)
    {
        SHELL_Printf("-%s\n\n", response_str[LORA_OK]);
    }
    else
    {
        SHELL_Printf("-%s=%d\n\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_USend(CPU_INT16U        argc,
                               CPU_CHAR         *argv[],
                               SHELL_OUT_FNCT    out_fnct,
                               SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint32_t ulLen;

    if (argc == 2)
    {
        ulLen = strlen(argv[1]);

        nPacketLen = HexString2Array(argv[1], pPacket, sizeof(pPacket));
        if (nPacketLen == 0)
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
        else if (ulLen > nPacketLen * 2)
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
        else
        {
            ret = LORAWAN_SendData(LORA_APP_SERVER_PORT, false, pPacket, nPacketLen, false, false);
            err_code = (ret == LORA_OK) ? LORA_CMD_ERR_NONE : LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? nPacketLen : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_CSend(CPU_INT16U        argc,
                               CPU_CHAR         *argv[],
                               SHELL_OUT_FNCT    out_fnct,
                               SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint32_t ulLen;

    if (argc == 2)
    {
        ulLen = strlen(argv[1]);

        nPacketLen = HexString2Array(argv[1], pPacket, sizeof(pPacket));
        if (nPacketLen == 0)
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
        else if (ulLen > nPacketLen * 2)
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
        else
        {
            ret = LORAWAN_SendData(LORA_APP_SERVER_PORT, true, pPacket, nPacketLen, false, false);
            err_code = (ret == LORA_OK) ? LORA_CMD_ERR_NONE : LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? nPacketLen : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static const uint8_t max_payload_tbl[LORAMAC_TX_DATARATE_MAX + 1] =
{
    65, /* DR0 */
    65, /* DR1 */
    65, /* DR2 */
    65, /* DR3 */
    65, /* DR4 */
    65  /* DR5 */
};

static CPU_INT16S AT_CMD_SendUnconfirmedMsg(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    RTOS_ERR err;
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint8_t packet_len = 0;
    int8_t cur_dr = LORAMAC_GetDatarate();

    if (argc == 2)
    {
        packet_len = atoi(argv[1]);
        if ((packet_len > 0) && (packet_len <= max_payload_tbl[cur_dr]))
        {
            ret = LORAWAN_SendData(LORA_APP_SERVER_PORT, false, pPacket, packet_len, false, false);
            err_code = (ret == LORA_OK) ? LORA_CMD_ERR_NONE : LORA_CMD_ERR_NETWORK;
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (err_code == LORA_CMD_ERR_NONE)
    {
        OSTimeDly(OS_MS_2_TICKS(6000),
                  OS_OPT_TIME_DLY,
                  &err);
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? packet_len : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SendConfirmedMsg(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    uint8_t packet_len = 0;
    int8_t cur_dr = LORAMAC_GetDatarate();

    if (argc == 2)
    {
        packet_len = atoi(argv[1]);
        if ((packet_len > 0) && (packet_len <= max_payload_tbl[cur_dr]))
        {
            ret = LORAWAN_SendData(LORA_APP_SERVER_PORT, true, pPacket, packet_len, true, false);
            err_code = (ret == LORA_OK) ? LORA_CMD_ERR_NONE : LORA_CMD_ERR_NETWORK;
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? packet_len : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_TestSend(CPU_INT16U        argc,
                                  CPU_CHAR         *argv[],
                                  SHELL_OUT_FNCT    out_fnct,
                                  SHELL_CMD_PARAM  *p_cmd_param)
{
#if 0
    SHELL_Printf("TEST SEND PACKET\n");
    if (argc == 4)
    {
        uint32_t ulPort = atoi(argv[1]);
        uint32_t ulCount = atoi(argv[2]);
        uint32_t ulLen = atoi(argv[3]);

        if (ulLen > sizeof(pPacket) - 1)
        {
            SHELL_Printf("- ERROR, Max Payload Size : %d Byte\n", sizeof(pPacket) - 1);
            return 0;
        }

        if (LORAWAN_IsNetworkJoined())
        {
            memset(pPacket, 0, sizeof(pPacket));
            for (uint32_t i = 0 ; i < ulCount ; i++)
            {
                for (uint32_t j = 0 ; j < ulLen ; j++)
                {
                    pPacket[j] = '0' + (i + j) % 10;
                }

                if (LORATEST_Send(ulPort, 1, 10, pPacket, ulLen) == false)
                {
                    SHELL_Printf("ERROR, Failed to send packet[%ld] - %s!\n", i, pPacket);
                }
                else
                {
                    SHELL_Printf("[%3ld] - %s - SUCCESS!\n", i, pPacket);
                }
            }
        }
        else
        {
            SHELL_Printf("- ERROR, The device is not joined to the network.\n");
        }
    }
    else
    {
        SHELL_Printf("- ERROR, Invalid Arguments\n");
    }
#endif

    return (SHELL_EXEC_ERR_NONE);
}


static CPU_INT16S AT_CMD_LinkCheck(CPU_INT16U        argc,
                                   CPU_CHAR         *argv[],
                                   SHELL_OUT_FNCT    out_fnct,
                                   SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;

    if (argc == 1)
    {
        ret = LORAWAN_SendLinkCheckRequest();
        if (ret == LORA_FAIL)
        {
            err_code = LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (err_code == LORA_CMD_ERR_NONE)
    {
        SHELL_Printf("-%s=%d,%d\n", response_str[LORA_OK], g_demod_margin, g_nb_gateways);
    }
    else
    {
        SHELL_Printf("=%s=%d\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_DeviceTimeRequest(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;

    if (argc == 1)
    {
        ret = LORAWAN_SendDevTimeReq();
        if (ret == LORA_FAIL)
        {
            err_code = LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (err_code == LORA_CMD_ERR_NONE)
    {
        SHELL_Printf("-%s=%10s\n", response_str[LORA_OK], g_device_time);
    }
    else
    {
        SHELL_Printf("=%s=%d\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}


static CPU_INT16S AT_CMD_ChangeMsgType(CPU_INT16U        argc,
                                       CPU_CHAR         *argv[],
                                       SHELL_OUT_FNCT    out_fnct,
                                       SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    bool is_cfm;

    if (argc == 2)
    {
        is_cfm = SHELL_GetBool(argv[1], false);
        ret = MainApp_ChangeMsgType(is_cfm);
        if (ret == LORA_FAIL)
        {
            err_code = LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (err_code == LORA_CMD_ERR_NONE)
    {
        SHELL_Printf("-%s=%s\n", response_str[LORA_OK], (is_cfm == true) ? "CONFIRMED" : "UNCONFIRMED");
    }
    else
    {
        SHELL_Printf("=%s=%d\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}


static CPU_INT16S AT_CMD_ChangeTestMode(CPU_INT16U        argc,
                                        CPU_CHAR         *argv[],
                                        SHELL_OUT_FNCT    out_fnct,
                                        SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    bool run;

    if (argc == 2)
    {
        run = SHELL_GetBool(argv[1], false);
        ret = MainApp_ChangeTestMode(run);
        if (ret == LORA_FAIL)
        {
            err_code = LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    if (err_code == LORA_CMD_ERR_NONE)
    {
        SHELL_Printf("-%s=%s\n", response_str[LORA_OK], (run == true) ? "RUN" : "STOP");
    }
    else
    {
        SHELL_Printf("=%s=%d\n", response_str[LORA_FAIL], err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}


static CPU_INT16S AT_CMD_Mac(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        switch (LORAMAC_GetClassType())
        {
            case CLASS_A :
                SHELL_Printf("%16s : Class A\n", "Device Class");
                break;
            case CLASS_B :
                SHELL_Printf("%16s : Class B\n", "Device Class");
                break;
            case CLASS_C :
                SHELL_Printf("%16s : Class C\n", "Device Class");
                break;
            default :
                SHELL_Printf("%16s : Unknown\n", "Device Class");
                break;
        }

        SHELL_Printf("%16s : %d\n", "RSSI", LORAWAN_GetRSSI());
        SHELL_Printf("%16s : %d\n", "SNR", LORAWAN_GetSNR());
        SHELL_Printf("%16s : %d\n", "DATARATE", LORAMAC_GetDatarate());
        SHELL_Printf("%16s : %d\n", "Tx Power", LORAMAC_GetTxPower());
        SHELL_Printf("%16s : %d\n", "Retransmission", LORAWAN_GetMaxRetries());

        uint32_t    AppNonce;
        LORAMAC_GetAppNonce(&AppNonce);
        SHELL_Printf("%16s : %06lx\n", "App Nonce", AppNonce);

    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_Status(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("TBD\n");
#if 0
    if (argc == 1)
    {
        SHELL_Printf("%16s : %s\n", "LoRaWAN", LORAWAN_GetStatusString());
    }
#endif

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_ClearRealKeyFlag(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    CLEAR_USERFLAG(UNIT_USE_RAK);

    /* Reboot device */
    DeviceSystemReset(SYSTEM_RESET_NORMAL, 0);

    return (SHELL_EXEC_ERR_NONE);
}
#if 0
static CPU_INT16S AT_CMD_SetConfig(CPU_INT16U        argc,
                                   CPU_CHAR         *argv[],
                                   SHELL_OUT_FNCT    out_fnct,
                                   SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("TBD\n");
#if 0
    int nRet = -1;

    SHELL_Printf("Set Configuration\n");

    if (argc == 3)
    {
        if (strcasecmp(argv[1], "auto_attach") == 0)
        {
            bool bEnable = SHELL_GetBool(argv[2], UNIT_AUTO_ATTACH);

            UPDATE_USERFLAG(FLAG_AUTO_ATTACH, bEnable);

            nRet = 0;
        }
    }

    if (nRet != 0)
    {
        SHELL_Printf(" - Invalid command\n");
    }
#endif
    return (SHELL_EXEC_ERR_NONE);
}
#endif
static CPU_INT16S AT_CMD_GetConfig(CPU_INT16U        argc,
                                   CPU_CHAR         *argv[],
                                   SHELL_OUT_FNCT    out_fnct,
                                   SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("Get Configuration\n");
    SHELL_PrintSystemInfo();

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetTxDR(CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    int8_t nDatarate;

    if (argc == 1)
    {
        nDatarate = LORAMAC_GetDatarate();
        ret = LORA_OK;
    }
    else if (argc == 2)
    {
        nDatarate = atoi(argv[1]);
        if ((LORAMAC_TX_DATARATE_MIN <= nDatarate) && (nDatarate <= LORAMAC_TX_DATARATE_MAX))
        {
            ret = (LORAMAC_SetDatarate(nDatarate) == true) ? LORA_OK : LORA_FAIL;
            if (ret != LORA_OK)
            {
                err_code = LORA_CMD_ERR_NETWORK;
            }
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? nDatarate : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_TxPower(CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    int8_t nTxPower = 0;

    if (argc == 1)
    {
        nTxPower = LORAMAC_GetTxPower();
        ret = LORA_OK;
    }
    else if (argc == 2)
    {
        nTxPower = atoi(argv[1]);
        if ((LORAMAC_TX_POWER_MIN <= nTxPower) && (nTxPower <= LORAMAC_TX_POWER_MAX))
        {
            if (LORAMAC_SetTxPower(nTxPower) == true)
            {
                ret = LORA_OK;
            }
            else
            {
                err_code = LORA_CMD_ERR_NETWORK;
            }
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? nTxPower : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetChannelAndTxPower(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    int8_t nTxPower = 0;
    int8_t nTxChannel = 0;

    if (argc == 3)
    {
        nTxChannel = atoi(argv[1]);
        nTxPower = atoi(argv[2]);

        if (nTxChannel == 25)
        {
            if ((2 <= nTxPower) && (nTxPower <= LORAMAC_TX_POWER_MAX))
            {
                LoRaMacTestSetChannel(nTxChannel);
                if (LORAMAC_SetTxPower(nTxPower) == true)
                {
                    ret = LORA_OK;
                }
                else
                {
                    err_code = LORA_CMD_ERR_NETWORK;
                }
            }
            else
            {
                err_code = LORA_CMD_ERR_INVALID_PARAM;
            }
        }
        else if ((LORAMAC_TX_CHANNEL_MIN <= nTxChannel) && (nTxChannel <= LORAMAC_TX_CHANNEL_MAX))
        {
            if ((LORAMAC_TX_POWER_MIN <= nTxPower) && (nTxPower <= LORAMAC_TX_POWER_MAX))
            {
                LoRaMacTestSetChannel(nTxChannel);
                if (LORAMAC_SetTxPower(nTxPower) == true)
                {
                    ret = LORA_OK;
                }
                else
                {
                    err_code = LORA_CMD_ERR_NETWORK;
                }
            }
            else
            {
                err_code = LORA_CMD_ERR_INVALID_PARAM;
            }
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=", response_str[ret]);
    if (ret == LORA_OK)
    {
        SHELL_Printf("%d,%d\n\n", nTxChannel, nTxPower);
    }
    else
    {
        SHELL_Printf("%d\n\n", err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_Channel(CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        SHELL_Printf("GET TX FREQUENCY\n");
#if defined(REGION_KR920)
        SHELL_Printf("- Tx Frequency : %d\n", 921900000 + 200000 * LoRaMacTestGetChannel());
#elif defined(REGION_IN865)
        SHELL_Printf("- Tx Frequency : %d\n", 865400000 + 200000 * LoRaMacTestGetChannel());
#endif
    }
    else
    {
        bool ret = false;

        SHELL_Printf("SET TX CHANNEL\n");

        if (argc == 2)
        {
            int8_t nTxChannel = atoi(argv[1]);
            if ((LORAMAC_TX_CHANNEL_MIN <= nTxChannel) && (nTxChannel <= LORAMAC_TX_CHANNEL_MAX))
            {
                ret = LoRaMacTestSetChannel(nTxChannel - LORAMAC_TX_CHANNEL_MIN + 1);
            }
        }

        if (ret)
        {
            SHELL_Printf("- Tx CH ID : %d\n", LoRaMacTestGetChannel() +  25);
        }
        else
        {
            SHELL_Printf("- ERROR, Invalid Arguments\n");
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_ADR(CPU_INT16U        argc,
                             CPU_CHAR         *argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    bool adr;

    if (argc == 1)
    {
        ret = LORA_OK;
        adr = LORAMAC_GetADR();
    }
    else if (argc == 2)
    {
        adr = SHELL_GetBool(argv[1], false);
        if (LORAMAC_SetADR(adr) == true)
        {
            ret = LORA_OK;
        }
        else
        {
            err_code = LORA_CMD_ERR_NETWORK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=", response_str[ret]);
    if (ret == LORA_OK)
    {
        SHELL_Printf("%s\n\n", (adr == true) ? "On" : "Off");
    }
    else
    {
        SHELL_Printf("%d\n\n", err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_RSSI_SNR(CPU_INT16U        argc,
                                  CPU_CHAR         *argv[],
                                  SHELL_OUT_FNCT    out_fnct,
                                  SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    int16_t rssi = 0;
    uint8_t snr = 0;

    if (argc == 1)
    {
        rssi = LORAWAN_GetRSSI();
        snr = LORAWAN_GetSNR();

        if ((rssi == 0) && (snr == 0))
        {
            err_code = LORA_CMD_ERR_NETWORK;
        }
        else
        {
            ret = LORA_OK;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=", response_str[ret]);
    if (ret == LORA_OK)
    {
        SHELL_Printf("%d,%d\n\n", LORAWAN_GetRSSI(), LORAWAN_GetSNR());
    }
    else
    {
        SHELL_Printf("%d\n\n", err_code);
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_TxRetransmissionNumber(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t ret = LORA_FAIL;
    int8_t err_code = LORA_CMD_ERR_NONE;
    int8_t nRetries = 0;

    if (argc == 1)
    {
        ret = LORA_OK;
        nRetries = LORAWAN_GetMaxRetries();
    }
    else if (argc == 2)
    {
        nRetries = atoi(argv[1]);
        if ((LORAWAN_RETRANSMISSION_MIN <= nRetries) && (nRetries <= LORAWAN_RETRANSMISSION_MAX))
        {
            if (LORAWAN_SetMaxRetries(nRetries) == LORA_OK)
            {
                ret = LORA_OK;
            }
            else
            {
                err_code = LORA_CMD_ERR_NETWORK;
            }
        }
        else
        {
            err_code = LORA_CMD_ERR_INVALID_PARAM;
        }
    }
    else
    {
        err_code = LORA_CMD_ERR_NUM_PARAM;
    }

    SHELL_Printf("-%s=%d\n\n", response_str[ret], (ret == LORA_OK) ? nRetries : err_code);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_UnconfirmedRetransmissionNumber(CPU_INT16U        argc,
        CPU_CHAR         *argv[],
        SHELL_OUT_FNCT    out_fnct,
        SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        SHELL_Printf("Get Tx Unconfirmed Retransmission Count\n");
        SHELL_Printf("- Count : %ld\n", LORAMAC_GetChannelsNbRepeat());
    }
    else
    {
        bool ret = false;

        SHELL_Printf("Set Tx Unconfirmed Retransmission Count\n");
        if (argc == 2)
        {
            uint32_t nCount = atoi(argv[1]);
            if ((LORAMAC_TX_RETRANSMISSION_COUNT_MIN <= nCount) && (nCount <= LORAMAC_TX_RETRANSMISSION_COUNT_MAX))
            {
                ret = LORAMAC_SetChannelsNbRepeat(nCount);
            }
        }

        if (ret)
        {
            SHELL_Printf("- Count : %ld\n", LORAMAC_GetChannelsNbRepeat());
        }
        else
        {
            SHELL_Printf("- ERROR, Invalid Arguments\n");
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_FCNT(CPU_INT16U        argc,
                              CPU_CHAR         *argv[],
                              SHELL_OUT_FNCT    out_fnct,
                              SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        SHELL_Printf("GET UP/DOWN LINK COUNTER\n");
        SHELL_Printf("- Up Link Counter : %ld\n", LORAMAC_GetUpLinkCounter());
        SHELL_Printf("- Down Link Counter : %ld\n", LORAMAC_GetDownLinkCounter());
    }
    else
    {
        bool rc = false;

        if (argc == 3)
        {
            uint32_t ulCounter = atoi(argv[2]);

            if (strcasecmp(argv[1], "up") == 0)
            {
                LORAMAC_SetUpLinkCounter(ulCounter);
                rc = true;
            }
            else if (strcasecmp(argv[1], "down") == 0)
            {
                LORAMAC_SetDownLinkCounter(ulCounter);
                rc = true;
            }
        }

        if (rc)
        {
            SHELL_Printf("- Up Link Counter : %ld\n", LORAMAC_GetUpLinkCounter());
            SHELL_Printf("- Down Link Counter : %ld\n", LORAMAC_GetDownLinkCounter());
        }
        else
        {
            SHELL_Printf("- ERROR, Invalid Arguments\n");
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}


static CPU_INT16S AT_CMD_SetTestMode(CPU_INT16U        argc,
                                     CPU_CHAR         *argv[],
                                     SHELL_OUT_FNCT    out_fnct,
                                     SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 2)
    {
        if (strcasecmp(argv[1], "enable") == 0)
        {
            LoRaMacSetTestMode(true);
        }
        else if (strcasecmp(argv[1], "disable") == 0)
        {
            LoRaMacSetTestMode(false);
        }
    }

    SHELL_Printf("Test Mode : %s\n", (LoRaMacGetTestMode() ? "Enable" : "Disable"));

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetFactoryMode(CPU_INT16U        argc,
                                        CPU_CHAR         *argv[],
                                        SHELL_OUT_FNCT    out_fnct,
                                        SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("- Operation mode changed to factory mode.\n");

    //    RTOS_ERR err;
    SET_USERFLAG(FLAG_FACTORY_TEST);
    //    Shell_CmdTblRem("LoRaCmds", &err);
    //    Shell_CmdTblAdd("FactCmds", pShellTestCmds, &err);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetSktMode(CPU_INT16U        argc,
                                    CPU_CHAR         *argv[],
                                    SHELL_OUT_FNCT    out_fnct,
                                    SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("- Operation mode changed to skt certification test mode.\n");

    if (UNIT_FACTORY_TEST)
    {
        CLEAR_USERFLAG(FLAG_FACTORY_TEST);
    }
    if (UNIT_SKT_OTB_TEST)
    {
        CLEAR_USERFLAG(FLAG_SKT_OTB_TEST);
    }
    if (UNIT_CERTI_TEST)
    {
        CLEAR_USERFLAG(FLAG_CERTI_TEST);
    }

    /* Set flag about skt certification test mode */
    if (!UNIT_SKT_CERTI_TEST)
    {
        SET_USERFLAG(FLAG_SKT_CERTI_TEST);
    }

    /* Reboot device */
    DeviceSystemReset(SYSTEM_RESET_NORMAL, 0);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetOtbMode(CPU_INT16U        argc,
                                    CPU_CHAR         *argv[],
                                    SHELL_OUT_FNCT    out_fnct,
                                    SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 2)
    {
        if (strcasecmp(argv[1], "ON") == 0)
        {
            if (!UNIT_SKT_OTB_TEST)
            {
                SET_USERFLAG(FLAG_SKT_OTB_TEST);
            }
            SHELL_Printf("LoRa Debug Log : Enable\n");
        }
        else if (strcasecmp(argv[1], "OFF") == 0)
        {
            if (UNIT_SKT_OTB_TEST)
            {
                CLEAR_USERFLAG(FLAG_SKT_OTB_TEST);
            }
            SHELL_Printf("LoRa Debug Log : Disable\n");
        }
        else
        {
            SHELL_Printf("Invalid arguments\n");
        }
    }
    else
    {
        SHELL_Printf("Invalid arguments\n");
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetCertiMode(CPU_INT16U       argc,
                                      CPU_CHAR        *argv[],
                                      SHELL_OUT_FNCT   out_fnct,
                                      SHELL_CMD_PARAM *p_cmd_param)
{
    SHELL_Printf("- Operation mode changed to certification test mode.\n");

    /* Clear flag */
    CLEAR_USERFLAG(FLAG_SKT_CERTI_TEST);
    CLEAR_USERFLAG(FLAG_FACTORY_TEST);
    CLEAR_USERFLAG(FLAG_SKT_OTB_TEST);

    /* Set flag */
    SET_USERFLAG(FLAG_CERTI_TEST);

    /* Reboot device */
    DeviceSystemReset(SYSTEM_RESET_NORMAL, 0);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetLoRaWANMode(CPU_INT16U        argc,
                                        CPU_CHAR         *argv[],
                                        SHELL_OUT_FNCT    out_fnct,
                                        SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("- Operation mode changed to LoRaWAN mode.\n");

    /* Clear flag */
    if (UNIT_FACTORY_TEST)
    {
        CLEAR_USERFLAG(FLAG_FACTORY_TEST);
    }
    if (UNIT_SKT_CERTI_TEST)
    {
        CLEAR_USERFLAG(FLAG_SKT_CERTI_TEST);
    }
    if (UNIT_CERTI_TEST)
    {
        CLEAR_USERFLAG(FLAG_CERTI_TEST);
    }
    if (UNIT_SKT_OTB_TEST)
    {
        CLEAR_USERFLAG(FLAG_SKT_OTB_TEST);
    }

    /* Reboot device */
    DeviceSystemReset(SYSTEM_RESET_NORMAL, 0);

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_TxCW(CPU_INT16U        argc,
                              CPU_CHAR         *argv[],
                              SHELL_OUT_FNCT    out_fnct,
                              SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("Tx Continuous\n");
    if (!UNIT_FACTORY_TEST)
    {
        if (argc == 5)
        {
            uint32_t ulChannel = atoi(argv[1]);
            uint32_t ulDatarate = atoi(argv[2]);
            uint32_t ulPower = atoi(argv[3]);
            uint32_t ulTimeout = atoi(argv[4]);  // second

            LoRaMacTestSetChannel(ulChannel);
            LORAMAC_SetDatarate(ulDatarate);
            LORAMAC_SetTxPower(ulPower);
            SHELL_Printf("CH ID %d, DataRate %d, Power %d, Time(s) %d\n", ulChannel, ulDatarate, ulPower, ulTimeout);

            LORAMAC_TxCW(ulTimeout);
        }
        else
        {
            SHELL_Printf("- ERROR, Invalid Arguments\n");
            SHELL_Printf("- Example, AT+TXCW 1 1 14 5\n");
        }
    }
    else
    {
        if (argc == 1)
        {
            SHELL_Printf("Usage : %s <Frequency> <Power> <Time>\n", argv[0]);
        }
        else if (argc == 4)
        {
            uint32_t ulFrequency = atoi(argv[1]);
            uint32_t ulPower = atoi(argv[2]);
            uint32_t ulTimeout = atoi(argv[3]);
            uint32_t ulTime = 0;
            SX1276SetTxContinuousWave(ulFrequency, ulPower, ulTimeout);

            while (SX1276GetStatus() == RF_TX_RUNNING)
            {
                //                vTaskDelay(1000);
                ulTime++;
                SHELL_Printf("- Progress : %3ld%%(%ld/%ld)\n", ulTime * 100 / ulTimeout, ulTime, ulTimeout);
            }
            SHELL_Printf("- Done.\n");
        }
        else
        {
            SHELL_Printf("- ERROR, Invalid Arguments\n");
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_TestRx(CPU_INT16U        argc,
                                CPU_CHAR         *argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("Test Rx\n");
    if (argc == 1)
    {
    }
    else if (argc == 2)
    {
        if ((strcasecmp(argv[1], "0") == 0) || (strcasecmp(argv[1], "disable") == 0))
        {
            SHELL_Printf("Test Rx disable\n");
            LoRaMacTestRx(false);
        }
        else if ((strcasecmp(argv[1], "1") == 0) || (strcasecmp(argv[1], "enable") == 0))
        {
            SHELL_Printf("Test Rx enable\n");
            LoRaMacTestRx(true);
        }
        else
        {
            SHELL_Printf("- ERROR\n");
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_ControlLoRaLDO(CPU_INT16U        argc,
                                        CPU_CHAR         *argv[],
                                        SHELL_OUT_FNCT    out_fnct,
                                        SHELL_CMD_PARAM  *p_cmd_param)
{
    SHELL_Printf("Control LoRa LDO\n");
    if (argc == 1)
    {
    }
    else if (argc == 2)
    {
        if ((strcasecmp(argv[1], "0") == 0) || (strcasecmp(argv[1], "disable") == 0))
        {
            SHELL_Printf("LDO disable\n");
            GPIO_PinModeSet(BSP_LORA_EN_PORT, BSP_LORA_EN_PIN, gpioModePushPull, 0);
        }
        else if ((strcasecmp(argv[1], "1") == 0) || (strcasecmp(argv[1], "enable") == 0))
        {
            SHELL_Printf("LDO enable\n");
            GPIO_PinModeSet(BSP_LORA_EN_PORT, BSP_LORA_EN_PIN, gpioModePushPull, 1);
        }
        else
        {
            SHELL_Printf("- ERROR\n");
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_SetHardwareRevision(CPU_INT16U        argc,
                                            CPU_CHAR         *argv[],
                                            SHELL_OUT_FNCT    out_fnct,
                                            SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        uint8_t rev = (uint8_t*)UNIT_HWREV;
        SHELL_Printf("GET HardwareRevision\n");
        SHELL_Printf("- HardwareRevision: %d.%d", rev/10, rev%10);
    }
    else
    {
        if (UNIT_FACTORY_TEST)
        {
            bool ret = false;

            SHELL_Printf("SET HardwareRevision\n");

            if (argc == 2)
            {
              double val = atof(argv[1]);

              if (val > 25.5 || val < 0.4)
              {
                SHELL_Printf("- ERROR, 0.4 < version < 25.5 \n");
              }
              else
              {
                DeviceUserDataHardwareRevision((uint8_t)(val*10));
                ret = true;
              }
            }

            if (ret)
            {
              uint8_t rev = (uint8_t*)UNIT_HWREV;
              SHELL_Printf("GET HardwareRevision\n");
              SHELL_Printf("- HardwareRevision: %d.%d", rev/10, rev%10);
            }
            else
            {
                SHELL_Printf("- ERROR, Invalid Arguments\n");
            }
        }
    }

    return (SHELL_EXEC_ERR_NONE);

}

static CPU_INT16S AT_CMD_BluetoothID(CPU_INT16U        argc,
                                     CPU_CHAR         *argv[],
                                     SHELL_OUT_FNCT    out_fnct,
                                     SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc == 1)
    {
        SHELL_Printf("GET BTID\n");
        SHELL_Printf("- Bluetooth ID : ");
        SHELL_Dump(UNIT_BTID, BLUETOOTH_ID_SIZE);
    }
    else
    {
        if (UNIT_FACTORY_TEST)
        {
            bool ret = false;
            uint8_t pBtID[6];

            SHELL_Printf("SET BTID\n");

            if ((argc == 2) && (HexString2Array(argv[1], pBtID, sizeof(pBtID)) == BLUETOOTH_ID_SIZE))
            {
                DeviceUserDateSetBluetoothID(pBtID);
                ret = true;
            }

            if (ret)
            {
                SHELL_Printf("- Bluetooth ID : ");
                SHELL_Dump(UNIT_BTID, BLUETOOTH_ID_SIZE);
                //normal reset(because advertise packet is modified)
                gecko_cmd_system_reset(0);
            }
            else
            {
                SHELL_Printf("- ERROR, Invalid Arguments\n");
            }
        }
    }

    return (SHELL_EXEC_ERR_NONE);

}

static CPU_INT16S AT_CMD_ChangeMode(CPU_INT16U        argc,
                                    CPU_CHAR         *argv[],
                                    SHELL_OUT_FNCT    out_fnct,
                                    SHELL_CMD_PARAM  *p_cmd_param)
{
    int op_mode = 0;

    SHELL_Printf("Change operation mode\n");
    op_mode = atoi(argv[1]);

    if (argc == 1)
    {
        return SHELL_EXEC_ERR;
    }
    else if (argc == 2)
    {
        if (op_mode == 0)
        {
            SHELL_Printf("==> NORMAL MODE\n");
            MainApp_ChangeOpMode(MAIN_OP_MODE_NORMAL);
        }
        else if (op_mode == 1)
        {
            SHELL_Printf("==> EMERGENCY MODE\n");
            MainApp_ChangeOpMode(MAIN_OP_MODE_SOS);
        }
        else
        {
            return SHELL_EXEC_ERR;
        }
    }
    else
    {
        return SHELL_EXEC_ERR;
    }

    return (SHELL_EXEC_ERR_NONE);
}

static CPU_INT16S AT_CMD_GetChList(CPU_INT16U        argc,
                                   CPU_CHAR         *argv[],
                                   SHELL_OUT_FNCT    out_fnct,
                                   SHELL_CMD_PARAM  *p_cmd_param)
{
    uint32_t i;
    ChannelParams_t channel;
    LORAMAC_GetChannel(LoRaMacTestGetChannel(), &channel);

    SHELL_Printf("- %22s   %5s %10s %4s\n", "Channel Informations", "Index", "Frequency", "Band");
    for (i = 0 ; i < 8 ; i++)
    {
        if (!LORAMAC_GetChannel(i, &channel))
        {
            break;
        }
        SHELL_Printf("  %22s   %5ld %3ld.%02ld MHz %4d\n", "", i, channel.Frequency / 1000000, (channel.Frequency % 1000000) / 10000, channel.Band);
    }

    return (SHELL_EXEC_ERR_NONE);
}


static void SHELL_Dump(const uint8_t *pData, uint32_t ulDataLen)
{
    for (int i = 0; i < ulDataLen; i++)
    {
        SHELL_Printf("%02X", pData[i]);
    }
}

static void SHELL_PrintSystemInfo(void)
{
    uint32_t i;
    ChannelParams_t channel;
    LORAMAC_GetChannel(LoRaMacTestGetChannel(), &channel);

    SHELL_Printf("[ System ]\n");
    SHELL_Printf("- %22s : %s\n", "Model", SILABS_AF_DEVICE_NAME);
    SHELL_Printf("- %22s : %s\n", "Auto Attach", (UNIT_AUTO_ATTACH ? "Enable" : "Disable"));
    SHELL_Printf("- %22s : %s\n", "Use OTTA", (UNIT_USE_OTAA ? "Enable" : "Disable"));
    SHELL_Printf("- %22s : %s\n", "Use SKT App", (UNIT_USE_SKT_APP ? "Enable" : "Disable"));
    SHELL_Printf("- %22s : %s\n", "Use Real App Key", (UNIT_USE_RAK ? "Enable" : "Disable"));
    SHELL_Printf("\n");
    SHELL_Printf("[ LoRaWAC ]\n");
    SHELL_Printf("- %22s : %4ld.%02ld MHz\n", "Current Channel", channel.Frequency / 1000000, (channel.Frequency % 1000000) / 10000);
    SHELL_Printf("- %22s : %d\n", "Channel Tx Power", LoRaMacTestGetChannelsTxPower());
    SHELL_Printf("- %22s : %d\n", "Max Duty Cycle", LoRaMacTestGetMaxDCycle());
    SHELL_Printf("- %22s : %d\n", "Aggregated Duty Cycle", LoRaMacTestGetAggreagtedDCycle());
    SHELL_Printf("- %22s : %d\n", "Current Rx1 DR Offset", LoRaMacTestGetRx1DrOffset());
    SHELL_Printf("- %22s : %d\n", "Rx2 DataRate", LoRaMacTestGetRx2Datarate());
    SHELL_Printf("- %22s : %ld\n", "Rx1 Delay", LORAMAC_GetRx1Delay());
    SHELL_Printf("- %22s : %ld\n", "Rx2 Delay", LORAMAC_GetRx2Delay());
    SHELL_Printf("- %22s : %ld\n", "Join Delay1", LORAMAC_GetJoinDelay1());
    SHELL_Printf("- %22s : %ld\n", "Join Delay2", LORAMAC_GetJoinDelay2());
    SHELL_Printf("- %22s : %d\n", "Retransmission Count", LORAMAC_GetRetries());
    SHELL_Printf("- %22s : %04x\n", "Channels Mask", LORAMAC_GetChannelsMask());

    SHELL_Printf("- %22s   %5s %10s %4s\n", "Channel Informations", "Index", "Frequency", "Band");
    for (i = 0 ; i < 16 ; i++)
    {
        if (!LORAMAC_GetChannel(i, &channel))
        {
            break;
        }
        SHELL_Printf("  %22s   %5ld %3ld.%02ld MHz %4d\n", "", i, channel.Frequency / 1000000, (channel.Frequency % 1000000) / 10000, channel.Band);
    }
}

static bool SHELL_GetBool(char *pArgv, bool bDefault)
{
    if (strcasecmp(pArgv, "enable") == 0)
    {
        return true;
    }
    else if (strcasecmp(pArgv, "disable") == 0)
    {
        return false;
    }
    else if (strcasecmp(pArgv, "1") == 0)
    {
        return true;
    }
    else if (strcasecmp(pArgv, "0") == 0)
    {
        return false;
    }
    else if (strcasecmp(pArgv, "true") == 0)
    {
        return true;
    }
    else if (strcasecmp(pArgv, "false") == 0)
    {
        return false;
    }
    else if (strcasecmp(pArgv, "on") == 0)
    {
        return true;
    }
    else if (strcasecmp(pArgv, "off") == 0)
    {
        return false;
    }

    return bDefault;
}


/***************************************************************************************************
*                                            Commands
***************************************************************************************************/
SHELL_CMD  pShellCommonCmds [] =
{
    {"AT+VER",       AT_CMD_FirmwareVer},            // Firmware Information - 17
    {"AT+DEVEUI",    AT_CMD_DevEUI},                 // Get Device EUI - 2
    {"AT+APPEUI",    AT_CMD_AppEUI},                 // Set/Get Application EUI
    {"AT+APPKEY",    AT_CMD_AppKey},                 // Set/Get Application Key - 3,4
    {0,              0}
};

SHELL_CMD pShellTestCmds[] =
{
    {"AT+RESET",     AT_CMD_Reset},                  // Reset - 1
    {"AT+USEND",     AT_CMD_USend},                  // Sending the user defined packet - 6
    {"AT+CSEND",     AT_CMD_CSend},                  // Sending the user defined packet - 6
    {"AT+RXS",       AT_CMD_RSSI_SNR},               // Get RSSI and SNR - 7
    {"AT+DR",        AT_CMD_SetTxDR},                // Set Tx Data Rate - 8, 9
    {"AT+TXPOWER",   AT_CMD_TxPower},                // Set Tx Power - 10
    {"AT+CHTX",      AT_CMD_SetChannelAndTxPower},   // Set Channel and Tx Power - 11
    {"AT+ADR",       AT_CMD_ADR},                    // Set/Get ADR Flag - 12
    {"AT+CRTNUM",    AT_CMD_TxRetransmissionNumber}, // Tx Retransmission Number - 15, 16
    {"AT+URTNUM",    AT_CMD_UnconfirmedRetransmissionNumber}, // Tx Retransmission Number(Unconfirmed)

    {"AT+RAPPKEY",   AT_CMD_RealAppKey},             // Get Real Application Key

#if 1
    {"AT+JOIN",      AT_CMD_Join},                   // Join

    {"AT+UTX",       AT_CMD_SendUnconfirmedMsg},
    {"AT+CTX",       AT_CMD_SendConfirmedMsg},
    {"AT+LCHK",      AT_CMD_LinkCheck},              // Link Check Request
    {"AT+DEVT",      AT_CMD_DeviceTimeRequest},      // Device Time Request
    {"AT+CFM",       AT_CMD_ChangeMsgType},
    {"AT+REP",       AT_CMD_ChangeTestMode},

    {"AT+GCFG",      AT_CMD_GetConfig},              // Get Configuration
    //    {"AT+SCFG",      AT_CMD_SetConfig},              // Set Configuration
    {"AT+MAC",       AT_CMD_Mac},                    // Get/Set MAC
    {"AT+STAT",      AT_CMD_Status},                 // Get Status

    {"AT+CH",        AT_CMD_Channel},                // Set/Get Channel

    {"AT+FCNT",      AT_CMD_FCNT},                   // changing the down link FCnt for testing
#endif
    {"AT+CHLIST",    AT_CMD_GetChList},             // CH Lists

    {"AT+TESTMODE",  AT_CMD_SetTestMode},            // Test Mode
    {"AT+TSEND",     AT_CMD_TestSend},               // Sending the user defined packet for test

    {"AT+LORAWAN",   AT_CMD_SetLoRaWANMode},         // Set LoRaWAN Mode Enable
    {"AT+FACTORY",   AT_CMD_SetFactoryMode},         // Set Factory Test Mode
    {"AT+SKT",       AT_CMD_SetSktMode},             // Set Skt Certification Test Mode
    {"AT+OTB",       AT_CMD_SetOtbMode},             // Set Skt OTB test mode
    {"AT+CERTI",     AT_CMD_SetCertiMode},           // Set Certification Test Mode

    {"AT+CLRRK",     AT_CMD_ClearRealKeyFlag},       // Clear realkey for using pseudo join

    {"AT+BAT",       AT_CMD_BATT},                   // Battery
    {"AT+BTID",      AT_CMD_BluetoothID},            // Bluetooth ID

    {"AT+TXCW",      AT_CMD_TxCW},                   // Tx Continuous
    {"AT+TRX",       AT_CMD_TestRx},                 // Rx Continuous
    {"AT+LORALDO",   AT_CMD_ControlLoRaLDO},         // control LoRa LDO

    {"AT+CM",        AT_CMD_ChangeMode},             // change operation mode

    {"AT+HWREV",     AT_CMD_SetHardwareRevision},    // Set Hardware Revision

    {0,              0}
};
