#ifndef LORA_H
#define LORA_H

#include "LoRaMac.h"
#include "Region.h"
#include "Commissioning.h"
#include "utilities.h"

/***************************************************************************************************
*                                             TYPEDEFS
***************************************************************************************************/
//! @def Macro to declare a packed structure or union
#ifndef __packed__
#define __packed__ __attribute__((packed))
#endif

/***************************************************************************************************
*                                             DEFINES
***************************************************************************************************/

#define LORA_OK                                              ((uint8_t) 0)
#define LORA_FAIL                                            ((uint8_t) 1)

#define LORA_CMD_ERR_NONE                                    ((int8_t)(0))
#define LORA_CMD_ERR_UNKNOWN_CMD                             ((int8_t)(-1))
#define LORA_CMD_ERR_NUM_PARAM                               ((int8_t)(-2))
#define LORA_CMD_ERR_INVALID_PARAM                           ((int8_t)(-3))
#define LORA_CMD_ERR_NETWORK                                 ((int8_t)(-4))
#define LORA_CMD_ERR_FLASH                                   ((int8_t)(-5))
#define LORA_CMD_ERR_NOT_AVAIABLE                            ((int8_t)(-6))

/** Default number of retries when confirmed message is sent out LoRaWAN network */
#define LORAWAN_RETRIES                             8
#define LORAMAC_TX_DATARATE_MAX                     5

#if USE_SKT_FORMAT
#define LORAMAC_REGION_DEFAULT                      LORAMAC_REGION_KR920
#else
#if defined(REGION_IN865)
#define LORAMAC_REGION_DEFAULT                      LORAMAC_REGION_IN865
#elif defined(REGION_US915)
#define LORAMAC_REGION_DEFAULT                      LORAMAC_REGION_US915
#elif defined(REGION_KR920)
#define LORAMAC_REGION_DEFAULT                      LORAMAC_REGION_KR920
#elif defined(REGION_AS923)
#define LORAMAC_REGION_DEFAULT                      LORAMAC_REGION_AS923
#elif defined(REGION_EU868)
#define LORAMAC_REGION_DEFAULT                      LORAMAC_REGION_EU868
#else
#error "Invalid LoRa Region"
#endif
#endif

#ifdef _DEBUG
#define LORAMAC_DEFAULT_RF_PERIOD                   5
#else
#define LORAMAC_DEFAULT_RF_PERIOD                   10
#endif

/*--- Fport --------------------------------------------------------------------------------------*/
/* Device management application message requirement (SKT requirement) */
#define SKT_DEVICE_SERVICE_PORT                     0xDE
/* Device/Network interworking application message requirements (SKT requirement) */
#define SKT_NETWORK_SERVICE_PORT                    0xDF
/* Service management application message requirement (Daliworks requirement) */
#define DALIWORKS_SERVICE_PORT                      0xDD
/* LoRa certification test message requirement */
#define LORA_CERTIFICATION_TEST_PORT                0xE0    /* 224 */
/* 3rd Party application message requirement */
#define LORA_APP_SERVER_PORT                        0x01    /* 0x01 - 0xDD */
/*--- SKT ----------------------------------------------------------------------------------------*/
#define SKT_MAX_PAYLOAD_SIZE                        65
/*--- Lineable -----------------------------------------------------------------------------------*/
#define CHANGE_OP_MODE_PACKET_LENGTH                (4)

typedef enum {
    LORA_JOIN_TYPE_OTTA = 0,
    LORA_JOIN_TYPE_ABP,
    LORA_JOIN_TYPE_PSEUDO,
    LORA_JOIN_TYPE_REAL,

    LORA_JOIN_TYPE_AUTO,

    LORA_JOIN_TYPE_NONE
} LoRaJoinType;

enum {
    LORAAPP_EVENT_RX_REQ = 0,
    LORAAPP_EVENT_RX_DATA,
    LORAAPP_EVENT_REPEAT,
    LORAAPP_EVENT_RESET,

    LORAAPP_EVENT_MAX
};

typedef struct {
    uint32_t id;
    uint32_t size;
    uint32_t addr;
} LoraMsg;

/***************************************************************************************************
*                                             MACROS
***************************************************************************************************/


/***************************************************************************************************
*                                             VARIABLES
***************************************************************************************************/

extern CPU_CHAR g_device_time[30];
extern uint8_t g_demod_margin;
extern uint8_t g_nb_gateways;

/***************************************************************************************************
*                                             FUNCTION PROTOTYPES
***************************************************************************************************/

uint8_t LORAWAN_Init(void);
uint8_t LORAWAN_JoinNetwork(LoRaJoinType join);
bool    LORAWAN_IsNetworkJoined(void);
uint8_t LORAWAN_SetMaxRetries(uint8_t retries);
uint8_t LORAWAN_SendDevTimeReq(void);
uint8_t LORAWAN_SendLinkCheckRequest(void);
uint8_t LORAWAN_GetMaxRetries(void);
uint8_t LORAWAN_GetSNR(void);
int16_t LORAWAN_GetRSSI(void);
bool    LORAWAN_GetNetID(uint32_t *net_id);
uint8_t LORAWAN_SendData(uint8_t  port,
                         bool     is_cfm,
                         uint8_t *data,
                         uint8_t  size,
                         bool     recv_wait,
                         bool     ack_type);
uint8_t LORAWAN_SendOnly(uint8_t  port,
                         bool     is_cfm,
                         uint8_t *data,
                         uint8_t  size);

#endif //LORA_H

