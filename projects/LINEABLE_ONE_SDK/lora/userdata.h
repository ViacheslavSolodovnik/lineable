/*
****************************************************************************************************
*                                               MODULE
****************************************************************************************************
*/
#ifndef USERDATA_H_
#define USERDATA_H_

#include "rtos_app.h"

/*
****************************************************************************************************
*                                                INCLUDE
****************************************************************************************************
*/


/*
****************************************************************************************************
*                                            PUBLIC DEFINES
****************************************************************************************************
*/

#define UD_SUCCESS      0x00
#define UD_ERROR        0x01

typedef enum {
    UID_LORA_MAGIC_CODE = 0,
    UID_DEV_EUI = 1,
    UID_APP_KEY = 3,
    UID_PSEUDO_APP_KEY = 7,
    UID_REAL_APP_KEY = 11,
    UID_PROVISIONING_INFO = 15,
    UID_RETX_CNT = 16
} UserData_ID;

#define USERPAGE                (USERDATA_BASE)
#define USERPAGEPTR             ((const USERDATA *)USERDATA_BASE)

SL_PACK_START(1)
typedef struct {
    uint32_t Region;            /* LoRaWAN Region ID */
    uint32_t NetID;             /* LoRaWAN Network ID */
    uint8_t  DevEui[8];         /* LoRaWAN Device Unique ID */
    uint8_t  AppEui[8];         /* LoRaWAN Application Unique ID */
    uint8_t  AppKey[16];        /* LoRaWAN Encryption Key */
    uint8_t  NwkSKey[16];       /* LoRaWAN Network Shared Encryption Key */
    uint8_t  AppSKey[16];       /* LoRaWAN Application Shared Encryption Key */
} SL_ATTRIBUTE_PACKED LORAWAN_INFO;
SL_PACK_END()

SL_PACK_START(1)
typedef struct {
    uint8_t RealAppKey[16];     /* LoRaWAN SKT Real Encryption Key */
} SL_ATTRIBUTE_PACKED SKT_INFO;
SL_PACK_END()

SL_PACK_START(1)
typedef struct {
    uint16_t     DataCRC;
    uint8_t      DeviceType;
    uint8_t      HardwareRevision;
    uint8_t      RunCyclicTask;
    uint32_t     DefaultRFPeriod;       /* Default RF periodic transmission (in sec.) */
    uint32_t     DeviceSerialNumber;    /* Device Serial Number */
    uint16_t     DeviceFlags;           /* Device permanent stored status flag */
    LORAWAN_INFO LoRaWAN;               /* LoRaWAN information */
    SKT_INFO     SKT;
    uint8_t      BluetoothID[6];        /* Bluetooth ID */
    uint16_t     TraceFlags;
} SL_ATTRIBUTE_PACKED USERDATA;
SL_PACK_END()

typedef union {
    uint8_t Flash[2048];
    USERDATA UserData;
} FLASH_PAGE;

extern const FLASH_PAGE _USERPAGE_;

/**
 * Make const USERDATA *volatile to force compiler to reload real data each time
 * and not optimize code assuming the value from the empty initialization
 */

#define USERDATAPTR ((volatile const USERDATA *)&_USERPAGE_.UserData)

//! @brief Macro replacement to access permanent data checksum
#define UNIT_CHECKSUM       ((const uint16_t)(USERDATAPTR)->DataCRC)          //! @hideinitializer
//! @brief Macro replacement to access permanent data Device Type
#define UNIT_DEVICETYPE     ((const uint8_t)(USERDATAPTR)->DeviceType)       //! @hideinitializer
//! @brief Macro replacement to access permanent data Device Type
#define UNIT_HWREV          ((const uint8_t)(USERDATAPTR)->HardwareRevision)       //! @hideinitializer
//! @brief Macro replacement to access permanent data Serial Number
#define UNIT_SERIALNUMBER   ((const uint32_t)(USERDATAPTR)->DeviceSerialNumber) //! @hideinitializer
//! @brief Macro replacement to access permanent data Run Cyclic Task
#define UNIT_RUN            ((const uint8_t)(USERDATAPTR)->RunCyclicTask)     //! @hideinitializer
//! @brief Macro replacement to access permanent data RF Period
#define UNIT_RFPERIOD       ((const uint32_t)(USERDATAPTR)->DefaultRFPeriod)   //! @hideinitializer
//! @brief Macro replacement to access permanent data LoRaWAN Region ID
#define UNIT_REGION         ((const uint32_t)(USERDATAPTR)->LoRaWAN.Region)    //! @hideinitializer
//! @brief Macro replacement to access permanent data LoRaWAN Network ID
#define UNIT_NETWORKID      ((const uint32_t)(USERDATAPTR)->LoRaWAN.NetID)     //! @hideinitializer
//! @brief Macro replacement to access permanent data Device Unit ID
#define UNIT_DEVEUID        ((const uint8_t*)(USERDATAPTR)->LoRaWAN.DevEui)   //! @hideinitializer
//! @brief Macro replacement to access permanent data Application Unit ID
#define UNIT_APPEUID        ((const uint8_t*)(USERDATAPTR)->LoRaWAN.AppEui)   //! @hideinitializer
//! @brief Macro replacement to access permanent data Application encryption key
#define UNIT_APPKEY         ((const uint8_t*)(USERDATAPTR)->LoRaWAN.AppKey)   //! @hideinitializer
//! @brief Macro replacement to access permanent data Network session encryption key
#define UNIT_NETSKEY        ((const uint8_t*)(USERDATAPTR)->LoRaWAN.NwkSKey)  //! @hideinitializer
//! @brief Macro replacement to access permanent data Application session encryption key
#define UNIT_APPSKEY        ((const uint8_t*)(USERDATAPTR)->LoRaWAN.AppSKey)  //! @hideinitializer
//! @brief Macro replacement to access permanent data Application real encryption key
#define UNIT_REALAPPKEY     ((const uint8_t*)(USERDATAPTR)->SKT.RealAppKey)   //! @hideinitializer
//! @brief Macro replacement to access permanent data Bluetooth ID
#define UNIT_BTID           ((const uint8_t*)(USERDATAPTR)->BluetoothID)      //! @hideinitializer

/**
 * Permanently stored status flags
 */
#define FLAG_INSTALLED          0x0001  //!< Device is installed @hideinitializer
#define FLAG_USE_OTAA           0x0002  //!< Device uses OTAA to join LoRaWAN network @hideinitializer
#define FLAG_DISALLOW_RESET     0x0004  //!< Device cannot be reset by user using magnet when set @hideinitializer
#define FLAG_USE_CTM            0x0010  //!< Device is use to cyclic transmission mode.
#define FLAG_AUTO_ATTACH        0x0020  //!< Device is attach to network automatically.

/*!
 * @brief Set this flag to use SKT/Daliworks LoRaWAN types of messages. If this flag is set
 * to zero, a default set of messages/commands will be used. this value is copied from the
 * User Data Flash Region upon device reset
 * @remark This will probably be the version used in the rest of the world
 */
#define FLAG_USE_SKT_APP        0x0100
#define FLAG_USE_RAK            0x0200  //!< Device is use to real app key.

/* Define each test mode */
#define FLAG_FACTORY_TEST       0x1000  //!< Device runs factory test
#define FLAG_SKT_CERTI_TEST     0x2000  //!< Device runs skt certification test
#define FLAG_SKT_OTB_TEST       0x4000  //!< Device runs skt OTB test
#define FLAG_CERTI_TEST         0x8000  //!< Device runs performance test

//! @brief Macro to permanently set a flag value in the User Information block
#define SET_USERFLAG(f)     DeviceUserDataSetFlag((f),(f))
//! @brief Macro to permanently clear a flag value in the User Information block
#define CLEAR_USERFLAG(f)   DeviceUserDataSetFlag((f),0)
//! @brief Macro to permanently set a flag value in the User Information block depending on the boolean parameter
#define UPDATE_USERFLAG(f,b)    DeviceUserDataSetFlag((f),(b) ? (f) : 0)


#define UNIT_INSTALLED       ((USERDATAPTR)->DeviceFlags & FLAG_INSTALLED)       //!< Device is installed @hideinitializer
#define UNIT_USE_OTAA        ((USERDATAPTR)->DeviceFlags & FLAG_USE_OTAA)        //!< Device uses OTAA to join LoRaWAN network @hideinitializer
#define UNIT_DISALLOW_RESET  ((USERDATAPTR)->DeviceFlags & FLAG_DISALLOW_RESET)  //!< Device cannot be reset by user @hideinitializer
#define UNIT_CTM_ON          ((USERDATAPTR)->DeviceFlags & FLAG_USE_CTM)         //!< Device is use to cyclic transmission mode. @hideinitializer
#define UNIT_USE_SKT_APP     ((USERDATAPTR)->DeviceFlags & FLAG_USE_SKT_APP)     //!< Device is using SKT/Daliworks LoRaWAN Application @hideinitializer
#define UNIT_USE_RAK         ((USERDATAPTR)->DeviceFlags & FLAG_USE_RAK)         //!< Device is use to real app key @hideinitializer
#define UNIT_AUTO_ATTACH     ((USERDATAPTR)->DeviceFlags & FLAG_AUTO_ATTACH)     //!< Device is attach to network automatically
#define UNIT_FACTORY_TEST    ((USERDATAPTR)->DeviceFlags & FLAG_FACTORY_TEST)    //!< Device is in factory test mode @hideinitializer
#define UNIT_SKT_CERTI_TEST  ((USERDATAPTR)->DeviceFlags & FLAG_SKT_CERTI_TEST)  //!< Device is in skt certification test mode @hideinitializer
#define UNIT_SKT_OTB_TEST    ((USERDATAPTR)->DeviceFlags & FLAG_SKT_OTB_TEST)    //!< Device is in skt certification test mode @hideinitializer
#define UNIT_CERTI_TEST      ((USERDATAPTR)->DeviceFlags & FLAG_CERTI_TEST)      //!< Device is in skt certification test mode @hideinitializer

#define FLAG_TRACE_ENABLE       0x0001
#define FLAG_TRACE_DUMP         0x0002
#define FLAG_TRACE_LORAMAC      0x0010
#define FLAG_TRACE_LORAWAN      0x0020
#define FLAG_TRACE_DALIWORKS    0x0040
#define FLAG_TRACE_SKT          0x0080
#define FLAG_TRACE_SUPERVISOR   0x0100

//! @brief Macro to permanently set a flag value in the User Information block
#define SET_TRACE_FLAG(f)       DeviceUserDataSetTraceFlag((f),(f))
//! @brief Macro to permanently clear a flag value in the User Information block
#define CLR_RACE_FLAG(f)        DeviceUserDataSetTraceFlag((f),0)
//! @brief Macro to permanently set a flag value in the User Information block depending on the boolean parameter
#define UPDATE_TRACE_FLAG(f,b)  DeviceUserDataSetTraceFlag((f),(b) ? (f) : 0)

#define UNIT_TRACE_ENABLE       ((USERDATAPTR)->TraceFlags & FLAG_TRACE_ENABLE) //!< Device is installed @hideinitializer
#define UNIT_TRACE_DUMP         ((USERDATAPTR)->TraceFlags & FLAG_TRACE_DUMP)
#define UNIT_TRACE_LORAMAC      ((USERDATAPTR)->TraceFlags & FLAG_TRACE_LORAMAC)
#define UNIT_TRACE_LORAWAN      ((USERDATAPTR)->TraceFlags & FLAG_TRACE_LORAWAN)
#define UNIT_TRACE_DALIWORKS    ((USERDATAPTR)->TraceFlags & FLAG_TRACE_DALIWORKS)
#define UNIT_TRACE_SKT          ((USERDATAPTR)->TraceFlags & FLAG_TRACE_SKT)
#define UNIT_TRACE(f)           ((f) && (((USERDATAPTR)->TraceFlags & (f)) != 0))

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION PROTOTYPES
****************************************************************************************************
*/

uint32_t user_page_update(UserData_ID ud_id, uint32_t *tempdata);

void DeviceUserDataCheck(void);
void DeviceUserDataSave(USERDATA* DataPtr);
void DeviceUserDataReset(void);
void DeviceUserDataHardwareRevision(uint8_t hwrev);
void DeviceUserDateSetSerialNumber(unsigned long serial);
void DeviceUserDataSetFlag(unsigned short Mask, unsigned short Value);
void DeviceUserDataSetRFPeriod(unsigned long Period);
void DeviceUserDateSetBluetoothID(uint8_t *BtID);

void DeviceUserDataSetAppKey(uint8_t *AppKey) ;
void DeviceUserDataSetAppEUI(uint8_t *AppEUI) ;
void DeviceUserDataSetDevEUI(uint8_t *DevEUI) ;
void DeviceUserDataSetSKTRealAppKey(uint8_t *RealAppKey) ;

void DeviceUserDataSetTraceFlag(unsigned short Mask, unsigned short Value);

#endif /** USERDATA_H_ */
