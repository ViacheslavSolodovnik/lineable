/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: LoRaMac classA device implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/

/*! \file classA/SensorNode/main.c */

/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include "lib_math.h"
#include "rtos_app.h"
#include "device.h"
#include "userdata.h"
#include "app_main.h"
#include "lora_skt.h"
#include "lora.h"
#include "lora_packet.h"
#include "timer.h"
#include "sys_os.h"

#include "loramac_ex.h"

#if defined(REGION_EU868)
#include "LoRaMacTest.h"
#endif

/***************************************************************************************************
*                                            DEBUG OPTION
***************************************************************************************************/
#undef  __MODULE__
#define __MODULE__  "LoRa WAN"
#undef  DBG_MODULE
#define DBG_MODULE 0
#define LORA_DEBUG 0

/***************************************************************************************************
*                                             TYPEDEFS
***************************************************************************************************/

/***************************************************************************************************
*                                            LOCAL DEFINES
***************************************************************************************************/

/** Default datarate */
#define LORAWAN_DEFAULT_DATARATE                    DR_0

/** LoRaWAN confirmed messages */
#define LORAWAN_CONFIRMED_MSG_ON                    false

/** LoRaWAN Adaptive Data Rate
 *  \remark Please note that when ADR is enabled the end-device should be static
 */
#define LORAWAN_ADR_ON                              1

#if defined(REGION_EU868)
/** LoRaWAN ETSI duty cycle control enable/disable
 *  \remark Please note that ETSI mandates duty cycled transmissions. Use only for test purposes
 */
#define LORAWAN_DUTYCYCLE_ON                        true

#define USE_SEMTECH_DEFAULT_CHANNEL_LINEUP          1

#if (USE_SEMTECH_DEFAULT_CHANNEL_LINEUP == 1)
#define LC4                { 867100000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC5                { 867300000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC6                { 867500000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC7                { 867700000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC8                { 867900000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC9                { 868800000, 0, { ( ( DR_7 << 4 ) | DR_7 ) }, 2 }
#define LC10               { 868300000, 0, { ( ( DR_6 << 4 ) | DR_6 ) }, 1 }
#endif
#endif

#define LORAWAN_DEFAULT_JOIN_RETRY_COUNT            3

/* Define lorawan event flag */
#define LORAWAN_EVENT_FLAG_MLME_CONFIRM             ((OS_FLAGS)(1 << 0))
#define LORAWAN_EVENT_FLAG_MCPS_INDICATION          ((OS_FLAGS)(1 << 1))
#define LORAWAN_EVENT_FLAG_SEND_ACK                 ((OS_FLAGS)(1 << 2))

#define LORAWAN_SEND_MSG_TIMEOUT    (OS_MS_2_TICKS(  9000))    /* LORAWAN_SendMessage Timeout value */
#define LORAWAN_JOIN_TIMEOUT        (OS_MS_2_TICKS( 10000))   /* LORAWAN JOIN Timeout value */
#define LORAWAN_RECV_RX_TIMEOUT     (OS_MS_2_TICKS( 10000))   /* LORAWAN Receive RX timeout value */
#define LORAWAN_MCPS_CFM_TIMEOUT    (OS_MS_2_TICKS(120000))

/** LORAWAN task message structure definition */
typedef struct
{
    uint8_t  Port;                       /* LoRaWAN destination port             */
    Mcps_t   Request;                    /* LoRaWAN packet type                  */
    uint8_t  NbTrials;                   /* LoRaWAN number of trials             */
    uint8_t  Size;                       /* LoRaWAN message size                 */
    uint8_t *Buffer;                     /* LoRaWAN message pointer              */

    LoRaMacEventInfoStatus_t Status;     /* LoRaWAN message result code          */
} LoRaWANPacket;

SL_PACK_START(1)
typedef struct
{
    uint8_t DL_PacketType;
    uint8_t DL_ProtocolVer;
    uint8_t DL_OperationMode;
    uint8_t DL_Reserved;
} DownLinkPayload;
SL_PACK_END()

SL_PACK_START(1)
typedef struct
{
    uint8_t Version;
    uint8_t MessageType;
    uint8_t PayloadLen;
    DownLinkPayload Payload;
} Skt_Message;
SL_PACK_END()

/***************************************************************************************************
*                                 GLOBAL VARIABLES
***************************************************************************************************/

/***************************************************************************************************
*                                       VARIABLES
***************************************************************************************************/

static OS_TCB       g_lorawan_event_task_tcb;
static CPU_STK      g_lorawan_event_task_stk[LORAWAN_EVENT_TASK_STK_SIZE];
static OS_FLAG_GRP  g_lorawan_event_flags;

static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;

static uint8_t LoRaWAN_Retries = LORAWAN_RETRIES;

static OS_MUTEX g_send_lock;
static OS_SEM g_mlme_cfm_done;
static OS_SEM g_mcps_cfm_done;
static OS_SEM g_mcps_ind_done;

static uint8_t  nSNR = 0;
static uint16_t nRSSI = 0;

static bool g_lorawan_joined;
static bool g_mcps_cfm_ok;
static bool g_mcps_cfm_ack;

static struct
{
    MlmeConfirm_t       mlme_cfm;
    McpsConfirm_t       mcps_cfm;
    McpsIndication_t    mcps_ind;
} g_lorawan_event_data;

static uint8_t g_lora_packet_buff[255] = {0};

static uint8_t g_lora_mcps_ind_buff[255] = {0};

static TimerTime_t lora_send_time = 0;

CPU_CHAR g_device_time[30];
uint8_t g_demod_margin;
uint8_t g_nb_gateways;

/***************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
***************************************************************************************************/
static void    McpsConfirm(McpsConfirm_t *mcpsConfirm);
static void    McpsIndication(McpsIndication_t *mcpsIndication);
static void    MlmeConfirm(MlmeConfirm_t *mlmeConfirm);
static void    LoRaMacInit(void);
static void    ExecMlmeConfirm(void);
static void    ExecMcpsIndication(void);
static uint8_t ExecJoinNetworkUseOTTA(uint8_t *dev_eui,
                                      uint8_t *app_eui,
                                      uint8_t *app_key,
                                      bool     wait);
static uint8_t ExecJoinNetworkUseABP(void);
static uint8_t ExecJoinNetworkUsePseudo(void);
static uint8_t ExecJoinNetworkUseReal(void);
static uint8_t ExecSendAck(void);
static uint8_t ExecSendPacket(LoRaWANPacket *packet);
static void    ExecShowErrorStatus(LoRaMacEventInfoStatus_t status);
static void    LoRaWANEventTask(void *p_arg);

static inline uint8_t LockSendFunc(void);
static inline uint8_t UnlockSendFunc(void);

/***************************************************************************************************
*                                           PUBLIC FUNCTION
***************************************************************************************************/
uint8_t LORAWAN_Init(void)
{
    RTOS_ERR err;
    uint8_t error = 0;

    /* Create mutex */
    OSMutexCreate((OS_MUTEX *)&g_send_lock,
                  (CPU_CHAR *) "LoRaWAN Send Lock",
                  (RTOS_ERR *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* Create semaphore */
    OSSemCreate((OS_SEM     *)&g_mlme_cfm_done,
                (CPU_CHAR   *) "LoRaWAN Mlme Confirm Wait",
                (OS_SEM_CTR  ) 0,
                (RTOS_ERR   *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
    OSSemCreate((OS_SEM     *)&g_mcps_cfm_done,
                (CPU_CHAR   *) "LoRaWAN Mcps Confirm Wait",
                (OS_SEM_CTR  ) 0,
                (RTOS_ERR   *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;
    OSSemCreate((OS_SEM     *)&g_mcps_ind_done,
                (CPU_CHAR   *) "LoRaWAN Mcps Indication Wait",
                (OS_SEM_CTR  ) 0,
                (RTOS_ERR   *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* Event flags */
    OSFlagCreate((OS_FLAG_GRP *)&g_lorawan_event_flags,
                 (CPU_CHAR    *) "LoRaWAN Event Flags",
                 (OS_FLAGS     ) 0,
                 (RTOS_ERR    *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* Event task */
    OSTaskCreate((OS_TCB       *)&g_lorawan_event_task_tcb,
                 (CPU_CHAR     *) "LoRaWAN Event Task",
                 (OS_TASK_PTR   ) LoRaWANEventTask,
                 (void         *) DEF_NULL,
                 (OS_PRIO       ) LORAWAN_EVENT_TASK_PRIO,
                 (CPU_STK      *)&g_lorawan_event_task_stk[0],
                 (CPU_STK_SIZE  ) (LORAWAN_EVENT_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  ) LORAWAN_EVENT_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) 0u,
                 (OS_TICK       ) 0u,
                 (void         *) DEF_NULL,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    error += (RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE) ? 0 : 1;

    /* Initialize LoRaMAC */
    LoRaMacInit();

    return error;
}

uint8_t LORAWAN_JoinNetwork(LoRaJoinType join)
{
    uint8_t err = LORA_FAIL;

    switch (join)
    {
        case LORA_JOIN_TYPE_OTTA:
            err = ExecJoinNetworkUseOTTA((uint8_t *)UNIT_DEVEUID,
                                         (uint8_t *)UNIT_APPEUID,
                                         (uint8_t *)UNIT_APPKEY,
                                         false);
            break;
        case LORA_JOIN_TYPE_ABP:
            err = ExecJoinNetworkUseABP();
            break;
        case LORA_JOIN_TYPE_PSEUDO:
            err = ExecJoinNetworkUsePseudo();
            break;
        case LORA_JOIN_TYPE_REAL:
            err = ExecJoinNetworkUseReal();
            break;

        case LORA_JOIN_TYPE_AUTO:
            if (UNIT_USE_SKT_APP)
            {
                if (UNIT_USE_RAK)
                {
                    err = ExecJoinNetworkUseReal();
                }
                else
                {
                    err = ExecJoinNetworkUsePseudo();
                }
            }
            else
            {
                APP_TRACE_LORA("Join-Request (%s)\n", (UNIT_USE_OTAA) ? "OTTA" : "ABP");
                if (UNIT_USE_OTAA)
                {
                    err = ExecJoinNetworkUseOTTA((uint8_t *)UNIT_DEVEUID,
                                                 (uint8_t *)UNIT_APPEUID,
                                                 (uint8_t *)UNIT_APPKEY,
                                                 false);
                }
                else
                {
                    err = ExecJoinNetworkUseABP();
                }
            }
            break;
        default:
            break;
    }

    return err;
}

bool LORAWAN_IsNetworkJoined(void)
{
    MibRequestConfirm_t mib_req;

    mib_req.Type = MIB_NETWORK_JOINED;
    mib_req.Param.IsNetworkJoined = false;
    LoRaMacMibGetRequestConfirm(&mib_req);

    return mib_req.Param.IsNetworkJoined;
}

uint8_t LORAWAN_SetMaxRetries(uint8_t retries)
{
    if (retries < 9)
    {
        LoRaWAN_Retries = (retries) ? retries : LORAWAN_RETRIES;
        return LORA_OK;
    }

    return LORA_FAIL;
}

uint8_t LORAWAN_GetMaxRetries(void)
{
    return LoRaWAN_Retries;
}

uint8_t LORAWAN_GetSNR(void)
{
    return nSNR;
}

int16_t LORAWAN_GetRSSI(void)
{
    return nRSSI;
}

bool LORAWAN_GetNetID(uint32_t *net_id)
{
    bool rc = false;
    MibRequestConfirm_t mibReq;
    LoRaMacStatus_t status;

    mibReq.Type = MIB_NET_ID;
    status = LoRaMacMibGetRequestConfirm(&mibReq);
    if (status == LORAMAC_STATUS_OK)
    {
        *net_id = mibReq.Param.NetID;
        rc = true;
    }

    return rc;
}

uint8_t LORAWAN_SendData(uint8_t  port,
                         bool     is_cfm,
                         uint8_t *data,
                         uint8_t  size,
                         bool     recv_wait,
                         bool     ack_type)
{
    RTOS_ERR err;
    uint8_t rc;
    LoRaWANPacket packet;
    RAND_NBR rand_delay = 0;

    /* Lock send func */
    LockSendFunc();

    OSSemSet((OS_SEM     *)&g_mcps_ind_done,
             (OS_SEM_CTR  ) 0,
             (RTOS_ERR   *)&err);


    if (ack_type == false)
    {
        if ((TimerGetElapsedTime(lora_send_time) < 10000))
        {
            OSTimeDly(OS_MS_2_TICKS(15000 - TimerGetElapsedTime(lora_send_time)), OS_OPT_TIME_DLY, &err);
        }

        Math_RandSetSeed(Math_RandSeed((RAND_NBR)lora_send_time));
        rand_delay = Math_Rand() % 10;
        rand_delay++;
        //APP_TRACE_LORA_M("\t Send Random Delay : start(%ld)\n", rand_delay);
        SYSOS_DelayMs(TIME_SEC(rand_delay));
        //APP_TRACE_LORA_M("\t Send Random Delay : done\n");
    }

    /* Make packet */
    packet.Port    = port;
    packet.Request = (is_cfm == true) ? MCPS_CONFIRMED : MCPS_UNCONFIRMED;
    packet.Size    = size;
    packet.Buffer  = &g_lora_packet_buff[0];
    if (size != 0)
    {
        Mem_Copy(packet.Buffer, data, size);
    }

    /* Send packet */
    rc = ExecSendPacket(&packet);
    if (rc == LORA_OK)
    {
        /* Check ack if needed */
        if (packet.Request == MCPS_CONFIRMED)
        {
            rc = (g_mcps_cfm_ack == true) ? LORA_OK : LORA_FAIL;
        }
    }
    else
    {
        ExecShowErrorStatus(packet.Status);
    }

    lora_send_time = TimerGetCurrentTime();

    /* Unlock send func */
    UnlockSendFunc();

    /* Wait receive done if needed */
    if ((rc == LORA_OK) && (recv_wait == true))
    {
        OSSemPend((OS_SEM   *)&g_mcps_ind_done,
                  (OS_TICK   ) LORAWAN_RECV_RX_TIMEOUT,
                  (OS_OPT    ) OS_OPT_PEND_BLOCKING,
                  (CPU_TS   *) DEF_NULL,
                  (RTOS_ERR *)&err);
    }

    return rc;
}

uint8_t LORAWAN_SendOnly(uint8_t  port,
                         bool     is_cfm,
                         uint8_t *data,
                         uint8_t  size)
{
    uint8_t rc;
    LoRaWANPacket packet;

    /* Lock send func */
    LockSendFunc();

    /* Make packet */
    packet.Port    = port;
    packet.Request = (is_cfm == true) ? MCPS_CONFIRMED : MCPS_UNCONFIRMED;
    packet.Size    = size;
    packet.Buffer  = &g_lora_packet_buff[0];
    if (size != 0) Mem_Copy(packet.Buffer, data, size);

    /* Send packet */
    rc = ExecSendPacket(&packet);
    if (rc == LORA_OK)
    {
        /* Check ack if needed */
        if (packet.Request == MCPS_CONFIRMED)
        {
            rc = (g_mcps_cfm_ack == true) ? LORA_OK : LORA_FAIL;
        }
    }
    else
    {
        ExecShowErrorStatus(packet.Status);
    }

     /* Unlock send func */
    UnlockSendFunc();

    return rc;
}

uint8_t LORAWAN_SendDevTimeReq(void)
{
    uint8_t err;
    MlmeReq_t mlmeReq;

    APP_TRACE_LORA("DeviceTimeReq\n");

    /* Add mac command */
    mlmeReq.Type = MLME_DEV_TIME;
    if (LoRaMacMlmeRequest(&mlmeReq) != LORAMAC_STATUS_OK)
    {
        APP_TRACE_DEBUG("LoRaMacMlmeRequest failed.\n");
        return LORA_FAIL;
    }

    /* Send immediately */
    err = LORAWAN_SendData(0, true, DEF_NULL, 0, false, true);

    return err;
}

uint8_t LORAWAN_SendLinkCheckRequest(void)
{
    uint8_t err;
    MlmeReq_t mlmeReq;

    APP_TRACE_LORA("LinkCheckReq\n");

    /* Add mac command */
    mlmeReq.Type = MLME_LINK_CHECK;
    if (LoRaMacMlmeRequest(&mlmeReq) != LORAMAC_STATUS_OK)
    {
        APP_TRACE_DEBUG("LoRaMacMlmeRequest failed.\n");
        return LORA_FAIL;
    }

    /* Send immediately */
    err = LORAWAN_SendData(0, true, DEF_NULL, 0, false, true);

    return err;
}

/***************************************************************************************************
*                                           LOCAL FUNCTION
***************************************************************************************************/

static inline uint8_t LockSendFunc(void)
{
    RTOS_ERR os_err;

    OSMutexPend((OS_MUTEX *)&g_send_lock,
                (OS_TICK   ) 0,
                (OS_OPT    ) OS_OPT_PEND_BLOCKING,
                (CPU_TS   *) DEF_NULL,
                (RTOS_ERR *)&os_err);

    return (RTOS_ERR_CODE_GET(os_err) == RTOS_ERR_NONE) ? LORA_OK : LORA_FAIL;
}

static inline uint8_t UnlockSendFunc(void)
{
    RTOS_ERR os_err;

    OSMutexPost((OS_MUTEX *)&g_send_lock,
                (OS_OPT    ) OS_OPT_POST_NONE,
                (RTOS_ERR *)&os_err);

    return (RTOS_ERR_CODE_GET(os_err) == RTOS_ERR_NONE) ? LORA_OK : LORA_FAIL;
}

/*!
 * \brief   MCPS-Confirm event function
 *
 * \param   [IN] mcpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void McpsConfirm(McpsConfirm_t *mcpsConfirm)
{
    RTOS_ERR err;

    Mem_Copy(&g_lorawan_event_data.mcps_cfm, mcpsConfirm, sizeof(McpsConfirm_t));

    /* Process event */
    if (mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK)
    {

        g_mcps_cfm_ok = true;

        switch (mcpsConfirm->McpsRequest)
        {
            case MCPS_UNCONFIRMED:
                break;
            case MCPS_CONFIRMED:
                g_mcps_cfm_ack = mcpsConfirm->AckReceived;
                break;
            default:
                break;
        }
    }

    /* Post mcps confirmed done */
    OSSemPost((OS_SEM   *)&g_mcps_cfm_done,
              (OS_OPT    ) OS_OPT_POST_NONE,
              (RTOS_ERR *)&err);
}

/*!
 * \brief   MCPS-Indication event function
 *
 * \param   [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes.
 */
static void McpsIndication(McpsIndication_t *mcpsIndication)
{
    RTOS_ERR err;

    /* Send ack if received mac command */
    if ((mcpsIndication->Status == LORAMAC_EVENT_INFO_STATUS_OK) &&
        (mcpsIndication->McpsIndication == MCPS_CONFIRMED))
    {
        OSFlagPost((OS_FLAG_GRP *)&g_lorawan_event_flags,
                   (OS_FLAGS     ) LORAWAN_EVENT_FLAG_SEND_ACK,
                   (OS_OPT       ) OS_OPT_POST_FLAG_SET,
                   (RTOS_ERR    *)&err);
    }

    if (mcpsIndication->BufferSize != 0)
    {
        Mem_Copy(&g_lorawan_event_data.mcps_ind, mcpsIndication, sizeof(McpsIndication_t));
        Mem_Copy(&g_lora_mcps_ind_buff[0], mcpsIndication->Buffer, mcpsIndication->BufferSize);
        g_lorawan_event_data.mcps_ind.Buffer = &g_lora_mcps_ind_buff[0];

        //APP_TRACE_LOG("%s: Port=0x%02X, bSize=%d\n", __func__, g_lorawan_event_data.mcps_ind.Port, g_lorawan_event_data.mcps_ind.BufferSize);
        //SYSOS_DumpData(g_lorawan_event_data.mcps_ind.Buffer, g_lorawan_event_data.mcps_ind.BufferSize, "MCPS IND");

        OSFlagPost((OS_FLAG_GRP *)&g_lorawan_event_flags,
                   (OS_FLAGS     ) LORAWAN_EVENT_FLAG_MCPS_INDICATION,
                   (OS_OPT       ) OS_OPT_POST_FLAG_SET,
                   (RTOS_ERR    *)&err);
    }
}

/*!
 * \brief   MLME-Confirm event function
 *
 * \param   [IN] mlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void MlmeConfirm(MlmeConfirm_t *mlmeConfirm)
{
    RTOS_ERR err;

    Mem_Copy(&g_lorawan_event_data.mlme_cfm, mlmeConfirm, sizeof(MlmeConfirm_t));

    OSFlagPost((OS_FLAG_GRP *)&g_lorawan_event_flags,
               (OS_FLAGS     ) LORAWAN_EVENT_FLAG_MLME_CONFIRM,
               (OS_OPT       ) OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
}

static void LoRaMacInit(void)
{
    APP_TRACE_DEBUG("LoRaMAC Init:\n");

    if (UNIT_REGION != LORAMAC_REGION_DEFAULT)
    {
        APP_TRACE_DEBUG("- %15s : mismatch region\n", "REGION");
        DeviceSystemReset(SYSTEM_RESET_WITH_USERDATA, 0);
    }

#if LORA_DEBUG
    switch (UNIT_REGION)
    {
        case LORAMAC_REGION_IN865:
            APP_TRACE_DEBUG("- %15s : %s\n", "Region", "IN865");
            break;
        case LORAMAC_REGION_US915:
            APP_TRACE_DEBUG("- %15s : %s\n", "Region", "US915");
            break;
        case LORAMAC_REGION_KR920:
            APP_TRACE_DEBUG("- %15s : %s\n", "Region", "KR920");
            break;
        case LORAMAC_REGION_EU868:
            APP_TRACE_DEBUG("- %15s : %s\n", "Region", "EU868");
            break;
        case LORAMAC_REGION_AS923:
            APP_TRACE_DEBUG("- %15s : %s\n", "Region", "AS923");
            break;
        default:
            APP_TRACE_DEBUG("- %15s : %s\n", "Region", "Invalid");
            break;
    }
    APP_TRACE_DEBUG("- %15s : %s\n", "Use OTTA", (UNIT_USE_OTAA ? "Enable" : "Disable"));
    if (UNIT_USE_SKT_APP)
    {
        APP_TRACE_DEBUG("- %15s : %s\n", "Use SKT App", "Enable");
    }
#endif
    APP_TRACE_DEBUG("- %15s : %s\n", "CLASS", "A");

    #if (!USE_SKT_FORMAT)
    DeviceUserDataSetFlag(FLAG_INSTALLED, FLAG_INSTALLED);
    #endif

    /** initialize LoRaMac configuration */
    LoRaMacPrimitives.MacMcpsConfirm    = McpsConfirm;
    LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
    LoRaMacPrimitives.MacMlmeConfirm    = MlmeConfirm;
    LoRaMacCallbacks.GetBatteryLevel    = 0;//BoardGetBatteryLevel;

    LoRaMacInitialization(&LoRaMacPrimitives, &LoRaMacCallbacks, UNIT_REGION);

    LORAMAC_SetADR(LORAWAN_ADR_ON);
    LORAMAC_SetPublicNetwork(LORAWAN_PUBLIC_NETWORK);
    LORAMAC_SetDutyCycle(PHY_DUTY_CYCLE);
    LORAMAC_SetClassType(CLASS_A);
}

static void ExecMlmeConfirm(void)
{
    RTOS_ERR err;
    MlmeConfirm_t *mlme_cfm = &g_lorawan_event_data.mlme_cfm;

    switch (mlme_cfm->MlmeRequest)
    {
        case MLME_JOIN:
            if (mlme_cfm->Status == LORAMAC_EVENT_INFO_STATUS_OK)
            {
                g_lorawan_joined = true;
                APP_TRACE_LORA("%-22s Success\n", "{Join-Accept}");
                MainApp_DoTimeSync();
            }
            else
            {
                g_lorawan_joined = false;
                APP_TRACE_LORA("%-22s Failed\n", "{Join-Accept}");
            }
            break;
        case MLME_LINK_CHECK:
            if (mlme_cfm->Status != LORAMAC_EVENT_INFO_STATUS_OK)
            {
                break;
            }
            APP_TRACE_LORA("%-22s Margin=%d, GwCnt=%d\n", "{LinkCheckAns}",
                           mlme_cfm->DemodMargin, mlme_cfm->NbGateways);
            break;
        case MLME_DEV_TIME:
        {
            CLK_DATE_TIME gps_origin;
            CLK_TS_SEC gps_origin_sec = 0;

            if (mlme_cfm->Status != LORAMAC_EVENT_INFO_STATUS_OK)
            {
                break;
            }

            APP_TRACE_LORA("%-22s Epoch=%ld, frac_sec=%d\n", "{DeviceTimeAns}",
                           mlme_cfm->Epoch, mlme_cfm->FracSec);

            /* 1980-01-06 Sun 00:00:00 */
            gps_origin.Yr = 1980;
            gps_origin.Month = 1;
            gps_origin.Day = 6;
            gps_origin.DayOfWk = 1;
            gps_origin.DayOfYr = 366;   /* 1980 is leap year */
            gps_origin.Hr = 0;
            gps_origin.Min = 0;
            gps_origin.Sec = 0;
            gps_origin.TZ_sec = 0;
            Clk_DateTimeToTS_Unix(&gps_origin_sec, &gps_origin);

            /* Adjust leap sencond : 2018 : 18 sec */
            MainApp_UpdateSysTime(mlme_cfm->Epoch + gps_origin_sec - 18);
            break;
        }
        default:
            break;
    }

    OSSemPost((OS_SEM   *)&g_mlme_cfm_done,
              (OS_OPT    ) OS_OPT_POST_NONE,
              (RTOS_ERR *)&err);
}

static void ExecMcpsIndication(void)
{
    RTOS_ERR err;
    McpsIndication_t *mcps_ind = &g_lorawan_event_data.mcps_ind;
    #if (!USE_SKT_FORMAT)
    DownLinkPayload *DL_payload = (DownLinkPayload *)mcps_ind->Buffer;
    #endif

    /* Check rx data */
    if ((mcps_ind->Status == LORAMAC_EVENT_INFO_STATUS_OK) &&
        (mcps_ind->RxData == true) &&
        (mcps_ind->BufferSize != 0))
    {
        switch (mcps_ind->Port)
        {
            case SKT_DEVICE_SERVICE_PORT:
                SKT_ParseDevMgmtData(mcps_ind->Buffer, mcps_ind->BufferSize);
                break;
            case SKT_NETWORK_SERVICE_PORT:
                SKT_ParseProvisioningData(mcps_ind->Buffer, mcps_ind->BufferSize);
                break;
            case DALIWORKS_SERVICE_PORT:
                break;
            case LORA_CERTIFICATION_TEST_PORT:
                break;
            case LORA_APP_SERVER_PORT:
                APP_TRACE_DEBUG("LORA_APP_SERVER_PORT downlink\n");

                #if (!USE_SKT_FORMAT)
                APP_TRACE_DEBUG("payload=%02X %02X %02X %02X\n", mcps_ind->Buffer[0], mcps_ind->Buffer[1],
                                mcps_ind->Buffer[2], mcps_ind->Buffer[3]);
                /** change period(emergency <-> normal) */
                if (mcps_ind->BufferSize == CHANGE_OP_MODE_PACKET_LENGTH)
                {
                    if (DL_payload->DL_OperationMode == DOWNLINK_OP_EMERGENCY_MODE)
                    {
                        MainApp_ChangeOpMode(MAIN_OP_MODE_SOS);
                        APP_TRACE_DEBUG("EMERGENCY MODE\n");
                    }
                    else if (DL_payload->DL_OperationMode == DOWNLINK_OP_NORMAL_MODE)
                    {
                        MainApp_ChangeOpMode(MAIN_OP_MODE_NORMAL);
                        APP_TRACE_DEBUG("NORMAL MODE\n");
                    }
                }
                #endif
                break;
            default:
                break;
        }
    }

    /* Post mcps indication done */
    OSSemPost((OS_SEM   *)&g_mcps_ind_done,
              (OS_OPT    ) OS_OPT_POST_NONE,
              (RTOS_ERR *)&err);
}

static uint8_t ExecJoinNetworkUseOTTA(uint8_t *dev_eui,
                                      uint8_t *app_eui,
                                      uint8_t *app_key,
                                      bool     wait)
{
    RTOS_ERR err;
    MlmeReq_t mlme_req;

    g_lorawan_joined = false;

    OSSemSet((OS_SEM     *)&g_mlme_cfm_done,
             (OS_SEM_CTR  ) 0,
             (RTOS_ERR   *)&err);

    /* Request JOIN */
    mlme_req.Type              = MLME_JOIN;
    mlme_req.Req.Join.DevEui   = dev_eui;
    mlme_req.Req.Join.AppEui   = app_eui;
    mlme_req.Req.Join.AppKey   = app_key;
    mlme_req.Req.Join.NbTrials = LORAWAN_DEFAULT_JOIN_RETRY_COUNT;

    LoRaMacMlmeRequest(&mlme_req);

    /* Wait join request done */
    OSSemPend((OS_SEM   *)&g_mlme_cfm_done,
              (OS_TICK   ) ((wait == true) ? 0 : LORAWAN_JOIN_TIMEOUT),
              (OS_OPT    ) OS_OPT_PEND_BLOCKING,
              (CPU_TS   *) DEF_NULL,
              (RTOS_ERR *)&err);

    return (g_lorawan_joined == true) ? LORA_OK : LORA_FAIL;
}

static uint8_t ExecJoinNetworkUseABP(void)
{
    MibRequestConfirm_t mib_req;

    APP_TRACE_DEBUG("Use ABP\n");

    /** Choose a random device address if not already defined in Commissioning.h */
    if (UNIT_SERIALNUMBER == 0)
    {
        srand1(0);                                              /* Random seed initialization */
        DeviceUserDateSetSerialNumber(randr(0, 0x01FFFFFF));    /* Choose a random device address */
    }

    mib_req.Type = MIB_NET_ID;
    mib_req.Param.NetID = UNIT_NETWORKID;
    LoRaMacMibSetRequestConfirm(&mib_req);

    mib_req.Type = MIB_DEV_ADDR;
    mib_req.Param.DevAddr = UNIT_SERIALNUMBER;
    LoRaMacMibSetRequestConfirm(&mib_req);

    mib_req.Type = MIB_NWK_SKEY;
    mib_req.Param.NwkSKey = (uint8_t *)UNIT_NETSKEY;
    LoRaMacMibSetRequestConfirm(&mib_req);

    mib_req.Type = MIB_APP_SKEY;
    mib_req.Param.AppSKey = (uint8_t *)UNIT_APPSKEY;
    LoRaMacMibSetRequestConfirm(&mib_req);

    mib_req.Type = MIB_NETWORK_JOINED;
    mib_req.Param.IsNetworkJoined = true;
    LoRaMacMibGetRequestConfirm(&mib_req);

    return LORA_OK;
}

static uint8_t ExecJoinNetworkUsePseudo(void)
{
    uint8_t err;

    /** Request to pseudo join */
    APP_TRACE_LORA("Join-Request (pseudo)\n");
    do
    {
        err = ExecJoinNetworkUseOTTA((uint8_t *)UNIT_DEVEUID,
                                     (uint8_t *)UNIT_APPEUID,
                                     (uint8_t *)UNIT_APPKEY,
                                     true);
        if (err == LORA_FAIL)
        {
            APP_TRACE_LORA("\n");
        }
    }
    while (err == LORA_FAIL);

    /** Request to real app key alloc */
    APP_TRACE_LORA("AppKeyAllocReq\n");
    do
    {
        err = SKT_RealAppKeyAllocReq();
    }
    while (err == LORA_FAIL);

    /** Request to real app key rx report */
    APP_TRACE_LORA("AppKeyReportReq\n");
    do
    {
        err = SKT_RealAppKeyRxReportReq();
    }
    while (err == LORA_FAIL);

    printf("PSEUDO_JOIN_DONE\n");
    DeviceUserDataSetFlag(FLAG_INSTALLED, FLAG_INSTALLED);

    /** Request to real join */
    APP_TRACE_LORA("Join-Request (real)\n");
    do
    {
        err = ExecJoinNetworkUseOTTA((uint8_t *)UNIT_DEVEUID,
                                     (uint8_t *)UNIT_APPEUID,
                                     (uint8_t *)UNIT_REALAPPKEY,
                                     true);
    }
    while (err == LORA_FAIL);

    return LORA_OK;
}

static uint8_t ExecJoinNetworkUseReal(void)
{
    uint8_t err;

    if (!UNIT_USE_RAK)
    {
        return LORA_FAIL;
    }

    APP_TRACE_LORA("Join-Request (real)\n");
    err = ExecJoinNetworkUseOTTA((uint8_t *)UNIT_DEVEUID,
                                 (uint8_t *)UNIT_APPEUID,
                                 (uint8_t *)UNIT_REALAPPKEY,
                                 false);

    return err;
}

static uint8_t ExecSendAck(void)
{
    uint8_t err;
    MibRequestConfirm_t mib_req;

    APP_TRACE_DEBUG("Send Ack\n");

    /** Set ack flag */
    mib_req.Type = MIB_ADD_ACK;
    if (LoRaMacMibSetRequestConfirm(&mib_req) != LORAMAC_STATUS_OK)
    {
        return LORA_FAIL;
    }

    /* Send immediately */
    err = LORAWAN_SendData(0, false, DEF_NULL, 0, false, true);

    return err;
}

static void ExecShowErrorStatus(LoRaMacEventInfoStatus_t status)
{
    switch (status)
    {
        case LORAMAC_EVENT_INFO_STATUS_ERROR:
            APP_TRACE_DEBUG("Error!\n");
            break;
        case LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT:
            APP_TRACE_DEBUG("Tx Timeout!\n");
            break;
        default:
            APP_TRACE_DEBUG("Show Error Status %d\n", status);
            break;
    }
}

static uint8_t ExecSendPacket(LoRaWANPacket *packet)
{
    RTOS_ERR err;
    McpsReq_t mcpsReq;
    LoRaMacTxInfo_t txInfo;
    uint8_t ret;
    int8_t current_dr;

    APP_TRACE_DEBUG("%s:\n", __func__);

    /* Initialize */
    packet->Status = LORAMAC_EVENT_INFO_STATUS_OK;
    g_mcps_cfm_ok  = false;
    g_mcps_cfm_ack = false;

    OSSemSet((OS_SEM     *)&g_mcps_cfm_done,
             (OS_SEM_CTR  ) 0,
             (RTOS_ERR   *)&err);

    /* Get current datarate */
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_CHANNELS_DATARATE;
    LoRaMacMibGetRequestConfirm(&mibReq);
    current_dr = mibReq.Param.ChannelsDatarate;

    if (LoRaMacQueryTxPossible(packet->Size, &txInfo) != LORAMAC_STATUS_OK)
    {
        packet->Status = LORAMAC_EVENT_INFO_STATUS_TX_DR_PAYLOAD_SIZE_ERROR;
        return LORA_FAIL;
    }

    // For Temp Solution for Canada Demo
#ifdef REGION_US915
    if (current_dr == DR_0)
    {
        mibReq.Type = MIB_CHANNELS_DATARATE;
        mibReq.Param.ChannelsDatarate = DR_1;
        LoRaMacMibSetRequestConfirm(&mibReq);
        current_dr = DR_1;
    }
#endif

    mcpsReq.Type = packet->Request;
    switch (mcpsReq.Type)
    {
        case MCPS_UNCONFIRMED:
            mcpsReq.Req.Unconfirmed.fPort       = packet->Port;
            mcpsReq.Req.Unconfirmed.fBuffer     = &packet->Buffer[0];
            mcpsReq.Req.Unconfirmed.fBufferSize = packet->Size;
            mcpsReq.Req.Unconfirmed.Datarate    = current_dr;
            break;
        case MCPS_CONFIRMED:
            mcpsReq.Req.Confirmed.fPort       = packet->Port;
            mcpsReq.Req.Confirmed.fBuffer     = &packet->Buffer[0];
            mcpsReq.Req.Confirmed.fBufferSize = packet->Size;
            mcpsReq.Req.Confirmed.Datarate    = current_dr;
            mcpsReq.Req.Confirmed.NbTrials    = LoRaWAN_Retries;
            break;
        default:
            packet->Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
            return LORA_FAIL;
    }


#if USE_SKT_OTB_TEST
    printf("\t\t  [UP] Request %s : Port=%02X, Size=%d\n",
           (mcpsReq.Type == MCPS_CONFIRMED) ? "Confirmed" : "Unconfirmed",
           packet->Port, packet->Size);
    if (packet->Size != 0)
        SYSOS_DumpData(&packet->Buffer[0], packet->Size, "Payload");
#else
    APP_TRACE_DEBUG("\t Request %s : Port=%02X, Size=%d\n",
                    (mcpsReq.Type == MCPS_CONFIRMED) ? "Confirmed" : "Unconfirmed",
                    packet->Port, packet->Size);
#endif

    /* Send data */
    if (LoRaMacMcpsRequest(&mcpsReq) == LORAMAC_STATUS_OK)
    {
        /* Wait Send done */
        OSSemPend((OS_SEM   *)&g_mcps_cfm_done,
                  (OS_TICK   ) LORAWAN_MCPS_CFM_TIMEOUT,
                  (OS_OPT    ) OS_OPT_PEND_BLOCKING,
                  (CPU_TS   *) DEF_NULL,
                  (RTOS_ERR *)&err);
        APP_TRACE_DEBUG("\t recv mcps cfm done\n");
    }
    else
    {
        APP_TRACE_DEBUG("\t LoRaMacMcpsRequest failed\n");
        packet->Status = LORAMAC_EVENT_INFO_STATUS_ERROR;
    }

    ret = (g_mcps_cfm_ok == true) ? LORA_OK : LORA_FAIL;

    APP_TRACE_DEBUG("\t Done (%d)\n", ret);

    return ret;
}

static void LoRaWANEventTask(void *p_arg)
{
    RTOS_ERR err;
    OS_FLAGS event;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON)
    {
        /* Wait event flag */
        event = OSFlagPend((OS_FLAG_GRP *)&g_lorawan_event_flags,
                           (OS_FLAGS     ) (LORAWAN_EVENT_FLAG_MLME_CONFIRM + LORAWAN_EVENT_FLAG_MCPS_INDICATION + LORAWAN_EVENT_FLAG_SEND_ACK),
                           (OS_TICK      ) 0,
                           (OS_OPT       ) (OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ANY + OS_OPT_PEND_FLAG_CONSUME),
                           (CPU_TS      *) DEF_NULL,
                           (RTOS_ERR    *)&err);

        if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
        {
            continue;
        }

        if (event & LORAWAN_EVENT_FLAG_MLME_CONFIRM)
        {
            ExecMlmeConfirm();
        }

        if (event & LORAWAN_EVENT_FLAG_SEND_ACK)
        {
            ExecSendAck();
        }

        if (event & LORAWAN_EVENT_FLAG_MCPS_INDICATION)
        {
            nRSSI = g_lorawan_event_data.mcps_ind.Rssi;
            nSNR  = g_lorawan_event_data.mcps_ind.Snr;

            /* Parsing received data */
            ExecMcpsIndication();
        }
    }
}

#if 0
static bool ExecCertification(McpsIndication_t *ind)
{
    bool rc = false;

    APP_TRACE_DEBUG("%s:\n", __func__);

    if (g_compliance_test.running == false)
    {
        /* Check compliance test enable command (i) */
        if (ind->BufferSize == 4)
        {
            uint32_t active_tm = *(uint32_t *)&ind->Buffer[0];

            if (active_tm == 0x01010101)
            {
                APP_TRACE_DEBUG("\t- ActiveTM\n");
                /* Initialize value */
                g_compliance_test.mcps_type = MCPS_UNCONFIRMED;
                g_compliance_test.app_port = LORA_CERTIFICATION_TEST_PORT;
                g_compliance_test.app_data_size = 2;
                LORAWAN_ResetDownLinkCounter();
                g_compliance_test.link_check = false;
                g_compliance_test.demod_margin = 0;
                g_compliance_test.nb_gateways = 0;
                g_compliance_test.running = true;
                g_compliance_test.state = 1;
                /* Enable ADR */
                LORAMAC_SetADR(true);
            }
        }
    }
    else
    {
        if (ind)
        {
            g_compliance_test.state = ind->Buffer[0];
            APP_TRACE_DEBUG("\t- Running - state:%d\n", g_compliance_test.state);
            switch (g_compliance_test.state)
            {
                case 0:                                 /* Check compliance test disable command (ii) */
                    g_compliance_test.running = false;
                    LORAMAC_SetADR(true);
                    break;
                case 1:                                 /* (iii, iv)                                  */
                    g_compliance_test.app_data_size = 2;
                    break;
                case 2:                                 /* Enable confirmed messages (v)              */
                    g_compliance_test.mcps_type = MCPS_CONFIRMED;
                    g_compliance_test.state = 1;
                    break;
                case 3:                                 /* Disable confirmed message (vi)             */
                    g_compliance_test.mcps_type = MCPS_UNCONFIRMED;
                    g_compliance_test.state = 1;
                    break;
                case 4:                                 /* (vii)                                      */
                    g_compliance_test.app_data_size = ind->BufferSize;

                    g_compliance_test.app_data_buffer[0] = 4;
                    {
                        uint8_t i;
                        for (i = 1; i < ind->BufferSize; i++)
                        {
                            g_compliance_test.app_data_buffer[i] = ind->Buffer[i] + 1;
                        }
                    }
                    break;
                case 5:                                 /* (viii)                                     */
                    LORAWAN_SendLinkCheckRequest();
                    break;
                case 6:                                 /* (ix)                                       */
                    /* Disable test mode and revert back to normal operation */
                    g_compliance_test.running = false;
                    LORAMAC_SetADR(true);
                    break;
                case 7:                                 /* (x)                                        */
                    if (ind->BufferSize == 3)
                    {
                        LORAMAC_TxCW((uint32_t)((ind->Buffer[1] << 8) | (ind->Buffer[2])));
                    }
                    else if (ind->BufferSize == 7)
                    {
                        LORAMAC_TxCW1(
                            (uint32_t)((ind->Buffer[1] << 8) | (ind->Buffer[2])),
                            (uint32_t)(((ind->Buffer[3] << 16) | (ind->Buffer[4] << 8) | (ind->Buffer[5])) * 100),
                            (uint32_t)(ind->Buffer[6]));
                    }
                    else
                    {
                        /* do nothing */
                    }
                    g_compliance_test.state = 1;
                    break;
            }
        }
    }

    return rc;
}
#endif

