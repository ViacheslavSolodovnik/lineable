#ifndef LORA_TEST_H
#define LORA_TEST_H

#include "os.h"
#include "bsphalconfig.h"
#include "rtos_app.h"

#if (configUSE_LORA_TEST > 0)
/* Exported typedef -----------------------------------------------------------*/



/* Exported define ------------------------------------------------------------*/



/* Exported macro -------------------------------------------------------------*/



/* Exported variables ---------------------------------------------------------*/



/* Function prototypes --------------------------------------------------------*/

void LoRaTestInit( void );

void LoRaTestDeInit( void );

int LoRaTx( int TxID, int ChId, int DR, int TxPower, int DelayTime );

int LoRaRx( int ChId, int DR );

int LoraCW( void );

int LoRaStop( void );

#endif // (configUSE_LORA_TEST > 0)
#endif // LORA_TEST_H

