/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdio.h>
#include "rtos_app.h"

#include "lora_packet.h"

/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/
SL_PACK_START(1)
typedef struct {
	uint8_t  device_type;
    uint8_t  packet_type;
    uint8_t  protocol_ver_type;
    uint8_t  seq_no;
    uint32_t time_value;
    uint8_t  status_flag_type;
    uint8_t  sos_type;
    uint8_t  bat_level;
    uint8_t  hrm_value;
    uint16_t step_count;
    uint8_t  loc_type;          /* 0:no location, 1:beacon, 2:GPS */
    union {
        struct {
            uint8_t id[6];
        } beacon;
        struct {
            uint8_t packed_long[3];
            uint8_t packed_lat[3];
        } gps;
    };
    uint8_t  reserved1;
} SL_ATTRIBUTE_PACKED LoraTxPacket;
SL_PACK_END()

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/
void LORA_MakePacket(DeviceType device_type,
                     LoraPacketType packet_type,
                     ProtocolVersionType protocol_ver_type,
                     StatusFlagType status_flag_type,
                     LoraSosType sos_type,
                     LocationType loc_type,
                     SensorData sensor_data,
                     uint8_t *buff)
{
    static uint8_t sequence = 0;
    LoraTxPacket *packet = (LoraTxPacket *)buff;

    packet->device_type = device_type;
    packet->packet_type = packet_type;
    packet->protocol_ver_type = protocol_ver_type;
    packet->seq_no = sequence++;
    packet->status_flag_type = status_flag_type;
    packet->sos_type = sos_type;

    packet->time_value = sensor_data.time_data.epoch_timestamp;
    packet->bat_level = sensor_data.batt_data.level;
    packet->hrm_value = sensor_data.hrm_data.heart_rate;
    packet->step_count = sensor_data.accel_data.steps;

    packet->loc_type = loc_type;       /* 0:no location, 1:beacon, 2:GPS */
    switch (packet->loc_type) {
    case 1:
        packet->beacon.id[0] = sensor_data.beacon_data.id[0];
        packet->beacon.id[1] = sensor_data.beacon_data.id[1];
        packet->beacon.id[2] = sensor_data.beacon_data.id[2];
        packet->beacon.id[3] = sensor_data.beacon_data.id[3];
        packet->beacon.id[4] = sensor_data.beacon_data.id[4];
        packet->beacon.id[5] = sensor_data.beacon_data.id[5];
        break;
    case 2:
        packet->gps.packed_long[0] = sensor_data.gps_data.longitude[0];
        packet->gps.packed_long[1] = sensor_data.gps_data.longitude[1];
        packet->gps.packed_long[2] = sensor_data.gps_data.longitude[2];
        packet->gps.packed_lat[0]  = sensor_data.gps_data.latitude[0];
        packet->gps.packed_lat[1]  = sensor_data.gps_data.latitude[1];
        packet->gps.packed_lat[2]  = sensor_data.gps_data.latitude[2];
        break;

    case 0:
    default:
        break;
    }
    packet->reserved1 = 0;
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

