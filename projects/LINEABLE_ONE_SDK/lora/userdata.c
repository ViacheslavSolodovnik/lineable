/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdio.h>
#include "rtos_app.h"

#include "flash.h"

#include "LoRaMac.h"
#include "Commissioning.h"
#include "lora.h"

#include "userdata.h"

/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/

#undef  __MODULE__
#define __MODULE__ "USERDATA"
#undef  DBG_MODULE
#define DBG_MODULE 0

/** for CRC16 */
#define CRC16_CCITT     0x1021  /* CCITT Standard CRC16 polynomial for X25/XMODEM/SD/HDLC/Bluetooth etc... */
#define CRC16_ANSI      0x8005  /* ANSI/IBM CRC16 polynomial for BiSync/MODBUS/USB etc... */
/** Macro to calculate standard CCITT CRC16 value */
#define CRC16_Calculate(b,c)  CRC16_CalculatePolynomial((b),(c),CRC16_CCITT)
/** Macro to calculate standard CCITT CRC16 on memory range */
#define CRC16_CalculateRange(b,l,c) CRC16_CalculateRangePolynomial((b),(l),(c),CRC16_CCITT)

#define BLUETOOTH_ID                             { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

/** Reserve a whole flash page inside the main Flash Memory */
const USERDATA DEFAULT_USERDATA = {
    .DataCRC = 0xFFFE,
    .DeviceType = DEVICETYPE_DEFAULT,
    .HardwareRevision = LINEABLE_DEFAULT_HARDWARE_REVSION,
    .DeviceSerialNumber = LORAWAN_DEVICE_ADDRESS,
#if (OVER_THE_AIR_ACTIVATION > 0)
#if (USE_SKT_FORMAT > 0)
    .DeviceFlags = FLAG_USE_SKT_APP | FLAG_USE_OTAA,
#else
    .DeviceFlags = FLAG_USE_OTAA,
#endif
#else
#if (USE_SKT_FORMAT > 0)
    .DeviceFlags = FLAG_USE_SKT_APP,
#else
    .DeviceFlags = 0,
#endif
#endif
    .DefaultRFPeriod = LORAMAC_DEFAULT_RF_PERIOD,
    .BluetoothID     = BLUETOOTH_ID,
    .LoRaWAN = {
        .Region  = LORAMAC_REGION_DEFAULT,
        .NetID   = LORAWAN_NETWORK_ID,
        .DevEui  = LORAWAN_DEVICE_EUI,
        .AppEui  = LORAWAN_APPLICATION_EUI,
        .AppKey  = LORAWAN_APPLICATION_KEY,
        .NwkSKey = LORAWAN_NWKSKEY,
        .AppSKey = LORAWAN_APPSKEY
    },
    .TraceFlags = FLAG_TRACE_ENABLE | FLAG_TRACE_LORAMAC | FLAG_TRACE_LORAWAN | FLAG_TRACE_DALIWORKS | FLAG_TRACE_SKT | FLAG_TRACE_SUPERVISOR,
};

const FLASH_PAGE __attribute__((aligned(2048)))
                 __attribute__((__used__))
                 __attribute__((section(".userdata")))
_USERPAGE_ = {
    .UserData = {
        .DataCRC = 0xFFFE,
        .DeviceType = DEVICETYPE_DEFAULT,
        .HardwareRevision = LINEABLE_DEFAULT_HARDWARE_REVSION,
        .DeviceSerialNumber = LORAWAN_DEVICE_ADDRESS,
#if (OVER_THE_AIR_ACTIVATION > 0)
#if (USE_SKT_FORMAT > 0)
        .DeviceFlags = FLAG_USE_SKT_APP | FLAG_USE_OTAA,
#else
        .DeviceFlags = FLAG_USE_OTAA,
#endif
#else
#if (USE_SKT_FORMAT > 0)
        .DeviceFlags = FLAG_USE_SKT_APP,
#else
        .DeviceFlags = 0,
#endif
#endif
        .DefaultRFPeriod = LORAMAC_DEFAULT_RF_PERIOD,
        .BluetoothID     = BLUETOOTH_ID,
        .LoRaWAN = {
            .Region  = LORAMAC_REGION_DEFAULT,
            .NetID   = LORAWAN_NETWORK_ID,
            .DevEui  = LORAWAN_DEVICE_EUI,
            .AppEui  = LORAWAN_APPLICATION_EUI,
            .AppKey  = LORAWAN_APPLICATION_KEY,
            .NwkSKey = LORAWAN_NWKSKEY,
            .AppSKey = LORAWAN_APPSKEY
        },
        .TraceFlags = FLAG_TRACE_ENABLE | FLAG_TRACE_LORAMAC | FLAG_TRACE_LORAWAN | FLAG_TRACE_DALIWORKS | FLAG_TRACE_SKT | FLAG_TRACE_SUPERVISOR,
    }
};

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

static bool checkBlank(uint8_t *p, int len);

static uint16_t CRC16_CalculatePolynomial(const uint8_t Byte, const uint16_t prevCrc, const uint16_t Polynomial);
static uint16_t CRC16_CalculateRangePolynomial(const uint8_t *Buffer, uint16_t length, uint16_t prevCrc, const uint16_t Polynomial);

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/

uint32_t user_page_update(UserData_ID ud_id, uint32_t *tempdata)
{
    PP_UNUSED_PARAM(ud_id);
    PP_UNUSED_PARAM(tempdata);

    return UD_SUCCESS;
}

void DeviceUserDataCheck(void)
{
    USERDATA UData;
    uint16_t m_crc_c, m_crc_r;
    uint16_t b_crc_c, b_crc_r;
    union {
        uint8_t value;
        struct {
            uint8_t both     : 1;
            uint8_t backup   : 1;
            uint8_t main     : 1;
            uint8_t reserved : 5;
        } state;
    } op_mode;

    /* MAIN */
    m_crc_c = CRC16_CalculateRange(((uint8_t *)USERDATAPTR) + sizeof(uint16_t), sizeof(USERDATA) - sizeof(uint16_t), 0xFFFF);
    m_crc_r = UNIT_CHECKSUM;
    /* BACKUP */
    b_crc_c = CRC16_CalculateRange(((uint8_t *)USERPAGE) + sizeof(uint16_t), sizeof(USERDATA) - sizeof(uint16_t), 0xFFFF);
    b_crc_r = (uint16_t)(((USERDATA *)USERPAGE)->DataCRC);

    op_mode.value = 0;
    op_mode.state.main   = (m_crc_c != m_crc_r) ? 1 : 0;
    op_mode.state.backup = (b_crc_c != b_crc_r) ? 1 : 0;
    op_mode.state.both   = (m_crc_c != b_crc_c) ? 1 : 0;

    #if 0
    /* forced update when firmware was updated */
    if (m_crc_r == 0xFFFE) {
        op_mode.state.main   = 1;
        op_mode.state.backup = 1;
        op_mode.state.both   = 0;
    }
    #endif

    /**
     *                                       1: broken, 0: ok
     *
     *      MAIN | BACKUP | COMPARE | VALUE |   OPERATION
     *      -------------------------------------------------
     *        1  |     1  |      0  |  0x6  |
     *        1  |     1  |      1  |  0x7  | DEFAULT -> MAIN
     *      -------------------------------------------------
     *        0  |     0  |      0  |  0x0  | do nothing
     *      -------------------------------------------------
     *        0  |     0  |      1  |  0x1  |
     *        0  |     1  |      0  |  0x2  |
     *        0  |     1  |      1  |  0x3  | MAIN -> BACKUP
     *      -------------------------------------------------
     *        1  |     0  |      0  |  0x4  |
     *        1  |     0  |      1  |  0x5  | BACKUP -> MAIN
     *      -------------------------------------------------
     */

    switch (op_mode.value) {
    case 0x6:
        APP_TRACE_DEBUG("%s : Forced erase BACKUP\n", __func__);
        FLASHEraseUserData();
    case 0x7:
        APP_TRACE_DEBUG("%s : DEFAULT --> MAIN\n", __func__);
        Mem_Copy((uint8_t *)&UData, (uint8_t *)&DEFAULT_USERDATA, sizeof(USERDATA));
        UData.DeviceFlags &= ~FLAG_INSTALLED;       /* Make sure device is not installed by default */
        UData.DataCRC = 0x0000;
        /* write data to MAIN (update CRC) */
        DeviceUserDataSave(&UData);
        break;
    case 0x1:
    case 0x2:
    case 0x3:
        APP_TRACE_DEBUG("%s : MAIN --> BACKUP\n", __func__);
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        /* write data to BACKUP */
        FLASHOpen();
        if (FLASHEraseUserData() == FLASH_NO_ERROR)
            FLASHWriteUserData(0, (uint8_t*)&UData, sizeof(USERDATA));
        FLASHClose();
        break;
    case 0x4:
    case 0x5:
        APP_TRACE_DEBUG("%s : BACKUP --> MAIN\n", __func__);
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERPAGE, sizeof(USERDATA));
        /* write data to MAIN */
        DeviceUserDataSave(&UData);
        break;
    case 0x0:
    default:
        /* do nothing */
        APP_TRACE_DEBUG("%s : Do nothing\n", __func__);
        break;
    }
}

void DeviceUserDataSave(USERDATA *DataPtr)
{
    if (!Mem_Cmp((void *)USERDATAPTR, DataPtr, sizeof(USERDATA))) {
        APP_TRACE_DEBUG("%s: Do\n", __func__);
        /* Make sure some correct LoRaWAN info is stored in Flash Memory */
        if (checkBlank(DataPtr->LoRaWAN.DevEui, sizeof(DataPtr->LoRaWAN.DevEui)))
            Mem_Copy(DataPtr->LoRaWAN.DevEui, (const uint8_t[])LORAWAN_DEVICE_EUI, sizeof(DataPtr->LoRaWAN.DevEui));

        if (checkBlank(DataPtr->LoRaWAN.AppEui, sizeof(DataPtr->LoRaWAN.AppEui)))
            Mem_Copy(DataPtr->LoRaWAN.AppEui, (const uint8_t[])LORAWAN_APPLICATION_EUI, sizeof(DataPtr->LoRaWAN.AppEui));

        if (checkBlank(DataPtr->LoRaWAN.AppKey, sizeof(DataPtr->LoRaWAN.AppKey)))
            Mem_Copy(DataPtr->LoRaWAN.AppKey, (const uint8_t[])LORAWAN_APPLICATION_KEY, sizeof(DataPtr->LoRaWAN.AppKey));

        if (checkBlank(DataPtr->LoRaWAN.AppSKey, sizeof(DataPtr->LoRaWAN.AppSKey)))
            Mem_Copy(DataPtr->LoRaWAN.AppSKey, (const uint8_t[])LORAWAN_APPSKEY, sizeof(DataPtr->LoRaWAN.AppSKey));

        if (checkBlank(DataPtr->LoRaWAN.NwkSKey, sizeof(DataPtr->LoRaWAN.NwkSKey)))
            Mem_Copy(DataPtr->LoRaWAN.NwkSKey, (const uint8_t[])LORAWAN_NWKSKEY, sizeof(DataPtr->LoRaWAN.NwkSKey));

        if (checkBlank(DataPtr->BluetoothID, sizeof(DataPtr->BluetoothID)))
            Mem_Copy(DataPtr->BluetoothID, (const uint8_t[])BLUETOOTH_ID, sizeof(DataPtr->BluetoothID));

        DataPtr->DataCRC = CRC16_CalculateRange(((uint8_t *)DataPtr) + sizeof(uint16_t), sizeof(USERDATA) - sizeof(uint16_t), 0xFFFF);

        FLASHOpen();
        if (FLASHEraseBlock((void *)USERDATAPTR) == FLASH_NO_ERROR)
            FLASHWrite((void *)USERDATAPTR, (uint8_t *)DataPtr, sizeof(USERDATA));
        FLASHClose();
    }
}

void DeviceUserDataReset(void)
{
    USERDATA UData;

    APP_TRACE_DEBUG("%s\n", __func__);

    /* clear MAIN CRC */
    Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
    UData.DataCRC = 0xFFFF;

    FLASHOpen();
    if (FLASHEraseBlock((void *)USERDATAPTR) == FLASH_NO_ERROR)
        FLASHWrite((void *)USERDATAPTR, (uint8_t *)&UData, sizeof(USERDATA));
    FLASHEraseUserData();
    FLASHClose();
}

void DeviceUserDataHardwareRevision(uint8_t hwrev)
{
    if (USERDATAPTR->HardwareRevision != hwrev) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        UData.HardwareRevision = hwrev;
        DeviceUserDataSave(&UData);
    }
}

void DeviceUserDateSetSerialNumber(uint32_t serial)
{
    if (USERDATAPTR->DeviceSerialNumber != serial) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        UData.DeviceSerialNumber = serial;
        DeviceUserDataSave(&UData);

        #if 0
        // Reset sensitive data
        Mem_Set(&UData.LoRaWAN, 0xFF, sizeof(LORAWAN_INFO));
        UData.DeviceFlags &= ~FLAG_INSTALLED;
        FLASHOpen();
        if (FLASHEraseUserData() == FLASH_NO_ERROR)
            FLASHWriteUserData(0, (uint8_t *)&UData, sizeof(USERDATA));
        FLASHClose();
        #endif
    }
}

void DeviceUserDataSetFlag(uint16_t Mask, uint16_t Value)
{
    if ((USERDATAPTR->DeviceFlags & Mask) != Value) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        UData.DeviceFlags &= ~Mask;
        UData.DeviceFlags |= Value;
        DeviceUserDataSave(&UData);
    }
}

void DeviceUserDataSetRFPeriod(uint32_t Period)
{
    if ((USERDATAPTR->DefaultRFPeriod) != Period) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        UData.DefaultRFPeriod = Period;
        DeviceUserDataSave(&UData);
    }
}

void DeviceUserDateSetBluetoothID(uint8_t *BtID)
{
    if (!Mem_Cmp(BtID, (void *)(USERDATAPTR->BluetoothID), 6)) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        Mem_Copy(UData.BluetoothID, BtID, 6);
        DeviceUserDataSave(&UData);
    }
}

void DeviceUserDataSetDevEUI(uint8_t *DevEUI)
{
    if (!Mem_Cmp(DevEUI, (void *)(USERDATAPTR->LoRaWAN.DevEui), 8)) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        Mem_Copy(UData.LoRaWAN.DevEui, DevEUI, 8);
        DeviceUserDataSave(&UData);

        #if 0
        /* Reset sensitive data */
        UData.DeviceFlags &= ~FLAG_INSTALLED;
        FLASHOpen();
        if (FLASHEraseUserData() == FLASH_NO_ERROR)
            FLASHWriteUserData(0, (uint8_t *)&UData, sizeof(USERDATA));
        FLASHClose();
        #endif
    }
}

void DeviceUserDataSetAppEUI(uint8_t *AppEUI)
{
    if (!Mem_Cmp(AppEUI, (void *)(USERDATAPTR->LoRaWAN.AppEui), 8)) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        Mem_Copy(UData.LoRaWAN.AppEui, AppEUI, 8);
        DeviceUserDataSave(&UData);

        #if 0
        /* Reset sensitive data */
        UData.DeviceFlags &= ~FLAG_INSTALLED;
        FLASHOpen();
        if (FLASHEraseUserData() == FLASH_NO_ERROR)
            FLASHWriteUserData(0, (uint8_t *)&UData, sizeof(USERDATA));
        FLASHClose();
        #endif
    }
}

void DeviceUserDataSetAppKey(uint8_t *AppKey)
{
    if (!Mem_Cmp(AppKey, (void *)(USERDATAPTR->LoRaWAN.AppKey), 16)) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        Mem_Copy(UData.LoRaWAN.AppKey, AppKey, 16);
        DeviceUserDataSave(&UData);

        #if 0
        /* Reset sensitive data */
        UData.DeviceFlags &= ~FLAG_INSTALLED;
        FLASHOpen();
        if (FLASHEraseUserData() == FLASH_NO_ERROR)
            FLASHWriteUserData(0, (uint8_t *)&UData, sizeof(USERDATA));
        FLASHClose();
        #endif
    }
}

void DeviceUserDataSetTraceFlag(uint16_t Mask, uint16_t Value)
{
    if ((USERDATAPTR->TraceFlags & Mask) != Value) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        UData.TraceFlags &= ~Mask;
        UData.TraceFlags |= Value;
        DeviceUserDataSave(&UData);
    }
}

void DeviceUserDataSetSKTRealAppKey(uint8_t *RealAppKey)
{
    if (!Mem_Cmp(RealAppKey, (void *)(USERDATAPTR->SKT.RealAppKey), 16)) {
        USERDATA UData;
        Mem_Copy((uint8_t *)&UData, (uint8_t *)USERDATAPTR, sizeof(USERDATA));
        Mem_Copy(UData.SKT.RealAppKey, RealAppKey, 16);
        DeviceUserDataSave(&UData);

        #if 0
        /* Reset sensitive data */
        UData.DeviceFlags &= ~FLAG_INSTALLED;
        FLASHOpen();
        if (FLASHEraseUserData() == FLASH_NO_ERROR)
            FLASHWriteUserData(0, (uint8_t *)&UData, sizeof(USERDATA));
        FLASHClose();
        #endif
    }
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

static bool checkBlank(uint8_t *p, int len)
{
    bool rc = true;
    while (rc && len--) {
        if (*p && (*p != 0xFF)) rc = false;
        p++;
    }
    return rc;
}

static uint16_t CRC16_CalculatePolynomial(const uint8_t Byte, const uint16_t prevCrc, const uint16_t Polynom)
{
    uint16_t CRC = prevCrc ^ (((uint16_t)Byte) << 8);
    for (uint8_t i = 8; i; i--) {
        if (CRC & 0x8000)
            CRC = (CRC << 1) ^ Polynom;
        else
            CRC = CRC << 1;
    }
    return ((uint16_t)(CRC & 0xFFFF));
}

static uint16_t CRC16_CalculateRangePolynomial(const uint8_t *Buffer, uint16_t length, uint16_t prevCrc, const uint16_t Polynom)
{
    while (length--)
        prevCrc = CRC16_CalculatePolynomial(*Buffer++, prevCrc, Polynom);
    return prevCrc;
}
