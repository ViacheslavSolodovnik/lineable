/*
****************************************************************************************************
*                                               MODULE
****************************************************************************************************
*/

#ifndef LORA_PACKET_H_
#define LORA_PACKET_H_

/*
****************************************************************************************************
*                                                INCLUDE
****************************************************************************************************
*/

#include <stdint.h>

/*
****************************************************************************************************
*                                            PUBLIC DEFINES
****************************************************************************************************
*/
#define LORA_PACKET_LENGTH          22
#define LORA_PACKET_MAX_LENGTH      255

#define LORA_PACKET_CERTI_LENGTH    10

typedef enum {
    DEVICE_TYPE_LINEABLE_ONE = 0,
}DeviceType;

typedef enum {
    PACKET_TYPE_DEV_INFO    = 0x00,
    PACKET_TYPE_SCHEDULE    = 0x01,
} LoraPacketType;

typedef enum {
    PROTOCOL_VERSION_TYPE_0 = 0,
}ProtocolVersionType;

typedef enum {
    STATUS_FLAG_TYPE_WORN   = 0,
    STATUS_FLAG_TYPE_REMOVE = 1,
} StatusFlagType;

typedef enum {
    ALERT_TYPE_SCHEDULE     = 0x00,
    ALERT_TYPE_SOS_BTN      = 0x01,
    ALERT_TYPE_IMMOBILITY   = 0x02,
    ALERT_TYPE_FALL         = 0x03,
    ALERT_TYPE_HRM_RISK     = 0x04,
    ALERT_TYPE_HRM_SEVERE   = 0x05,
    ALERT_TYPE_SKIN_TEMP    = 0x06,
    ALERT_TYPE_HAZARD_AREA  = 0x07,
    ALERT_TYPE_DEV_INFO     = 0x08      /* replaced period time (minute) */
} LoraAlertType;

typedef enum {
    SOS_TYPE_NO_SOS        = 0x00,
    SOS_TYPE_PROTECTOR_REQ = 0x01,
    SOS_TYPE_SWITCH_PRESS  = 0x02,
    SOS_TYPE_IMMOBILITY    = 0x03,
    SOS_TYPE_FALL          = 0x04,
    SOS_TYPE_HRM_RISK      = 0x05,
    SOS_TYPE_HRM_SEVERE    = 0x06,
    SOS_TYPE_SKIN_TEMP     = 0x07,
    SOS_TYPE_HAZARD_AREA   = 0x08,
} LoraSosType;

typedef enum {
    LOCATION_TYPE_NONE      = 0,
    LOCATION_TYPE_BEACON    = 1,
    LOCATION_TYPE_GPS       = 2,
} LocationType;

/** Downlink */
typedef enum {
	DOWNLINK_PACKET_TYPE_TIME           = 0x00,
	DOWNLINK_PACKET_TYPE_CHANGE_OP_MODE = 0x01,
}LoraDownlinkPacketType;

typedef enum {
	DOWNLINK_OP_NORMAL_MODE    = 0x00,
	DOWNLINK_OP_EMERGENCY_MODE = 0x01,
}LoraDownlinkOperationModeType;

/** Battery level data structure */
typedef struct {
    uint8_t level;
} FuelgaugeData;

/** HRM sensor data structure */
typedef struct {
    int16_t heart_rate;
} HrmData;

/** Accelerometer sensor data structure */
typedef struct {
    uint16_t steps;
} AccelData;

/** Beacon sensor data structure */
typedef struct {
    uint8_t id[6];
} BeaconData;

/** GPS sensor data structure */
typedef struct {
    uint8_t longitude[3];
    uint8_t latitude[3];
} GpsData;

/** time sensor data structure */
typedef struct {
    uint32_t epoch_timestamp;
} TimeData;

typedef struct {
    TimeData        time_data;
    FuelgaugeData   batt_data;
    HrmData         hrm_data;
    AccelData       accel_data;
    BeaconData      beacon_data;
    GpsData         gps_data;
} SensorData;

/*
****************************************************************************************************
*                                      PUBLIC FUNCTION PROTOTYPES
****************************************************************************************************
*/
void LORA_MakePacket(DeviceType device_type,
                     LoraPacketType packet_type,
                     ProtocolVersionType protocol_ver_type,
                     StatusFlagType status_flag_type,
                     LoraSosType sos_type,
                     LocationType loc_type,
                     SensorData sensor_data,
                     uint8_t *buff);

#endif /* LORA_PACKET_H_ */
