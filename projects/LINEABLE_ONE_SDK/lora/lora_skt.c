/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rtos_app.h"
#include "userdata.h"
#include "sys_timer.h"
#include "device.h"
#include "clk.h"

#include "LoRaMacCrypto.h"
#include "lora_packet.h"
#include "lora.h"
#include "loramac_ex.h"
#include "app_main.h"

#include "lora_skt.h"


/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/

#undef  __MODULE__
#define __MODULE__ "LoRa APP"
#undef  DBG_MODULE
#define DBG_MODULE      0
#define LORA_APP_DEBUG  0

/*--- Message Type -------------------------------------------------------------------------------*/
/* for Provisioning Application */
#define MSG_SKT_NET_REAL_APP_KEY_ALLOC_REQ      0x00
#define MSG_SKT_NET_REAL_APP_KEY_ALLOC_ANS      0x01
#define MSG_SKT_NET_REAL_APP_KEY_RX_REPORT_REQ  0x02
#define MSG_SKT_NET_REAL_APP_KEY_RX_REPORT_ANS  0x03
#define MSG_SKT_NET_CONFIRMED_UP_NB_RETRANS     0x04
/* for ThingPlug Application */
#define MSG_SKT_DEV_EXT_DEVICE_MANAGEMENT       0x00
#define MSG_SKT_DEV_RESET                       0x80
#define MSG_SKT_DEV_SET_UPLINK_DATA_INTERVAL    0x81
#define MSG_SKT_DEV_UPLINK_DATA_REQ             0x82

/** Current LoRaWAN Message version to use */
#define LORA_SKT_MESSAGE_VERSION                0

/** SKT/Daliworks LoRaWAN buffer abstraction */
typedef struct {
    uint8_t Version;
    uint8_t MessageType;
    uint8_t PayloadLen;
    uint8_t Payload[1];
} SktMessage;

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

static uint8_t ExecSktMakePacket(SktMessage *pMessage,
                                 uint8_t     nType,
                                 uint8_t    *pPayload,
                                 uint8_t     nPayloadLen);

/*
****************************************************************************************************
*                                       GLOBAL VARIABLES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

static uint8_t g_skt_app_nonce[3];
static uint8_t g_skt_recv_cmd = 0;

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/

uint8_t SKT_RealAppKeyAllocReq(void)
{
    uint8_t rc;
    uint8_t size;
    SktMessage data;

    g_skt_recv_cmd = 0;

    /* Make packet */
    size = ExecSktMakePacket(&data, MSG_SKT_NET_REAL_APP_KEY_ALLOC_REQ, DEF_NULL, 0);

    /* Send real app key alloc request */
    rc = LORAWAN_SendData(SKT_NETWORK_SERVICE_PORT, true, (uint8_t *)&data, size, true, false);

    if (rc == LORA_OK) {
        rc = (g_skt_recv_cmd == MSG_SKT_NET_REAL_APP_KEY_ALLOC_ANS) ? LORA_OK : LORA_FAIL;
    }

    return rc;
}

uint8_t SKT_RealAppKeyRxReportReq(void)
{
    uint8_t rc;
    uint8_t size;
    SktMessage data;

    g_skt_recv_cmd = 0;

    /* Make packet */
    size = ExecSktMakePacket(&data, MSG_SKT_NET_REAL_APP_KEY_RX_REPORT_REQ, DEF_NULL, 0);

    /* Send real app key report request */
    rc = LORAWAN_SendData(SKT_NETWORK_SERVICE_PORT, true, (uint8_t *)&data, size, true, false);

    if (rc == LORA_OK) {
        rc = (g_skt_recv_cmd == MSG_SKT_NET_REAL_APP_KEY_RX_REPORT_ANS) ? LORA_OK : LORA_FAIL;
    }

    return rc;
}

bool SKT_ParseDevMgmtData(uint8_t *data, uint8_t size)
{
    bool rc = false;
    SktMessage *payload = (SktMessage *)data;

    if (payload->Version > LORA_SKT_MESSAGE_VERSION) {
        return rc;    // Ignore malformed message
    }

    APP_TRACE_DEBUG("Exec STK : %02x\n", payload->MessageType);

    switch (payload->MessageType) {
    case MSG_SKT_DEV_RESET:
        /* WARNING: Upon factory reset the unit will not communicated with the LoRaWAN anymore
         * until it is re-installed using the magnet.
         * If a factory reset is requested, uncomment next line
         *      DeviceUserDataSetFlag(FLAG_INSTALLED, 0); */

        APP_TRACE_LORA("%-22s Ver=%d, MsgType=%02X\n",
                        "{DevReset}", payload->Version, payload->MessageType);
        DeviceSystemReset(SYSTEM_RESET_NORMAL, 5000);   /* need 5sec */
        /** This code will never return */
        break;

    case MSG_SKT_DEV_EXT_DEVICE_MANAGEMENT:
        APP_TRACE_LORA("%-22s Ver=%d, MsgType=%02X, PayloadLen=%d, payload=%02X %02X %02X %02X\n",
                        "{extDevMgmt}", payload->Version, payload->MessageType, payload->PayloadLen,
                                        payload->Payload[0], payload->Payload[1], payload->Payload[2], payload->Payload[3]);

        /** change period(emergency <-> normal) */
        #if (USE_SKT_OTB_TEST == 0)
        if (payload->PayloadLen == CHANGE_OP_MODE_PACKET_LENGTH)
        {
            if (payload->Payload[0] == 0x01)
            {
                if (payload->Payload[2] == 0x00)
                {
                    APP_TRACE_LOG("OP MODE : NORMAL\n");
                    MainApp_ChangeOpMode(MAIN_OP_MODE_NORMAL);
                }
                else if (payload->Payload[2] == 0x01)
                {
                    APP_TRACE_LOG("OP MODE : SOS\n");
                    MainApp_ChangeOpMode(MAIN_OP_MODE_SOS);
                }
                else
                {
                    APP_TRACE_LOG("OP MODE : Invalid\n");
                }
            }
        }
        #endif
        break;
    case MSG_SKT_DEV_SET_UPLINK_DATA_INTERVAL:
        APP_TRACE_LORA("%-22s Ver=%d, MsgType=%02X, PayloadLen=%d\n",
                        "{RepPerChange}", payload->Version, payload->MessageType, payload->PayloadLen);
        break;
    case MSG_SKT_DEV_UPLINK_DATA_REQ:
        APP_TRACE_LORA("%-22s Ver=%d, MsgType=%02X, PayloadLen=%d\n",
                        "{RepImmediate}", payload->Version, payload->MessageType, payload->PayloadLen);
        break;
    default:
        break;
    }

    return rc;
}

bool SKT_ParseProvisioningData(uint8_t *data, uint8_t size)
{
    bool rc = false;
    SktMessage *payload = (SktMessage *)data;

    if (payload->Version > LORA_SKT_MESSAGE_VERSION) {
        return rc;    // Ignore malformed message
    }

    switch (payload->MessageType) {
    case MSG_SKT_NET_REAL_APP_KEY_ALLOC_ANS:
        if (payload->PayloadLen == sizeof(g_skt_app_nonce)) {
            memcpy(g_skt_app_nonce, payload->Payload, 3);
            APP_TRACE_LORA("%-22s Nonce=%02X%02X%02X\n", "{AppKeyAllocAns}",
                g_skt_app_nonce[2], g_skt_app_nonce[1], g_skt_app_nonce[0]);
        }
        break;

    case MSG_SKT_NET_REAL_APP_KEY_RX_REPORT_ANS: {
        uint8_t RealAppKey[16] = {0};
        uint32_t net_id = 0;

        LORAWAN_GetNetID(&net_id);
        LoRaMacJoinComputeRealAppKey(UNIT_APPKEY, g_skt_app_nonce, net_id, RealAppKey);

        #if LORA_APP_DEBUG
        APP_TRACE_DEBUG("%16s : %06lx\n", "Net ID", net_id);
        //ExecDumpData(RealAppKey, sizeof(RealAppKey), "Real App Key : ");
        #endif

        /* Save real app key */
        DeviceUserDataSetSKTRealAppKey(RealAppKey);
        /* Set flag about real app key */
        DeviceUserDataSetFlag(FLAG_USE_RAK, FLAG_USE_RAK);

        APP_TRACE_LORA("%-22s\n", "{AppKeyReportAns}");
        break;
    }

    case MSG_SKT_NET_CONFIRMED_UP_NB_RETRANS:
        LORAWAN_SetMaxRetries(payload->Payload[0]);

        APP_TRACE_LORA("%-22s NbRet=%d\n", "{ConfirmedUpNbRet}", payload->Payload[0]);
        break;

    default:
        break;
    }

    g_skt_recv_cmd = payload->MessageType;

    return rc;
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/
static uint8_t ExecSktMakePacket(SktMessage *pMessage,
                                 uint8_t     nType,
                                 uint8_t    *pPayload,
                                 uint8_t     nPayloadLen)
{
    pMessage->Version     = LORA_SKT_MESSAGE_VERSION;
    pMessage->MessageType = nType;
    pMessage->PayloadLen  = nPayloadLen;
    if (nPayloadLen != 0) {
        Mem_Copy(pMessage->Payload, pPayload, nPayloadLen);
    }

    return sizeof(SktMessage) + nPayloadLen - 1;
}

