/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: Helper functions implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include "board.h"
#include "utilities.h"

/*!
 * Redefinition of rand() and srand() standard C functions.
 * These functions are redefined in order to get the same behavior across
 * different compiler toolchains implementations.
 */
// Standard random functions redefinition start
#define RAND_LOCAL_MAX 2147483647L

static uint32_t next = 1;

int32_t rand1( void )
{
    return ( ( next = next * 1103515245L + 12345L ) % RAND_LOCAL_MAX );
}

void srand1( uint32_t seed )
{
    next = seed;
}
// Standard random functions redefinition end

int32_t randr( int32_t min, int32_t max )
{
    return ( int32_t )rand1( ) % ( max - min + 1 ) + min;
}

void memcpyr( uint8_t *dst, const uint8_t *src, uint16_t size )
{
    dst = dst + ( size - 1 );
    while( size-- )
    {
        *dst-- = *src++;
    }
}

int8_t Nibble2HexChar( uint8_t a )
{
    if( a < 10 )
    {
        return '0' + a;
    }
    else if( a < 16 )
    {
        return 'A' + ( a - 10 );
    }
    else
    {
        return '?';
    }
}

uint8_t HexChar2Nibble( char a )
{
    if( '0' <= a && a <= '9')
    {
        return a - '0';
    }
    else if( 'a' <= a && a <= 'f')
    {
        return a - 'a' + 10 ;
    }
    else if( 'A' <= a && a <= 'F')
    {
        return a - 'A' + 10 ;
    }
    else
    {
        return 0;
    }
}

int8_t HexString2Array( const char* pString, uint8_t* pArray, uint32_t ulMaxLength )
{
    uint32_t ulLength = strlen(pString);

    if ((ulLength == 0) || (ulLength %2 > ulMaxLength) )
    {
        return 0;
    }

    for(uint32_t i = 0 ; i < ulLength ; i += 2)
    {
        pArray[i/2] = (HexChar2Nibble(pString[i]) << 4) | HexChar2Nibble(pString[i+1]);
    }

    return ulLength / 2;
}

uint16_t crc16_compute(const uint8_t *p_data, uint32_t d_size, const uint16_t *p_crc)
{
    uint32_t i;
    uint16_t ncrc = 0;

    if (!p_data || (d_size == 0)) {
        return 0;
    }

    ncrc = (p_crc == NULL) ? 0xffff : *p_crc;

    for (i = 0; i < d_size; i++) {
        ncrc  = (uint8_t)(ncrc >> 8) | (ncrc << 8);
        ncrc ^= p_data[i];
        ncrc ^= (uint8_t)(ncrc & 0xff) >> 4;
        ncrc ^= (ncrc << 8) << 4;
        ncrc ^= ((ncrc & 0xff) << 4) << 1;
    }

    return ncrc;
}

/* END OF FILE */
