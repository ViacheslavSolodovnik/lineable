/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LSM6DS3_ACC_GYRO_H_
#define _LSM6DS3_ACC_GYRO_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Defien features -----------------------------------------------------------*/
#define FEATURE_USE_DOUBLETAP
#define FEATURE_USE_PEDOMETER
#define FEATURE_USE_FIFO

/* Exported types ------------------------------------------------------------*/
enum fifo_mode {
    BYPASS = 0,
    FIFO,
    CONTINUOUS,
};

/* Exported macro ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
#define LSM6DS3_I2C_BUS_ADDRESS         (0x6A)

#define ST_INDIO_DEV_NUM                6

#define MOTION_EVENT_FALL_DET              ((uint8_t)(1 << 0))
#define MOTION_EVENT_INACTIVITY_DET        ((uint8_t)(1 << 1))
#define MOTION_EVENT_ACTIVITY_DET          ((uint8_t)(1 << 2))
#define MOTION_EVENT_DOUBLE_TAP_DET        ((uint8_t)(1 << 3))
#define MOTION_EVENT_TILT_DET              ((uint8_t)(1 << 4))
#define MOTION_EVENT_DET_MOTION            ((uint8_t)(1 << 5))
#define MOTION_EVENT_DET_REST              ((uint8_t)(1 << 6))

/**************************************************************************//**
* @name Error Codes
* @{
******************************************************************************/
#define LSM6DS3_OK                             0x00  /**< No errors                                        */
#define LSM6DS3_ERROR                          0x01  /**< The driver is not initialized                    */
#define LSM6DS3_ERROR_I2C_TRANSACTION_FAILED   0x02  /**< I2C transaction failed                           */
#define LSM6DS3_ERROR_DEVICE_ID_MISMATCH       0x03  /**< The device ID does not match the expected value  */
/**@}*/

/** Error Code : Motion */
#define MOTION_DEVICE_OK                             (0)  /**< No errors                                        */
#define MOTION_DEVICE_ERROR                          (1)  /**< Errors                                           */
#define MOTION_DEVICE_ERROR_DRIVER_NOT_INITIALIZED   (2)  /**< The driver is not initialized                    */
#define MOTION_DEVICE_ERROR_I2C_TRANSACTION_FAILED   (3)  /**< I2C transaction failed                           */
#define MOTION_DEVICE_ERROR_DEVICE_ID_MISMATCH       (4)  /**< The device ID does not match the expected value  */

/** Error Code : Accelerometer */
#define ACCEL_DEVICE_OK                             (0)  /**< No errors                                        */
#define ACCEL_DEVICE_ERROR_DRIVER_NOT_INITIALIZED   (1)  /**< The driver is not initialized                    */
#define ACCEL_DEVICE_ERROR_I2C_TRANSACTION_FAILED   (2)  /**< I2C transaction failed                           */
#define ACCEL_DEVICE_ERROR_DEVICE_ID_MISMATCH       (3)  /**< The device ID does not match the expected value  */
#define ACCEL_DEVICE_ERROR_FIFO_OVER_RUN            (4)

extern uint8_t LSM6DS3_Init(GPIOINT_IrqCallbackPtr_t cb_func);
extern uint8_t LSM6DS3_Prove(void);
extern uint8_t LSM6DS3_Start(void);
extern uint8_t LSM6DS3_Stop(void);
extern uint8_t LSM6DS3_MOTION_CtrlActivation(uint8_t enable);
extern uint8_t LSM6DS3_MOTION_CtrlTilt(uint8_t enable);
extern uint8_t LSM6DS3_GetEvent(uint8_t *p_event);
extern uint8_t LSM6DS3_CheckSensor(void);
extern uint8_t DEV_ACCEL_Init(void);
#ifdef FEATURE_USE_PEDOMETER
extern uint8_t LSM6DS3_StepCountRead(uint32_t *steps);
extern uint8_t LSM6DS3_StepCountReset(void);
#endif
#ifdef FEATURE_USE_FIFO
extern uint8_t LSM6DS3_ReadFifo(uint16_t num_bytes, uint8_t *data);
#endif
#endif /* _LSM6DS3_ACC_GYRO_H_ */

