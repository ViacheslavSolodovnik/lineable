#ifndef DRV_WATCHDOG_H_
#define DRV_WATCHDOG_H_

#include <stdbool.h>
#include <stdint.h>

#define SYS_WATCHDOG_TRIGGER (1 << 31)

/**
 * Initialize drv_watchdog module
 *
 * This should be called before using drv_watchdog module, preferably as early as possible.
 *
 */
void  drv_watchdog_init(void);

/**
 * Register current task in drv_watchdog module
 *
 * Returned identifier shall be used in all other calls to drv_watchdog from current task.
 * Once registered, task shall notify drv_watchdog periodically using drv_watchdog_notify() to
 * prevent watchdog expiration. It's up to actual task how this is done, but task can request that
 * it will be triggered periodically using task notify feature and it should notify-back drv_watchdog
 * as a response.
 *
 * \param [in] notify_trigger   true if task notify should be triggered periodically
 *
 * \return identifier on success, -1 on failure
 *
 * \sa drv_watchdog_notify
 *
 */
int8_t drv_watchdog_register(void);

/**
 * Unregister task from drv_watchdog module
 *
 * \param [in] id       identifier
 *
 * \sa drv_watchdog_register
 *
 */
void drv_watchdog_unregister(int8_t id);

/**
 * Inform drv_watchdog module about the wdog id of the IDLE task
 *
 * \param [in] id       IDLE task wdog identifier
 *
 * \sa drv_watchdog_register
 *
 */
void drv_watchdog_configure_idle_id(int8_t id);

/**
 * Suspend task monitoring in drv_watchdog module
 *
 * Suspended task is not unregistered entirely but will not be monitored by watchdog until resumed.
 * It's faster than unregistering and registering task again.
 *
 * \param [in] id       identifier
 *
 * \sa drv_watchdog_resume
 *
 */
void drv_watchdog_suspend(int8_t id);

/**
 * Resume task monitoring in drv_watchdog module
 *
 * Resumes task monitoring suspended previously by drv_watchdog_suspend().
 *
 * \param [in] id       identifier
 *
 * \note This function does not notify the watchdog service for this task. It is possible that
 *       monitor resuming occurs too close to the time that the watchdog expires, before the task
 *       has a chance to explicitly send a notification. This can lead to an unwanted reset.
 *       Therefore, either call drv_watchdog_notify() before calling drv_watchdog_resume(), or use
 *       drv_watchdog_notify_and_resume() instead.
 *
 * \sa drv_watchdog_suspend
 * \sa drv_watchdog_notify_and_resume
 *
 */
void drv_watchdog_resume(int8_t id);

/**
 * Notify drv_watchdog module for task
 *
 * Registered task shall use this periodically to notify drv_watchdog module that it's alive. This
 * should be done frequently enough to fit into hw_watchdog interval set by dg_configWDOG_RESET_VALUE.
 *
 * \param [in] id       identifier
 *
 * \sa drv_watchdog_set_latency
 *
 */
void drv_watchdog_notify(int8_t id);

/**
 * Notify drv_watchdog module for task with handle \p id and resume its monitoring
 *
 * This function combines the functionality of drv_watchdog_notify() and drv_watchdog_resume().
 *
 * \param [in] id       identifier
 *
 * \sa drv_watchdog_notify()
 * \sa drv_watchdog_resume()
 *
 */
void drv_watchdog_notify_and_resume(int8_t id);

/**
 * Set watchdog latency for task
 *
 * This allows task to miss given number of notifications to drv_watchdog without triggering
 * platform reset. Once set, it's allowed that task does not notify drv_watchdog for \p latency
 * consecutive hw_watchdog intervals (as set by dg_configWDOG_RESET_VALUE) which can be used to
 * allow for parts of code which are known to block for long period of time (i.e. computation).
 * This value is set once and does not reload automatically, thus it shall be set every time
 * increased latency is required.
 *
 * \param [in] id       identifier
 * \param [in] latency  latency
 *
 * \sa drv_watchdog_notify
 *
 */
void drv_watchdog_set_latency(int8_t id, uint8_t latency);

/**
 * Find out if the only task currently monitored is the IDLE task.
 *
 * \return true if only the IDLE task is currently monitored, else false. This function is used
 * from CPM in order to know if watchdog should be stopped during sleep.
 */
bool drv_watchdog_monitor_mask_empty(void);

#endif /* SYS_WATCHDOG_H_ */

/**
 * \}
 * \}
 * \}
 */

