/*******************************************************************************
 *  Copyright 2019 Lineable (C) by All Rights Reserved.
 *  Unauthorized redistribution of this source code, in whole or part,
 *  without the express written permission of Lineable is strictly prohibited.
*******************************************************************************/

/**
 *
 * @file fuelguage.h
 * @brief Device independent fuelguage interface.
*/

#ifndef DRIVERS_FUELGAUGE_FUELGAUGE_H_
#define DRIVERS_FUELGAUGE_FUELGAUGE_H_

#include <stdint.h>
#include <stdbool.h>

/*******************************************************************************
* @name Fuelgauge Error Codes
*******************************************************************************/
/**@{*/
#define FUELGAUGE_OK                             0x0000  /**< No errors                                        */
#define FUELGAUGE_ERROR                          0x0001  /**<                                                  */
#define FUELGAUGE_ERROR_I2C_TRANSACTION_FAILED   0x0002  /**< I2C transaction failed                           */
#define FUELGAUGE_ERROR_DEVICE_ID_MISMATCH       0x0003  /**< The device ID does not match the expected value  */
#define FUELGAUGE_ERROR_CHECKSUM_MISMATCH        0x0004  /**< Checksum value does not match the expected value */
/**@}*/

/*******************************************************************************
* @name Fuelgauge public interface
*******************************************************************************/
/**@{*/
/**
 * Initialize fuel gauge module and underlying hardware
 *
 * @return FUELGAUGE_OK on success and FUELGAUGE_ERROR_XXXX otherwise
 */
uint16_t FUELGAUGE_Initialize(void);

/**
 * Return current cell voltage
 *
 * @param vol_level output parameter stores cell voltage
 * @return FUELGAUGE_OK on success and FUELGAUGE_ERROR_XXXX otherwise
 */
uint16_t FUELGAUGE_GetVcell(uint16_t *vol_level);

/**
 * Returns battery state of charge
 *
 * @param soc output parametr. Stores battery state of charge
 * @return FUELGAUGE_OK on success and FUELGAUGE_ERROR_XXXX otherwise
 */
uint16_t FUELGAUGE_GetSoc(uint16_t *soc);
/**@}*/

#endif /* DRIVERS_FUELGAUGE_FUELGAUGE_H_ */
