/*******************************************************************************
//  Copyright 2019 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.

 * @file lc709203f.c
 * @brief Driver for lc709203f fuelgauge
 * @version 0.0
 ******************************************************************************/

#include "rtos_app.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>

#include  <drivers/peripheral/i2c/micrium_i2c.h>

#include "lc709203f.h"

/*******************************************************************************
*                                 DEBUG OPTION
*******************************************************************************/

#undef  __MODULE__
#define __MODULE__ "FUELGAUGE"
#undef  DBG_MODULE
#define DBG_MODULE 0

#ifdef configUSE_FUELGAUGE
/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/

/*******************************************************************************
*                                  DEFINES
*******************************************************************************/

/*******************************************************************************
*                                  MACROS
*******************************************************************************/

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/

/*******************************************************************************
*                         PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/
static uint16_t LC709203F_ReadReg(uint8_t reg, uint16_t *data);
static uint16_t LC709203F_WriteReg(uint8_t reg, uint16_t data);
static uint8_t Get_CRC(uint8_t *rec_values, uint8_t len);
static uint16_t LC709203F_Initialize(void);

/*******************************************************************************
*                              PRIVATE FUNCTIONS
*******************************************************************************/
/*******************************************************************************
 * @brief
 *    Generic Reading function with I2C
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint16_t LC709203F_ReadReg(uint8_t reg, uint16_t *data)
{
    I2C_TransferReturn_TypeDef ret;
    uint8_t rec_buf[3];

    /* add do while() code because of i2c error */
    do{
    	ret = i2cm_readData(i2c0_id, FUELGAUGE_LC709203F_I2C_BUS_ADDRESS, reg, rec_buf, 3);

        if (ret != i2cTransferDone) {
        	ret = FUELGAUGE_ERROR_I2C_TRANSACTION_FAILED;
        }

        uint8_t crc_buf[5] = { FUELGAUGE_LC709203F_I2C_BUS_ADDRESS << 1, reg, ((FUELGAUGE_LC709203F_I2C_BUS_ADDRESS << 1) | 0x01), rec_buf[0], rec_buf[1] };
        if (Get_CRC(crc_buf, 5) != rec_buf[2]) {
        	ret = FUELGAUGE_ERROR_CHECKSUM_MISMATCH;
        }
    }while(ret != FUELGAUGE_OK);

    APP_TRACE_DEBUG("%x %x %x\r\n", rec_buf[0], rec_buf[1], rec_buf[2]);
    *data = (uint16_t)rec_buf[0] | ((uint16_t)rec_buf[1] << 8);

    return ret;
}

/*******************************************************************************
 * @brief
 *    Generic Writing function with I2C
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint16_t LC709203F_WriteReg(uint8_t reg, uint16_t data)
{
    I2C_TransferReturn_TypeDef ret;
    uint8_t crc_buf[4] = { FUELGAUGE_LC709203F_I2C_BUS_ADDRESS << 1, reg, (uint8_t)data, (uint8_t)(data<<8) };
    uint8_t send_buf[3] = { crc_buf[2], crc_buf[3], Get_CRC(crc_buf, 4) };

    ret = i2cm_writeData(i2c0_id, FUELGAUGE_LC709203F_I2C_BUS_ADDRESS, reg, send_buf, 3);

    if (ret != i2cTransferDone) {
        return FUELGAUGE_ERROR_I2C_TRANSACTION_FAILED;
    }

    return FUELGAUGE_OK;
}

/*******************************************************************************
 * @brief
 *    does a crc check
 *
 * @return
 *    Returns the checksum
 ******************************************************************************/
static uint8_t Get_CRC(uint8_t *rec_values, uint8_t len)
{
    uint8_t crc = 0x00;
    uint8_t current_byte;
    uint8_t bit;


    for (current_byte = 0; current_byte < len; current_byte++) {
        crc ^= (rec_values[current_byte]);
        for (bit = 8; bit > 0; bit--) {
            if (crc & 0x80) {
                crc = (crc << 1) ^ LC709203F_CRC_POLYNOMIAL;
            }
            else {
                crc = (crc << 1);
            }
        }
    }
    return crc;
}

/*******************************************************************************
*                              PUBLIC FUNCTIONS
*******************************************************************************/


/*******************************************************************************
 * @brief
 *    Fuelgauge initialize
 *
 * @return
 *    Returns zero on OK, non-zero otherwise  TPS65720_GPIO_DIR
 ******************************************************************************/
static uint16_t LC709203F_Initialize(void)
{
    RTOS_ERR err;

    /* delay between 150ms and 600ms */
    OSTimeDly(OS_MS_2_TICKS(100), OS_OPT_TIME_DLY, &err); //intialization time : 90ms(datasheet 5page)

    /* Set operation mode(operation:0x01, sleep:0x02) */
    if (LC709203F_WriteReg(LC709203F_REG_POWER_MODE, 0x0001) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE_ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    //init sequence(LC709203F-D document 16page)
    /* Set APA(battery type:0x06, 230mAh:about 0x16) */
    if (LC709203F_WriteReg(LC709203F_REG_APA, 0x0016) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE_ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    /* Set Battery Profile(battery type:0x06, Number of Parameter:0x0706) */
    if (LC709203F_WriteReg(LC709203F_REG_CHANGE_PARAMETER, 0x0001) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE_ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    /* Set initial RSOC */
    if (LC709203F_WriteReg(LC709203F_REG_INITIAL_RSOC, 0xAA55) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE_ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    /* Set via I2C Mode(I2C:0x00, Thermistor:0x01) */
    if (LC709203F_WriteReg(LC709203F_REG_STATUS, 0x0000) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE_ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    // /* Set Temperature(default) */
    // if (LC709203F_WriteReg(LC709203F_REG_CELL_TEMP, 0xAA00) != FUELGAUGE_OK) {
   	//     APP_TRACE_DEBUG("FUELGAUGE_ERROR\r\n");
    //     return FUELGAUGE_ERROR;
    // }

    /* delay at least 150ms */
    OSTimeDly(OS_MS_2_TICKS(200), OS_OPT_TIME_DLY, &err);

    return FUELGAUGE_OK;
}


uint16_t FUELGAUGE_Initialize(void)
{
	return LC709203F_Initialize();
}

/*******************************************************************************
 * @brief
 *    Get VCell
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint16_t FUELGAUGE_GetVcell(uint16_t *vol_level)
{
	uint16_t rec_buf;

    if (LC709203F_ReadReg(LC709203F_REG_CELL_VOLTAGE, &rec_buf) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE Read ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    *vol_level = rec_buf;

    return FUELGAUGE_OK;
}

/*******************************************************************************
 * @brief
 *    Get SOC
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint16_t FUELGAUGE_GetSoc(uint16_t *soc_value)
{
	uint16_t rec_buf;

    if (LC709203F_ReadReg(LC709203F_REG_RSOC, &rec_buf) != FUELGAUGE_OK) {
    	APP_TRACE_DEBUG("FUELGAUGE Read ERROR\r\n");
        return FUELGAUGE_ERROR;
    }

    *soc_value = rec_buf;

    return FUELGAUGE_OK;
}
#endif
