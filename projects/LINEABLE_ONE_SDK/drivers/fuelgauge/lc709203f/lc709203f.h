/*
 * lc709203f.h
 *
 *  Created on: 2019. 3. 28.
 *      Author: Eliott
 */

#ifndef DRIVERS_INCLUDE_FUELGAUGE_LC709203F_H_
#define DRIVERS_INCLUDE_FUELGAUGE_LC709203F_H_

#include <drivers/fuelgauge/fuelgauge.h>

#ifdef configUSE_FUELGAUGE
/*******************************************************************************
*                                  INCLUDES
*******************************************************************************/


/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/


/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
/*****************************************************************************
* @name I2C definitions
******************************************************************************/
#define FUELGAUGE_I2C_BUS_TIMEOUT                   (1000)
#define FUELGAUGE_I2C_DEVICE                        (I2C0)
#define FUELGAUGE_LC709203F_I2C_BUS_ADDRESS         (0x0B)


// Register Map
#define LC709203F_REG_CELL_VOLTAGE        0x09
#define LC709203F_REG_RSOC                0x0D
#define LC709203F_REG_ITE                 0x0F
#define LC709203F_REG_ID                  0x11
#define LC709203F_REG_CELL_TEMP           0x08
#define LC709203F_REG_STATUS              0x16
#define LC709203F_REG_POWER_MODE          0x15
#define LC709203F_REG_ALARM_VOLTAGE       0x14
#define LC709203F_REG_ALARM_RSOC          0x13
#define LC709203F_REG_CHANGE_PARAMETER    0x12
#define LC709203F_REG_APT                 0x0C
#define LC709203F_REG_APA                 0x0B
#define LC709203F_REG_CURRENT_DIRECTION   0x0A
#define LC709203F_REG_THERMISTOR          0x06
#define LC709203F_REG_BEFORE_RSOC         0x04
#define LC709203F_REG_INITIAL_RSOC        0x07
#define LC709203F_CRC_POLYNOMIAL          0x07

/*******************************************************************************
*                                  MACROS
*******************************************************************************/

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/

#endif /* configUSE_FUELGAUGE */

#endif /* DRIVERS_INCLUDE_FUELGAUGE_LC709203F_H_ */
