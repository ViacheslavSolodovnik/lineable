#include <bsphalconfig.h>//#include "bsp.h"
#include "gpiointerrupt.h"
#include "em_cmu.h"
#include "em_rmu.h"
#include "em_wdog.h"
#include "drv_watchdog.h"
#include <kernel/include/os.h>
#include <common/include/rtos_utils.h>
/**
 * \brief Maximum watchdog tasks
 *
 * Maximum number of tasks that the Watchdog Service can monitor. It can be larger (up to 32) than
 * needed, at the expense of increased Retention Memory requirement.
 * 
 */
#ifndef configWDOG_MAX_TASKS_CNT
#define configWDOG_MAX_TASKS_CNT     (5)
#endif



#if configUSE_WDOG

/* mutex to synchronize access to wdog data */
static OS_MUTEX lock;

/* number of tasks registered for wdog */
static int8_t max_task_id = 0;

/* bitmask of tasks identifiers which are registered */
static uint32_t tasks_mask = 0;

/* bitmask of tasks identifiers which are monitored (registered and not suspended) */
static uint32_t tasks_monitored_mask = 0;

/* bitmask of tasks which notified during last period */
volatile static uint32_t notified_mask = 0;

/* allowed latency set by tasks, if any */
static uint8_t tasks_latency[configWDOG_MAX_TASKS_CNT] = {0,};

#ifndef RELEASE
/* handles of monitored tasks */
OS_TCB*  tasks_handle[configWDOG_MAX_TASKS_CNT] = {0,};
#endif

/* the wdog id of the IDLE task */
static int8_t idle_task_id = -1;

#define VALIDATE_ID(id) \
        do { \
                if ((id) < 0 || (id) >= configWDOG_MAX_TASKS_CNT) { \
                    APP_RTOS_ASSERT_DBG(0,0); \
                        return; \
                } \
        } while (0)

#if configUSE_WDOG

/**
 * \brief Return current OS task handle
 *
 * \return current task handle of type OS_TASK
 *
 */
#define OS_GET_CURRENT_TASK()               (OSTCBCurPtr)

static void reset_watchdog(void)
{
    notified_mask = 0;
    WDOGn_Feed(DEFAULT_WDOG);
}
/* ISR */
/******************************************************************************
 * @brief WDOG Interrupt Handler. Clears interrupt flag.
 *        The interrupt table is in assembly startup file startup_efm32.s
 *
 *****************************************************************************/
#define WDOG_INQ_FLAG_SEL    WDOG_IEN_WARN//WDOG_IEN_PEM0
void WDOG0_IRQHandler(void)
{
    uint32_t tmp_mask = tasks_monitored_mask;
    uint32_t latency_mask = 0;
    int i;

    WDOGn_IntClear(DEFAULT_WDOG, WDOG_INQ_FLAG_SEL );

    /*
     * watchdog is reset immediately when we detect that all tasks notified during period so
     * no need to check this here
     *
     * but if we're here, then check if some tasks have non-zero latency and remove them from
     * notify mask check and also decrease latency for each task
     */
    for (i = 0; i <= max_task_id; i++) {
            if (tasks_latency[i] == 0) {
                    continue;
            }

            tasks_latency[i]--;

            latency_mask |= (1 << i);
    }

    /*
     * check if all remaining tasks notified and reset hw_watchdog in such case
     */
    tmp_mask &= ~latency_mask;
    if ((notified_mask & tmp_mask) == tmp_mask) {
            goto reset_wdog;
    }

    /*
     * latency for all tasks expired and some of them still did not notify drv_watchdog
     * we'll let watchdog reset the system
     */
     // Wait for the reset to occur
    while (1);

reset_wdog:
    reset_watchdog();
}

#endif



#endif

void drv_watchdog_init(void)
{
#if configUSE_WDOG
    RTOS_ERR err;
    unsigned long resetCause;      /* Reset cause */

    max_task_id = 0;
    notified_mask = 0;
    // Enabling clock to the interface of the low energy modules (including the Watchdog)
    CMU_ClockEnable(cmuClock_HFLE, true);

    /* Store the cause of the last reset, and clear the reset cause register */
    resetCause = RMU_ResetCauseGet();
    RMU_ResetCauseClear();

    /* Check if the watchdog triggered the last reset */
    if (resetCause & RMU_RSTCAUSE_WDOGRST){
        printf("\n\n\nBITE!\n\nSOURCE:0x%x\n", (unsigned int) resetCause);
    }
  // Watchdog Initialize settings 
    WDOG_Init_TypeDef wdogInit = WDOG_INIT_DEFAULT;
    wdogInit.enable   = false; /* Start watchdog when initialization is done. */ 
    wdogInit.debugRun = false;  /* WDOG is not counting during debug halt. */
    wdogInit.em2Run   = false;            /* WDOG is not counting when in EM2. */
    wdogInit.clkSel = wdogClkSelULFRCO; /* Select 1kHZ WDOG oscillator */
    wdogInit.perSel = wdogPeriod_4k;    /* Set the watchdog period to 4096 clock periods (ie ~4 seconds) */
    //wdogInit.winSel = wdogIllegalWindowTime50_0pct; /* Set window to 50 percent (ie ~2 seconds) */
    wdogInit.warnSel = wdogWarnTime25pct; /* Set warning to 25 percent (ie ~1 seconds) */

  // Initializing watchdog with chosen settings 
    WDOGn_Init(DEFAULT_WDOG, &wdogInit);
    OSMutexCreate(&lock, "watchdog", &err);

    /* Enabling watchdog, since it was not enabled during initialization */
    WDOGn_Enable(DEFAULT_WDOG, true);
    /* Locking watchdog register (reset needed to unlock) */
    //WDOGn_Lock(DEFAULT_WDOG);

    //test
    //WDOGn_IntEnable(DEFAULT_WDOG, WDOG_INQ_FLAG_SEL);
    //NVIC_EnableIRQ(WDOG0_IRQn);
#endif
}

int8_t drv_watchdog_register(void)
{
#if configUSE_WDOG
    RTOS_ERR err;
    int8_t id = 0;

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);

    while (tasks_mask & (1 << id)) {
            id++;
    }

    if (id >= configWDOG_MAX_TASKS_CNT) {
        /* Don't allow registration of more than configWDOG_MAX_TASKS_CNT */
        APP_RTOS_ASSERT_DBG(0,0); 
        return -1;
    }

    tasks_mask |= (1 << id);
    tasks_monitored_mask |= (1 << id);

#ifndef RELEASE
    tasks_handle[id] = OS_GET_CURRENT_TASK();
#endif

    if (id > max_task_id) {
        max_task_id = id;
    }

    if (id == 0) {
        WDOG_Feed();
        WDOGn_IntEnable(DEFAULT_WDOG, WDOG_INQ_FLAG_SEL);
        NVIC_EnableIRQ(WDOG0_IRQn);
    }

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);

    return id;
#else
    return 0;
#endif
}

void drv_watchdog_unregister(int8_t id)
{
#if configUSE_WDOG
    RTOS_ERR err;
    uint32_t tmp_mask;
    int8_t new_max = 0;

    VALIDATE_ID(id);

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );

    tasks_mask &= ~(1 << id);
    tasks_monitored_mask &= ~(1 << id);
    tasks_latency[id] = 0;

#ifndef RELEASE
    tasks_handle[id] = DEF_NULL;
#endif
    /* recalculate max task id */
    tmp_mask = tasks_mask;
    while (tmp_mask) {
            tmp_mask >>= 1;
            new_max++;
    }

    max_task_id = new_max;

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);
#endif
}

void drv_watchdog_configure_idle_id(int8_t id)
{
#if configUSE_WDOG
    idle_task_id = id;

#ifndef RELEASE
    tasks_handle[id] = &OSIdleTaskTCB;
#endif

#endif
}

void drv_watchdog_suspend(int8_t id)
{
#if configUSE_WDOG
    RTOS_ERR err;
    VALIDATE_ID(id);

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );

    tasks_monitored_mask &= ~(1 << id);

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);
#endif
}

#if configUSE_WDOG
static inline void resume_monitoring(int8_t id)
{
    tasks_monitored_mask |= (1 << id);
    tasks_monitored_mask &= tasks_mask;
}
#endif

void drv_watchdog_resume(int8_t id)
{
#if configUSE_WDOG
    RTOS_ERR err;
    
    VALIDATE_ID(id);

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );

    resume_monitoring(id);

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);
#endif
}

#if configUSE_WDOG
static inline void notify_about_task(int8_t id)
{
    /* Make sure that the requested task is one of the watched tasks */
    APP_RTOS_ASSERT_DBG((tasks_mask & (1 << id)), 0);

    if (tasks_mask & (1 << id)) {
        notified_mask |= (1 << id);

        /*
            * we also reset latency here because it's ok for app to notify before latency
            * expired, but it should start with zero latency for next notification interval
            */
        tasks_latency[id] = 0;

        if ((notified_mask & tasks_monitored_mask) == tasks_monitored_mask) {
                reset_watchdog();
        }
    }

}

static inline void notify_idle(int8_t id)
{
    /* Notify the IDLE task every time one of the monitored tasks notifies the service. */
    if ((id != idle_task_id) && (idle_task_id != -1)) {
        drv_watchdog_notify(idle_task_id);
    }
}
#endif

void drv_watchdog_notify(int8_t id)
{
#if configUSE_WDOG
    RTOS_ERR err;

    VALIDATE_ID(id);

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );

    notify_about_task(id);

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);

    notify_idle(id);
#endif
}

void drv_watchdog_notify_and_resume(int8_t id)
{
#if configUSE_WDOG
    RTOS_ERR err;
    
    VALIDATE_ID(id);

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );

    resume_monitoring(id);
    notify_about_task(id);

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);

    notify_idle(id);
#endif
}

void drv_watchdog_set_latency(int8_t id, uint8_t latency)
{
#if configUSE_WDOG
    RTOS_ERR err;

    VALIDATE_ID(id);

    OSMutexPend(&lock, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err );

    tasks_latency[id] = latency;

    OSMutexPost(&lock, OS_OPT_POST_NONE ,&err);
#endif
}

bool drv_watchdog_monitor_mask_empty()
{
#if configUSE_WDOG
    return ((idle_task_id != -1) && (tasks_monitored_mask == (1 << idle_task_id)));
#else
    return true;
#endif
}



