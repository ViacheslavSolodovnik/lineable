/***************************************************************************************************
//
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//--------------------------------------------------------------------------------------------------
//   Author:
//   File name :
//   Created :
//   Last Update :
***************************************************************************************************/

/***************************************************************************************************
* INCLUDE FILES
***************************************************************************************************/
/*---- system and platform files -----------------------------------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <math.h>
#include "rtos_app.h"
#include "gpiointerrupt.h"
#include  <drivers/peripheral/i2c/micrium_i2c.h>
#include "userdata.h"
#include "sys_os.h"
/*---- program files -----------------------------------------------------------------------------*/
#include "lsm6ds3_acc_gyro.h"

#undef  __MODULE__
#define __MODULE__ "6AXIS"
#undef  DBG_MODULE
#define DBG_MODULE 0

/***************************************************************************************************
* EXTERNAL REFERENCES             NOTE: only use if not available in header file
***************************************************************************************************/
/*---- function prototypes -----------------------------------------------------------------------*/
/*---- data declarations -------------------------------------------------------------------------*/

/***************************************************************************************************
* PUBLIC DECLARATIONS             Defined here, used elsewhere
***************************************************************************************************/
/*---- context -----------------------------------------------------------------------------------*/
/*---- function prototypes -----------------------------------------------------------------------*/
uint8_t LSM6DS3_Init(GPIOINT_IrqCallbackPtr_t cb_func);
uint8_t LSM6DS3_Prove(void);
uint8_t LSM6DS3_Start(void);
uint8_t LSM6DS3_Stop(void);
uint8_t LSM6DS3_GetEvent(uint8_t *p_event);
uint8_t LSM6DS3_CheckSensor(void);
#ifdef FEATURE_USE_PEDOMETER
uint8_t LSM6DS3_StepCountRead(uint32_t *steps);
uint8_t LSM6DS3_StepCountReset(void);
#endif
#ifdef FEATURE_USE_FIFO
uint8_t LSM6DS3_ReadFifo(uint16_t num_bytes, uint8_t *data);
#endif
/*---- data declarations -------------------------------------------------------------------------*/

/***************************************************************************************************
* PRIVATE DECLARATIONS            Defined here, used elsewhere
***************************************************************************************************/
/*---- context -----------------------------------------------------------------------------------*/
#define MIN(a, b)                   (((a) < (b)) ? (a) : (b))
#define BIT(x)                      (1 << x)
#define LSM6DS3_REG_EN_BIT                          0x01
#define LSM6DS3_REG_DIS_BIT                         0x00
/* Output Data Rate Value */
#define LSM6DS3_ODR_LIST_NUM                     6
#define LSM6DS3_ODR_POWER_OFF_VAL                0x00
#define LSM6DS3_ODR_13HZ_VAL                     0x01
#define LSM6DS3_ODR_26HZ_VAL                     0x02
#define LSM6DS3_ODR_52HZ_VAL                     0x03
#define LSM6DS3_ODR_104HZ_VAL                    0x04
#define LSM6DS3_ODR_208HZ_VAL                    0x05
#define LSM6DS3_ODR_416HZ_VAL                    0x06
/** Who I am ID register */
#define LSM6DS3_WAI_REG_ADDR                    0x0f
#define     LSM6DS3_WAI_VALUE                   0x69
/** INT1, INT2 pin control registers */
#define LSM6DS3_INT1_CTRL_REG_ADDR              0x0d
#define     INT1_CTRL_INT1_FULL_FLAG_MASK       0x20
#define     INT1_CTRL_INT1_STEP_DETECTOR_MASK   0x80
#define LSM6DS3_INT2_CTRL_REG_ADDR              0x0e
#define     INT2_CTRL_INT2_STEP_DELTA_MASK      0x80
#define     INT2_CTRL_INT2_FULL_FLAG_MASK       0x20
#define     INT2_CTRL_INT2_FIFO_OVR_MASK        0x10
#define     INT2_CTRL_INT2_FTH_MASK             0x08
/** Control regiseters */
#define LSM6DS3_CTRL1_XL_REG_ADDR               0x10
#define     CTRL1_XL_ODR_XL_MASK                0xf0
#define LSM6DS3_CTRL2_G_REG_ADDR                0x11
#define     CTRL2_G_ODR_G_MASK                  0xf0
#define LSM6DS3_CTRL3_C_REG_ADDR                0x12
#define     CTRL3_C_BOOT_MASK                   0x80
#define     CTRL3_C_BDU_MASK                    0x40
#define     CTRL3_C_SW_RESET_MASK               0x01
#define LSM6DS3_CTRL4_C_REG_ADDR                0x13
#define     CTRL4_C_INT2_ON_INT1_MASK           0x20
#define     CTRL4_C__STOP_ON_FTH_MASK           0x01
#define LSM6DS3_CTRL6_C_REG_ADDR                0x15
#define     CTRL6_C_XL_HM_MODE_MASK             0x10
#define LSM6DS3_CTRL7_G_REG_ADDR                0x16
#define     CTRL7_G_G_HM_MODE_MASK              0x80
#define     CTRL7_G_ROUNDING_STATUS_MASK        0x04
#define LSM6DS3_CTRL10_C_REG_ADDR               0x19
#define     CTRL10_C_PEDO_RST_STEP_MASK         0x02
#define     CTRL10_C_FUNC_EN_MASK               0x04
/** Embedded functions configuration registers */
#define LSM6DS3_FUNC_CFG_ACCESS_REG_ADDR        0x01
#define     FUNC_CFG_ACCESS_FUNC_CFG_EN_MASK    0x80
/** FIFO configuration registers */
#define LSM6DS3_FIFO_CTRL1_REG_ADDR             0x06
#define LSM6DS3_FIFO_CTRL2_REG_ADDR             0x07
#define     FIFO_CTRL_FTH_MASK                  0x0fff
#define LSM6DS3_FIFO_CTRL3_REG_ADDR             0x08
#define     FIFO_CTRL3_DEC_FIFO_GYRO_MASK       0x38
#define     FIFO_CTRL3_DEC_FIFO_XL_MASK         0x07
#define         DEC_NOT_IN_FIFO                 (0)
#define         DEC_NO_DECIMATION               (1)
#define LSM6DS3_FIFO_CTRL4_REG_ADDR             0x09
#define LSM6DS3_FIFO_CTRL5_REG_ADDR             0x0a
#define     FIFO_CTRL5_FIFO_MODE_MASK           0x07
#define         FIFO_MODE_BYPASS                (0)    /* FIFO disable */
#define         FIFO_MODE_FIFO                  (1)    /* Stops collecting data when FIFO is full */
#define         FIFO_MODE_CONTINUOUS            (6)    /* If the FIFO is full, the new sample overwrites the older one */
#define     FIFO_CTRL5_ODR_FIFO_MASK            0x78
#define         FIFO_ODR_26HZ                   (2)
#define         FIFO_ODR_52HZ                   (3)
#define         FIFO_ODR_104HZ                  (4)
/** Step counter output registers */
#define LSM6DS3_STEP_COUNTER_L_REG_ADDR         0x4b
#define LSM6DS3_STEP_COUNTER_H_REG_ADDR         0x4c
/** FIFO status registers */
#define LSM6DS3_FIFO_STATUS1_REG_ADDR           0x3a
#define LSM6DS3_FIFO_STATUS2_REG_ADDR           0x3b
#define     FIFO_STATUS2_DIFF_FIFO_MASK         0x0f
#define     FIFO_STATUS2_FTH_MAKS               0x80
#define     FIFO_STATUS2_FIFO_OVER_RUN_MASK     0x40
#define     FIFO_STATUS2_FIFO_FULL_MASK         0x20
#define     FIFO_STATUS2_FIFO_EMPTY_MASK        0x10
#define         FIFO_BYTE_FOR_CHANNEL           (2)
#define         FIFO_ELEMENT_LEN_BYTE           (6)
#define         FIFO_MAX_SIZE                   (8192)
#define         FIFO_MAX_THRESHOLD              (1092)
#define         FIFO_MAX_LENGTH                 (FIFO_MAX_SIZE / FIFO_ELEMENT_LEN_BYTE)
#define LSM6DS3_FIFO_STATUS3_REG_ADDR           0x3c
#define LSM6DS3_FIFO_STATUS4_REG_ADDR           0x3d
/** FIFO data output registers */
#define LSM6DS3_FIFO_DATA_OUT_L_REG_ADDR        0x3e
/** Interrupt registers */
#define LSM6DS3_WAKE_UP_SRC_REG_ADDR            0x1b
#define     WAKE_UP_SRC_FF_IA_MASK              0x20
#define     WAKE_UP_SRC_SLEEP_STATE_IA_MASK     0x10
#define     WAKE_UP_SRC_WU_IA_MASK              0x08
#define LSM6DS3_TAP_SRC_REG_ADDR                0x1c
#define     TAP_SRC_DOUBLE_TAP_MASK             0x10
#define LSM6DS3_D6D_SRC_REG_ADDR                0x1d
#define LSM6DS3_STATUS_REG_ADDR                 0x1e
#define LSM6DS3_FUNC_SRC_REG_ADDR               0x53
#define     FUNC_SRC_TILT_IA_MASK               0x20
#define     FUNC_SRC_STEP_CNT_DELTA_IA_MASK     0x80
#define     FUNC_SRC_STEP_DETECTED_IA_MASK      0x10
/** Interrupt registers */
#define LSM6DS3_TAP_CFG_REG_ADDR                0x58
#define     TAP_CFG_PEDO_EN_MASK                0x40
#define     TAP_CFG_TILT_EN_MASK                0x20
#define     TAP_CFG_SLOPE_FDS_MASK              0x10
#define     TAP_CFG_TAP_X_EN_MASK               0x08
#define     TAP_CFG_TAP_Y_EN_MASK               0x04
#define     TAP_CFG_TAP_Z_EN_MASK               0x02
#define     TAP_CFG_LIR_MASK                    0x01
#define LSM6DS3_TAP_THS_6D_REG_ADDR             0x59
#define     TAP_THS_6D_TAP_THS_MASK             0x0f
#define         TAP_THS_DEFAULT                 (12)
#define LSM6DS3_INT_DUR2_REG_ADDR               0x5a
#define     INT_DUR2_DUR_MASK                   0xf0
#define         DUR_MAX_TIME_GAP                (1)
#define     INT_DUR2_QUIET_MASK                 0x0c
#define         QUIET_TIME_AFTER_TAP            (0)
#define     INT_DUR2_SHOCK_MASK                 0x03
#define         MAX_DUR_OVRTHS                  (0)
#define LSM6DS3_WAKE_UP_THS_REG_ADDR            0x5b
#define     WAKE_UP_THS_SINGLE_DOUBLE_TAP_MASK  0x80
#define     WAKE_UP_THS_INACTIVITY_MASK         0x40
#define     WAKE_UP_THS_WK_THS_MASK             0x3f
#define         THS_WAKEUP_VAL                  (2)
#define LSM6DS3_WAKE_UP_DUR_REG_ADDR            0x5c
#define     WAKE_UP_DUR_FF_DUR5_MASK            0x80
#define     WAKE_UP_DUR_SLEEP_DUR_MASK          0x0f
#define         DUR_GO_SLEEP_MODE_VAL           (3)
#define LSM6DS3_FREE_FALL_REG_ADDR              0x5d
#define     FREE_FALL_FF_DUR_MASK               0xf8
#define         FREE_FALL_DUR_VAL               (15)
#define     FREE_FALL_FF_THS_MASK               0x07
#define         FREE_FALL_THS_VAL               (0)
#define LSM6DS3_MD1_CFG_REG_ADDR                0x5e
#define     MD1_CFG_INT1_INACT_STATE_MASK       0x80
#define     MD1_CFG_INT1_DOUBLE_TAP_MASK        0x08
#define     MD1_CFG_INT1_FF_MASK                0x10
#define     MD1_CFG_INT1_TILT_MASK              0x02
#define LSM6DS3_MD2_CFG_REG_ADDR                0x5f
/* */
#define NUM_OF_FIFO_SAMPLE_DATA                 50
/* */
#define DEFAULT_SENSOR_ODR                      52
/* */
enum st_mask_id {
    ST_MASK_ID_ACCEL = 0,
    ST_MASK_ID_GYRO,
    ST_MASK_ID_MOTION,
    ST_MASK_ID_MAX
};
typedef struct {
    uint16_t head;
    uint16_t tail;
    uint16_t size;
    uint8_t  *element;
} Lsm6ds3FifoBuf;
/*---- function prototypes -----------------------------------------------------------------------*/
static int8_t I2C_WriteReg(uint8_t reg_addr, uint8_t len, uint8_t *data);
static int8_t I2C_ReadReg(uint8_t reg_addr, uint16_t len, uint8_t *data);
static int8_t I2C_WriteRegWithMask(uint8_t reg_addr, uint8_t mask, uint8_t data);
static int8_t ProbeSensor(void);
static int8_t InitSensor(void);
static int8_t SetODRValue(enum st_mask_id id, uint8_t odr, bool force);
#ifdef FEATURE_USE_DOUBLETAP
static int8_t ConfigDoubleTap(void);
#endif
#ifdef FEATURE_USE_TILT
static int8_t ConfigTilt(void);
#endif
#ifdef FEATURE_USE_PEDOMETER
static int8_t ConfigPedometer(void);
#endif
#ifdef FEATURE_USE_FIFO
static uint8_t ConfigFifo(void);
static int8_t SetFifoMode(enum fifo_mode fm);
static int8_t SetFifoWatermark(uint32_t pattern);
static int8_t SetFifoIntr(void);
#endif
/*---- data declarations -------------------------------------------------------------------------*/
struct st_lsm6ds3_odr_reg {
    uint32_t hz;
    uint8_t value;
};
static const struct st_lsm6ds3_odr_table {
    uint8_t addr[ST_MASK_ID_MAX];
    uint8_t mask[ST_MASK_ID_MAX];
    struct st_lsm6ds3_odr_reg odr_avl[LSM6DS3_ODR_LIST_NUM];
} g_lsm6ds3_odr_table = {
    .addr[ST_MASK_ID_ACCEL]  = LSM6DS3_CTRL1_XL_REG_ADDR,
    .mask[ST_MASK_ID_ACCEL]  = CTRL1_XL_ODR_XL_MASK,
    .addr[ST_MASK_ID_GYRO]   = LSM6DS3_CTRL2_G_REG_ADDR,
    .mask[ST_MASK_ID_GYRO]   = CTRL2_G_ODR_G_MASK,
    .addr[ST_MASK_ID_MOTION] = LSM6DS3_CTRL1_XL_REG_ADDR,
    .mask[ST_MASK_ID_MOTION] = CTRL1_XL_ODR_XL_MASK,
    .odr_avl[0] = { .hz = 13,  .value = LSM6DS3_ODR_13HZ_VAL  },
    .odr_avl[1] = { .hz = 26,  .value = LSM6DS3_ODR_26HZ_VAL  },
    .odr_avl[2] = { .hz = 52,  .value = LSM6DS3_ODR_52HZ_VAL  },
    .odr_avl[3] = { .hz = 104, .value = LSM6DS3_ODR_104HZ_VAL },
    .odr_avl[4] = { .hz = 208, .value = LSM6DS3_ODR_208HZ_VAL },
    .odr_avl[5] = { .hz = 416, .value = LSM6DS3_ODR_416HZ_VAL },
};
static uint16_t sensors_enable = 0;

/***************************************************************************************************
* PUBLIC FUNCTION DEFINITIONS
***************************************************************************************************/

uint8_t LSM6DS3_Init(GPIOINT_IrqCallbackPtr_t cb_func)
{
    /* Initialize device register */
    if (InitSensor() != LSM6DS3_OK) {
        return MOTION_DEVICE_ERROR_DRIVER_NOT_INITIALIZED;
    }

    /* Active function */
#ifdef FEATURE_USE_DOUBLETAP
    ConfigDoubleTap();
#endif
#ifdef FEATURE_USE_TILT
    ConfigTilt();
#endif
#ifdef FEATURE_USE_PEDOMETER
    ConfigPedometer();
#endif
#ifdef FEATURE_USE_FIFO
    ConfigFifo();
#endif

    /* Initialize INT1 */
    if (cb_func != DEF_NULL) {
        GPIO_PinModeSet(BSP_ACC_GYRO_INT1_PORT, BSP_ACC_GYRO_INT1_PIN, gpioModeInput, 0);
        GPIO_IntDisable(1 << BSP_ACC_GYRO_INT1_PIN);
        GPIO_IntConfig(BSP_ACC_GYRO_INT1_PORT, BSP_ACC_GYRO_INT1_PIN, true, false, true);
        GPIOINT_CallbackRegister(BSP_ACC_GYRO_INT1_PIN, cb_func);
    }

    return MOTION_DEVICE_OK;
}

uint8_t LSM6DS3_Prove(void)
{
    return (ProbeSensor() == MOTION_DEVICE_OK) ? MOTION_DEVICE_OK : MOTION_DEVICE_ERROR;
}

uint8_t LSM6DS3_Start(void)
{
    /* Enable ODR */
    SetODRValue(ST_MASK_ID_MOTION, DEFAULT_SENSOR_ODR, false);
#ifdef FEATURE_USE_FIFO
    SetODRValue(ST_MASK_ID_ACCEL, DEFAULT_SENSOR_ODR, false);
    /* Fifo reset */
    SetFifoMode(BYPASS);
    SetFifoMode(CONTINUOUS);
#endif

    /* Enable interrupt */
    if (GPIO_PinInGet(BSP_ACC_GYRO_INT1_PORT, BSP_ACC_GYRO_INT1_PIN) == 1) {
        GPIO_IntSet(1 << BSP_ACC_GYRO_INT1_PIN);
    }
    GPIO_IntEnable(1 << BSP_ACC_GYRO_INT1_PIN);

    return MOTION_DEVICE_OK;
}

uint8_t LSM6DS3_Stop(void)
{
    /* Clear and disable interrupt */
    GPIO_IntClear(1 << BSP_ACC_GYRO_INT1_PIN);
    GPIO_IntDisable(1 << BSP_ACC_GYRO_INT1_PIN);

    /* Power off */
    SetODRValue(ST_MASK_ID_MOTION, 0, false);

    return MOTION_DEVICE_OK;
}

uint8_t LSM6DS3_GetEvent(uint8_t *p_event)
{
    uint8_t event = 0;
    uint8_t evt_reg[4] = {0};

    I2C_ReadReg(LSM6DS3_STATUS_REG_ADDR, 3, &evt_reg[0]);
    I2C_ReadReg(LSM6DS3_TAP_SRC_REG_ADDR, 1, &evt_reg[3]);

    APP_TRACE_DEBUG("\t %-16s = 0x%02X\n", "STATUS_REG", evt_reg[0]);
    APP_TRACE_DEBUG("\t %-16s = 0x%02X\n", "FUNC_SRC_REG", evt_reg[1]);
    APP_TRACE_DEBUG("\t %-16s = 0x%02X\n", "WAKE_UP_SRC_REG", evt_reg[2]);
    APP_TRACE_DEBUG("\t %-16s = 0x%02X\n", "TAP_SRC_REG", evt_reg[3]);

    /* Tilt */
    if (evt_reg[1] & FUNC_SRC_TILT_IA_MASK) {
        APP_TRACE_DEBUG("\t - TILT DETECTION!\n");
        event |= MOTION_EVENT_TILT_DET;
    }
    /* Pedometer */
    if (evt_reg[1] & FUNC_SRC_STEP_CNT_DELTA_IA_MASK) {
        APP_TRACE_DEBUG("\t - STEP_CNT_DELTA!\n");
        event |= MOTION_EVENT_DET_MOTION;
    }
    if (evt_reg[1] & FUNC_SRC_STEP_DETECTED_IA_MASK) {
        APP_TRACE_DEBUG("\t - STEP_DETECTED!\n");
        event |= MOTION_EVENT_DET_MOTION;
    }
    /* Double tab */
    if (evt_reg[3] & TAP_SRC_DOUBLE_TAP_MASK) {
        APP_TRACE_DEBUG("\t - DOUBLE TAP!\n");
        event |= MOTION_EVENT_DOUBLE_TAP_DET;
    }

    *p_event = event;

    return MOTION_DEVICE_OK;
}

uint8_t LSM6DS3_CheckSensor(void)
{
    return (uint8_t)(ProbeSensor());
}

#ifdef FEATURE_USE_PEDOMETER
uint8_t LSM6DS3_StepCountRead(uint32_t *steps)
{
    int8_t err;
    uint8_t step_cnt[2] = {0};

    err = I2C_ReadReg(LSM6DS3_STEP_COUNTER_L_REG_ADDR, 2, &step_cnt[0]);
    if (err < 0) {
        step_cnt[0] = 0;
        step_cnt[1] = 0;
    }

    *steps = (uint32_t)(((uint16_t)(step_cnt[1] << 8) & 0xFF00) | (uint16_t)step_cnt[0]);

    APP_TRACE_DEBUG("%s: step = %ld\n", *steps);

    return err;
}

uint8_t LSM6DS3_StepCountReset(void)
{
    int8_t err;
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL10_C_REG_ADDR, CTRL10_C_PEDO_RST_STEP_MASK, LSM6DS3_REG_DIS_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t Step couldn't be reset. err: %d", err);
        return -LSM6DS3_ERROR;
    }
    return (uint8_t)I2C_WriteRegWithMask(LSM6DS3_CTRL10_C_REG_ADDR, CTRL10_C_PEDO_RST_STEP_MASK, LSM6DS3_REG_EN_BIT);
}
#endif

#ifdef FEATURE_USE_FIFO
uint8_t LSM6DS3_ReadFifo(uint16_t num_bytes, uint8_t *data)
{
    int8_t err;
    uint8_t fifo_status[2] = {0};
    uint16_t read_len = 0, byte_in_pattern;
    uint8_t sip = 1;

    /** read FIFO_STATUS1, FIFO_STATUS2 register */
    err = I2C_ReadReg(LSM6DS3_FIFO_STATUS1_REG_ADDR, 2, fifo_status);
    if (err < 0) {
        return ACCEL_DEVICE_ERROR_I2C_TRANSACTION_FAILED;
    }

    /** check FIFO overun error */
    if (fifo_status[1] & FIFO_STATUS2_FIFO_OVER_RUN_MASK) {
        SetFifoMode(BYPASS);
        SetFifoMode(CONTINUOUS);

        APP_TRACE_DEBUG("\t data fifo overrun.\n");
        return ACCEL_DEVICE_ERROR_FIFO_OVER_RUN;
    }

    read_len = ((fifo_status[1] & FIFO_STATUS2_DIFF_FIFO_MASK) << 8) | fifo_status[0];
    read_len *= FIFO_BYTE_FOR_CHANNEL;

    /** byte size of one pattern */
    byte_in_pattern = sip * FIFO_ELEMENT_LEN_BYTE;

    read_len = (read_len / byte_in_pattern) * byte_in_pattern;
    if (read_len == 0) {
        return ACCEL_DEVICE_ERROR_I2C_TRANSACTION_FAILED;
    }

    /* Read fifo data */
    err = I2C_ReadReg(LSM6DS3_FIFO_DATA_OUT_L_REG_ADDR, read_len, &data[0]);
    if (err < 0) {
        APP_TRACE_DEBUG("ERROR: fifo_data\n");
        return ACCEL_DEVICE_ERROR_I2C_TRANSACTION_FAILED;
    }

    return ACCEL_DEVICE_OK;
}
#endif

/***************************************************************************************************
* PRIVATE FUNCTION DEFINITIONS
***************************************************************************************************/

static int8_t I2C_WriteReg(uint8_t reg_addr, uint8_t len, uint8_t *data)
{
	return (int8_t)i2cm_writeData(i2c0_id, LSM6DS3_I2C_BUS_ADDRESS, reg_addr, data, len);
}

static int8_t I2C_ReadReg(uint8_t reg_addr, uint16_t len, uint8_t *data)
{
	return (int8_t)i2cm_readData(i2c0_id, LSM6DS3_I2C_BUS_ADDRESS, reg_addr, data, len);
}

static int8_t I2C_WriteRegWithMask(uint8_t reg_addr, uint8_t mask, uint8_t data)
{
    int8_t err;
    uint8_t new_data = 0x00, old_data = 0x00;

    err = I2C_ReadReg(reg_addr, 1, &old_data);
    if (err < 0) {
        return err;
    }

    new_data = ((old_data & (~mask)) | ((data << SYSOS_CalcFFS(mask)) & mask));

    if (new_data == old_data) {
        return 1;
    }

    return I2C_WriteReg(reg_addr, 1, &new_data);
}

static int8_t ProbeSensor(void)
{
    uint8_t wai;

    if (I2C_ReadReg(LSM6DS3_WAI_REG_ADDR, 1, &wai) < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_DEVICE_ID_MISMATCH));
        return LSM6DS3_ERROR_DEVICE_ID_MISMATCH;
    }

    if (wai != LSM6DS3_WAI_VALUE) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    return MOTION_DEVICE_OK;
}

static int8_t InitSensor(void)
{
    static bool is_initialized = false;
    int8_t err;

    if (is_initialized == true) {
        return MOTION_DEVICE_OK;
    }

    err = ProbeSensor();
    if (err != MOTION_DEVICE_OK) {
        return err;
    }

    /* Software reset */
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL1_XL_REG_ADDR, CTRL1_XL_ODR_XL_MASK, 6);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL3_C_REG_ADDR, CTRL3_C_SW_RESET_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }
    while (DEF_TRUE) {
        uint8_t value = 0;
        err = I2C_ReadReg(LSM6DS3_CTRL3_C_REG_ADDR, 1, &value);
        if ((err == 0) && ((value & CTRL3_C_SW_RESET_MASK) == 0x0)) {
            break;
        }
    }

    /* Disable odr */
    SetODRValue(ST_MASK_ID_ACCEL, 0, true);

    /** Latch interrupts */
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_CFG_REG_ADDR, TAP_CFG_LIR_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /** Enable BDU for sensors data */
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL3_C_REG_ADDR, CTRL3_C_BDU_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    err = I2C_WriteRegWithMask(LSM6DS3_CTRL7_G_REG_ADDR, CTRL7_G_ROUNDING_STATUS_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /** Disable high-performance operating mode because of power consumption */
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL6_C_REG_ADDR, CTRL6_C_XL_HM_MODE_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL7_G_REG_ADDR, CTRL7_G_G_HM_MODE_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /** Enable FIFO threshold level use */
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL4_C_REG_ADDR, CTRL4_C__STOP_ON_FTH_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /** Enable embedded functionalities */
    err = I2C_WriteRegWithMask(LSM6DS3_CTRL10_C_REG_ADDR, CTRL10_C_FUNC_EN_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    is_initialized = true;      /* Initialize done */

    return LSM6DS3_OK;
}

static int8_t SetODRValue(enum st_mask_id id, uint8_t odr, bool force)
{
    int8_t err;
    uint8_t reg_value;
    uint8_t i;
    bool scan_odr = true;

    if (odr == 0) {
        if (force) {
            scan_odr = false;
        } else {
            if ((sensors_enable & ~BIT(id)) != 0) {
                sensors_enable &= ~BIT(id);
                return 0;
            }
        }
    }

    /* Get odr value from table */
    if (scan_odr) {
        for (i = 0; i < LSM6DS3_ODR_LIST_NUM; i++) {
            if (g_lsm6ds3_odr_table.odr_avl[i].hz == odr) {
                break;
            }
        }

        reg_value = g_lsm6ds3_odr_table.odr_avl[i].value;
    } else {
        reg_value = LSM6DS3_ODR_POWER_OFF_VAL;
    }

    /* Write ODR to register */
    err = I2C_WriteRegWithMask(g_lsm6ds3_odr_table.addr[id],
                               g_lsm6ds3_odr_table.mask[id],
                               reg_value);
    if (err < 0) {
        return err;
    }

    if (force) {
        sensors_enable = 0;
    } else if (reg_value == LSM6DS3_ODR_POWER_OFF_VAL) {
        sensors_enable &= -BIT(id);
    } else {
        sensors_enable |= BIT(id);
    }

    return LSM6DS3_OK;
}

#ifdef FEATURE_USE_DOUBLETAP
static int8_t ConfigDoubleTap(void)
{
    int8_t err;

    /** Enable tap detection on X, Y, Z axis */
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_CFG_REG_ADDR, TAP_CFG_TAP_X_EN_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_CFG_REG_ADDR, TAP_CFG_TAP_Y_EN_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_CFG_REG_ADDR, TAP_CFG_TAP_Z_EN_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    /** Set tap threshold              */
    /*  312.5mg = 5(0x05) * FS_XL / 2^5 */
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_THS_6D_REG_ADDR, TAP_THS_6D_TAP_THS_MASK, TAP_THS_DEFAULT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    /** Set duration, quiet and shock time windows */
    /*  79.6 = 4 / ODR_XL(52)                      */
    err = I2C_WriteRegWithMask(LSM6DS3_INT_DUR2_REG_ADDR, INT_DUR2_SHOCK_MASK, MAX_DUR_OVRTHS);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }
    /*  38.5 = 2 / ODR_XL(52)                      */
    err = I2C_WriteRegWithMask(LSM6DS3_INT_DUR2_REG_ADDR, INT_DUR2_QUIET_MASK, QUIET_TIME_AFTER_TAP);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }
    /*  615.4 = 1 * 32 / ODR_XL(52)                */
    err = I2C_WriteRegWithMask(LSM6DS3_INT_DUR2_REG_ADDR, INT_DUR2_DUR_MASK, DUR_MAX_TIME_GAP);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    /** Single & Double tap enabled (SINGLE_DOUBLE_TAP = 1) */
    err = I2C_WriteRegWithMask(LSM6DS3_WAKE_UP_THS_REG_ADDR, WAKE_UP_THS_SINGLE_DOUBLE_TAP_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    /** Double tap interrupt driven to INT1 pin */
    err = I2C_WriteRegWithMask(LSM6DS3_MD1_CFG_REG_ADDR, MD1_CFG_INT1_DOUBLE_TAP_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    return LSM6DS3_OK;
}
#endif

#ifdef FEATURE_USE_TILT
static int8_t ConfigTilt(void)
{
    int8_t err;

    /** Enable tilt detection */
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_CFG_REG_ADDR, TAP_CFG_TILT_EN_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    /** Tilt detector interrupt driven to INT1 pin */
    err = I2C_WriteRegWithMask(LSM6DS3_MD1_CFG_REG_ADDR, MD1_CFG_INT1_TILT_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    return LSM6DS3_OK;
}
#endif

#ifdef FEATURE_USE_PEDOMETER
static int8_t ConfigPedometer(void)
{
    int8_t err;

    /* Enable pedometer algorithm */
    err = I2C_WriteRegWithMask(LSM6DS3_TAP_CFG_REG_ADDR, TAP_CFG_PEDO_EN_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /* Step Detector interrupt driven to INT1 pin */
    err = I2C_WriteRegWithMask(LSM6DS3_INT1_CTRL_REG_ADDR, INT1_CTRL_INT1_STEP_DETECTOR_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    return LSM6DS3_OK;
}
#endif

#ifdef FEATURE_USE_FIFO
static uint8_t ConfigFifo(void)
{
    int8_t err;

    /* Set fifo watermark */
    SetFifoWatermark(NUM_OF_FIFO_SAMPLE_DATA + 1);

    /* Accelerometer FIFO decimation setting: No decimation */
    err = I2C_WriteRegWithMask(LSM6DS3_FIFO_CTRL3_REG_ADDR, FIFO_CTRL3_DEC_FIFO_XL_MASK, DEC_NO_DECIMATION);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /* Set fifo interrupt */
    SetFifoIntr();

    /* FIFO ODR selection, setting FIFO_MODE also */
    err = I2C_WriteRegWithMask(LSM6DS3_FIFO_CTRL5_REG_ADDR, FIFO_CTRL5_ODR_FIFO_MASK, FIFO_ODR_52HZ);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    return MOTION_DEVICE_OK;
}

static int8_t SetFifoMode(enum fifo_mode fm)
{
    int8_t err;
    uint8_t fifo_mode;

    switch (fm) {
        case BYPASS:
            fifo_mode = FIFO_MODE_BYPASS;
            break;
        case FIFO:
            fifo_mode = FIFO_MODE_FIFO;
            break;
        case CONTINUOUS:
            fifo_mode = FIFO_MODE_CONTINUOUS;
            break;
        default:
            return -LSM6DS3_ERROR;
    }

    err = I2C_WriteRegWithMask(LSM6DS3_FIFO_CTRL5_REG_ADDR, FIFO_CTRL5_FIFO_MODE_MASK, fifo_mode);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    return 0;
}

static int8_t SetFifoWatermark(uint32_t pattern)
{
    int8_t err;
    uint8_t reg_value = 0;
    uint16_t fifo_watermark;
    uint32_t fifo_len, sip = 0, min_pattern = 0;    /** sip: samples in pattern */

    min_pattern = pattern;
    sip = 1;

    min_pattern = MIN(min_pattern, ((uint32_t)FIFO_MAX_THRESHOLD / sip));

    fifo_len = min_pattern * sip * FIFO_ELEMENT_LEN_BYTE;
    fifo_watermark = (fifo_len / 2);

    APP_TRACE_DEBUG("%s: min_patter=%d, fifo_len=%d, fifo_watermark=%d\n", __func__,
                    min_pattern, fifo_len, fifo_watermark);

    if (fifo_watermark < (FIFO_ELEMENT_LEN_BYTE / 2)) {
        fifo_watermark = FIFO_ELEMENT_LEN_BYTE / 2;
    }

    err = I2C_ReadReg(LSM6DS3_FIFO_CTRL2_REG_ADDR, 1, &reg_value);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    fifo_watermark = (fifo_watermark & FIFO_CTRL_FTH_MASK) |
                     ((reg_value & ~FIFO_CTRL_FTH_MASK) << 8);

    APP_TRACE_DEBUG("%s: fifo_watermark=0x%X\n", __func__, fifo_watermark);

    err = I2C_WriteReg(LSM6DS3_FIFO_CTRL1_REG_ADDR, 2, (uint8_t *)&fifo_watermark);
    if (err < 0) {
        return -LSM6DS3_ERROR;
    }

    return 0;
}

static int8_t SetFifoIntr(void)
{
    int8_t err;

    /* FIFO full flag interrupt enable on INT2 */
    err = I2C_WriteRegWithMask(LSM6DS3_INT2_CTRL_REG_ADDR, INT2_CTRL_INT2_FULL_FLAG_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /* FIFO overrun interrupt enable on INT2 */
    err = I2C_WriteRegWithMask(LSM6DS3_INT2_CTRL_REG_ADDR, INT2_CTRL_INT2_FIFO_OVR_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    /* FIFO threshold interrupt enable on INT2 */
    err = I2C_WriteRegWithMask(LSM6DS3_INT2_CTRL_REG_ADDR, INT2_CTRL_INT2_FTH_MASK, LSM6DS3_REG_EN_BIT);
    if (err < 0) {
        APP_TRACE_DEBUG("\t %s\n", STRINGIZE(LSM6DS3_ERROR_I2C_TRANSACTION_FAILED));
        return LSM6DS3_ERROR_I2C_TRANSACTION_FAILED;
    }

    return LSM6DS3_OK;
}
#endif

