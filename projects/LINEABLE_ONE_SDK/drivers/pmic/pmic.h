/*******************************************************************************
 * @copyright { Copyright 2019 Lineable (C) by All Rights Reserved.
 *  Unauthorized redistribution of this source code, in whole or part,
 *  without the express written permission of Lineable is strictly prohibited.
 *  }
*******************************************************************************/

/**
 *
 * @file pmic.h
 * @brief Device independent PMIC interface.
*/

#ifndef DRIVERS_PMIC_PMIC_H_
#define DRIVERS_PMIC_PMIC_H_

#include <stdint.h>
#include <stdbool.h>
#include <drivers/peripheral/i2c/micrium_i2c.h>

/**************************************************************************//**
* @name PMIC error codes
******************************************************************************/
#define PMIC_OK                             0x0000  /**< No errors                                        */
#define PMIC_ERROR                          0x0001  /**<                                                  */
#define PMIC_ERROR_I2C_TRANSACTION_FAILED   0x0002  /**< I2C transaction failed                           */
#define PMIC_ERROR_DEVICE_ID_MISMATCH       0x0003  /**< The device ID does not match the expected value  */


/**
 * PMIC LED available colors
 */
typedef enum {
	OFF,
	WHITE,
	AMBER,
	BLUE,
	CYAN,
	GREEN,
	MAGENTA,
	RED,
} pmic_led_colors_t;

/*******************************************************************************
* @name Fuelgauge public interface
*******************************************************************************/
/**@{*/
uint32_t PMIC_CheckChargerState(bool *state);


/**
 * Initialize PMIC module and underlying hardware
 *
 * @return PMIC_OK on success and PMIC_ERROR_XXXX otherwise
 */
uint32_t PMIC_Init(I2CM_ID interface);
uint32_t PMIC_Start(void);
uint16_t PMIC_ReadCharState(uint8_t *ch_state);

void PMICLED_Tap(void);
void PMICLED_SetColor(pmic_led_colors_t color);

/**@}*/
#endif /* DRIVERS_PMIC_PMIC_H_ */
