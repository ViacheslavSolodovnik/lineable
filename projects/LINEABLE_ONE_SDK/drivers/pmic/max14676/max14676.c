/*******************************************************************************
 * @file max14676.c
 * @brief Driver for the MAXIM PMIC max14676d
 * @version 0.0
 ******************************************************************************/

#include <stdlib.h>
#include <stdarg.h>

#include <rtos_app.h>
#include <gpiointerrupt.h>
#include <timer.h>

#include <drivers/pmic/pmic.h>
#include <drivers/fuelgauge/fuelgauge.h>

#include "max14676.h"
/*******************************************************************************
*                                 DEBUG OPTION
*******************************************************************************/

#undef  __MODULE__
#define __MODULE__ "PMIC"
#undef  DBG_MODULE
#define DBG_MODULE 0
#define PMIC_DEBUG 0

#if (configUSE_PMIC == PMIC_DEVICE_MAX14676)
/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/

/*******************************************************************************
*                                  DEFINES
*******************************************************************************/

#if (LINEABLE_BOARD_TYPE == LINEABLE_ONE)
#define RED_LED_LOC     MAX14676_REG_LED_0
#define GREEN_LED_LOC   MAX14676_REG_LED_1
#define BLUE_LED_LOC    MAX14676_REG_LED_2

#define LED_RED_IDX     0
#define LED_GREEN_IDX   1
#define LED_BLUE_IDX    2
#else
#define RED_LED_LOC     MAX14676_REG_LED_2
#define GREEN_LED_LOC   MAX14676_REG_LED_1
#define BLUE_LED_LOC    MAX14676_REG_LED_0

#define LED_RED_IDX     2
#define LED_GREEN_IDX   1
#define LED_BLUE_IDX    0
#endif

#define LED_STATE_OFF   0
#define LED_STATE_ON    1

#define LED_OFF_BLINK   1000   /* 1 sec */

#define LED_OFF_TAP     2000   /* 2 sec */

#define PMIC_EVENT_FLAG_EVT_CHARGING_LED  ((OS_FLAGS)(1<<0))
#define PMIC_EVENT_FLAG_EVT_TAP_LED       ((OS_FLAGS)(1<<1))

/*******************************************************************************
*                                  MACROS
*******************************************************************************/

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/

static uint8_t PmicDeviceId;
/* Task */
static OS_TCB       g_pmic_task;
static CPU_STK      g_pmic_task_stk[PMIC_APP_TASK_STK_SIZE];
/* Event Flag */
static OS_FLAG_GRP  g_pmic_event_flags;
/* for LED Blink */
static TimerEvent_t g_led_blink_timer;
static uint32_t     g_led_blink_num = 0;
static uint32_t     g_led_blink_on_duty = 0;
static bool         g_led_blink_force_on = false;

static TimerEvent_t g_led_tap_timer;

static I2CM_ID  g_i2c_interface;

/*******************************************************************************
*                         PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/
static int32_t  MAX14676x_Read1Byte(uint8_t reg, uint8_t *data);
static int32_t  MAX14676x_ReadBytes(uint8_t reg, uint8_t *data, uint16_t len);
static int32_t  MAX14676x_Write1Byte(uint8_t reg, uint8_t data);
static int32_t  MAX14676x_WriteBytes(uint8_t reg, uint8_t *data, uint16_t len);
static uint32_t MAX14676x_ConfigCharge(void);
static uint32_t MAX14676x_ConfigPowerUp(void);
static uint32_t MAX14676x_ConfigInit(void);
static void     MAX14676x_IntrCb(uint8_t pin);
static void     MAX14676x_InitIntr(GPIOINT_IrqCallbackPtr_t callbackPtr);
static void     MAX14676x_SetLEDStepCurrent(uint8_t w_data);
static void     MAX14676x_SetLEDColor(uint8_t red, uint8_t green, uint8_t blue);
static void     MAX14676x_SetLEDControl(uint8_t red, uint8_t green, uint8_t blue);
static void     MAX14676x_BlinkLED(void);
static uint32_t MAX14676x_EnableLDO(void);

/*******************************************************************************
*                              PRIVATE FUNCTIONS
*******************************************************************************/

/**
 * @brief Generic Reading function with I2C
 * @param reg MAX14676x register address
 * @param data pointer to buffer for received data
 * @return Returns PMIC_OK on success, PMIC_ERROR_I2C_TRANSACTION_FAILED otherwise
 */
static int32_t MAX14676x_Read1Byte(uint8_t reg, uint8_t *data)
{
    return i2cm_readData(g_i2c_interface, PMIC_I2C_BUS_ADDRESS, reg, data, 1) == i2cTransferDone ?
            PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/**
 * @brief Generic Reading function with I2C
 * @param reg MAX14676x register address
 * @param data pointer to buffer for received data
 * @param len size of data to read
 * @return Returns PMIC_OK on success, PMIC_ERROR_I2C_TRANSACTION_FAILED otherwise
 */
static int32_t MAX14676x_ReadBytes(uint8_t reg, uint8_t *data, uint16_t len)
{
    return i2cm_readData(g_i2c_interface, PMIC_I2C_BUS_ADDRESS, reg, data, len) == i2cTransferDone ?
            PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/**
 * @brief Generic writing function with I2C
 * @param reg MAX14676x register address
 * @param rec_buf pointer to buffer for data to be written
 * @return Returns PMIC_OK on success, PMIC_ERROR_I2C_TRANSACTION_FAILED otherwise
 */
static int32_t MAX14676x_Write1Byte(uint8_t reg, uint8_t data)
{
    return i2cm_writeData(g_i2c_interface, PMIC_I2C_BUS_ADDRESS, reg, &data, 1) == i2cTransferDone ?
                PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/**
 * @brief Generic writing function with I2C
 * @param reg MAX14676x register address
 * @param rec_buf pointer to buffer for data to be written
 * @return Returns PMIC_OK on success, PMIC_ERROR_I2C_TRANSACTION_FAILED otherwise
 */
static int32_t MAX14676x_WriteBytes(uint8_t reg, uint8_t *data, uint16_t len)
{
    return i2cm_writeData(g_i2c_interface, PMIC_I2C_BUS_ADDRESS, reg, data, len) == i2cTransferDone ?
                PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/*******************************************************************************
 * @brief
 *    Charge configuration of max14676
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t MAX14676x_ConfigCharge(void)
{
    uint8_t status_b_reg = 0;
    uint8_t chg_cntl_a_reg = 0;
    uint8_t chg_cntl_b_reg = 0;

    APP_TRACE_DEBUG("%s:\n", __func__);

    /* Check charger connectivity */
    if (MAX14676x_Read1Byte(MAX14676_REG_STATUS_B, &status_b_reg) != PMIC_OK) {
        APP_TRACE_DEBUG("\t- Read Error: REG_STATUS_B\n");
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read REG_STATUS_B: 0x%02X\n", status_b_reg);

    if (status_b_reg & USB_OK_INT_M) {
        APP_TRACE_DEBUG("\t- Charger connected!\n");

        /* Fast-Charge Current Setting
        *  : fast-charge current must be set at least 10mA higher then charge done
        *  - MAX14676D = 350mA
        *  - MAX14676  = 200mA
        */
        chg_cntl_a_reg = (PmicDeviceId == PMIC_DEVICE_ID_MAX14676D) ? (IF_CFG_7 << 2) : (IF_CFG_4 << 2);
        APP_TRACE_DEBUG("\t- Write REG_CHG_CNTL_A: 0x%02X\n", chg_cntl_a_reg);
        if (MAX14676x_Write1Byte(MAX14676_REG_CHG_CNTL_A, chg_cntl_a_reg) != PMIC_OK) {
            APP_TRACE_DEBUG("\t- Write Error: REG_CHG_CNTL_A\n");
            return PMIC_ERROR;
        }

        /* Set enable charging */
        chg_cntl_b_reg = CHG_ENABLE;    /* ThermEn:0=Disable, ChgEn:1=Enable, ChgDone:000=7.5mA */
    } else {
        APP_TRACE_DEBUG("\t- Charger dis-connected!\n");

        /* Set disable charging */
        chg_cntl_b_reg = CHG_DISABLE;   /* ThermEn:0=Disable, ChgEn:0=Disable, ChgDone:000=7.5mA */
    }

    /* Write ChgEn bit */
    APP_TRACE_DEBUG("\t- Write REG_CHG_CNTL_B: 0x%02X\n", chg_cntl_b_reg);
    if (MAX14676x_Write1Byte(MAX14676_REG_CHG_CNTL_B, chg_cntl_b_reg) != PMIC_OK) {
        APP_TRACE_DEBUG("\t- Write Error: REG_CHG_CNTL_B\n");
        return PMIC_ERROR;
    }

    if (chg_cntl_b_reg == CHG_ENABLE) {
        PMICLED_Blink(LED__ON, 2, 0);
    } else {
        PMICLED_Blink(LED_OFF, 0, 0);
    }

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Configure the power
 *
 * @param[in] enable, voltage
 *    boost enable/disable and, which holds the configuration parameters
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t MAX14676x_ConfigPowerUp(void)
{
    uint8_t r_data = 0;

    APP_TRACE_DEBUG("%s:\n", __func__);

    if (MAX14676x_Read1Byte(MAX14676_REG_PWR_CFG, &r_data) != PMIC_OK) {
        APP_TRACE_DEBUG("\t- Read Error: REG_PWR_CFG\n");
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read REG_PWR_CFG = 0x%X\n", r_data);

    r_data |= 0x80; /* Stay On : This bit is used to ensure that the processor booted correctly */

    APP_TRACE_DEBUG("\t- Write REG_PWR_CFG = 0x%X\n", r_data);
    if (MAX14676x_Write1Byte(MAX14676_REG_PWR_CFG, r_data) != PMIC_OK) {
        APP_TRACE_DEBUG("\t- Error: REG_PWR_CFG\n");
        return PMIC_ERROR;
    }

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Configuration for max14676
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t MAX14676x_ConfigInit(void)
{
    uint8_t dummy[2] = {0};

    /* Configure charger registers */
    if (MAX14676x_ConfigCharge() != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* Configure interrupts
     *  - Use only UsbOkInt
     */
    dummy[0] = 0x00;
    dummy[1] = USB_OK_INT_M;
    if (MAX14676x_WriteBytes(MAX14676_REG_INT_MASK_A, &dummy[0], 2) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* Clear Interrupts by read register */
    if (MAX14676x_ReadBytes(MAX14676_REG_INT_A, &dummy[0], 2) != PMIC_OK) {
        return PMIC_ERROR;
    }

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Interrupt callback
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static void MAX14676x_IntrCb(uint8_t pin)
{
    if (pin == BSP_PMIC_INT_PIN) {
        /* do nothing */
    }
}

static void MAX14676x_InitIntr(GPIOINT_IrqCallbackPtr_t callbackPtr)
{
    GPIO_IntDisable(1 << BSP_PMIC_INT_PIN);

    GPIO_IntConfig(BSP_PMIC_INT_PORT, BSP_PMIC_INT_PIN, false, true, true);

    GPIOINT_CallbackRegister(BSP_PMIC_INT_PIN, callbackPtr);

    if (GPIO_PinInGet(BSP_PMIC_INT_PORT, BSP_PMIC_INT_PIN) == 0) {
        GPIO_IntSet(1 << BSP_PMIC_INT_PIN);
    }

    GPIO_IntEnable(1 << BSP_PMIC_INT_PIN);
}

/*******************************************************************************
 * @brief
 *    LED Step current configuration of max14676
 *
 * @return
 *
 ******************************************************************************/
static void MAX14676x_SetLEDStepCurrent(uint8_t w_data)
{
    if (w_data > MAX14676_ISTEP_1_2mA) {
        w_data = MAX14676_ISTEP_1_2mA;
    }

    MAX14676x_Write1Byte(MAX14676_REG_LED_CFG, w_data);
    return;
}

/*******************************************************************************
 * @brief
 *    change only color value, not on off
 *
 * @return
 *    input range : 0 ~ 0x18(24)
 ******************************************************************************/
static void MAX14676x_SetLEDColor(uint8_t red, uint8_t green, uint8_t blue)
{
    uint8_t reg_data;

    if (PMIC_OK == MAX14676x_Read1Byte(RED_LED_LOC, &reg_data)) {
        printf("reg_data = %d\n", reg_data);
        red = (uint8_t)(((int)red * MAX14676_LEDX_I_MAX) / 0xFF);
        if (red > MAX14676_LEDX_I_MAX) {
            red = MAX14676_LEDX_I_MAX;
        }

        if ((red << MAX14676_LEDX_I_POS) != (reg_data & MAX14676_LEDX_I_MASK)) {
            // copy current control values
            red |= (reg_data & ~MAX14676_LEDX_I_MASK);
            APP_TRACE_DEBUG("%s: 0x%X\r\n", STRINGIZE(RED_LED_LOC), red);
            MAX14676x_Write1Byte(RED_LED_LOC, red);
        }
    }

    if (PMIC_OK == MAX14676x_Read1Byte(GREEN_LED_LOC, &reg_data)) {
        green = (uint8_t)(((int)green * MAX14676_LEDX_I_MAX) / 0xFF);
        if (green > MAX14676_LEDX_I_MAX) {
            green = MAX14676_LEDX_I_MAX;
        }

        if ((green << MAX14676_LEDX_I_POS) != (reg_data & MAX14676_LEDX_I_MASK)) {
            // copy current control values
            green |= (reg_data & ~MAX14676_LEDX_I_MASK);
            APP_TRACE_DEBUG("%s: 0x%X\r\n", STRINGIZE(GREEN_LED_LOC), green);
            MAX14676x_Write1Byte(GREEN_LED_LOC, green);
        }
    }

    if (PMIC_OK == MAX14676x_Read1Byte(BLUE_LED_LOC, &reg_data)) {
        blue = (uint8_t)(((int)blue * MAX14676_LEDX_I_MAX) / 0xFF);
        if (blue > MAX14676_LEDX_I_MAX) {
            blue = MAX14676_LEDX_I_MAX;
        }

        if ((blue << MAX14676_LEDX_I_POS) != (reg_data & MAX14676_LEDX_I_MASK)) {
            // copy current control values
            blue |= (reg_data & ~MAX14676_LEDX_I_MASK);
            APP_TRACE_DEBUG("%s: 0x%X\r\n", STRINGIZE(BLUE_LED_LOC), blue);
            MAX14676x_Write1Byte(BLUE_LED_LOC, blue);
        }
    }
}

/*******************************************************************************
 * @brief
 *  indicattor control
 *               0  : off(manual mode),
 *               1  : on (manual mode)
 *               others  : controlled by PMIC charger
 *
 * @return
 ******************************************************************************/
static void MAX14676x_SetLEDControl(uint8_t red, uint8_t green, uint8_t blue)
{
    uint8_t reg_data;
    if (PMIC_OK == MAX14676x_Read1Byte(RED_LED_LOC, &reg_data)) {
        if (red > MAX14676_LEDX_CFG_ON) {
            red = MAX14676_LEDX_CFG_CHARGER;
        }

        if ((red << MAX14676_LEDX_CFG_POS) != (reg_data & MAX14676_LEDX_CFG_MASK)) {
            red = (red & MAX14676_LEDX_CFG_MASK) | (reg_data & ~MAX14676_LEDX_CFG_MASK);
            APP_TRACE_DEBUG("%s: 0x%X\r\n", STRINGIZE(RED_LED_LOC), red);
            MAX14676x_Write1Byte(RED_LED_LOC, red);
        }
    }

    if (PMIC_OK == MAX14676x_Read1Byte(GREEN_LED_LOC, &reg_data)) {
        if (green > MAX14676_LEDX_CFG_ON) {
            green = MAX14676_LEDX_CFG_CHARGER;
        }

        if ((green << MAX14676_LEDX_CFG_POS) != (reg_data & MAX14676_LEDX_CFG_MASK)) {
            green = (green & MAX14676_LEDX_CFG_MASK) | (reg_data & ~MAX14676_LEDX_CFG_MASK);
            APP_TRACE_DEBUG("%s: 0x%X\r\n", STRINGIZE(GREEN_LED_LOC), green);
            MAX14676x_Write1Byte(GREEN_LED_LOC, green);
        }
    }

    if (PMIC_OK == MAX14676x_Read1Byte(BLUE_LED_LOC, &reg_data)) {
        if (blue > MAX14676_LEDX_CFG_ON) {
            blue = MAX14676_LEDX_CFG_CHARGER;
        }

        if ((blue << MAX14676_LEDX_CFG_POS) != (reg_data & MAX14676_LEDX_CFG_MASK)) {
            blue = (blue & MAX14676_LEDX_CFG_MASK) | (reg_data & ~MAX14676_LEDX_CFG_MASK);
            APP_TRACE_DEBUG("%s: 0x%X\r\n", STRINGIZE(BLUE_LED_LOC), blue);
            MAX14676x_Write1Byte(BLUE_LED_LOC, blue);
        }
    }
}

static void MAX14676x_BlinkLED(void)
{
    RTOS_ERR err;

    TimerStop(&g_led_blink_timer);                  /* Stop timer */

    OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
               (OS_FLAGS     )PMIC_EVENT_FLAG_EVT_CHARGING_LED,
               (OS_OPT       )OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
}

static void MAX14676x_TapLED(void)
{
    RTOS_ERR err;

    TimerStop(&g_led_tap_timer);                  /* Stop timer */

    OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
               (OS_FLAGS     )PMIC_EVENT_FLAG_EVT_TAP_LED,
               (OS_OPT       )OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);

}

/*******************************************************************************
 * @brief
 *    LDO enable for MAX14676 and MAX14676A
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static uint32_t MAX14676x_EnableLDO(void)
{
#if 0   /* Only need MAX14676A */
    APP_TRACE_DEBUG("%s: Write REG_LDO_CFG = 0x%X\n", __func__, LDO_ENABLED);
    if (MAX14676x_Write1Byte(MAX14676_REG_LDO_CFG, LDO_ENABLED) != PMIC_OK) {
        APP_TRACE_DEBUG("%s: Write Error REG_LDO_CFG\n", __func__);
        return PMIC_ERROR;
    }
#endif

    return PMIC_OK;
}

static void MAX14676x_Task(void *p_arg)
{
    RTOS_ERR err;
    OS_FLAGS event = 0;
    static uint8_t curr_led_state = LED_STATE_OFF;
    uint16_t level = 0;
    uint32_t sec;
    bool is_battery_full = false;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON) {
        event = OSFlagPend((OS_FLAG_GRP *)&g_pmic_event_flags,
                           (OS_FLAGS     )PMIC_EVENT_FLAG_EVT_CHARGING_LED
                                          + PMIC_EVENT_FLAG_EVT_TAP_LED,
                           (OS_TICK      )0,
                           (OS_OPT       )OS_OPT_PEND_BLOCKING
                                          + OS_OPT_PEND_FLAG_SET_ANY
                                          + OS_OPT_PEND_FLAG_CONSUME,
                           (CPU_TS      *)NULL,
                           (RTOS_ERR    *)&err);

        if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
            APP_TRACE_DEBUG("%s - ERROR: OSFlagPend\n", __func__);
            continue;
        }

        /*------------------------------------------------------------------------------------------
         *  Charging LED BLINK Event
         -----------------------------------------------------------------------------------------*/
        if (event & PMIC_EVENT_FLAG_EVT_CHARGING_LED) {
            APP_TRACE_DEBUG("PMIC_EVENT_FLAG_EVT_CHARGING_LED:\n");

            if (curr_led_state == LED_STATE_ON) {
                APP_TRACE_DEBUG("\t- off\n");
                if (is_battery_full == false) {
                    PMICLED_SetColor(OFF);
                }
                sec = LED_OFF_BLINK;                        /* Set off duty */
                g_led_blink_num--;
            } else {
                is_battery_full = false;
                FUELGAUGE_GetSoc(&level);                 /* Get current battery level */
                APP_TRACE_DEBUG("\t- on (%d)\n", level);
                if (level <= 25) {                          /* Turn on LED */
                    PMICLED_SetColor(RED);
                } else if (level <= 75) {
                    PMICLED_SetColor(AMBER);
                } else if (level < 100) {
                	PMICLED_SetColor(GREEN);
                } else {
                	PMICLED_SetColor(BLUE);
                    is_battery_full = true;
                }
                sec = g_led_blink_on_duty;                  /* Set on duty */
            }

            curr_led_state ^= 1;

            if ((g_led_blink_num > 0) || (g_led_blink_force_on == true)) {
                APP_TRACE_DEBUG("\t- re-start timer\n");
                TimerSetValue(&g_led_blink_timer, sec);     /* Re-start timer */
                TimerStart(&g_led_blink_timer);
            }
        }

        /*------------------------------------------------------------------------------------------
         *  TAP LED Event
         -----------------------------------------------------------------------------------------*/
        if (event & PMIC_EVENT_FLAG_EVT_TAP_LED) {
        	PMICLED_SetColor(OFF);
        }
    }
}

/*******************************************************************************
*                              PUBLIC FUNCTIONS
*******************************************************************************/

/*******************************************************************************
 * @brief
 *    Configure the boost
 *
 * @param[in] enable, voltage
 *    boost enable/disable and, which holds the configuration parameters
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint32_t PMIC_ConfigBoost(uint8_t enable, uint8_t voltage)
{
    if (MAX14676x_Write1Byte(MAX14676_REG_BST_CFG, enable | voltage) != PMIC_OK) {
        return PMIC_ERROR;
    }

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Initializes the pmic (max14676d) chip
 *
 * @param[out] deviceId
 *    The device ID of the connected chip
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint32_t PMIC_Init(I2CM_ID interface)
{
    RTOS_ERR err;

    APP_TRACE_DEBUG("%s:\n", __func__);

    g_i2c_interface = interface;
    /* Read device ID to determine if we have a MAX14676D connected */
    if (MAX14676x_Read1Byte(MAX14676_REG_DEVID, &PmicDeviceId) != PMIC_OK) {
        APP_TRACE_DEBUG("\t- ERROR: Read device ID\n");
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- REG_DEVID = 0x%x\n", PmicDeviceId);

    /* Initialize PMIC */
    switch (PmicDeviceId) {
    case PMIC_DEVICE_ID_MAX14676D:
        APP_TRACE_DEBUG("\t- Device ID : MAX14676D\n");
        break;
    case PMIC_DEVICE_ID_MAX14676:
        APP_TRACE_DEBUG("\t- Device ID : MAX14676\n");

        /* Set StayOn */
        if (MAX14676x_ConfigPowerUp() != PMIC_OK) {
            APP_TRACE_DEBUG("\t- ERROR: Config Power-UP\n");
            return PMIC_ERROR;
        }

        if (MAX14676x_EnableLDO() != PMIC_OK) {
            APP_TRACE_DEBUG("\t- ERROR: Enable LDO\n");
            return PMIC_ERROR;
        }
        break;
    default:
        APP_TRACE_DEBUG("\t- ERROR: Invalid PMIC Device ID = 0x%x\n", PmicDeviceId);
        return PMIC_ERROR_DEVICE_ID_MISMATCH;
    }

#if PMIC_DEBUG
    {
        uint8_t reg_status[3] = {0};

        /* Read StatusA, StatusB, StatusC register */
        if (MAX14676x_ReadBytes(MAX14676_REG_STATUS_A, &reg_status[0], 3) != PMIC_OK) {
            APP_TRACE_DEBUG("\t- ERROR: Read StatusB, StatusC reg\n");
            return PMIC_ERROR;
        }

        APP_TRACE_DEBUG("\tMAX14676_REG_STATUS_A: 0x%X\n", reg_status[0]);
        APP_TRACE_DEBUG("\t-> %s\n", reg_status[0] == 0 ? "Chager off" : \
                        reg_status[0] == 1 ? "Charging suspended" : \
                        reg_status[0] == 2 ? "Precharge in progress" : \
                        reg_status[0] == 3 ? "Fast-charge1" : \
                        reg_status[0] == 4 ? "Fast-charge2" : \
                        reg_status[0] == 5 ? "Maintain charge in porgress" : \
                        reg_status[0] == 6 ? "Maintain charge timer done" : "Charger fault condition");
        APP_TRACE_DEBUG("\tMAX14676_REG_STATUS_B: 0x%X\n", reg_status[1]);
        APP_TRACE_DEBUG("\tMAX14676_REG_STATUS_C: 0x%X\n", reg_status[2]);
    }
#endif

    /* Create event flag */
    OSFlagCreate((OS_FLAG_GRP *)&g_pmic_event_flags,
                 (CPU_CHAR    *)"PMIC Flags",
                 (OS_FLAGS     )0,
                 (RTOS_ERR    *)&err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
        APP_TRACE_DEBUG("\t- ERROR: Create PMIC Event Flag\n");
        return PMIC_ERROR;
    }

    /* Create task */
    OSTaskCreate((OS_TCB       *)&g_pmic_task,
                 (CPU_CHAR     *)"Pmic Task",
                 (OS_TASK_PTR   )MAX14676x_Task,
                 (void         *)DEF_NULL,
                 (OS_PRIO       )PMIC_APP_TASK_PRIO,
                 (CPU_STK      *)&g_pmic_task_stk[0],
                 (CPU_STK_SIZE  )(PMIC_APP_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  )PMIC_APP_TASK_STK_SIZE,
                 (OS_MSG_QTY    )0u,
                 (OS_TICK       )0u,
                 (void         *)DEF_NULL,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
        APP_TRACE_DEBUG("\t- ERROR: Create PMIC Task\n");
        return PMIC_ERROR;
    }

    FUELGAUGE_Initialize();

    /* Set configuration for LED */
    MAX14676x_SetLEDStepCurrent(MAX14676_ISTEP_0_6mA);
    TimerInit(&g_led_blink_timer, MAX14676x_BlinkLED);
    TimerInit(&g_led_tap_timer, MAX14676x_TapLED);

    /* Set interrupt and callback function */
    MAX14676x_InitIntr(MAX14676x_IntrCb);

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    De-initializes the pmic (max14676)
 *
 * @return
 *    None
 ******************************************************************************/
void PMIC_DeInit(void)
{
    return;
}

uint32_t PMIC_Start(void)
{
    /* Initialize charge & interrupt register */
    if (MAX14676x_ConfigInit() != PMIC_OK) {
        APP_TRACE_DEBUG("\t- ERROR: ConfigInit\n");
        return PMIC_ERROR;
    }

    return PMIC_OK;
}

uint32_t PMIC_CheckChargerState(bool *state)
{
    uint8_t reg_value;

    APP_TRACE_DEBUG("%s:\n", __func__);

    if (MAX14676x_Read1Byte(MAX14676_REG_STATUS_A, &reg_value) != PMIC_OK) {
        APP_TRACE_DEBUG("\t- ERROR: Read StatusA\n");
        return PMIC_ERROR;
    }

    APP_TRACE_DEBUG("\t- Read REG_STATUS_A = 0x%02x\n", reg_value);

    if ((reg_value == 0x00) || (reg_value == 0x07)) {
        *state = false;
    } else {
        *state = true;
    }

    APP_TRACE_DEBUG("\t- Charger is %s\n", (*state == true) ? "IN" : "OUT");

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Interrupt handler
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint32_t PMIC_ChargerIntr(void)
{
    uint8_t reg_intB = 0;

    APP_TRACE_DEBUG("%s:\n", __FUNCTION__);

    /* Read interrupt register */
    if (MAX14676x_Read1Byte(MAX14676_REG_INT_B, &reg_intB) != PMIC_OK) {
        APP_TRACE_DEBUG("\tRead Error REG_INT_B!!");
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- REG_INT_AB = 0x%02X\n", reg_intB);

    /* Check UsbOkInt */
    if (reg_intB & USB_OK_INT_M) {
        if (MAX14676x_ConfigCharge() != PMIC_OK) {
            return PMIC_ERROR;
        }
    }

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    power off without ChargeIn,
 *    pmic reset with ChargeIn
 *
 * @return
 *    None
 ******************************************************************************/
void PMICLED_PowerOffFactory(void)
{
    uint8_t statusB;

    if (PMIC_OK  == MAX14676x_Read1Byte(MAX14676_REG_STATUS_B, &statusB)) {
        MAX14676x_SetLEDColor(0xFF, 0, 0);
        if (statusB & USB_OK_INT_M) { // same positin with interrupt status
            APP_TRACE_DEBUG("CHGIN present and valid: device will be ON again \r\n");
            MAX14676x_SetLEDControl(MAX14676_LEDX_CFG_ON, 0, 0);
        } else {
            MAX14676x_SetLEDControl(0, 0, 0);
        }
    }

    MAX14676x_Write1Byte(MAX14676_REG_PWR_CFG, 0x05); // POWER OFF cmd
}

void PMICLED_SetColor(pmic_led_colors_t color)
{
	uint8_t red_state, green_state, blue_state;
	uint8_t red_current, green_current, blue_current;
    switch (color)
    {
		case OFF:
			red_state = LED_OFF;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED_OFF; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED_OFF; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case WHITE:
			red_state = LED__ON;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED__ON; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED__ON; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case AMBER:
			red_state = LED__ON;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED__ON; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED_OFF; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case BLUE:
			red_state = LED_OFF;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED_OFF; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED__ON; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case CYAN:
			red_state = LED_OFF;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED__ON; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED__ON; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case GREEN:
			red_state = LED_OFF;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED__ON; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED_OFF; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case MAGENTA:
			red_state = LED__ON;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED_OFF; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED__ON; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		case RED:
			red_state = LED__ON;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED_OFF; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED_OFF; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
		default:
			APP_TRACE_DEBUG("\t- LED unknown color\n");
			red_state = LED_OFF;	red_current = MAX14676_LEDX_I_MAX;
			green_state = LED_OFF; 	green_current = MAX14676_LEDX_I_MAX;
			blue_state = LED_OFF; 	blue_current = MAX14676_LEDX_I_MAX;
			break;
	}

    uint8_t data;

	data = (red_state << MAX14676_LEDX_CFG_POS) | red_current;
	MAX14676x_Write1Byte(RED_LED_LOC, data);

	data = (green_state << MAX14676_LEDX_CFG_POS) | green_current;
	MAX14676x_Write1Byte(GREEN_LED_LOC, data);

	data = (blue_state << MAX14676_LEDX_CFG_POS) | blue_current;
	MAX14676x_Write1Byte(BLUE_LED_LOC, data);

}

void PMICLED_Blink(bool ctrl, uint32_t duty, uint32_t count)
{
    APP_TRACE_DEBUG("%s:\n", __func__);

    /* Stop timer */
    TimerStop(&g_led_blink_timer);

    if (ctrl == false) {
        APP_TRACE_DEBUG("\t- LED Off\n");
        g_led_blink_num = 0;
        g_led_blink_on_duty = 0;
        g_led_blink_force_on = false;

        PMICLED_SetColor(OFF);                           /* Turn off LED */
    } else {
        APP_TRACE_DEBUG("\t- LED On\n");
        g_led_blink_num = count;
        g_led_blink_on_duty = TIME_SEC(duty);
        g_led_blink_force_on = false;
        if (count == 0) {
            g_led_blink_force_on = true;
        }

        TimerSetValue(&g_led_blink_timer, 10);   /* Timer start immediately */
        TimerStart(&g_led_blink_timer);
    }
}

void PMICLED_Tap(void)
{
    APP_TRACE_DEBUG("%s:\n", __func__);

    uint16_t level = 0;
    uint32_t msec;
    msec = LED_OFF_TAP;

    /* Stop timer */
    TimerStop(&g_led_tap_timer);

    FUELGAUGE_GetSoc(&level);                 /* Get current battery level */
    APP_TRACE_DEBUG("\t- on (%d)\n", level);
    if (level <= 25) {                          /* Turn on LED */
    	PMICLED_SetColor(RED);
    } else if (level <= 75) {
    	PMICLED_SetColor(AMBER);
    } else if (level < 100) {
    	PMICLED_SetColor(GREEN);
    } else {
    	PMICLED_SetColor(BLUE);
    }

    TimerSetValue(&g_led_tap_timer, msec);
    TimerStart(&g_led_tap_timer);
}
#endif /* configUSE_PMIC */
