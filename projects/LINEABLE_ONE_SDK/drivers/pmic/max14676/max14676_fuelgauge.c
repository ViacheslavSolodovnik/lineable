/*******************************************************************************
 * @file pmic_fuelgauge.c
 * @brief Driver for fuelgauge of max14676d
 * @version 0.0
 ******************************************************************************/

#include "rtos_app.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>

#include  <drivers/peripheral/i2c/micrium_i2c.h>

#include <drivers/fuelgauge/fuelgauge.h>
#include "max14676_fuelgauge.h"
/*******************************************************************************
*                                 DEBUG OPTION
*******************************************************************************/

#undef  __MODULE__
#define __MODULE__ "FUELGAUGE"
#undef  DBG_MODULE
#define DBG_MODULE 0
#define FUELGAUGE_DEBUG 0

#if (configUSE_PMIC == PMIC_DEVICE_MAX14676)
/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/

/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
#define V_MAX    4200
#define V_MIN    3200


/*******************************************************************************
*                                  MACROS
*******************************************************************************/

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/
static uint8_t max14676_custom_data[] = {
// MODEL DATA
    0x9C, 0x00, 0xAD, 0x90, 0xB2, 0x90, 0xB6, 0x60,
    0xB8, 0xF0, 0xBB, 0xE0, 0xBC, 0xB0, 0xBD, 0x40,
    0xBD, 0xB0, 0xC0, 0x30, 0xC2, 0x80, 0xC6, 0x50,
    0xC9, 0xA0, 0xCC, 0x10, 0xCF, 0xF0, 0xD2, 0x70,
    0x01, 0x20, 0x0C, 0xA0, 0x0C, 0x00, 0x0A, 0x00,
    0x1A, 0x00, 0x66, 0xC0, 0x7B, 0xE0, 0x47, 0xE0,
    0x1F, 0xC0, 0x13, 0xE0, 0x17, 0x60, 0x13, 0x40,
    0x0B, 0xE0, 0x11, 0xE0, 0x02, 0xE0, 0x02, 0xE0,
// RCOMPSEG1
    0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80,
    0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80,
// RCOMPSEG2
    0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80,
    0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80,
};


/*******************************************************************************
*                         PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/
static int8_t MAX14676x_ReadReg(uint8_t Reg, uint16_t *Data);
static int8_t MAX14676x_WriteReg(uint8_t Reg, uint16_t Data);
static int8_t MAX14676x_WriteBlockData(uint8_t nAddr, uint8_t *nRegValue);


/*******************************************************************************
*                              PRIVATE FUNCTIONS
*******************************************************************************/
/*******************************************************************************
 * @brief
 *    Generic Reading function with I2C
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static int8_t MAX14676x_ReadReg(uint8_t reg, uint16_t *data)
{
    I2C_TransferReturn_TypeDef ret;
    uint8_t i2c_read_data[2];

    ret = i2cm_readData(i2c0_id, PMIC_FUELGAUGE_I2C_BUS_ADDRESS, reg, i2c_read_data, 2);

    if (ret != i2cTransferDone) {
        return PMIC_ERROR_I2C_TRANSACTION_FAILED;
    }

    *data = (i2c_read_data[0] << 8) + i2c_read_data[1];

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Generic Writing function with I2C
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static int8_t MAX14676x_WriteReg(uint8_t reg, uint16_t data)
{

    uint8_t write_data[2];

    write_data[0] = (uint8_t)(data >> 8);
    write_data[1] = (uint8_t)(data & 0xFF);

    return i2cm_writeData(i2c0_id, PMIC_FUELGAUGE_I2C_BUS_ADDRESS, reg, write_data, 2) == i2cTransferDone ?
            PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/*******************************************************************************
 * @brief
 *    Block writing function with I2C
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
static int8_t MAX14676x_WriteBlockData(uint8_t nAddr, uint8_t *nRegValue)
{
    uint8_t write_data[2], i;
    uint8_t reg;

    for (i = 0; i < 8; i++) {
        write_data[0] = nRegValue[0 + i * 2];
        write_data[1] = nRegValue[1 + i * 2];

        reg = nAddr + i * 2;

        if (i2cm_writeData(i2c0_id, PMIC_FUELGAUGE_I2C_BUS_ADDRESS, reg, write_data, 2) != i2cTransferDone) {
            return PMIC_ERROR;
        }
    }

    return PMIC_OK;
}


/*******************************************************************************
*                              PUBLIC FUNCTIONS
*******************************************************************************/

/*******************************************************************************
 * @brief
 *    Get VCell
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint16_t FUELGAUGE_GetSoc(uint16_t *soc_value)
{
    uint16_t vcell = 0;
    uint16_t level = 0, vol = 0;

    *soc_value = 0;

    if (MAX14676x_ReadReg(MAX14676_VCELL, &vcell) != PMIC_OK) { // 72.125uV/cell
        APP_TRACE_DEBUG("%s: err %d\n", __func__, vcell);
        return PMIC_ERROR;
    }

    vol = (vcell * 0.0725 + 300);
    APP_TRACE_DEBUG("%s: vol %d\n", __func__, vol);

    if (vol < 3200) {
        vol = 3200;
    }

    level = (uint8_t)((int)(vol - V_MIN) * 100 / (V_MAX - V_MIN));
    APP_TRACE_DEBUG("%s: soc_value %d\n", __func__, level);

    if ((level < 0) && (level > 100)) {
        APP_TRACE_DEBUG("%s: Limit out of battery level %d\n", __func__, level);
        return PMIC_ERROR;
    }

    *soc_value = level;

    return PMIC_OK;
}


/*******************************************************************************
 * @brief
 *    Get SOC
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint16_t FUELGAUGE_GetVcell(uint16_t *vol_level)
{
    uint16_t read_data = 0;
    uint8_t soc1, soc2;

    if (MAX14676x_ReadReg(MAX14676_SOC, &read_data) != PMIC_OK) {
        APP_TRACE_DEBUG("%s: err %d\n", __func__, read_data);
        return PMIC_ERROR;
    }

    soc1 = read_data >> 9;
    *vol_level = soc1;
    APP_TRACE_DEBUG("%s: test_soc1 %d\n", __func__, *vol_level);
#if 0
    soc1 = read_data >> 8;
    soc2 = read_data & 0xFF;


    *vol_level = ((soc1 << 8) + soc2) / 512;
    APP_TRACE_DEBUG("%s: soc1 %d, soc2 %d\n", __func__, soc1, soc2);
    APP_TRACE_DEBUG("%s: test_soc1 %d\n", __func__, *vol_level);


    *vol_level = ((soc1 * 256) + soc2) * 0.001953125;
    APP_TRACE_DEBUG("%s: test_soc1 %d\n", __func__, *vol_level);
#endif

    return PMIC_OK;
}


/*******************************************************************************
 * @brief
 *    Fuelgauge initialize
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint16_t FUELGAUGE_Initialize(void)
{
    RTOS_ERR err;
    uint8_t soc1;
    uint16_t read_data = 0, config_ori = 0, ocv_ori = 0;
    int i;

    /* unlock model access */
    if (MAX14676x_WriteReg(MAX14676_UNLOCK, MAX14676_UNLOCK_VALUE) != PMIC_OK) {
        APP_TRACE_DEBUG("Unlock failed. Check unlock! \r\n");
        return PMIC_ERROR;
    }

    /* read config */
    if (MAX14676x_ReadReg(MAX14676_CONFIG, &config_ori) != PMIC_OK) {
        return PMIC_ERROR;
    }

    APP_TRACE_DEBUG("Config 0x%x\r\n", config_ori);

    /* read ocv */
    if (MAX14676x_ReadReg(MAX14676_OCV, &ocv_ori) != PMIC_OK) {
        return PMIC_ERROR;
    }

    APP_TRACE_DEBUG("OCV 0x%x\r\n", ocv_ori);

    /* verify model access unlocked */
    if (read_data == 0xFFFF) {
        APP_TRACE_DEBUG("Config and OCV read fail. Check unlock! \r\n");
        return PMIC_ERROR;
    }

    /* write OCV */
    if (MAX14676x_WriteReg(MAX14676_OCV, 0xDC70) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* Write RCOMP to a Maximum value of 0xFF00h */
    if (MAX14676x_WriteReg(MAX14676_CONFIG, 0xFF00) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* write custom model data */
    for (i = 0; i < 6; i += 1) {
        if (MAX14676x_WriteBlockData((MAX14676_TABLE + i * 16), &max14676_custom_data[i * 0x10]) != PMIC_OK) {
            APP_TRACE_DEBUG("%s: error writing model data:\n", __func__);
            return PMIC_ERROR;
        }
    }

#ifdef FUELGAUGE_DEBUG
    APP_TRACE_DEBUG("\r\n");
    APP_TRACE_DEBUG("Check default custom table.. \r\n");
    for (i = 0; i < 48; i += 1) {
        MAX14676x_ReadReg(0x40 + i * 2, &read_data);
        APP_TRACE_DEBUG("0x%x read data : %4x \r\n", 0x40 + i * 2, read_data);
    }
#endif

    /* delay between 150ms and 600ms */
    OSTimeDly(OS_MS_2_TICKS(200), OS_OPT_TIME_DLY, &err);

    /* 7write OCV */
    if (MAX14676x_WriteReg(MAX14676_OCV, 0xDC70) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* disable Hibernate */
    if (MAX14676x_WriteReg(MAX14676_HIBRT, 0x0000) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* lock model access */
    if (MAX14676x_WriteReg(MAX14676_UNLOCK, 0x0000) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* delay between 150ms and 600ms */
    OSTimeDly(OS_MS_2_TICKS(200), OS_OPT_TIME_DLY, &err);

    /* read soc register and compare to expected result */
    if (MAX14676x_ReadReg(MAX14676_SOC, &read_data) != PMIC_OK) {
        return PMIC_ERROR;
    }

    soc1 = read_data >> 8;
    if ((soc1 >= 0xCD) && (soc1 <= 0xCF)) {
        APP_TRACE_DEBUG("soc1 %x \r\n", read_data);
        APP_TRACE_DEBUG("Model was loaded successfully.\n");
    } else {
        APP_TRACE_DEBUG("soc1 %x \r\n", read_data);
        APP_TRACE_DEBUG("Model was not loaded successfully.\n\n");
    }

    /* unlock model access */
    if (MAX14676x_WriteReg(MAX14676_UNLOCK, MAX14676_UNLOCK_VALUE) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* restore config */
    if (MAX14676x_WriteReg(MAX14676_CONFIG, config_ori) != PMIC_OK) { // should check recommanded Alert thershold
        return PMIC_ERROR;
    }

    /* restore ocv */
    if (MAX14676x_WriteReg(MAX14676_OCV, ocv_ori) != PMIC_OK) { // should check recommanded Alert thershold
        return PMIC_ERROR;
    }

    /* Lock model access */
    if (MAX14676x_WriteReg(MAX14676_UNLOCK, 0x0000) != PMIC_OK) {
        return PMIC_ERROR;
    }

    /* delay at least 150ms */
    OSTimeDly(OS_MS_2_TICKS(200), OS_OPT_TIME_DLY, &err);

    return PMIC_OK;
}
#endif /* configUSE_PMIC */
