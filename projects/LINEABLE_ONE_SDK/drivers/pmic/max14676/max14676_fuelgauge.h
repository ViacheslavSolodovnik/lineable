/***************************************************************************//**
 * @file pmic_fuelgauge.h
 * @brief
 * @version
 ******************************************************************************/

#ifndef PMIC_FUELGAUGE_H
#define PMIC_FUELGAUGE_H

#if (configUSE_PMIC == PMIC_DEVICE_MAX14676)
/*******************************************************************************
*                                  INCLUDES
*******************************************************************************/


/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/


/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
/*****************************************************************************
* @name I2C definitions
******************************************************************************/
#define PMIC_I2C_BUS_TIMEOUT                   (1000)
#define PMIC_I2C_DEVICE                        (I2C0)
#define PMIC_FUELGAUGE_I2C_BUS_ADDRESS         (0x36)

/**************************************************************************//**
* @name Error Codes
******************************************************************************/
#define PMIC_OK                             0x0000  /**< No errors                                        */
#define PMIC_ERROR                          0x0001  /**<                                                  */
#define PMIC_ERROR_I2C_TRANSACTION_FAILED   0x0002  /**< I2C transaction failed                           */
#define PMIC_ERROR_DEVICE_ID_MISMATCH       0x0003  /**< The device ID does not match the expected value  */


// Register Map
#define MAX14676_VCELL          0x02
#define MAX14676_SOC            0x04
#define MAX14676_MODE           0x06
#define MAX14676_VER            0x08
#define MAX14676_HIBRT          0x0A
#define MAX14676_CONFIG         0x0C
#define MAX14676_OCV            0x0E
#define MAX14676_VALRT          0x14
#define MAX14676_CRATE          0x16
#define MAX14676_VRESET         0x18
#define MAX14676_STATUS         0x1A

#define MAX14676_UNLOCK         0x3E
#define MAX14676_TABLE          0x40
#define MAX14676_RCOMPSEG1      0x80
#define MAX14676_RCOMPSEG2      0x90
#define MAX14676_CMD            0xFE

#define MAX14676_UNLOCK_VALUE   0x4A57
#define MAX14676_RESET_VALUE    0x5400
#define MAX14676_DELAY          1000
#define MAX14676_BATTERY_FULL   100
#define MAX14676_BATTERY_LOW    15
#define MAX14676_VERSION_NO     0x11

/*******************************************************************************
*                                  MACROS
*******************************************************************************/

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/

/*******************************************************************************
*                          PUBLIC FUNCTION PROTOTYPES
*******************************************************************************/
#endif /* configUSE_PMIC */

#endif // PMIC_FUELGAUGE_H
