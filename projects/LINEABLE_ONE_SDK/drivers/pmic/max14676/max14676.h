#ifndef PMIC_MAX14676_H
#define PMIC_MAX14676_H

#if (configUSE_PMIC == PMIC_DEVICE_MAX14676)
/*******************************************************************************
*                                  INCLUDES
*******************************************************************************/
#include <stdint.h>
#include "gpiointerrupt.h"

/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/
typedef enum {
    MAX14676_REG_DEVID         =   0x00,
    MAX14676_REG_CHIP_REV      =   0x01,
    MAX14676_REG_STATUS_A      =   0x02,
    MAX14676_REG_STATUS_B      =   0x03,
    MAX14676_REG_STATUS_C      =   0x04,
    MAX14676_REG_INT_A         =   0x05,
    MAX14676_REG_INT_B         =   0x06,
    MAX14676_REG_INT_MASK_A    =   0x07,
    MAX14676_REG_INT_MASK_B    =   0x08,
    MAX14676_REG_RSVD_0        =   0x09,
    MAX14676_REG_I_LIM_CNTL    =   0x0A,
    MAX14676_REG_CHG_CNTL_A    =   0x0B,
    MAX14676_REG_CHG_CNTL_B    =   0x0C,
    MAX14676_REG_CHG_TMR       =   0x0D,
    MAX14676_REG_CHG_V_SET     =   0x0E,
    MAX14676_REG_RSVD_1        =   0x0F,
    MAX14676_REG_CHG_P_CHG     =   0x10,
    MAX14676_REG_C_DET_CNTL_B  =   0x11,
    MAX14676_REG_CHG_CNTL_C    =   0x12,
    MAX14676_REG_I_LIM_MON     =   0x13,
    MAX14676_REG_RSVD_2        =   0x14,
    MAX14676_REG_BUCK_CFG      =   0x15,
    MAX14676_REG_LDO_CFG       =   0x16,  // no in D
    MAX14676_REG_P_CHG_CNTL    =   0x17,  // no in D
    MAX14676_REG_RSVD_3        =   0x18,  // no in D
    MAX14676_REG_BST_CFG       =   0x19,
    MAX14676_REG_LED_CFG       =   0x1A,
    MAX14676_REG_LED_0         =   0x1B,
    MAX14676_REG_LED_1         =   0x1C,
    MAX14676_REG_LED_2         =   0x1D,
    MAX14676_REG_PWR_CFG       =   0x1E,
    MAX14676_REG_PINS          =   0x1F,
    MAX14676_REG_BOOT_SQ       =   0x20
} MAX14676_SERIES_REG_MAP_t;


// INT_MASK_B (0x08)
typedef enum {
    BAT_THRM_SD_INT_M    =   0x01,
    BAT_HIGH_T_INT_M     =   0x02,
    BAT_STAT_INT_M       =   0x04,
    USB_OK_INT_M         =   0x08,
    CHG_THRM_SD_INT_M    =   0x10,
    I_LIM_INT_M          =   0x20,
} MAX14676_INT_MASK_B_t;


// CHG_CNTL_A (0x0B)
typedef enum {             //    14676/A           14676D
    IF_CFG_0    =   0x00,  //     37.5mA           9.38mA
    IF_CFG_1    =   0x01,  //       75mA           18.8mA
    IF_CFG_2    =   0x02,  //      100mA             25mA
    IF_CFG_3    =   0x03,  //      150mA           37.5mA
    IF_CFG_4    =   0x04,  //      200mA             50mA
    IF_CFG_5    =   0x05,  //      250mA           62.5mA
    IF_CFG_6    =   0x06,  //      300mA             75mA
    IF_CFG_7    =   0x07,  //      350mA           87.5mA
} MAX14676_FAST_CHARGE_CURRENT_CFG_t;


// CHG_V_SET (0x0E)
typedef enum {
    BAT_RE_CHG_0   =   0x00,  // 70mV
    BAT_RE_CHG_1   =   0x40,  // 120mV
    BAT_RE_CHG_3   =   0x80,  // 170mV
    BAT_RE_CHG_4   =   0xC0,  // 220mV
} MAX14676_BAT_RE_CHG_t;

typedef enum {
    BAT_CHG_0   =   0x00,     // 4.20V
    BAT_CHG_1   =   0x10,     // 4.25V
    BAT_CHG_3   =   0x20,     // 4.30V
    BAT_CHG_4   =   0x30,     // 4.35V
} MAX14676_BAT_REG_t;


// CHG_CNTL_B (0x0C)
typedef enum {
    CHG_DISABLE   =   0x00,
    CHG_ENABLE    =   0x10,
} MAX14676_CHG_EN_t;


// LDO_CFG (0x16)
typedef enum {
    LDO_DISABLED               =   0x00,
    LDO_ENABLED                =   0x40,
    LDO_ENABLED_WITH_MPC0      =   0x80,
    LDO_ENABLED_WITH_MPC1      =   0xC0
} MAX14676_LDO_CFG_t;


// BST_CFG (0x19)
typedef enum {
    BOOST_DISABLE              =   0x00,
    BOOST_ENABLED              =   0x40,
    BOOST_ENABLED_WITH_MPC0    =   0x80,
    BOOST_ENABLED_WITH_MPC1    =   0xC0
} MAX14676_BST_EN_t;

typedef enum {
    BOOST_VOL_5V               =   0x00,
    BOOST_VOL_6V               =   0x01,
    BOOST_VOL_7V               =   0x02,
    BOOST_VOL_8V               =   0x03,
    BOOST_VOL_9V               =   0x04,
    BOOST_VOL_10V              =   0x05,
    BOOST_VOL_11V              =   0x06,
    BOOST_VOL_12V              =   0x07,
    BOOST_VOL_13V              =   0x08,
    BOOST_VOL_14V              =   0x09,
    BOOST_VOL_15V              =   0x0A,
    BOOST_VOL_16V              =   0x0B,
    BOOST_VOL_17V              =   0x0C,
} MAX14676_BST_VOLTAGE_t;

typedef uint32_t (*PMIC_IrqCallbackPtr_t)(void);

/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
/*****************************************************************************
* @name I2C definitions
******************************************************************************/
#define PMIC_I2C_BUS_TIMEOUT         (1000)
#define PMIC_I2C_DEVICE              (I2C0)
#define PMIC_I2C_BUS_ADDRESS         (0x28)

/*****************************************************************************
* @name Error Codes
******************************************************************************/
#define PMIC_OK                             0x0000  /**< No errors                                        */
#define PMIC_ERROR                          0x0001  /**<                                                  */
#define PMIC_ERROR_I2C_TRANSACTION_FAILED   0x0002  /**< I2C transaction failed                           */
#define PMIC_ERROR_DEVICE_ID_MISMATCH       0x0003  /**< The device ID does not match the expected value  */

/*****************************************************************************
* @name Device ID
******************************************************************************/
#define PMIC_DEVICE_ID_MAX14676                0x2C   /**< Device ID of the MAX14676          */
#define PMIC_DEVICE_ID_MAX14676D               0x3E   /**< Device ID of the MAX14676D         */

/*****************************************************************************
* @name LED ON/OFF
******************************************************************************/
#define LED__ON   1
#define LED_OFF  0


#define MAX14676_REG_LED_CFG      0x1A
#define MAX14676_LED_ISTEP_POS    0
#define MAX14676_LED_ISTEP_MASK   (0x03<<MAX14676_LED_ISTEP_POS)
#define MAX14676_ISTEP_0_6mA      0
#define MAX14676_ISTEP_1mA        1
#define MAX14676_ISTEP_1_2mA      2


#define MAX14676_REG_LED_0        0x1B
#define MAX14676_REG_LED_1        0x1C
#define MAX14676_REG_LED_2        0x1D
#define MAX14676_LEDX_CFG_POS     5
#define MAX14676_LEDX_CFG_MASK    (0x7<<MAX14676_LEDX_CFG_POS)
#define MAX14676_LEDX_CFG_OFF     (0)
#define MAX14676_LEDX_CFG_ON      (1)
#define MAX14676_LEDX_CFG_CHARGER (3)

#define MAX14676_LEDX_I_POS       (0)
#define MAX14676_LEDX_I_MASK      (0x1F<<MAX14676_LEDX_I_POS)
#define MAX14676_LEDX_I_MAX       (0x18<<MAX14676_LEDX_I_POS)

/*******************************************************************************
*                                  MACROS
*******************************************************************************/

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/

/*******************************************************************************
*                          PUBLIC FUNCTION PROTOTYPES
*******************************************************************************/
void     PMIC_DeInit(void);
uint32_t PMIC_ConfigBoost(uint8_t enable, uint8_t voltage); // oled VCC
uint32_t PMIC_ChargerIntr(void);

void PMICLED_Blink(bool ctrl, uint32_t duty, uint32_t count);
void PMICLED_PowerOffFactory(void);
#endif /* configUSE_PMIC */

#endif // PMIC_MAX14676_H
