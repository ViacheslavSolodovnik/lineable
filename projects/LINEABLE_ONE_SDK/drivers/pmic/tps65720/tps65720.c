/**
 * @copyright { Copyright 2019 Lineable (C) by All Rights Reserved.
 *  Unauthorized redistribution of this source code, in whole or part,
 *  without the express written permission of Lineable is strictly prohibited.
 *  }
 * @file tps65720.c
 * @brief Driver for PMIC TPS65720
 */

#include <rtos_app.h>
#include <sys_os.h>
#include <timer.h>

#include <drivers/pmic/pmic.h>
#include <drivers/fuelgauge/fuelgauge.h>

#include "tps65720.h"

/*******************************************************************************
*                                 DEBUG OPTION
*******************************************************************************/

#undef  __MODULE__
#define __MODULE__ "PMIC_TPS65720"
#undef  DBG_MODULE
#define DBG_MODULE 0

#if (configUSE_PMIC == PMIC_DEVICE_TPS65720)

/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
#define CHARGE_CHK_PERIOD (1000)    /* 1 sec   */
#define CHARGING_LED_PERIOD (1000)  /* 1 sec   */
#define LED_TAP_TIMEOUT (2000)      /* 2 sec   */

#define PMIC_EVENT_FLAG_EVT_STOPPED_CHARGING             ((OS_FLAGS)(1<<0))
#define PMIC_EVENT_FLAG_EVT_CHARGING_LED                 ((OS_FLAGS)(1<<1))
#define PMIC_EVENT_FLAG_EVT_TAP_LED_TIMEOUT              ((OS_FLAGS)(1<<2))
#define PMIC_EVENT_FLAG_EVT_TAP_LED                      ((OS_FLAGS)(1<<3))
#define PMIC_EVENT_FLAG_EVT_STARTED_CHARGING             ((OS_FLAGS)(1<<4))
#define PMIC_EVENT_FLAG_EVT_ALL                         (PMIC_EVENT_FLAG_EVT_CHARGING_LED               | \
                                                         PMIC_EVENT_FLAG_EVT_STOPPED_CHARGING          | \
                                                         PMIC_EVENT_FLAG_EVT_TAP_LED_TIMEOUT                   | \
                                                         PMIC_EVENT_FLAG_EVT_STARTED_CHARGING          | \
                                                         PMIC_EVENT_FLAG_EVT_TAP_LED)

#define CH_PGOOD_STATUS_BIT            (0x04)
#define POWER_SOURCE_IS_NOT_PRESENT    (0)

/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/
/* Task */
static OS_TCB       g_pmic_task;
static CPU_STK      g_pmic_task_stk[PMIC_APP_TASK_STK_SIZE];

static OS_TCB       g_pmic_charger_check_task;
static CPU_STK      g_pmic_charger_check_task_stk[PMIC_CHARGE_CHECK_TASK_STK_SIZE];
/* Event Flag */
static OS_FLAG_GRP  g_pmic_event_flags;
/* for LED Blink */
static TimerEvent_t g_led_tap_timer_timeout;
static TimerEvent_t g_led_handle_charging_timer;

static I2CM_ID      g_i2c_interface;

/*******************************************************************************
*                         PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/
static int8_t TPS65720_ReadReg(uint8_t Reg, uint8_t *Data);
static int8_t TPS65720_WriteReg(uint8_t Reg, uint8_t Data);
static void TPS65720_TapLED_timeout(void);

/**
* @name Internal utility functions
* @{
*/

/**
 * @brief Generic Reading function with I2C
 * @param reg TPS65720 register address
 * @param rec_buf pointer to buffer for received data
 * @return Returns PMIC_OK on success, PMIC_ERROR_I2C_TRANSACTION_FAILED otherwise
 */
static int8_t TPS65720_ReadReg(uint8_t reg, uint8_t *data)
{
    return i2cm_readData(g_i2c_interface, TPS65720_I2C_BUS_ADDRESS, reg, data, 1) == i2cTransferDone ?
            PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/**
 * @brief Generic writing function with I2C
 * @param reg TPS65720 register address
 * @param rec_buf pointer to buffer for data to be written
 * @return Returns PMIC_OK on success, PMIC_ERROR_I2C_TRANSACTION_FAILED otherwise
 */
static int8_t TPS65720_WriteReg(uint8_t reg, uint8_t data)
{
    return i2cm_writeData(g_i2c_interface, TPS65720_I2C_BUS_ADDRESS, reg, &data, 1) == i2cTransferDone ?
            PMIC_OK : PMIC_ERROR_I2C_TRANSACTION_FAILED;
}

/**@}*/

static void TPS65720_TapLED_timeout(void)
{
    RTOS_ERR err;

    TimerStop(&g_led_tap_timer_timeout);                  /* Stop timer */

    OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
                   (OS_FLAGS     ) PMIC_EVENT_FLAG_EVT_TAP_LED_TIMEOUT,
                   (OS_OPT       ) OS_OPT_POST_FLAG_SET,
                   (RTOS_ERR    *)&err);
}

static void PMIC_ChargingStarted(void)
{
    /* Start timer immediately */
    TimerSetValue(&g_led_handle_charging_timer, 0);
    TimerStart(&g_led_handle_charging_timer);
}

static void PMIC_HandleCharging(void)
{
    RTOS_ERR err;

    TimerStop(&g_led_handle_charging_timer);              /* Stop timer */

    OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
                   (OS_FLAGS     ) PMIC_EVENT_FLAG_EVT_CHARGING_LED,
                   (OS_OPT       ) OS_OPT_POST_FLAG_SET,
                   (RTOS_ERR    *)&err);

    TimerSetValue(&g_led_handle_charging_timer, CHARGING_LED_PERIOD);
    TimerStart(&g_led_handle_charging_timer);
}

static void PMIC_ChargingStopped(void)
{
    TimerStop(&g_led_handle_charging_timer);
    PMICLED_SetColor(OFF);
}

/*******************************************************************************
 * @brief
 *    Fuelgauge initialize
 *
 * @return
 *    Returns zero on OK, non-zero otherwise  TPS65720_GPIO_DIR
 ******************************************************************************/
static uint16_t TPS65720_Initialize(void)
{
    RTOS_ERR err;

    /* delay between 150ms and 600ms */
    OSTimeDly(OS_MS_2_TICKS(300), OS_OPT_TIME_DLY, &err); //battery detection timer : 250ms(datasheet 14page)

    /* charge termination disable, the others(default) */
    if (TPS65720_WriteReg(TPS65720_CHGCONFIG0, 0x6D) != PMIC_OK)
    {
        return PMIC_ERROR;
    }

    /* charge voltage selection : 4.3V */
    if (TPS65720_WriteReg(TPS65720_CHGCONFIG3, 0xC1) != PMIC_OK)
    {
        return PMIC_ERROR;
    }

    /* set output direction(GPIO1, GPIO2, GPIO3) */
    if (TPS65720_WriteReg(TPS65720_GPIO_DIR, 0x01) != PMIC_OK)
    {
        return PMIC_ERROR;
    }

    /* turn off all leds */
    if (TPS65720_WriteReg(TPS65720_GPIO_SSC, 0x0E) != PMIC_OK)
    {
        return PMIC_ERROR;
    }

    /* delay at least 150ms */
    OSTimeDly(OS_MS_2_TICKS(200), OS_OPT_TIME_DLY, &err);

    return PMIC_OK;
}

static void TPS65720_ChargerCheckTask(void *p_arg)
{
    bool charging_state = false;
    RTOS_ERR err;
    bool prev_charging_state;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON)
    {
        prev_charging_state = charging_state;

        PMIC_CheckChargerState(&charging_state);

        if (prev_charging_state == charging_state) {
            SYSOS_DelayMs(CHARGE_CHK_PERIOD);
            continue;
        }

        if (charging_state) {
            OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
                       (OS_FLAGS     ) PMIC_EVENT_FLAG_EVT_STARTED_CHARGING,
                       (OS_OPT       ) OS_OPT_POST_FLAG_SET,
                       (RTOS_ERR    *)&err);
        } else {
            OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
                       (OS_FLAGS     ) PMIC_EVENT_FLAG_EVT_STOPPED_CHARGING,
                       (OS_OPT       ) OS_OPT_POST_FLAG_SET,
                       (RTOS_ERR    *)&err);
        }

        /*
         * TODO We shouldn't check device charging state in the loop.
         * This needs to be changed to the interrupt-based logic
         * */
        SYSOS_DelayMs(CHARGE_CHK_PERIOD);
    }
}

static void TPS65720_Task(void *p_arg)
{
    RTOS_ERR err;
    OS_FLAGS event = 0;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON)
    {
        event = OSFlagPend((OS_FLAG_GRP *)&g_pmic_event_flags,
                           (OS_FLAGS     ) PMIC_EVENT_FLAG_EVT_ALL,
                           (OS_TICK      ) 0,
                           (OS_OPT       ) OS_OPT_DEFAULT,
                           (CPU_TS      *) NULL,
                           (RTOS_ERR    *)&err);

        if (event & PMIC_EVENT_FLAG_EVT_STARTED_CHARGING) {
            PMIC_ChargingStarted();
        }

        if (event & PMIC_EVENT_FLAG_EVT_STOPPED_CHARGING) {
            PMIC_ChargingStopped();
        }

        if (event & PMIC_EVENT_FLAG_EVT_CHARGING_LED) {
            static uint8_t blink = 1;
            uint16_t soc_val;

            FUELGAUGE_GetSoc(&soc_val);
            if (soc_val == 100)
                PMICLED_SetColor(WHITE);
            else
                PMICLED_SetColor((blink == 1) ? WHITE : OFF);
            blink ^= 1;
        }

        /*----------------------------------------------------------------------
         *  TAP LED Event
         ---------------------------------------------------------------------*/
        if (event & PMIC_EVENT_FLAG_EVT_TAP_LED) {
            TimerStop(&g_led_tap_timer_timeout);

            PMICLED_SetColor(WHITE);

            TimerSetValue(&g_led_tap_timer_timeout, LED_TAP_TIMEOUT);
            TimerStart(&g_led_tap_timer_timeout);
        }

        if (event & PMIC_EVENT_FLAG_EVT_TAP_LED_TIMEOUT) {
            PMICLED_SetColor(OFF);
        }
    }
}

/*******************************************************************************
*                              PUBLIC FUNCTIONS
*******************************************************************************/

uint32_t PMIC_Init(I2CM_ID interface)
{
    RTOS_ERR err;

    APP_TRACE_DEBUG("%s:\n", __func__);

    g_i2c_interface = interface; // FIXME TODO introduce config structure

    /* Initialize TI PMIC, LEDs */
    if (TPS65720_Initialize() != PMIC_OK) {
        APP_TRACE_DEBUG("TI PMIC Init error\n");
        return PMIC_ERROR;
    }

    /* Create event flag */
    OSFlagCreate((OS_FLAG_GRP *)&g_pmic_event_flags,
                 (CPU_CHAR    *) "PMIC Flags",
                 (OS_FLAGS     ) 0,
                 (RTOS_ERR    *)&err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
        APP_TRACE_DEBUG("\t- ERROR: Create PMIC Event Flag\n");
        return PMIC_ERROR;
    }

    /* Create task */
    OSTaskCreate((OS_TCB       *)&g_pmic_task,
                 (CPU_CHAR     *) "Pmic Task",
                 (OS_TASK_PTR   ) TPS65720_Task,
                 (void         *) DEF_NULL,
                 (OS_PRIO       ) PMIC_APP_TASK_PRIO,
                 (CPU_STK      *)&g_pmic_task_stk[0],
                 (CPU_STK_SIZE  ) (PMIC_APP_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  ) PMIC_APP_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) 0u,
                 (OS_TICK       ) 0u,
                 (void         *) DEF_NULL,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
        APP_TRACE_DEBUG("\t- ERROR: Create PMIC Task\n");
        return PMIC_ERROR;
    }

    /* Create task */
    OSTaskCreate((OS_TCB       *)&g_pmic_charger_check_task,
                 (CPU_CHAR     *) "Pmic Charger Check Task",
                 (OS_TASK_PTR   ) TPS65720_ChargerCheckTask,
                 (void         *) DEF_NULL,
                 (OS_PRIO       ) PMIC_CHARGE_CHECK_TASK_PRIO,
                 (CPU_STK      *)&g_pmic_charger_check_task_stk[0],
                 (CPU_STK_SIZE  ) (PMIC_CHARGE_CHECK_TASK_STK_SIZE / 10U),
                 (CPU_STK_SIZE  ) PMIC_CHARGE_CHECK_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) 0u,
                 (OS_TICK       ) 0u,
                 (void         *) DEF_NULL,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK + OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE) {
        APP_TRACE_DEBUG("\t- ERROR: Create PMIC Charger Check Task\n");
        return PMIC_ERROR;
    }

    /* Set timer value */
    TimerInit(&g_led_tap_timer_timeout, TPS65720_TapLED_timeout);
    TimerInit(&g_led_handle_charging_timer, PMIC_HandleCharging);

    return PMIC_OK;
}

uint32_t PMIC_Start(void)
{
    return PMIC_OK;
}

void PMICLED_SetColor(pmic_led_colors_t color)
{
    uint8_t new_data = 0x00, old_data = 0x00;
    uint8_t mask = 0x0E;

    uint8_t internal_color = 0;
    switch (color) {
        case OFF:
            internal_color = LED_COLOR_OFF;
            break;
        case WHITE:
            internal_color = LED_COLOR_WHITE;
            break;
        case AMBER:
            internal_color = LED_COLOR_AMBER;
            break;
        case BLUE:
            internal_color = LED_COLOR_BLUE;
            break;
        case CYAN:
            internal_color = LED_COLOR_CYAN;
            break;
        case GREEN:
            internal_color = LED_COLOR_GREEN;
            break;
        case MAGENTA:
            internal_color = LED_COLOR_MAGENTA;
            break;
        case RED:
            internal_color = LED_COLOR_RED;
            break;
        default:
            APP_TRACE_DEBUG("\t- LED unknown color\n");
            internal_color = LED_COLOR_OFF;
            break;
    }

    TPS65720_ReadReg(TPS65720_GPIO_SSC, &old_data);
    new_data = ((old_data & (~mask)) | ((internal_color << SYSOS_CalcFFS(mask)) & mask));
    TPS65720_WriteReg(TPS65720_GPIO_SSC, new_data);
}

void PMICLED_Tap(void)
{
    RTOS_ERR err;

    APP_TRACE_DEBUG("%s:\n", __func__);

    OSFlagPost((OS_FLAG_GRP *)&g_pmic_event_flags,
               (OS_FLAGS     ) PMIC_EVENT_FLAG_EVT_TAP_LED,
               (OS_OPT       ) OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
}

uint32_t PMIC_CheckChargerState(bool *state)
{
    uint8_t reg_value;

    APP_TRACE_DEBUG("%s:\n", __func__);

    if (TPS65720_ReadReg(TPS65720_CHGSTATUS, &reg_value) != PMIC_OK) {
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_CHGSTATUS = 0x%02x\n", reg_value);

    if ((reg_value & CH_PGOOD_STATUS_BIT) == POWER_SOURCE_IS_NOT_PRESENT) {
        *state = false;
    } else {
        *state = true;
    }

    APP_TRACE_DEBUG("\t- Charger is %s\n", (*state == true) ? "IN" : "OUT");

    return PMIC_OK;
}

/*******************************************************************************
 * @brief
 *    Read charge state
 *
 * @return
 *    Returns zero on OK, non-zero otherwise
 ******************************************************************************/
uint16_t PMIC_ReadCharState(uint8_t *ch_state)
{
    uint8_t reg_value;

    if (TPS65720_ReadReg(TPS65720_CHGSTATE, &reg_value) != PMIC_OK)
    {
        APP_TRACE_DEBUG("FUELGAUGE Read ERROR\r\n");
        return PMIC_OK;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_CHGSTATE = 0x%02x\n", reg_value);

    if (TPS65720_ReadReg(TPS65720_CHGSTATUS, &reg_value) != PMIC_OK)
    {
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_CHGSTATUS = 0x%02x\n", reg_value);

    if (TPS65720_ReadReg(TPS65720_CHGCONFIG0, &reg_value) != PMIC_OK)
    {
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_CHGCONFIG0 = 0x%02x\n", reg_value);

    if (TPS65720_ReadReg(TPS65720_IR0, &reg_value) != PMIC_OK)
    {
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_IR0 = 0x%02x\n", reg_value);

    if (TPS65720_ReadReg(TPS65720_IR1, &reg_value) != PMIC_OK)
    {
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_IR1 = 0x%02x\n", reg_value);

    if (TPS65720_ReadReg(TPS65720_IR2, &reg_value) != PMIC_OK)
    {
        return PMIC_ERROR;
    }
    APP_TRACE_DEBUG("\t- Read TPS65720_IR2 = 0x%02x\n", reg_value);

    *ch_state = reg_value;

    return PMIC_OK;
}
#endif /* configUSE_PMIC */
