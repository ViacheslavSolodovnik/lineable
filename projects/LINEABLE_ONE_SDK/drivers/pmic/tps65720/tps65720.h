/*******************************************************************************
 * @copyright { Copyright 2019 Lineable (C) by All Rights Reserved.
 *  Unauthorized redistribution of this source code, in whole or part,
 *  without the express written permission of Lineable is strictly prohibited.
 *  }
*******************************************************************************/

/**
 *
 * @file tps65720.h
 * @brief TPS65720 PMIC interface
*/

#ifndef DRIVERS_SOURCE_PMIC_TPS65720_H_
#define DRIVERS_SOURCE_PMIC_TPS65720_H_

#include <stdint.h>
#include <stdbool.h>

#if (configUSE_PMIC == PMIC_DEVICE_TPS65720)
/*******************************************************************************
*                                  INCLUDES
*******************************************************************************/


/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/


/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
/**
* @name I2C definitions
* @{
*/
#define TPS65720_I2C_BUS_TIMEOUT	(1000)
#define TPS65720_I2C_BUS_ADDRESS    (0x48)
/**@}*/

/**
* @name TPS65720 I2C register map
* @{
*/
#define TPS65720_CHGSTATUS     0x01
#define TPS65720_CHGCONFIG0    0x02
#define TPS65720_CHGCONFIG1    0x03
#define TPS65720_CHGCONFIG2    0x04
#define TPS65720_CHGCONFIG3    0x05
#define TPS65720_CHGSTATE      0x06
#define TPS65720_DEFDCDC1      0x07
#define TPS65720_LDO_CTRL      0x08
#define TPS65720_CONTROL0      0x09
#define TPS65720_CONTROL1      0x0A
#define TPS65720_GPIO_SSC      0x0B
#define TPS65720_GPIO_DIR      0x0C
#define TPS65720_IRMASK0       0x0D
#define TPS65720_IRMASK1       0x0E
#define TPS65720_IRMASK2       0x0F
#define TPS65720_IR0           0x10
#define TPS65720_IR1           0x11
#define TPS65720_IR2           0x12
/**@}*/

/*******************************************************************************
*                                  MACROS
*******************************************************************************/
#define COLOR_RED              (0x6)       /* 110b */
#define COLOR_GREEN            (0x5)       /* 101b */
#define COLOR_BLUE             (0x3)       /* 011b */
#define LED_COLOR_OFF          (COLOR_RED | COLOR_GREEN | COLOR_BLUE)
#define LED_COLOR_RED          (COLOR_RED)
#define LED_COLOR_GREEN        (COLOR_GREEN)
#define LED_COLOR_BLUE         (COLOR_BLUE)
#define LED_COLOR_AMBER        (COLOR_RED & COLOR_GREEN)
#define LED_COLOR_MAGENTA      (COLOR_RED & COLOR_BLUE)
#define LED_COLOR_CYAN         (COLOR_GREEN & COLOR_BLUE)
#define LED_COLOR_WHITE        (COLOR_RED & COLOR_GREEN & COLOR_BLUE)

#endif /* configUSE_PMIC */

#endif /* DRIVERS_SOURCE_PMIC_TPS65720_H_ */
