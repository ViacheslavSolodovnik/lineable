/**
 * @copyright { Copyright 2019 Lineable (C) by All Rights Reserved.
 *  Unauthorized redistribution of this source code, in whole or part,
 *  without the express written permission of Lineable is strictly prohibited.
 *  }
 * @file micrium_i2c.c
 * @brief I2C master driver for Micrium OS based on emlib
 */


#ifndef DRIVERS_PERIPHERAL_MICRIUM_I2C_H_
#define DRIVERS_PERIPHERAL_MICRIUM_I2C_H_

#include <stddef.h>
#include <stdint.h>

#include "bsp.h"

#include <kernel/include/os.h>

#include <em_cmu.h>
#include <em_gpio.h>
#include <em_i2c.h>
#include <em_ldma.h>

/**
* @addtogroup micrium_drivers
* @{
* @name Micrium OS EMLib based I2C driver
* @details
*  This module contains functions to control the I2C peripheral of Silicon
*  Labs 32-bit MCUs and SoCs. It is interrupt based communication and uses Micrium OS synchronization primitives -
*  mutexes and queues. It requires several configuration defines to work properly:
*   - configI2C(0|1)_USED - (1|0): defines set of available and used interfaces
*   - BSP_I2C(0|1)_SCL_(PORT|PIN) - GPIO port and pin number for SCL pin
*   - BSP_I2C(0|1)_SDA_(PORT|PIN) - GPIO port and pin number for SDA pin
*   - BSP_I2C(0|1)_(SCL|SDA)_LOC  - SDA and SCL locarions
*  Settings prefixed by BSP_ are subject to remove from driver and left here only for simplicity
*/

/**
 * I2C driver instance initialization structure.
 * This data structure contains a number of I2C configuration options
 * required for driver instance initialization.
 */
typedef struct {
    I2C_TypeDef           *port;          	/**< Peripheral port */
    GPIO_Port_TypeDef     sclPort;        	/**< SCL pin port number */
    uint8_t               sclPin;         	/**< SCL pin number */
    GPIO_Port_TypeDef     sdaPort;        	/**< SDA pin port number */
    uint8_t               sdaPin;         	/**< SDA pin number */
#if defined (_I2C_ROUTELOC0_MASK)
    uint8_t               portLocationScl; 	/**< Port location of SCL signal */
    uint8_t               portLocationSda; 	/**< Port location of SDA signal */
#else
    uint8_t               portLocation;   	/**< Port location */
#endif
    uint32_t              i2cRefFreq;     	/**< I2C reference clock */
    uint32_t              i2cMaxFreq;     	/**< I2C max bus frequency to use */
    I2C_ClockHLR_TypeDef  i2cClhr;        	/**< Clock low/high ratio control */
} I2CM_Init_TypeDef;


typedef struct {
    OS_MUTEX 			lock_mtx;			/**< Mutex for port access control */
    OS_Q 				transfer_q;			/**< Transfer finish queue */
    I2C_TypeDef 		*i2c;				/**< I2C port registers */
} I2CM_DESC, *I2CM_ID;


#if (configI2C0_USED == 1)
    extern I2CM_ID i2c0_id;
#endif

#if (configI2C1_USED == 1)
    extern I2CM_ID i2c1_id;
#endif


/**
 * Initialize I2C before first usage. Uses setting defined earlier
 */
void i2cm_init(void);

/**
 * @brief
 * 	generic I2C transfer blocking call.
 * @details
 * 	Generic I2C transfer functions. Uses transfer descriptor @ref I2C_TransferSeq_TypeDef from @ref emlib.
 * 	Will block calling thread until requested interface is free. Locks interface mutex for time required to transfer
 * 	all data.
 * @param id I2C interface ID
 * @param seq transfer descriptor from EMLIB
 * @return retuns status of transacton
 */
I2C_TransferReturn_TypeDef i2cm_transfer(I2CM_ID id, I2C_TransferSeq_TypeDef *seq);

/**
 * @brief
 * 	Performs WRITE_WRITE I2C transfer.
 * @details
 *	Function performs write transaction prefixed by specific register address.
 *
 * @param id I2C interface ID
 * @param device_addr Device 7 bit address on I2C bus.
 * @param start_addr 8 bit of register address
 * @param data pointer to a data to send
 * @param data_len length of data to send
 * @return
 */
I2C_TransferReturn_TypeDef i2cm_writeData(I2CM_ID id, uint16_t device_addr, uint8_t start_addr,
                                            uint8_t *data, uint16_t data_len);

/**
 * @brief
 * 	Performs WRITE_READ I2C transfer.
 * @details
 *	Function performs read transaction prefixed by write with register address.
 *
 * @param id I2C interface ID
 * @param device_addr Device 7 bit address on I2C bus.
 * @param start_addr 8 bit of register address
 * @param data pointer to buffer for receiving data
 * @param data_len length of data to receive
 * @return
 */
I2C_TransferReturn_TypeDef i2cm_readData(I2CM_ID id, uint16_t device_addr, uint8_t start_addr,
                                            uint8_t *data, uint16_t data_len);

/**
* @}
*/
#endif /* DRIVERS_PERIPHERAL_MICRIUM_I2C_H_ */
