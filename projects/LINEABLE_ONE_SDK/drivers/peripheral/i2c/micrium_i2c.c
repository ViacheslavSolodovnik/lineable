/**
 * @copyright { Copyright 2019 Lineable (C) by All Rights Reserved.
 *  Unauthorized redistribution of this source code, in whole or part,
 *  without the express written permission of Lineable is strictly prohibited.
 *  }
 * @file micrium_i2c.c
 * @brief I2C master driver for Micrium OS based on emlib
 */

#include "micrium_i2c.h"
#include <em_core.h>

#if (configI2C0_USED == 1)
    static const I2CM_Init_TypeDef i2c0_init = {                                \
        I2C0,  /* Use I2C instance */                                           \
        BSP_I2C0_SCL_PORT,    /* SCL port */                                    \
        BSP_I2C0_SCL_PIN,     /* SCL pin */                                     \
        BSP_I2C0_SDA_PORT,    /* SDA port */                                    \
        BSP_I2C0_SDA_PIN,     /* SDA pin */                                     \
        BSP_I2C0_SCL_LOC,     /* Location of SCL */                             \
        BSP_I2C0_SDA_LOC,     /* Location of SDA */                             \
        0,                    /* Use currently configured reference clock */    \
        I2C_FREQ_FAST_MAX,                                                      \
        i2cClockHLRFast,                                                        \
    };
    static I2CM_DESC s_i2c0_desc = {
        .i2c = I2C0
    };
    I2CM_ID i2c0_id = &s_i2c0_desc;
#endif

#if (configI2C1_USED == 1)
    static const I2CM_Init_TypeDef i2c1_init = {                                \
        I2C1,  /* Use I2C instance */                                           \
        BSP_I2C1_SCL_PORT,    /* SCL port */                                    \
        BSP_I2C1_SCL_PIN,     /* SCL pin */                                     \
        BSP_I2C1_SDA_PORT,    /* SDA port */                                    \
        BSP_I2C1_SDA_PIN,     /* SDA pin */                                     \
        BSP_I2C1_SCL_LOC,     /* Location of SCL */                             \
        BSP_I2C1_SDA_LOC,     /* Location of SDA */                             \
        0,                    /* Use currently configured reference clock */    \
        I2C_FREQ_FAST_MAX,                                                      \
        i2cClockHLRFast,                                                        \
    };
    static I2CM_DESC	s_i2c1_desc  = {
        .i2c = I2C1
    };
    I2CM_ID i2c1_id = &s_i2c1_desc;
#endif

#if (configI2C0_USED == 1)
/**
 * @brief I2C0 IRQ handler
 */
void I2C0_IRQHandler(void)
{
    static I2C_TransferReturn_TypeDef ret = i2cTransferDone;

    CORE_DECLARE_IRQ_STATE;
    CORE_ENTER_ATOMIC();
    OSIntEnter();
    CORE_EXIT_ATOMIC();

    ret = I2C_Transfer(s_i2c0_desc.i2c);
    if (ret != i2cTransferInProgress) {
        RTOS_ERR  err;
        OSQPost(&s_i2c0_desc.transfer_q, &ret, sizeof(I2C_TransferReturn_TypeDef), OS_OPT_POST_ALL, &err);
    }

    CORE_ENTER_ATOMIC();
    OSIntExit();
    CORE_EXIT_ATOMIC();
}
#endif

#if (configI2C1_USED == 1)
/**
 * @brief I2C0 IRQ handler
 */
void I2C1_IRQHandler(void)
{
    CORE_DECLARE_IRQ_STATE;
    CORE_ENTER_ATOMIC();
    OSIntEnter();
    CORE_EXIT_ATOMIC();

    static I2C_TransferReturn_TypeDef ret = i2cTransferDone;
    ret = I2C_Transfer(s_i2c1_desc.i2c);
    if (ret != i2cTransferInProgress) {
        RTOS_ERR  err;
        OSQPost(&s_i2c1_desc.transfer_q, &ret, sizeof(I2C_TransferReturn_TypeDef), OS_OPT_POST_ALL, &err);
    }
    CORE_ENTER_ATOMIC();
    OSIntExit();
    CORE_EXIT_ATOMIC();
}
#endif

/**
 * Initializer "inspired by i2cspm code"
 *
 * @param init ID of interface to init
 */
static void i2cm_enable(const I2CM_Init_TypeDef *init)
{
    int i;
    CMU_Clock_TypeDef i2cClock;
    I2C_Init_TypeDef i2cInit;

    EFM_ASSERT(init != NULL);

    i2cClock = init->port == I2C0 ? cmuClock_I2C0 : cmuClock_I2C1;
    CMU_ClockEnable(i2cClock, true);

    /* Output value must be set to 1 to not drive lines low. Set
     SCL first, to ensure it is high before changing SDA. */
    GPIO_PinModeSet(init->sclPort, init->sclPin, gpioModeWiredAndPullUp, 1);
    GPIO_PinModeSet(init->sdaPort, init->sdaPin, gpioModeWiredAndPullUp, 1);

    /* In some situations, after a reset during an I2C transfer, the slave
     device may be left in an unknown state. Send 9 clock pulses to
     set slave in a defined state. */
    for (i = 0; i < 9; i++) {
        GPIO_PinOutSet(init->sclPort, init->sclPin);
        GPIO_PinOutClear(init->sclPort, init->sclPin);
    }

    /* Enable pins and set location */
#if defined (_I2C_ROUTEPEN_MASK)
    init->port->ROUTEPEN = I2C_ROUTEPEN_SDAPEN | I2C_ROUTEPEN_SCLPEN;
    init->port->ROUTELOC0 = (init->portLocationSda << _I2C_ROUTELOC0_SDALOC_SHIFT)
            | (init->portLocationScl << _I2C_ROUTELOC0_SCLLOC_SHIFT);
#else
    init->port->ROUTE = I2C_ROUTE_SDAPEN
    | I2C_ROUTE_SCLPEN
    | (init->portLocation << _I2C_ROUTE_LOCATION_SHIFT);
#endif

    /* Set emlib init parameters */
    i2cInit.enable = true;
    i2cInit.master = true; /* master mode only */
    i2cInit.freq = init->i2cMaxFreq;
    i2cInit.refFreq = init->i2cRefFreq;
    i2cInit.clhr = init->i2cClhr;

    I2C_Init(init->port, &i2cInit);
}

void i2cm_init(void)
{
    RTOS_ERR  err;

#if (configI2C0_USED == 1)
    OSMutexCreate(&s_i2c0_desc.lock_mtx, "I2C0_MTX", &err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
            return;

    OSQCreate(&s_i2c0_desc.transfer_q, "I2C0_Q", 1, &err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
        return;

    i2cm_enable(&i2c0_init);
    NVIC_EnableIRQ(I2C0_IRQn);
#endif

#if (configI2C1_USED == 1)
    OSMutexCreate(&s_i2c1_desc.lock_mtx, "I2C1_MTX", &err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
        return;
    OSQCreate(&s_i2c1_desc.transfer_q, "I2C1_Q", 1, &err);
    if (RTOS_ERR_CODE_GET(err) != RTOS_ERR_NONE)
        return;

    i2cm_enable(&i2c1_init);
    NVIC_EnableIRQ(I2C1_IRQn);
#endif
    CMU_ClockEnable(cmuClock_HFPER, true);
}

I2C_TransferReturn_TypeDef i2cm_transfer(I2CM_ID id, I2C_TransferSeq_TypeDef *seq)
{
    I2C_TransferReturn_TypeDef ret;
    OS_MSG_SIZE msg_size;
    RTOS_ERR  err;

    OSMutexPend(&id->lock_mtx, 0, OS_OPT_PEND_BLOCKING, DEF_NULL, &err); // FIXME introduce timeout sequence
    OSQFlush(&id->transfer_q, &err);

    ret = I2C_TransferInit(id->i2c, seq);
    if (ret != i2cTransferInProgress) {
        OSMutexPost(&id->lock_mtx, OS_OPT_POST_NONE, &err);
        return ret;
    }

    ret = *(I2C_TransferReturn_TypeDef*)OSQPend(&id->transfer_q, (OS_TICK) 0, OS_OPT_PEND_BLOCKING, &msg_size,
            DEF_NULL, &err);
    OSMutexPost(&id->lock_mtx, OS_OPT_POST_NONE, &err);
    return ret;

}


I2C_TransferReturn_TypeDef i2cm_writeData(I2CM_ID id, uint16_t device_addr, uint8_t start_addr,
                                            uint8_t *data, uint16_t data_len)
{
    I2C_TransferSeq_TypeDef transfer = {
        .addr = (device_addr << 1),
        .flags = I2C_FLAG_WRITE_WRITE,
        .buf = {
            {
                .data = &start_addr,
                .len = 1
            },
            {
                .data = data,
                .len = data_len
            }
        }
    };
    return i2cm_transfer(id, &transfer);
}

I2C_TransferReturn_TypeDef i2cm_readData(I2CM_ID id, uint16_t device_addr, uint8_t start_addr,
                                            uint8_t *data, uint16_t data_len)
{
    I2C_TransferSeq_TypeDef transfer = {
        .addr = (device_addr << 1),
        .flags = I2C_FLAG_WRITE_READ,
        .buf = {
            {
                .data = &start_addr,
                .len = 1
            },
            {
                .data = data,
                .len = data_len
            }
        }
    };
    return i2cm_transfer(id, &transfer);
}

/**@}*/
