/**************************************************************************//**
 * Copyright 2017 Silicon Laboratories, Inc.
 *
 * @section License
 * LICENSE_PLACEHOLDER
 *****************************************************************************/

/**************************************************************************//**
 * @file   main.c
 * @brief  A minimal project structure, used as a starting point for custom
 *         Dynamic-Multiprotocol applications. The project has the basic
 *         functionality enabling peripheral connectivity, without GATT
 *         services. It runs on top of Micrium OS RTOS and multiprotocol RAIL.
 *****************************************************************************/
#include "rtos_app.h"

#include <common/include/platform_mgr.h>
#if (OS_CFG_TRACE_EN == DEF_ENABLED)
#include <kernel/include/os_trace.h>
#endif

#include <stdio.h>

#include "em_cmu.h"
#include "sleep.h"
#include "gpiointerrupt.h"
#include "userdata.h"

#include <mbedtls/threading.h>

#include "app_ble.h"
#include "nvm3.h"
#if (configUSE_WDOG > 0)
#include "drv_watchdog.h"
#endif

#include <drivers/fuelgauge/fuelgauge.h>
#include <drivers/pmic/pmic.h>

#if (configUSE_UI_TASK > 0)
#include "udelay.h"
#include "app_ui.h"
#endif

#if (configUSE_HRM > 0)
#include "app_hrm.h"
#endif

#if (configUSE_GPS > 0)
#include "gps.h"
#endif

#if (configUSE_LORA > 0)
#include "lora.h"
#include "sx1276-board.h"
#include "lora_test_cmd.h"
#endif

#if (configUSE_LORA_TEST > 0)
#include "lora_test.h"
#endif

#if (configUSE_ACC_GYRO_SENSOR > 0)
#include "lsm6ds3_acc_gyro.h"
#endif

#include "app_main.h"


/**********************************************************************************************************
 *********************************************************************************************************
 *                                             LOCAL DEFINES
 *********************************************************************************************************
 **********************************************************************************************************/
#undef  __MODULE__
#define __MODULE__ "MAIN"
#undef DBG_MODULE
#define DBG_MODULE 0


#if ( RTOS_CFG_EXTERNALIZE_OPTIONAL_CFG_EN == DEF_ENABLED)
/* This option is useful to reduce the amount of memory and code space used.*/

// Tick Task Configuration
#if (OS_CFG_TASK_TICK_EN == DEF_ENABLED)
/* Setup a 1024 Hz tick instead of the default 1000 Hz. This improves
  accuracy when using dynamic tick which runs of the RTCC at 32768 Hz. */
#define TICK_TASK_PRIO                    KERNEL_TICK_TASK_PRIO_DFLT //0u
#define TICK_TASK_STK_SIZE                256u
static CPU_STK TickTaskStk[TICK_TASK_STK_SIZE];
#define TICK_TASK_CFG                     .TickTaskCfg = \
{                                                        \
    .StkBasePtr = &TickTaskStk[0],                       \
    .StkSize    = (TICK_TASK_STK_SIZE),                  \
    .Prio       = TICK_TASK_PRIO,                        \
    .RateHz     = 1024u                                  \
},
#else
#define TICK_TASK_CFG
#endif


// Idle Task Configuration
#if (OS_CFG_TASK_IDLE_EN == DEF_ENABLED)
#define IDLE_TASK_STK_SIZE                256u
static CPU_STK IdleTaskStk[IDLE_TASK_STK_SIZE];
#define IDLE_TASK_CFG                     .IdleTask = \
{                                                     \
    .StkBasePtr = &IdleTaskStk[0],                    \
    .StkSize    = IDLE_TASK_STK_SIZE                  \
},
#else
#define IDLE_TASK_CFG
#endif


// Timer Task Configuration
#if (OS_CFG_TMR_EN == DEF_ENABLED)
#define TIMER_TASK_PRIO                   4u
#define TIMER_TASK_STK_SIZE               256u
static CPU_STK TimerTaskStk[TIMER_TASK_STK_SIZE];
#define TIMER_TASK_CFG                    .TmrTaskCfg = \
{                                                       \
    .StkBasePtr = &TimerTaskStk[0],                     \
    .StkSize    = TIMER_TASK_STK_SIZE,                  \
    .Prio       = TIMER_TASK_PRIO,                      \
    .RateHz     = 10u                                  \
},
#else
#define TIMER_TASK_CFG
#endif

// ISR Configuration
#define ISR_STK_SIZE                      256u
static CPU_STK ISRStk[ISR_STK_SIZE];
#define ISR_CFG                           .ISR = \
{                                                \
    .StkBasePtr = (CPU_STK*)&ISRStk[0],          \
    .StkSize    = (ISR_STK_SIZE)                 \
},


#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
#define STAT_TASK_STK_SIZE               256u
static  CPU_STK  StatTaskStk[STAT_TASK_STK_SIZE];
#define STAT_TASK_CFG          .StatTaskCfg = \
{                                             \
    .StkBasePtr = (CPU_STK*)&StatTaskStk[0],  \
    .StkSize    = STAT_TASK_STK_SIZE,         \
    .Prio       = KERNEL_STAT_TASK_PRIO_DFLT, \
    .RateHz     = 10u                         \
},
#else
#define STAT_TASK_CFG
#endif


#define  OS_INIT_CFG_APP            { \
    ISR_CFG                           \
    IDLE_TASK_CFG                     \
    TICK_TASK_CFG                     \
    TIMER_TASK_CFG                    \
    STAT_TASK_CFG                     \
    .MsgPoolSize     = 100u,            \
    .TaskStkLimit    = 10u,            \
    .MemSeg          = DEF_NULL       \
}

#if (RTOS_CFG_LOG_EN == DEF_ENABLED)
// Logging task
#define LOGGING_TASK_PRIO                 20u
#define LOGGING_TASK_STACK_SIZE           (1000 / sizeof(CPU_STK))
static CPU_STK loggingTaskStk[LOGGING_TASK_STACK_SIZE];
static OS_TCB  loggingTaskTCB;

#define COMMON_INIT_CFG_APP  {    \
    .CommonMemSegPtr = DEF_NULL,    \
    .LoggingCfg =                   \
    {                               \
      .AsyncBufSize = 512u          \
    },                              \
    .LoggingMemSegPtr = DEF_NULL    \
}

#else

#define COMMON_INIT_CFG_APP {   \
    .CommonMemSegPtr = DEF_NULL \
}

#endif

#define PLATFORM_MGR_INIT_CFG_APP { \
    .PoolBlkQtyInit = 0u,           \
    .PoolBlkQtyMax  = LIB_MEM_BLK_QTY_UNLIMITED \
}

const  OS_INIT_CFG          OS_InitCfg          = OS_INIT_CFG_APP;
const COMMON_INIT_CFG       Common_InitCfg      = COMMON_INIT_CFG_APP;
const PLATFORM_MGR_INIT_CFG PlatformMgr_InitCfg = PLATFORM_MGR_INIT_CFG_APP;

#ifdef  RTOS_MODULE_COMMON_SHELL_AVAIL
const  SHELL_INIT_CFG  Shell_InitCfg = {\
        .CfgCmdUsage =                  \
        {                               \
                .CmdTblItemNbrInit = 10u,   \
                .CmdTblItemNbrMax  = 10u,   \
                .CmdArgNbrMax      = 13u,   \
                .CmdNameLenMax     = 10u    \
            },                              \
            .MemSegPtr = DEF_NULL           \
        };
#endif

#endif


#if (configUSE_WDOG > 0)
int8_t idle_task_wdog_id = -1;
#endif


// Micrium OS hooks
static void idleHook(void);
static void setupHooks(void);
static void peri_gpio_init(void)
{
    /*********************************************************************/
    /** I2C config                                                      **/
    /*********************************************************************/
    GPIO_PinModeSet(BSP_I2C0_SCL_PORT, BSP_I2C0_SCL_PIN, gpioModeDisabled, 0);
    GPIO_PinModeSet(BSP_I2C0_SDA_PORT, BSP_I2C0_SDA_PIN, gpioModeDisabled, 0);

    GPIO_PinModeSet(BSP_I2C1_SCL_PORT, BSP_I2C1_SCL_PIN, gpioModeDisabled, 0);
    GPIO_PinModeSet(BSP_I2C1_SDA_PORT, BSP_I2C1_SDA_PIN, gpioModeDisabled, 0);

    /*********************************************************************/
    /** PMIC config                                                     **/
    /*********************************************************************/
    GPIO_PinModeSet(BSP_PMIC_ALRT_PORT, BSP_PMIC_ALRT_PIN, gpioModePushPull, 1);
    GPIO_PinModeSet(BSP_PMIC_INT_PORT, BSP_PMIC_INT_PIN, gpioModePushPull, 1);

#if (configUSE_MOTER > 0)
    /*********************************************************************/
    /** Initialize Motor                                                **/
    /*********************************************************************/
    GPIO_PinModeSet(BSP_MOTOR_EN_PORT, BSP_MOTOR_EN_PIN, gpioModePushPull, 0);
#endif

    /*********************************************************************/
    /** Configure the Interrupt pins                                    **/
    /*********************************************************************/
#if (configUSE_HRM > 0)
    GPIO_PinModeSet(BSP_HRM_INT_PORT, BSP_HRM_INT_PIN, gpioModeInputPull, 1);
#endif//(configUSE_HRM > 0)

#if (configUSE_ACC_GYRO_SENSOR > 0)
    GPIO_PinModeSet(BSP_ACC_GYRO_INT1_PORT, BSP_ACC_GYRO_INT1_PIN, gpioModeInput, 0);
    GPIO_PinModeSet(BSP_ACC_GYRO_INT2_PORT, BSP_ACC_GYRO_INT2_PIN, gpioModeInput, 0);
#endif

#if (configUSE_GPS > 0)
    /* this is to prevent GPS from wrong operation of uart */
    GPIO_PinModeSet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN, gpioModePushPull, 0);// default OFF
#endif

#if (configUSE_LORA > 0)
    GPIO_PinModeSet(BSP_LORA_EN_PORT, BSP_LORA_EN_PIN, gpioModePushPull, 1);

    SX1276IoInit();
#endif

}


static CPU_STK exMainStartTaskStk[EX_MAIN_START_TASK_STK_SIZE];
static OS_TCB  exMainStartTaskTCB;
static void    exMainStartTask(void *p_arg);


/**************************************************************************//**
 * Main.
 *
 * @returns Returns 1.
 *
 * This is the standard entry point for C applications. It is assumed that your
 * code will call main() once you have performed all necessary initialization.
 *****************************************************************************/
int main(void)
{
    RTOS_ERR err;
    BSP_SystemInit(peri_gpio_init);

#if (configUSE_DISPLAY > 0)
    UDELAY_Calibrate(); // oled u sec delay
#endif

    // init here because of retargeted printf
    TerminalSerial_Init();

    debug_printf_data("\n");
    debug_printf_data("----------------------------------------------\n");
    debug_printf_data(" %s\n", SILABS_AF_DEVICE_NAME);
    debug_printf_data("  - %-12s : %s\n", "BOARD TYPE", kBoardName(LINEABLE_BOARD_TYPE));
    debug_printf_data("  - %-12s : %s (0x%08X)\n", "VERSION", FIRMWARE_VERSION_STR, FIRMWARE_VERSION_NUMBER);
    debug_printf_data("  - %-12s : %s - %s\n", "BUILD TIME", __DATE__, __TIME__);
    debug_printf_data("----------------------------------------------\n");
    debug_printf_data("  - %-12s : %s (%s)\n", "LoRa", "CLASS A", LORA_INFO_BAND);
    debug_printf_data("----------------------------------------------\n");


#if (RTOS_CFG_EXTERNALIZE_OPTIONAL_CFG_EN == DEF_DISABLED)
    /*********************
    * Configure the number of messages managed by the kernel.
    * pecify the number of messages managed by the kernel. (Optional. If not called, default values will be used.)
    */
    //OS_ConfigureMsgPoolSize(APP_KERNEL_MAX_MESSAGES);

    /* Configure the Idle Task's stack.          */
    //OS_ConfigureIdleTaskStk(&App_IdleTaskStk[0],
    //                            APP_KERNEL_IDLE_TASK_STK_SIZE);
    // Setup a 1024 Hz tick instead of the default 1000 Hz. This improves
    // accuracy when using dynamic tick which runs of the RTCC at 32768 Hz.
    OS_TASK_CFG tickTaskCfg = {
        .StkBasePtr = DEF_NULL,
        .StkSize    = 256u,
        .Prio       = KERNEL_TICK_TASK_PRIO_DFLT,
        .RateHz     = 1024u
    };
    OS_ConfigureTickTask(&tickTaskCfg);
#endif

    // Initialize the Kernel.
    OSInit(&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);

    // Initialize Kernel tick source.
    BSP_TickInit();

    // Setup the Micrium OS hooks.
    setupHooks();

    THREADING_setup();
    // Create the Ex Main Start task
    OSTaskCreate(&exMainStartTaskTCB,
                 "Ex Main Start Task",
                 exMainStartTask,
                 DEF_NULL,
                 EX_MAIN_START_TASK_PRIO,
                 &exMainStartTaskStk[0],
                 (EX_MAIN_START_TASK_STK_SIZE / 10u),
                 EX_MAIN_START_TASK_STK_SIZE,
                 0u,
                 0u,
                 DEF_NULL,
                 (OS_OPT_TASK_STK_CLR),
                 &err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);

    // Start the kernel.
    OSStart(&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);

    return (1);
}

#if (RTOS_CFG_LOG_EN == DEF_ENABLED)
void loggingTask(void *p_arg)
{
    PP_UNUSED_PARAM(p_arg);
    RTOS_ERR err;
    OS_SEM_CTR ctr;

    while (DEF_TRUE) {
        // Block until another task signals this task.
        ctr = OSTaskSemPend(0,
                        OS_OPT_PEND_BLOCKING,
                        DEF_NULL,
                        &err);
        APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), RTOS_ERR_CODE_GET(err));
        Log_Output();
    }
}
#endif

/***************************************************************************//**
 * This is the idle hook.
 *
 * This will be called by the Micrium OS idle task when there is no other task
 * ready to run. We just enter the lowest possible energy mode.
 ******************************************************************************/
extern void SleepAndSyncProtimer(void);
static void idleHook(void)
{
#if (configUSE_WDOG > 0)
    if(idle_task_wdog_id != -1){
        drv_watchdog_notify(idle_task_wdog_id);
    }
#endif
#if (RTOS_CFG_LOG_EN == DEF_ENABLED)
    CPU_BOOLEAN dataIsAvail;
    RTOS_ERR err;
    OS_SEM_CTR ctr;

    dataIsAvail = Log_DataIsAvail();
    if (dataIsAvail) {
        ctr = OSTaskSemPost(&loggingTaskTCB, OS_OPT_POST_NONE, &err);
        APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), RTOS_ERR_CODE_GET(err));
    } else
#endif
    {
    // Put MCU in the lowest sleep mode available, usually EM2.
      SleepAndSyncProtimer();
    }
}

/***************************************************************************//**
 * Setup the Micrium OS hooks.
 *
 * Setup the Micrium OS hooks. We are only using the idle task hook in this
 * example. See the Mcirium OS documentation for more information on the other
 * available hooks.
 ******************************************************************************/
static void setupHooks(void)
{
    CPU_SR_ALLOC();

    CPU_CRITICAL_ENTER();
    SLEEP_SleepBlockBegin(sleepEM2);        /* Don't allow EM3, since we use LF clocks. */
    OS_AppIdleTaskHookPtr = idleHook;
    CPU_CRITICAL_EXIT();
}

/**************************************************************************//**
 * Task to initialise Bluetooth and Proprietary tasks.
 *
 * @param p_arg Pointer to an optional data area which can pass parameters to
 *              the task when the task executes.
 *
 * This is the task that will be called by from main() when all services are
 * initialized successfully.
 *****************************************************************************/
static void exMainStartTask(void *p_arg)
{
    PP_UNUSED_PARAM(p_arg);
    RTOS_ERR err;

#if (OS_CFG_STAT_TASK_EN == DEF_ENABLED)
    // Initialize CPU Usage.
    OSStatTaskCPUUsageInit(&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE),;);
#endif

#if (OS_CFG_TRACE_EN == DEF_ENABLED)
    OS_TRACE_INIT();    /* Initialize the Kernel events trace recorder.         */
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    // Initialize interrupts disabled measurement.
    CPU_IntDisMeasMaxCurReset();
#endif

#if (configUSE_WDOG > 0)
    /*
     * Initialize platform watchdog
     */
    drv_watchdog_init();
    // Register the Idle task first.
    idle_task_wdog_id = drv_watchdog_register();
    APP_RTOS_ASSERT_DBG((idle_task_wdog_id != -1), ;);
    drv_watchdog_configure_idle_id(idle_task_wdog_id);
#endif

    // Initialise common module
    Common_Init(&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);

#ifdef  RTOS_MODULE_COMMON_SHELL_AVAIL
    APP_TRACE_DEBUG("Initialize Shell ... \n");
    Shell_Init(&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);
#endif

#ifdef  RTOS_MODULE_COMMON_CLK_AVAIL
    APP_TRACE_DEBUG("Initialize ClK module...\n");
    Clk_Init(&err);
    APP_RTOS_ASSERT_CRITICAL(err.Code == RTOS_ERR_NONE, ;);
    {
        CLK_DATE_TIME         date_time;
        CPU_CHAR              date_time_str[30];

        if (DEF_OK == Clk_GetDateTime(&date_time)) {
            Clk_DateTimeToStr(&date_time,
                              CLK_STR_FMT_YYYY_MM_DD_HH_MM_SS_UTC,
                              date_time_str,
                              30);

            APP_TRACE_DEBUG("SAVED(or EPOC(2018-06-20 00:00:00)) TIME\r\n");
            APP_TRACE_DEBUG("\t= %s\r\n", date_time_str);
        } else {
            /* Last step */
            if (DEF_OK == Clk_DateTimeMake(&date_time,
                                           2019u,
                                           CLK_MONTH_AUG,
                                           02u,
                                           7u,
                                           8u,
                                           9u,
                                           CLK_TZ_SEC_FROM_UTC_GET(0))) {

                Clk_SetDateTime(&date_time);
                APP_TRACE_DEBUG("NEWLY SET TIME(hard to happen)\r\n");
            }
        }
    }
#endif

    // Bradnon: not verified
    //Auth_Init(&err);
    //APP_RTOS_ASSERT_CRITICAL(err.Code == RTOS_ERR_NONE, ;);

    // Initialize the BSP. It is expected that the BSP will register all the hardware controller to
    // the platform manager at this moment. After the call to the function Common_Init().
    BSP_PeriphInit();
    GPIOINT_Init();

    debugInit();

#if (RTOS_CFG_LOG_EN == DEF_ENABLED)
  // Create the Logging task
    OSTaskCreate(&loggingTaskTCB,
                 "Logging Task",
                 loggingTask,
                 0u,
                 LOGGING_TASK_PRIO,
                 &loggingTaskStk[0u],
                 (LOGGING_TASK_STACK_SIZE / 10u),
                 LOGGING_TASK_STACK_SIZE,
                 0u,
                 0u,
                 0u,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), RTOS_ERR_CODE_GET(err));
#endif


if (PMIC_Init(configPMIC_INTERFACE) != PMIC_OK) {
	APP_TRACE_DEBUG("PMIC Init error\n");
}
if(FUELGAUGE_Initialize() != FUELGAUGE_OK) {
	APP_TRACE_DEBUG("Fuel gauge Init error\n");
}

#if 0
    // Create the Proprietary Application task
    ble_task_start(BLUETOOTH_APP_TASK_PRIO);
#endif

#if (configUSE_UI_TASK > 0)
    ui_task_start(UI_APP_TASK_PRIO);
#endif

#if (configUSE_GPS > 0)
    gps_task_start(GPS_APP_TASK_PRIO);
#endif

#if (configUSE_HRM > 0)
    hrm_task_start(HRM_APP_TASK_PRIO);
#endif

        MainApp_Init();

    if (UNIT_FACTORY_TEST) {
        LoRaTestInit();

        #ifdef RTOS_MODULE_COMMON_SHELL_AVAIL
        Shell_CmdTblAdd("Common", pShellCommonCmds, &err);
        Shell_CmdTblAdd("TestCmds", pShellTestCmds, &err);
        #endif
    } else {
        #if ((LIB_MEM_CFG_DBG_INFO_EN == DEF_ENABLED) && \
             (LIB_MEM_CFG_HEAP_SIZE > 0) && \
             (APP_TRACE_LEVEL > TRACE_LEVEL_DBG))
        Mem_OutputUsage((void *)printf,  &err);
        #endif

        #ifdef RTOS_MODULE_COMMON_SHELL_AVAIL
        Shell_CmdTblAdd("Common", pShellCommonCmds, &err);

        #if configUSE_SKT_TEST_CMD
        Shell_CmdTblAdd("TestCmds", pShellTestCmds, &err);
        #endif
        #endif

        MainApp_Start();
    }

    // Create the Proprietary Application task
    ble_task_start(BLUETOOTH_APP_TASK_PRIO);

    // Done starting everyone else so let's exit.
    OSTaskDel((OS_TCB *)0, &err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), 1);
}
