#ifndef BSPHALCONFIG_H
#define BSPHALCONFIG_H

#include  <bsphalconfig_defines.h>
#include  "ble-configuration.h"

#if    (LINEABLE_BOARD_TYPE == TATA_FACTORY_ES1)
#define DEVICETYPE_DEFAULT      (20)
#elif  (LINEABLE_BOARD_TYPE == TATA_FACTORY_REFERENCE)
#define DEVICETYPE_DEFAULT      (20)
#elif  (LINEABLE_BOARD_TYPE == MINI_REFERENCE)
#define DEVICETYPE_DEFAULT      (20)
#elif  (LINEABLE_BOARD_TYPE == LINEABLE_ONE)
#define DEVICETYPE_DEFAULT      (20)
#elif  (LINEABLE_BOARD_TYPE == LINEABLE_ONE_REV03)
#define DEVICETYPE_DEFAULT      (20)
#else
#error "Board is not defined"
#endif

/* port configuration, generated from hardware configuration tool */
#include "hal-config-board.h"


#ifdef BSP_CLK_LFXO_CTUNE
#undef BSP_CLK_LFXO_CTUNE
#endif
#define BSP_CLK_LFXO_CTUNE                            (32)

#define HAL_EXTFLASH_FREQUENCY                        (1000000)

#define HAL_PA_ENABLE                                 (1)
#define HAL_PA_RAMP                                   (10)
#define HAL_PA_2P4_LOWPOWER                           (0)
#define HAL_PA_POWER                                  (252)
#define HAL_PA_CURVE_HEADER                            "pa_curves_efr32.h"
#ifdef FEATURE_PA_HIGH_POWER
#define HAL_PA_VOLTAGE                                (3300)
#else // FEATURE_PA_HIGH_POWER
#define HAL_PA_VOLTAGE                                (1800)
#endif // FEATURE_PA_HIGH_POWER


#define HAL_PTI_ENABLE                                (0)
#define HAL_PTI_MODE                                  (HAL_PTI_MODE_UART)
#define HAL_PTI_BAUD_RATE                             (1600000)





/**********************************************************************************************************
 *********************************************************************************************************
 *                                     CLOCK CONFIGURATION
 *********************************************************************************************************
 **********************************************************************************************************/

#define  BSP_HF_CLK_SEL                                     BSP_HF_CLK_HFRCO

#define  BSP_LF_CLK_SEL                                     BSP_LF_CLK_LFRCO


#if BSP_DCDC_PRESENT
#define configUSE_DCDC                                      (1)
#endif

#define configUSE_SPI_FLASH                                 (0)


/**
 * \brief Watchdog Service
 *
 * - 1: enabled
 * - 0: disabled
 *
 */
#define configUSE_WDOG                                      (0)
/**
 * \brief Maximum watchdog tasks
 *
 * Maximum number of tasks that the Watchdog Service can monitor. It can be larger (up to 32) than
 * needed
*/
#define configWDOG_MAX_TASKS_CNT                             5

#define configI2C0_USED 1
#define configI2C1_USED 1

// PMIC
#define PMIC_DEVICE_NONE                                      0
#define PMIC_DEVICE_MAX14676                                  1
#define PMIC_DEVICE_TPS65720                                  2    /* Rev0.3 board */
#define configUSE_PMIC                                       (PMIC_DEVICE_TPS65720)

#define configPMIC_INTERFACE 							  	  i2c0_id
//Fuel gauge(Rev0.3 only)
#if (configUSE_PMIC == PMIC_DEVICE_TPS65720)
#define configUSE_FUELGAUGE                                  1
#endif

#define HRM_DEVICE_NONE                                      0
#define HRM_DEVICE_SI117                                     1
#define HRM_DEVICE_MAX30101                                  2
#define configUSE_HRM                                       (HRM_DEVICE_SI117)

#if ((LINEABLE_BOARD_TYPE == TATA_FACTORY_ES1 && configUSE_HRM == HRM_DEVICE_MAX30101))
#error "WRONG HRM configuration"
#endif

/*--- Customer -----------------------------------------------------------------------------------*/
#define SKT_KR920                                           (1)
#define SWISSCOM_EU868                                      (2)
#define RYOSHO_AS923                                        (3)
#define SPEZIA_AS923                                        (4)
#define COMCAST_US915                                       (5)
#define NNNCo_AS923                                         (6)
#define LORA_VENDOR                                         SKT_KR920
/*--- LoRa ---------------------------------------------------------------------------------------*/
#define configUSE_LORA                                      (1)
#define configUSE_LORA_TEST                                 (1)
#define configUSE_SKT_TEST_CMD                              (1)

#if   (LORA_VENDOR == SKT_KR920)
    #define REGION_KR920
    #define USE_SKT_FORMAT                                  (1)
    #define USE_SKT_OTB_TEST                                (0)    //when OTB or OTA, then high(1)
    #define USE_SKT_OTA_TEST                                (0)
    #define LORA_INFO_BAND                                  "KR920"
    #define LORA_SUPPORT_TIMESYNC                           (1)
#elif (LORA_VENDOR == SWISSCOM_EU868)
    #define REGION_EU868
    #define USE_SKT_FORMAT                                  (0)
    #define USE_SKT_OTB_TEST                                (0)    //when OTB or OTA, then high(1)
    #define USE_SKT_OTA_TEST                                (0)
    #define LORA_INFO_BAND                                  "EU868"
    #define LORA_SUPPORT_TIMESYNC                           (1)
#elif (LORA_VENDOR == RYOSHO_AS923)
    #define REGION_AS923
    #define USE_SKT_FORMAT                                  (0)
    #define USE_SKT_OTB_TEST                                (0)    //when OTB or OTA, then high(1)
    #define USE_SKT_OTA_TEST                                (0)
    #define LORA_INFO_BAND                                  "AS923"
    #define LORA_SUPPORT_TIMESYNC                           (1)
#elif (LORA_VENDOR == SPEZIA_AS923)
    #define REGION_AS923
    #define USE_SKT_FORMAT                                  (0)
    #define USE_SKT_OTB_TEST                                (0)    //when OTB or OTA, then high(1)
    #define USE_SKT_OTA_TEST                                (0)
    #define USE_SPEZIA_OR_SCNEX                             (1)
    #define LORA_INFO_BAND                                  "AS923"
    #define LORA_SUPPORT_TIMESYNC                           (1)
#elif (LORA_VENDOR == COMCAST_US915)
    #define REGION_US915
    #define USE_SKT_FORMAT                                  (0)
    #define USE_SKT_OTB_TEST                                (0)    //when OTB or OTA, then high(1)
    #define USE_SKT_OTA_TEST                                (0)
    #define USE_COMCAST                                     (0)
    #define LORA_INFO_BAND                                  "US915"
    #define LORA_SUPPORT_TIMESYNC                           (1)
#elif (LORA_VENDOR == NNNCo_AS923)
    #define REGION_AS923
    #define USE_SKT_FORMAT                                  (0)
    #define USE_SKT_OTB_TEST                                (0)    //when OTB or OTA, then high(1)
    #define USE_SKT_OTA_TEST                                (0)
    #define LORA_INFO_BAND                                  "AS923"
    #define LORA_SUPPORT_TIMESYNC                           (1)
#else
#error "Select custmer"
#endif

// Accelerometer
#define configUSE_ACC_GYRO_SENSOR                           (1)

#define configUSE_UI_TASK                                   (0)
#if     (configUSE_UI_TASK == 1)
#define configUSE_DISPLAY                                   (1)
#else
#define configUSE_DISPLAY                                   (0)
#endif

// GPS
#define configUSE_GPS                                       (1)

// BLE                                            just one of four modes!!!
#define config_BLE_OFF_MODE                                 (0)
#define config_BLE_PERIPHERAL_MODE                          (1)
#define config_BLE_CENTRAL_MODE                             (0)
#define config_BLE_DUAL_MODE                                (0)

// HAL config is only supported on STK platform
#define configUSE_LEDS                                      (0)

#define configUSE_BUTTON                                    (0)

#define configUSE_TOUCH_CSENS                               (0)   /* Capacitive Sense (CSEN) */

#define configUSE_MOTER                                     (0)
#define configUSE_GET_BOOTINFO                              (1)

#if HAL_PTI_ENABLE && !defined(RAIL_PTI_CONFIG)
#if HAL_PTI_MODE == HAL_PTI_MODE_SPI
#define RAIL_PTI_CONFIG                                                   \
  {                                                                       \
    RAIL_PTI_MODE_SPI,     /* SPI mode */                                 \
    HAL_PTI_BAUD_RATE,     /* Baud rate */                                \
    BSP_PTI_DOUT_LOC,      /* DOUT location */                            \
    BSP_PTI_DOUT_PORT,     /* Get the port for this loc */                \
    BSP_PTI_DOUT_PIN,      /* Get the pin, location should match above */ \
    BSP_PTI_DCLK_LOC,      /* DCLK location */                            \
    BSP_PTI_DCLK_PORT,     /* Get the port for this loc */                \
    BSP_PTI_DCLK_PIN,      /* Get the pin, location should match above */ \
    BSP_PTI_DFRAME_LOC,    /* DFRAME location */                          \
    BSP_PTI_DFRAME_PORT,   /* Get the port for this loc */                \
    BSP_PTI_DFRAME_PIN,    /* Get the pin, location should match above */ \
  }
#elif HAL_PTI_MODE == HAL_PTI_MODE_UART
#define RAIL_PTI_CONFIG                                                   \
  {                                                                       \
    RAIL_PTI_MODE_UART,    /* UART mode */                                \
    HAL_PTI_BAUD_RATE,     /* Baud rate */                                \
    BSP_PTI_DOUT_LOC,      /* DOUT location */                            \
    BSP_PTI_DOUT_PORT,     /* Get the port for this loc */                \
    BSP_PTI_DOUT_PIN,      /* Get the pin, location should match above */ \
    0,                     /* No DCLK in UART mode */                     \
    0,                     /* No DCLK in UART mode */                     \
    0,                     /* No DCLK in UART mode */                     \
    BSP_PTI_DFRAME_LOC,    /* DFRAME location */                          \
    BSP_PTI_DFRAME_PORT,   /* Get the port for this loc */                \
    BSP_PTI_DFRAME_PIN,    /* Get the pin, location should match above */ \
  }
#elif HAL_PTI_MODE == HAL_PTI_MODE_UART_ONEWIRE
#define RAIL_PTI_CONFIG                                                        \
  {                                                                            \
    RAIL_PTI_MODE_UART_ONEWIRE, /* UART onewire mode */                        \
    HAL_PTI_BAUD_RATE,          /* Baud rate */                                \
    BSP_PTI_DOUT_LOC,           /* DOUT location */                            \
    BSP_PTI_DOUT_PORT,          /* Get the port for this loc */                \
    BSP_PTI_DOUT_PIN,           /* Get the pin, location should match above */ \
    0,                          /* No DCLK in UART onewire mode */             \
    0,                          /* No DCLK in UART onewire mode */             \
    0,                          /* No DCLK in UART onewire mode */             \
    0,                          /* No DFRAME in UART onewire mode */           \
    0,                          /* No DFRAME in UART onewire mode */           \
    0,                          /* No DFRAME in UART onewire mode */           \
  }
#else
  #error "Invalid PTI mode (HAL_PTI_MODE)"
#endif
#endif

#if !defined(RAIL_PA_2P4_CONFIG)
// HAL Config 2.4 GHz PA configuration enabled
#define RAIL_PA_2P4_CONFIG                                               \
  {                                                                      \
    RAIL_TX_POWER_MODE_2P4_HP,    /* Power Amplifier mode */             \
    HAL_PA_VOLTAGE,               /* Power Amplifier vPA Voltage mode */ \
    HAL_PA_RAMP,                  /* Desired ramp time in us */          \
  }
#endif

#if !defined(RAIL_PA_SUBGIG_CONFIG)
// HAL Config sub-GHz PA configuration enabled
#define RAIL_PA_SUBGIG_CONFIG                                            \
  {                                                                      \
    RAIL_TX_POWER_MODE_SUBGIG,    /* Power Amplifier mode */             \
    HAL_PA_VOLTAGE,               /* Power Amplifier vPA Voltage mode */ \
    HAL_PA_RAMP,                  /* Desired ramp time in us */          \
  }
#endif

#if defined(HAL_PA_POWER) && !defined(RAIL_PA_DEFAULT_POWER)
#define RAIL_PA_DEFAULT_POWER         HAL_PA_POWER
#endif

#if defined(HAL_PA_CURVE_HEADER) && !defined(RAIL_PA_CURVES)
#define RAIL_PA_CURVES                HAL_PA_CURVE_HEADER
#endif


/**************************************************************************
*TRACE CONFIG
***************************************************************************/
// Series 1 SWO pin has its own route location
#define BSP_TRACE_SWO_LOCATION     BSP_TRACE_SWO_LOC

/* Enable output on pin - GPIO Port F, Pin 2. */
#define TRACE_ENABLE_PINS()           \
  GPIO_PinModeSet(BSP_TRACE_SWO_PORT, \
                  BSP_TRACE_SWO_PIN,  \
                  gpioModePushPull,   \
                  1)

/*  No ETM trace */
#if 0
#define BSP_ETM_TRACE              /* This board supports ETM trace. */
#define BSP_TRACE_ETM_CLKLOC     0 /* ETM_TCLK = PF8  */
#define BSP_TRACE_ETM_TD0LOC     0 /* ETM_TD0  = PF9  */
#define BSP_TRACE_ETM_TD1LOC     0 /* ETM_TD1  = PF10 */
#define BSP_TRACE_ETM_TD2LOC     0 /* ETM_TD2  = PF11 */
#define BSP_TRACE_ETM_TD3LOC     0 /* ETM_TD3  = PF12 */
#endif

//#define CONFIG_RETARGET         0 // UART,  2: RTT(not implemented)

#define BSP_SERIAL_APP_PORT  (HAL_SERIAL_PORT_USART0)

/**
 ****************************************************************************************
 * \name                        Historical Data Settings
 ****************************************************************************************
 * \{ */
/**
 * \brief Enable/disable historical data storage
 *
 * 0:disable, 1:enable
 */
#define configUSE_HISTORY_STORAGE                        (0)

/**
 * \brief Configure historical data storage
 */
#if (configUSE_HISTORY_STORAGE)

    /**
     * \brief Converts 24h format time to minutes
     *
     * \param [in] hours                    Hours
     * \param [in] mins                     Minutes
     */
#define TIME_TO_MINS(hours, mins)                ((hours * 60) + mins)

    /**
     * \brief Historical data day minutes definition
     */
#define HIST_DAY_MINS                            TIME_TO_MINS(24, 00)
#define HIST_HR_SIZE                             (HIST_DAY_MINS * 3)
#define HIST_STEP_SIZE                           (HIST_DAY_MINS * 3)
#define HIST_CAL_SIZE                            (HIST_DAY_MINS * 3)

#if HIST_HR_SIZE < 17 || HIST_STEP_SIZE < 17 || HIST_CAL_SIZE < 17
#error "Please, revise historical flash area sizes."
#endif

#endif /* configUSE_HISTORY_STORAGE */
/** \} */




/**
 * \brief Configure magnetometer sensor
 *
 * Time (in msec) to pass for the magnetometer sensor data to be considered calibrated. (0:disable)
 */
#define MAG_CAL_INITIALIZATION_TIME                     (500)
/** \} */

/**
 ****************************************************************************************
 * \name                Sensor Settings (Temperature)  ENV sensor
 ****************************************************************************************
 * \{ */
/**
 * \brief Select temperature sensor module
 * - SENSOR_NONE
 * - SENSOR_BME280
 */
#define SC_TEMPERATURE_SENSOR                           (SENSOR_NONE)

/**
 * \brief Enable/disable temperature sensor data
 *
 * 0:disable, 1:enable
 */
#define TEMPERATURE_DATA_ENABLE                         (1)


/**
 ****************************************************************************************
 * \name                        Sensor Settings (Pressure)
 ****************************************************************************************
 * \{ */
/**
 * \brief Select pressure sensor module
 * - SENSOR_NONE
 * - SENSOR_BME280
 */
#define SC_PRESSURE_SENSOR                                 (SENSOR_NONE)//(SENSOR_BME280)

/**
 * \brief Enable/disable pressure sensor data
 *
 * 0:disable, 1:enable
 */
#define PRESSURE_DATA_ENABLE                            (1)

/** \} */

/**
 ****************************************************************************************
 * \name                        Sensor Settings (Humidity)
 ****************************************************************************************
 * \{ */
/**
 * \brief Select humidity sensor module
 * - SENSOR_NONE
 * - SENSOR_BME280
 */
#define SC_HUMIDITY_SENSOR                                 (SENSOR_NONE)//(SENSOR_BME280)

/**
 * \brief Enable/disable humidity sensor data
 *
 * 0:disable, 1:enable
 */
#define HUMIDITY_DATA_ENABLE                            (1)

/** \} */

/**
 ****************************************************************************************
 * \name                        Sensor Settings (Health Care Optical)
 ****************************************************************************************
 * \{ */
/**
 * \brief Select health care optical sensor module
 * - SENSOR_NONE
 * - SENSOR_DI5115
 */
#define SC_OPTICAL_SENSOR                               (SENSOR_NONE)//(SENSOR_DI5115)
/** \} */

/**
 * \brief Enable/disable heart rate estimation service data
 *
 * 0:disable, 1:enable
 */
#define HEART_RATE_DATA_ENABLE                          (1)

/** \} */

/**
 * \brief Enable/disable step counting service data
 *
 * 0:disable, 1:enable
 */
#define STEP_COUNTING_DATA_ENABLE                       (1)

/** \} */

/**
 ****************************************************************************************
 * \name                        Service Settings (Calories Counting)
 ****************************************************************************************
 * \{ */
/**
 * \brief Select calories counting service
 * - SENSOR_NONE
 * - SERVICE_HC
 * - SERVICE_KIWI
 */
#define CALORIES_COUNTING_SERVICE                       (SERVICE_HC)

/**
 * \brief Enable/disable calories counting service data
 *
 * 0:disable, 1:enable
 */
#define CALORIES_COUNTING_DATA_ENABLE                   (1)


/** \} */


/**
 ****************************************************************************************
 * \name                        Service Settings (Body State Classification)
 ****************************************************************************************
 * \{ */
/**
 * \brief Select body state classification service
 * - SENSOR_NONE
 * - SERVICE_KIWI
 */
#define BODY_STATE_CLASSIF_SERVICE                      (SERVICE_NONE)

/**
 * \brief Enable/disable body state classification service data
 *
 * 0:disable, 1:enable
 */
#define BODY_STATE_CLASSIF_DATA_ENABLE                  (0)
/** \} */


#include <bsphalconfig_defaults.h>

#endif // BSPHALCONFIG_H
