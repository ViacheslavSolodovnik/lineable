/*
*********************************************************************************************************
*                                             EXAMPLE CODE
*********************************************************************************************************
* Licensing:
*   The licensor of this EXAMPLE CODE is Silicon Laboratories Inc.
*
*   Silicon Laboratories Inc. grants you a personal, worldwide, royalty-free, fully paid-up license to
*   use, copy, modify and distribute the EXAMPLE CODE software, or portions thereof, in any of your
*   products.
*
*   Your use of this EXAMPLE CODE is at your own risk. This EXAMPLE CODE does not come with any
*   warranties, and the licensor disclaims all implied warranties concerning performance, accuracy,
*   non-infringement, merchantability and fitness for your application.
*
*   The EXAMPLE CODE is provided "AS IS" and does not come with any support.
*
*   You can find user manuals, API references, release notes and more at: https://doc.micrium.com
*
*   You can contact us at: https://www.micrium.com
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                           RTOS DESCRIPTION
*
*                                      CONFIGURATION TEMPLATE FILE
*
* File : rtos_description.h
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*********************************************************************************************************
*                                               MODULE
*********************************************************************************************************
*********************************************************************************************************
*/

#ifndef  _RTOS_DESCRIPTION_H_
#define  _RTOS_DESCRIPTION_H_


/*
*********************************************************************************************************
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*********************************************************************************************************
*/

#include  <common/include/rtos_opt_def.h>


/*
*********************************************************************************************************
*********************************************************************************************************
*                                       ENVIRONMENT DESCRIPTION
*********************************************************************************************************
*********************************************************************************************************
*/

#define  RTOS_CPU_SEL                                       RTOS_CPU_SEL_SILABS_GECKO_AUTO	//RTOS_CPU_SEL_ARM_V7_M

#define  RTOS_TOOLCHAIN_SEL                                 RTOS_TOOLCHAIN_AUTO

#define  RTOS_INT_CONTROLLER_SEL                            RTOS_INT_CONTROLLER_AUTO //RTOS_INT_CONTROLLER_ARMV7_M


/*
*********************************************************************************************************
*********************************************************************************************************
*                                       RTOS MODULES DESCRIPTION
*********************************************************************************************************
*********************************************************************************************************
*/

                                                                /* ---------------------- KERNEL ---------------------- */
#define  RTOS_MODULE_KERNEL_AVAIL

                                                                /* ---------------------- COMMON ---------------------- */
#define  RTOS_MODULE_COMMON_CLK_AVAIL
#define  RTOS_MODULE_COMMON_SHELL_AVAIL

                                                                /* ------------------------ IO ------------------------ */
//#define  RTOS_MODULE_IO_AVAIL

//#define  RTOS_MODULE_IO_SERIAL_AVAIL
//#define  RTOS_MODULE_IO_SERIAL_SPI_AVAIL
                                                                /* ------------------- FILE SYSTEM -------------------- */
                                                                /* --------------------- NETWORK ---------------------- */


                                                                /* -------------------- USB DEVICE -------------------- */
                                                                /* --------------------- USB HOST --------------------- */
/*
*********************************************************************************************************
*********************************************************************************************************
*                                             MODULE END
*********************************************************************************************************
*********************************************************************************************************
*/

#endif                                                          /* End of rtos_description.h module include.            */
