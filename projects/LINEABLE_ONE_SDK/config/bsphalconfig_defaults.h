/*
*********************************************************************************************************
*
*
*
* File :
*********************************************************************************************************
*/

#ifndef  _BSPHALCONFIG_DEFAULTS_H_
#define  _BSPHALCONFIG_DEFAULTS_H_



/**
 ****************************************************************************************
 * \name                        Control Interface Settings
 ****************************************************************************************
 * \{ */
#ifndef  configUSE_DCDC
#define  configUSE_DCDC                                     (0)
#endif


#ifndef  configUSE_SPI_FLASH
#define  configUSE_SPI_FLASH                                 (0)
#endif

#ifndef  configUSE_LEDS
#define  configUSE_LEDS                                      (0)
#endif


#ifndef configUSE_DISPLAY
#define  configUSE_DISPLAY                                   (0)
#endif

#ifndef configUSE_BUTTON
#define configUSE_BUTTON                                     (0)
#endif


#ifndef configUSE_TOUCH_CSENS
#define configUSE_TOUCH_CSENS                                (0)
#endif

/** \} */






/*
*********************************************************************************************************
*                                     TRACE / DEBUG CONFIGURATION
*********************************************************************************************************
* \{ */

#ifndef  TRACE_LEVEL_OFF
#define  TRACE_LEVEL_OFF                               0
#endif

#ifndef  TRACE_LEVEL_INFO
#define  TRACE_LEVEL_INFO                              1
#endif

#ifndef  TRACE_LEVEL_DBG
#define  TRACE_LEVEL_DBG                               2
#endif

#ifdef RELEASE
#define  APP_TRACE_LEVEL                        TRACE_LEVEL_OFF
#else
#define  APP_TRACE_LEVEL                        TRACE_LEVEL_DBG
#endif

/** \} */

/**
 ****************************************************************************************
 * \name                        Historical Data Settings
 ****************************************************************************************
 * \{ */
/**
 * \brief Enable/disable historical data storage
 */
#ifndef configUSE_HISTORY_STORAGE
#       define configUSE_HISTORY_STORAGE                 (0)
#endif /* configUSE_HISTORY_STORAGE */
/** \} */






//#include "health_toolbox_defs.h"

/*
*********************************************************************************************************
*********************************************************************************************************
*                                             MODULE END
*********************************************************************************************************
*********************************************************************************************************
*/

#endif  //_BSPHALCONFIG_DEFAULTS_H_
