/*
 * app_cfg.h
 *
 *  Created on: 2018. 4. 25.
 *      Author: Brandon Kim
 */

#ifndef  APP_CFG_MODULE_PRESENT
#define  APP_CFG_MODULE_PRESENT

#include <stdio.h> // temp until redefine printf
/*
*********************************************************************************************************
*                                            TASK PRIORITIES
*                                            TASK STACK SIZES
*********************************************************************************************************
*/

/**
 *      task priority:
 *          0  <----------> 32(-1)
 *         HIGH             LOW
 */

#define TASK_PRIORITY_HIGH                 8u
#define TASK_PRIORITY_MID                 10u
#define TASK_PRIORITY_LOW                 12u

/*------------------------------------------------------------------------------------------------*/
/* ESSENTIAL                                                                                      */
/*------------------------------------------------------------------------------------------------*/
#define EX_MAIN_START_TASK_PRIO           21u       // Ex Main Start task
#define EX_MAIN_START_TASK_STK_SIZE       512

#define APP_CFG_TASK_BLUETOOTH_LL_PRIO    3u
#define APP_CFG_TASK_BLUETOOTH_STACK_PRIO 4u

#define BLUETOOTH_APP_TASK_PRIO           5u
#define BLUETOOTH_APP_TASK_STK_SIZE      ((2*1024) / sizeof(CPU_STK))

#define LORAWAN_EVENT_TASK_PRIO           5u
#define LORAWAN_EVENT_TASK_STK_SIZE      ((2*1024) / sizeof(CPU_STK))

/*------------------------------------------------------------------------------------------------*/
/* DEVICE                                                                                         */
/*------------------------------------------------------------------------------------------------*/
#define HRM_APP_TASK_PRIO                 TASK_PRIORITY_HIGH
#define HRM_APP_TASK_STK_SIZE            ((5*1024) / sizeof(CPU_STK))

#define GPS_APP_TASK_PRIO                 TASK_PRIORITY_MID
#define GPS_APP_TASK_STK_SIZE            ((2*1024) / sizeof(CPU_STK))
#define GPS_COMM_TASK_PRIO                TASK_PRIORITY_HIGH
#define GPS_COMM_TASK_STK_SIZE           ((4*1024) / sizeof(CPU_STK))

#define PMIC_APP_TASK_PRIO                TASK_PRIORITY_HIGH
#define PMIC_APP_TASK_STK_SIZE           ((1*1024) / sizeof(CPU_STK))
#define PMIC_CHARGE_CHECK_TASK_PRIO       TASK_PRIORITY_HIGH
#define PMIC_CHARGE_CHECK_TASK_STK_SIZE  ((1*1024) / sizeof(CPU_STK))

/*------------------------------------------------------------------------------------------------*/
/* APPLICATION                                                                                    */
/*------------------------------------------------------------------------------------------------*/
#define UI_APP_TASK_PRIO                  TASK_PRIORITY_MID
#define UI_APP_TASK_STK_SIZE             ((2*1024) / sizeof(CPU_STK))

#define MAIN_EVENT_TASK_PRIO              TASK_PRIORITY_MID
#define MAIN_EVENT_TASK_STK_SIZE         ((2*1024) / sizeof(CPU_STK))

#define MAIN_STATE_TASK_PRIO              TASK_PRIORITY_MID
#define MAIN_STATE_TASK_STK_SIZE         ((8*1024) / sizeof(CPU_STK))
#define MAIN_STATE_TASK_Q_NUM             10u

#define MAIN_COMM_TASK_PRIO               TASK_PRIORITY_MID
#define MAIN_COMM_TASK_STK_SIZE          ((2*1024) / sizeof(CPU_STK))

/**
 ****************************************************************************************
 * \name                        BLE services
 ****************************************************************************************
 * \{ */
/**
 * \brief Health Care Service (Dialog)
 *
 * 0:disable, 1:enable
 */
#define configUSE_HCS                                  (0)

/**
 * \brief Battery Service
 *
 * 0:disable, 1:enable
 */
#define configUSE_BAS                                  (1)

/**
 * \brief Heart Rate Service
 *
 * 0:disable, 1:enable
 */
#define configUSE_HRS                                  (1)

/**
 * \brief Current Time Service
 *
 * 0:disable, 1:enable
 */
#define configUSE_CTS                                   (1)

/**
 * \brief OTA Service
 *
 * 0:disable, 1:enable
 */
#define  configUSE_OTA                                  (1)

/** \} */


#endif /*APP_CFG_MODULE_PRESENT*/

