#ifndef _UCOS_APP_H_
#define _UCOS_APP_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#include <bsp.h>
#include <common/include/common.h>
#include <common/include/lib_def.h>
#include <common/include/rtos_utils.h>
#include <common/include/toolchains.h>
#include <common/include/rtos_prio.h>
#include <kernel/include/os.h>
#include <common/include/clk.h>
#include <common/include/shell.h>
#include "app_cfg.h"
#include "debug.h"

#ifdef __cplusplus
}
#endif

#endif // _UCOS_APP_H_
