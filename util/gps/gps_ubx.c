/*------------------------------------------------------------------------------
* ublox.c : ublox receiver dependent functions
*
*          Copyright (C) 2007-2018 by T.TAKASU, All rights reserved.
*          Copyright (C) 2014 by T.SUZUKI, All rights reserved.
*
* https://github.com/tomojitakasu/RTKLIB/blob/master/src/rcv/ublox.c
*-----------------------------------------------------------------------------*/
#include "rtos_app.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "arm_math.h"//#include <math.h>
#include "gps.h"
#include "gps_ubx.h"
#include "em_leuart.h"


#if configUSE_GPS

#ifndef GNSS_PRINTF
#define GNSS_DEBUG_ON    0
#if GNSS_DEBUG_ON
#define GNSS_PRINTF(...) printf(__VA_ARGS__)
#else
#define GNSS_PRINTF(...)
#endif
#endif

#define ID_ACKACK   ((UBX_CLASS_ACK<<8)|UBX_ID_ACK_ACK)
#define ID_ACKNAK   ((UBX_CLASS_ACK<<8)|UBX_ID_ACK_NAK)

#define ID_CFGPRT   ((UBX_CLASS_CFG<<8)|UBX_ID_CFG_PRT)
#define ID_NAVPVT   ((UBX_CLASS_NAV<<8)|UBX_ID_NAV_PVT)       //Navigation Position Velocity Time Solution
#define ID_NAVSTATUS   ((UBX_CLASS_NAV<<8)|(UBX_ID_NAV_STATUS))
#define ID_MGAANO   ((UBX_CLASS_MGA<<8)|UBX_ID_MGA_ANO)

/*
Note that during a leap second there may be more (or less) than 60 seconds in a
minute; see the description of leap seconds for details.
This message combines position, velocity and time solution, including accuracy figures */
#define ID_NAVSOL       ((UBX_CLASS_NAV<<8)|UBX_ID_NAV_SOL)      /* ubx message id: nav solution info */
/* This message has only been retained for backwards compatibility; users are recommended
to use the UBX-NAV-PVT message in preference.*/
#define ID_NAVTIMEGPS  ((UBX_CLASS_NAV<<8)|UBX_ID_NAV_TIMEGPS)      /* ubx message id: nav time gps */
#define ID_NAVTIMEUTC  ((UBX_CLASS_NAV<<8)|UBX_ID_NAV_TIMEUTC)      /* ubx message id: nav time gps */


/*
This message reports the precise BDS time of the most recent navigation solution including
validity flags and an accuracy estimate.  */



#define ID_TRKMEAS  0x0310      /* ubx message id: trace mesurement data */
#define ID_TRKSFRBX 0x030F      /* ubx message id: trace subframe buffer */


/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/

static int ubx_checksum_valid(unsigned char *buff, int len)
{
    uint8_t cka=0,ckb=0;
    int32_t i;

    for (i=2;i<len-2;i++) {
        cka+=buff[i]; ckb+=cka;
    }
    return cka==buff[len-2]&&ckb==buff[len-1];
}


/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
        send a UBX message, this function just takes the payload and calculates and adds checksum.
        \param cls the UBX class id
        \param id the UBX message id
        \param length size of the message payload to write
        \param data the message payload to write
        \return total bytes written
 ******************************************************************************************************/
int32_t ubxSend( uint8_t cls, uint8_t id, uint16_t length, const void *data )
{
    int32_t i;
    uint8_t tx_data[128];
    unsigned char cka=0,ckb=0;
    const uint8_t *p = data;
    uint8_t *q = tx_data;

    *q++  = UBXSYNC1;
    *q++  = UBXSYNC2;
    *q++  = cls;
    *q++  = id;
    *q++  = (uint8_t)(length&0xFF);// little endian
    *q++  = (uint8_t)(length>>8);

    for (i = 0; i < length; i++) {
        *q++ = *p++;
    }

    // check sum
    length+=6;// class, id, length
    for(i = 2; i< length ; i++) {
         cka+=tx_data[i]; ckb+=cka;
    }

    tx_data[i] = cka;
    tx_data[i+1] = ckb;

    length +=2;

    //hexdump((uint8_t *)"ubx tx", tx_data, length);

    for(i = 0; i< length; i++){
        LEUART_Tx(LEUART0, tx_data[i]);
    }

    return length;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
void ubxSetCfgPRT(  bool ubx_input, bool ubx_output,
                    bool nmea_input, bool nmea_output)
{
    struct ubxCfgPrt msg ;

    msg.portID = 1;          //Port identifier for UART 1
    msg.reserved1 = 0;
    msg.txReady = 0;
    msg.mode = 0x000008C0;
    msg.baudRate = 9600;
    msg.inProtoMask = 0;       // Specifies input protocols
    msg.outProtoMask = 0;      // Specifies output protocols
    msg.flags = 0;
    msg.reserved2[0] = 0;
    msg.reserved2[1] = 0;

    if (ubx_input)
        msg.inProtoMask |= 0x0001;   // set first bit
    else
        msg.inProtoMask &= 0xFFFE;   // clear first bit

    if (nmea_input)
        msg.inProtoMask |=  0x0002;   // set second bit
    else
        msg.inProtoMask &=  0xFFFD;   // clear second bit

    if (ubx_output)
        msg.outProtoMask |= 0x0001;   // set first bit
    else
        msg.outProtoMask &=  0xFFFE;   // clear first bit

    if (nmea_output)
        msg.outProtoMask |=  0x0002;   // set second bit
    else
        msg.outProtoMask &=  0xFFFD;  // clear second bit

    ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_PRT, 20, &msg);
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 // Port identifier = 3 for USB (default value if left blank)
  //                 = 1 or 2 for UART    : use 1
 ******************************************************************************************************/
// Poll Port Configuration
void ubxPollCfgPrt(uint8_t port_identifier)
{
    ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_PRT, 1, &port_identifier);
}

/*****************************************************************************************************
 *Function Name   : Reset Receiver / Clear Backup Data Structures
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 UBX-CFG-RST (0x06 0x04)
 ******************************************************************************************************/
void ubxSetCfgRST(uint16_t nav_bbr_mask, uint8_t reset_mode)
{
    struct ubxCfgRst msg;

    msg.navBbrMask = nav_bbr_mask;
    // Startup Modes
    // Hotstart 0x000
    // Warmstart 0x0001
    // Coldstart 0xFFFF
    msg.resetMode = reset_mode;
    // Reset Modes:
    // Hardware Reset 0x00
    // Controlled Software Reset 0x01
    // Controlled Software Reset - Only GPS 0x02
    // Hardware Reset After Shutdown 0x04
    // Controlled GPS Stop 0x08
    // Controlled GPS Start 0x09
    msg.reserved1  = 0;//reserved 1

    ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_RST, 4, &msg);
    // need to wait ?//OSSemPendAbort?
}
/*****************************************************************************************************
 *Function Name   : Clear, Save and Load configurations
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 UBX-CFG-CFG (0x06 0x09)
 ******************************************************************************************************/
void ubxSetCfgCFG(uint32_t clear, uint32_t save, uint32_t load)
{
    struct ubxCfgCfg msg;
    /*
    0 PRT
    1 MSG
    2 INF
    3 NAV
    */
    msg.clearMask = clear;
    msg.saveMask = save;
    msg.loadMask  = load;

    ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_CFG, 12, &msg);

}
/*****************************************************************************************************
 *Function Name   :
 *Input           :
  power     0x00 -> Full power
            0x01 -> Balanced
            0x02 -> Interval
            0x03 -> Aggressive with 1Hz
            0x04 -> Aggressive with 2Hz
            0x05 -> Aggressive with 4Hz
            0xFF -> Invalid (only when polling)
  period
    Recommended minimum period is 10s,
    Only valid when powerSetupValueset to Interval, otherwise must be set to '0'.
  on_time
    Duration of the ON phase, must be smaller than the period.
    Only valid when powerSetupValue set to Interval, otherwise must be set to '0'.
 *Output          :
 *Description     :
 *Remarks         :
 UBX-CFG-RST (0x06 0x86)

 ******************************************************************************************************/
void ubxSetCfgPMS(uint8_t power, uint16_t period, uint16_t on_time)
{
    struct ubxCfgPms msg;
    msg.version = 0; // 0x00 for this version
    msg.powerSetupValue = power;
    if(power == 0x02){
        if(on_time > period){
            GNSS_PRINTF("on time must be maller than the period");
            on_time = period-1;
        }
        msg.period = period;
        msg.onTime = on_time;
    }
    else{
        msg.period = 0;
        msg.onTime = 0;
    }
    msg.reserved1[0]  = 0;//reserved 1
    msg.reserved1[1]  = 0;//reserved 1

    ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_PMS, 8, &msg);
}

void ubx_setMgaIniTimeUtc(uint16_t yyyy, uint8_t Mm, uint8_t dd, uint8_t Hh, uint8_t mm, uint8_t ss)
{
  struct ubxMgaIniTime_UTC data;

  data.type         = 0x10;
  data.version      = 0x00;
  data.ref          = 0x00;
  data.leapSecs     = 0x80;
  data.year         = yyyy;
  data.month        = Mm;
  data.day          = dd;
  data.hour         = Hh;
  data.minute       = mm;
  data.second       = ss;
  data.reserved1    = 0;
  data.ns           = 0;
  data.tAccS        = 0;
  data.reserved2[0] = 0;
  data.reserved2[1] = 0;
  data.tAccNs       = 0;

  ubxSend( UBX_CLASS_MGA, UBX_ID_MGA_INI, 24, &data);
}

void ubx_setMgaIniPositionLlh(int32_t latitude, int32_t longitude, int32_t altitude, uint32_t positionAccuracy)
{
  struct ubxMgaIniPos_LLH data;

  data.type         = 0x01;
  data.version      = 0x00;
  data.reserved1[0] = 0;
  data.reserved1[1] = 0;
  data.lat          = latitude;
  data.lon          = longitude;
  data.alt          = altitude;
  data.posAcc       = positionAccuracy;

  ubxSend( UBX_CLASS_MGA, UBX_ID_MGA_INI, 20, &data);
}


#if 0// for reference struct tm in time.h
    struct tm {
       int tm_sec;         /* seconds,  range 0 to 59          */
       int tm_min;         /* minutes, range 0 to 59           */
       int tm_hour;        /* hours, range 0 to 23             */
       int tm_mday;        /* day of the month, range 1 to 31  */
       int tm_mon;         /* month, range 0 to 11             */
       int tm_year;        /* The number of years since 1900   */
       int tm_wday;        /* day of the week, range 0 to 6    */
       int tm_yday;        /* day in the year, range 0 to 365  */
       int tm_isdst;       /* daylight saving time             */
    };
    /*
    *********************************************************************************************************
    *                                       CLOCK DATE/TIME DATA TYPE
    *
    * Note(s) : (1) Same date/time structure is used for all epoch. Thus Year value ('Yr') should be a value
    *               between the epoch start and end years.
    *
    *           (2) Seconds value of 60 is valid to be compatible with leap second adjustment and the atomic
    *               clock time stucture.
    *
    *           (3) Time zone is based on Coordinated Universal Time (UTC) & has valid values :
    *
    *               (a) Between  +|- 12 hours (+|- 43200 seconds)
    *               (b) Multiples of 15 minutes
    *********************************************************************************************************
    */

    typedef  struct  clk_date_time {
        CLK_YR      Yr;                                             /**< Yr        [epoch start to end yr), (see Note #1).    */
        CLK_MONTH   Month;                                          /**< Month     [          1 to     12], (Jan to Dec).     */
        CLK_DAY     Day;                                            /**< Day       [          1 to     31].                   */
        CLK_DAY     DayOfWk;                                        /**< Day of wk [          1 to      7], (Sun to Sat).     */
        CLK_DAY     DayOfYr;                                        /**< Day of yr [          1 to    366].                   */
        CLK_HR      Hr;                                             /**< Hr        [          0 to     23].                   */
        CLK_MIN     Min;                                            /**< Min       [          0 to     59].                   */
        CLK_SEC     Sec;                                            /**< Sec       [          0 to     60], (see Note #2).    */
        CLK_TZ_SEC  TZ_sec;                                         /**< TZ        [     -43200 to  43200], (see Note #3).    */
    } CLK_DATE_TIME;

#endif
/// Check if a year is a leap year
static bool isLeapYear(uint32_t year)
{
    bool leapYear = false;

    if (year % 400 == 0) {
        leapYear = true;
    } else if (year % 4 == 0) {
        leapYear = true;
    }

    return leapYear;
}

extern GNSS_STATUS_t gnss;
/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 NOT complited
 //https://github.com/u-blox/c030-tracker/blob/master/main.cpp
 ******************************************************************************************************/
int decode_NavTimeUTC(uint8_t *raw_data, int32_t raw_length)
{
    struct ubxNavTimeUTC *utc = (struct ubxNavTimeUTC *)raw_data;

    GNSS_PRINTF("time %d-%d-%d %d:%d:%d\r\n", utc->year, utc->month, utc->day, utc->hour, utc->min, utc->sec);

#if 1// no libray
/// For date conversion.
    static const uint8_t daysInMonth[] =
    {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    static const uint8_t daysInMonthLeapYear[] =
    {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


    uint32_t gnssTime = 0;
    uint32_t months;
    uint32_t year;
    uint32_t x;

    if ( utc->valid & 0x04) {
        // Year 1999-2099, so need to adjust to get year since 1970
        year = ((uint32_t) (utc->year)) - 1999 + 29;
        // Month (1 to 12), so take away 1 to make it zero-based
        months = utc->month - 1;
        months += year * 12;
        // Work out the number of seconds due to the year/month count
        for (x = 0; x < months; x++) {
            if (isLeapYear ((x / 12) + 1970)) {
                gnssTime += daysInMonthLeapYear[x % 12] * 3600 * 24;
            } else {
                gnssTime += daysInMonth[x % 12] * 3600 * 24;
            }
        }
        // Day (1 to 31)
        gnssTime += ((uint32_t) utc->day - 1) * 3600 * 24;
        // Hour (0 to 23)
        gnssTime += ((uint32_t) utc->hour) * 3600;
        // Minute (0 to 59)
        gnssTime += ((uint32_t) utc->min) * 60;
        // Second (0 to 60)
        gnssTime += utc->sec;

        //GNSS_PRINTF("GNSS time is %u.\n", gnssTime);

        gnss.unixTime = gnssTime;
        //Clk_SetTS_Unix(gnss.unixTime);
    }
#elif  RTOS_MODULE_COMMON_CLK_AVAIL
    if ( utc->valid & 0x04) {
        CLK_DATE_TIME date_time ;
        CLK_TS_SEC    unix_sec;
        date_time.Yr  = utc->year;
        date_time.Month = utc->month;
        date_time.Day = utc->day;
        date_time.Hr = utc->hour;
        date_time.Min = utc->min;
        date_time.Sec = utc->sec;
        date_time.DayOfWk = CLK_FIRST_DAY_OF_WK;// default
        date_time.DayOfYr = CLK_FIRST_DAY_OF_YR;//
        date_time.TZ_sec  = 0;
        if(DEF_OK == Clk_DateTimeToTS_Unix(&unix_sec, &date_time)){
            gnss.unixTime = unix_sec;
            //Clk_SetTS_Unix(gnssTime);
        }else{
            GNSS_PRINTF("%s return error\r\n", __FUNCTION__);
        }
    }
#else
    if ( utc->valid & 0x04) {
        struct tm t;
        t.tm_year = utc->year -1900;
        t.tm_mon = utc->month;           // Month, 0 - jan
        t.tm_mday = utc->day;          // Day of the month
        t.tm_hour = utc->hour;
        t.tm_min = utc->min;
        t.tm_sec = utc->sec;
        t.tm_isdst = -1;        // Is DST on? 1 = yes, 0 = no, -1 = unknown
        gnss.unixTime = (uint32_t)mktime(&t);
    }
#endif


    return GPS_PARSE_OK;
}
/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ID: 0x01  0x07
 ******************************************************************************************************/
int decode_NavPvt(uint8_t *raw_data, int32_t raw_length)
{
    struct ubxNavPvt *pvt=(struct ubxNavPvt *)raw_data;
    uint8_t bit_data;
    GNSS_PRINTF("time %d-%d-%d %d:%d:%d\r\n", pvt->year, pvt->month, pvt->day, pvt->hour, pvt->min, pvt->sec);
    //hexdump((uint8_t *)"NavPvt", raw_data, raw_length);
    if(pvt->valid > 0){
        bit_data = pvt->valid;
        if(bit_data & 0x04){
            // time and day fully resoved
            ;
        }
        else{
            if(bit_data & 0x01){
                ;    // valid date
            }
            if(bit_data & 0x02){
                ;    //valid time
            }
        }
    }

    if(pvt->fixType > 1){ // 2: 2D-fix
        bit_data = pvt->flags;
        GNSS_PRINTF("flags : 0x%X\r\n", bit_data);
        //psm State
        GNSS_PRINTF("Power Save Mode state: %d\r\n", ((bit_data&0x1c)>>2));
        if(bit_data&0x01){
            GNSS_PRINTF("gnssFixOK\r\n");
            gnss.ing.lon = pvt->lon;
            gnss.ing.lat = pvt->lat;
            gnss.ing.altitude = pvt->height;
            //gnss.
        }


    }

    return GPS_PARSE_OK;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ID: 0x01 0x03
 ******************************************************************************************************/
int decode_NavStatus(uint8_t *raw_data, int32_t raw_length)
{
    struct ubxNavStatus *status=(struct ubxNavStatus *)raw_data;

    GNSS_PRINTF("GPS Fix Type: 0x%X\n", status->gpsFix);
    if(status->gpsFix == 0){
        GNSS_PRINTF("No Fix\nTTFF: none ms\n");
        GNSS_PRINTF("Milliseconds since Startup/Reset: %d\n",   (int)(status->msss / 1000.));
    } else if (status->gpsFix == 0x01) {
        GNSS_PRINTF("Dead Reckoning Only\n");
    } else if (status->gpsFix == 0x02) {
        GNSS_PRINTF("2D Fix\n");
    } else if (status->gpsFix == 0x03) {
        GNSS_PRINTF("3D Fix\n");
    } else if (status->gpsFix == 0x04) {
        GNSS_PRINTF("GPS + Dead Reckoning\n");
    } else if (status->gpsFix == 0x05) {
        GNSS_PRINTF("Time Only\n");
    }

    if (status->gpsFix != 0x00) {
        GNSS_PRINTF("TTFF: %d  sec\n", (int)(status->ttff / 1000.));
        GNSS_PRINTF("Milliseconds since Startup/Reset: %d sec\n", (int)(status->msss / 1000.));
    }

    GNSS_PRINTF("iTOW %d \r\n", (int)status->iTOW);

    return GPS_PARSE_OK;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/

uint8_t ubxDecoder( uint8_t *raw_data, int32_t raw_length )
{
    struct ubxHeader *head = (struct ubxHeader *)raw_data;
    uint16_t msg_id;
    msg_id =  ((uint16_t)head->clsID<<8) + head->msgID;
    /* checksum */
    if (!ubx_checksum_valid(raw_data,raw_length)) {
        GNSS_PRINTF("ubx checksum error: msg_id=%04x len=%d\n",msg_id, (int)raw_length);
        return GPS_PARSE_FAIL;
    }

    //GNSS_PRINTF("decode_ubx: msg_id=%04x len=%d\r\n", (int)msg_id, (int)raw_length);
    //hexdump((uint8_t *)"ubx rx", raw_data, raw_length);
    // no more need header section
    raw_data += UBX_HEADER_SIZE;
    raw_length -= UBX_HEADER_SIZE;
    switch (msg_id) {
        case ID_ACKACK  : GNSS_PRINTF("UBX ACK : 0x%02X 0x%02X \r\n", *raw_data, *(raw_data+1));break;
        case ID_ACKNAK  : GNSS_PRINTF("UBX NAK : 0x%02X 0x%02X \r\n", *raw_data, *(raw_data+1));break;
        case ID_NAVTIMEUTC : return decode_NavTimeUTC (raw_data, raw_length);
        case ID_NAVPVT  : return decode_NavPvt  (raw_data, raw_length);
        case ID_NAVSTATUS: return decode_NavStatus (raw_data, raw_length);
        case ID_CFGPRT :  hexdump((uint8_t *)"CfgPrt", raw_data, raw_length);break;
        default: GNSS_PRINTF("[no handler]UBX 0x%02X 0x%02X (%4d)", head->clsID, head->msgID, (int)raw_length+UBX_HEADER_SIZE);
            hexdump((uint8_t *)"ubx", raw_data, raw_length);
    }

    return GPS_PARSE_OK;
}


#if 0// need to impelement
//https://github.com/u-blox/c030-tracker/blob/master/main.cpp
/// Determine whether GNSS has the necessary data to be
// put into power save state.  This is to have downloaded
// ephemeris data from sufficient satellites and to have
// calibrated its RTC.  There is also a timeout check.
// NOTE: it is up to the caller to check separately if we
// have the required accuracy of fix.
static bool gnssCanPowerSave()
{
    bool canPowerSave = false;

    LOG_MSG("Checking if GNSS can power save...\n");

    if (gnssIsOn()) {
        LOG_MSG("GNSS has been on for %d second(s).\n", (int) (time(NULL) - r.gnssPowerOnTime));
        // Time GNSS out if we're outside the maximum time and are in full working-day
        // operation.  NOTE: if we're in "slow mode" operation, rather than full
        // working-day operation, we want to try our damndest and will power GNSS
        // off anyway at the end of the short "slow mode" wakeup.
        if ((time(NULL) - r.gnssPowerOnTime < GNSS_MAX_ON_TIME_SECONDS) || (time(NULL) < START_TIME_FULL_WORKING_DAY_OPERATION_UNIX_UTC)) {
            uint32_t powerSaveState = 0;
            uint32_t numEntries = 0;
            uint32_t numEphemerisData = 0;
            uint32_t totalCN = 0;
            uint32_t peakCN = 0;

            // See ublox7-V14_ReceiverDescrProtSpec section 38.2 (MON-HW)
            LOG_MSG("Checking if RTC is calibrated (MON-HW)...\n");
            if (gnss.sendUbx(0x0A, 0x09, NULL, 0) >= 0) {
                if (readGnssMsg(msgBuffer, sizeof(msgBuffer), GNSS_WAIT_FOR_RESPONSE_MILLISECONDS) > 0) {
                    if ((msgBuffer[22 + GNSS_UBX_PROTOCOL_HEADER_SIZE] & 0x01) == 0x01) {
                        // If the rtcCalib bit is set, we're doing good...
                        powerSaveState++;
                        LOG_MSG("RTC is calibrated.\n");
                    } else {
                        LOG_MSG("RTC is NOT calibrated.\n");
                    }
                } else {
                    LOG_MSG("No response.\n");
                }
            }

            // See ublox7-V14_ReceiverDescrProtSpec section 39.11 (NAV-SVINFO)
            LOG_MSG("Checking ephemeris data (NAV-SVINFO)...\n");
            if (gnss.sendUbx(0x01, 0x30, NULL, 0) >= 0) {
                if (readGnssMsg(msgBuffer, sizeof(msgBuffer), GNSS_WAIT_FOR_RESPONSE_MILLISECONDS) > 0) {
                    // Check how many satellites we have ephemeris data for
                    numEntries = msgBuffer[4 + GNSS_UBX_PROTOCOL_HEADER_SIZE];
                    LOG_MSG("  > %d entry/entries in the list", (int) numEntries);

                    if (numEntries > 0) {
                        LOG_MSG(": \n");
                        uint32_t i = GNSS_UBX_PROTOCOL_HEADER_SIZE  + 8; // 8 is offset to start of satellites array
                        // Now need to check that enough are used for navigation
                        for (uint32_t x = 0; x < numEntries; x++) {
                            // Print out the info
                            LOG_MSG("  > chn %3d", msgBuffer[i]);
                            LOG_MSG(", svid %3d", msgBuffer[i + 1]);
                            LOG_MSG(", flags 0x%02x", msgBuffer[i + 2]);
                            LOG_MSG(", quality 0x%02x", msgBuffer[i + 3]);
                            LOG_MSG(", C/N (dBHz) %3d", msgBuffer[i + 4]);
                            LOG_MSG(", elev %3d", msgBuffer[i + 5]);
                            LOG_MSG(", azim %5d", (int) (((uint16_t) msgBuffer[i + 6]) + ((uint16_t) (msgBuffer[i + 7] << 8))));
                            LOG_MSG(", prRes %10d", (int) littleEndianUint32(&(msgBuffer[i + 8])));
                            if ((msgBuffer[i + 2] & 0x01) == 0x01) { // 2 is offset to flags field in a satellite block
                                numEphemerisData++;
                                totalCN += msgBuffer[i + 4];
                                if (msgBuffer[i + 4] > peakCN) {
                                    peakCN = msgBuffer[i + 4];
                                }
                                LOG_MSG(", used for navigation.\n");
                            } else {
                                LOG_MSG(", NOT usable.\n");
                            }
                            i += 12; // 12 is the size of a satellite block
                        }
                        LOG_MSG("  > %d satellite(s) used for navigation with %d required.\n", (int) numEphemerisData, GNSS_MIN_NUM_EPHEMERIS_DATA);
                        if (numEphemerisData >= GNSS_MIN_NUM_EPHEMERIS_DATA) {
                            // Doing even better
                            powerSaveState++;
                        }
                    } else {
                        LOG_MSG(".\n");
                    }
                } else {
                    LOG_MSG("No response.\n");
                }

                // Calculate the informational variables
                if (numEphemerisData > 0) {
                    gnssNumSatellitesUsable = numEphemerisData;
                    gnssPeakCNUsed = peakCN;
                    gnssAverageCNUsed = totalCN / numEphemerisData;
                }

                // Determine the outcome
                if (powerSaveState == 2) {
                    canPowerSave = true;
                    LOG_MSG("GNSS can now power save.\n");
                } else {
                    LOG_MSG("GNSS NOT yet ready to power save.\n");
                }
            }
        } else {
            canPowerSave = true;
            LOG_MSG("GNSS isn't ready but we're out of time so put GNSS to sleep anyway.\n");
        }
    } else {
        canPowerSave = true;
        LOG_MSG("GNSS is already off.\n");
    }

    return canPowerSave;
}

#endif

#endif//#if configUSE_GPS
