/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include "assistNow.h"

#include <stdio.h>
#include <stdlib.h>

#include "rtos_app.h"
#include "debug.h"
#include "em_msc.h"
#include "gps_ubx.h"
#include "gps.h"
#include "flash.h"
/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/
#undef  __MODULE__
#define __MODULE__ "ASSIST_NOW"
#undef DBG_MODULE
#define DBG_MODULE 0


/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/
SL_PACK_START(1)
struct  ubxMgaAnoMessage
{//u-blox 8 / u-blox M8 from protocol version 15 up to version 23.01
    uint8_t sync1;      // start of packet first byte (0xB5)
    uint8_t sync2;      // start of packet second byte (0x62)
    uint8_t clsID;        // Class that defines basic subset of message (NAV, RXM, etc.)
    uint8_t msgID;         // Message ID
    uint16_t length;    // length of the payload data, excluding header and checksum
    uint8_t type;           // Message type (0x00 for this type)
    uint8_t version;        // Message version (0x00 for this version)
    uint8_t svId;           // Satellite identifier (see SatelliteNumbering)
    uint8_t gnssId;         // GNSS identifier (see Satellite Numbering)
    uint8_t year;           // years since the year 2000
    uint8_t month;          // month (1..12)
    uint8_t day;            // day (1..31)
    uint8_t reserved1;      // Reserved
    uint8_t data[64];       // assistance data
    uint8_t reserved2[4];   // Reserved
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

SL_PACK_START(1)
struct  buffer
{
    uint8_t data[168];
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

SL_PACK_START(1)
struct mgaBinMsg
{
  struct ubxHeader head;
  struct ubxMgaAno data;
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

static uint32_t* getWriteAddress();

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/
int AssistNow_saveMga2Flash(uint8_t *data, uint32_t len)
{
  uint32_t* writeAddress = getWriteAddress();
  APP_TRACE_DEBUG("Write Address: %08X \n", writeAddress);

  struct buffer *pvt=(struct buffer *)data;

  MSC_Init();
  MSC_WriteWord(writeAddress, pvt, len);
  MSC_Deinit();

  APP_TRACE_DEBUG("first 8 bytes: %X %X \n", *(pvt+0), *(pvt+1));
  return 0;
}

int AssistNow_eraseMgaData()
{
    uint32_t *addr = (uint32_t *)ASSIST_NOW_START;
    uint32_t flashPageSize=FLASHGetPageSize();
    uint32_t flashPages = (ASSIST_NOW_END - ASSIST_NOW_START)/flashPageSize;
    uint8_t i = 0;

    APP_TRACE_DEBUG("\t- AssistNow_eraseMgaData; flashPageSize: %d  0x%X flashPages: %d  0x%X\n",flashPageSize,flashPageSize,flashPages,flashPages );

    FLASHOpen();
    for(i=0;i<flashPages;i++)
    {
      //APP_TRACE_DEBUG("\t-  page: %d addr: %08X \n",i, addr+(i*flashPageSize/sizeof(uint32_t)) );
      int8_t res =  FLASHEraseBlock(addr + (i*(flashPageSize/sizeof(uint32_t))));
      if(res != 0) APP_TRACE_DEBUG("\t- AssistNow_eraseMgaData; res: %d addr:%08X \n",res,addr + (i*(flashPageSize/sizeof(uint32_t))));
    }
    FLASHClose();

    return 0;
}

int AssistNow_sendMgaDataToModule()
{
  CLK_DATE_TIME         date_time;
  uint32_t *addr = (uint32_t *)ASSIST_NOW_START;
  uint32_t i = 0;

  Clk_GetDateTime(&date_time);
  APP_TRACE_DEBUG("\t- SystemTime %02d.%02d.%02d\n",date_time.Day,date_time.Month,date_time.Yr);

//location und zeitupdate gehören nicht hier hin sondern in die gnssStart funkion, hier date time als übergabe parameter übergeben

  if(date_time.Yr > 2018)// date should be higher than firmwareDate   TBD
  {
    ubx_setMgaIniTimeUtc(date_time.Yr, date_time.Month, date_time.Day, date_time.Hr, date_time.Min, date_time.Sec);
  }


  // TBD location update
  LocationUpdateParams_t *lastLocation;
  lastLocation = gnssGetLastLoc();
  if(lastLocation != NULL)
  {

    APP_TRACE_DEBUG("\t- LastLocation:  lon=%lf  lat=%lf  alt=%lf  valid=%d\n", lastLocation->lon,
                                                      lastLocation->lat, lastLocation->altitude, lastLocation->valid);
    APP_TRACE_DEBUG("\t- LastLocation:  lon=%d  lat=%d  alt=%d  valid=%d\n",
                                                                  (int32_t)(lastLocation->lon*10000000),
                                                                  (int32_t)(lastLocation->lat*10000000),
                                                                  (uint32_t)(lastLocation->altitude*100),
                                                                  lastLocation->valid);

     ubx_setMgaIniPositionLlh((int32_t)(lastLocation->lat*10000000),
                              (int32_t)(lastLocation->lon*10000000),
                              (uint32_t)(lastLocation->altitude*100),
                              0);
  }

  while(addr[i] != ASSIST_NOW_LAST && (addr+i) < (uint32_t*) ASSIST_NOW_END)
  {
    struct mgaBinMsg *ele;
    ele = (struct mgaBinMsg *)(addr+i);
    //APP_TRACE_DEBUG("\t- %02d.%02d.%02d\n", ele->data.day, ele->data.month, ele->data.year);

    if (date_time.Day == ele->data.day && date_time.Month == ele->data.month && date_time.Yr-2000 == ele->data.year)
    {
      APP_TRACE_DEBUG("\t- %02d.%02d.%02d\n", ele->data.day, ele->data.month, ele->data.year);
      ubxSend( ele->head.clsID, ele->head.msgID, ele->head.length, &ele->data);
    }
    i += 21;
  }

  return 0;
}


/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

static uint32_t* getWriteAddress()
{
  uint32_t *addr = (uint32_t *)ASSIST_NOW_START;
  uint8_t found = 0;
  uint32_t i=0;

  while(addr[i] != ASSIST_NOW_LAST && &addr[i] !=(uint32_t*) ASSIST_NOW_END)
  {
    i++;
  }
  APP_TRACE_DEBUG(" ELE: %08X \n", &addr[i]);

  if ((addr+i) >= (uint32_t*) ASSIST_NOW_END)//&addr[i] == ASSIST_NOW_END)
  {
    return (uint32_t *)ASSIST_NOW_START;
  }

	return &addr[i];
}
