#ifndef __GPS_UBX_H__
#define __GPS_UBX_H__

/* REFERENCE
u-blox 8 / u-blox M8 Receiver Description
Including Protocol Specification v15-20.30,22-23.01
UBX-13003221 - R15
Revision and date R15 (26415b7) 6 March 2018
*/



#define UBXSYNC1    0xB5        /* ubx message sync code 1 */
#define UBXSYNC2    0x62        /* ubx message sync code 2 */

//! UBX Protocol Class/Message ID's
#define UBX_CLASS_ACK       0x05
    #define UBX_ID_ACK_ACK  0x01
    #define UBX_ID_ACK_NAK  0x00
#define UBX_CLASS_AID       0x0B
    #define UBX_ID_AID_ALM  0x30
    #define UBX_ID_AID_AOP  0x33
    #define UBX_ID_AID_EPH  0x31
    #define UBX_ID_AID_HUI  0x02
    #define UBX_ID_AID_INI  0x01
#define UBX_CLASS_CFG       0x06
    #define UBX_ID_CFG_ANT    0x13
    #define UBX_ID_CFG_BATCH  0x93
    #define UBX_ID_CFG_CFG    0x09
    #define UBX_ID_CFG_DAT    0x06

    #define UBX_ID_CFG_NAV5   0x24
    #define UBX_ID_CFG_NAVX5  0x23

    #define UBX_ID_CFG_EKF 0x12
    #define UBX_ID_CFG_ESFGWT 0x29
    #define UBX_ID_CFG_FXN 0x0E
    #define UBX_ID_CFG_ITFM 0x39
    #define UBX_ID_CFG_MSG 0x01
    #define UBX_ID_CFG_NMEA 0x17
    #define UBX_ID_CFG_NVS 0x22
    #define UBX_ID_CFG_PM2 0x3B
    #define UBX_ID_CFG_PM 0x32
    #define UBX_ID_CFG_PMS 0x86
    #define UBX_ID_CFG_PRT 0x00
    #define UBX_ID_CFG_RATE 0x08
    #define UBX_ID_CFG_RINV 0x34
    #define UBX_ID_CFG_RST 0x04
    #define UBX_ID_CFG_RXM 0x11
    #define UBX_ID_CFG_SBAS 0x16
    #define UBX_ID_CFG_TMODE2 0x3D
    #define UBX_ID_CFG_TMODE 0x1D
    #define UBX_ID_CFG_TP5 0x31
    #define UBX_ID_CFG_TP 0x07
    #define UBX_ID_CFG_USB 0x1B

#define UBX_CLASS_ESF         0x10
    #define UBX_ID_ESF_INS    0x15
    #define UBX_ID_ESF_MEAS   0x02
    #define UBX_ID_ESF_RAW    0x03
    #define UBX_ID_ESF_STATUS 0x10

#define UBX_CLASS_HNR         0x28
    #define UBX_ID_HNR_PVT    0x00

#define UBX_CLASS_INF 0x04
    #define UBX_ID_INF_DEBUG 0x04
    #define UBX_ID_INF_ERROR 0x00
    #define UBX_ID_INF_NOTICE 0x02
    #define UBX_ID_INF_TEST 0x03
    #define UBX_ID_INF_WARNING 0x01
#define UBX_CLASS_MON 0x0A
    #define UBX_ID_MON_HW2 0x0B
    #define UBX_ID_MON_HW 0x09
    #define UBX_ID_MON_IO 0x02
    #define UBX_ID_MON_MSGPP 0x06
    #define UBX_ID_MON_RXBUF 0x07
    #define UBX_ID_MON_RXR 0x21
    #define UBX_ID_MON_TXBUF 0X08
    #define UBX_ID_MON_VER 0x04
#define UBX_CLASS_NAV              0x01
    #define UBX_ID_NAV_AOPSTATUS   0x60
    #define UBX_ID_NAV_ATT         0x05
    #define UBX_ID_NAV_CLOCK       0x22
    #define UBX_ID_NAV_DGPS        0x31
    #define UBX_ID_NAV_DOP         0x04
    #define UBX_ID_NAV_EOE         0x61
    #define UBX_ID_NAV_GEOFENCE    0x39

    #define UBX_ID_NAV_POSECEF     0x01
    #define UBX_ID_NAV_POSLLH      0x02
    #define UBX_ID_NAV_PVT         0x07
    #define UBX_ID_NAV_RESETODO    0x10

    #define UBX_ID_NAV_SBAS         0x32
    #define UBX_ID_NAV_SOL          0x06
    #define UBX_ID_NAV_STATUS       0x03
    #define UBX_ID_NAV_SVINFO       0x30
    #define UBX_ID_NAV_TIMEGPS      0x20
    #define UBX_ID_NAV_TIMEUTC      0x21
    #define UBX_ID_NAV_VELECEF      0x11
    #define UBX_ID_NAV_VELNED       0x12

#define UBX_CLASS_RXM 0x02

#define UBX_CLASS_TIM 0x0D
    #define UBX_ID_TIM_SVIN 0x04
    #define UBX_ID_TIM_TM2 0x03
    #define UBX_ID_TIM_TP 0x01
    #define UBX_ID_TIM_VRFY 0x06

#define UBX_CLASS_MGA 0x13
    #define UBX_ID_MGA_ANO 0x20
    #define UBX_ID_MGA_INI 0x40

#define UBX_HEADER_SIZE    6
#define UBX_CHECKSUM_SIZE  2
SL_PACK_START(1)
struct  ubxHeader
{
    uint8_t sync1;      // start of packet first byte (0xB5)
    uint8_t sync2;      // start of packet second byte (0x62)
    uint8_t clsID;        // Class that defines basic subset of message (NAV, RXM, etc.)
    uint8_t msgID;         // Message ID
    uint16_t length;    // length of the payload data, excluding header and checksum
} SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
 * CFG-CFG Message Structure
 * This message clears, saves, or loads novalitle memory.
 * Set masks to 0x061F to clear, save, or load all values.
 * ID: 0x06  0x09 Payload Length=12 bytes
 *
 *  bit 0: ioPort Communications port settings. Modifying this sub-section results in an IO system reset. Because of this undefined
 *         data may be output for a short period of time after receiving the message.
 *  bit 1: msgConf Message configuration
 *  bit 2: infMsg INF message configuration
 *  bit 3: navConf Navigation configuration
 *  bit 4: rxmConf Receiver Manager configuration
 *
 *  bit 8:  senConf Sensor interface configuration (not supported in protocol versions less than 19)
 *  bit 9:  rinvConf Remote inventory configuration
 *  bit 10: antConf Antenna configuration
 *  bit 11: logConf Logging configuration
 *  bit 12: ftsConf FTS configuration. Only applicable to the FTS product variant.
 */
SL_PACK_START(1)
struct  ubxCfgCfg {
    uint32_t clearMask;     // clear mask
    uint32_t saveMask;     // save mask
    uint32_t loadMask;     // load mask
    uint8_t  deviceMask;   // optional block
} SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
 * CFG-PRT Message Structure
 * This message configures a USART or USB port.
 * Use to specify input/output protocols to use
 * ID: 0x06  0x00 Payload Length=20 bytes
 */
SL_PACK_START(1)
struct  ubxCfgPrt
{
    uint8_t portID; // port identifier (0 or 1 for USART or 3 for USB)
    uint8_t reserved1; // reserved
    uint16_t txReady; // transmit ready status
    uint32_t mode;      //
    uint32_t baudRate;  //
    uint16_t inProtoMask; // input protocol mask
    uint16_t outProtoMask; // output protocol mask
    uint16_t flags;       //
    uint8_t  reserved2[2]; // reserved
} SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*!
* CFG-NMEA Message Structure
* NEWEST MESSAGE VERSION NMEA protocol configuration
* ID: 0x06  0x17 Payload Length=12 bytes
*/
SL_PACK_START(1)
//32.11.19.3 Extended NMEA protocol configuration V1
struct  ubxCfgNmeaV1
{//u-blox 8 / u-blox M8 from protocol version 15 up to version 23.01
    uint8_t filter;         // filter flags (see graphic in spec sheet)
    uint8_t nmeaVersion;    // 0x41: NMEA version 4.1. 0x40: NMEA version 4.0. 0x23: NMEA version 2.3. 0x21: NMEA version 2.1
    uint8_t numSV;          // Maximum Number of SVs to report per TalkerId. 0: unlimited. 8: 8 SVs. 12: 12 SVs. 16: 16 SVs
    uint8_t flags;          // flags (see graphic in spec sheet)
    uint32_t gnssToFilter;  // Filters out satellites based on their GNSS. If a bitfield is enabled, the corresponding satellites will be not output.
    uint8_t svNumbering;    // Configures the display of satellites that do not have an NMEA-defined value. Note: this does not apply to satellites with an unknown ID. 0: Strict - Satellites are not output 1: Extended - Use proprietary numbering (see Satellite numbering in spec sheet)
    uint8_t mainTalkerId;   // By default the main Talker ID (i.e. the Talker ID used for all messages other than GSV) is determined by the GNSS assignment of the receiver's channels (see UBX-CFG-GNSS). This field enables the main Talker ID to be overridden. 0: Main Talker ID is not overridden 1: Set main Talker ID to 'GP' 2: Set main Talker ID to 'GL' 3: Set main Talker ID to 'GN' 4: Set main Talker ID to 'GA' 5: Set main Talker ID to 'GB'
    uint8_t gsvTalkerId;    // By default the Talker ID for GSV messages is GNSS specific (as defined by NMEA). This field enables the GSV Talker ID to be overridden. 0: Use GNSS specific Talker ID (as defined by NMEA) 1: Use the main Talker ID
    uint8_t version;        // Message version (set to 0 for this version)
    uint8_t bdsTalkerId[2]; // Sets the two characters that should be used for the BeiDou Talker ID If these are set to zero, the default BeiDou TalkerId will be used
    uint8_t reserved1[6];      // Reserved
} SL_ATTRIBUTE_PACKED;
SL_PACK_END()


SL_PACK_START(1)
//u-blox 8 / u-blox M8 from protocol version 15 up to version 23.01
// length 36 bytes
struct  ubxCfgNav5
{ // 0x06 0x24
    uint16_t  mask;      // parameters bitmask (only masked params applied)
    uint8_t   dynModel;   /* Dynamic platform model:
                            0: portable
                            2: stationary
                            3: pedestrian
                            4: automotive
                            5: sea
                            6: airborne with <1g acceleration
                            7: airborne with <2g acceleration
                            8: airborne with <4g acceleration
                            9: wrist worn watch (not supported in protocol
                            versions less than 18)    */
    uint8_t   fixMode;     //< positioning fix mode
    int32_t   fixedAlt;    // (scale .01) (m)
    uint32_t  fixedAltVar; // (scale .0001) (m^2)
    int8_t    minElev;     // (deg)
    uint8_t   drLimit;     // max time to perform DR w/out GPS (sec)
    uint16_t  pDop;        // (scale .1)
    uint16_t  tDop;        // (scale .1)
    uint16_t  pAcc;         //  (m)
    uint16_t  tAcc;         // (m)
    uint8_t   staticHoldThresh;   // cm/s  //static hold threshold
    uint8_t   dgnssTimeout;       // (s) DGNSS timerout
    uint8_t   cnoThreshNumSVs;    // dBHz  number of satellites required to have C/No above cnoThresh for a fix to be attemped
    uint8_t   cnoThresh;          // dBHz
    uint8_t   reserved1[2];
    uint16_t  staticHoldMaxDist;
    uint8_t   utcStandard; // 0: automatic, 3, 6, 7,
    uint8_t   reserved2[5]; // reserved (always set to zero)

} SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
* CFG-NAVX5 Message Structure
* Navigation Engine Expert Settings
* ID: 0x06  0x23 Payload Length=40 bytes
*/
SL_PACK_START(1)
//32.11.18.2 Navigation Engine Expert Settings
//u-blox 8 / u-blox M8 from protocol version 18 up to version 23.01
// length 40 bytes
struct  ubxCfgNavX5
{ // 0x06 0x23
    uint16_t  version;     // 2 for this version
    uint16_t  mask1;       //First parameters bitmask

    uint32_t  mask2;       //Second parameters bitmask.

    uint8_t   reserved1[2];
    uint8_t   minSVs;
    uint8_t   maxSVs;

    uint8_t   minCNO;
    uint8_t   reserved2;
    uint8_t   iniFix3D;
    uint8_t   reserved3[2];
    uint8_t   ackAiding;
    uint16_t  wknRollover;

    uint8_t   sigAttenCompMode;
    uint8_t   reserved4;
    uint8_t   reserved5[2];

    uint8_t   reserved6[2];
    uint8_t   usePPP;
    uint8_t   aopCfg;

    uint8_t   reserved7[2];
    uint16_t  aopOrbMaxErr;

    uint8_t   reserved8[4];

    uint8_t   reserved9[3];
    uint8_t   useAdr;
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
* CFG-PWR Message Structure
* Put receiver in a defined power state
* ID: 0x06  0x57 Payload Length=8 bytes
*/
SL_PACK_START(1)
struct  ubxCfgPwr{
    uint8_t version;        // Message version (1 for this version)
    uint8_t reserved1[3];        // Reserved
    uint32_t state;        // Enter system state. 0x52554E20: GNSS running. 0x53544F50: GNSS stopped. 0x42434B50: Software Backup
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
* CFG-PMS Message Structure
* Powr mode setup
* ID: 0x06  0x86 Payload Length=8 bytes
Using UBX-CFG-PMS to set Super-E mode 1, 2, 4Hz navigation rates sets 180 s
minAcqTime instead the default 300 s in protocol version 23.01.
*/
SL_PACK_START(1)
struct  ubxCfgPms{
    uint8_t version;        // Message version ( 00 for this version)
    uint8_t powerSetupValue;    //0x00 -> Full power
                                //0x01 -> Balanced
                                //0x02 -> Interval
                                //0x03 -> Aggressive with 1Hz
                                //0x04 -> Aggressive with 2Hz
                                //0x05 -> Aggressive with 4Hz
                                //0xFF -> Invalid (only when polling)      // Reserved
    uint16_t period;            //Position update period and search period. Recommended minimum period is 10s
    uint16_t onTime;            //Duration of the ON phase, must be smaller than the period.
                                //Only valid when powerSetupValue set to
                                //Interval, otherwise must be set to '0'.
    uint8_t reserved1[2];
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
* NAV-TIMEGPS Message Structure
* GPS Time Solution
* Message Type: PERIODIC/POLLED
* ID: 0x01  0x20  Payload Length= 16 bytes
*/
SL_PACK_START(1)
struct  ubxNavTimeGPS
{
    uint32_t iTOW;  // GPS ms time of week
    int32_t  fTOW;   // fractional nanoseconds remainder
    int16_t  week;   // GPS week
    int8_t   leapS;// GPS UTC leap seconds
    uint8_t  valid;  // validity flags
    uint32_t tAcc;  // time accuracy measurement (nanosecs)
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*!
* NAV-TIMEUTC Message Structure
* UTC Time Solution
* Message Type: PERIODIC/POLLED
* ID: 0x01  0x21  Payload Length= 20 bytes
*/
SL_PACK_START(1)
struct  ubxNavTimeUTC
{
    uint32_t iTOW;  // GPS time of week (msec)
    uint32_t tAcc;  // time accuracy measurement
    int32_t  nano;   // Nanoseconds of second
    uint16_t year;  // year
    uint8_t  month;  // month
    uint8_t  day;    // day
    uint8_t  hour;   // hour
    uint8_t  min;    // minute
    uint8_t  sec;    // second
    uint8_t  valid;  // validity flags
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*!
* NAV-PVT Message Structure
* Navigation Position Velocity Time Solution
* Message Type: PERIODIC/POLLED
*  Payload Length=92 bytes
*/
SL_PACK_START(1)
struct  ubxNavPvt
{

    uint32_t iTOW;      // ms : GPS time of week of the navigation epoch. See the description of iTOW for details

    uint16_t year;
    uint8_t  month;   //   Month, range 1..12 UTC
    uint8_t  day;     //   Day of month, range 1..31 UTC

    uint8_t  hour;    //   Hour of day, range 0..23 UTC
    uint8_t  min;     //   Minute of hour, range 0..59 UTC
    uint8_t  sec;     //   Seconds of minute, range 0..60(59?) UTC
    uint8_t  valid;   //   Validity Flags (see graphic below)

    uint32_t tAcc;       // ns       Time accuracy estimate UTC
    int32_t  nano;       // ns       Fraction of second, range -1e9..1e9 UTC

    uint8_t  fixType;    // -        GNSSfix Type, range 0..5
    uint8_t  flags;      // -        Fix Status Flags
/*
Important: Users are recommended to check the gnssFixOK flag in the UBX-NAV-PVT or the
NMEA valid flag. Fixes not marked valid should not normally be used.
0 bit   : gnssFixOK 1 = valid fix (i.e within DOP & accuracy masks)
1 bit   : diffSoln 1 = differential corrections were applied
2-4bits : Power Save Mode state (see Power Management):
            0: PSM is not active
            1: Enabled (an intermediate state before Acquisition state
            2: Acquisition
            3: Tracking
            4: Power Optimized Tracking
            5: Inactive
*/
    uint8_t  flags2;
    uint8_t  numSV;      // -        Number of satellites used in Nav Solution

    int32_t  lon;        // deg      Longitude (1e-7)
    int32_t  lat;        // deg      Latitude (1e-7)
    int32_t  height;     // mm       Height above Ellipsoid
    int32_t  hMSL;       // mm       Height above mean sea level
    uint32_t hAcc;       // mm       Horizontal Accuracy Estimate
    uint32_t vAcc;       // mm       Vertical Accuracy Estimate
    int32_t  velN;       // mm/s     NED north velocity
    int32_t  velE;       // mm/s     NED east velocity
    int32_t  velD;       // mm/s     NED down velocity
    int32_t  gSpeed;     // mm/s     Ground Speed (2-D)
    int32_t  headMot;    // deg      Heading of motion 2-D (1e-5)
    uint32_t sAcc;       // mm/s     Speed Accuracy Estimate
    uint32_t headAcc;    // deg      Heading Accuracy Estimate (1e-5)

    uint16_t pDOP;       // -        Position DOP (0.01)
    uint8_t  reserved1[6];

    int32_t  headVeh; // 1e-5

    int16_t  magDec;  // 1e-2
    uint16_t  magAcc;  // 1e-2

}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*!
* NAV-SAT Message Structure
* Satellite Information
* Message Type: PERIODIC/POLLED
* ID: 0x01  0x35  Payload Length=8 + 12*numSvs bytes
*/
SL_PACK_START(1)
struct  ubxNavSat
{
        uint32_t iTOW;     // GPS time of week of the navigation epoch. See the description of iTOW for details.
        uint8_t version;       // Message version (1 for this version)
        uint8_t numSvs;        // Number of satellites
        uint8_t reserved1[2];     // Reserved
        uint8_t gnssId;        // GNSS identifier (see Satellite numbering) for assignment
        uint8_t svId;      // Satellite identifier (see Satellite numbering) for assignment
        uint8_t cno;       // Carrier to noise ratio (signal strength)
        int8_t elev;       // Elevation (range: +/-90), unknown if out of range
        int16_t azim;      // Azimuth (range +/-180), unknown if elevation is out of range
        int16_t prRes;     // Pseudo range residual
        uint32_t flags;        // Bitmask (see graphic below)
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*!
 * NAV-STATUS Message Structure
 * Receiver Navigation Status
 * Message Type: PERIODIC/POLLED
 * ID: 0x01 0x03 Payload Length=16 bytes
 */
SL_PACK_START(1)
struct  ubxNavStatus
{
    uint32_t iTOW;      // Time of Week (ms)
    uint8_t  gpsFix;    /* GPSfix Type, this value does not qualify a fix as
                        valid and within the limits. See note on flag
                        gpsFixOk below.
                        0x00 = no fix
                        0x01 = dead reckoning only
                        0x02 = 2D-fix
                        0x03 = 3D-fix
                        0x04 = GPS + dead reckoning combined
                        0x05 = Time only fix
                        0x06..0xff = reserved */
    uint8_t flags;
    uint8_t fixStat;
    uint8_t flags2;
    uint32_t ttff;      // TTFF (ms)
    uint32_t msss;      // Milliseconds since startup/reset
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
 * CFG-RST Message Structure
 * This message allows a receiver to be reset.
 * ID: 0x06  0x04 Payload Length=4 bytes
 */
SL_PACK_START(1)
struct  ubxCfgRst
{
    uint16_t navBbrMask;  //!< Nav data to clear: 0x0000 = hot start, 0x0001 = warm start, 0xFFFF=cold start
    uint8_t  resetMode;     //!< Reset mode
    uint8_t  reserved1;     //!< reserved
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
 * MGA-ANO Message Structure
 * Multiple GNSS AssistNow Offline Assistance
 * ID: 0x13  0x20 Payload Length=76 bytes
 */
SL_PACK_START(1)
struct  ubxMgaAno
{//u-blox 8 / u-blox M8 from protocol version 15 up to version 23.01
    uint8_t type;           // Message type (0x00 for this type)
    uint8_t version;        // Message version (0x00 for this version)
    uint8_t svId;           // Satellite identifier (see SatelliteNumbering)
    uint8_t gnssId;         // GNSS identifier (see Satellite Numbering)
    uint8_t year;           // years since the year 2000
    uint8_t month;          // month (1..12)
    uint8_t day;            // day (1..31)
    uint8_t reserved1;      // Reserved
    uint8_t data[64];       // assistance data
    uint8_t reserved2[4];   // Reserved
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

/*!
 * MGA-INI-TIME_UTC Message Structure
 * Initial Time Assistance
 * ID: 0x13  0x40 Payload Length=24 bytes
 */
SL_PACK_START(1)
struct  ubxMgaIniTime_UTC
{//u-blox 8 / u-blox M8 from protocol version 15 up to version 23.01
    uint8_t type;         // Message type (0x10 for this type)
    uint8_t version;      // Message version (0x00 for this version)
    uint8_t ref;          //Reference to be used to set time
                              //Name Description
                              //source
                                    //0: none, i.e. on receipt of message (will be inaccurate!)
                                    //1: relative to pulse sent to EXTINT0
                                    //2: relative to pulse sent to EXTINT1
                                    //3-15: reserved
                              //fall
                                    //use falling edge of EXTINT pulse (default rising) - only if source is EXTINT
                              //last
                                    //use last EXTINT pulse (default next pulse) - only if source is EXTINT
    int8_t leapSecs;      //Number of leap seconds since 1980 (or 0x80 = -128 if unknown)
    uint16_t year;        // Year
    uint8_t month;        // Month, starting at 1
    uint8_t day;          // Day, starting at 1
    uint8_t hour;         // Hour, from 0 to 23
    uint8_t minute;       // Minute, from 0 to 59
    uint8_t second;       // Seconds, from 0 to 59
    uint8_t reserved1;    // Reserved
    uint32_t ns;          // Nanoseconds, from 0 to 999,999,999
    uint16_t tAccS;       // Seconds part of time accuracy
    uint8_t reserved2[2]; // Reserved
    uint32_t tAccNs;      // Nanoseconds part of time accuracy, from 0 to 999,999,999
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()


/*!
 * MGA-INI-POS_LLH Message Structure
 * Initial Position Assistance
 * ID: 0x13  0x40 Payload Length=20 bytes
 */
SL_PACK_START(1)
struct  ubxMgaIniPos_LLH
{//u-blox 8 / u-blox M8 from protocol version 15 up to version 23.01
    uint8_t type;         // Message type (0x01 for this type)
    uint8_t version;      // Message version (0x00 for this version)
    uint8_t reserved1[2]; // Reserved
    int32_t lat;          // [deg] WGS84 Latitude
    int32_t lon;          // [deg] WGS84 Longitude
    int32_t alt;          // [cm] WGS84 Altitude
    uint32_t posAcc;      // [cm] Position accuracy (stddev)
}SL_ATTRIBUTE_PACKED;
SL_PACK_END()

#define MAXFREQ     7                   /* max NFREQ */

#define FREQ1       1.57542E9           /* L1/E1  frequency (Hz) */
#define FREQ2       1.22760E9           /* L2     frequency (Hz) */
#define FREQ5       1.17645E9           /* L5/E5a frequency (Hz) */
#define FREQ6       1.27875E9           /* E6/LEX frequency (Hz) */
#define FREQ7       1.20714E9           /* E5b    frequency (Hz) */
#define FREQ8       1.191795E9          /* E5a+b  frequency (Hz) */
#define FREQ1_GLO   1.60200E9           /* GLONASS G1 base frequency (Hz) */
#define DFRQ1_GLO   0.56250E6           /* GLONASS G1 bias frequency (Hz/n) */
#define FREQ2_GLO   1.24600E9           /* GLONASS G2 base frequency (Hz) */
#define DFRQ2_GLO   0.43750E6           /* GLONASS G2 bias frequency (Hz/n) */
#define FREQ3_GLO   1.202025E9          /* GLONASS G3 frequency (Hz) */
#define FREQ1_CMP   1.561098E9          /* BeiDou B1 frequency (Hz) */
#define FREQ2_CMP   1.20714E9           /* BeiDou B2 frequency (Hz) */
#define FREQ3_CMP   1.26852E9           /* BeiDou B3 frequency (Hz) */



#define EFACT_GPS   1.0                 /* error factor: GPS */
#define EFACT_GLO   1.5                 /* error factor: GLONASS */
#define EFACT_GAL   1.0                 /* error factor: Galileo */
#define EFACT_QZS   1.0                 /* error factor: QZSS */
#define EFACT_CMP   1.0                 /* error factor: BeiDou */
#define EFACT_SBS   3.0                 /* error factor: SBAS */


#define SYS_NONE    0x00                /* navigation system: none */
#define SYS_GPS     0x01                /* navigation system: GPS */
#define SYS_SBS     0x02                /* navigation system: SBAS */
#define SYS_GLO     0x04                /* navigation system: GLONASS */
#define SYS_GAL     0x08                /* navigation system: Galileo */
#define SYS_QZS     0x10                /* navigation system: QZSS */
#define SYS_CMP     0x20                /* navigation system: BeiDou */
#define SYS_IRN     0x40                /* navigation system: IRNSS */
#define SYS_LEO     0x80                /* navigation system: LEO */
#define SYS_ALL     0xFF                /* navigation system: all */


#define TSYS_GPS    0                   /* time system: GPS time */
#define TSYS_UTC    1                   /* time system: UTC */
#define TSYS_GLO    2                   /* time system: GLONASS time */
#define TSYS_GAL    3                   /* time system: Galileo time */
#define TSYS_QZS    4                   /* time system: QZSS time */
#define TSYS_CMP    5                   /* time system: BeiDou time */

typedef struct tag_gtime_t
{  /* time struct */
    time_t time;        /* time (s) expressed by standard time_t */
    double sec;         /* fraction of second under 1 s */
} gtime_t;


extern uint8_t ubxDecoder( uint8_t *raw_data, int32_t raw_length );
extern int32_t ubxSend( uint8_t class, uint8_t id, uint16_t length, const void *data );
extern void ubxSetCfgPRT(  bool ubx_input, bool ubx_output,
                    bool nmea_input, bool nmea_output);
extern void ubxSetCfgRST(uint16_t nav_bbr_mask, uint8_t reset_mode) ;
extern void ubxSetCfgPMS(uint8_t power, uint16_t period, uint16_t on_time);
extern void ubxSetCfgCFG(uint32_t clear, uint32_t save, uint32_t load);

void ubx_setMgaIniTimeUtc(uint16_t yyyy, uint8_t MM, uint8_t dd, uint8_t HH, uint8_t mm, uint8_t ss);
void ubx_setMgaIniPositionLlh(int32_t latitude, int32_t longitude, int32_t altitude, uint32_t positionAccuracy);

#endif//__GPS_UBX_H__
