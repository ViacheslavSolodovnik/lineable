#ifndef __ASSIST_NOW__
#define __ASSIST_NOW__

#include "bg_types.h"

//40 flash pages * 2048bytes
// startaddress = 0x00100000 endOfFlash - 0x14000
#define ASSIST_NOW_START  0x000EC000
#define ASSIST_NOW_END    0x00100000

#define ASSIST_NOW_LAST   0xFFFFFFFF

int AssistNow_saveMga2Flash(uint8_t *data, uint32_t len);
int AssistNow_eraseMgaData();
int AssistNow_sendMgaDataToModule();

#endif  //__ASSIST_NOW__
