/****************************************************************************************
//
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author: Brandon
//   File name :
//   Created :
//   Last Update :

Basic arch
NMEA parsing : use as a timer and status check
UBX  parsing : use to get location


According to U-BLOX8 doc
Customers enabling BeiDou and/or Galileo who wish to use the NMEA protocol are recommended
to select NMEA version 4.1, as earlier versions have no support for these two GNSS. See the NMEA
protocol section for details on selecting NMEA versions.
Target : NMEA v4.0

****************************************************************************************/
#include "rtos_app.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "arm_math.h"//#include <math.h>
//#include <gpiointerrupt.h>
//#include "em_device.h"
#include <common/include/clk.h>
#include "em_cmu.h"
#include "em_core.h"
#include "em_gpio.h"
#include "em_leuart.h"
#include "timer.h"
#include "sys_os.h"
#include "app_main.h"
#include "app_ble.h"
#include "gps.h"
#include "gps_ubx.h"
#include "assistNow.h"

#if 1 //configUSE_GPS
/*******************************************************************************
*                                 DEBUG OPTION
*******************************************************************************/
#undef  __MODULE__
#define __MODULE__ "GPS"
#undef  DBG_MODULE
#define DBG_MODULE 0
#define GNSS_DEBUG 0


#define FEATURE_GPS_NMEA_RMC    (0)
#define FEATURE_GPS_NMEA_GSV    (0)
#define FEATURE_GPS_NMEA_GSA    (1)
#define FEATURE_GPS_NMEA_GGA    (1)
#define FEATURE_GPS_NMEA_GNS    (0)

/*******************************************************************************
*                                  DEFINES
*******************************************************************************/
#define GNSS_EVT_READ_UART   (1<<0)
#define GNSS_EVT_POWER_ON    (1<<1)
#define GNSS_EVT_POWER_OFF   (1<<2)
#define GNSS_EVT_CONFIG      (1<<3)
#define GNSS_EVT_ALL         (GNSS_EVT_READ_UART    | \
                              GNSS_EVT_POWER_ON     | \
                              GNSS_EVT_POWER_OFF    | \
                              GNSS_EVT_CONFIG)

#define GNSS_NMEA_HEADER  '$'
#define GNSS_NMEA_FOOTER1 '\r'
#define GNSS_NMEA_FOOTER2 '\n'

#define GNSS_NMEA_REPORT_INTERVAL   1 /* secondd */

#define GNSS_LINE_WAIT   0
#define GNSS_LINE_NMEA   1
#define GNSS_LINE_UBX    2

/* NMEA sentence max length, including \r\n (chars) */
//#define NMEA_MAX_LENGTH       82  //Limit82 Mode
/* NMEA sentence prefix length (num chars), Ex: GPGLL */
#define  NMEA_PREFIX_LENGTH  5

/*!
 * \brief Maximum number of data byte that we will accept from the GPS
 */
#define GPS_BUFFER_MAX   256

#define GNSS_RX_FIFO_SIZE                       512

//NMEA v4.1 and above only //#define NMEA_GPGSA_MAX_FILED     18 //21-3
#define NMEA_GPGSA_MAX_FILED     17

/*******************************************************************************
*                                  TYPEDEFS
*******************************************************************************/
typedef struct
{
    uint16_t size;
    uint16_t ubx_size;
    uint8_t  packet_type;
    uint8_t  data[GPS_BUFFER_MAX];
} gnss_line_t;

GNSS_CONFIG_t gnss_config =
{
    .saveConfig    = 0,
    .powerMode     = GNSS_POWER_HIGH,
    .periodSeconds = 2 * 60,
    .stayOnSeconds = 60,
};

typedef struct tag_ring_buffer_t
{
    uint16_t    head;// write position
    uint16_t    tail;// read  position
    uint16_t    size;
    uint8_t     *element;
} ring_buffer_t;


enum
{
    /* GGA Value indexes */
    NMEA_GPGGA_TIME = 1,
    NMEA_GPGGA_LAT = 2,
    NMEA_GPGGA_NS = 3,
    NMEA_GPGGA_LONG = 4,
    NMEA_GPGGA_EW = 5,
    NMEA_GPGGA_QUALITY = 6,
    NMEA_GPGGA_NUM_SV = 7,
    NMEA_GPGGA_HDOP = 8,
    NMEA_GPGGA_ALT = 9,
    NMEA_GPGGA_U_ALT = 10,
    NMEA_GPGGA_SEP = 11,
    NMEA_GPGGA_U_SEP = 12,
    NMEA_GPGGA_DIFF_AGE = 13,
    NMEA_GPGGA_DIFF_STATION = 14,

    /* GSV Value indexes */
    NMEA_GPGSV_NUMMSG = 1,
    NMEA_GPGSV_MSGNUM = 2,
    NMEA_GPGSV_NUMSV = 3,
};


/*******************************************************************************
*                                  MACROS
*******************************************************************************/
/* Various type of NMEA data we can receive with the Gps */
// 32bit value from NMEA address
#define  NMEA_TAG(X)                   (((int32_t)*(X)<<24)|(*(X+2)<<16)|(*(X+3)<<8)|(*(X+4)<<0))
// X is talker id and not used for parsing,
#define  NMEA_TAG_GEN(A,X,B,C,D)     (((uint32_t)(A)<<24)|((uint32_t)(B)<<16)|((uint32_t)(C)<<8)|((uint32_t)(D)<<0))
#define  NmeaTypeGxGGA   NMEA_TAG_GEN('G','N','G','G','A')
#define  NmeaTypeGxGSA   NMEA_TAG_GEN('G','N','G','S','A')
#define  NmeaTypeGxGNS   NMEA_TAG_GEN('G','N','G','N','S')
#define  NmeaTypeGxGVC   NMEA_TAG_GEN('G','N','G','S','V')
#define  NmeaTypeGxRMC   NMEA_TAG_GEN('G','N','R','M','C')
#define  NmeaTypeGxTXT   NMEA_TAG_GEN('G','N','T','X','T')
#define  NmeaTypeGxGSV   NMEA_TAG_GEN('G','N','G','S','V')
#define  NmeaTypeGxGLL   NMEA_TAG_GEN('G','N','G','L','L')

#define IS_BUFFER_EMPTY(X)    ((X)->tail == (X)->head)
#define IS_BUFFER_FULL(X)     ((((X)->head + 1)%(X)->size) ==(X)->tail)


/*******************************************************************************
*                                  VARIABLES
*******************************************************************************/
GNSS_STATUS_t gnss;
static  OS_FLAG_GRP     gnssEventFlags;
static  OS_TCB          gnssAppTaskTCB;
static  CPU_STK         gnssAppTaskStk[GPS_APP_TASK_STK_SIZE];
static  OS_TCB          gnssCommTaskTCB;
static  CPU_STK         gnssCommTaskStk[GPS_COMM_TASK_STK_SIZE];

/*!
 * FIFO buffers size
 */

static uint8_t gnssRxFifo[GNSS_RX_FIFO_SIZE];

static ring_buffer_t gnss_rx_rb;

static bool g_gnss_bypass = false;
static bool g_gnss_prove = false;

static TimerEvent_t g_gnss_cont_timer;

static uint8_t g_nmea_rx_buff[GPS_BUFFER_MAX];

/*******************************************************************************
*                         PRIVATE FUNCTION PROTOTYPES
*******************************************************************************/


/*******************************************************************************
*                              PRIVATE FUNCTIONS
*******************************************************************************/
static void ring_buffer_init(ring_buffer_t *rb, uint8_t *buffer, uint32_t size)
{
    rb->head = rb->tail = 0;
    rb->size = size;
    rb->element = buffer;
}

static void ring_buffer_reset(ring_buffer_t *rb)
{
    rb->head = rb->tail = 0;
}

static void GnssContTimerCb(void)
{
    TimerStop(&g_gnss_cont_timer);

    gnssPowerOff();
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 payload and calculates and adds checksum. ($ and *XX\r\n will be added)
 ******************************************************************************************************/
static int16_t nmea_send(const void *data_wo_checksum)
{
    const char hexTab[16] =
    {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
    };

    const uint8_t *p = data_wo_checksum;
    int16_t length = 0;
    uint8_t chk = 0;


    LEUART_Tx(LEUART0, '$');// $
    while (*p != '\0' && *p != '*')
    {
        LEUART_Tx(LEUART0, *p);
        chk ^= (uint8_t) * p;
        p++;
        length++;
    }

    LEUART_Tx(LEUART0, '*');
    LEUART_Tx(LEUART0, hexTab[chk >> 4]);
    LEUART_Tx(LEUART0, hexTab[chk & 0xf]);
    LEUART_Tx(LEUART0, '\r');
    LEUART_Tx(LEUART0, '\n');

    return (length + 5 + 1);// tx length including '$'
}

static void nmea_msg_control(const char *msg, uint8_t rate)
{
    // rate 0 is off
    char buffer[32];
    if (rate == 0)
    {
        sprintf(buffer, "PUBX,40,%s,0,0,0,0,0,0", msg);
    }
    else
    {
        sprintf(buffer, "PUBX,40,%s,0,%d,0,0,0,0", msg, (int)rate);
    }

    nmea_send(buffer);
}

static void gnssConfig(void)
{
    // initial message setting
    nmea_msg_control("GLL", 1);
    nmea_msg_control("VTG", 1);
    nmea_msg_control("GGA", 1);
    nmea_msg_control("GNS", GNSS_NMEA_REPORT_INTERVAL);
    nmea_msg_control("RMC", GNSS_NMEA_REPORT_INTERVAL);
    //GNSS DOP and Active Satellites
    nmea_msg_control("GSA", 1);
    //GNSS Satellites in View
    nmea_msg_control("GSV", GNSS_NMEA_REPORT_INTERVAL);

#if 1
        /***************************
         *  setting from gilanix
         *************************/
        //32.11.19.3 Extended NMEA protocol configuration V1
        /*  Prompt COG output event if COG frozen
         *  Numbering used for SVs not supported by NMEA -> 1 Extended
         *  */
        const uint8_t cog_output_extended[] = { \
                 0x20, /* trackFilt Enable COG output even if COG is frozen */
                 0x40, /* 0x40: NMEA version 4.0 */
                 0x00, /* Maximum Number of SVs to report per TalkerId. 0: unlimited*/
                 0x02, /* enable considering mode.*/
                 0x00, 0x00, 0x00, 0x00, /* 0bit : Disable reporting of GPS satellites, 1bit : Disable reporting of SBAS satellites */
                 0x01, /* 1: Extended - Use proprietary numbering */
                 0x00, /* 0: main talker ID is not overridden */
                 0x00, /* 0: Use GNSS specific Talker ID (as defined by NMEA) */
                 0x01, /* Message version (set to 1 for this version) */
                 0x00, 0x00, /* If these are set to zero, the default BeiDou TalkerId will be used */
                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        struct ubxCfgNmeaV1 *nmeaV1 = (struct ubxCfgNmeaV1 *)cog_output_extended;
        ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_NMEA, 20, nmeaV1);
#endif

#if 1
        /*  Static Hold: static hold 0.2m/s to stop position drift
            Dynamic Model: portable */
        const uint8_t static_hold_dynamic_model[] = { \
                 0xFF, 0xFF,
                 0x00, // portable
                 0x03, // fixMode - 3: auto 2D/3D
                 0x00, 0x00, 0x00, 0x00,
                 0x10, 0x27, 0x00, 0x00,
                 0x05, // minElev
                 0x00,
                 0xFA, 0x00,
                 0xFA, 0x00,
                 0x64, 0x00,
                 0x2C, 0x01, //tAcc
                 0x14, //staticHoldThresh
                 0x00,
                 0x00,
                 0x00,
                 0x10, 0x27, //reserved1
                 0x00, 0x00, //staticHoldMaxDist
                 0x00,
                 0x00, 0x00, 0x00, 0x00, 0x00 };

        struct  ubxCfgNav5 *nav5 = (struct  ubxCfgNav5 *)static_hold_dynamic_model;
        ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_NAV5, 36, nav5);
#endif

#if 1
        //32.11.18.2 Navigation Engine Expert Settings
        /* AssistNow Autonomous(GNSS Fast Fix):  */
        const uint8_t assistNow_On[] = { \
                 0x02, 0x00, /* version */
                 0x4C, 0x66, /* mask1 */
                 0xC0, 0x00, 0x00,  0x00, /* mask2 */
                 0x00, 0x00,  /* reserved1 */
                 0x03,        /* Minimum number of satellites for navigation */
                 0x10,        /* maximum number of satellites for navigation */
                 0x00,        /* Minimum satellite signal level for navigation */
                 0x00,        /* reserved2 */
                 0x00,        /* 1 = initial fix must be 3D */
                 0x00, 0x00,  /* reserved3 */
                 0x00,        /* 1 = issue acknowledgements for assistance message input */
                 0x00, 0x00,  /* wknRollover */
                 0x00,        /* sigAttenCompMode : Only supported on certain products */
                 0x00,        /* reserved4 */
                 0x00, 0x00,  /* reserved 5 */
                 0x00, 0x00,  /* reserved 6 */
                 0x00,          /* usePPP */
                 0x01,          /* aopCfg : 1 = enable AssistNow Autonomous */
                 0x00, 0x00,    /* reserved7 */
                 0x64, 0x00,    /* aopOrbMaxErr */
                 0x00, 0x00, 0x00, 0x00, /* reserved8 */
                 0x00, 0x00, 0x00,       /* reserved9 */
                 0x00 /* useAdr */ };

        struct ubxCfgNavX5 *navx5 = (struct ubxCfgNavX5 *)assistNow_On;
        ubxSend(UBX_CLASS_CFG, UBX_ID_CFG_NAVX5, 40, navx5);
#endif

}

static int32_t hex2num(uint8_t c)
{
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    if (c >= 'a' && c <= 'f')
    {
        return c - 'a' + 10;
    }
    if (c >= 'A' && c <= 'F')
    {
        return c - 'A' + 10;
    }
    return -1;
}

// input is two byte length hex
static int32_t hex2byte(const uint8_t *hex)
{
    int a, b;
    a = hex2num(*hex++);
    if (a < 0)
    {
        return -1;
    }
    b = hex2num(*hex++);
    if (b < 0)
    {
        return -1;
    }
    return (a << 4) | b;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 *   Calculates the checksum for a NMEA sentence
 *   Skip the first '$' if necessary and calculate checksum until '*' character is
 *   reached (or buffSize exceeded).
 ******************************************************************************************************/
static uint8_t nmea_get_checksum(const uint8_t *sentence)
{
    const uint8_t *n = sentence + 1;//skip the starting dollar sign.
    uint8_t chk = 0;

    /* While current char isn't '*' or sentence ending (null terminated) */
    while ('*' != *n && '\0' != *n)
    {
        chk ^= (uint8_t) * n;
        n++;
    }

    return chk;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
                    removed end mark from a received packet. end mark size = 2
 ******************************************************************************************************/
static int nmea_validate(const uint8_t *sentence, size_t length, int check_checksum)
{
    const uint8_t *n;

    /* should have atleast 9 characters including end mark*/
    if (9 - 2 > length)
    {
        return -1;
    }

    /* should start with $ */
    if (GNSS_NMEA_HEADER != *sentence)
    {
        //APP_TRACE_LOG("error:%s (%s)\n", __FUNCTION__, sentence);
        return -1;
    }

    /* should have a 5 letter, uppercase word */
    n = sentence;
    while (++n < sentence + 6)
    {
        if (*n < 'A' || *n > 'Z')
        {
            /* not uppercase letter */
            //APP_TRACE_LOG("not uppercase letter\n");
            return -1;
        }
    }

    /* should have a comma after the type word */
    if (',' != sentence[6])
    {
        //APP_TRACE_LOG("should have a comma after the type word\n");
        return -1;
    }

    /* check for checksum */
    if (1 == check_checksum)
    {
        uint8_t actual_chk;
        uint8_t expected_chk;

        /* has checksum? */
        if ('*' != sentence[length - (5 - 2)])
        {
            //APP_TRACE_LOG("no checksum\n");
            return -1;
        }

        actual_chk = nmea_get_checksum(sentence);
        expected_chk = (uint8_t) hex2byte(&sentence[length - (4 - 2)]);
        if (expected_chk != actual_chk)
        {
            //APP_TRACE_LOG("wrong checksum\n");
            return -1;
        }
    }

    return 0;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
static uint32_t nmea_utctime_to_sec(uint8_t *p)
{
    uint32_t seconds = 0;
    //Hour
    seconds  = ((*p++ - '0') * 10) * 3600;
    seconds += ((*p++ - '0')) * 3600;
    //Minute
    seconds += ((*p++ - '0') * 10) * 60;
    seconds += ((*p++ - '0')) * 60;
    //Second
    seconds += ((*p++ - '0') * 10);
    seconds += ((*p++ - '0'));
    return seconds;
}

#if FEATURE_GPS_NMEA_RMC
/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
static uint32_t nmea_utcdate_to_sec(uint8_t *p)
{

    int16_t year;
    uint8_t date, month;
    ;
    //date
    date   = ((*p++ - '0') * 10);
    date  += ((*p++ - '0'));
    //month
    month  = ((*p++ - '0') * 10);
    month += ((*p++ - '0'));
    //year  2 digits
    year   = ((*p++ - '0') * 10);
    year  += ((*p++ - '0'));

    // Some receivers report date year as 70 or 80 when their internal clock has
    // not yet synchronized with the satellites
    // Yep, this trick wouldn't work after 2069 year ^_^
    if (year > 69)
    {
        // Assume what year is less than 2000
        year += 1900;
    }
    else
    {
        // Assume what year is greater than 2000
        // Copy fix_date to date and fix_time to time in case of the $GPZDA sentence are disabled
        year += 2000;
    }

#ifdef  RTOS_MODULE_COMMON_CLK_AVAIL
    CLK_DATE_TIME date_time ;
    CLK_TS_SEC    unix_sec;

    date_time.Yr  = year;
    date_time.Month = month;
    date_time.Day = date;
    date_time.Hr = 0;
    date_time.Min = 0;
    date_time.Sec = 0;
    date_time.DayOfWk = CLK_FIRST_DAY_OF_WK;// default
    date_time.DayOfYr = CLK_FIRST_DAY_OF_YR;//
    date_time.TZ_sec  = 0;

    if (DEF_OK == Clk_DateTimeToTS_Unix(&unix_sec, &date_time))
    {
        return (uint32_t)unix_sec;
    }
    else
    {
        APP_TRACE_DEBUG("%s return error\r\n", __FUNCTION__);
    }
    return 0;
#else
    struct tm t;
    t.tm_year = year - 1900;
    t.tm_mon = month;           // Month, 0 - jan
    t.tm_mday = date;          // Day of the month
    t.tm_hour = 0;
    t.tm_min = 0;
    t.tm_sec = 0;
    t.tm_isdst = -1;        // Is DST on? 1 = yes, 0 = no, -1 = unknown
    return (uint32_t)mktime(&t);
#endif

}
#endif

/*****************************************************************************
reference :
https://archive.codeplex.com/?p=coordinate
https://github.com/kristianpaul/osgps/blob/master/nmea.c

Latitude: 05�� 13' 01.3" S
Longitude: 080�� 37' 49.5" W
In degrees and decimals: { -(5 + 13/60 + 1/3600), -(80 + 37/60 + 49/3600) } equals { -5.2169..., -80.6303... }
Optimized storage: { -(5*3600 + 13*60 + 1.3), -(80*3600 + 37*60 + 49.5) } equals { -18781.3, -290269.5 }
 *****************************************************************************/
static LocationType_t nmeaGetLatitude(uint8_t *nmeaLatitude /* null terminated string */)
{
    //format : ddmm.mmmmm
    LocationType_t lat;
    int degree;
    int minute_decimal;
    int minute_fraction = 0;
    int fraction_size = 1;

    degree  = (*nmeaLatitude++ - '0') * 10;
    degree += (*nmeaLatitude++ - '0');
    minute_decimal = (*nmeaLatitude++ - '0') * 10;
    minute_decimal += (*nmeaLatitude++ - '0');
    nmeaLatitude++;//skip '.'
    while (*nmeaLatitude)
    {
        minute_fraction = (minute_fraction * 10) + (*nmeaLatitude++ - '0');
        fraction_size *= 10;
    }

    lat = (LocationType_t)degree + (((LocationType_t)minute_decimal + ((LocationType_t)minute_fraction / (LocationType_t)fraction_size)) / 60.0);

    return lat;
}


static LocationType_t nmeaGetLongitude(uint8_t *nmeaLongitude /* null terminated string */)
{
    //format : dddmm.mmmmm
    LocationType_t lon;
    int degree;
    int minute_decimal;
    int minute_fraction = 0;
    int fraction_size = 1;

    degree  = (*nmeaLongitude++ - '0') * 100;
    degree += (*nmeaLongitude++ - '0') * 10;
    degree += (*nmeaLongitude++ - '0');
    minute_decimal  = (*nmeaLongitude++ - '0') * 10;
    minute_decimal += (*nmeaLongitude++ - '0');
    nmeaLongitude++;//skip '.'
    while (*nmeaLongitude)
    {
        minute_fraction = (minute_fraction * 10) + (*nmeaLongitude++ - '0');
        fraction_size *= 10;
    }

    lon = (LocationType_t)degree + (((LocationType_t)minute_decimal + ((LocationType_t)minute_fraction / (LocationType_t)fraction_size)) / 60.0);

    return lon;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
    raw_data data will be modified during parsing.
    After this fuction, the original data is not exist.

 The number of satellites in view, together with each SV ID, elevation azimuth, and signal
 strength (C/No) value. Only four satellite details are transmitted in one message.
 In a multi-GNSS system sets of GSV messages will be output multiple times, one
 set for each GNSS.

 ******************************************************************************************************/
static uint8_t nmea_xxGSV(uint8_t *raw_data, int32_t raw_length)
{
#if FEATURE_GPS_NMEA_GSV

    int32_t index;
    int32_t field_size;

    ////APP_TRACE_LOG("[GSV] %s\n", raw_data);

    for (index = NMEA_GPGSV_NUMMSG; index < NMEA_GPGSV_NUMSV + 1; index++)
    {
        for (field_size = 0; field_size < raw_length; field_size++)
        {
            if (raw_data[field_size] == (uint8_t)',' || raw_data[field_size] == '*')
            {
                raw_data[field_size] = '\0';
                break;
            }
        }

        switch (index)
        {
            case NMEA_GPGSV_NUMSV:      /* Number of satelites in view */
                if (field_size > 0)
                {
                    gnss.numSV = (*raw_data - '0') * 10 + (*(raw_data + 1) - '0');
                }
                break;
            default:
                break;
        }

        raw_data += (field_size + 1);
        raw_length -= (field_size + 1);
        if (raw_length < 3)
        {
            break;    /* no more filed check sum only */
        }
    }

#if GNSS_DEBUG
    APP_TRACE_LOG("[GSV] numSv=%d\n", gnss.numSV);
#endif
#endif

    return GPS_PARSE_OK;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
    raw_data data will be modified for easy parsing.
    After this fuction, the original data is not exist.

 The GNSS receiver operating mode, satellites used for navigation, and DOP values.
 . If less than 12 SVs are used for navigation, the remaining fields are left empty. If more
 than 12 SVs are used for navigation, only the IDs of the first 12 are output.
 . The SV numbers (fields 'sv') are in the range of 1 to 32 for GPS satellites, and 33 to 64
 for SBAS satellites (33 = SBAS PRN 120, 34 = SBAS PRN 121, and so on)
 In a multi-GNSS system this message will be output multiple times, once for each
 GNSS.

 ******************************************************************************************************/
static uint8_t nmea_xxGSA(uint8_t *raw_data, int32_t raw_length)
{
#if FEATURE_GPS_NMEA_GSA
    int32_t index;
    int32_t field_size;

    /* start point is after the first ',', ie, from field index 1*/
    for (index = 1; index < NMEA_GPGSA_MAX_FILED + 1; index++)
    {
        for (field_size = 0; field_size < raw_length; field_size++)
        {
            if (raw_data[field_size] == (uint8_t)',' || raw_data[field_size] == '*') // last fied end with '*'
            {
                raw_data[field_size] = '\0';// null terminate
                break;
            }
        }

        switch (index)
        {
            case 2:             /* navMode | 1-Fix not available 2-2D Fix 3-3D fix */
                if (field_size > 0)
                {
                    gnss.navMode = raw_data[0] - '0';
                }
                break;
            case 15: // PDOP
                if (field_size > 0)
                {
                    gnss.pDOP = strtof((char *)raw_data, DEF_NULL);
                }
                break;
            case 16: // HDOP
                if (field_size > 0)
                {
                    gnss.hDOP = strtof((char *)raw_data, DEF_NULL);
                }
                break;
            case 17: // VDOP
                if (field_size > 0)
                {
                    gnss.vDOP = strtof((char *)raw_data, DEF_NULL);
                }
                break;
            default:
                break;
        }

        raw_data += (field_size + 1); //next field
        raw_length -= (field_size + 1);
        if (raw_length < 3) // no more filed check sum only
        {
            break;
        }
    }

#if GNSS_DEBUG
    APP_TRACE_LOG("[GSA] [%d] pDop=%f, hDop=%f, vDop=%f\n",
                  gnss.navMode,
                  gnss.pDOP,
                  gnss.hDOP,
                  gnss.vDOP);
#endif

#endif

    return GPS_PARSE_OK;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         : Global positioning system fix data

    raw_data data will be modified for easy parsing.
    After this fuction, the original data is not exist.

 The output of this message is dependent on the currently selected datum (default:
 WGS84). The NMEA specification indicates that the GGA message is GPS specific.
 However, when the receiver is configured for multi-GNSS, the GGA message
 contents will be generated from the multi-GNSS solution. For multi-GNSS use, it is
 recommended that the NMEA-GNS message is used instead.
 Time and position, together with GPS fixing related data (number of satellites in use, and
 the resulting HDOP, age of differential data if in use, etc.).

 ******************************************************************************************************/
static uint8_t nmea_xxGGA(uint8_t *raw_data, int32_t raw_length)
{
#if FEATURE_GPS_NMEA_GGA
    int32_t index;
    int32_t field_size;
    bool is_loc_fix = false;

    /* start point is after the first ',', ie, from field index 1*/
    for (index = NMEA_GPGGA_TIME; index < NMEA_GPGGA_DIFF_STATION + 1; index++)
    {
        for (field_size = 0; field_size < raw_length; field_size++)
        {
            if (raw_data[field_size] == (uint8_t)',' || raw_data[field_size] == '*') // last fied end with '*'
            {
                raw_data[field_size] = '\0';// null terminate
                break;
            }
        }

        switch (index)
        {
            case NMEA_GPGGA_TIME:
                if (field_size >= 6)
                {
                    gnss.ing.utcTime = nmea_utctime_to_sec(raw_data);
                    //APP_TRACE_DEBUG("Seconds %d\r\n", (int)seconds);
                }
                break;
            case NMEA_GPGGA_LAT: //ddmm.mmmmm
                if (field_size > 9)
                {
                    gnss.ing.lat = nmeaGetLatitude(raw_data);
                    //APP_TRACE_DEBUG("NMEA_GPGGA_LAT %f\n", gnss.ing.lat);
                }
                break;
            case NMEA_GPGGA_NS:
                if (field_size > 0)
                {
                    if (*raw_data == 'S')
                    {
                        gnss.ing.lat *= -1;
                    }
                }
                break;
            case NMEA_GPGGA_LONG:
                if (field_size > 9)
                {
                    gnss.ing.lon = nmeaGetLongitude(raw_data);
                    //APP_TRACE_DEBUG("NMEA_GPGGA_LONG %f\n", gnss.ing.lon);
                }
                break;
            case NMEA_GPGGA_EW:
                if (field_size > 0)
                {
                    if (*raw_data == 'W')
                    {
                        gnss.ing.lon *= -1;
                    }
                }
                break;
            case NMEA_GPGGA_QUALITY:
                if (field_size > 0)
                {
                    if (*raw_data > '0')
                    {
                        gnss.ing.valid = true;
                        is_loc_fix = true;
                    }
                }
                break;
            case NMEA_GPGGA_NUM_SV:
                if (field_size > 0)
                {
                    /* Number of satellites used (range: 0-12) */
                    gnss.numGPSSVs = (*raw_data - '0') * 10 + (*(raw_data + 1) - '0');
                }
                break;
            case NMEA_GPGGA_HDOP:
                if (field_size > 0)
                {
                    /* x.xx -> x x x */
                    gnss.hDOP = (*raw_data - '0') * 100 + (*(raw_data + 2) - '0') * 10 + (*(raw_data + 3) - '0');
                }
                break;
            case NMEA_GPGGA_ALT:
                if (field_size > 0)
                {
                    gnss.ing.altitude = strtof((char *)raw_data, DEF_NULL);
                }
                break;
            case NMEA_GPGGA_U_ALT://fixed'M'
                break;
            case NMEA_GPGGA_SEP:
                if (field_size > 0)
                {
                    /* xx.x -> x x x */
                    gnss.sep = (*raw_data - '0') * 100 + (*(raw_data + 2) - '0') * 10 + (*(raw_data + 3) - '0');
                }
                break;
            case NMEA_GPGGA_U_SEP://fixed'M'
                break;
            case NMEA_GPGGA_DIFF_AGE:
                ;//APP_TRACE_DEBUG("Skip NMEA_GPGGA_DIFF_AGE\r\n");
                break;
            case NMEA_GPGGA_DIFF_STATION:
                ;//APP_TRACE_DEBUG("Skip NMEA_GPGGA_DIFF_STATION\r\n");
                break;
            default:
                break;
        }

        raw_data += (field_size + 1); //next field
        raw_length -= (field_size + 1);
        if (raw_length < 3) // no more filed check sum only
        {
            break;
        }
    }

#if GNSS_DEBUG
    APP_TRACE_LOG("[GGA] [%c] nSv=%d, Lat=%f, Lon=%f, utc=%ld\n",
                  (is_loc_fix == true) ? 'F' : 'N',
                  gnss.numGPSSVs,
                  gnss.ing.lat,
                  gnss.ing.lon,
                  gnss.ing.utcTime);
#endif

    if (is_loc_fix == true)
    {
        gnssPowerOff();
    }
#endif

    return GPS_PARSE_OK;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         : Global positioning system fix data

    raw_data data will be modified for easy parsing.
    After this fuction, the original data is not exist.

 The output of this message is dependent on the currently selected datum (default:WGS84)
 Time and position, together with GNSS fixing related data (number of satellites in use, and
 the resulting HDOP, age of differential data if in use, etc.).

 ******************************************************************************************************/
static uint8_t nmea_xxGNS(uint8_t *raw_data, int32_t raw_length)
{
#if FEATURE_GPS_NMEA_GNS

    /* Value indexes */
#define NMEA_GNS_TIME                 1
#define NMEA_GNS_LAT                  2
#define NMEA_GNS_NS                   3
#define NMEA_GNS_LONG                 4
#define NMEA_GNS_EW                   5
#define NMEA_GNS_POS_MODE             6
#define NMEA_GNS_NUM_SV               7
#define NMEA_GNS_HDOP                 8
#define NMEA_GNS_ALT                  9
#define NMEA_GNS_SEP                  10
#define NMEA_GNS_DIFF_AGE             11
#define NMEA_GNS_DIFF_STATION         12
#define NMEA_GNS_NAV_STATUS           13   // NMEA v4.1

    int32_t index;
    int32_t field_size;

    /* start point is after the first ',', ie, from field index 1*/

    //APP_TRACE_DEBUG("[GNS]%s\n", raw_data);

    for (index = NMEA_GNS_TIME; index < NMEA_GNS_DIFF_STATION + 1; index++)
    {

        for (field_size = 0; field_size < raw_length; field_size++)
        {
            if (raw_data[field_size] == (uint8_t)',' || raw_data[field_size] == '*') // last fied end with '*'
            {
                raw_data[field_size] = '\0';// null terminate
                break;
            }
        }

        //APP_TRACE_DEBUG("%s\n", raw_data);
        //APP_TRACE_DEBUG("%d filed size = %d\n", (int)index, (int)field_size);

        switch (index)
        {
            case NMEA_GNS_TIME:
                if (field_size > 0)
                {
                    // TO DO: fix as correct routine, this is for testing
                    gnss.ing.utcTime = Str_ParseNbr_Int32U((const  CPU_CHAR *)raw_data,
                                                           DEF_NULL,
                                                           DEF_NBR_BASE_DEC);

                    //APP_TRACE_DEBUG("UTC time %d\r\n", (int)gnss.ing.utcTime);
                }
                break;
            case NMEA_GNS_LAT: //ddmm.mmmmm
                if (field_size > 9)
                {
                    gnss.ing.lat = nmeaGetLatitude(raw_data);
                    //APP_TRACE_DEBUG("NMEA_GNS_LAT %f\r\n", gnss.ing.lat);
                }
                break;
            case NMEA_GNS_NS:
                if (field_size > 0)
                {
                    if (*raw_data == 'S')
                    {
                        gnss.ing.lat *= -1;
                    }
                }
                break;
            case NMEA_GNS_LONG:
                if (field_size > 9)
                {
                    gnss.ing.lon = nmeaGetLongitude(raw_data);
                    //APP_TRACE_DEBUG("NMEA_GNS_LONG %f\r\n", gnss.ing.lon);
                }
                break;
            case NMEA_GNS_EW:// east west
                if (field_size > 0)
                {
                    if (*raw_data == 'W')
                    {
                        gnss.ing.lon *= -1;
                    }
                }
                break;
            case NMEA_GNS_POS_MODE:// N, E, F, R, A/D
                if (field_size > 0)
                {
                    gnss.posMode = *raw_data;
                    gnss.posModeGLO = *(raw_data + 1);
                    //APP_TRACE_DEBUG("POS MODE %s\r\n", raw_data);
                }
                break;
            case NMEA_GNS_NUM_SV:
                if (field_size > 0)
                {
                    gnss.numGPSSVs = (*raw_data - '0') * 10 + (*(raw_data + 1) - '0');
                }
                break;
            case NMEA_GNS_HDOP:
                if (field_size > 0)
                {
                    /* x.xx -> x x x store as x100 */
                    gnss.hDOP = (*raw_data - '0') * 100 + (*(raw_data + 2) - '0') * 10 + (*(raw_data + 3) - '0');
                }
                break;
            case NMEA_GNS_ALT:
                if (field_size > 0)
                {
                    gnss.ing.altitude = strtof((char *)raw_data, DEF_NULL);
                }
                break;
            case NMEA_GNS_SEP:
                if (field_size > 0)
                {
                    /* xx.x -> x x x */
                    gnss.sep = (*raw_data - '0') * 100 + (*(raw_data + 2) - '0') * 10 + (*(raw_data + 3) - '0');
                }
                break;
            case NMEA_GNS_DIFF_AGE:
                ;//APP_TRACE_DEBUG("Skip NMEA_GNS_DIFF_AGE\r\n");
                break;
            case NMEA_GNS_DIFF_STATION:
                ;//APP_TRACE_DEBUG("Skip NMEA_GNS_DIFF_STATION\r\n");
                break;
            case NMEA_GNS_NAV_STATUS: // NMEA v4.1
            default:
                break;
        }

        raw_data += (field_size + 1); //next field
        raw_length -= (field_size + 1);
        // check if there is a filed
        if (raw_length < 3) // no more field
        {
            break;
        }
    }
#endif

    return GPS_PARSE_OK;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         : Recommended Minimum data

    raw_data data will be modified for easy parsing.
    After this fuction, the original data is not exist.

 The output of this message is dependent on the currently selected datum (default:
 WGS84)  The recommended minimum sentence defined by NMEA for GNSS system data.

 ******************************************************************************************************/
static uint8_t nmea_xxRMC(uint8_t *raw_data, int32_t raw_length)
{
#if FEATURE_GPS_NMEA_RMC
    /* Value indexes */
#define NMEA_xxRMC_TIME                 1
#define NMEA_xxRMC_STATUS               2
#define NMEA_xxRMC_LAT                  3
#define NMEA_xxRMC_NS                   4
#define NMEA_xxRMC_LONG                 5
#define NMEA_xxRMC_EW                   6
#define NMEA_xxRMC_SPD                  7
#define NMEA_xxRMC_COG                  8
#define NMEA_xxRMC_DATE                 9
#define NMEA_xxRMC_MV                   10
#define NMEA_xxRMC_MV_EW                11
#define NMEA_xxRMC_POS_MODE             12
#define NMEA_xxRMC_NAV_STATUS           13     //16-3//NMEA v4.1 and above only

    int32_t index;
    int32_t field_size;
    uint32_t seconds = 0; // utc seconds
    char    status = 'V'; // field 2
    /* start point is after the first ',', ie, from field index 1*/

    //APP_TRACE_DEBUG("[RMC]%s\n", raw_data);


    for (index = NMEA_xxRMC_TIME; index < NMEA_xxRMC_POS_MODE + 1; index++)
    {

        for (field_size = 0; field_size < raw_length; field_size++)
        {
            if (raw_data[field_size] == ',' || raw_data[field_size] == '*')
            {
                raw_data[field_size] = '\0';// split with null terminate
                break;
            }
        }
        //APP_TRACE_DEBUG("%s\r\n", raw_data);
        ///APP_TRACE_DEBUG("%d filed size = %d\r\n", (int)index, (int)field_size);

        switch (index)
        {
            case NMEA_xxRMC_TIME:
                if (field_size >= 6)
                {
                    seconds = nmea_utctime_to_sec(raw_data);
                    //APP_TRACE_DEBUG("Seconds %d\r\n", (int)seconds);
                }
                break;
            case NMEA_xxRMC_STATUS:
                if (field_size > 0)
                {
                    status = *raw_data;
                }
                break;
            case NMEA_xxRMC_LAT:
                if (field_size > 9 && status == 'A')
                {
                    gnss.ing.lat = nmeaGetLatitude(raw_data);
                    //APP_TRACE_DEBUG("NMEA_xxRMC_LAT %f\r\n", gnss.ing.lat);
                }
                break;
            case NMEA_xxRMC_NS:
                if (field_size > 0 && status == 'A')
                {
                    if (*raw_data == 'S')
                    {
                        gnss.ing.lat *= -1;
                    }
                }
                break;
            case NMEA_xxRMC_LONG:
                if (field_size > 9 && status == 'A')
                {
                    gnss.ing.lon = nmeaGetLongitude(raw_data);
                    //APP_TRACE_DEBUG("NMEA_xxRMC_LONG %f\r\n", gnss.ing.lon);
                }
                break;
            case NMEA_xxRMC_EW:
                if (field_size > 0 && status == 'A')
                {
                    if (*raw_data == 'W')
                    {
                        gnss.ing.lon *= -1;
                    }
                }
                break;
            case NMEA_xxRMC_SPD:
                break;
            case NMEA_xxRMC_COG:
                break;
            case NMEA_xxRMC_DATE:
                if (field_size >= 6 && status == 'A')
                {
                    seconds += nmea_utcdate_to_sec(raw_data);
                    //APP_TRACE_DEBUG("unix time %d \r\n", (int)seconds);
                    gnss.ing.utcTime = seconds;
                }
                break;
            case NMEA_xxRMC_MV:
                //Magnetic variation value. Only supported in ADR 4.10 and above.
                break;
            case NMEA_xxRMC_MV_EW:
                //Magnetic variation E/W indicator. Only supported in ADR 4.10 and above.
                break;
            case NMEA_xxRMC_POS_MODE:
                if (field_size > 0)
                {
                    gnss.posMode = *raw_data;
                    //APP_TRACE_DEBUG("RMC_POS_MODE: %c\r\n", gnss.posMode);
                }
                break;
            case NMEA_xxRMC_NAV_STATUS://NMEA v4.1 and above only
                if (field_size > 0)
                {
                    //APP_TRACE_DEBUG("%s\r\n", raw_data);
                }
                break;

            default:
                break;
        }

        raw_data += (field_size + 1); //next field
        raw_length -= (field_size + 1);
        if (raw_length < 3) // no more filed check sum only
        {
            break;//exit
        }
    }
#endif

    return GPS_PARSE_OK;
}

#ifdef RTOS_MODULE_COMMON_SHELL_AVAIL

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :

 ******************************************************************************************************/
static  CPU_INT16S  gnss_dbg(CPU_INT16U        argc,
                             CPU_CHAR         *p_argv[],
                             SHELL_OUT_FNCT    out_fnct,
                             SHELL_CMD_PARAM  *p_cmd_param)
{

    //uint8_t  buff[32];
    if (argc != 2)
    {
        APP_TRACE_DEBUG("usage: gnss [COMMNAD] \r\n on, off, ubx, nmea, config, ... \r\n");
        return SHELL_EXEC_ERR;
    }

    APP_TRACE_DEBUG(" argv[%d] = %s\n", 1, p_argv[1]);

    if (0 == Str_Cmp(p_argv[1], "on"))
    {
        //gnssPowerOn();
        if (GPIO_PinOutGet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN) == 0)
        {
            APP_TRACE_LOG("MODULE [ON]\n");
            ring_buffer_reset(&gnss_rx_rb);
            GPIO_PinOutSet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN);     // power up the GPS
            gnss_reset_to_coldstart();
        }
    }
    else if (0 == Str_Cmp(p_argv[1], "off"))
    {
        //gnssPowerOff();
        /* turn off gps only normal mode */
        APP_TRACE_LOG("MODULE [OFF]\n");
        GPIO_PinOutClear(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN);    // power down the GPS
        ring_buffer_reset(&gnss_rx_rb);
        gnss.numSV = 0;
        gnss.numGPSSVs = 0;
    }
    else if (0 == Str_Cmp(p_argv[1], "conf"))
    {

        //APP_TRACE_DEBUG("custom setting( not save )\r\n");
        //gnssConfig();
        RTOS_ERR err;
        gnss_config.saveConfig = 0;
        OSFlagPost(&gnssEventFlags,
                   GNSS_EVT_CONFIG,
                   OS_OPT_POST_FLAG_SET,
                   &err);
    }
    else if (0 == Str_Cmp(p_argv[1], "save"))
    {
        APP_TRACE_DEBUG("save current config\r\n");
        ubxSetCfgCFG(0x00, 0x0F, 0x0F);
        gnss_config.saveConfig = 1;
    }
    else if (0 == Str_Cmp(p_argv[1], "clear"))
    {
        APP_TRACE_DEBUG("factory default\r\n");
        ubxSetCfgCFG(0xFF, 0x00, 0xFF);
        gnss_config.saveConfig = 1;// do not overwite
    }
    else if (0 == Str_Cmp(p_argv[1], "cs"))
    {
        gnss_reset_to_coldstart();
    }
    else if (0 == Str_Cmp(p_argv[1], "hs"))
    {
        gnss_reset_to_hotstart();    // hardfault
    }
    else if (0 == Str_Cmp(p_argv[1], "ws"))
    {
        gnss_reset_to_warmstart();    //
    }
    else if (0 == Str_Cmp(p_argv[1], "power"))
    {
        ubxSetCfgPMS(GNSS_POWER_INTERVAL, gnss_config.periodSeconds, gnss_config.stayOnSeconds);
    }
    else if (0 == Str_Cmp(p_argv[1], "ubx"))
    {
        ubxSetCfgPRT(1, 1, 0, 0);
        //ubxSend(0x06, 0x00, 20, "\x01\x00\x00\x00\xC0\x08\x00\x00\x80\x25\x00\x00\x01\x00\x01\x00\x00\x00\x00\x00");

    }
    else if (0 == Str_Cmp(p_argv[1], "nmea"))
    {
        ubxSetCfgPRT(1, 1, 1, 1);
        //ubxSend(0x06, 0x00, 20, "\x01\x00\x00\x00\xC0\x08\x00\x00\x80\x25\x00\x00\x03\x00\x03\x00\x00\x00\x00\x00");
    }
    else if (0 == Str_Cmp(p_argv[1], "fix"))
    {
        int32_t latitude, longitude;
        int16_t altitude = 0;
        gnssGetPositionBinary(&latitude, &longitude, &altitude);
        APP_TRACE_DEBUG("latitude = 0x%lx, longitude = 0x%lx, altitude = 0x%x\n", latitude, longitude, altitude);
    }
    else if (0 == Str_Cmp(p_argv[1], "bypass"))
    {
        g_gnss_bypass = true;
    }
    else if (0 == Str_Cmp(p_argv[1], "pm"))
    {
        uint8_t msg[16] =
        {
            0x00,                       /* version */
            0x00, 0x00, 0x00,           /* reserved1 */
            0x00, 0x00, 0x00, 0x00,     /* duration (ms) */
            0x00, 0x00, 0x00, 0x00,     /* flags */
            0x08, 0x00, 0x00, 0x00,     /* wakeupSources */
        };
        uint32_t *duration = (uint32_t *)(&msg[4]);
        *duration = 30000;  /* 30 sec */

        ubxSend(0x02, 0x41, 16, &msg);
    }
    else if (0 == Str_Cmp(p_argv[1], "od"))
    {
      AssistNow_sendMgaDataToModule();
    }
    else
    {
        APP_TRACE_DEBUG("not implemented\n");
    }

    return (SHELL_EXEC_ERR_NONE);
}

static  CPU_INT16S  gnss_dbg_ubx_poll(CPU_INT16U        argc,
                                      CPU_CHAR         *argv[],
                                      SHELL_OUT_FNCT    out_fnct,
                                      SHELL_CMD_PARAM  *p_cmd_param)
{
    uint8_t cls, id;

    if (argc != 3)
    {
        APP_TRACE_DEBUG("\t - All number are treated as hex\n");
        APP_TRACE_DEBUG("\t   ex) ubxpoll 06 13  : poll 0x06 0x13\n");
        return (SHELL_EXEC_ERR_NONE);
    }

    argv++; // skip ubxpoll

    if (*argv != NULL)
    {
        cls = Str_ParseNbr_Int32U((const  CPU_CHAR *)*argv++,
                                  DEF_NULL,
                                  DEF_NBR_BASE_HEX);
        if (*argv != NULL)
        {
            id = Str_ParseNbr_Int32U((const  CPU_CHAR *)*argv++,
                                     DEF_NULL,
                                     DEF_NBR_BASE_HEX);

            ubxSend(cls, id, 0, DEF_NULL);
        }
    }

    return (SHELL_EXEC_ERR_NONE);
}


SHELL_CMD  GnssDebug_CmdTbl [] =
{
    { "gnss",    gnss_dbg          },
    { "ubxpoll", gnss_dbg_ubx_poll },
    { 0,         0                 }
};
#endif

/*******************************************************************************
*                              PUBLIC FUNCTIONS
*******************************************************************************/
/*****************************************************************************
 * Description : GNSS UART IRQ Handler
 *****************************************************************************/
void gnssRxISR(void) //void LEUART0_IRQHandler(void)
{
    ring_buffer_t *rb = &gnss_rx_rb;
    uint16_t next;

    while (DEF_ON)
    {
        next = rb->head + 1;
        if (next >= rb->size)
        {
            next = 0;
        }

        if (next == rb->tail)
        {
            //rx buffer full
            LEUART_IntDisable(LEUART0, LEUART_IF_RXDATAV);
            break;
        }
        //not full

        if ((LEUART0->STATUS & LEUART_STATUS_RXDATAV) == 0)
        {
            break;
        }

        rb->element[rb->head] = LEUART_RxDataGet(LEUART0);
        rb->head = next;
    }
}

int32_t GetRxFifo(uint8_t *pc)
{
    ring_buffer_t *rb = &gnss_rx_rb;
    CPU_SR_ALLOC();

    if (rb->element)
    {
        uint16_t rd_idx = rb->tail;
        if (rd_idx == rb->head)
        {
            LEUART_IntEnable(LEUART0, LEUART_IF_RXDATAV);
            return 0;// buffer empty
        }

        CPU_CRITICAL_ENTER();
        {
            *pc = rb->element[rd_idx++];
            if (rd_idx >= rb->size)
            {
                rd_idx = 0;
            }
            rb->tail = rd_idx;
        }
        CPU_CRITICAL_EXIT();

        //if (*pc != '\r') debug_printf_data("%c", *pc);

        return 1;// one byte read
    }

    return -1;// NO  RING BUFFER CASE
}

/*****************************************************************************
 * Description : Intializes UART
 *****************************************************************************/
void gnssPortInit(void)
{
    /* Enable peripheral clocks */
    CMU_ClockEnable(cmuClock_HFPER, true);

    /* Configure GPIO pins */
    CMU_ClockEnable(cmuClock_GPIO, true);

    LEUART_TypeDef *leuart = LEUART0;
    LEUART_Init_TypeDef init =
    {
        leuartEnable,
        0,
        9600,
        leuartDatabits8,
        leuartNoParity,
        leuartStopbits1
    };

    /* Enable CORE LE clock in order to access LE modules */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Select LFXO for LEUARTs (and wait for it to stabilize) */
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);

    CMU_ClockEnable(cmuClock_LEUART0, true);

    /* Do not prescale clock */
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_1);

    /* Configure LEUART */
    init.enable = leuartDisable;

    LEUART_Init(leuart, &init);
    ring_buffer_init(&gnss_rx_rb, gnssRxFifo, GNSS_RX_FIFO_SIZE);

    /* Enable pins at default location */
    leuart->ROUTEPEN = USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
    leuart->ROUTELOC0 = (leuart->ROUTELOC0
                         & ~(_LEUART_ROUTELOC0_TXLOC_MASK
                             | _LEUART_ROUTELOC0_RXLOC_MASK))
                        | (BSP_GPS_TX_LOC << _LEUART_ROUTELOC0_TXLOC_SHIFT)
                        | (BSP_GPS_RX_LOC << _LEUART_ROUTELOC0_RXLOC_SHIFT);

    GPIO_PinModeSet(BSP_GPS_TX_PORT, BSP_GPS_TX_PIN, gpioModePushPull, 1);
    GPIO_PinModeSet(BSP_GPS_RX_PORT, BSP_GPS_RX_PIN, gpioModeInputPull, 1);

    // Clear any false IRQ request
    LEUART_IntClear(leuart, ~0x00);
    NVIC_ClearPendingIRQ(LEUART0_IRQn);


    CPU_IntSrcHandlerSetKA(LEUART0_IRQn + CPU_INT_EXT0, gnssRxISR);

    /* Enable RX interrupts */
    LEUART_IntEnable(leuart, LEUART_IF_RXDATAV);
    NVIC_EnableIRQ(LEUART0_IRQn);

    /* Finally enable it */
    LEUART_Enable(leuart, leuartEnable);

}

/*!
 * \brief Parses the NMEA sentence.
 *
 * \remark Only parses GPGGA and GPRMC sentences
 *
 * \param [IN] raw_data Data buffer to be parsed
 * \param [IN] raw_length Size of data buffer
 *
 * \retval status
 */
/**
 *  test string
    //some example/test strings
    char test_gprmc[] = "$GPRMC,112911.000,A,4856.3328,N,01146.8259,E,35.75,108.51,050807,,,A*57";
    char test_gpgga[] = "$GPGGA,112911.000,4856.3328,N,01146.8259,E,1,06,2.0,353.1,M,47.5,M,,0000*50";
    char test_gpgsa[] = "$GPGSA,A,3,27,28,10,29,08,26,,,,,,,3.2,2.0,2.5*3F";
    char test_gpgs1[] = "$GPGSV,3,1,12,27,17,075,20,19,07,027,,21,05,296,,18,11,325,*77";
    char test_gpgs2[] = "$GPGSV,3,2,12,28,62,095,44,10,32,199,30,29,79,302,40,08,40,067,43*71";
    char test_gpgs3[] = "$GPGSV,3,3,12,09,08,259,29,26,65,304,45,24,02,263,,17,08,134,28*7E";
*/

uint8_t gnss_talker_id = 0;
uint8_t nmeaDecoder(uint8_t *raw_data, int32_t raw_length)
{
    uint32_t field_type;

    if (0 != nmea_validate(raw_data, raw_length, 1))
    {
        //APP_TRACE_LOG("nmea format error\n");
        return GPS_PARSE_FAIL;
    }

    //most receivers sent GPRMC as last, but u-blox send as first: use other trigger
    //determine most suitable trigger on actually received messages
    field_type = NMEA_TAG(raw_data + 1);  // $XXXXX

    if (field_type != NmeaTypeGxGSV)
    {
        /*The GSV message reports the signal strength of the visible satellites. However,
        the Talker ID it uses is specific to the GNSS it is reporting information for, so
        for a multi-GNSS receiver it will not be the same as the main Talker ID. (e.g.
        other messages will be using the 'GN' Talker ID but the GSV message will use
        GNSS-sepcific Talker IDs)
        */
        if (raw_data[2] != gnss_talker_id)
        {
            gnss_talker_id = raw_data[2];
            //APP_TRACE_DEBUG("nmea talker id G%c\n", gnss_talker_id);
        }
    }

    if (g_gnss_bypass)
    {
        printf("%s\n", raw_data);
        return GPS_PARSE_OK;
    }
    else
    {
        switch (field_type)
        {
            case NmeaTypeGxTXT:
            {
                //APP_TRACE_DEBUG("%s\n", (const char *)raw_data + 7); //NMEA_PREFIX_LENGTH + $ + ,

                // last boot screen message
                if (DEF_NULL != Str_Str_N((char *)raw_data + 7, "PF=", raw_length - 7))
                {
                    RTOS_ERR err;

                    g_gnss_prove = true;

                    OSFlagPost(&gnssEventFlags,
                               GNSS_EVT_CONFIG,
                               OS_OPT_POST_FLAG_SET,
                               &err);
                }

                return GPS_PARSE_OK;
            }
            case NmeaTypeGxGNS:
                return nmea_xxGNS(raw_data + 7, raw_length - 7);
            case NmeaTypeGxGGA:
                return nmea_xxGGA(raw_data + 7, raw_length - 7);
            case NmeaTypeGxRMC:
                return nmea_xxRMC(raw_data + 7, raw_length - 7); //GSA better than RMC
            case NmeaTypeGxGSV:
                return nmea_xxGSV(raw_data + 7, raw_length - 7);
            case NmeaTypeGxGSA:
                return nmea_xxGSA(raw_data + 7, raw_length - 7);
            default:
                //APP_TRACE_DEBUG("Not parsing: %c%c%c%c%c\r\n", raw_data[1],raw_data[2],raw_data[3],raw_data[4],raw_data[5]);
                //APP_TRACE_DEBUG("%s\n", raw_data);
                return GPS_PARSE_OK;
        }
    }
}

/*****************************************************************************
 * @brief


 -- The value is converted to a three-byte binary value.  The minimum value  that  can
 be  stored  is  -8,388,608;  the maximum is 8,388,607.  Some numbers outside this range
 will be accepted on input and converted without warning to a number within  the  range.
 To  allow  null  values,  use  "11"  instead  (see  "value" above).

 *****************************************************************************/
void gnssGetPositionBinary(int32_t *lat, int32_t *lon, int16_t *alt)
{
    /* Value used for the conversion of the position from DMS to decimal */
#define  MaxNorthPosition  8388607       // 2^23 - 1
#define  MaxSouthPosition  8388608       // -2^23
#define  MaxEastPosition   8388607       // 2^23 - 1
#define  MaxWestPosition   8388608       // -2^23

    long double temp;

    if (gnss.ing.lat >= 0) // North
    {
        temp = gnss.ing.lat * MaxNorthPosition;
        *lat = temp / 90;
    }
    else // South
    {
        temp = gnss.ing.lat * MaxSouthPosition;
        *lat = temp / 90;
    }

    if (gnss.ing.lon >= 0)  // East
    {
        temp = gnss.ing.lon * MaxEastPosition;
        *lon = temp / 180;
    }
    else                // West
    {
        temp = gnss.ing.lon * MaxWestPosition;
        *lon = temp / 180;
    }

    *alt = round(gnss.ing.altitude);
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 BX-CFG-RST (0x06 0x04)

Cold start
In cold start mode, the receiver has no information from the last position (e.g. time, velocity,
frequency etc.) at startup. Therefore, the receiver must search the full time and frequency space, and all
possible satellite numbers. If a satellite signal is found, it is tracked to decode the ephemeris (18-36 seconds
under strong signal conditions), whereas the other channels continue to search satellites. Once there is a
sufficient number of satellites with valid ephemeris, the receiver can calculate position and velocity data.
Other GNSS receiver manufacturers call this startup mode Factory Startup.


Warm start
In warm start mode, the receiver has approximate information for time, position, and coarse
satellite position data (Almanac). In this mode, after power-up, the receiver normally needs to download
ephemeris before it can calculate position and velocity data. As the ephemeris data usually is outdated after
4 hours, the receiver will typically start with a Warm start if it has been powered down for more than 4
hours. In this scenario, several augmentations are possible. See the section on Multi-GNSS Assistance.


Hot start
In hot start mode, the receiver was powered down only for a short time (4 hours or less), so that
its ephemeris is still valid. Since the receiver doesn't need to download ephemeris again, this is the fastest
startup method.

 ******************************************************************************************************/
// Receiver Reset Messages - Force Cold Start
void gnss_reset_to_coldstart(void)
{
    APP_TRACE_LOG("Receiver reset to cold start state.\n");
    ubxSetCfgRST(0xFFFF, 0x02);
}

// Receiver Reset Messages - Force Warm Start
void gnss_reset_to_warmstart(void)
{
    APP_TRACE_DEBUG("Receiver reset to warm start state.\n");
    ubxSetCfgRST(0x0001, 0x02);
}

// Receiver Reset Messages - Force Hot Start
void gnss_reset_to_hotstart(void)
{
    APP_TRACE_DEBUG("Receiver reset to hot start state.\n");
    ubxSetCfgRST(0x0000, 0x02);
}

uint8_t gps_position_get(uint8_t *value)
{
    int32_t latitude = 0, longitude = 0;
    int16_t altitude = 0;
    bool is_valid = gnss.ing.valid;

    if (is_valid == true)
    {
        gnssGetPositionBinary(&latitude, &longitude, &altitude);

        gnss.ing.valid = false;

        gnss.last.valid    = gnss.ing.valid;
        gnss.last.lat      = gnss.ing.lat;
        gnss.last.lon      = gnss.ing.lon;
        gnss.last.altitude = gnss.ing.altitude;
        gnss.last.utcTime  = gnss.ing.utcTime;
    }

    value[0] = (latitude >> 16) & 0xFF;
    value[1] = (latitude >> 8) & 0xFF;
    value[2] = latitude & 0xFF;

    value[3] = (longitude >> 16) & 0xFF;
    value[4] = (longitude >> 8) & 0xFF;
    value[5] = longitude & 0xFF;

    return (is_valid == true) ? 1 : 0;
}

void gps_setLastPosition(LocationUpdateParams_t gps)
{
  gnss.last.valid    = gps.valid;
  gnss.last.lat      = gps.lat;
  gnss.last.lon      = gps.lon;
  gnss.last.altitude = gps.altitude;
  gnss.last.utcTime  = gps.utcTime;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
void gnssPowerOn(void)
{
    RTOS_ERR err;

    APP_TRACE_LOG("%s : (BLE - %s)\n", __FUNCTION__, (BLE_APP_IsConnected == true) ? "ON " : "OFF");

    if (BLE_APP_IsConnected == true) return;

    OSFlagPost(&gnssEventFlags,
               GNSS_EVT_POWER_ON,
               OS_OPT_POST_FLAG_SET,
               &err);

    g_gnss_prove = false;
}


/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
void gnssPowerOff(void)
{
    RTOS_ERR err;

    if (MainApp_GetOpMode() == MAIN_OP_MODE_SOS) return;

    APP_TRACE_LOG("%s\n", __FUNCTION__);

    OSFlagPost(&gnssEventFlags,
               GNSS_EVT_POWER_OFF,
               OS_OPT_POST_FLAG_SET,
               &err);
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
bool gnssSetPowerMode(uint8_t power)
{
    if (GPIO_PinOutGet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN))
    {
        if (power == GNSS_POWER_HIGH)
        {
            ubxSetCfgPMS(GNSS_POWER_HIGH, 0, 0);//Balanced
        }
        else if (power == GNSS_POWER_LOW)
        {
            ubxSetCfgPMS(GNSS_POWER_LOW, 0, 0);//aggresive
        }
        else if (power == GNSS_POWER_INTERVAL)
        {
            ubxSetCfgPMS(GNSS_POWER_INTERVAL, gnss_config.periodSeconds, gnss_config.stayOnSeconds);
        }
        gnss_config.powerMode = power;
        return true;
    }
    gnss_config.powerMode = power;
    return false;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
bool gnssLocAvailable(void)
{
    return gnss.last.valid;
}

const LocationUpdateParams_t *gnssGetLastLoc(void)
{
    return &(gnss.last);
    //return (gnss.last.valid) ? &(gnss.last) : NULL;
}

bool gnssProve(void)
{
    return g_gnss_prove;
}

void gnssTask(void *p_arg)
{
    OS_FLAGS ret_flag = 0;
    RTOS_ERR err;

    PP_UNUSED_PARAM(p_arg);

    TimerInit(&g_gnss_cont_timer, GnssContTimerCb);

    gnssPortInit();

    while (DEF_TRUE)
    {
        ret_flag = OSFlagPend(&gnssEventFlags,
                              GNSS_EVT_ALL,
                              0,
                              OS_OPT_DEFAULT,
                              NULL,
                              &err);

        if (ret_flag & GNSS_EVT_POWER_ON)
        {
            if (GPIO_PinOutGet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN) == 0)
            {
                APP_TRACE_LOG("MODULE [ON]\n");
                ring_buffer_reset(&gnss_rx_rb);
                GPIO_PinModeSet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN, gpioModePushPull, 1);
            }

            // ChAng GPS
            AssistNow_sendMgaDataToModule();
        }

        if (ret_flag & GNSS_EVT_POWER_OFF)
        {
            /* turn off gps only normal mode */
            APP_TRACE_LOG("MODULE [OFF]\n");
            GPIO_PinModeSet(BSP_GPS_EN_PORT, BSP_GPS_EN_PIN, gpioModePushPull, 0);
            ring_buffer_reset(&gnss_rx_rb);
            gnss.numSV = 0;
            gnss.numGPSSVs = 0;
            continue;
        }

        if (ret_flag & GNSS_EVT_CONFIG)
        {
            if (gnss_config.saveConfig == 0)
            {
                APP_TRACE_LOG("gnss config\n\n\n\n");
                /* rx ring buffer large enough for ubx data */
                gnssConfig();
                gnssSetPowerMode(gnss_config.powerMode);
                ubxSetCfgCFG(0x0F, 0x0F, 0x0F);// save
                gnss_config.saveConfig = 1;
            }
        }
    }
}

uint8_t GetRxCmd(const uint8_t *pdata, uint16_t *size)
{
    uint8_t recv_done = 0;
    uint8_t byte;
    uint16_t i = 0;
    TimerTime_t cur_time;

    Mem_Set(g_nmea_rx_buff, '\0', GPS_BUFFER_MAX);

    while (recv_done == 0)
    {
        /* Get ch from ring buffer, and check timeout */
        cur_time = TimerGetCurrentTime();
        while (GetRxFifo(&byte) <= 0)
        {
            if (TimerGetElapsedTime(cur_time) >= 2000)
            {
                return 0;
            }
            SYSOS_DelayMs(100);
        }

        /* Process the response */
        g_nmea_rx_buff[i] = byte;

        if (i != 0)
        {
            if ((g_nmea_rx_buff[i - 1] == '\r') && (g_nmea_rx_buff[i] == '\n'))
            {
                g_nmea_rx_buff[i - 1] = '\0';
                nmeaDecoder(g_nmea_rx_buff, i - 1);
                recv_done = 1;
            }
        }

        i++;
    }

    return 1;
}

void gnssCommTask(void *p_arg)
{
    static  gnss_line_t    gnss_parse_data;
    gnss_line_t *gnss_parse_ptr = &gnss_parse_data;

    memset(gnss_parse_ptr, 0, sizeof(gnss_line_t));
    gnss_parse_ptr->size        = 0;
    gnss_parse_ptr->ubx_size    = 0;
    gnss_parse_ptr->packet_type = GNSS_LINE_WAIT;

    PP_UNUSED_PARAM(p_arg);

    while (DEF_ON)
    {
        GetRxCmd((const uint8_t *)&gnss_parse_ptr->data[0], &gnss_parse_ptr->size);
        SYSOS_DelayMs(50);
    }
}

void gps_task_start(OS_PRIO task_priority)
{
    RTOS_ERR err;

    OSFlagCreate(&gnssEventFlags,
                 "gnss evt",
                 0,
                 &err);

    OSTaskCreate(&gnssAppTaskTCB,
                 "GNSS Task",
                 gnssTask,
                 0u,
                 task_priority,
                 &gnssAppTaskStk[0u],
                 GPS_APP_TASK_STK_SIZE / 10u,
                 GPS_APP_TASK_STK_SIZE,
                 0u,
                 0u,
                 0u,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);

    OSTaskCreate(&gnssCommTaskTCB,
                 "GNSS Comm Task",
                 gnssCommTask,
                 0u,
                 GPS_COMM_TASK_PRIO,
                 &gnssCommTaskStk[0u],
                 GPS_COMM_TASK_STK_SIZE / 10u,
                 GPS_COMM_TASK_STK_SIZE,
                 0u,
                 0u,
                 0u,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);

    gnss.last.valid = false;
    gnss.last.lat = 37.544456;
    gnss.last.lon = 127.047667;
    gnss.last.altitude = 10.00;

#ifdef RTOS_MODULE_COMMON_SHELL_AVAIL
    Shell_CmdTblAdd((CPU_CHAR *)"GNSS", GnssDebug_CmdTbl, &err);
#endif
}
#endif
