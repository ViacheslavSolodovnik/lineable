/****************************************************************************************
//
//
//  Copyright 2018 Lineable (C) by All Rights Reserved.
//  Unauthorized redistribution of this source code, in whole or part,
//  without the express written permission of Lineable is strictly prohibited.
//---------------------------------------------------------------------------------------
//   Author:
//   File name :
//   Created :
//   Last Update :
****************************************************************************************/

#ifndef __GPS_H__
#define __GPS_H__

/*!
 * Generic definition
 */
#ifndef GPS_PARSE_OK
#define GPS_PARSE_OK                                0
#endif

#ifndef GPS_PARSE_FAIL
#define GPS_PARSE_FAIL                              1
#endif

/**
 * Description of the time scale used.
 */
typedef enum {
    GNSS_TIME_SCALE_UTC = 0,        /**< GNSS time is provided according UTC time scale (with leap seconds). This is the preferred time scale. */
    GNSS_TIME_SCALE_GPS = 1         /**< GNSS time is provided according GPS time scale (no leap seconds since 06-Jan-1980). This time scale will only be used if UTC is not available. */
} EGNSSTimeScale;


/// The maximum time to wait for a GNSS fix
#define GNSS_MAX_ON_TIME_SECONDS (60 * 10)

/// How long to wait for an Ack message back from the GNSS module.
#define GNSS_WAIT_FOR_ACK_MILLISECONDS 3000

/// How long to wait for responses from the GNSS module.
#define GNSS_WAIT_FOR_RESPONSE_MILLISECONDS 5000

#if 0
/**
 * Enumeration to describe the type of GNSS system to which a particular GNSS satellite belongs.
 * For GNSS systems providing different signals (frequencies), separate values are provided for each signal.
 * The enumeration values can be used in bitmasks to represent combinations of satellite systems,
 * e.g. in case of multiconstellation GNSS or GNSS + augmentation systems
 */
typedef enum {
    GNSS_SYSTEM_GPS            = 0x00000001,       /**< GPS (L1 signal)*/
    GNSS_SYSTEM_GLONASS        = 0x00000002,       /**< GLONASS (L1 signal) */
    GNSS_SYSTEM_GALILEO        = 0x00000004,       /**< GALILEO (E1 signal) */
    GNSS_SYSTEM_BEIDOU         = 0x00000008,       /**< BeiDou aka COMPASS (B1 signal) */
    GNSS_SYSTEM_GPS_L2         = 0x00000010,       /**< GPS (L2 signal) */
    GNSS_SYSTEM_GPS_L5         = 0x00000020,       /**< GPS (L5 signal) */
    GNSS_SYSTEM_GLONASS_L2     = 0x00000040,       /**< GLONASS (L2 signal) */
    GNSS_SYSTEM_BEIDOU_B2      = 0x00000080,       /**< BeiDou aka COMPASS (B2 signal) */
    /* Numbers >= 0x00010000 are used to identify SBAS (satellite based augmentation system) */
    GNSS_SYSTEM_SBAS_WAAS      = 0x00010000,       /**< WAAS (North America) */
    GNSS_SYSTEM_SBAS_EGNOS     = 0x00020000,       /**< EGNOS (Europe) */
    GNSS_SYSTEM_SBAS_MSAS      = 0x00040000,       /**< MSAS (Japan) */
    GNSS_SYSTEM_SBAS_QZSS_SAIF = 0x00080000,       /**< QZSS-SAIF (Japan) */
    GNSS_SYSTEM_SBAS_SDCM      = 0x00100000,       /**< SDCM (Russia) */
    GNSS_SYSTEM_SBAS_GAGAN     = 0x00200000,       /**< GAGAN (India) */
} EGNSSSystem;
#endif
/** Power mode selection */
enum {
   GNSS_POWER_HIGH = 0x01,    /** Balanced, power on default? */
   GNSS_POWER_LOW  = 0x03,     /** Aggressive 1Hz */
   GNSS_POWER_INTERVAL= 0x02,   /** Interval */
};

typedef struct {
    uint16_t gps_week;
    uint32_t tow;       /* time of week (in seconds) */
}GPSTime_t;

/**
 * GPS time starts from UTC 01/06/1980(MM/DD/YYYY) and includes 2 fields:
 * week and tow. For a given GPS time, week is the passed week number since
 * the GPS start time, and tow(time of week) is the passed seconds since the
 * last Sunday. For example, a given UTC time [10/30/3014
 * 01:10:00](MM/DD/YYYY HH:MM:SS) can be converted into GPS time [1816,
 * 4200](week, seconds).
 *
 * GPS time can be used as a quite accurate time once position is fixed.
 */

//typedef float LocationType_t;
typedef double LocationType_t;
typedef float Altitude_t;
typedef struct  LocationUpdateParams_tag{
    bool           valid;   /* Does this update contain a valid location. */
    LocationType_t lat;
    LocationType_t lon;
    Altitude_t     altitude;
    uint32_t       utcTime;
}LocationUpdateParams_t;



typedef struct {
    uint8_t     powerMode;
    uint8_t     nmea_interval;
    uint8_t     saveConfig;
    uint8_t     reserved1;
    uint32_t    periodSeconds;
    uint32_t    stayOnSeconds;
    //location_update_cb_t   update;
}GNSS_CONFIG_t;

typedef struct {
    uint8_t     navMode;
    /* GSA, navMode,
    1 = No fix,
    2 = 2D fix,
    3 = 3D fix */
    uint8_t     posMode;
    uint8_t     posModeGLO;
    /* GLL,VTG,RMC, GNS posMode :
    N = No fix,
    E = Estimated/Dead reckoning fix,
    A = Autonomous GNSS fix,
    D = Differential GNSS fix,
    F = RTK float,
    R = RTK fixed */
    uint16_t       numGPSSVs; /* num GPS Satellites */
    uint16_t       numGLOSVs; /* num GLONASS Satellites */
    uint16_t       numSV;     /* num of satelites in view */
    GPSTime_t      gpsTime;
    uint32_t       unixTime;
    uint32_t       gnssPowerOnTime;
    LocationUpdateParams_t last;
    LocationUpdateParams_t ing;
    uint16_t       pDOP;      // -       Scaling 0.01 // Position DOP (0.01)
    uint16_t       gDOP;      // -       Scaling 0.01 // Position DOP (0.01)
    uint16_t       tDOP;      // -       Scaling 0.01 // Position DOP (0.01)
    uint16_t       vDOP;      // -       Scaling 0.01 // Position DOP (0.01)
    uint16_t       hDOP;      // -       Scaling 0.01 // Position DOP (0.01)
    uint16_t       sep;       //GNS,GGA  Scaling 0.1  Geoid separation: difference between ellipsoid and mean sea level

}GNSS_STATUS_t;

/*!
 * \brief Initializes the handling of the GPS receiver
 */
//extern void gnssInit( void );

/*!
 * \brief Switch ON the GPS
 */
extern void gnssPowerOn( void );

/*!
 * \brief Switch OFF the GPS
 */
extern void gnssPowerOff( void );


/**
 * Set the operating mode for power. Typically this allows the user to
 * choose between various low-power modes. Entering low-power modes often
 * results in a trade-off between accuracy and power consumption.
 * @param power The new power mode.
 * @return      true if the update was successful.
 */
extern bool gnssSetPowerMode(uint8_t power);


/**
 * @return  true if we've obtained at least one valid location since last
 *     calling start().
 *
 * @Note: This is cleared after reset().
 */
extern bool gnssLocAvailable(void);


/**
 * @return  the last valid location if there is any; else NULL.
 */
extern const LocationUpdateParams_t *gnssGetLastLoc(void);

/**
 * Type declaration for a callback to be invoked from interrupt context upon
 * receiving new location data.
 *
 * @Note: Please note that the handler gets invoked from interrupt context.
 * Users *should not* do any long running or blocking operations in the
 * handler.
 */
typedef void (* location_update_cb_t)(const LocationUpdateParams_t *params);



/**
* Setup the locationUpdate callback.
*
* @Note: Please note that the handler gets invoked from interrupt context.
* Users *should not* do any long running or blocking operations in the
* handler.
*/
extern void gnssSetLoc_cb(location_update_cb_t callback);


/**
  * In low-power operation, the GPS controller may be expected to hibernate
  * for extended periods and location updates may be infrequent. It should
  * then be possible for an application to demand location data when needed.
  *
  * This calls results in a locationCallback if there is a useful location to
  * report.
  */
extern void gnssGetImmediateLoc(void);

/**
  * get location compact data values
  * this fuction is for lora transfer
  */
extern void gnssGetPositionBinary(int32_t *lat, int32_t *lon, int16_t *alt);

extern void gps_task_start(OS_PRIO task_priority);

void gnss_reset_to_coldstart(void);
void gnss_reset_to_warmstart(void);
void gnss_reset_to_hotstart(void);

uint8_t gps_position_get(uint8_t *value);

bool gnssProve(void);

void gps_setLastPosition(LocationUpdateParams_t gps);


#endif  // __GPS_H__
