// make_pixmap.cpp : Defines the entry point for the console application.
//base code : http://paulbourke.net/dataformats/bmp/

#define _CRT_SECURE_NO_DEPRECATE

#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
//#include "rle.h"
//#include <memory.h>
//#include <sys\stat.h>
//#include <time.h>
#define OK		1
#define NOT_OK	0


#define USE_COMPRESSION   0

//
// Define forward referenced function prototypes.
//

int Debug = 1;

/* O - 0 = success, -1 = failure */
int SaveDIBitmap(const char *filename, /* I - File to load */
					BITMAPINFO *info,     /* I - Bitmap information */
					unsigned char    *bits);     /* I - Bitmap data */

/* O - Bitmap data */
unsigned char *LoadDIBitmap(const char *filename, /* I - File to load */
							BITMAPINFO **info);    /* O - Bitmap information */
int ValidDIBitmap(BITMAPINFO *info);

unsigned char *removeStridePaddingChangeVertical(BITMAPINFO * info, unsigned char *oldBits, int *new_bin_size);
int CompressRLE(BITMAPINFO * info, unsigned char *bindata, int bin_size, unsigned char *file_name);
int creatHeaderfile(BITMAPINFO * info, unsigned char *bits, int bin_size, char *filename);
BITMAPINFO		*BitmapInfo; /* Bitmap information */
unsigned char   *BitmapBits; /* Bitmap data */


int main(int argc, char *argv[])
{
#if 0//test
	argv[1] = "C:\\workspace\\Lineable\\beanpole\\mkpixmap\\make_pixmap\\make_pixmap\\serial_icon.bmp";
#else
	if (argc < 2) {
		printf("Usage: %s bmpfile\n", argv[0]);
		exit(1);
	}
#endif
	//
	// Read and validate input file.
	//
	BitmapBits = LoadDIBitmap(argv[1], &BitmapInfo);

	if (BitmapBits == NULL)
	{
		printf("Open error: %s\n", argv[1]);
		exit(1);
	}

	char file_name[128];
	char file_name2[128]; 
	char *lastdot;
	char *ptr = strrchr(argv[1], '\\');
	if (ptr != NULL)
		strcpy(file_name, ptr + 1);
	else
		strcpy(file_name, argv[1]);

	lastdot = strrchr(file_name, '.');
	if (lastdot != NULL) {
		*(lastdot + 1) = 'h';
		*(lastdot + 2) = '\0';
	}

	sprintf(file_name2, "org_%s", file_name);

	if (ValidDIBitmap(BitmapInfo))
	{
		unsigned char *pConverted;
		int clean_bin_size;
		
		pConverted = removeStridePaddingChangeVertical(BitmapInfo, BitmapBits, &clean_bin_size);
		if (pConverted)
		{
#if (USE_COMPRESSION > 0)
			if(CompressRLE(BitmapInfo, pConverted, clean_bin_size, file_name) < 0)
				creatHeaderfile(BitmapInfo, pConverted, clean_bin_size, file_name2);
#else
			creatHeaderfile(BitmapInfo, pConverted, clean_bin_size, file_name);
#endif

			//SaveDIBitmap("test2.bmp", BitmapInfo, BitmapBits);
			free(pConverted);
		}
	}
	else
	{
		printf("Faild to generate files\n");
	}

	if (BitmapInfo)
	{
		free(BitmapInfo);
		free(BitmapBits);
	}
	return (0);
}

#if 0
// faster
static const uint8_t table[] = {
	0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
	0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
	0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
	0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
	0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
	0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
	0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
	0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
	0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
	0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
	0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
	0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
	0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
	0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
	0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
	0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
	0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
	0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
	0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
	0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
	0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
	0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
	0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
	0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
	0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
	0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
	0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
	0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
	0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
	0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
	0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
	0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
};
uint8_t reverse_byte(uint8_t x)
{
	return table[x];
}

uint16_t reverse_word(uint16_t n)
{
	return (table[n & 0xFF] << 8) | table[n >> 8];
}
#else// smaller// for example
//Index 1==0b0001 => 0b1000
//Index 7==0b0111 => 0b1110
//etc
uint8_t reverse_byte(uint8_t n) {
	static uint8_t lookup[16] = {
		0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
		0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };

	// Reverse the top and bottom nibble then swap them.
	return (lookup[n & 0x0F] << 4) | lookup[n >> 4];
}

// Detailed breakdown of the math
//  + lookup reverse of bottom nibble
//  |       + grab bottom nibble
//  |       |        + move bottom result into top nibble
//  |       |        |     + combine the bottom and top results 
//  |       |        |     | + lookup reverse of top nibble
//  |       |        |     | |       + grab top nibble
//  V       V        V     V V       V
// (lookup[n&0b1111] << 4) | lookup[n>>4]
#endif



int ValidDIBitmap(BITMAPINFO *info)
{
	if(info->bmiHeader.biPlanes != 1)
	{
		printf("BMP: Expected Planes = 1 [ignored]\n");
		return 0;
	}

	if (info->bmiHeader.biBitCount != 1) {
		printf("BMP: Expected BitsPerPixel = 1 for this tool\n");
		return 0;
	}

	if (info->bmiHeader.biCompression != 0) {
		printf("BMP: need compressed one\n");
		return 0;
	}

#if 0// need not	
	if (info->bmiHeader.biWidth & 0x07)
	{
		printf("Width should be divide by 8 : %d [wrong]\n", info->bmiHeader.biWidth);
		return 0;
	}
#endif
#if 0
	/* need for vertical 8bit align*/
	if (info->bmiHeader.biHeight & 0x07)
	{
		printf("Height should be divide by 8 : %d [wrong]\n", info->bmiHeader.biHeight);
		return 0;
	}
#endif
	

	return 1;
}

int creatHeaderfile(BITMAPINFO * info, unsigned char *bits, int bin_size, char *filename)
{
	int x, y, i, bytesize, stride;
	FILE     *fp;          /* Open file pointer */
	int width = info->bmiHeader.biWidth;
	int height = info->bmiHeader.biHeight;
	unsigned char *pBytes;
	char *array_name, *lastdot;

	if ((fp = fopen(filename, "wb")) == NULL)
		return (-1);

	if ((array_name = malloc(strlen(filename) + 1)) == NULL)
		return -1;
	printf("create header file\r\n");

	strcpy(array_name, filename);
	lastdot = strrchr(array_name, '.');
	if (lastdot != NULL)
		*lastdot = '\0';
	/////////////////////////////////////////////////////
	//important 
	// Define macro to give 32-bit padded image width.
	stride = (((width)+31) / 32 * 32);
	//////////////////////////////////////////
	bytesize = ((width+7)>>3)*height;
	fprintf(fp, "/*------------------------------------------------------------------------\r\n");
	fprintf(fp, "* Pixel Map : %s\r\n", filename);
	fprintf(fp, "*------------------------------------------------------------------------\r\n");
	fprintf(fp, "*  width: %d   x  height: %d , pixel byte size : %d \r\n", width, height, bytesize);
	fprintf(fp, "*  width byte size : %d  \r\n", (width+7)>>3);
	fprintf(fp, "*-------------------------------------------------------------------------*/\r\n");
	fprintf(fp, "static const uint8_t %s[%d] = { ", array_name, bin_size);

	//padding removed
	for (x = 0, i = 0; x <bin_size; x++) {
		if ((x % 16) == 0) {
			fprintf(fp, "\r\n");
		}
		fprintf(fp, "0x%.2x, ", (*bits++));
	}
	fprintf(fp, "\r\n};\r\n\r\n");

	fprintf(fp, "const GL_Bitmap_T Bmp_%s = {\r\n\t{ %d, %d },\r\n\t%d,\r\n\t%d, /* bpp */\r\n\t%d,/* compressed */\r\n\t{ 0, 0 },\r\n\t(uint8_t *)%s\r\n};",
		array_name, width, height, bytesize, 1, (bytesize != bin_size) ? 1 : 0, array_name);
	
	fprintf(fp, "\r\n/*EOF*/\r\n");
	fclose(fp);
	return 0;
}


unsigned char *removeStridePaddingChangeVertical(BITMAPINFO * info, unsigned char *oldBits, int *new_bin_size)
{
	//## BMP 에서 DIB 사용시 주의사항
	// 비트맵 영상이 저장될 때는 이미지가 거꾸로 저장된다.
	// 즉, 비트맵에서 영상처리를 위해 사용할 배열로 다시 저장할 때는 거꾸로 뒤집어서 저장해 주어야 한다.

	int x, y, j, height, width, stride, bin_size;
	unsigned char *bindata;
	width = info->bmiHeader.biWidth;
	height = info->bmiHeader.biHeight;
	unsigned char *pBytes;
	printf("remove stride and flip vertical\n");
	/////////////////////////////////////////////////////
	//important 
	// Define macro to give 32-bit padded image width.
	stride = (((width)+31) / 32 * 32);
	printf("width = %d, stride = %d\n", width, stride);
	if (width != stride) {
		printf("width and stride is not same\n");
	}

	bin_size = (((width + 7) >> 3) * height);
	if ((bindata = (unsigned char *)malloc(bin_size)) == NULL) {
		printf("malloc error line %d\n", __LINE__);
		return NULL;
	}
	// change vertical order and remove bmp padding and revere byte
	for (y = height - 1, j = 0; y >= 0; y--) {
		pBytes = oldBits + y*(stride >> 3);
		for (x = ((width+7) >> 3) - 1; x >= 0; x--) {
			//bindata[j++] = (*pBytes++)^0xFF;
			bindata[j++] = (*pBytes++);
			//bindata[j++] = reverse_byte(*pBytes++);
		}
	}

	printf("BMP INFO byte size : %d\n", info->bmiHeader.biSize);
	printf("malloc size %d, copied size %d\n", bin_size, j);
	*new_bin_size = j;
	return bindata;
}

#if 0
unsigned char * rleCompression(BITMAPINFO * info, unsigned char *bindata)
{
	int x, y, i, j, outsize, insize, stride;
	int width = info->bmiHeader.biWidth;
	int height = info->bmiHeader.biHeight;
	unsigned char *newBits, *bindata;

	insize = (((width + 7) >> 3) * height);
	/* Worst case buffer size */
	outsize = ((insize * 104) + 50)/100 + 384;

	if ((bindata = (unsigned char *)malloc(insize)) == NULL)
		return NULL;
	if ((newBits = (unsigned char *)malloc(outsize)) == NULL)
		return NULL;

	memset(bindata, 0, insize);
	memset(newBits, 0, outsize);

	///////////////////////////////////////////////////
	/////////////////////////////////////////////////////
	//important 
	// Define macro to give 32-bit padded image width.
	stride = (((width)+31) / 32 * 32);
	//////////////////////////////////////////
	printf("width = %d, stride = %d\n", width, stride);
	// remove stride padding, change vertical order
	for (y = height - 1, j = 0; y >= 0; y--) {
		for (x = (width >> 3) - 1, i = 0; x >= 0; x--) {
			bindata[j++] = oldBits[y*(stride >> 3) + i++];
		}
	}
	
	outsize = RLE_Compress(bindata, newBits, insize);
	free(bindata);
	info->bmiHeader.biCompression = outsize; // just temp save for output
	return newBits;
}
#endif

#if 1
/*
*	RLE compression method:
*
*	high bit of byte (0x80) indicates literal run of 0x7F
*	mask count WORDs (2-byte pairs)
*
*	high bit not set (0x00) indicates a repeat run of 0x7F
*	mask count WORDs of the following WORD (2-byte pair).
*
*	0xFF indicates the start of a new line
*	0xFE indicates the end of the image
*/
int CompressRLE(BITMAPINFO * info, unsigned char *bindata, int bin_size, unsigned char *file_name)
{
	int width = info->bmiHeader.biWidth;
	int height = info->bmiHeader.biHeight;

	unsigned char *byte_ptr = NULL;
	long max_data_size;
	unsigned char *current_count;
	unsigned char *next_line;	// 2 bytes, little endian
	int line_offset;
	long current_data_size;
	int x, y;
	unsigned char *compressed_data;

	unsigned long previous = 0;
	unsigned long pixel = 0;

	max_data_size = bin_size;
	printf("input bin size : %d\r\n", bin_size);
	// Allocate a working area that we discard later
	compressed_data = (unsigned char *)malloc(max_data_size + 16);	/* Added overrun buffer */
	byte_ptr = compressed_data;

	// Ensure that we don't get too close to the end of the data
	max_data_size -= 4 + 2;
	if (max_data_size <= 0) current_data_size = -1;

	// Two pixels packed into one byte
	width = (width + 7) >> 3;

	current_data_size = 0;
	for (y = 0; ((y < height) && (current_data_size >= 0)); y++)
	{
		// Check against low-water mark before continuing
		if (current_data_size >= max_data_size)
		{
			current_data_size = -1;
			break;
		}

		byte_ptr[current_data_size++] = 0xFF;	// SOL: Line start
		next_line = &byte_ptr[current_data_size];
		byte_ptr[current_data_size++] = 0x00;	// Next line offset, low byte
		byte_ptr[current_data_size++] = 0x00;	// Next line offset, high byte
		current_count = &byte_ptr[current_data_size++];
		*current_count = 0x80;	// Start with literals...
		previous = 0;

		for (x = 0; x < width; x++)
		{
			if (current_data_size >= max_data_size)
			{
				current_data_size = -1;
				break;
			}

			int index = x + (y * width);

			
			pixel = bindata[index];
			

			if (*current_count != 0x80)
			{
				// Either next literal or part of a run
				if (*current_count & 0x80)
				{
					*current_count += 1;
					if (previous != pixel)
					{
						// Another literal
						previous = pixel;
						byte_ptr[current_data_size++] = (unsigned char)(pixel & 0xFF);

						// Check vs. literal limit
						if (*current_count == 0xFD)
						{
							current_count = &byte_ptr[current_data_size++];
							*current_count = 0x80;	// Start with literals...
						}
					}
					else
					{
						// Start a run
						if (*current_count == 0x82)
						{
							// Reverse literal
							*current_count = 0x02;
						}
						else
						{
							// End literal
							*current_count -= 2;

							current_data_size -= 1;

							current_count = &byte_ptr[current_data_size++];
							*current_count = 0x02;
							byte_ptr[current_data_size++] = (unsigned char)(pixel & 0xFF);
						}
					}
				}
				else
				{
					// We're in a run already
					if (previous != pixel)
					{
						// Stop the run
						current_count = &byte_ptr[current_data_size++];
						*current_count = 0x81;	// Start with literals...
						previous = pixel;
						byte_ptr[current_data_size++] = (unsigned char)(pixel & 0xFF);
					}
					else
					{
						// Just add to the run
						*current_count += 1;
						if (*current_count == 0x7F)
						{
							// End this...
							current_count = &byte_ptr[current_data_size++];
							*current_count = 0x80;	// Start with literals...
						}
					}
				}
			}
			else
			{
				// First literal...
				*current_count += 1;
				previous = pixel;
				byte_ptr[current_data_size++] = (unsigned char)(pixel & 0xFF);
			}
		}

		// Point the line that's just completed at the next
		line_offset = &byte_ptr[current_data_size] - next_line + 1;
		*next_line = (unsigned char)(line_offset & 0xFF);
		*(next_line + 1) = (unsigned char)((line_offset >> 8) & 0xFF);
	}
	if (current_data_size >= 0) byte_ptr[current_data_size++] = 0xFE;	// EOI

	// If we got a smaller compressed image, set the flag
	if (current_data_size > 0)
	{
		if (current_data_size >= max_data_size)
		{
			printf("compression size is bigger than original file %d >= %d \n", current_data_size, max_data_size);
			current_data_size = -1;
		}
		else
		{
			printf("compression done and create header file %d\n", current_data_size);
			//copy to dest
			creatHeaderfile(info, compressed_data, current_data_size, file_name);
		}
	}

	free(compressed_data);

	return current_data_size;
}
#endif


/*
* 'LoadDIBitmap()' - Load a DIB/BMP file from disk.
*
* Returns a pointer to the bitmap if successful, NULL otherwise...
*/

unsigned char *                          /* O - Bitmap data */
LoadDIBitmap(const char *filename, /* I - File to load */
	BITMAPINFO **info)    /* O - Bitmap information */
{
	FILE             *fp;          /* Open file pointer */
	unsigned char    *bits;        /* Bitmap pixel bits */
	unsigned int     bitsize;      /* Size of bitmap */
	unsigned int     infosize;     /* Size of header information */
	BITMAPFILEHEADER header;       /* File header */

	/* Try opening the file; use "rb" mode to read this *binary* file. */
	if ((fp = fopen(filename, "rb")) == NULL)
		return (NULL);

	/* Read the file header and any following bitmap information... */
	if (fread(&header, sizeof(BITMAPFILEHEADER), 1, fp) < 1)
	{
		/* Couldn't read the file header - return NULL... */
		fclose(fp);
		return (NULL);
	}

	
	if (header.bfType != 'MB')	/* Check for BM reversed... */
	{
		/* Not a bitmap file - return NULL... */
		printf("Not Bitmap \n");
		fclose(fp);
		return (NULL);
	}

	if (Debug) {
		unsigned char *ucBuff = (unsigned char *)&header;
		printf("Bitmap File Header \n");
		printf("bfType		: [%c%c]\n", *ucBuff, *(ucBuff + 1));
		printf("bfSize   	: [%u] Bytes\n", header.bfSize);
		printf("bfReserved1	: [0x%x]\n", header.bfReserved1);
		printf("bfReserved2	: [0x%x]\n", header.bfReserved2);
		printf("bfOffBits	: [%u] Bytes\n", header.bfOffBits);
	}

	infosize = header.bfOffBits - sizeof(BITMAPFILEHEADER);
	if ((*info = (BITMAPINFO *)malloc(infosize)) == NULL)
	{
		/* Couldn't allocate memory for bitmap info - return NULL... */
		printf("malloc error\n");
		fclose(fp);
		return (NULL);
	}

	if (fread(*info, 1, infosize, fp) < infosize)
	{
		/* Couldn't read the bitmap header - return NULL... */
		printf("fread error\n");
		free(*info);
		fclose(fp);
		return (NULL);
	}

	if ((*info)->bmiColors) {
		printf("BMI COLOR exist\n");

	}

	/* Now that we have all the header info read in, allocate memory for *
	* the bitmap and read *it* in...                                    */
	if ((bitsize = (*info)->bmiHeader.biSizeImage) == 0)
		bitsize = ((*info)->bmiHeader.biWidth * (*info)->bmiHeader.biBitCount + 7) / 8 * abs((*info)->bmiHeader.biHeight);

	if ((bits = malloc(bitsize)) == NULL)
	{
		printf("malloc error2\n");
		/* Couldn't allocate memory - return NULL! */
		free(*info);
		fclose(fp);
		return (NULL);
	}

	if (fread(bits, 1, bitsize, fp) < bitsize)
	{
		printf("fread2 error\n");
		/* Couldn't read bitmap - free memory and return NULL! */
		free(*info);
		free(bits);
		fclose(fp);
		return (NULL);
	}

	if (Debug) {
		printf("Bitmap Info Header \n");
		printf("header size     : [%u] Bytes\n", (*info)->bmiHeader.biSize);
		printf("biWidth         : [%u] pixel\n", (*info)->bmiHeader.biWidth);
		printf("biHeight        : [%u] pixel\n", (*info)->bmiHeader.biHeight);
		printf("biPlanes        : [%d] \n", (*info)->bmiHeader.biPlanes);
		printf("biBitCount      : [%d] \n", (*info)->bmiHeader.biBitCount);
		printf("biCompression   : [%u] \n", (*info)->bmiHeader.biCompression);
		printf("biSizeImage     : [%u] Bytes\n", (*info)->bmiHeader.biSizeImage);
		printf("biXPelsPerMeter : [%lu] meter\n", (*info)->bmiHeader.biXPelsPerMeter);
		printf("biYPelsPerMeter : [%lu] meter\n", (*info)->bmiHeader.biYPelsPerMeter);
		printf("biClrUsed       : [%u] \n", (*info)->bmiHeader.biClrUsed);
		printf("biClrImportant : [%u] \n", (*info)->bmiHeader.biClrImportant);
	}




	/* OK, everything went fine - return the allocated bitmap... */
	fclose(fp);
	return (bits);
}


/*
* 'SaveDIBitmap()' - Save a DIB/BMP file to disk.
*
* Returns 0 on success or -1 on failure...
*/

int                                /* O - 0 = success, -1 = failure */
SaveDIBitmap(const char *filename, /* I - File to load */
	BITMAPINFO *info,     /* I - Bitmap information */
	unsigned char    *bits)     /* I - Bitmap data */
{
	FILE             *fp;          /* Open file pointer */
	unsigned int              size,         /* Size of file */
		infosize,     /* Size of bitmap info */
		bitsize;      /* Size of bitmap pixels */
	BITMAPFILEHEADER header;       /* File header */


								   /* Try opening the file; use "wb" mode to write this *binary* file. */
	if ((fp = fopen(filename, "wb")) == NULL)
		return (-1);

	/* Figure out the bitmap size */
	if (info->bmiHeader.biSizeImage == 0)
		bitsize = (info->bmiHeader.biWidth *
			info->bmiHeader.biBitCount + 7) / 8 *
		abs(info->bmiHeader.biHeight);
	else
		bitsize = info->bmiHeader.biSizeImage;

	/* Figure out the header size */
	infosize = sizeof(BITMAPINFOHEADER);
	switch (info->bmiHeader.biCompression)
	{
	case BI_BITFIELDS:
		infosize += 12; /* Add 3 RGB doubleword masks */
		if (info->bmiHeader.biClrUsed == 0)
			break;
	case BI_RGB:
		if (info->bmiHeader.biBitCount > 8 &&
			info->bmiHeader.biClrUsed == 0)
			break;
	case BI_RLE8:
	case BI_RLE4:
		if (info->bmiHeader.biClrUsed == 0)
			infosize += (1 << info->bmiHeader.biBitCount) * 4;
		else
			infosize += info->bmiHeader.biClrUsed * 4;
		break;
	}

	size = sizeof(BITMAPFILEHEADER) + infosize + bitsize;

	/* Write the file header, bitmap information, and bitmap pixel data... */
	header.bfType = 'MB'; /* Non-portable... sigh */
	header.bfSize = size;
	header.bfReserved1 = 0;
	header.bfReserved2 = 0;
	header.bfOffBits = sizeof(BITMAPFILEHEADER) + infosize;

	if (fwrite(&header, 1, sizeof(BITMAPFILEHEADER), fp) < sizeof(BITMAPFILEHEADER))
	{
		/* Couldn't write the file header - return... */
		fclose(fp);
		return (-1);
	}

	if (fwrite(info, 1, infosize, fp) < infosize)
	{
		/* Couldn't write the bitmap header - return... */
		fclose(fp);
		return (-1);
	}

	if (fwrite(bits, 1, bitsize, fp) < bitsize)
	{
		/* Couldn't write the bitmap - return... */
		fclose(fp);
		return (-1);
	}

	/* OK, everything went fine - return... */
	fclose(fp);
	return (0);
}