/**************************************************************************//**
 * @brief Implementation specific functions for HRM code
 * @version x.x.x
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2017 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement that is included in
 * with the software release. Before using this software for any purpose, you
 * must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include "rtos_app.h"
#include <stdio.h>
#include <string.h>

#include  <drivers/peripheral/i2c/micrium_i2c.h>
#include "em_gpio.h"

#include "accelerometer.h"
#include "accel_resample.h"
#include "sihrmUser.h"
#include "si117xhrm.h"
#include "si117xSkinDetection.h"


/*****************************************************************************
* Debug Option
*****************************************************************************/
#undef  __MODULE__
#define __MODULE__ "HRM"
#undef  DBG_MODULE
#define DBG_MODULE 0


/*****************************************************************************
* Defines
*****************************************************************************/
#define SI117X_I2C_ADDRESS      0x70



typedef struct Si117xPortConfig {
    I2CM_ID         	i2cId; //I2C_TypeDef       *i2cPort;   /**< I2C port Si117x is connected to */
    uint8_t           	i2cAddress; /**< I2C address of Si117x */
    GPIO_Port_TypeDef 	irqPort;    /**< Port for Si117x INT pin */
    uint8_t           	irqPin;     /**< Pin for Si117x INT pin */
} Si117xPortConfig_t;

static Si117xPortConfig_t handle_data;

void Si117xEnableInterrupt(HANDLE *si117x_handle);
void Si117xDisableInterrupt(HANDLE *si117x_handle);
void Si117xInitInterrupt(HANDLE *si117x_handle,  GPIOINT_IrqCallbackPtr_t callbackPtr);
void delay_10ms();

/*  interrupt sequence counter  */
static uint16_t irqSequence = 0;

/*  Interrupt queue
 *
 *  When determining the size of this queue the max interrupt latency
 *  should be considered.  The queue should be sized such that an overflow
 *  does not occur during the max interrupt latency.
 *
 *  Generally 3x the FIFO interrupt level is safe.  For example, if the
 *  FIFO level is set to approximately 15 samples then the queue should 45.
 *
 */
#define IRQ_QUEUE_DEPTH   45     //  interrupt queue size in bytes
static SI117XDRV_DataQueueID_t sihrmQueueID = SI117XDRV_UNINITIALIZED_QUEUE_ID;
SI117XDRV_PPGSample_t sihrmIrqQueue[IRQ_QUEUE_DEPTH];

/*  Store the revision ID of the si117x device for use by the IRQ function  */
static uint8_t si117xRevId;

uint8_t raw_data_dumping = 1;   // 0=Do not dump raw data samples, 1= dump raw data samples

uint16_t PPG1 = 0;
uint16_t PPG2 = 0;

/**************************************************************************//**
 * @brief Enables/Disables USB debug output.
 *****************************************************************************/
int32_t sihrmUser_SetupDebug(sihrmUserHandle_t handle, int32_t enable, void *debug)
{
  //do nothing here...everything is done in uart_debug.c
  return SI117xHRM_SUCCESS;
}

/**************************************************************************//**
 * @brief Prints USB debug output.
 *****************************************************************************/
int32_t sihrmUser_OutputDebugMessage(sihrmUserHandle_t handle, const uint8_t *message)
{
  return SI117xHRM_SUCCESS;
}


/**************************************************************************//**
 * @brief Outputs sample to USB debug interface
 *****************************************************************************/
int32_t sihrmUser_OutputRawSampleDebugMessage(sihrmUserHandle_t handle, Si117xhrmIrqSample_t *sample, Si117xhrmSampleAuxDebug_t *aux)
{

  PPG1 = sample->ppg1;
  PPG2 = sample->ppg2;

  return SI117xHRM_SUCCESS;
}



/**************************************************************************//**
 * @brief
 *****************************************************************************/
int32_t sihrmUser_PostSkinSensingCallback(sihrmUserHandle_t handle)
{ int32_t error = SI117xHRM_SUCCESS;

  SI117XDRV_FifoFlush(handle->deviceID);    // clear FIFO of any residual data

  // Initialize for Fs ratio locking
  AccelResample_Reset();
  Accelerometer_Enable();
  return error;
}

/**************************************************************************//**
 * @brief Independent interrupt processing routine for Accelerometer
 *****************************************************************************/
static uint8_t AccfifoData[350];   //2048 is the max size of the FIFO...this can be reduced to save RAM
int32_t sihrmUser_ProcessIrqAccel(sihrmUserHandle_t handle, uint16_t timestamp, Si117xhrmUserConfiguration_t *configuration)
{
  int32_t error = 0;

#if (ACCEL_DEVICE == LSM6DS3)
    //int16_t accel_int;
    error = (int32_t)Accelerometer_ReadFifo(ACCEL_FIFO_LEVEL * 6, AccfifoData);
    if (error != 0) {
        /** Read XYZ samples frame from accelerometer FIFO */
        AccelResample_ProcessAccFifoDataRecord(AccfifoData, ACCEL_FIFO_LEVEL * 6, timestamp, sihrmQueueID, configuration);
    } else {
        APP_TRACE_DEBUG("ACCEL:: Read fifo data NG!!\n");
    }
#elif (ACCEL_DEVICE == BMA255)
  int16_t accel_int;

  // Must check for any accelerometer interrupt.
  Accelerometer_GetIrqStatus(&accel_int);
  if ((accel_int & ACCELEROMETER_FIFO_WM_INT) != 0)
  {
    // Read XYZ samples frame from accelerometer FIFO
    Accelerometer_ReadFifo(ACCEL_FIFO_LEVEL*6, AccfifoData);
    Accelerometer_ClearIRQ();
    AccelResample_ProcessAccFifoDataRecord(AccfifoData, ACCEL_FIFO_LEVEL*6, timestamp, sihrmQueueID,configuration);
  }
#elif(ACCEL_DEVICE == DUMMY_ACCEL )
    int16_t accel_int;
    Accelerometer_GetIrqStatus(&accel_int);
    if (accel_int > 0) {
        Accelerometer_ReadFifo(ACCEL_FIFO_LEVEL * 6, AccfifoData);
        Accelerometer_ClearIRQ();
        AccelResample_ProcessAccFifoDataRecord(AccfifoData, ACCEL_FIFO_LEVEL * 6, timestamp, sihrmQueueID, configuration);
    }
#endif

  return error;
}

/**************************************************************************//**
 * @brief Main interrupt processing routine for Si117x.
 *****************************************************************************/
#define FIFO_BLOCK_SIZE 254

int32_t sihrmUser_ProcessIrq(sihrmUserHandle_t handle, uint16_t timestamp, SI117XDRV_GlobalCfg_t *globalCfg)
{
  int32_t error = 0;
  uint8_t DC_bytesToRead = 16;
  uint8_t fifoData[FIFO_BLOCK_SIZE];   //2048 is the max size of the FIFO...this can be reduced to save RAM
  int32_t irqstat;
  uint8_t numBytesToRead;

  // FIFO mode interrupt handling
  uint16_t numBytesInSi117xFifo;

  /* check for Si117x FIFO IRQ (cannot assume) */
  irqstat = Si117xGetIrqStatus(handle->si117xHandle, si117xRevId);

  if (irqstat & PPG1_IRQ)      //used for legacy mode and fifo mode during DC sensing
  {
    // Read the FIFO right here in to elastic buffer
    Si117xBlockRead(handle->si117xHandle, REG_FIFO_READ, DC_bytesToRead, fifoData);
    irqSequence++;
    SI117XDRV_ProcessPPGInterrupt(handle->deviceID,fifoData,timestamp,irqSequence);
  }
  else if (irqstat & FIFO_IRQ)
  {
#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SOFTWARE)
    AccelResample_Si117xInterruptNotification(timestamp);    //Save the PPG Interrupt timestamp
#endif

    numBytesInSi117xFifo = globalCfg->fifo_int_level;
    irqSequence++;

    while (numBytesInSi117xFifo > 0 )	// repeated block read may be needed since block read is limited to 255
    {
      if (numBytesInSi117xFifo > FIFO_BLOCK_SIZE)
      { numBytesToRead = FIFO_BLOCK_SIZE;
      }
      else
      { numBytesToRead = numBytesInSi117xFifo;
      }
      Si117xBlockRead(handle->si117xHandle, REG_FIFO_READ, numBytesToRead, fifoData);
      SI117XDRV_ProcessFifoData(handle->deviceID, fifoData, numBytesToRead, timestamp, irqSequence,0);
      numBytesInSi117xFifo = numBytesInSi117xFifo - numBytesToRead;
    }
  }
  else if (irqstat & WD_IRQ)
  {
  	  si117xhrm_PPGSkinDetectInterruptHandler(handle->deviceID);
  }

  return error;
}

/**************************************************************************//**
 * @brief Query number of entries in the interrupt queue.
 *****************************************************************************/
int32_t sihrmUser_SampleQueueNumentries(sihrmUserHandle_t handle)
{
  (void) handle;
  int16_t count=0;
//  Si117xDisableInterrupt();
  count = SI117XDRV_NumSamplesInQueue (sihrmQueueID);
//  Si117xEnableInterrupt();
  return count;
}

/**************************************************************************//**
 * @brief Get sample from the interrupt queue.
 *****************************************************************************/
int32_t sihrmUser_SampleQueue_Get(sihrmUserHandle_t handle, uint8_t getAccelSamples, Si117xhrmIrqSample_t *samples)
{
  (void) handle;
  int16_t error = SI117xHRM_SUCCESS;
  SI117XDRV_PPGSample_t s;

  if ((sihrmUser_SampleQueueNumentries(handle)>0) && ((getAccelSamples == 0) || (AccelResample_HasData()>0)))
  {// Si117xDisableInterrupt();
    SI117XDRV_DequeuePPGSampleData(sihrmQueueID,&s);
   // Si117xEnableInterrupt();

    //copy to sihrmIrqSample struct
    samples->sequence = s.sequence;
    samples->timestamp = s.timestamp;
    samples->syncMessage = s.syncMessage;
    samples->ppg1 = s.ppg1;
    samples->ppg2 = s.ppg2;
    samples->ppg3 = s.ppg3;
    samples->ppg4 = s.ppg4;
    samples->accelerometer_x = 0;
    samples->accelerometer_y = 0;
    samples->accelerometer_z = 0;

    if (getAccelSamples==1)  // FIFO mode and not DC sensing, get resampled G data (y and Z are zeros)
    {
        samples->accelerometer_x = AccelResample_GetSample();
        samples->accelerometer_y = 0;
        samples->accelerometer_z = 0;
    }
  }
  else
  {
    error = SI117xHRM_ERROR_SAMPLE_QUEUE_EMPTY; //accelerometer sample queue empty
    goto Error;
  }


Error:
  return error;
}


/**************************************************************************//**
 * @brief Empty the interrupt queue.
 *****************************************************************************/
int32_t sihrmUser_SampleQueue_Clear(sihrmUserHandle_t handle)
{
  (void) handle;
  SI117XDRV_ClearQueue(sihrmQueueID);
  irqSequence = 0;
  return 0;
}

/**************************************************************************//**
 * @brief Initialize low level handle and clear irq queue.
 *****************************************************************************/
int32_t sihrmUser_Initialize(void *port, int16_t options, sihrmUserHandle_t handle)
{
  (void) options;

  si117xRevId = (uint8_t)Si117xReadFromRegister (handle->si117xHandle, REG_REV_ID);

  if (sihrmQueueID == SI117XDRV_UNINITIALIZED_QUEUE_ID)
  {
    SI117XDRV_AllocatePPGDataQueue(&sihrmQueueID, sihrmIrqQueue, IRQ_QUEUE_DEPTH*SI117XDRV_PPG_SAMPLE_SIZE_BYTES);
    SI117XDRV_RegisterAccelSyncCallback(handle->deviceID, AccelResample_SynchMessageReceived, NULL);
  }
  sihrmUser_SampleQueue_Clear(handle);

  return SI117xHRM_SUCCESS;
}

/**************************************************************************//**
 * @brief Close Si117x.
 *****************************************************************************/
int32_t sihrmUser_Close(sihrmUserHandle_t handle)
{
  (void) handle;

  return SI117xHRM_SUCCESS;
}

/**************************************************************************//**
 * @brief Write to Si117x register
 *****************************************************************************/
int32_t Si117xWriteToRegister(HANDLE si117x_handle, uint8_t address, uint8_t data)
{
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;
    /* Select register to start reading from */
    address = address | 0x40;
    return (int32_t)i2cm_writeData(handle->i2cId, handle->i2cAddress, address, &data, 1);
}

/**************************************************************************//**
 * @brief Read from Si117x register.
 *****************************************************************************/
int32_t Si117xReadFromRegister(HANDLE si117x_handle, uint8_t address)
{
    uint8_t data;
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;
    /* Select register to start reading from */
    address = address | 0x40;
    if (i2cTransferDone != i2cm_readData(handle->i2cId, handle->i2cAddress, address, &data, 1)) {
        data = 0xff;
    }
    return data;
}

/**************************************************************************//**
 * @brief block write to si117x
 * Block writes should never be used.
 *****************************************************************************/
int32_t Si117xBlockWrite(HANDLE si117x_handle,
                         uint8_t address, uint8_t length, uint8_t const *values)
{
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;
    //address = address | 0x40;
    return (int32_t)i2cm_writeData(handle->i2cId, handle->i2cAddress, address, (uint8_t *)values, length);
}

/**************************************************************************//**
 * @brief Block read from Si117x.
 *****************************************************************************/
int32_t Si117xBlockRead(HANDLE si117x_handle,
                        uint8_t address, uint8_t length, uint8_t *values)
{
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;
    /* Select register to start reading from */
    address = address | 0x40;
    return (int32_t)i2cm_readData(handle->i2cId, handle->i2cAddress, address, values, length);
}

/**************************************************************************//**
 * @brief Disable GPIO interrupt for Si117x interrupt.
 *****************************************************************************/
void Si117xDisableInterrupt(HANDLE *si117x_handle)
{
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;
    GPIO_IntDisable(1 << handle->irqPin);
}

/**************************************************************************//**
 * @brief Enable GPIO interrupt for Si117x.
 *****************************************************************************/
void Si117xEnableInterrupt(HANDLE *si117x_handle)
{
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;
    if (GPIO_PinInGet(handle->irqPort, handle->irqPin) == 0) {
        GPIO_IntSet(1 << handle->irqPin);
    }
    GPIO_IntEnable(1 << handle->irqPin);
}

/**************************************************************************//**
 * @brief Set callback of GPIO interrupt for Si117x.
 *****************************************************************************/
void Si117xInitInterrupt(HANDLE *si117x_handle,  GPIOINT_IrqCallbackPtr_t callbackPtr)
{
    Si117xPortConfig_t *handle = (Si117xPortConfig_t *)si117x_handle;

    Si117xDisableInterrupt(si117x_handle);
    GPIO_IntConfig(handle->irqPort, handle->irqPin, false, true, true);
    GPIOINT_CallbackRegister(handle->irqPin, callbackPtr);
    Si117xEnableInterrupt(si117x_handle);
}



/**************************************************************************//**
 * @brief Close Si117x.
 *****************************************************************************/
int16_t Si117xClose(HANDLE si117x_handle)
{
    Si117xPortConfig_t *handle;
    handle = (Si117xPortConfig_t *)si117x_handle;
    handle->i2cAddress = 0xff;
    return 0;
}


/**************************************************************************//**
 * @brief Initialize low level handle and clear irq queue.
 *****************************************************************************/
int16_t Si117xInit(void *port, int options, HANDLE *si117x_handle)
{
    int16_t error = 0;
    uint8_t data;
    Si117xPortConfig_t  *port_cfg;

    *si117x_handle = &handle_data;//// assign memory
    port_cfg = *si117x_handle;

    port_cfg->irqPort = BSP_HRM_INT_PORT;
    port_cfg->irqPin  = BSP_HRM_INT_PIN;
    port_cfg->i2cId = i2c1_id;
    port_cfg->i2cAddress = SI117X_I2C_ADDRESS;

    data = Si117xReadFromRegister(port_cfg, REG_PART_ID);

    if (!(((data >= 0x71) && (data <= 0x75)) ||
          ((data >= 0x81) && (data <= 0x85)) ||
          ((data >= 0x91) && (data <= 0x95)))) {
        error = -1;
    }

    APP_TRACE_DEBUG("Silicon Labs HRM PART ID = 0x%x\n", data);
    if (error != 0) {
        return SI117xHRM_ERROR_INVALID_PART_ID;
    }

    return SI117xHRM_SUCCESS;
}

int16_t Si117xCheckSensor(void)
{
    int16_t error = 0;
    uint8_t data;
    Si117xPortConfig_t port_cfg;

    port_cfg.irqPort = BSP_HRM_INT_PORT;
    port_cfg.irqPin  = BSP_HRM_INT_PIN;
    port_cfg.i2cId = i2c1_id;
    port_cfg.i2cAddress = SI117X_I2C_ADDRESS;

    data = Si117xReadFromRegister((HANDLE)&port_cfg, REG_PART_ID);

    if (!(((data >= 0x71) && (data <= 0x75)) ||
          ((data >= 0x81) && (data <= 0x85)) ||
          ((data >= 0x91) && (data <= 0x95)))) {
        error = -1;
    }

    APP_TRACE_DEBUG("Silicon Labs HRM PART ID = 0x%x\n", data);
    if (error != 0) {
        return SI117xHRM_ERROR_INVALID_PART_ID;
    }

    return SI117xHRM_SUCCESS;
}

/**************************************************************************//**
 * @brief 10ms delay required by Si117x reset sequence.
 *****************************************************************************/
void delay_10ms(void)
{
    RTOS_ERR    err;
    OSTimeDly(OS_MS_2_TICKS(10),
              OS_OPT_TIME_DLY,
              &err);
}

