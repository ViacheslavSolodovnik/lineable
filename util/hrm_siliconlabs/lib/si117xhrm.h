/** @file si117xhrm.h
 *  @brief Main header file for si117xhrm library.
 *******************************************************************************
 * @section License
 * <b>(C) Copyright 2018 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *  This software is confidential and proprietary information owned by Silicon
 *  Labs which is subject to worldwide copyright protection.  No use or other
 *  rights associated with this software may exist other than as expressly
 *  stated by written agreement with Silicon Labs.  No licenses to the
 *  intellectual property rights included in the software, including patent and
 *  trade secret rights, are otherwise given by implication, estoppel or otherwise.
 *  This contains the prototypes and datatypes needed
 *  to use the si114xhrm library.
 */


#ifndef SI117xHRM_H__

#define SI117xHRM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "si117xdrv.h"
#include "si117x_functions.h"

/*****************************************************************************
 * Error Codes
******************************************************************************/
#define SI117xHRM_SUCCESS                            0
#define SI117xHRM_ERROR_RESERVED                    -1      //this error is reserved for internal purposes
#define SI117xHRM_ERROR_INVALID_MEASUREMENT_RATE    -2
#define SI117xHRM_ERROR_SAMPLE_QUEUE_EMPTY          -3
#define SI117xHRM_ERROR_INVALID_PART_ID             -4
#define SI117xHRM_ERROR_FUNCTION_NOT_SUPPORTED      -5
#define SI117xHRM_ERROR_BAD_POINTER                 -6
#define SI117xHRM_ERROR_DEBUG_DISABLED              -7
#define SI117xHRM_ERROR_NON_SUPPORTED_PART_ID       -8
#define SI117xHRM_ERROR_INVALID_SAMPLE_DATA         -9
#define SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE         -10
#define SI117xHRM_ERROR_PARAM2_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 1)
#define SI117xHRM_ERROR_PARAM3_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 2)
#define SI117xHRM_ERROR_PARAM4_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 3)
#define SI117xHRM_ERROR_PARAM5_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 4)
#define SI117xHRM_ERROR_PARAM6_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 5)
#define SI117xHRM_ERROR_PARAM7_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 6)
#define SI117xHRM_ERROR_PARAM8_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 7)
#define SI117xHRM_ERROR_PARAM9_OUT_OF_RANGE         (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 8)
#define SI117xHRM_ERROR_PARAM10_OUT_OF_RANGE        (SI117xHRM_ERROR_PARAM1_OUT_OF_RANGE - 9)
#define SI117xHRM_ERROR_RESERVED99                  -99    //this error is reserved for internal purposes

/*****************************************************************************
 * hrmStatus Flags
 *
 * The HRM status values are a bit field. More than one status can be 'on' at
 * any given time.  In many cases it is the responsibility of the application
 * to clear a statuf flag
******************************************************************************/
#define SI117xHRM_STATUS_SKIN_CONTACT_OFF             (1<<0)
#define SI117xHRM_STATUS_HR_UPDATED                   (1<<1)
#define SI117xHRM_STATUS_SATURATION_DETECTED          (1<<2)
#define SI117xHRM_STATUS_RR_INTERVAL_ACTIVE           (1<<3)
#define SI117xHRM_STATUS_RESERVED04                   (1<<4)
#define SI117xHRM_STATUS_RESERVED05                   (1<<5)
#define SI117xHRM_STATUS_RESERVED06                   (1<<6)
#define SI117xHRM_STATUS_PS_AGC_DONE                  (1<<7)
#define SI117xHRM_STATUS_RESERVED08                   (1<<8)
#define SI117xHRM_STATUS_RESERVED09                   (1<<9)
#define SI117xHRM_STATUS_RESERVED10                   (1<<10)
#define SI117xHRM_STATUS_INANIMATE_SUBJECT_SUSPECTED  (1<<11)
#define SI117xHRM_STATUS_RESERVED12                   (1<<12)
#define SI117xHRM_STATUS_RESERVED13                   (1<<13)
#define SI117xHRM_STATUS_RESERVED14                   (1<<14)
#define SI117xHRM_STATUS_PS_DC_SENSE                  (1<<15)
#define SI117xHRM_STATUS_HIGHEST_BIT                  15

#define SI117xHRM_STATUS_PENDING                      (1<<18)


/*****************************************************************************
 * Device Configuration Macros
 *
 * The following macros can be used to initialize the deviceConfiguration to
 * default values.  Default macros exist for a few Silicon Labs sensors and/or
 * evaluation boards.
******************************************************************************/
#define SI117xHRM_DEVICE_CONFIG_DEFAULT_SI1175G2                      \
{ 0,           /* ppg4_threshold is not used */                       \
  {{           /* PPG1 */                                             \
    0x18,      /* ppg1_led1_config; disabled */                       \
    0x58,      /* ppg1_led2_config; enable LED2=Green(Near LED) */    \
    0x18,      /* ppg1_led3_config; disabled */                       \
    0x18,      /* ppg1_led4_config; disabled */                       \
    0xF,       /* ppg1_mode; All quadrants enabled */                 \
    0x3,       /* ppg1_measconfig; */                                 \
    0x0,       /* ppg1_adcconfig; Set for low power configuration */  \
  },                                                                  \
  {            /* PPG2 */                                             \
    0x18,      /* ppg2_led1_config; disabled */                       \
    0x18,      /* ppg2_led2_config; disabled */                       \
    0x58,      /* ppg2_led3_config; enable LED3=Green (Far LED) */    \
    0x18,      /* ppg2_led4_config; disabled */                       \
    0xF,       /* ppg2_mode; All quadrants enabled */                 \
    0x3,       /* ppg2_measconfig; */                                 \
    0x70,      /* ppg2_adcconfig; Set for low noise configuration */  \
  },                                                                  \
  {            /* PPG3 - Note Used */                                 \
    0x18,      /* ppg3_led1_config; disabled */                       \
    0x58,      /* ppg3_led2_config; disabled */                       \
    0x18,      /* ppg3_led3_config; disabled */                       \
    0x18,      /* ppg3_led4_config; disabled */                       \
    0xF,       /* ppg3_mode; All quadrants enabled */                 \
    0x3,       /* ppg3_measconfig; */                                 \
    0x0,       /* ppg3_adcconfig; */                                  \
  },                                                                  \
  {            /* PPG4 - Note Used */                                 \
    0x18,      /* ppg4_led1_config; disabled */                       \
    0x18,      /* ppg4_led2_config; disabled */                       \
    0x58,      /* ppg4_led3_config; disabled */                       \
    0x18,      /* ppg4_led4_config; disabled */                       \
    0xF,       /* ppg4_mode; All quadrants enabled */                 \
    0x3,       /* ppg4_measconfig; */                                 \
    0x0,       /* ppg4_adcconfig; */                                  \
  }},                                                                 \
}


#define SI117xHRM_DEVICE_CONFIG_DEFAULT_SI118X_EVB                    \
{ 0,           /* ppg4_threshold is not used */                       \
  {{           /* PPG1 */                                             \
    0x18,      /* ppg1_led1_config; disabled */                       \
    0x18,      /* ppg1_led2_config; disabled */                       \
    0x58,      /* ppg1_led3_config; enable LED3=Green(Near LED) */    \
    0x18,      /* ppg1_led4_config; disabled */                       \
    0xF,       /* ppg1_mode; All quadrants enabled */                 \
    0x3,       /* ppg1_measconfig; */                                 \
    0x0,       /* ppg1_adcconfig; Set for low power configuration */  \
  },                                                                  \
  {            /* PPG2 */                                             \
    0x18,      /* ppg2_led1_config; disabled */                       \
    0x58,      /* ppg2_led2_config; enable LED2=Green (Far LED) */    \
    0x18,      /* ppg2_led3_config; disabled */                       \
    0x18,      /* ppg2_led4_config; disabled */                       \
    0xF,       /* ppg2_mode; All quadrants enabled */                 \
    0x3,       /* ppg2_measconfig; */                                 \
    0x70,      /* ppg2_adcconfig; Set for low noise configuration */  \
  },                                                                  \
  {            /* PPG3 - Not Used */                                  \
    0x18,      /* ppg3_led1_config; disabled */                       \
    0x18,      /* ppg3_led2_config; disabled */                       \
    0x18,      /* ppg3_led3_config; disabled */                       \
    0x18,      /* ppg3_led4_config; disabled */                       \
    0xF,       /* ppg3_mode; All quadrants enabled */                 \
    0x3,       /* ppg3_measconfig; */                                 \
    0x0,       /* ppg3_adcconfig; */                                  \
  },                                                                  \
  {            /* PPG4 - Not Used */                                  \
    0x18,      /* ppg4_led1_config; disabled */                       \
    0x18,      /* ppg4_led2_config; disabled */                       \
    0x18,      /* ppg4_led3_config; disabled */                       \
    0x18,      /* ppg4_led4_config; disabled */                       \
    0xF,       /* ppg4_mode; All quadrants enabled */                 \
    0x3,       /* ppg4_measconfig; */                                 \
    0x0,       /* ppg4_adcconfig; */                                  \
  }},                                                                 \
}


#define SI117xHRM_DEVICE_CONFIG_DEFAULT_EXT_PD                    \
{ 0,           /* ppg4_threshold is not used */                       \
  {{           /* PPG1 */                                             \
    0x58,      /* ppg1_led1_config; enable LED1 Green */              \
    0x18,      /* ppg1_led2_config; disabled */                       \
    0x18,      /* ppg1_led3_config; disabled */                       \
    0x18,      /* ppg1_led4_config; disabled */                       \
    0x10,      /* ppg1_mode; use external photodiode 1 */             \
    0x3,       /* ppg1_measconfig; */                                 \
    0x0,       /* ppg1_adcconfig; Set for low power configuration */  \
  },                                                                  \
  {            /* PPG2 */                                             \
    0x18,      /* ppg2_led1_config; disabled */                       \
    0x18,      /* ppg2_led2_config; disabled */                       \
    0x18,      /* ppg2_led3_config; disabled */                       \
    0x58,      /* ppg2_led4_config; enable LED4 Green  */             \
    0x10,      /* ppg2_mode; use external photodiode 1 */             \
    0x3,       /* ppg2_measconfig; */                                 \
    0x70,      /* ppg2_adcconfig; Set for low noise configuration */  \
  },                                                                  \
  {            /* PPG3 - Note Used */                                 \
    0x18,      /* ppg3_led1_config; disabled */                       \
    0x18,      /* ppg3_led2_config; disabled */                       \
    0x18,      /* ppg3_led3_config; disabled */                       \
    0x18,      /* ppg3_led4_config; disabled */                       \
    0x10,      /* ppg3_mode; use external photodiode 1 */             \
    0x3,       /* ppg3_measconfig; */                                 \
    0x70,      /* ppg3_adcconfig; */                                  \
  },                                                                  \
  {            /* PPG4 - Note Used */                                 \
    0x18,      /* ppg4_led1_config; disabled */                       \
    0x18,      /* ppg4_led2_config; disabled */                       \
    0x18,      /* ppg4_led3_config; disabled */                       \
    0x18,      /* ppg4_led4_config; disabled */                       \
    0x10,      /* ppg4_mode; use external photodiode 1 */             \
    0x3,       /* ppg4_measconfig; */                                 \
    0x70,      /* ppg4_adcconfig; */                                  \
  }},                                                                 \
}


/*****************************************************************************
 * Float type used in hrm algorithm
******************************************************************************/
typedef float sihrmFloat_t;


/*****************************************************************************
 * Data structure for HRM Data returned from the algorithm
 *
******************************************************************************/
typedef struct Si117xhrmData
{
  int16_t Fs;
  uint32_t hrmAgcResults;   //4x 8-bits: Each 8-bits = (Clk_Div[i]0x3)<<(i*8+6) | (LEDcurrent[i]0x3f)<<(i*8): i=0~3 for ppg[0~3].
  uint16_t hrmRawPpg;
  uint16_t hrmRawPpg2,hrmRawPpg3,hrmRawPpg4;
  int16_t hrUpdateInterval;
  uint16_t hrmHRQ;
  sihrmFloat_t hrmAuxData1;
  uint16_t hrmAuxData2;
  uint16_t hrmHrvRRms[2]; // 2 RrInterval<<1+RrValid values per second
} Si117xhrmData_t;


/*****************************************************************************
* Interrupt Sample
*
******************************************************************************/
typedef struct Si117xhrmIrqSample
{
  uint16_t  sequence;         // sequence number
  uint16_t  timestamp;        // 16-bit Timestamp to record
  uint16_t  ppg1;             // PPG1 Sample
  uint16_t  ppg2;             // PPG1 Sample
  uint16_t  ppg3;             // PPG1 Sample
  uint16_t  ppg4;             // PPG1 Sample
  uint8_t syncMessage;       // if a synch message is found in the FIFO since the last sample it should be placed here
  int16_t accelerometer_x;    //Accelerometer X
  int16_t accelerometer_y;    //Accelerometer Y
  int16_t accelerometer_z;    //Accelerometer Z
} Si117xhrmIrqSample_t;




/*****************************************************************************
* Data structure for auxillary debug data
*
******************************************************************************/
#define  SI117xHRM_NUM_AUX_DEBUG_DATA_VALUES   7

typedef struct Si117xhrmSampleAuxDebug
{ uint32_t data[SI117xHRM_NUM_AUX_DEBUG_DATA_VALUES];
}Si117xhrmSampleAuxDebug_t;



/*  Si117xhrmUserConfiguration_t algorithmControl bits  */
#define SI117xHRM_CONTROL_DEBUG_CHANNEL_ENABLE_BIT          0
#define SI117xHRM_CONTROL_DEBUG_FIFO_TEST_MODE_ENABLE_BIT   1
#define SI117xHRM_CONTROL_IOD_ENABLE_BIT                    2
#define SI117xHRM_CONTROL_RR_INTERVAL_ENABLE_BIT            3

#define SI117xHRM_CONTROL_DEBUG_CHANNEL_ENABLE_MASK          0x1
#define SI117xHRM_CONTROL_DEBUG_FIFO_TEST_MODE_ENABLE_MASK   0x2
#define SI117xHRM_CONTROL_IOD_ENABLE_MASK                    0x4
#define SI117xHRM_CONTROL_RR_INTERVAL_ENABLE_MASK            0x8


/*****************************************************************************
* Defines and typedefs for memory allocation and management
******************************************************************************/

/**
 * Number of bytes needed in RAM for basic HRM
 */
 #define SI117xHRM_HRM_DATA_SIZE                          4148

/**
 * Number of bytes needed in RAM for HRM with dynamic multi-LED feature
 */
 #define SI117xHRM_HRM_MULTI_LED_DATA_SIZE         2560

/**
 * Number of bytes needed in RAM for RR Interval
 */
 #define SI117xHRM_RR_INTERVAL_DATA_SIZE                  1356

/**
 * Number of bytes needed in RAM for Inanimate Object Detection (IOD)
 */
 #define SI117xHRM_IOD_DATA_SIZE   336


/**
 * Struct for allocating memory for basic HRM
 */
typedef struct Si117xhrmDataStorage
{ HANDLE si117xHandle;                            //Handle for low-level si117x driver (see si117x_functions and si117x_sys_out
  SI117XDRV_DeviceSelect_t deviceID;              //Handle for Si117xDrv
  void *userData;                                 //pointer to user-defined data.  Set to NULL if not used.
  uint8_t hrmData[SI117xHRM_HRM_DATA_SIZE];       //data block for algorithm use.  Do not modify after initialization
} Si117xhrmDataStorage_t;


/**
 * Struct for allocating memory for multi led mode
 */
typedef struct Si117xhrmMultiLedDataStorage
{ uint8_t multiLedData[SI117xHRM_HRM_MULTI_LED_DATA_SIZE];
} Si117xhrmMultiLedDataStorage_t;

/**
* Struct for allocating memory for RR Interval
  */
typedef struct Si117xhrmRrIntervalDataStorage
{ uint8_t rrData[SI117xHRM_RR_INTERVAL_DATA_SIZE];
} Si117xhrmRrIntervalDataStorage_t;

/**
* Struct for allocating memory for Inanimate Object Detection
 */
typedef struct Si117xhrmIodDataStorage
{ uint8_t iodData[SI117xHRM_IOD_DATA_SIZE];
} Si117xhrmIodDataStorage_t;



typedef Si117xhrmDataStorage_t * Si117xhrmHandle_t;


/*****************************************************************************
* Algorithm and device configuration data types
******************************************************************************/

typedef  SI117XDRV_PPGCfg_t Si117xhrmDeviceConfiguration_t;

typedef struct Si117xhrmUserConfiguration
{
  uint8_t taskEnable;
  uint8_t powerMode;
  uint8_t algorithmControl;        //bitfield: 0 - debug enable, 1 - FIFO Test Mode enable, 2 - IOD enable, 3 - RR Interval enable
  uint8_t accelerometerSynchronizationMode;
  uint32_t timestampPeriod100ns;                             //The timestamp period in units of 100 nanoseconds...
  uint32_t accelerometerNormalizationFactorx10;
  uint16_t numSamplesPerInterrupt;
  uint8_t oversampling;                                      // 0, 2, 4 or 8
  void *debugData;
  Si117xhrmDeviceConfiguration_t *deviceConfiguration;      //The value should be set to NULL for si1175
  Si117xhrmMultiLedDataStorage_t *multiLedDataStorage;
  Si117xhrmRrIntervalDataStorage_t *rrDataStorage;
  Si117xhrmIodDataStorage_t *iodDataStorage;
}Si117xhrmUserConfiguration_t;


/**************************************************************************//**
 * @brief
 *  Initialize the optical sensor device and the HRM algorithm
 *
 * @param[in] portName
 *  Platform specific data to specify the i2c port information.
 *
 * @param[in] options
 *  Initialization options flags.
 *
 * @param[in] handle
 *  Pointer to si117xhrm handle
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_Initialize(void *portName, int32_t options, Si117xhrmHandle_t *handle);

/**************************************************************************//**
 * @brief
 *  Close the optical sensor device and algorithm
 *
 * @param[in] handle
 *  Pointer to si117xhrm handle
 *
 * @return
 *  Returns error status
 *****************************************************************************/
int32_t si117xhrm_Close(Si117xhrmHandle_t handle);

/**************************************************************************//**
 * @brief
 *  Configure si117xhrm debugging mode.
 *
 * @param[in] handle
 *  Pointer to si117xhrm handle
 *
 * @param[in] enable
 *  Enable or Disable debug
 *
 * @param[in] debug
 *  Pointer to debug status
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_SetupDebug(Si117xhrmHandle_t handle, int32_t enable, void *debug);


/**************************************************************************//**
 * @brief
 *  Output debug message
 *
 * @param[in] handle
 *  Pointer to si117xhrm handle
 *
 * @param[in] message
 *  Message data
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_OutputDebugMessage(Si117xhrmHandle_t handle, int8_t * message);

/**************************************************************************//**
 * @brief
 *  Configure device and algorithm
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[in] configuration
 *  Pointer to a configuration structure of type Si117xhrmConfiguration_t
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_Configure(Si117xhrmHandle_t handle, Si117xhrmUserConfiguration_t *configuration);

/**************************************************************************//**
 * @brief
 *  Query the algorithm power mode setting.
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[out] mode
 *  Pointer to a location where the power mode will be returned
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_GetPowerMode(Si117xhrmHandle_t handle, uint8_t *mode);

/**************************************************************************//**
 * @brief
 *  Set the algorithm power mode setting.
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[in] mode
 *  The power mode.  0 = low power
 *                   1 = performance mode
 *                   2 = multi-LED mode
 *
 * @param[in] multiLedDataStorage
 *  Multi-LED mode requires that the application allocate data storage of type
 *  Si117xhrmMultiLedDataStorage_t for use by the algorithm.  When setting the
 *  mode to 2, the application must pass the address to this allocated memory
 *  to this parameter.  In all other cases, the user can pass NULL.
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_SetPowerMode(Si117xhrmHandle_t handle, uint8_t mode, Si117xhrmMultiLedDataStorage_t *multiLedDataStorage);

/**************************************************************************//**
 * @brief
 *  Enable/Disable running of Inanimate Objects Detection
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[in] enable
 *  0 = disabled, 1 = enabled
 *
 * @param[in] iodDataStorage
 *  Inanimate Object Detection requires that the application allocate data
 *  storage of type Si117xhrmIodDataStorage_t for use by the algorithm.  When
 *  enabling Inanimate Object Detection, the application must pass the address
 *  to this allocated memory to this parameter.  In all other cases, the user
 *  can pass NULL.
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_EnableInanimateObjectDetection(Si117xhrmHandle_t handle, uint8_t enable, Si117xhrmIodDataStorage_t *iodDataStorage);

/**************************************************************************//**
 * @brief
 *  Returns the enable/disable  setting of Inanimate Objects Detection
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[out] enable
 *  0 = disabled, 1 = enabled
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_GetInanimateObjectDetection(Si117xhrmHandle_t handle, uint8_t *enable);


/**************************************************************************//**
 * @brief
 *  Enable/Disable running of RR Intervals
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[in] enable
 *  0 = disabled, 1 = enabled
 *
 * @param[in] rrDataStorage
 *  RR Intervals require that the application allocate data storage of type
 *  Si117xhrmRrIntervalDataStorage_t for use by the algorithm.  When enabling
 *  RR intervals, the application must pass the address to this allocated memory
 *  to this parameter.  In all other cases, the user can pass NULL.
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_EnableRrInterval(Si117xhrmHandle_t handle, uint8_t enable, Si117xhrmRrIntervalDataStorage_t *rrDataStorage);



/**************************************************************************//**
 * @brief
 *  Returns the enable/disable  setting of RR Intervals
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[out] enable
 *  0 = disabled, 1 = enabled
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_GetRrIntervalEnable(Si117xhrmHandle_t handle, uint8_t *enable);

/**************************************************************************//**
 * @brief
 *  HRM process engine.  This function should be called at least once per sample
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[out] heartRate
 *  Pointer to a location where this function will return the heart rate result
 *
 * @param[in] numSamples
 *
 * @param[out] numSamplesProcessed
 *
 * @param[out] hrmStatus
 *  Pointer to a integer where this function will report status flags
 *
 * @param[out] hrmData
 *  Optional pointer to a si117xhrmData_t structure where this function will return
 *  auxiliary data useful for the application.  If the application is not
 *  interested in this data it may pass NULL to this parameter.
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_Process(Si117xhrmHandle_t handle, int16_t *heartRate, int16_t numSamples, int16_t *numSamplesProcessed, int32_t *hrmStatus, Si117xhrmData_t *hrmData);

/**************************************************************************//*
 * @brief
 *  Start the device's autonomous measurement operation.
 *  The device must be configured before calling this function.
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[in] reset
 *  Reset the internal parameters of the HRM algorithm.  If set to false the
 *  HRM algorithm will begin running using parameter values from the last time
 *  it was run.  If the users heart rate has changed significantly since the
 *  last time the algorithm has run and reset is set to false, it could take
 *  longer to converge on the correct heart rate.  It is recommended to set
 *  reset to true if the HRM algorithm has been stopped for greater than 15s.
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_Run(Si117xhrmHandle_t handle, int8_t reset);

/**************************************************************************//**
 * @brief
 *  Pause the device's autonomous measurement operation.
 *  HRM must be running before calling this function.
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_Pause(Si117xhrmHandle_t handle);


/**************************************************************************//**
 * @brief
 *  Returns algorithm version
 *
 * @param[out] revision
 *  String representing the si117xhrm library version.  The version string has
 *  a maximum size of 32 bytes including the terminating 0.
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_QuerySoftwareRevision(int8_t *revision);

/**************************************************************************//**
 * @brief
 *  Get low level i2c handle.
 *
 *  NOTE: This function is deprecated because the handle is available directly
 *        from Si117xhrmHandle_t.
 *
 * @param[in] handle
 *  si117xhrm handle
 *
 * @param[out] deviceHandle
 *  Low level i2c handle
 *
 * @return
 *  Returns error status.
 *****************************************************************************/
int32_t si117xhrm_GetLowLevelHandle(Si117xhrmHandle_t handle, HANDLE *deviceHandle);




#ifdef __cplusplus
}
#endif

#endif    //SI117xHRM_H__
