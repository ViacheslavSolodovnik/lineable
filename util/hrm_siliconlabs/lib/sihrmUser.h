/**************************************************************************//**
 * @brief Implementation specific functions for HRM code
 * @version 3.20.3
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
//#include "em_gpio.h"		//required for GPIO_Port_TypeDef
#include "si117xhrm.h"          //ToDo:  Should this file should be device agnostic?  Should it include this?
#include "si117xdrv.h"
#include "gpiointerrupt.h"

#ifndef SIHRMUSER_H
#define SIHRMUSER_H

#ifdef __cplusplus
extern "C" {
#endif


extern uint16_t PPG1;
extern uint16_t PPG2;



typedef Si117xhrmHandle_t sihrmUserHandle_t;

/*  Debug Channel  */
int32_t sihrmUser_SetupDebug(sihrmUserHandle_t sihrmUser_handle, int32_t enable, void *debug);
int32_t sihrmUser_OutputDebugMessage(sihrmUserHandle_t sihrmUser_handle, const uint8_t *message);
int32_t sihrmUser_OutputRawSampleDebugMessage(sihrmUserHandle_t sihrmUser_handle, Si117xhrmIrqSample_t *sample, Si117xhrmSampleAuxDebug_t *aux);


int32_t sihrmUser_ProcessIrq(sihrmUserHandle_t handle, uint16_t timestamp, SI117XDRV_GlobalCfg_t *globalCfg);
int32_t sihrmUser_ProcessIrqAccel(sihrmUserHandle_t sihrmUser_handle, uint16_t timestamp, Si117xhrmUserConfiguration_t *configuration);
int32_t sihrmUser_SampleQueueNumentries(sihrmUserHandle_t sihrmUser_handle);
int32_t sihrmUser_SampleQueue_Get(sihrmUserHandle_t sihrmUser_handle, uint8_t getAccelSamples, Si117xhrmIrqSample_t *samples);
int32_t sihrmUser_SampleQueue_Clear(sihrmUserHandle_t sihrmUser_handle);
int32_t sihrmUser_Initialize(void *port, int16_t options, sihrmUserHandle_t sihrmUser_handle);
int32_t sihrmUser_Close(sihrmUserHandle_t sihrmUser_handle);

void Si117xDisableInterrupt (HANDLE *si117x_handle);
void Si117xInitInterrupt (HANDLE *si117x_handle,  GPIOINT_IrqCallbackPtr_t callbackPtr);

int32_t sihrmUser_PostSkinSensingCallback(sihrmUserHandle_t sihrmUser_handle);

int16_t Si117xInit(void *port, int options, HANDLE *si117x_handle);
int16_t Si117xClose(HANDLE si117x_handle);
int16_t Si117xCheckSensor(void);

#ifdef __cplusplus
}
#endif

#endif   //SIHRMUSER_H
