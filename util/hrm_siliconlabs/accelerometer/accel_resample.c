
#include "rtos_app.h"
#include <stdint.h>
#include "arm_math.h"//#include <math.h>
#include "Resample_Coefs.h"
#include "accel_resample.h"
#include "accelerometer.h"
#include "si117xhrm.h"
#include "si117xdrv.h"

#include "userdata.h"
#include "gpiointerrupt.h"
#include "lsm6ds3_acc_gyro.h"

#if (ACCEL_DEVICE == LSM6DS3)
#define ACCEL_SAMPLE_RATE_HZ            26    //FsAccel=31.25Hz
#else
#define ACCEL_SAMPLE_RATE_HZ            31.25    //FsAccel=31.25Hz
#endif
#define SI117x_SAMPLE_RATE_HZ           25       //FsResample=25.00Hz
#define RESAMPLER_RATIO_NOMINAL         ((sihrmFloat_t)ACCEL_SAMPLE_RATE_HZ/(sihrmFloat_t)SI117x_SAMPLE_RATE_HZ)
#define A_SYNCH_BY_FIFO				    1		// 1=A SYNCH at G FIFO rate. 0=A SYNCH at G sample rate
#define ResamplerOutputCountBuf_SIZE    120
#define PPG_SAMPLES_PER_SYNC_BUF_SIZE   120
#define ACC_RESAMPLED_QUEUE_SIZE        128
#define A_SYNC_AVERAGING                1
#define FsRatio_MIN                     (sihrmFloat_t)0.833
#define FsRatio_MAX                     (sihrmFloat_t)1.875
#define SYNC_MARKER_INTERVAL            32*ACCEL_FIFO_LEVEL
#define Resampler_AccelR                128
#define Resampler_AccelL                2
#define LPF_FILTER_LEN                  4


#if A_SYNCH_BY_FIFO
  #define RESAMPLE_RATIO_BOOT_FACTOR_DOWN   (sihrmFloat_t)0.99
  #define RESAMPLE_RATIO_BOOT_FACTOR_UP     (sihrmFloat_t)1.01
#else
  #define RESAMPLE_RATIO_BOOT_FACTOR_DOWN   (sihrmFloat_t)0.95
  #define RESAMPLE_RATIO_BOOT_FACTOR_UP     (sihrmFloat_t)1.05
#endif


/*****************************************************************************
* Debug Option
*****************************************************************************/
#undef  __MODULE__
#define __MODULE__ "ACC_RES"
#undef  DBG_MODULE
#define DBG_MODULE 0
#define ACC_RES    0


//  Accelerometer interrupt data buffer
static int16_t AccIrqQueue[ACC_RESAMPLED_QUEUE_SIZE];
/*  interrupt queue current get index  */
static uint16_t AccirqQueueGetIndex = 0;
/*  interrupt queue current put index  */
static uint16_t AccirqQueuePutIndex = 0;

static uint8_t A_SYNC_init=1;
static uint8_t A_SYNC_count=0;
static uint16_t ppgTotal;

static uint16_t PPG_INT_time=0;
static int16_t PpgAccelSampleSlip=0;
static int16_t FsSlip=0;


static sihrmFloat_t FsRatio_Accel_Resample=RESAMPLER_RATIO_NOMINAL; //Initialization: FsAccel=31.25Hz, FsResample=25.00Hz.
static sihrmFloat_t FsRatio_Accel_Resample_Mean=RESAMPLER_RATIO_NOMINAL;
static sihrmFloat_t TimeStamp=0;
static sihrmFloat_t Resampler_Input[Resampler_AccelL*2]={0,0,0,0};
static int16_t ResamplerOutput_PutIndex;
static int16_t ResamplerOutput[ACCEL_FIFO_LEVEL], ResamplerOutput_GetIndex; //set but never used?
static uint16_t ResamplerOutputCount=0;
static uint16_t GsensorInputCount=0;

#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SYNC_MSG)
  static uint16_t ppg_samples_per_SYNC[PPG_SAMPLES_PER_SYNC_BUF_SIZE];
  static uint8_t ppg_samples_per_SYNC_put;

  //static uint8_t ppg_samples_per_SYNC_get;
  static uint16_t ResamplerOutputCountBuf[ResamplerOutputCountBuf_SIZE];
  static uint8_t ResamplerOutputCountPut=0;
  static uint16_t ResampledTotal;
#endif

static sihrmFloat_t Accel_LPF_In[LPF_FILTER_LEN]={0,0,0,0};
static sihrmFloat_t Accel_LPF_Out[LPF_FILTER_LEN]={0,0,0,0};
static sihrmFloat_t LPF_b_ptr[] = {
       0.05017303865640,
       0.01331803127312,
       0.01331803127312,
       0.05017303865640 };

static sihrmFloat_t LPF_a_ptr[] = {
       1.00000000000000,
      -2.05868929614050,
       1.70732726497812,
      -0.52165582897859 };

/**************************************************************************//**
 * @brief  Low Pass Filter (LPF) used by the the resampler
 *****************************************************************************/
static sihrmFloat_t LPF_Filter(sihrmFloat_t input)
{
  //LPF: [LPF_b, LPF_a] = ellip(3, 1, 30, 200/60/Fs0*2);
  //The filter is a "Direct Form II Transposed" implementation of the standard difference equation:
  //a(1)*y(n) = b(1)*x(n) + b(2)*x(n-1) + ... + b(nb+1)*x(n-nb)
  //                      - a(2)*y(n-1) - ... - a(na+1)*y(n-na)
  int8_t i, j;

  for (i = LPF_FILTER_LEN - 1; i > 0; i--) //Shift the LPF in/out data buffers and add the new input sample
  {
    Accel_LPF_In[i] = Accel_LPF_In[i - 1];
    Accel_LPF_Out[i] = Accel_LPF_Out[i - 1];
  }
  Accel_LPF_In[0] = input;            //Add the new input sample
  Accel_LPF_Out[0] = 0;
  for (j = 0; j < LPF_FILTER_LEN; j++)    //a(1)=1, y(n) = b(1)*x(n) + b(2)*x(n-1) + ... + b(nb+1)*x(n-nb)
  {
    Accel_LPF_Out[0] += LPF_b_ptr[j] * Accel_LPF_In[j];
  }
  for (j = 1; j < LPF_FILTER_LEN; j++)    //y =y(n)- a(2)*y(n-1) - ... - a(na+1)*y(n-na)
  {
    Accel_LPF_Out[0] -= LPF_a_ptr[j] * Accel_LPF_Out[j];
  }

  return Accel_LPF_Out[0];
}

/**************************************************************************//**
 * @brief  Monitor Resampler Synchronization
 *****************************************************************************/
void AccelResample_SynchMessageReceived(SI117XDRV_DeviceSelect_t device, uint16_t ppg_count, void *user)
{

#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SYNC_MSG)

  int16_t j;
  uint16_t m;

  ppg_samples_per_SYNC[ppg_samples_per_SYNC_put++]=ppg_count; // # of ppg samples in last 32 G-samples
   if (ppg_samples_per_SYNC_put == PPG_SAMPLES_PER_SYNC_BUF_SIZE)
     ppg_samples_per_SYNC_put = 0;

  if (A_SYNC_init==1)  // Skip before resampling really started
    return;

  j = ppg_samples_per_SYNC_put-1;
  if (j < 0)
    j=PPG_SAMPLES_PER_SYNC_BUF_SIZE-1;

  m = ppg_samples_per_SYNC[j--];
  if (m==0)
    return;

  ppgTotal = ppgTotal + m;

  j = ResamplerOutputCountPut-1;
  if (j < 0)
    j = ResamplerOutputCountBuf_SIZE-1;
  m = ResamplerOutputCountBuf[j--];
  if (m ==0)
    return;

  ResampledTotal = ResampledTotal + m;


  FsSlip =    ppgTotal - ResampledTotal;  // Resampler synchronization offset

#endif   //AccelResample_ADJUSTMENT_METHOD
}


/**************************************************************************//**
 * @brief  Query data that helps debug the accelerometer resampler
 *****************************************************************************/
int16_t AccelResample_QueryDebugData( AccelResample_DebugData_t *data )
{

  data->fsRatio = FsRatio_Accel_Resample;
#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SYNC_MSG)
  data->slip = FsSlip;
  data->ppgTotal = ppgTotal;
  data->ResampledTotal = ResampledTotal;

#else
  data->slip = PpgAccelSampleSlip;
  data->fsRatioMean = FsRatio_Accel_Resample_Mean;
#endif

  return 0;
}

/**************************************************************************//**
 * @brief  Reset the resampler
 *****************************************************************************/
uint16_t AccelResample_Reset()
{ uint16_t error = 0;
  uint16_t i;

  AccirqQueueGetIndex=0;
  AccirqQueuePutIndex=0;
  ppgTotal=0;

  //SI117XDRV_resetSynchSampleCounters (fifoStateMachineID); //now done in initialize fifo

  A_SYNC_init=1;
  A_SYNC_count=0;
  FsSlip=0;

#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SYNC_MSG)
  ResamplerOutputCountPut=0;
  ResampledTotal=0;
  ppg_samples_per_SYNC_put=0;
  for (i=0; i<PPG_SAMPLES_PER_SYNC_BUF_SIZE; i++)
    ppg_samples_per_SYNC[i]=0;

  for (i=0; i<ResamplerOutputCountBuf_SIZE; i++)
    ResamplerOutputCountBuf[i]=0;

#endif

  for (i=0; i<LPF_FILTER_LEN; i++)
  { Accel_LPF_In[i] = 0;
    Accel_LPF_Out[i] = 0;
  }

  FsRatio_Accel_Resample=RESAMPLER_RATIO_NOMINAL;
  FsRatio_Accel_Resample_Mean=RESAMPLER_RATIO_NOMINAL;
  TimeStamp=0;

  for(i=0; i<Resampler_AccelL*2; i++)
  { Resampler_Input[i]=0;
  }
  ResamplerOutputCount=0;
  GsensorInputCount=0;
  PpgAccelSampleSlip=0;

  return error;
}


/**************************************************************************//**
 * @brief Check is any resampled G data
 *****************************************************************************/
uint16_t AccelResample_HasData(void)
{
  uint16_t data=1;
  if (AccirqQueuePutIndex == AccirqQueueGetIndex)
    data = 0;

  return (data);
}

/**************************************************************************//**
 * @brief Get resampled G data
 *****************************************************************************/
uint16_t AccelResample_GetSample(void)
{
  uint16_t data;
  data = AccIrqQueue[AccirqQueueGetIndex];
  AccirqQueueGetIndex++;
  if (AccirqQueueGetIndex == ACC_RESAMPLED_QUEUE_SIZE)
    AccirqQueueGetIndex = 0;

  return (data);
}


/**************************************************************************//**
 * @brief Notify the resampler that a si117x IRQ occured
 *****************************************************************************/
int16_t AccelResample_Si117xInterruptNotification(uint16_t timestamp)
{
  PPG_INT_time = timestamp;
  return 0;
}

/**************************************************************************//**
 * @brief Put Accelerometer FIFO data into G-sample queue
 *****************************************************************************/
int16_t AccelResample_ProcessAccFifoDataRecord(uint8_t *record, uint16_t fifo_length, uint16_t timestamp, SI117XDRV_DataQueueID_t ppgQueueID, Si117xhrmUserConfiguration_t *configuration)
{
  int16_t data[3];
  uint16_t i, j;
  uint16_t k, m, n; //The Re-sampling uses.
  sihrmFloat_t xyz, LPF_xyz, AccelDecimation, Resampler_output; //The Re-sampling uses.
  int16_t p, q, temp;
  uint8_t ds;
  uint32_t timestampFreqHz;

  //Debug:
  for (j=0; j<ACCEL_FIFO_LEVEL; j++)
  { ResamplerOutput[j]=0;
  }
  ResamplerOutput_PutIndex=0;
  ResamplerOutput_GetIndex=0; //Debug: for <DLE>a data dump: initialization.

#if ACCEL_DEVICE==BMA222
  ds = 0x00;  // No data in LSB byte.  BMA222 is 8-bit
#elif ACCEL_DEVICE==LSM6DS3
  ds = 0xff;
#else
  ds = 0xf0;  // Take 4 bits from LSB byte.  BMA255 is 12-bit
#endif

  for (j=0; j<fifo_length/6; j++) //fifo_length is in bytes. 6 or 3 bytes for X,Y,Z.
  {
    data[0] = (((uint16_t)((*(record+1)) << 8) & 0xff00) | (*(record+0) & ds));            // X
    data[1] = (((uint16_t)((*(record+3)) << 8) & 0xff00) | (*(record+2) & ds));            // Y
    data[2] = (((uint16_t)((*(record+5)) << 8) & 0xff00) | (*(record+4) & ds));            // Z
    record = record +12;

    //debug_printf_data("[%02d] %6d %6d %6d\n", j, data[0], data[1], data[2]);

  //*********************************************************
  //Accelerometer re-sampling process:
  //*********************************************************
    //1) Calculate sqrt(X^2+Y^2+Z^2)
    xyz=(sihrmFloat_t)(data[0]*data[0])+(sihrmFloat_t)(data[1]*data[1])+(sihrmFloat_t)(data[2]*data[2]);
    xyz=sqrt(xyz)/4;

    //2) Apply LPF to XYZ to remove the high-frequency noise
    LPF_xyz=LPF_Filter(xyz);
    //debug_output[debug_iptr-1]=LPF_xyz; //ResamplerDebug:

    //3 Re-sample LPF(XYZ) from 31.25Hz to 25Hz (Output 0 or 1 sample). The code below can work for any FsAccel/FsResample ratio.
    AccelDecimation=Resampler_AccelR*FsRatio_Accel_Resample;
    Resampler_Input[Resampler_AccelL*2-1]=LPF_xyz; //Last element is the newest. Input the sample at FsAccel

Resampler_Additional_Output:
    if (TimeStamp >= Resampler_AccelR)
    {
      TimeStamp=TimeStamp-Resampler_AccelR;
      for (n=1; n<Resampler_AccelL*2; n++)
      { Resampler_Input[n-1]=Resampler_Input[n]; //Shift the resampler buffer. Last element is the newest.
      }
    }
    else
    {
      m=Resampler_AccelL*2;
      k=Resampler_AccelR-((int16_t)TimeStamp+1); //0<=k<=Resampler_AccelR-1, 0<=TimeStamp<Resampler_AccelR. k=Bank index.
      i=k*m;                //Filter index
      Resampler_output=0;

      for (n=0; n<m; n++)
      { if (i <= Resampler_AccelR/2*m) //Reduce the resampler filter coef size by half to 2*4*128/2=512 bytes (2 for int16_t)
          Resampler_output += Resampler_Coefs[i+n]*Resampler_Input[n]; //Re-sampler filtering
        else
          Resampler_output += Resampler_Coefs[Resampler_AccelR*m-i +(m-1)-n]*Resampler_Input[n]; //Re-sampler filtering
      }

      Resampler_output=Resampler_output/32767; //32767=Q15 as Resampler_Coefs is in Q15=2^15-1

      //Debug:
      ResamplerOutput[ResamplerOutput_PutIndex++]=(int16_t)Resampler_output; //Debug: for <DLE>a data dump: output the resampled sample at FsResample.

      ResamplerOutputCount++;         // Count resampled samples

      // Save resampled G-data in queue to be merged with PPG sample

      if (A_SYNC_init == 0)
    	  AccIrqQueue[AccirqQueuePutIndex] = (int16_t)Resampler_output;
      else
      {   // Very first samples are not really valid. Force first 15 samples to be 1G to improve start up.
    	  AccIrqQueue[AccirqQueuePutIndex] = configuration->accelerometerNormalizationFactorx10/10;
    	  A_SYNC_init = A_SYNC_init+1;
    	  if (A_SYNC_init >= 16)
    		  A_SYNC_init = 0;
      }
      AccirqQueuePutIndex++;
      if(AccirqQueuePutIndex == ACC_RESAMPLED_QUEUE_SIZE)
        AccirqQueuePutIndex = 0;

      //debug_output[debug_optr++]=Resampler_output; //ResamplerDebug: Output the sample at FsResample
      TimeStamp += AccelDecimation; //Increase the resampler's timestamp.
      goto Resampler_Additional_Output; //Go to check if the re-sampler outputs 1 more sample.
    }
    //*********************************************************
    //End of accelerometer re-sampling process
    //*********************************************************

#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SYNC_MSG)
    GsensorInputCount++;
    if (GsensorInputCount == SYNC_MARKER_INTERVAL)  // Processed 32 G-samples. Record how many resampled samples are produced.
    {
      ResamplerOutputCountBuf[ResamplerOutputCountPut++]=ResamplerOutputCount;
      ResamplerOutputCount = 0;
      if (ResamplerOutputCountPut == ResamplerOutputCountBuf_SIZE)
        ResamplerOutputCountPut = 0;
      GsensorInputCount = 0;

      if (A_SYNC_init == 1)
        A_SYNC_init = 0;
      else
      { A_SYNC_count++;
        if (A_SYNC_count == A_SYNC_AVERAGING)
        { A_SYNC_count=0;
          temp = 0;
          q = ppg_samples_per_SYNC_put-1;
          if (q < 0)
            q = PPG_SAMPLES_PER_SYNC_BUF_SIZE-1;

          for (p=0; p<A_SYNC_AVERAGING; p++)
          { temp = temp + ppg_samples_per_SYNC[q--];
            if (q < 0)
              q = PPG_SAMPLES_PER_SYNC_BUF_SIZE-1;
          }
          FsRatio_Accel_Resample = (sihrmFloat_t)SYNC_MARKER_INTERVAL*A_SYNC_AVERAGING/temp;

          // Boost the ratio to try to pull it back the other way faster
          if (FsSlip > 15)
            FsRatio_Accel_Resample = FsRatio_Accel_Resample * RESAMPLE_RATIO_BOOT_FACTOR_DOWN;
          if (FsSlip < -15)
            FsRatio_Accel_Resample = FsRatio_Accel_Resample * RESAMPLE_RATIO_BOOT_FACTOR_UP;

          if ((FsRatio_Accel_Resample < FsRatio_MIN)||(FsRatio_Accel_Resample > FsRatio_MAX))
            FsRatio_Accel_Resample = RESAMPLER_RATIO_NOMINAL;
        }
      }
    }
  }
#else
  }

  // FsRatio_Accel_Resample update scheme
  // Definition: SampleSlip = the difference between the resampled G-sensor samples in queue and (the PPG samples in FIFO + PPG samples in queue) when G-sensor interrupt occurs.
  // Basically, use the current SampleSlip and last SampleSlip to update FsRatio_Accel_Resample.
  // The goal is to find optimal FsRatio_Accel_Resample, so SampleSlip is close to 0.
  //
  // 1) Save the timestamp to PPG_INT_time when the PPG interrupt occurs.
  // 2) When the G-sensor interrupt occurs, the new G-sensor samples are resampled into the G-sensor queue.
  // 3) Calculate the G-sensor samples in queue using its queue Get and Put pointers.
  // 4) Calculate the elapsed time after PPG_INT_time, and use the elapsed time to estimate the PPG samples in FIFO.
  // 5) Calculate the total of the PPG samples in FIFO and the PPG samples in queue using its queue Get and Put pointers.
  // 6) Calculate the sample difference (SampleSlip) between the unprocessed PPG and G-sensor samples.
  // 7) if SampleSlip is within range (-1~+1 sample), FsRatio_Accel_Resample=FsRatio_Accel_Resample_Mean.
  // 8) If SampleSlip is out of range, update FsRatio_Accel_Resample based on the current SampleSlip and the different between current SampleSlip and last SampleSlip.
  // 9) Update FsRatio_Accel_Resample_Mean that is the optimal FsRatio_Accel_Resample.
/*
  1. What are the minimum values for IRQ_QUEUE_SIZE and ACC_RESAMPLED_QUEUE_SIZE for Si117xSamplesPerChannel=15 and ACCEL_FIFO_LEVEL=24.

  1.a) IRQ_QUEUE_SIZE minimum value
  As the Accelerometer interrupt interval (768ms) is larger than the PPG interrupt interval (600ms), the minimum should be 0.768*25=19.2 samples if the 2 rates
  are locked perfectly with SampleSlip=0. Considered SampleSlip can be up to +/-2 after the timing is locked (+/-3 during the timing acquiring), the minimum
  is 19.2+3=22.2 samples. So, 24 samples should be safe for IRQ_QUEUE_SIZE=24*20=480.

  1.b) ACC_RESAMPLED_QUEUE_SIZE minimum value
  24 samples should be also safe for ACC_RESAMPLED_QUEUE_SIZE.

  The test with IRQ_QUEUE_SIZE=480 and ACC_RESAMPLED_QUEUE_SIZE=24 showed that the timing locked very well for 3-hour test.

  2. Dynamic range: RESAMPLER_RATIO_NOMINAL=31.25/25 (FsAccel=31.25Hz, FsResample=25.00Hz). Below are the test results for 31.25Hz +/-5%=29.76~32.82.
  RESAMPLER_RATIO_NOMINAL=29.69/25; //IRQ_QUEUE_SIZE(440 failed at B1, 460 worked). ACC_RESAMPLED_QUEUE_SIZE(24 failed at B4, 25 worked)
  RESAMPLER_RATIO_NOMINAL=32.81/25; //IRQ_QUEUE_SIZE(500 failed at B1, 520 worked). ACC_RESAMPLED_QUEUE_SIZE(20 failed at B4, 21 worked)
  IRQ_QUEUE_SIZE=28*20 & ACC_RESAMPLED_QUEUE_SIZE=32 worked up to 31.25*(1+/-0.07)/25, or +/-7% of FsAccel.

  3. Timing acquisition effectiveness: Locked in 20~30s.

  4. Decision: Use IRQ_QUEUE_SIZE=28*20, ACC_RESAMPLED_QUEUE_SIZE=32 for Si117xSamplesPerChannel=15 and ACCEL_FIFO_LEVEL=24. (Please also see the comments in heart_rate_monitor.h)
*/

  temp=(int16_t)(timestamp-PPG_INT_time);
  if (timestamp < PPG_INT_time)                 //DoTo: This "if" line seems not needed.
    temp = 0xffff-PPG_INT_time+timestamp+1;

  timestampFreqHz = 10e6 / configuration->timestampPeriod100ns;
  temp=((int32_t)SI117x_SAMPLE_RATE_HZ * temp + timestampFreqHz / 2) / timestampFreqHz; //Total Ppg Samples In Fifo. +8192/2 due to round-up.

  if (configuration->oversampling != 0)
    temp += SI117XDRV_NumSamplesInQueue (ppgQueueID)/configuration->oversampling;
  else
    temp += SI117XDRV_NumSamplesInQueue (ppgQueueID);


  q=AccirqQueuePutIndex-AccirqQueueGetIndex;
  if (AccirqQueuePutIndex<AccirqQueueGetIndex)
    q += ACC_RESAMPLED_QUEUE_SIZE; //q = Total resampled G-sample in queue.
  p=PpgAccelSampleSlip; //Save the last SampleSlip
  PpgAccelSampleSlip=(temp-q); //Calculate the current SampleSlip. Positive if more Ppg samples.

  if (((temp-q)>=2) || ((temp-q)<=-2))
  {//When out-of-range, adjust FsRatio_Accel_Resample and update FsRatio_Accel_Resample_Mean.
    FsRatio_Accel_Resample -= ((sihrmFloat_t)temp-q)/1100 + (PpgAccelSampleSlip-p)/280; // 1/1100 and 1/280 are the update rates. Smaller FsRatio generates more resampled G-sensor samples.
    FsRatio_Accel_Resample_Mean = 0.93*(FsRatio_Accel_Resample_Mean-FsRatio_Accel_Resample) + FsRatio_Accel_Resample;
  }
  else
    FsRatio_Accel_Resample = FsRatio_Accel_Resample_Mean; //Use the mean FsRatio

#endif

  return 0;
}





