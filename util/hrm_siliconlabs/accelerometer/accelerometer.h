#ifndef __ACCELEROMETER_H_
#define __ACCELEROMETER_H_

#include "em_gpio.h"


#define ACCELEROMETER_SUCCESS    0

#define ADXL345       1            //This code currently only support the BMA255
#define BMA255        2
#define LSM6DS3       3
#define ACCEL_DEVICE    LSM6DS3    // Select which accelerometer

#if (ACCEL_DEVICE == ADXL345)
  #define ACCEL_I2C_ADDR    0x1d
  #define ACCEL_ID          0xe5
#endif

#if (ACCEL_DEVICE == BMA255)
  #define ACCEL_I2C_ADDR    0x18
  #define ACCEL_ID          0xFA

  #define ACCELEROMETER_FIFO_WM_INT    0x40

  #define ACCELEROMETER_REG_BGW_CHIPID     0x00
#endif

#if (ACCEL_DEVICE == LSM6DS3)
#endif

#define ACCEL_FIFO_LEVEL  24  // number of 6-byte frame

typedef struct AccelPortConfig
{
  uint8_t i2cAddress;
  GPIO_Port_TypeDef irqPort;
  int irqPin;
  I2C_TypeDef *i2c;
}AccelPortConfig_t;

typedef AccelPortConfig_t* AccelHandle_t;


int16_t Accelerometer_Init(AccelPortConfig_t* i2c);
int16_t Accelerometer_DeepSuspend(void);
int16_t Accelerometer_Disable(void);
int16_t Accelerometer_Enable(void);
int16_t Accelerometer_ReadDataFromDevice(int16_t *accel_x, int16_t *accel_y, int16_t *accel_z);
int16_t Accelerometer_ReadFifo(uint8_t numBytes, uint8_t *data);
int16_t Accelerometer_ClearIRQ(void);
int16_t Accelerometer_GetIrqStatus(int16_t *status);
void Accelerometer_EnableInterrupt(void);
void Accelerometer_DisableInterrupt(void);
int16_t Accelerometer_ConfigureIrqOutput (uint8_t irq, uint8_t openDrain, uint8_t activeHigh);

int16_t AccelInit(AccelPortConfig_t* port, int options, AccelHandle_t* accel_handle);
int16_t AccelWriteToRegister(AccelPortConfig_t* accel_handle, uint8_t address, uint8_t data);
int16_t AccelBlockRead(AccelPortConfig_t* accel_handle, uint8_t address, uint8_t length, uint8_t *values);
int16_t AccelReadFromRegister(AccelPortConfig_t*accel_handle, uint8_t address);




#endif //__ACCELEROMETER_H
