#ifndef ACCEL_RESAMPLE_H
#define ACCEL_RESAMPLE_H

#ifdef __cplusplus
extern "C" {
#endif


#include "si117xhrm.h"
#include "si117xdrv.h"

#define AccelResample_ADJUSTMENT_METHOD_SYNC_MSG    0
#define AccelResample_ADJUSTMENT_METHOD_SOFTWARE    1

#define AccelResample_ADJUSTMENT_METHOD     AccelResample_ADJUSTMENT_METHOD_SOFTWARE

typedef struct AccelResample_DebugData
{
  int16_t slip;
  sihrmFloat_t fsRatio;
  
#if (AccelResample_ADJUSTMENT_METHOD == AccelResample_ADJUSTMENT_METHOD_SYNC_MSG)   
  uint16_t ppgTotal;
  uint16_t ResampledTotal;
#else
  sihrmFloat_t fsRatioMean;
#endif  
} AccelResample_DebugData_t;


uint16_t AccelResample_GetSample(void);
void AccelResample_SynchMessageReceived(SI117XDRV_DeviceSelect_t device, uint16_t ppg_count, void *user);
uint16_t AccelResample_Reset(void);
uint16_t AccelResample_HasData(void);
int16_t AccelResample_ProcessAccFifoDataRecord(uint8_t *record, uint16_t fifo_length, uint16_t timestamp, SI117XDRV_DataQueueID_t ppgQueueID, Si117xhrmUserConfiguration_t *configuration);
int16_t AccelResample_Si117xInterruptNotification(uint16_t timestamp);
int16_t AccelResample_QueryDebugData( AccelResample_DebugData_t *data );


#ifdef __cplusplus
}
#endif

#endif //ACCEL_RESAMPLE_H
