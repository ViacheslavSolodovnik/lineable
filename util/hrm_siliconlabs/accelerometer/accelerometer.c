/**************************************************************************//**
 * @brief Implementation specific functions for HRM code
 * @version x.xx.x
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2018 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "accelerometer.h"
#include "em_gpio.h"
#include  <drivers/peripheral/i2c/micrium_i2c.h>
#if (ACCEL_DEVICE == LSM6DS3)
#include "gpiointerrupt.h"
#include "lsm6ds3_acc_gyro.h"
#endif


/*  Critical Sections in this code are meant to prevent I2C or SPI collisions. If bus transactions occur
 *  inside an interrupt context then collisions can occur.  In this case, the code should disable any
 *  interrupts in which a collision may occur.  The "safe" thing to do is to ensure that bus transactions
 *  never occur in the interrupt context.  In this case critical sections are not needed.
 */
#define ACCELEROMETER_USE_CRITICAL_SECTIONS  0

#if (ACCELEROMETER_USE_CRITICAL_SECTIONS == 1)
	void Accelerometer_EnterCriticalSection(void)
	{
	   /*  add code here to disable any interrupts that may impact this section
	    *  typically this is any interrupt in which an I2C or SPI transaction may occur
	    */

	}

	void Accelerometer_ExitCriticalSection(void)
	{
		   /*  add code here to re-enable any interrupts that may impact this section
		    *  typically this is any interrupt in which an I2C or SPI transaction may occur
		    *  Be careful not to accidently enable interrupts that were not previously disabled
		    */

	}

#else
  #define Accelerometer_EnterCriticalSection()     //Do nothing
  #define Accelerometer_ExitCriticalSection()      //Do nothing
#endif




/*  accelerometer low-level handle */
static AccelHandle_t accelHandle;
static AccelPortConfig_t _handle;

/*  Non-exported Function Prototypes  */


static int16_t Accel_i2c_smbus_write_byte_data(AccelHandle_t accel_handle, uint8_t address, uint8_t data);
static int16_t Accel_i2c_smbus_read_byte_data(AccelHandle_t accel_handle, uint8_t address, uint8_t *data);
static int16_t Accel_i2c_smbus_write_i2c_block_data(AccelHandle_t accel_handle, uint8_t address, uint8_t length, uint8_t const* values);
static int16_t Accel_i2c_smbus_read_i2c_block_data(AccelHandle_t accel_handle, uint8_t address, uint8_t length, uint8_t* values);


/**************************************************************************//**
 * @brief Initialize the Accelerometer
 *****************************************************************************/
int16_t Accelerometer_Init(AccelPortConfig_t* i2c)
{
  if(AccelInit(i2c, 0, &accelHandle) == 0)
  {
#if (ACCEL_DEVICE != LSM6DS3)
    //Set INT1, INT2 pin to push-pull and active low
    AccelWriteToRegister(accelHandle, 0x20, 0x02);

  /* Setup BMA255 accelerometer but put it in Standby mode */
    AccelWriteToRegister(accelHandle, 0x0f, 0x08);		// +/- 8g
    AccelWriteToRegister(accelHandle, 0x10, 0x09);		// BW=15.62Hz, Fs = 2 x BW = 31.25

  /* Standby mode */
    AccelWriteToRegister(accelHandle, 0x12, 0x40);		// low power mode
    AccelWriteToRegister(accelHandle, 0x11, 0x80);		// Suspend power mode

    AccelWriteToRegister(accelHandle, 0x30, ACCEL_FIFO_LEVEL);		// 24, FIFO watermark =< 32
    AccelWriteToRegister(accelHandle, 0x21, 0x87);		// INT is latched

    /* Clear FIFO */
    AccelWriteToRegister(accelHandle, 0x3e, 0x40);	    // Stop when FIFO full

    AccelWriteToRegister(accelHandle, 0x17, 0x00);		// INT disabled.
    AccelWriteToRegister(accelHandle, 0x1a, 0x00);		// INT2 pin disabled
    AccelReadFromRegister(accelHandle, 0x0a);             // clear INGT flag
#endif
  }
  else
    accelHandle = 0;

  return ACCELEROMETER_SUCCESS;
}


/**************************************************************************//**
 * @brief Place the accelerometer into deep suspend mode
 *****************************************************************************/
int16_t Accelerometer_DeepSuspend(void)
{
  AccelWriteToRegister(accelHandle, 0x11, 0x20);		// Deep Suspend power mode
  return ACCELEROMETER_SUCCESS;

}

/**************************************************************************//**
 * @brief Configure the IRQ Output.  The irq parameter is 0 (IRQ 1) or 2 (IRQ 2)
 *****************************************************************************/
int16_t Accelerometer_ConfigureIrqOutput (uint8_t irq, uint8_t openDrain, uint8_t activeHigh)
{
  uint32_t regValue=0;

  regValue = AccelReadFromRegister(accelHandle, 0x20);

  regValue &= 0xFF;
  if(openDrain == 1)
    regValue |= 0x2 << (irq*2);
  else
    regValue &= ~(0x2 << (irq*2));

  if(activeHigh == 1)
    regValue |= 0x1 << (irq*2);
  else
    regValue &= ~(0x1 << (irq*2));

  AccelWriteToRegister(accelHandle, 0x20, regValue);
  return ACCELEROMETER_SUCCESS;
}


/**************************************************************************//**
 * @brief Enable the Accelerometer
 *****************************************************************************/
int16_t Accelerometer_Enable(void)
{  // For BMA255 only
#if (ACCEL_DEVICE == LSM6DS3)
#else
#if LEGACY==1  //legacy mode is when the si117x is configured to interrupt every sample.  In this mode the accel interrupt is disabled and we read accel when si117x IRQ is handeled
  AccelWriteToRegister(accelHandle, 0x17, 0x00);		// INT disabled.
  AccelWriteToRegister(accelHandle, 0x1a, 0x00);		// INT2 pin disabled
  AccelReadFromRegister(accelHandle, 0x0a);             // clear INGT flag
#else

  AccelWriteToRegister(accelHandle, 0x30, ACCEL_FIFO_LEVEL);		// 24, FIFO watermark =< 32
  AccelWriteToRegister(accelHandle, 0x17, 0x50);		// FIFO watermark INT enabled.
  AccelWriteToRegister(accelHandle, 0x1a, 0x42);		// INT2 and INT1 pins enabled for FIFO watermark

#endif

  AccelWriteToRegister(accelHandle, 0x3e, 0x40);		// clear Acc FIFO
  AccelWriteToRegister(accelHandle, 0x11, 0x00);		// Normal power mode
  AccelWriteToRegister(accelHandle, 0x12, 0x00);		// Normal power mode
  AccelWriteToRegister(accelHandle, 0x21, 0x87);		// Clear INT
#endif
  return ACCELEROMETER_SUCCESS;
}


/**************************************************************************//**
 * @brief Disable the Accelerometer
 *****************************************************************************/
int16_t Accelerometer_Disable(void)
{   // For BMA255 only
  AccelWriteToRegister(accelHandle, 0x12, 0x40);    // low power mode
  AccelWriteToRegister(accelHandle, 0x11, 0x80);    // Suspend power mode
  AccelWriteToRegister(accelHandle, 0x17, 0x00);    // Disable INT enabled.
  AccelWriteToRegister(accelHandle, 0x21, 0x87);    // clear INT so the
  return ACCELEROMETER_SUCCESS;
}


/**************************************************************************//**
 * @brief Reads a single  x,y,z data set
 *****************************************************************************/
int16_t Accelerometer_ReadDataFromDevice(int16_t *accel_x, int16_t *accel_y, int16_t *accel_z)
{
  uint8_t data_buffer[6];
  Accel_i2c_smbus_read_i2c_block_data(accelHandle, 0x02, 6, data_buffer);		// Grab all data registers

  *accel_x = (((uint16_t)(data_buffer[1]) << 8) & 0xff00) | data_buffer[0];            // X
  *accel_y = (((uint16_t)(data_buffer[3]) << 8) & 0xff00) | data_buffer[2];            // Y
  *accel_z = (((uint16_t)(data_buffer[5]) << 8) & 0xff00) | data_buffer[4];            // Z

  return ACCELEROMETER_SUCCESS;
}


/**************************************************************************//**
 * @brief Read specified number of bytes from accelerometer FIFO
 *****************************************************************************/
int16_t Accelerometer_ReadFifo(uint8_t numBytes, uint8_t *data)
{
#if (ACCEL_DEVICE == LSM6DS3)
  return (LSM6DS3_ReadFifo((uint16_t)numBytes, data) == LSM6DS3_OK) ? 1 : 0;
#else
  return AccelBlockRead(accelHandle, 0x3f, numBytes, data);   // each sample is 6 bytes
#endif
}


/**************************************************************************//**
 * @brief Get IRQ Status
 *****************************************************************************/
int16_t Accelerometer_GetIrqStatus(int16_t *status)
{ *status =  AccelReadFromRegister(accelHandle, 0x0a);
  if(*status < 0)
    return *status;
  else
    return ACCELEROMETER_SUCCESS;
}


/**************************************************************************//**
 * @brief Clear the accelerometer IRQ
 *****************************************************************************/
int16_t Accelerometer_ClearIRQ(void)
{
    return AccelWriteToRegister(accelHandle, 0x21, 0x87);  // clear INT
}


/**************************************************************************//**
 * @brief Write to Accel register
 *****************************************************************************/
int16_t AccelWriteToRegister(AccelHandle_t accel_handle, uint8_t address, uint8_t data)
{
	  return Accel_i2c_smbus_write_byte_data(accel_handle, address, data);
}

/**************************************************************************//**
 * @brief Read from Accel register.
 *****************************************************************************/
int16_t AccelReadFromRegister(AccelHandle_t accel_handle, uint8_t address)
{
  uint8_t data;
  Accel_i2c_smbus_read_byte_data(accel_handle, address, &data);
  return data;
}

/**************************************************************************//**
 * @brief block write to Accel
 *****************************************************************************/
int16_t AccelBlockWrite(AccelHandle_t accel_handle,
                     uint8_t address, uint8_t length, uint8_t const *values)
{
  return Accel_i2c_smbus_write_i2c_block_data(accel_handle,
                                               address,
                                               length,
                                               values);
}

/**************************************************************************//**
 * @brief Block read from Accel.
 *****************************************************************************/
int16_t AccelBlockRead(AccelHandle_t accel_handle, uint8_t address, uint8_t length, uint8_t *values)
{
    return Accel_i2c_smbus_read_i2c_block_data(accel_handle,
                           address,    length,     values);
}

/**************************************************************************//**
 * @brief Disable GPIO interrupt for Accel interrupt.
 *****************************************************************************/
void Accelerometer_DisableInterrupt(void)
{

	GPIO_IntDisable(1<<accelHandle->irqPin);
}

/**************************************************************************//**
 * @brief Enable GPIO interrupt for Accel.
 *****************************************************************************/
void Accelerometer_EnableInterrupt(void)
{
	if (GPIO_PinInGet(accelHandle->irqPort, accelHandle->irqPin) == 0)
		GPIO_IntSet(1<<accelHandle->irqPin);
	GPIO_IntEnable(1<<accelHandle->irqPin);
}



/**************************************************************************//**
 * @brief Initialize low level handle and clear irq queue.
 *****************************************************************************/
int16_t AccelInit(AccelPortConfig_t* port, int options, AccelHandle_t* accel_handle)
{
#if (ACCEL_DEVICE == LSM6DS3)
  _handle.i2cAddress = ((AccelPortConfig_t*)port)->i2cAddress<<1;
  _handle.irqPort = ((AccelPortConfig_t*)port)->irqPort;
  _handle.irqPin = ((AccelPortConfig_t*)port)->irqPin;
  _handle.i2c = ((AccelPortConfig_t*)port)->i2c;

  *accel_handle = &_handle;

  return 0;
#else
  int16_t error = 0;
  uint8_t data;
  (void) options;

  _handle.i2cAddress = ((AccelPortConfig_t*)port)->i2cAddress<<1;
  _handle.irqPort = ((AccelPortConfig_t*)port)->irqPort;
  _handle.irqPin = ((AccelPortConfig_t*)port)->irqPin;
  _handle.i2c = ((AccelPortConfig_t*)port)->i2c;

  *accel_handle = &_handle;

  data = AccelReadFromRegister(&_handle, ACCELEROMETER_REG_BGW_CHIPID);

  if ((_handle.i2cAddress == (ACCEL_I2C_ADDR<<1)) && (data != ACCEL_ID))
    error = -1;

  return error;
#endif
}

/**************************************************************************//**
 * @brief Close Accel.
 *****************************************************************************/
int16_t AccelClose(AccelHandle_t accel_handle)
{
  accel_handle->i2cAddress = 0xff;
  return 0;
}

/**************************************************************************//**
 * @brief Reset not implemented.
 *****************************************************************************/
int16_t AccelSysReset(AccelHandle_t accel_handle)
{
  (void) accel_handle;
  return 0;
}


/**************************************************************************//**
 * @brief Write to Accel i2c.
 *****************************************************************************/
static int16_t Accel_i2c_smbus_write_byte_data(AccelHandle_t accel_handle, uint8_t address, uint8_t data)
{
#ifdef NOT_USED
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[2];
  uint8_t i2c_read_data[1];

  Accelerometer_EnterCriticalSection();

  seq.addr  = accel_handle->i2cAddress;
  seq.flags = I2C_FLAG_WRITE;
  /* Select register and data to write */
  i2c_write_data[0] = address;
  i2c_write_data[1] = data;
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 2;
  seq.buf[1].data = i2c_read_data;
  seq.buf[1].len  = 0;

  ret = I2CSPM_Transfer(accel_handle->i2c, &seq);

  Accelerometer_ExitCriticalSection();

  if (ret != i2cTransferDone)
  {
    return((int) ret);
  }
#endif
  return((int) 0);
}

/**************************************************************************//**
 * @brief Read from Accel i2c.
 *****************************************************************************/
static int16_t Accel_i2c_smbus_read_byte_data(AccelHandle_t accel_handle, uint8_t address, uint8_t *data)
{
#ifdef NOT_USED
  //  accel_handle is not used in the EFM32.   We use a global instead
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[1];

  Accelerometer_EnterCriticalSection();

  seq.addr  = accel_handle->i2cAddress;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select register to start reading from */
  i2c_write_data[0] = address;
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 1;
  /* Select length of data to be read */
  seq.buf[1].data = data;
  seq.buf[1].len  = 1;

  ret = I2CSPM_Transfer(accel_handle->i2c, &seq);

  Accelerometer_ExitCriticalSection();

  if (ret != i2cTransferDone)
  {
    *data = 0xff;
    return((int) ret);
  }
  return((int) 1);
#else
  *data = 0xff;
  return((int) 0);
#endif
}

/**************************************************************************//**
 * @brief Write block of data to Accel i2c.
 *****************************************************************************/
static int16_t Accel_i2c_smbus_write_i2c_block_data(AccelHandle_t accel_handle, uint8_t address, uint8_t length, uint8_t const* data)
{
#ifdef NOT_USED
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[10];
  uint8_t i2c_read_data[1];
  int i;

  Accelerometer_EnterCriticalSection();

  seq.addr  = accel_handle->i2cAddress;
  seq.flags = I2C_FLAG_WRITE;
  /* Select register to start writing to*/
  i2c_write_data[0] = address;
  for (i=0; i<length;i++)
  {
    i2c_write_data[i+1] = data[i];
  }
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 1+length;
  seq.buf[1].data = i2c_read_data;
  seq.buf[1].len  = 0;

  ret = I2CSPM_Transfer(accel_handle->i2c, &seq);

  Accelerometer_ExitCriticalSection();

  if (ret != i2cTransferDone)
  {
    return((int) ret);
  }
#endif

  return((int) 0);
}

/**************************************************************************//**
 * @brief Read block of data from Accel i2c.
 *****************************************************************************/
static int16_t Accel_i2c_smbus_read_i2c_block_data(AccelHandle_t accel_handle, uint8_t address, uint8_t length, uint8_t* data)
{
#ifdef NOT_USED
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef ret;
  uint8_t i2c_write_data[1];

  seq.addr  = accel_handle->i2cAddress;
  seq.flags = I2C_FLAG_WRITE_READ;

  Accelerometer_EnterCriticalSection();

  /* Select register to start reading from */
  i2c_write_data[0] = address;
  seq.buf[0].data = i2c_write_data;
  seq.buf[0].len  = 1;

  /* Select length of data to be read */
  seq.buf[1].data = data;
  seq.buf[1].len  = length;

  ret = I2CSPM_Transfer(accel_handle->i2c, &seq);

  Accelerometer_ExitCriticalSection();

  if (ret != i2cTransferDone)
  {
    return((int) ret);
  }
#else
  int i;
  for (i = 0; i < length; i++) {
    data[i] = 0;
  }
#endif

  return((int) 0);
}
