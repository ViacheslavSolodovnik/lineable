#ifndef HRM_APP_H__
#define HRM_APP_H__

#include <stdint.h>

#define Si117xFs       25           // Sensor Sampling rate
#define Si117xSamplesPerChannel  Si117xFs*3/5 // Samples per channel in FIFO
#define SI117X_MODE_SELECTION 		1 	//1--Enable Si117x mode selection (power-saving mode or performance mode). 0--Disable.


/* Brandon: added here compiler definitions of Silab example as C definition  */
//Brandon: removed this since fore easy code review.
//#define SIHRM_RR_INTERVAL_SUPPORT  0     /* skin contact off is not off */
#define SIHRM_POWER_MODE           2

/*  I2C characteristics for accelerometer  */
#define ACCEL_I2C_ADDR 0xFF
#define ACCEL_INT_PORT BSP_ACC_GYRO_INT2_PORT
#define ACCEL_INT_PIN BSP_ACC_GYRO_INT2_PIN

#if 0
#define RED_LED_PORT            gpioPortD
#define RED_LED_PIN             8
#define GREEN_LED_PORT          gpioPortD
#define GREEN_LED_PIN           9
#endif

void hrm_task_start(OS_PRIO task_priority);
void hrm_start(void);
void hrm_stop(void);
uint8_t hrm_get(int16_t *value);


#endif    //HRM_APP_H__
