#include "rtos_app.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "arm_math.h"
#include "em_cmu.h"
#include "em_letimer.h"
#include  <drivers/peripheral/i2c/micrium_i2c.h>
#include "gpiointerrupt.h"
#include "bsp_timer.h"
#include "userdata.h"
#include "timer.h"
#include "si117xhrm.h"
#include "sihrmUser.h"
#include "accelerometer.h"
#include "accel_resample.h"
#include "si117xSkinDetection.h"
#if (ACCEL_DEVICE == LSM6DS3)
#include "lsm6ds3_acc_gyro.h"
#endif
#include <drivers/pmic/pmic.h>
#include "lora_packet.h"
#include "app_main.h"
#include "app_hrm.h"

/*****************************************************************************
* Debug Option
*****************************************************************************/
#undef  __MODULE__
#define __MODULE__ "HRM"
#undef  DBG_MODULE
#define DBG_MODULE 0
#define HRM_DEBUG  0

//#define FEATURE_ADJUST_PPG_THRES

/*****************************************************************************
* Defines
*****************************************************************************/
/*  External Events  */
#define HRM_EVT_SI117X_IRQ           ((OS_FLAGS)(1 <<  0))
#define HRM_EVT_ACCEL_IRQ            ((OS_FLAGS)(1 <<  1))
#define HRM_EVT_HRM_LOOP             ((OS_FLAGS)(1 <<  2))
#define HRM_EVT_START                ((OS_FLAGS)(1 <<  3))
#define HRM_EVT_STOP                 ((OS_FLAGS)(1 <<  4))
#define HRM_EVT_TAKEOFF              ((OS_FLAGS)(1 <<  7))
#define HRM_EVT_FLAGS_ALL            (HRM_EVT_SI117X_IRQ +      \
                                      HRM_EVT_ACCEL_IRQ  +      \
                                      HRM_EVT_HRM_LOOP   +      \
                                      HRM_EVT_START      +      \
                                      HRM_EVT_STOP       +      \
                                      HRM_EVT_TAKEOFF           )

// Boost option for main power source
#define BOOST_ENABLE    0x40
#define BOOST_DISABLE   0x00

#define HRM_VALUE_AVG_NUM   8

/** Note. The power mode must be set to 1 or 2 to enable RR interval support
 *  0 is the default power mode and is the most power efficient
 */
static int power_mode = SIHRM_POWER_MODE;

// Timeout values
#define SKIN_CONTACT_TIMEOUT  49152    //LETIMER runs as 32768Hz...this value is 1.5 seconds of that timer

/** Configuration for the si117xdrv driver  */
static SI117XDRV_GlobalCfg_t si117xDrvGlobalCfg = SI117XDRV_DEFAULT_GLOBAL_CFG;

//#define FEATURE_USE_IOD

Si117xhrmMultiLedDataStorage_t multiLedData;
#ifdef FEATURE_USE_IOD
Si117xhrmIodDataStorage_t ioData;
#endif

Si117xhrmDeviceConfiguration_t deviceConfiguration75G2 = SI117xHRM_DEVICE_CONFIG_DEFAULT_SI1175G2;

Si117xhrmUserConfiguration_t hrmConfiguration =
{
    0x003,                     // task_enable;  PPG1_EN=1.
    0,                         // powerMode  0(Power-saving mode: uses ppg1 on near LED) or 1(Performance mode: uses ppg2 on far LED)
#ifdef FEATURE_USE_IOD
    5,
#else
    0,                         // algorithmControl
#endif
    0,                         // accelerometer sync mode
    305,                       // The timestamp period in 100 nanoseconds - 32768Hz
    40500,                     // accelerometerNormalizationFactorx10
    Si117xSamplesPerChannel,   // numSamplesPerIrq
    0,                         // oversampling
    0,                         // *debugData;
    &deviceConfiguration75G2,  // *deviceConfiguration;
    &multiLedData,             // multiLedDataStorage
    0,
    0                          // iodDataStorage
};

typedef enum HRMState
{
    HRM_STATE_STOPPED = 0,
    HRM_STATE_START,
    HRM_STATE_IDLE,         //hrm is not running...the application periodically checks for skin contact
    HRM_STATE_ACQUIRING,    //hrm has started but it have not yet processed its first frame
    HRM_STATE_ACTIVE,       //hrm is running normally
    HRM_STATE_MAX
} HRMState_t;

static HRMState_t hrmState = HRM_STATE_MAX;

static int32_t idleCount = 0;

static bool initializationDone = false;
/** Structure describing the i2c interface for the accelerometer */
AccelPortConfig_t accelI2C;

static uint16_t si117xIrqTimestamp;
static uint16_t accelIrqTimestamp;


static Si117xhrmDataStorage_t hrmDataStorage;
Si117xhrmHandle_t hrmHandle = 0;

static Si117xhrmData_t hrmData;
static int32_t hrmStatus = 0;

static int32_t g_prev_skinDetect = 0;

static int16_t hrmResult = 0;
// Store the timer value for use to calculate time since last skin contact check
static uint32_t skinContactTimer = 0;


OS_FLAG_GRP hrm_event_flags;
static void hrmTask(void *p_arg);
static OS_TCB hrm_AppTaskTCB;
static CPU_STK hrm_AppTaskStk[HRM_APP_TASK_STK_SIZE];

static TimerEvent_t hrm_measure_timeout;

/*****************************************************************************
* Function prototypes
*****************************************************************************/
static void hrmEnableSkinDetect(void);
static inline void hrmEvtSend(OS_FLAGS evt_flag);
static void GPIO_HRM_IRQHanlder(uint8_t pin);
static void MeasureTimeoutCb(void);

/*****************************************************************************
* Private Functions
*****************************************************************************/

#ifdef NOT_USED
#define RAND_LOCAL_MAX 2147483647L

static uint32_t next = 1;

static int32_t rand1(void)
{
    return ((next = next * 1103515245L + 12345L) % RAND_LOCAL_MAX);
}

static void srand1(uint32_t seed)
{
    next = seed;
}
// Standard random functions redefinition end

static int32_t randr(int32_t min, int32_t max)
{
    return (int32_t)rand1() % (max - min + 1) + min;
}
#endif

static inline void CtrlBoostEnable(bool enable)
{
    if (enable == true)
    {
        #if ((LINEABLE_BOARD_TYPE == TATA_FACTORY_REFERENCE) || (LINEABLE_BOARD_TYPE == TATA_FACTORY_ES1))
        GPIO_PinOutSet(BSP_HRM_BOOST_EN_PORT, BSP_HRM_BOOST_EN_PIN);
        #elif (LINEABLE_BOARD_TYPE == LINEABLE_ONE)
        PMIC_ConfigBoost(BOOST_ENABLE, 0x00);
        #elif (LINEABLE_BOARD_TYPE == LINEABLE_ONE_REV03)
        /* do nothing */
        #else
        #error "Unknown device"
        #endif
    }
    else
    {
        #if ((LINEABLE_BOARD_TYPE == TATA_FACTORY_REFERENCE) || (LINEABLE_BOARD_TYPE == TATA_FACTORY_ES1))
        GPIO_PinOutClear(BSP_HRM_BOOST_EN_PORT, BSP_HRM_BOOST_EN_PIN);
        #elif (LINEABLE_BOARD_TYPE == LINEABLE_ONE)
        PMIC_ConfigBoost(BOOST_DISABLE, 0x00);
        #elif (LINEABLE_BOARD_TYPE == LINEABLE_ONE_REV03)
        /* do nothing */
        #else
        #error "Unknown device"
        #endif
    }
}

static inline void hrmEvtSend(OS_FLAGS evt_flag)
{
    RTOS_ERR err;

    OSFlagPost((OS_FLAG_GRP *)&hrm_event_flags,
               (OS_FLAGS     ) evt_flag,
               (OS_OPT       ) OS_OPT_POST_FLAG_SET,
               (RTOS_ERR    *)&err);
}

static int CalculateFifoIntLevel(Si117xhrmUserConfiguration_t *config)
{
    int numChannels = 0;
    int fifoIntLevel = 0;
    if (config->taskEnable == 0xF)
    {
        numChannels = 4;
    }
    else if (config->powerMode == 2)
    {
        numChannels = 2;
    }
    else
    {
        numChannels = 1;
    }

    fifoIntLevel = 30 * numChannels * ((config->oversampling > 1) ? config->oversampling : 1);

    return fifoIntLevel;
}

/* The global settings of the si117x are configured by the application not
   * by the HRM algorithm.  This allows several algorithms to use the same sensor.
   * The global settings are configured using the function SI117XDRV_InitGlobal()
   * and the si117xDrvGlobalCfg struct below.
   *
   * The below settins configure the si117x for a 25Hz samples rate.  Note that
   * it is possible to configure it to a higher sample rate using the oversampling
   * member of the Si117xhrmUserConfiguration_t struct.  The algorithm always uses
   * 25Hz internally; therefore, the sample rate must be a multiple of 25Hz.
   * See AN1079 for further details
   */

static void Si117xGlobalConfig(Si117xhrmUserConfiguration_t *config)
{
    si117xDrvGlobalCfg.ppgSampleRateus = 40000;
    config->oversampling = 40000 / si117xDrvGlobalCfg.ppgSampleRateus;
    if (config->oversampling == 1)   //algo expects ratio = 0 for no oversampling
    {
        config->oversampling = 0;
    }
    si117xDrvGlobalCfg.taskEnable = (config->taskEnable & 0xf);
    si117xDrvGlobalCfg.fifo_int_level = CalculateFifoIntLevel(config);
    config->accelerometerSynchronizationMode = 0;
    si117xDrvGlobalCfg.synch_mode = config->accelerometerSynchronizationMode;
    si117xDrvGlobalCfg.irq_enable = SI117XDRV_IRQ_EN_FIFO;
    SI117XDRV_Reset(hrmHandle->deviceID);
    SI117XDRV_InitGlobal(hrmHandle->deviceID, &si117xDrvGlobalCfg);
    //configuredForHRV = false;
}

void initHrmApp(void)
{
    int8_t hrmVersion[32];
    LETIMER_Init_TypeDef letimerInit = LETIMER_INIT_DEFAULT;

    if (initializationDone)
    {
        return;
    }

    /*  Initialize the timestamp counter  */
    CMU_ClockEnable(cmuClock_LETIMER0, true);
    CMU_ClockEnable(cmuClock_CORELE, true);
    LETIMER_Init(LETIMER0, &letimerInit);

    accelI2C.i2cAddress = ACCEL_I2C_ADDR;
    accelI2C.irqPin     = ACCEL_INT_PIN;
    accelI2C.irqPort    = ACCEL_INT_PORT;
    accelI2C.i2c        = I2C0;

    /*  Initialize Accelerometer  */
    Accelerometer_Init(&accelI2C);
    /* Initialize interrupt pin */
    GPIO_IntDisable(1 << accelI2C.irqPin);
    GPIO_IntConfig(accelI2C.irqPort, accelI2C.irqPin, true, false, true);
    GPIOINT_CallbackRegister(accelI2C.irqPin, GPIO_HRM_IRQHanlder);

    /* Allocate memory block for hrm  */
    hrmHandle = &hrmDataStorage;

    /* Detect the si117x and initialize low level driver */
    if (Si117xInit(NULL, 0, &(hrmHandle->si117xHandle)) != 0)
    {
        APP_TRACE_DEBUG("HRM 117x Not Found\n");
    }

    /* Initialize the SI117XDRV API  */
    hrmHandle->deviceID = 0;
    SI117XDRV_InitAPI(hrmHandle->deviceID, hrmHandle->si117xHandle);

    /* Get the version of the HRM algorithm */
    si117xhrm_QuerySoftwareRevision(hrmVersion);
    APP_TRACE_DEBUG("HRM algorithm version : %s\n", hrmVersion);

    /* The global settings of the si117x are configured by the application not
     * by the HRM algorithm.  This allows several algorithms to use the same sensor.
     * The global settings are configured using the function SI117XDRV_InitGlobal()
     * and the si117xDrvGlobalCfg struct below.
     *
     * The below settings configure the si117x for a 25Hz samples rate.  Note that
     * it is possible to configure it to a larger sample rate using the oversampling
     * member of the Si117xhrmUserConfiguration_t struct.  The algorithm always uses
     * 25Hz internally; therefore, the sample rate must be a multiple of 25Hz.
     * See AN1079 for further details
     */
    si117xDrvGlobalCfg.ppgSampleRateus = 40000;                      //set the sample rate to 25Hz (40ms)
    si117xDrvGlobalCfg.taskEnable = hrmConfiguration.taskEnable;     //set the task enable the sample as the algorithm Si117xhrmUserConfiguration_t struct
    si117xDrvGlobalCfg.synch_mode = SynchModeNone;                   //We do not use the synch mode of the si117x
    si117xDrvGlobalCfg.fifo_int_level = Si117xSamplesPerChannel * 2; //Set Fifo watermark level in bytes (2 bytes per sample)
    si117xDrvGlobalCfg.irq_enable = SI117XDRV_IRQ_EN_FIFO;

    /*  Initialize the HRM algorithm */
    si117xhrm_Initialize(0, 0, &hrmHandle);
    SI117XDRV_InitGlobal(hrmHandle->deviceID, &si117xDrvGlobalCfg);

    hrmConfiguration.powerMode = power_mode;
    si117xhrm_Configure(hrmHandle, &hrmConfiguration); //This initializes the HRM buffers and variables.
#ifdef FEATURE_USE_IOD
    si117xhrm_EnableInanimateObjectDetection(hrmHandle, 1, &ioData);
#endif
    si117xhrm_SetPowerMode(hrmHandle, power_mode, &multiLedData);

    Si117xInitInterrupt(hrmHandle->si117xHandle, GPIO_HRM_IRQHanlder);

    hrmState = HRM_STATE_MAX;

    initializationDone = true;
}

static void hrmEnableSkinDetect(void)
{
    si117xhrmPPGSkinDetectConfig config = SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_6_IR;
    si117xhrm_EnablePPGSkinDetection(hrmHandle->deviceID, &config);
}

static void hrmStop(void)
{
    si117xhrm_Pause(hrmHandle);
    hrmResult = 0;
    hrmState = HRM_STATE_STOPPED;
}

void hrmLoop(uint8_t *okToSleep)
{
    int32_t skinDetect = 0;
    int16_t numSamplesProcessed;
    int16_t heartRate;
    Si117xhrmData_t *hrmDataPtr = &hrmData;
    int32_t error = SI117xHRM_SUCCESS;
    uint32_t off_time = 0;
    AccelResample_DebugData_t debug_data;
    *okToSleep = 0;

    /* If an event occur before initialization is complete then ignore it */
    if (initializationDone == false) return;

    switch (hrmState)
    {
        case HRM_STATE_STOPPED:
            *okToSleep = 1;
            break;

        case HRM_STATE_START:
            // start timeout timer
            TimerSetValue(&hrm_measure_timeout, TIME_SEC(35));
            TimerStart(&hrm_measure_timeout);
            hrmState = HRM_STATE_IDLE;
        case HRM_STATE_IDLE:
            skinDetect = si117xhrm_IsPPGSkinDetected(hrmHandle->deviceID);

            if (skinDetect == 1)
            {
                si117xhrm_DisablePPGSkinDetection(hrmHandle->deviceID);
                si117xhrm_ClearPPGSkinDetectStatus(hrmHandle->deviceID);
                Si117xGlobalConfig(&hrmConfiguration);
                hrmConfiguration.algorithmControl |= 1;
                si117xhrm_Configure(hrmHandle, &hrmConfiguration); //enable HRM measurements
                #ifdef FEATURE_USE_IOD
                si117xhrm_EnableInanimateObjectDetection(hrmHandle, 1, &ioData);
                #endif
                hrmState = HRM_STATE_ACQUIRING;

                // start the algorithm
                //Skin contact is checked every ~1 seconds in this application.  Therefore
                //idle_count > 14 is true when we have been idle for >14 seconds.
                si117xhrm_Run(hrmHandle, true);
                idleCount = 0;  //Reset the counter for the next idle.
                *okToSleep = 0;
            }
            else
            {
                idleCount++;
                *okToSleep = 1;
            }

            if (g_prev_skinDetect != skinDetect)
            {
                g_prev_skinDetect = skinDetect;
            }
            break;

        case HRM_STATE_ACQUIRING:
        case HRM_STATE_ACTIVE:
            error = si117xhrm_Process(hrmHandle, &heartRate, 1, &numSamplesProcessed, &hrmStatus, hrmDataPtr);
            //APP_TRACE_DEBUG("\t- | %2d | 0x%8x | %4d\n", hrmState, hrmStatus, error);
            AccelResample_QueryDebugData(&debug_data);
            #if HRM_DEBUG
            if (hrmStatus & SI117xHRM_STATUS_HR_UPDATED)
            {
                APP_TRACE_DEBUG("\t- hrmStatus = %2X, error = %d, heartRate = %d\n", hrmStatus, error, heartRate);
                APP_TRACE_DEBUG("debug_data.slip = %d, debug_data.fsRatio = %d\n", debug_data.slip, (uint32_t)(debug_data.fsRatio * 1000));
            }
            #endif

            if (hrmStatus & SI117xHRM_STATUS_PS_DC_SENSE)   //a DC Sense Occurred
            {
                hrmStatus &= ~SI117xHRM_STATUS_PS_DC_SENSE; // Clear the bit.
            }

            if (hrmStatus & SI117xHRM_STATUS_PS_AGC_DONE)   //a PS AGC Occurred
            {
                hrmStatus &= ~SI117xHRM_STATUS_PS_AGC_DONE; // Clear the bit.
            }

            if (hrmStatus & SI117xHRM_STATUS_HR_UPDATED)   // HR result was updated
            {
                hrmStatus &= ~SI117xHRM_STATUS_HR_UPDATED;  //Clear the status bit

                // save the value
                hrmResult = (heartRate + 5) / 10; //+5 is the round-off
                hrmState = HRM_STATE_ACTIVE;
            }

#ifdef FEATURE_USE_IOD
            if (hrmStatus & SI117xHRM_STATUS_INANIMATE_SUBJECT_SUSPECTED)
            {
                hrmStatus &= ~SI117xHRM_STATUS_INANIMATE_SUBJECT_SUSPECTED;     /* Clear the bit. */
                APP_TRACE_DEBUG("$$ INANIMATE SUBJECT\n");
                hrmState = HRM_STATE_IDLE;
                hrmStop();
                hrmEnableSkinDetect();
            }
#endif

            if (error == SI117xHRM_ERROR_SAMPLE_QUEUE_EMPTY)
            {
                *okToSleep = 1;     //if there are no more samples to process then it is OK to sleep
            }
            else if ((hrmStatus == 0) || (error != SI117xHRM_SUCCESS))
            {
                *okToSleep = 1;

                /* Reset hrm driver */
                si117xhrm_Pause(hrmHandle);
                if (idleCount++ > 14)
                {
                    idleCount = 0;
                    APP_TRACE_DEBUG("$$$$$RESET hard Hrm Driver\n");
                    si117xhrm_Run(hrmHandle, true);
                }
                else
                {
                    APP_TRACE_DEBUG("$$$$$RESET soft Hrm Driver\n");
                    si117xhrm_Run(hrmHandle, false);
                }
            }
            else
            {
                *okToSleep = 0;

                idleCount = 0;

                /* The algorithm thinks that skin contact has removed */
                if (hrmStatus & SI117xHRM_STATUS_SKIN_CONTACT_OFF)
                {
                    hrmStatus &= ~SI117xHRM_STATUS_SKIN_CONTACT_OFF;    /* Clear the bit */
                    APP_TRACE_DEBUG("\t --> contact off | ppg1=%d, ppg2=%d\n", PPG1, PPG2);

#ifdef FEATURE_ADJUST_PPG_THRES
                    if ((PPG1 < 6000) && (PPG2 < 6000))
                    {
                        off_time = LETIMER_CounterGet(LETIMER0);
                        if (skinContactTimer >= off_time)
                        {
                            off_time = skinContactTimer - off_time;
                        }
                        else
                        {
                            off_time = (0xFFFF - off_time) + skinContactTimer;
                        }

                        /* wait SKIN_CONTACT_TIMEOUT seconds before going to idle */
                        if (off_time >= SKIN_CONTACT_TIMEOUT)
                        {
                            hrmState = HRM_STATE_IDLE;
                            hrmStop();
                            hrmEnableSkinDetect();
                        }
                    }
#else
                    off_time = LETIMER_CounterGet(LETIMER0);
                    if (skinContactTimer >= off_time)
                    {
                        off_time = skinContactTimer - off_time;
                    }
                    else
                    {
                        off_time = (0xFFFF - off_time) + skinContactTimer;
                    }

                    /* wait SKIN_CONTACT_TIMEOUT seconds before going to idle */
                    if (off_time >= SKIN_CONTACT_TIMEOUT)
                    {
                        hrmState = HRM_STATE_IDLE;
                        hrmStop();
                        hrmEnableSkinDetect();
                    }
#endif
                }
                else
                {
                    skinContactTimer = LETIMER_CounterGet(LETIMER0);
                }
            }
            break;

        default:
            break;
    }
}

static void GPIO_HRM_IRQHanlder(uint8_t pin)
{
    if (pin == BSP_HRM_INT_PIN)
    {
        si117xIrqTimestamp = ~LETIMER_CounterGet(LETIMER0);
        hrmEvtSend(HRM_EVT_SI117X_IRQ);
    }

    if (pin == BSP_ACC_GYRO_INT2_PIN)
    {
        accelIrqTimestamp  = ~LETIMER_CounterGet(LETIMER0);
        hrmEvtSend(HRM_EVT_ACCEL_IRQ);
    }
}

static void hrmTask(void *p_arg)
{
    RTOS_ERR err;
    OS_FLAGS ret_flag = 0;
    uint8_t hrmOkToSleep;
    OS_TICK timeout = 10u;

    initHrmApp();

    while (DEF_ON)
    {
        hrmLoop(&hrmOkToSleep);

        ret_flag = OSFlagPend((OS_FLAG_GRP *)&hrm_event_flags,
                              (OS_FLAGS     ) HRM_EVT_FLAGS_ALL,
                              (OS_TICK      ) ((hrmOkToSleep == 1) ? 0u : timeout),
                              (OS_OPT       ) (OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ANY + OS_OPT_PEND_FLAG_CONSUME),
                              (CPU_TS      *) DEF_NULL,
                              (RTOS_ERR    *)&err);

        if (RTOS_ERR_CODE_GET(err) == RTOS_ERR_TIMEOUT) continue;

        if (ret_flag & HRM_EVT_START)
        {
            APP_TRACE_DEBUG("HRM ON (%d)\n", hrmState);
            if (hrmState == HRM_STATE_STOPPED)
            {
                hrmState = HRM_STATE_START;
                hrmEnableSkinDetect();
                Si117xInitInterrupt(hrmHandle->si117xHandle,  GPIO_HRM_IRQHanlder);
                /* Enable accel interrupt pin */
                if (GPIO_PinInGet(accelI2C.irqPort, accelI2C.irqPin) == 1)
                {
                    GPIO_IntSet(1 << accelI2C.irqPin);
                }
                GPIO_IntEnable(1 << accelI2C.irqPin);
            }
        }

        if (ret_flag & HRM_EVT_STOP)
        {
            APP_TRACE_DEBUG("HRM OFF (%d)\n", hrmState);
            if (hrmState != HRM_STATE_STOPPED)
            {
                Si117xDisableInterrupt(hrmHandle->si117xHandle);
                si117xhrm_DisablePPGSkinDetection(hrmHandle->deviceID);
                Accelerometer_Disable();
                hrmState = HRM_STATE_STOPPED;
                CtrlBoostEnable(false);
            }
        }

        if (ret_flag & HRM_EVT_TAKEOFF)
        {
            if (hrmState != HRM_STATE_STOPPED)
            {
                APP_TRACE_DEBUG("Forced take-off\n");
                hrmState = HRM_STATE_IDLE;
                hrmStop();
                hrmEnableSkinDetect();
            }
        }

        if (ret_flag & HRM_EVT_SI117X_IRQ)
        {
            sihrmUser_ProcessIrq(hrmHandle, si117xIrqTimestamp, &si117xDrvGlobalCfg);
        }

        if (ret_flag & HRM_EVT_ACCEL_IRQ)
        {
            sihrmUser_ProcessIrqAccel(hrmHandle, accelIrqTimestamp, &hrmConfiguration);
        }
    }
}

void hrm_start(void)
{
	bool is_charging = false;
	PMIC_CheckChargerState(&is_charging);

	APP_TRACE_LOG("PMIC_CheckChargerState: %d\n", is_charging);

	if (!is_charging) {
		CtrlBoostEnable(true);
		hrmEvtSend(HRM_EVT_START);        /* start HRM */
	}
}

void hrm_stop(void)
{
    hrmEvtSend(HRM_EVT_STOP);
}

void hrm_forced_takeoff(void)
{
    hrmEvtSend(HRM_EVT_TAKEOFF);
}

uint8_t hrm_get(int16_t *value)
{
    *value = 0;

    if (g_prev_skinDetect == 1)
    {
        *value = hrmResult;
        return STATUS_FLAG_TYPE_WORN;
    }
    else
    {
        return STATUS_FLAG_TYPE_REMOVE;
    }
}

static  CPU_INT16S  hrm_dbg_cmd(CPU_INT16U        argc,
                                CPU_CHAR         *p_argv[],
                                SHELL_OUT_FNCT    out_fnct,
                                SHELL_CMD_PARAM  *p_cmd_param)
{
    if (argc != 2)
    {
        APP_TRACE_DEBUG("usage: hrm [COMMNAD] start, stop\n");
        return SHELL_EXEC_ERR;
    }

    APP_TRACE_DEBUG(" argv[%d] = %s\n", 1, p_argv[1]);

    if (Str_Cmp(p_argv[1], "start") == 0)
    {
        hrm_start();
    }
    else if (Str_Cmp(p_argv[1], "stop") == 0)
    {
        hrm_stop();
    }
    else
    {
        APP_TRACE_DEBUG("not implemented ^^\n");
    }

    return (SHELL_EXEC_ERR_NONE);
}

SHELL_CMD  HrmDbg_CmdTbl [] =
{
    {"hrm",  hrm_dbg_cmd},
    {0,                0}
};

void hrm_task_start(OS_PRIO task_priority)
{
    RTOS_ERR err;

    OSFlagCreate((OS_FLAG_GRP *)&hrm_event_flags,
                 (CPU_CHAR    *) "hrm evt",
                 (OS_FLAGS     ) HRM_EVT_STOP,
                 (RTOS_ERR    *)&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);

    OSTaskCreate((OS_TCB       *)&hrm_AppTaskTCB,
                 (CPU_CHAR     *) "hrm task",
                 (OS_TASK_PTR   ) hrmTask,
                 (void         *) 0u,
                 (OS_PRIO       ) task_priority,
                 (CPU_STK      *)&hrm_AppTaskStk[0u],
                 (CPU_STK_SIZE  ) HRM_APP_TASK_STK_SIZE / 10u,
                 (CPU_STK_SIZE  ) HRM_APP_TASK_STK_SIZE,
                 (OS_MSG_QTY    ) 0u,
                 (OS_TICK       ) 0u,
                 (void         *) 0u,
                 (OS_OPT        ) (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (RTOS_ERR     *)&err);
    APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), ;);

    /* Initialize event timer */
    TimerInit(&hrm_measure_timeout, MeasureTimeoutCb);

#ifdef RTOS_MODULE_COMMON_SHELL_AVAIL
    Shell_CmdTblAdd((CPU_CHAR *)"HRM", HrmDbg_CmdTbl, &err);
#endif
}

static void MeasureTimeoutCb(void)
{
    TimerStop(&hrm_measure_timeout);

    // Stop hrm operation
    hrm_stop();
}

