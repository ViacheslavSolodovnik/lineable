/***************************************************************************//**
 * @file
 * @brief Config file for Driver for Si117x
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2017 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef SI117x_DRIVER_CONFIG_H
#define SI117x_DRIVER_CONFIG_H
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


#define SI117XDRV_MAX_DATA_QUEUES   6
#define SI117XDRV_MAX_NUM_DEVICES   2
#define SI117XDRV_MAX_NUM_CALLBACKS 6

#ifdef __cplusplus
}
#endif

#endif   //SI117x_DRIVER_CONFIG_H
