/***************************************************************************//**
 * @file
 * @brief Driver for Si117x skin detection
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef SI117XSKINDETECTION_H
#define SI117XSKINDETECTION_H

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

/** Skin detect configuration structure. */
typedef struct
{
  uint16_t measrate;
  uint8_t meascount;
  uint8_t ppg4_led1_config;
  uint8_t ppg4_led2_config;
  uint8_t ppg4_led3_config;
  uint8_t ppg4_led4_config;
  uint8_t ppg4_meas_config;
  uint8_t ppg4_threshold;
  uint8_t ppg4_mode;
  uint8_t ppg4_adcconfig;
} si117xhrmPPGSkinDetectConfig;

/** Si117xO2 config to use IR LED */
#define SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_IR  \
  { 0x4e20,    /* measrate */                          \
    0x1,       /* meascount */                         \
    0x18,      /* ppg4_led1_config */                  \
    0x18,      /* ppg4_led2_config */                  \
    0x18,      /* ppg4_led3_config */                  \
    0x58,      /* ppg4_led4_config */                  \
    0x03,      /* ppg4_meas_config */                  \
    0x20,      /* ppg4_threshold */                    \
    0x0F,      /* ppg4_mode */                         \
    0x70,      /* ppg4_adcconfig */                    \
  }

#if 0//org
/** Si117xO2 config to use green LED */
#define SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_GREEN  \
  { 0x4e20,    /* measrate */                             \
    0x1,       /* meascount */                            \
    0x18,      /* ppg4_led1_config */                     \
    0x58,      /* ppg4_led2_config */                     \
    0x18,      /* ppg4_led3_config */                     \
    0x18,      /* ppg4_led4_config */                     \
    0x03,      /* ppg4_meas_config */                     \
    0x20,      /* ppg4_threshold */                       \
    0x0F,      /* ppg4_mode */                            \
    0x70,      /* ppg4_adcconfig */                       \
  }
#else// LED 3 for skin detect
#define SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_GREEN  \
  { 0x4e20,    /* measrate */                             \
    0x1,       /* meascount */                            \
    0x18,      /* ppg4_led1_config */                     \
    0x18,      /* ppg4_led2_config */                     \
    0x58,      /* ppg4_led3_config */                     \
    0x18,      /* ppg4_led4_config */                     \
    0x03,      /* ppg4_meas_config */                     \
    0x20,      /* ppg4_threshold */                       \
    0x0F,      /* ppg4_mode */                            \
    0x70,      /* ppg4_adcconfig */                       \
  }
#endif

/** Si118x evb config to use green LED */
#define SI118XHRM_DEFAULT_EVB_PPG_SKIN_DETECT_CONFIG_GREEN  \
  { 0x4e20,    /* measrate */                             \
    0x1,       /* meascount */                            \
    0x18,      /* ppg4_led1_config */                     \
    0x58,      /* ppg4_led2_config */                     \
    0x18,      /* ppg4_led3_config */                     \
    0x18,      /* ppg4_led4_config */                     \
    0x03,      /* ppg4_meas_config */                     \
    0x18,      /* ppg4_threshold */                       \
    0x0F,      /* ppg4_mode */                            \
    0x70,      /* ppg4_adcconfig */                       \
  }

/** Si118x config to use green LED */
#define SI118XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_GREEN  \
  { 0x4e20,    /* measrate */                             \
    0x1,       /* meascount */                            \
    0x58,      /* ppg4_led1_config */                     \
    0x18,      /* ppg4_led2_config */                     \
    0x18,      /* ppg4_led3_config */                     \
    0x18,      /* ppg4_led4_config */                     \
    0x03,      /* ppg4_meas_config */                     \
    0x20,      /* ppg4_threshold */                       \
    0x0F,      /* ppg4_mode */                            \
    0x70,      /* ppg4_adcconfig */                       \
  }


/** Si119x config to use green LED */
#define EXT_PD_DEFAULT_PPG_SKIN_DETECT_CONFIG_GREEN     \
  { 0x4e20,    /* measrate */                           \
    0x1,       /* meascount */                          \
    0x58,      /* ppg4_led1_config */                   \
    0x18,      /* ppg4_led2_config */                   \
    0x18,      /* ppg4_led3_config */                   \
    0x18,      /* ppg4_led4_config */                   \
    0x03,      /* ppg4_meas_config */                   \
    0x20,      /* ppg4_threshold */                     \
    0x10,      /* ppg4_mode */                          \
    0x70,      /* ppg4_adcconfig */                     \
  }


/** The following #define's exist for backwards compatibility.  Previous versions of si117xSkinDetection code
    used the names shown below.  These #define's map the legacy names to the new names
**/

#define SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_6_IR      SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_IR
#define SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_6_GREEN   SI117XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_GREEN
#define SI118XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_6_GREEN   SI118XHRM_DEFAULT_PPG_SKIN_DETECT_CONFIG_GREEN

/** Error codes */
#define SI117XHRM_PPG_SKIN_DETECT_ERROR_NOT_SUPPORTED -1
#define SI117XHRM_PPG_SKIN_DETECT_NO_ERROR             0

/*******************************************************************************
 *****************************   PROTOTYPES   **********************************
 ******************************************************************************/
int32_t si117xhrm_EnablePPGSkinDetection (SI117XDRV_DeviceSelect_t device,si117xhrmPPGSkinDetectConfig *config);
int32_t si117xhrm_DisablePPGSkinDetection (SI117XDRV_DeviceSelect_t device);
int32_t si117xhrm_PPGSkinDetectInterruptHandler(SI117XDRV_DeviceSelect_t device);
int32_t si117xhrm_IsPPGSkinDetected(SI117XDRV_DeviceSelect_t device);
int32_t si117xhrm_ClearPPGSkinDetectStatus(SI117XDRV_DeviceSelect_t device);


#endif /* SI117XSKINDETECTION_H */
