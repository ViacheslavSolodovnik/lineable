/***************************************************************************//**
 * @file
 * @brief Driver for Si117x skin detection
 * @version 1.0.0
 *******************************************************************************
 * @section License
 * <b>Copyright 2016 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "si117x_functions.h"
#include "si117xdrv.h"
#include "si117xSkinDetection.h"
/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/**************************************************************************//**
 * @brief
 *  Verifies last skin detect event if there was one
 * @param[in] device
 *   DeviceID of the si117x device.  This ID some from si117xdrv API
 * @return
 *   Returns skin detect status
 *****************************************************************************/
int32_t si117xhrm_IsPPGSkinDetected(SI117XDRV_DeviceSelect_t device)
{
  bool si117xSkinDetect;
  SI117XDRV_WristDetection(device, &si117xSkinDetect);
  return si117xSkinDetect;
}

/**************************************************************************//**
 * @brief
 *  Clears skin detect status
 * @param[in] device
 *   DeviceID of the si117x device.  This ID some from si117xdrv API
 * @return
 *   Returns error codes
 *****************************************************************************/
int32_t si117xhrm_ClearPPGSkinDetectStatus(SI117XDRV_DeviceSelect_t device)
{
  SI117XDRV_ClearWristDetection(device);
  return 0;
}

/**************************************************************************//**
 * @brief
 *  Enables skin detection in the si117x
 * @param[in] device
 *   DeviceID of the si117x device.  This ID some from si117xdrv API
 * @param[in] config
 *   Configuration for skin detect
 * @return
 *   Returns error codes
 *****************************************************************************/
int32_t si117xhrm_EnablePPGSkinDetection(SI117XDRV_DeviceSelect_t device, si117xhrmPPGSkinDetectConfig *config)
{
  int retval = ECODE_SI117XDRV_OK;
  SI117XDRV_GlobalCfg_t globalCfg = SI117XDRV_DEFAULT_GLOBAL_CFG;
  SI117XDRV_PPGCfg_t ppgCfg = SI117XDRV_DEFAULT_PPG_CFG;
//	SI117XDRV_ECGLDCfg_t ecgLDCfg = SI117XDRV_DEFAULT_ECG_LD_CFG;
//	SI117XDRV_ECGCfg_t ecgCfg = SI117XDRV_DEFAULT_ECG_CFG;
  globalCfg.ppgSampleRateus = 1000 * 1000;
  globalCfg.ecgldSampleRateus = 1000 * 1000;
  globalCfg.taskEnable = SI117XDRV_TASK_PPG4_EN;// | SI117XDRV_TASK_ECG_LD_EN;
  globalCfg.fifo_int_level = 10;
  globalCfg.irq_enable = SI117XDRV_IRQ_EN_WD; //| SI117XDRV_IRQ_EN_LD;
  retval += SI117XDRV_Reset(device);
  retval += SI117XDRV_InitGlobal(device, &globalCfg);

  //set up ppg1 for green LED verification step
  ppgCfg.ppgCfg[0].ppg_led1_config = 0x18;
  ppgCfg.ppgCfg[0].ppg_led2_config = 0x58;
  ppgCfg.ppgCfg[0].ppg_led3_config = 0x18;
  ppgCfg.ppgCfg[0].ppg_led4_config = 0x18;
  ppgCfg.ppgCfg[0].ppg_mode = 0x0F;
  ppgCfg.ppgCfg[0].ppg_adcconfig = 0x70;
  ppgCfg.ppgCfg[0].ppg_measconfig = 0x03;

  ppgCfg.ppgCfg[3].ppg_led1_config = config->ppg4_led1_config;
  ppgCfg.ppgCfg[3].ppg_led2_config = config->ppg4_led2_config;
  ppgCfg.ppgCfg[3].ppg_led3_config = config->ppg4_led3_config;
  ppgCfg.ppgCfg[3].ppg_led4_config = config->ppg4_led4_config;
  ppgCfg.ppgCfg[3].ppg_mode = config->ppg4_mode;
  ppgCfg.ppgCfg[3].ppg_adcconfig = config->ppg4_adcconfig;
  ppgCfg.ppgCfg[3].ppg_measconfig = config->ppg4_meas_config;
  ppgCfg.ppg4_threshold = config->ppg4_threshold;

//	ecgLDCfg.ecg_ld_freq_sel = 5;
//	ecgLDCfg.ecg_ldconfig = 0x4B;
//	ecgLDCfg.ecg_threshold = 16;
//	ecgCfg.ecg_measconfig = 0x25;
//	ecgCfg.ecg_adcconfig = 0x70;
  retval += SI117XDRV_InitPPG(device, &ppgCfg);
  //SI117XDRV_InitECG(si117xDevice,&ecgCfg);
  //SI117XDRV_InitECGLD(si117xDevice,&ecgLDCfg);
  retval += SI117XDRV_Start(device);

  return retval;
}

/**************************************************************************//**
 * @brief
 *  Disables skin detection in the si117x
 * @param[in] device
 *   DeviceID of the si117x device.  This ID some from si117xdrv API
 * @return
 *   Returns error codes
 *****************************************************************************/
int32_t si117xhrm_DisablePPGSkinDetection(SI117XDRV_DeviceSelect_t device)
{
  SI117XDRV_Stop(device);
  return 0;
}

/**************************************************************************//**
 * @brief
 *  Interrupt handler for skin detect event in the si117x
 * @param[in] device
 *   DeviceID of the si117x device.  This ID some from si117xdrv API
 * @return
 *   Returns error codes
 *****************************************************************************/
int32_t si117xhrm_PPGSkinDetectInterruptHandler(SI117XDRV_DeviceSelect_t device)
{
  //since this code is called from interrupt keep it as short as possible
  SI117XDRV_ProcessWDInterrupt(device);
  return 0;
}
