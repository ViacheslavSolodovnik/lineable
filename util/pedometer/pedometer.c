/*
****************************************************************************************************
*                                             INCLUDE FILES
****************************************************************************************************
*/

#include <stdio.h>
#include "rtos_app.h"

#include "gpiointerrupt.h"
#include "lsm6ds3_acc_gyro.h"

#include "pedometer.h"

/*
****************************************************************************************************
*                                            LOCAL DEFINES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
****************************************************************************************************
*/

/*
****************************************************************************************************
*                                           PUBLIC FUNCTION
****************************************************************************************************
*/

int16_t PEDOMETER_Init(void)
{
    return 0;
}

int16_t PEDOMETER_ReadData(uint32_t *steps)
{
    int8_t err = 0;

    err = LSM6DS3_StepCountRead(steps);
    if (err == 0) {
        /* Reset step count */
        LSM6DS3_StepCountReset();
    }

    return err;
}

/*
****************************************************************************************************
*                                           LOCAL FUNCTION
****************************************************************************************************
*/

