/*
*********************************************************************************************************
*                                               uC/Shell
*                                            Shell utility
*
*                           (c) Copyright 2007-2013; Micrium, Inc.; Weston, FL
*
*                   All rights reserved.  Protected by international copyright laws.
*                   Knowledge of the source code may not be used to write a similar
*                   product.  This file may only be used in accordance with a license
*                   and should not be redistributed in any way.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                              TERMINAL
*
*                                    TEMPLATE COMMUNICATIONS PORT
*
* Filename      : terminal_serial.c
* Version       : V1.03.01
* Programmer(s) : BAN
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/
#include <stdio.h>
#include "rtos_app.h"
#include "em_device.h"
#include "em_cmu.h"
#include "em_core.h"
#include "em_gpio.h"
#include  "inc/terminal.h"

/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                     LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/

#define RETARGET_TXPORT           BSP_SERIAL_APP_TX_PORT
#define RETARGET_TXPIN            BSP_SERIAL_APP_TX_PIN
#define RETARGET_RXPORT           BSP_SERIAL_APP_RX_PORT
#define RETARGET_RXPIN            BSP_SERIAL_APP_RX_PIN

// Series 1 devices have one location per pin
#define RETARGET_TX_LOCATION      BSP_SERIAL_APP_TX_LOC
#define RETARGET_RX_LOCATION      BSP_SERIAL_APP_RX_LOC

#if BSP_SERIAL_APP_PORT == HAL_SERIAL_PORT_USART0
// USART0
#define RETARGET_UART       USART0
#define RETARGET_CLK        cmuClock_USART0
#define RETARGET_IRQ_NAME   USART0_RX_IRQHandler
#define RETARGET_IRQn       USART0_RX_IRQn
#define RETARGET_USART      1
#elif BSP_SERIAL_APP_PORT == HAL_SERIAL_PORT_USART1
// USART1
#define RETARGET_UART       USART1
#define RETARGET_CLK        cmuClock_USART1
#define RETARGET_IRQ_NAME   USART1_RX_IRQHandler
#define RETARGET_IRQn       USART1_RX_IRQn
#define RETARGET_USART      1
#elif BSP_SERIAL_APP_PORT == HAL_SERIAL_PORT_USART2
// USART2
#define RETARGET_UART       USART2
#define RETARGET_CLK        cmuClock_USART2
#define RETARGET_IRQ_NAME   USART2_RX_IRQHandler
#define RETARGET_IRQn       USART2_RX_IRQn
#define RETARGET_USART      1
#elif BSP_SERIAL_APP_PORT == HAL_SERIAL_PORT_USART3
// USART3
#define RETARGET_UART       USART3
#define RETARGET_CLK        cmuClock_USART3
#define RETARGET_IRQ_NAME   USART3_RX_IRQHandler
#define RETARGET_IRQn       USART3_RX_IRQn
#define RETARGET_USART      1
#elif BSP_SERIAL_APP_PORT == HAL_SERIAL_PORT_LEUART0
// LEUART0
#define RETARGET_UART       LEUART0
#define RETARGET_CLK        cmuClock_LEUART0
#define RETARGET_IRQ_NAME   LEUART0_IRQHandler
#define RETARGET_IRQn       LEUART0_IRQn
#define RETARGET_LEUART     1
#else
#error "retarget port was not defined"
#endif

#if defined(RETARGET_USART)
// UART is an USART
#define RETARGET_TX         USART_Tx
#define RETARGET_RX         USART_RxDataGet //no need to check the rx is valid in the new routine
#elif defined(RETARGET_LEUART)
// UART is a LEUART
#define RETARGET_TX         LEUART_Tx
#define RETARGET_RX         LEUART_RxDataGet //no need to check the rx is valid in the new routine
#else
#error "Invalid UART type"
#endif



/***************************************************************************//**
 * @addtogroup RetargetIo
 * @{
 ******************************************************************************/

#if defined(RETARGET_USART)
#include "em_usart.h"
#endif

#if defined(RETARGET_LEUART)
#include "em_leuart.h"
#endif

static bool             LFtoCRLF    = true;        /**< LF to CRLF conversion disabled */
static bool             initialized = false;    /**< Initialize UART/LEUART */

//typedef void (*term_rx_callback)(void *user_data, uint16_t written);

/* Receive FIFO buffer */
typedef struct tag_ring_buffer_t{
    uint16_t    head;// write position
    uint16_t    tail;// read  position
    uint16_t    size;
    uint8_t     *element;
} ring_buffer_t;

#define DO_BUFFER_EMPTY(X)    ((X)->tail = (X)->head)
#define IS_BUFFER_EMPTY(X)    ((X)->tail == (X)->head)
#define IS_BUFFER_FULL(X)     ((((X)->head + 1)%(X)->size) ==(X)->tail)
static void ring_buffer_init(ring_buffer_t *rb, uint8_t *buffer, uint32_t size)
{
    rb->head = rb->tail = 0;
    rb->size = size;
    rb->element = buffer;
}

static void ring_buffer_reset(ring_buffer_t *rb )
{
    rb->head = rb->tail = 0;
}
#define TERM_SERIAL_RX_FIFO_SIZE            64
static uint8_t term_serial_rx_fifo[TERM_SERIAL_RX_FIFO_SIZE];
static ring_buffer_t term_serial_rx_rb;
/**************************************************************************//**
 * @brief Disable RX interrupt
 *****************************************************************************/
/**************************************************************************//**
 * @brief Enable RX interrupt
 *****************************************************************************/
#if defined(RETARGET_USART)
#define disableRxInterrupt()  USART_IntDisable(RETARGET_UART, USART_IF_RXDATAV)
#define enableRxInterrupt()   USART_IntEnable(RETARGET_UART, USART_IF_RXDATAV)
#else
#define disableRxInterrupt()  LEUART_IntDisable(RETARGET_UART, LEUART_IF_RXDATAV)
#define enableRxInterrupt()   LEUART_IntEnable(RETARGET_UART, LEUART_IF_RXDATAV)
#endif

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     : UART/LEUART IRQ Handler
 *Remarks         :
 ******************************************************************************************************/
extern OS_TCB   Terminal_OS_TaskTCB;
static void termUartRxISR(void)
{
    RTOS_ERR err;
    ring_buffer_t *rb = &term_serial_rx_rb;
#if defined(RETARGET_USART)
  if ((RETARGET_UART->STATUS & USART_STATUS_RXDATAV)==0 || (rb->element == NULL)) {
#else
  if ((RETARGET_UART->IF & LEUART_IF_RXDATAV)==0 || (rb->element == NULL)) {
#endif
        return;
    }

    uint16_t next = rb->head + 1;

    if(IS_BUFFER_EMPTY(rb)){
        // the first new data from empty
        OSTaskSemPost(&Terminal_OS_TaskTCB, OS_OPT_POST_NONE, &err);
        //APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), RTOS_ERR_CODE_GET(err));
    }

    if(next >= rb->size){
        next = 0;
    }

    if( next == rb->tail){
        /* The RX buffer is full so we must wait for the RETARGET_ReadChar()
        * function to make some more room in the buffer. RX interrupts are
        * disabled to let the ISR exit. The RX interrupt will be enabled in ReadChar(). */
        disableRxInterrupt();
        return;
    }
    //not full

    rb->element[rb->head] = RETARGET_RX(RETARGET_UART);
    rb->head = next;

}
/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          : 1 : 1 byte read, 0: no data, -1: error
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
static int32_t termReadFifo( CPU_INT08U *pc)
{
    ring_buffer_t *rb = &term_serial_rx_rb;
    CPU_SR_ALLOC(); //CORE_DECLARE_IRQ_STATE

    if(rb->element){
        uint16_t rd_idx = rb->tail;
        if( rd_idx == rb->head ){
            /* Unconditionally enable the RX interrupt. RX interrupts are disabled when
            * a buffer full condition is entered. This way flow control can be handled
            * automatically by the hardware. */
            enableRxInterrupt();
            return 0;// buffer empty
        }

        CPU_CRITICAL_ENTER(); //ORE_ENTER_ATOMIC();
        {
            *pc = rb->element[rd_idx++];
            if(rd_idx >= rb->size)
                rd_idx = 0;
            rb->tail = rd_idx;
        }
        CPU_CRITICAL_EXIT(); //CORE_EXIT_ATOMIC();

        return 1;// 1byte read
    }
    //("RING BUFFER ERROR\r\n");
    return -1;// NO RING BUFFER CASE
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :  test function
 *Remarks         :
 ******************************************************************************************************/
int termReadPolling(CPU_INT08U *ptr, CPU_SIZE_T len)
{
    int rxBytes = 0;

    disableRxInterrupt();
    while (len--) {
#if defined(RETARGET_USART)
        *ptr = USART_Rx(RETARGET_UART);
#elif defined(RETARGET_LEUART)
        *prt = LEUART_Rx(RETARGET_UART);
#endif
        ptr++;
        rxBytes++;
    }
    enableRxInterrupt();
    return rxBytes;
}
/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         : this is only for retarget
 ******************************************************************************************************/
int termRead(CPU_INT08U *ptr, CPU_SIZE_T len)
{
    int rxBytes = 0;

    // no need to check this, always TX first
    //if (initialized == false) {
    //    TerminalSerial_Init();
    //}

    while (len--) {
        if(termReadFifo(ptr)> 0){
            ptr++;
            rxBytes++;
        }
        else{
            break;
        }
    }

    // need to wait until len

    return rxBytes;
}

/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     :
 *Remarks         :
 ******************************************************************************************************/
static inline int in_irq_context()
{
    if (OSIntNestingCtr > 0)
        return 1;
    else
        return 0;
}
/*****************************************************************************************************
 *Function Name   :
 *Input           :
 *Output          :
 *Description     : Flush UART/LEUART
 *Remarks         :
 ******************************************************************************************************/
static void termUartFlush(void)
{
#if defined(RETARGET_USART)

#if defined(USART_STATUS_TXIDLE)
#define _GENERIC_UART_STATUS_IDLE     USART_STATUS_TXIDLE
#else
#define _GENERIC_UART_STATUS_IDLE     USART_STATUS_TXC
#endif

#else

#if defined(LEUART_STATUS_TXIDLE)
#define _GENERIC_UART_STATUS_IDLE     LEUART_STATUS_TXIDLE
#else
#define _GENERIC_UART_STATUS_IDLE     LEUART_STATUS_TXC
#endif

#endif

  while (!(RETARGET_UART->STATUS & _GENERIC_UART_STATUS_IDLE)) ;
}

/**********************************************************************************************************
*                                        TerminalSerial_Init()
*
* Description : Initialize serial communications.
*
* Argument(s) : none.
*
* Return(s)   : DEF_OK,   if interface was opened.
*               DEF_FAIL, otherwise.
*
* Caller(s)   : Terminal_Init().
*
* Note(s)     : none.
**********************************************************************************************************/
CPU_BOOLEAN  TerminalSerial_Init (void)
{
    if (initialized == true)
        return DEF_OK;

    /* Enable peripheral clocks */
#if defined(_CMU_HFPERCLKEN0_MASK)
    CMU_ClockEnable(cmuClock_HFPER, true);
#endif
    /* Configure GPIO pins */
    CMU_ClockEnable(cmuClock_GPIO, true);
    /* To avoid false start, configure output as high */
    GPIO_PinModeSet(RETARGET_TXPORT, RETARGET_TXPIN, gpioModePushPull, 1);
    GPIO_PinModeSet(RETARGET_RXPORT, RETARGET_RXPIN, gpioModeInputPull, 1);

#if defined(RETARGET_USART)
    USART_TypeDef           *usart = RETARGET_UART;
    USART_InitAsync_TypeDef init   = USART_INITASYNC_DEFAULT;
    init.baudrate = 9600;

    CMU_ClockEnable(RETARGET_CLK, true);

    /* Configure USART for basic async operation */
    init.enable = usartDisable;
    USART_InitAsync(usart, &init);

    #if defined(GPIO_USART_ROUTEEN_TXPEN)
    /* Enable pins at correct UART/USART location. */
    GPIO->USARTROUTE[RETARGET_UART_INDEX].ROUTEEN =
    GPIO_USART_ROUTEEN_TXPEN | GPIO_USART_ROUTEEN_RXPEN;
    GPIO->USARTROUTE[RETARGET_UART_INDEX].TXROUTE =
    (RETARGET_TXPORT << _GPIO_USART_TXROUTE_PORT_SHIFT)
    | (RETARGET_TXPIN << _GPIO_USART_TXROUTE_PIN_SHIFT);
    GPIO->USARTROUTE[RETARGET_UART_INDEX].RXROUTE =
    (RETARGET_RXPORT << _GPIO_USART_RXROUTE_PORT_SHIFT)
    | (RETARGET_RXPIN << _GPIO_USART_RXROUTE_PIN_SHIFT);

    #else
    /* Enable pins at correct UART/USART location. */
    #if defined(USART_ROUTEPEN_RXPEN)
    usart->ROUTEPEN = USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
    usart->ROUTELOC0 = (usart->ROUTELOC0
                        & ~(_USART_ROUTELOC0_TXLOC_MASK
                            | _USART_ROUTELOC0_RXLOC_MASK) )
                        | (RETARGET_TX_LOCATION << _USART_ROUTELOC0_TXLOC_SHIFT)
                        | (RETARGET_RX_LOCATION << _USART_ROUTELOC0_RXLOC_SHIFT);
    #else
    usart->ROUTE = USART_ROUTE_RXPEN | USART_ROUTE_TXPEN | RETARGET_LOCATION;
    #endif
    #endif

    /* Clear previous RX interrupts */
    USART_IntClear(RETARGET_UART, USART_IF_RXDATAV);
    NVIC_ClearPendingIRQ(RETARGET_IRQn);

    ring_buffer_init(&term_serial_rx_rb, term_serial_rx_fifo, TERM_SERIAL_RX_FIFO_SIZE);

    /* Enable RX interrupts */
    USART_IntEnable(RETARGET_UART, USART_IF_RXDATAV);
    NVIC_EnableIRQ(RETARGET_IRQn);

    CPU_IntSrcHandlerSetKA(RETARGET_IRQn + CPU_INT_EXT0, termUartRxISR);
    /* Finally enable it */
    USART_Enable(usart, usartEnable);

#else
    LEUART_TypeDef      *leuart = RETARGET_UART;
    LEUART_Init_TypeDef init    = LEUART_INIT_DEFAULT;

    /* Enable CORE LE clock in order to access LE modules */
    CMU_ClockEnable(cmuClock_CORELE, true);

    #if defined(RETARGET_VCOM)
    /* Select HFXO/2 for LEUARTs (and wait for it to stabilize) */
    #if defined(_CMU_LFCLKSEL_LFB_HFCORECLKLEDIV2)
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
    #else
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_HFCLKLE);
    #endif
    #else
    /* Select LFXO for LEUARTs (and wait for it to stabilize) */
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
    #endif

    CMU_ClockEnable(RETARGET_CLK, true);

    /* Do not prescale clock */
    CMU_ClockDivSet(RETARGET_CLK, cmuClkDiv_1);

    /* Configure LEUART */
    init.enable = leuartDisable;
    #if defined(RETARGET_VCOM)
    init.baudrate = 115200;
    #endif
    LEUART_Init(leuart, &init);
    /* Enable pins at default location */
    #if defined(LEUART_ROUTEPEN_RXPEN)
    leuart->ROUTEPEN = USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
    leuart->ROUTELOC0 = (leuart->ROUTELOC0
                        & ~(_LEUART_ROUTELOC0_TXLOC_MASK
                            | _LEUART_ROUTELOC0_RXLOC_MASK) )
                        | (RETARGET_TX_LOCATION << _LEUART_ROUTELOC0_TXLOC_SHIFT)
                        | (RETARGET_RX_LOCATION << _LEUART_ROUTELOC0_RXLOC_SHIFT);
    #else
    leuart->ROUTE = LEUART_ROUTE_RXPEN | LEUART_ROUTE_TXPEN | RETARGET_LOCATION;
    #endif

    /* Clear previous RX interrupts */
    LEUART_IntClear(RETARGET_UART, LEUART_IF_RXDATAV);
    NVIC_ClearPendingIRQ(RETARGET_IRQn);

    ring_buffer_init(&term_serial_rx_rb, term_serial_rx_fifo, TERM_SERIAL_RX_FIFO_SIZE);

    /* Enable RX interrupts */
    LEUART_IntEnable(RETARGET_UART, LEUART_IF_RXDATAV);
    NVIC_EnableIRQ(RETARGET_IRQn);

    /* Finally enable it */
    LEUART_Enable(leuart, leuartEnable);
#endif

#if !defined(__CROSSWORKS_ARM) && defined(__GNUC__)
    setvbuf(stdout, NULL, _IONBF, 0);   /*Set unbuffered mode for stdout (newlib)*/
#endif

    LFtoCRLF    = true;
    initialized = true;
    termUartFlush();

    return (DEF_OK);
}


/**********************************************************************************************************
*                                        TerminalSerial_Exit()
*
* Description : Uninitialize serial communications.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : Terminal_Init().
*
* Note(s)     : none.
**********************************************************************************************************/

void  TerminalSerial_Exit (void)
{
    disableRxInterrupt();
    ring_buffer_reset(&term_serial_rx_rb);
    // Disable Rx route
    RETARGET_UART->ROUTEPEN &= ~USART_ROUTEPEN_RXPEN;

    GPIO_PinModeSet(RETARGET_TXPORT, RETARGET_TXPIN, gpioModeDisabled, 0);
    GPIO_PinModeSet(RETARGET_RXPORT, RETARGET_RXPIN, gpioModeDisabled, 0);

    RETARGET_UART->CMD = USART_CMD_RXDIS;
    RETARGET_UART->CMD = USART_CMD_TXDIS;

    CMU_ClockEnable(RETARGET_CLK, false);
}


/**********************************************************************************************************
*                                         TerminalSerial_Wr()
*
* Description : Serial output.
*
* Argument(s) : pbuf        Pointer to the buffer to transmit.
*
*               buf_len     Number of bytes in the buffer.
*
* Return(s)   : sent bytes.
*
* Caller(s)   : Terminal_Out().
*
* Note(s)     : none.
**********************************************************************************************************/

CPU_INT16S  TerminalSerial_Wr (CPU_CHAR     *pbuf,
                               CPU_SIZE_T   buf_len)
{
    int left = (int)buf_len;
    char c;

    if (initialized == false) {
        TerminalSerial_Init();
    }

    while ( left-- > 0){
        c = *pbuf++;
        /* Add CR or LF to CRLF if enabled */
        if((c == '\n')&& LFtoCRLF) {
            RETARGET_TX(RETARGET_UART, '\r');
        }

        RETARGET_TX(RETARGET_UART, c);
    }

    return (CPU_INT16S)(buf_len - left);
}


/**********************************************************************************************************
*                                       TerminalSerial_RdByte()
*
* Description : Serial byte input.
*
* Argument(s) : none.
*
* Return(s)   : Byte read from port.
*
* Caller(s)   : various.
*
* Note(s)     : none.
**********************************************************************************************************/

CPU_INT08U  TerminalSerial_RdByte (void)
{
    CPU_INT08U ch;
    RTOS_ERR err;
    OS_SEM_CTR ctr;
    ring_buffer_t *rb = &term_serial_rx_rb;

    ctr = 0;
    while(DEF_TRUE){

        if(IS_BUFFER_EMPTY(rb)){
            // Block until another task signals this task.
            ctr = OSTaskSemPend(0,
                            OS_OPT_PEND_BLOCKING,
                            DEF_NULL,
                            &err);
            APP_RTOS_ASSERT_DBG((RTOS_ERR_CODE_GET(err) == RTOS_ERR_NONE), RTOS_ERR_CODE_GET(err));
        }

        if(termReadFifo(&ch)> 0){
            break;
        }
    }

    (void)ctr;                                                  // Prevent compiler warning for not using 'ctr'
    return ch;
}

/**********************************************************************************************************
*                                       TerminalSerial_WrByte()
*
* Description : Serial byte output.
*
* Argument(s) : c           Byte to write.
*
* Return(s)   : none.
*
* Caller(s)   : various.
*
* Note(s)     : none.
**********************************************************************************************************/

void  TerminalSerial_WrByte (CPU_INT08U  c)
{
    TerminalSerial_Wr((CPU_CHAR *)&c, 1);
}

void TerminalSerial_ClearBuffer(void)
{
    ring_buffer_t *rb = &term_serial_rx_rb;

    DO_BUFFER_EMPTY(rb);
}

