/*
 * hrm_sensor_max30101.h
 *
 *  Created on: 2018. 5. 25.
 *      Author: Eliott
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef APP_HRM_SENSOR_HRM_SENSOR_MAX30101_H_
#define APP_HRM_SENSOR_HRM_SENSOR_MAX30101_H_

/* Exported macro ------------------------------------------------------------*/
/* Exported Types ------------------------------------------------------------*/
/* Exported Constants --------------------------------------------------------*/
/**
* @brief  I2C address.
*/
#define MAX30101_ADDRESS   (uint8_t)0x57	//before shift, SA0 ground

#define WORN			0x01
#define NOTWORN			0x00

//status
#define HRM_SENSOR_OK                             (0x0000)  /**< No errors                                        */
#define HRM_SENSOR_ERROR_DRIVER_NOT_INITIALIZED   (0x0001)  /**< The driver is not initialized                    */
#define HRM_SENSOR_ERROR_I2C_TRANSACTION_FAILED   (0x0002)  /**< I2C transaction failed                           */
#define HRM_SENSOR_ERROR_DEVICE_ID_MISMATCH       (0x0003)  /**< The device ID does not match the expected value  */

#define MAX_INT_STATUS              0x00
#define MAX_INT_ENABLE              0x02
#define MAX_FIFO_WRITE_PTR          0x04
#define MAX_OVERFLOW_CNTR           0x05
#define MAX_FIFO_READ_PTR           0x06
#define MAX_FIFO_DATA_REG           0x07
#define MAX_FIFO_CONFIG             0x08
#define MAX_MODE_CONF_REG           0x09
#define MAX_SP02_CONF               0x0A
#define MAX_RED_LED_CURRENT         0x0C
#define MAX_IR_LED_CURRENT          0x0D
#define MAX_GREEN_LED_CURRENT       0x0E
#define MAX_PROX_MODE_LED_CURRENT   0x10
#define MAX_MULTIMODE_LED_CONTROL_1 0x11
#define MAX_MULTIMODE_LED_CONTROL_2 0x12
#define MAX_PROXIMITY_THRESHOLD     0x30
#define MAX_DEVICE_ID				0xFF

#define EN_HR_SCAN          0x02
#define EN_SPO2_SCAN        0x03
#define EN_MULTI_LED        0x07

#define RESET_VALUE         0x40
#define SHUTDOWN_VALUE      0x80

#define MULTIMODE_RED_LED     0x01
#define MULTIMODE_IR_LED      0x02
#define MULTIMODE_GREEN_LED   0x03
#define MULTIMODE_RED_PILOT   0x05
#define MULTIMODE_IR_PILOT    0x06
#define MULTIMODE_GREEN_PILOT 0x07

#define SPO2_SAMP_50HZ      0x00
#define SPO2_SAMP_100HZ     0x01
#define SPO2_SAMP_200HZ     0x02
#define SPO2_SAMP_400HZ     0x03
#define SPO2_SAMP_800HZ     0x04
#define SPO2_SAMP_1000HZ    0x05
#define SPO2_SAMP_1600HZ    0x06
#define SPO2_SAMP_3200HZ    0x07

#define SPO2_ADC_15BIT      0x00
#define SPO2_ADC_16BIT      0x01
#define SPO2_ADC_17BIT      0x02
#define SPO2_ADC_18BIT      0x03

#define SPO2_ADCRANGE_2048  0x00
#define SPO2_ADCRANGE_4096  0x01
#define SPO2_ADCRANGE_8192  0x02
#define SPO2_ADCRANGE_16384 0x03

//INTERRUPT CONTROL REGISTER
#define EN_PROXIMITY        0x10
#define EN_AMBIENTLIGHT_OV  0x20
#define EN_PPG_DATA_RDY     0x40
#define EN_FIFO_FULL_INT    0x80
#define PWR_RDY_INTERRUPT   0x01

//add
#define MAX30101_WHO_AM_I_VAL	0x15

/* Exported functions ------------------------------------------------------- */
extern uint8_t MAX30101_Init(void);
extern void MAX30101_InitProximity(void);
extern uint8_t Check_WearStatus(void);
extern int8_t MAX30101_WriteRegister( uint8_t reg, uint8_t data);
extern int8_t MAX30101_ReadRegister( uint8_t reg, uint8_t *Pdata);

#endif /* APP_HRM_SENSOR_HRM_SENSOR_MAX30101_H_ */
