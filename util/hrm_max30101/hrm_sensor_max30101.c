/*
 * hrm_sensor_max30101.c
 *
 *  Created on: 2018. 5. 25.
 *      Author: Eliott
 */

/* Includes ------------------------------------------------------------------*/
#include "rtos_app.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "gpiointerrupt.h"
#include "hrm_sensor_max30101.h"
#include "i2cspm.h"

#if (configUSE_HRM == HRM_DEVICE_MAX30101)


#if 0// copy form debug.c 
{"prt", proximity_test},
static  CPU_INT16S  proximity_test (CPU_INT16U        argc,
                                 CPU_CHAR         *argv[],
                                 SHELL_OUT_FNCT    out_fnct,
                                 SHELL_CMD_PARAM  *p_cmd_param)
{
    RTOS_ERR err;
    uint8_t test_cnt = 10;    //10sec.

    while(test_cnt--)
    {
        if( Check_WearStatus() == WORN ){
            printf("worn detection\r\n");
        }else{
            printf("not worn\r\n");
        }

        OSTimeDly(OS_MS_2_TICKS(1000), OS_OPT_TIME_DLY, &err);    //1sec.
    }

  return (SHELL_EXEC_ERR_NONE);
}


#endif

/***************************************************************************//**
 * Local Function Prototypes
 ******************************************************************************/
static int8_t HRMSensor_i2cBus_Read(uint8_t devAddr, uint8_t regAddr, uint8_t *regData, uint8_t count);
static uint8_t MAX30101_Get_DeviceID(void);
/***************************************************************************//**
 * Local Variables
 ******************************************************************************/
/***************************************************************************//**
 * Local & Extern Functions
 ******************************************************************************/
static int8_t HRMSensor_i2cBus_Read(uint8_t devAddr, uint8_t regAddr, uint8_t *regData, uint8_t count)
{
#if 1
    I2C_TransferReturn_TypeDef ret;
    ret = I2CSPM_MasterRead(i2cmsId1, devAddr, &regAddr, 1, regData, count );
    if ( ret != i2cTransferDone ) {
      return HRM_SENSOR_ERROR_I2C_TRANSACTION_FAILED;
    }
    
    return HRM_SENSOR_OK;

#else
  I2C_TransferSeq_TypeDef seq;
  I2C_TransferReturn_TypeDef ret;

  seq.addr = devAddr << 1;
  seq.flags = I2C_FLAG_WRITE_READ;

  seq.buf[0].len  = 1;
  seq.buf[0].data = &regAddr;
  seq.buf[1].len  = count;
  seq.buf[1].data = regData;

#if 0
  BOARD_i2cBusSelect(BOARD_I2C_BUS_SELECT_COMMON);
#else
  I2C1->ROUTELOC0 = BSP_I2C1_ROUTELOC0;
  I2C1->ROUTEPEN = (I2C_ROUTEPEN_SCLPEN | I2C_ROUTEPEN_SDAPEN);
#endif

  ret = I2CSPM_Transfer(I2C1, &seq);

  if ( ret != i2cTransferDone ) {
    return HRM_SENSOR_ERROR_I2C_TRANSACTION_FAILED;
  }

  return HRM_SENSOR_OK;
#endif

}

/**
* @brief  I2C write to MAX 30101 register
* @param  none
* @param
* @retval Status
*/
int8_t MAX30101_WriteRegister( uint8_t reg, uint8_t data)
{
	I2C_TransferReturn_TypeDef ret;

    ret = I2CSPM_MasterWrite(i2cmsId1, MAX30101_ADDRESS, &reg, 1, &data, 1);

    if ( ret != i2cTransferDone ) {
    	return HRM_SENSOR_ERROR_I2C_TRANSACTION_FAILED;
    }

    return HRM_SENSOR_OK;
}

/**
* @brief  I2C read to MAX 30101 register
* @param  none
* @param
* @retval Status
*/
int8_t MAX30101_ReadRegister( uint8_t reg, uint8_t *Pdata)
{
	I2C_TransferReturn_TypeDef ret;
    uint8_t read_val;

    ret = I2CSPM_MasterRead(i2cmsId1, MAX30101_ADDRESS, &reg, 1, &read_val, 1);
    *Pdata = read_val;

    if ( ret != i2cTransferDone ) {
     	return HRM_SENSOR_ERROR_I2C_TRANSACTION_FAILED;
    }

    return HRM_SENSOR_OK;
}

/**
* @brief  Read identification code by DEVICEID register
* @param  none
* @param  Buffer to fill by Device identification Value.
* @retval Status
*/
uint8_t MAX30101_Get_DeviceID(void)
{
	uint8_t deviceid;
	uint8_t value[1];

	if( HRMSensor_i2cBus_Read(MAX30101_ADDRESS, MAX_DEVICE_ID, value, 1)!= 0 ){
		return HRM_SENSOR_ERROR_I2C_TRANSACTION_FAILED;
	}

	deviceid = value[0];

	//printf("device id : 0x%x\r\n", deviceid);

	if( deviceid != MAX30101_WHO_AM_I_VAL ){
		return HRM_SENSOR_ERROR_DEVICE_ID_MISMATCH;
	}

	return HRM_SENSOR_OK;
}

/**
* @brief  Initailize MAX30101.
* @param  none
* @retval Status
*/
uint8_t MAX30101_Init(void)
{
    if( MAX30101_Get_DeviceID() != 0 )
    {
    	return HRM_SENSOR_ERROR_DEVICE_ID_MISMATCH;
    }

    MAX30101_InitProximity();	//set proximity register
    return HRM_SENSOR_OK;
}

/**
* @brief  Initailize MAX30101 proximity.
* @param  none
* @retval Status
*/
void MAX30101_InitProximity(void)
{
	MAX30101_WriteRegister(MAX_INT_ENABLE,EN_PROXIMITY);
	MAX30101_WriteRegister(MAX_MODE_CONF_REG,EN_SPO2_SCAN);
	MAX30101_WriteRegister(MAX_SP02_CONF,0x00);
	MAX30101_WriteRegister(MAX_SP02_CONF,(uint8_t)((SPO2_ADCRANGE_2048 << 5) | (SPO2_SAMP_1600HZ << 2) | SPO2_ADC_18BIT));	//range modified
	MAX30101_WriteRegister(MAX_PROX_MODE_LED_CURRENT,0x0C);  //0C
	MAX30101_WriteRegister(MAX_MULTIMODE_LED_CONTROL_1,MULTIMODE_IR_LED); // 06 IR
	MAX30101_WriteRegister(MAX_MULTIMODE_LED_CONTROL_2,0x00);
//	MAX30101_WriteRegister(MAX_PROXIMITY_THRESHOLD,0x10);  //07  //TODO threshold value description
	MAX30101_WriteRegister(MAX_PROXIMITY_THRESHOLD,0x40);
}

/**
  * @brief  Checking for Proximity Interrupt for WORN Detection
  * @param  void
  * @retval Returns WORN or NOT_WORN
  */
uint8_t Check_WearStatus(void)
{
	uint8_t read_val;

    MAX30101_ReadRegister(MAX_INT_STATUS, &read_val);

    if(read_val & EN_PROXIMITY){
        return WORN;
    }else{
		return NOTWORN;
	}
}

/**
  * @brief  Initializing the Green HRM LED
  * 		Setting up sensor in Multi mode for collecting PPG data using GREEN LED
  *         Enabling Interrupt on Fifo full , Fifo is set to interrupt at 15 samples in fifo buffer, Fifo roll over is enabled
  * 		Led current is set to max 50mA
  *         PPG data configurations is set for 18bit ADC and 200Hz samping rate
  *
  * @param  void

  * @retval void
  */
#define LED_CURRENT 0xFF //0xFF - 50mA  //Select LED Current

void MAX30101_Init_GreenHRM(void)  //TODO return status of all I2C writes
{
	MAX30101_WriteRegister(MAX_INT_ENABLE,EN_FIFO_FULL_INT);   //Interrupt enable - PPG RDY Int
	MAX30101_WriteRegister(MAX_FIFO_WRITE_PTR,0x00);   	//FIFO WRT pointer
	MAX30101_WriteRegister(MAX_OVERFLOW_CNTR,0x00);   	//FIFO Overflow count
	MAX30101_WriteRegister(MAX_FIFO_READ_PTR,0x00);   	//FIFO READ Pointer
	MAX30101_WriteRegister(MAX_FIFO_DATA_REG,0x00);   	//FIFO data register
	MAX30101_WriteRegister(MAX_FIFO_CONFIG,0x15);   	//0x0F - Interrupt when 15 samples fifolengths are placed in the buffer
   	   	   	   	   	   	   	   	   	   	   	   	   	   	//0x10 - Roll over the fifo enabled

	MAX30101_WriteRegister(MAX_SP02_CONF,(uint8_t)((SPO2_ADCRANGE_8192 << 7) | (SPO2_SAMP_50HZ << 2) | SPO2_ADC_18BIT));
	MAX30101_WriteRegister(MAX_MODE_CONF_REG,EN_MULTI_LED);
	MAX30101_WriteRegister(MAX_GREEN_LED_CURRENT,LED_CURRENT); //Setting Current
	MAX30101_WriteRegister(MAX_MULTIMODE_LED_CONTROL_1,MULTIMODE_GREEN_LED);  //Turning GREEN LED in the Multi Mode
	MAX30101_WriteRegister(MAX_MULTIMODE_LED_CONTROL_2,0x00);
}

#endif
