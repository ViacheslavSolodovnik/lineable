#ifndef _HRM_LIB_H
#define _HRM_LIB_H
    
int GetHeartRate(float (*FFTOutput)[2], int SamplingRate, float PeakThresHold);
int Preprocessing(double *rawData, double SignalThresh, int SamplingRate);
//void FFT(int N, float *x, float (*X)[2]) ;
//void fft_rec(int N, int offset, int delta, float *x, float (*X)[2],float (*XX)[2]);


void fft(int N, double *x, float (*X)[2]);
void fft_rec(int N, int offset, int delta, double *x, float(*X)[2],float(*XX)[2]);

#define TWO_PI (6.2831853071795864769252867665590057683943L)
#define TOTALNOOFSAMPLES  512
#define SAMPLINGRATE      50

typedef unsigned char uint8_t;
#endif 
